recommended configuration for cmdbuild

* create cmdbuild conf directory in ${catalina.base}/conf/<cmdbuild_webapp_context>:

	mkdir -p <tomcat_home>/conf/cmdbuild

* copy default config files, modify at will:

	mv <tomcat_home>/webapps/cmdbuild/WEB-INF/conf/{database.conf,logback.xml} <tomcat_home>/conf/cmdbuild/
	
	vim <tomcat_home>/conf/cmdbuild/database.conf

done! now you can replace the whole cmdbuild webapp on update, without losing any config.

have a nice day :)

PS: if cmdbuild is deployed under webapps/my_cmdbuild, then <cmdbuild_webapp_context> = my_cmdbuild
    (and you should put you config files under ${catalina.base}/conf/my_cmdbuild/ ).
