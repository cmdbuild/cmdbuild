package org.cmdbuild.webapp;

import org.cmdbuild.config.leveltwo.UiFilterConfigurationImpl;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.Subscribe;
import java.io.IOException;
import static java.lang.String.format;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.Charsets;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.config.UiFilterConfiguration;
import org.cmdbuild.config.api.ConfigReloadEvent;
import static org.cmdbuild.config.leveltwo.UiFilterConfigurationImpl.AUTO_URL;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.ContentCachingResponseWrapper;

/**
 * this filters is used to embed global configuration inside main javascript
 * file; it is mostly used to set the 'baseUrl' param, that javascript code will
 * use to call rest ws.
 * <br><br>
 * TODO: cache response (or put this whole filter behind a caching filter)
 */
@Configuration
public class UiFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private UiFilterConfiguration config;
	private CmdbCache<String, byte[]> cache;

	@Autowired
	public void setCacheService(CacheService cacheService) {
		cache = cacheService.newCache("ui_code_by_path");
	}

	@Autowired
	public void setUiFilterConfiguration(UiFilterConfiguration configuration) {
		this.config = checkNotNull(configuration);
		config.getEventBus().register(new Object() {

			@Subscribe
			public void handleConfigUpdateEvent(ConfigReloadEvent event) {
				cache.invalidateAll();
			}
		});
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//do nothing; this init method is not invoked by spring configured filters
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		logger.debug("ui filter doFilter BEGIN");
		try {
			String resourceRequestUrl = ((HttpServletRequest) request).getRequestURL().toString();
			HttpServletResponse httpResponse = (HttpServletResponse) response;

			byte[] responseData = cache.get(resourceRequestUrl, () -> {
				try {

					String uiFilterUrlPattern = checkNotNull(trimToNull(config.getRequestUrlPattern()));

					ContentCachingResponseWrapper contentCachingResponseWrapper = new ContentCachingResponseWrapper(httpResponse);

					filterChain.doFilter(request, contentCachingResponseWrapper);
					logger.debug("ui filter, processing");

					// get url to replace from request url
					String baseRequestUrl = config.getBaseUrl();
					if (equal(baseRequestUrl, AUTO_URL)) {
						Matcher matcher = Pattern.compile(uiFilterUrlPattern).matcher(resourceRequestUrl);
						checkArgument(matcher.find(), "resource request url = %s does not match this filter url pattern = %s", resourceRequestUrl, uiFilterUrlPattern);
						baseRequestUrl = checkNotNull(trimToNull(matcher.group(1)), "request url parsing failed, using pattern = '%s'", uiFilterUrlPattern);
					}

					String content = new String(contentCachingResponseWrapper.getContentAsByteArray(), Charsets.UTF_8);

					String requestUrl = baseRequestUrl + "/services/rest/v3";
					String geoserverUrl = baseRequestUrl + "/services/geoserver";
					String bimserverUrl = baseRequestUrl + "/services/bimserver";

					logger.debug("set request url = {} geoserver url = {} bimserver url = {} (base url = {})", requestUrl, geoserverUrl, bimserverUrl, baseRequestUrl);

					content = replacePattern(content, "['\"]?baseUrl['\"]? *: *['\"](http[^'\"]+)['\"]", format("\"baseUrl\":\"%s\",\"processedByUiFilter\":true", requestUrl));//TODO escape base request url for javascript

					content = replacePattern(content, "['\"]?geoserverBaseUrl['\"]? *: *['\"](http[^'\"]+)['\"]", format("\"geoserverBaseUrl\":\"%s\"", geoserverUrl));//TODO escape base request url for javascript

					//TODO fix bim proxy, re enable this
//					content = replacePattern(content, "['\"]?bimserverBaseUrl['\"]? *: *['\"](http[^'\"]+)['\"]", format("\"bimserverBaseUrl\":\"%s\"", bimserverUrl));//TODO escape base request url for javascript

					return content.getBytes(Charsets.UTF_8);
				} catch (Exception ex) {
					throw runtime(ex);
				}
			});

			httpResponse.setHeader("CMDBuild-ProcessedByUiFilter", Boolean.TRUE.toString());
			response.setContentType("application/javascript");
			response.setContentLength(responseData.length);
			response.getOutputStream().write(responseData);

			logger.debug("ui filter doFilter END");
		} catch (Exception ex) {
			logger.error("error in ui filter", ex);
			throw ex;
		}
	}

	private String replacePattern(String content, String pattern, String replacement) {
		Matcher matcher = Pattern.compile(pattern).matcher(content);
		StringBuffer stringBuffer = new StringBuffer();
		if (matcher.find()) {
			matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(replacement));
			matcher.appendTail(stringBuffer);
			return stringBuffer.toString();
		} else {
			logger.warn("error processing ui js code, unable to find pattern = {}", pattern);
			return content;
		}
	}

}
