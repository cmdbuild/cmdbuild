package org.cmdbuild.webapp;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import static org.cmdbuild.webapp.DirectoryServiceImpl.getConfigFile;
import org.cmdbuild.webapp.log.LogbackConfigurator;
import static org.cmdbuild.webapp.log.LogbackConfigurator.LOGBACK_FILE_NAME;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * a servlet context listener to handle logback configuration (load at startup,
 * and reload on change via {@link FileAlterationMonitor}).
 *
 * @author davide
 */
public class LogbackConfigurationContextListener implements ServletContextListener {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		logger.info("load logback configuration");
		LogbackConfigurator.getInstance().configureLogbackSafe(getConfigFile(servletContextEvent.getServletContext(), LOGBACK_FILE_NAME));
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		//nothing to do
	}

}
