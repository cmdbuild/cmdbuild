/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import java.io.File;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import java.util.List;
import javax.annotation.Nullable;
import javax.servlet.ServletContext;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.DirectoryService;

/**
 *
 */
@Component("configDirectoryService")
public class DirectoryServiceImpl implements DirectoryService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final File rootDirectory, configDirectory, catalinaBase;

	public DirectoryServiceImpl(ServletContext servletContext) {
		checkNotNull(servletContext);
		rootDirectory = getFile("cmdbuild webapp root", () -> getServletContextRoot(servletContext));
		configDirectory = getFile("cmdbuild config dir", () -> getConfigDirectory(servletContext));
		catalinaBase = getFile("catalina base dir", () -> new File(checkNotBlank(System.getProperty("catalina.base"))));
	}

	@Override
	public DeploymentMode getDeploymentMode() {
		return DeploymentMode.TOMCAT;
	}

	@Override
	public File getConfigDirectory() {
		return checkNotNull(configDirectory, "config directory not available (check startup logs for errors)");
	}

	@Override
	public File getWebappRootDirectory() {
		return checkNotNull(rootDirectory, "root directory not available (check startup logs for errors)");
	}

	@Override
	public File getCatalinaBaseDirectory() {
		return checkNotNull(catalinaBase, "catalina base directory not available (check startup logs for errors)");
	}

	private @Nullable
	File getFile(String logInfo, Supplier<File> supplier) {
		File file;
		try {
			file = supplier.get();
			logger.info("{} is = {}", logInfo, file.getAbsolutePath());
		} catch (Exception ex) {
			logger.error("error loading {}", logInfo, ex);
			file = null;
		}
		return file;
	}

	public static File getConfigDirectory(ServletContext servletContext) {

		String cmdbuildContext = getServletContextRoot(servletContext).getName();

		List<String> candidatePaths = asList(
				servletContext.getInitParameter("configLocation"),//legacy param value
				servletContext.getInitParameter("org.cmdbuild.config.location"), //new param value
				format("%s/conf/%s", System.getProperty("catalina.base"), cmdbuildContext),
				servletContext.getRealPath("WEB-INF/conf"),
				getServletContextRootPath(servletContext) + "/WEB-INF/conf"); // this is required if WEB-INF/config is a symlink (for example when we emulate a cluster on a single host, and want to share config dir via symlink)

		for (String path : candidatePaths) {
			if (!isBlank(path)) {
				File file = new File(path);
				if (file.isDirectory()) {
					return file;
				}
			}
		}

		throw new RuntimeException("unable to find config directory");

	}

	public static File getConfigFile(ServletContext servletContext, String fileName) {
		return new File(getConfigDirectory(servletContext), fileName);
	}

	public static String getServletContextRootPath(ServletContext servletContext) {
		return servletContext.getRealPath("/");
	}

	public static File getServletContextRoot(ServletContext servletContext) {
		return new File(getServletContextRootPath(servletContext));
	}

}
