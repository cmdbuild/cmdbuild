/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * a session token filter that allows invalid sessions
 */
@Component
public class LenientSessionTokenFilter extends SessionTokenFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected boolean allowSessionsWithoutGroup() {
		return true;
	}

}
