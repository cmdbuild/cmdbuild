/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp.security;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.notNull;
import com.google.common.collect.ImmutableMap;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import org.cmdbuild.auth.grant.UserPrivilegesForObject;
import static org.cmdbuild.auth.login.AuthorityConst.ADMIN_ACCESS_AUTHORITY;
import static org.cmdbuild.auth.login.AuthorityConst.SYSTEM_ACCESS_AUTHORITY;
import org.cmdbuild.auth.role.RolePrivilege;
import static org.cmdbuild.auth.role.RolePrivilege.RP_ADMIN_ACCESS;
import static org.cmdbuild.auth.role.RolePrivilege.RP_SYSTEM_ACCESS;
import org.cmdbuild.auth.user.OperationUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.cmdbuild.auth.user.OperationUserStack;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 *
 */
public class AuthenticatedUser implements Authentication, OperationUserStack {

	private final static Map<RolePrivilege, GrantedAuthority> ROLE_PRIVILEGE_AUTHORITY_MAPPING = ImmutableMap.of(
			RP_ADMIN_ACCESS, new SimpleGrantedAuthority(ADMIN_ACCESS_AUTHORITY),
			RP_SYSTEM_ACCESS, new SimpleGrantedAuthority(SYSTEM_ACCESS_AUTHORITY));

	private final OperationUserStack operationUserStack;

	public AuthenticatedUser(OperationUserStack operationUserStack) {
		this.operationUserStack = checkNotNull(operationUserStack);
	}

	// Authentication impl BEGIN
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return operationUserStack.getRolePrivileges().stream().map(ROLE_PRIVILEGE_AUTHORITY_MAPPING::get).filter(notNull()).collect(toList());
	}

	@Override
	public Object getCredentials() {
		return "";
	}

	@Override
	public Object getDetails() {
		return "";
	}

	@Override
	public Object getPrincipal() {
		return getName();
	}

	@Override
	public boolean isAuthenticated() {
		return true;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		throw new UnsupportedOperationException("this user is always authenticated");
	}

	@Override
	public String getName() {
		return operationUserStack.getLoginUser().getUsername();
	}

	// Authentication impl END
	@Override
	public String toString() {
		return "MyAuthenticatedUser{" + "username=" + getName() + '}';
	}

	// operation user stack BEGIN
	@Override
	public OperationUser getCurrentOperationUser() {
		return operationUserStack.getCurrentOperationUser();
	}

	@Override
	public OperationUser getRootOperationUser() {
		return operationUserStack.getRootOperationUser();
	}

	@Override
	public List<OperationUser> getOperationUserStack() {
		return operationUserStack.getOperationUserStack();
	}

	@Override
	public int getOperationUserStackSize() {
		return operationUserStack.getOperationUserStackSize();
	}

	@Override
	public Set<RolePrivilege> getRolePrivileges() {
		return operationUserStack.getRolePrivileges();
	}

	@Override
	public Map<String, UserPrivilegesForObject> getAllPrivileges() {
		return operationUserStack.getAllPrivileges();
	}

	// operation user stack END
}
