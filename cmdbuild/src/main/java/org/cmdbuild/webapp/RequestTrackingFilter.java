/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Strings;
import com.google.common.base.Supplier;
import static com.google.common.base.Suppliers.memoize;
import com.google.common.eventbus.EventBus;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import static java.lang.Math.toIntExact;
import static java.lang.String.format;
import java.nio.charset.Charset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.apache.tika.Tika;
import org.cmdbuild.audit.RequestData;
import org.cmdbuild.audit.RequestEventService;
import org.cmdbuild.audit.RequestEventServiceImpl.RequestBeginEventImpl;
import org.cmdbuild.audit.RequestEventServiceImpl.RequestCompleteEventImpl;
import static org.cmdbuild.audit.RequestInfo.NO_ACTION_ID;
import static org.cmdbuild.audit.RequestInfo.NO_SESSION_ID;
import static org.cmdbuild.audit.RequestInfo.NO_USER_AGENT;
import static org.cmdbuild.audit.RequestInfo.PAYLOAD_TRACKING_DISABLED;
import static org.cmdbuild.audit.RequestInfo.RESPONSE_TRACKING_DISABLED;
import org.cmdbuild.audit.RequestDataImpl;
import static org.cmdbuild.audit.RequestDataUtils.buildBinaryPayload;
import org.cmdbuild.audit.RequestTrackingService;
import org.cmdbuild.common.error.ErrorAndWarningCollectorService;
import static org.cmdbuild.common.http.HttpConst.CMDBUILD_ACTION_ID_HEADER;
import static org.cmdbuild.common.http.HttpConst.CMDBUILD_REQUEST_ID_HEADER;
import static org.cmdbuild.webapp.security.SessionTokenFilter.getSessionTokenFromRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;
import org.cmdbuild.config.RequestTrackingConfiguration;
import org.cmdbuild.requestcontext.RequestContextService;
import org.cmdbuild.utils.date.DateUtils;
import static org.cmdbuild.utils.lang.CmdbStringUtils.truncate;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.codehaus.plexus.util.FileUtils;

@Configuration("RequestTrackingFilter")
public class RequestTrackingFilter extends OncePerRequestFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final RequestContextService requestContextService;
	private final RequestTrackingConfiguration config;
	private final RequestTrackingService service;
	private final EventBus eventBus;
	private final ErrorAndWarningCollectorService errorAndWarningCollectorService;
	private final Tika tika = new Tika();

	public RequestTrackingFilter(RequestContextService requestContextService, RequestEventService eventService, RequestTrackingConfiguration config, RequestTrackingService service, ErrorAndWarningCollectorService errorAndWarningCollectorService) {
		this.config = checkNotNull(config);
		this.service = checkNotNull(service);
		this.errorAndWarningCollectorService = checkNotNull(errorAndWarningCollectorService);
		this.eventBus = eventService.getEventBus();
		this.requestContextService = checkNotNull(requestContextService);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

		String trackingId = randomId(), requestId,
				requestIdFromHeader = trimToNull(request.getHeader(CMDBUILD_REQUEST_ID_HEADER));

		if (requestIdFromHeader == null) {
			requestId = trackingId.replaceFirst("....$", "_GEN");
			logger.trace("processing request with null requestId (using generated requestId = {})", requestId);
		} else {
			requestId = truncate(requestIdFromHeader, 50);
		}

		requestContextService.initCurrentRequestContext(format("request %s", requestId));
		eventBus.post(RequestBeginEventImpl.INSTANCE);

		String requestPath = request.getServletPath() + request.getPathInfo();

		boolean trackRequest = config.isRequestTrackingEnabled();
		if (trackRequest && !isBlank(config.getRegexForPathsToInclude())) {
			trackRequest = Pattern.compile(config.getRegexForPathsToInclude()).matcher(requestPath).find();
		}
		if (trackRequest && !isBlank(config.getRegexForPathsToExclude())) {
			trackRequest = !Pattern.compile(config.getRegexForPathsToExclude()).matcher(requestPath).find();
		}

		logger.trace("request BEGIN, track request = {}", trackRequest);

		HttpServletRequest requestToUse;
		HttpServletResponse responseToUse;

		boolean isFirstRequest = !isAsyncDispatch(request);
		if (isFirstRequest && !(request instanceof MyContentCachingRequestWrapper)) {
			requestToUse = new MyContentCachingRequestWrapper(request);
		} else {
			requestToUse = request;
		}

		if (isFirstRequest && !(response instanceof ContentCachingResponseWrapper)) {
			responseToUse = new ContentCachingResponseWrapper(response);
		} else {
			responseToUse = response;
		}

		ZonedDateTime beginTimestamp = DateUtils.now();

		Supplier<RequestData> requestBeginDataSupplier = memoize(() -> {
			return RequestDataImpl.builder()
					.withActionId(firstNonNull(trimToNull(request.getHeader(CMDBUILD_ACTION_ID_HEADER)), NO_ACTION_ID))
					.withRequestId(requestId)
					.withTrackingId(trackingId)
					.withSessionId(firstNonNull(getSessionTokenFromRequest(request), NO_SESSION_ID))
					.withClient(request.getRemoteAddr())
					.withUserAgent(firstNonNull(trimToNull(request.getHeader("user-agent")), NO_USER_AGENT))
					.withMethod(request.getMethod())
					.withPath(requestPath)
					.withQuery(Strings.nullToEmpty(request.getQueryString()))
					.withTimestamp(beginTimestamp)
					.withPayloadSize(request.getContentLength()) // request size from header
					.build();
		});

		//try to track request before processing
		if (trackRequest) {
			try {
				service.requestBegin(requestBeginDataSupplier.get());
			} catch (Exception ex) {
				logger.warn("error tracking request begin", ex);
			}
		}

		try {
			//do filter
			response.setHeader("CMDBuild-TrackingId", trackingId);
			filterChain.doFilter(requestToUse, responseToUse);
		} finally {

			logger.trace("request END, track request = {}", trackRequest);

			boolean includeRequestPayload;
			boolean includeResponsePayload;

			if (errorAndWarningCollectorService.getCurrentRequestEventCollector().hasErrors()) {
				logger.trace("response has error, turn on full tracking for request requestId = {} trackingId = {}", requestId, trackingId);
				trackRequest = true;
				includeRequestPayload = true;
				includeResponsePayload = true;
			} else {
				includeRequestPayload = config.includeRequestPayload();
				includeResponsePayload = config.includeResponsePayload();
			}

			ContentCachingResponseWrapper responseWrapper = checkNotNull(WebUtils.getNativeResponse(responseToUse, ContentCachingResponseWrapper.class), "unable to get response wrapper");

			//try to track request after processing
			if (trackRequest) {
				try {
					long elapsed = beginTimestamp.until(DateUtils.now(), ChronoUnit.MILLIS);
					RequestData requestComplete = RequestDataImpl.copyOf(requestBeginDataSupplier.get())
							.withElapsedTimeMillis(toIntExact(elapsed))
							.withStatusCode(response.getStatus())
							.withResponseSize(Integer.valueOf(firstNonNull(trimToNull(response.getHeader("Content-Length")), "-1"))) //response size from header
							.accept((builder) -> {
								try {
									if (includeRequestPayload) {
										MyContentCachingRequestWrapper wrapper = checkNotNull(WebUtils.getNativeRequest(requestToUse, MyContentCachingRequestWrapper.class), "unable to get request wrapper");
										byte[] data = wrapper.getContentAsByteArray();
										builder.withPayloadSize(data.length).withPayload(dataToString(data, wrapper.getContentType(), wrapper.getCharacterEncoding()));
									} else {
										builder.withPayload(PAYLOAD_TRACKING_DISABLED);
									}
								} catch (Exception ex) {
									logger.warn("error retrieving request payload for tracking", ex);
									builder.withPayload("PAYLOAD_TRACKING_ERROR: error retrieving request payload: " + ex.toString());
								}
								try {
									if (includeResponsePayload) {
										byte[] data = responseWrapper.getContentAsByteArray();
										builder.withResponseSize(data.length).withResponse(dataToString(data, responseWrapper.getContentType(), responseWrapper.getCharacterEncoding()));
									} else {
										builder.withResponse(RESPONSE_TRACKING_DISABLED);
									}
								} catch (Exception ex) {
									logger.warn("error retrieving response payload for tracking", ex);
									builder.withResponse("RESPONSE_TRACKING_ERROR: error retrieving response payload: " + ex.toString());
								}
							})
							.withErrorsAndWarningEvents(errorAndWarningCollectorService.getCurrentRequestEventCollector().getCollectedEvents())
							.build();
					service.requestComplete(requestComplete);
				} catch (Exception ex) {
					logger.warn("error tracking request completion", ex);
				}
			}

			responseWrapper.copyBodyToResponse();

			eventBus.post(RequestCompleteEventImpl.INSTANCE);
			requestContextService.destroyCurrentRequestContext();
		}

	}

	private String dataToString(byte[] data, @Nullable String contentType, @Nullable String characterEncoding) throws UnsupportedEncodingException {
		if (data.length == 0) {
			return "";
		} else {
			if (isBlank(contentType)) {
				contentType = tika.detect(data);
			}
			boolean isBinary = !(contentType.contains("text") || contentType.contains("json") || contentType.contains("x-www-form-urlencoded"));
			if (isBinary) {
				return buildBinaryPayload(format("%s (%s bytes)", contentType, FileUtils.byteCountToDisplaySize(data.length)), data);
			} else {
				if (isBlank(characterEncoding)) {
					//TODO this is not really correct. Http specs say that default character encoding should be ISO-something (see WebUtils.DEFAULT_CHARACTER_ENCODING). defaultCharset here is probably UTF8
					// BUT, utf8 is generally correct even if not explicitly set
					characterEncoding = Charset.defaultCharset().name();
				}
				return new String(data, characterEncoding);
			}
		}
	}

}
