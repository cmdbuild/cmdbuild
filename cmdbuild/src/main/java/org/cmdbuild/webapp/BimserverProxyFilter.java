/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.Iterables.getOnlyElement;
import java.io.IOException;
import static java.lang.Math.toIntExact;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.cmdbuild.config.BimserverConfiguration;
import static org.cmdbuild.config.UiFilterConfiguration.AUTO_URL;
import org.cmdbuild.config.leveltwo.UiFilterConfigurationImpl;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.lteqZeroToNull;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BimserverProxyFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private BimserverConfiguration configuration;
	@Autowired
	private UiFilterConfigurationImpl config;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//do nothing; this init method is not invoked by spring configured filters
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		logger.debug("bimserver proxy filter doFilter BEGIN");
		try {
			checkArgument(configuration.isEnabled(), "bimserver is not enabled");

			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;

			String requestPath = httpServletRequest.getRequestURI().replaceFirst(Pattern.quote(((HttpServletRequest) request).getContextPath()) + "/services/bimserver/?", "");

			logger.debug("bimserver proxy filter, processing request = {}", requestPath);

			if (requestPath.equals("x.getbimserveraddress")) {

				String baseUrl = config.getBaseUrl();
				if (equal(baseUrl, AUTO_URL)) {
					String pattern = "^(.*)/services/bimserver/.*";
					Matcher matcher = Pattern.compile(pattern).matcher(httpServletRequest.getRequestURL().toString());
					checkArgument(matcher.find(), "resource request url = %s does not match this filter url pattern = %s", httpServletRequest.getRequestURL(), pattern);
					baseUrl = checkNotNull(trimToNull(matcher.group(1)), "request url parsing failed, using pattern = '%s'", pattern);
				}

				httpServletResponse.setContentType("application/json");
				httpServletResponse.getWriter().write(toJson(map("address", baseUrl + "/services/bimserver")));

			} else {

				String requestUrl = checkNotBlank(configuration.getUrl(), "bimserver url not configured").replaceFirst("/$", "") + "/" + requestPath;
				if (isNotBlank(httpServletRequest.getQueryString())) {
					requestUrl += "?" + httpServletRequest.getQueryString();
				}

				HttpUriRequest httpRequest = buildRequest(requestUrl, httpServletRequest);

				logger.debug("forward call to bimserver url = {}", requestUrl);
				proxyRequest(httpRequest, httpServletResponse);
			}

			logger.debug("bimserver proxy filter doFilter END");
		} catch (Exception ex) {
			logger.error("error in bimserver proxy filter", ex);
			throw runtime(ex, "error processing bimserver proxy filter");//TODO return properly formatted json response
		}

	}

	public static HttpUriRequest buildRequest(String requestUrl, HttpServletRequest httpServletRequest) throws IOException {
		String method = httpServletRequest.getMethod();
		switch (method.toUpperCase()) {
			case HttpGet.METHOD_NAME:
				return new HttpGet(requestUrl);
			case HttpPost.METHOD_NAME:
				HttpPost httpPost = new HttpPost(requestUrl);
				httpPost.setHeader("Content-Type", httpServletRequest.getContentType());
				if (nullToEmpty(httpServletRequest.getContentType()).toLowerCase().contains("application/x-www-form-urlencoded")) {
					httpPost.setEntity(new StringEntity(getOnlyElement(httpServletRequest.getParameterMap().keySet())));//TODO improve this; thiw works only for single-entry form post without value, a degenerate case used by bimserver
				} else {
					httpPost.setEntity(new InputStreamEntity(httpServletRequest.getInputStream()));
				}
				return httpPost;
			default:
				throw unsupported("unsupported method = %s", method);
		}
	}

	public static void proxyRequest(HttpUriRequest httpRequest, HttpServletResponse httpServletResponse) throws IOException {
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build(); CloseableHttpResponse clientResponse = httpClient.execute(httpRequest)) {//TODO cache client
			String contentType = clientResponse.getFirstHeader("content-type").getValue();
			String contentDisposition = Optional.ofNullable(clientResponse.getFirstHeader("Content-Disposition")).map(Header::getValue).orElse(null);
			Integer resposeStatus = Optional.ofNullable(clientResponse.getStatusLine()).map(StatusLine::getStatusCode).orElse(null);
			Long contentLength = lteqZeroToNull(clientResponse.getEntity().getContentLength());

			if (resposeStatus != null) {
				httpServletResponse.setStatus(resposeStatus);
			}
			httpServletResponse.setContentType(contentType);
			if (isNotBlank(contentDisposition)) {
				httpServletResponse.setHeader("Content-Disposition", contentDisposition);
			}
			if (contentLength != null) {
				httpServletResponse.setContentLength(toIntExact(contentLength));//TODO use long method when moving to servlet api 3.1
			}
			if (clientResponse.getEntity() != null) {
				clientResponse.getEntity().writeTo(httpServletResponse.getOutputStream());
				EntityUtils.consume(clientResponse.getEntity());
			}
		}
	}

}
