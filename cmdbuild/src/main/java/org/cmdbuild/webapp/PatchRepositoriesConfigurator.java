package org.cmdbuild.webapp;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.util.List;

import org.cmdbuild.dao.config.inner.FilesStoreRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.config.inner.PatchFileRepository;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import org.cmdbuild.config.api.DirectoryService;

@Component
public class PatchRepositoriesConfigurator {

	private final DirectoryService directoryService;

	public PatchRepositoriesConfigurator(DirectoryService directoryService) {
		this.directoryService = checkNotNull(directoryService);
	}

	@Bean
	public List<PatchFileRepository> getRepositories() {
		List<PatchFileRepository> list = newArrayList();
		list.add(new FilesStoreRepository(new File(directoryService.getWebappRootDirectory(), "WEB-INF/patches/"), null));
		File ext = new File(directoryService.getWebappRootDirectory(), "WEB-INF/patches-ext/");
		if (ext.exists()) {
			list(ext.listFiles()).stream().filter(File::isDirectory).forEach((d) -> list.add(new FilesStoreRepository(d, d.getName())));
		}
		return list;
	}

}
