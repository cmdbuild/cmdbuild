/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp.log;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.PreDestroy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.cmdbuild.common.logging.LogbackConfigurationStore;
import static org.cmdbuild.webapp.log.LogbackConfigurator.LOGBACK_FILE_NAME;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.DirectoryService;

/**
 *
 */
@Component
public class LogbackConfigurationStoreImpl implements LogbackConfigurationStore {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DirectoryService configDirectoryService;

	private final LogbackConfigurator logbackConfigurator = LogbackConfigurator.getInstance();
	private final AtomicBoolean ignoreNextChange = new AtomicBoolean(false);
	private final boolean hasFilesystem;
	private FileAlterationMonitor monitor;

	public LogbackConfigurationStoreImpl(DirectoryService configDirectoryService) {
		this.configDirectoryService = checkNotNull(configDirectoryService);
		hasFilesystem = configDirectoryService.hasFilesystem();
		if (hasFilesystem) {
			startMonitor();
		}
	}

	@PreDestroy
	public void cleanup() {
		if (hasFilesystem) {
			stopMonitor();
		}
	}

	private synchronized void startMonitor() {
		try {
			monitor = new FileAlterationMonitor();
			FileAlterationObserver observer = new FileAlterationObserver(configDirectoryService.getConfigDirectory(), FileFilterUtils.nameFileFilter(LOGBACK_FILE_NAME));
			observer.addListener(new FileAlterationListenerAdaptor() {
				@Override
				public void onFileChange(File file) {
					if (!ignoreNextChange.getAndSet(false)) {
						logbackConfigurator.configureLogbackSafe(file);
					}
				}

			});
			monitor.addObserver(observer);
			monitor.start();
		} catch (Exception ex) {
			logger.error("error starting config file monitor", ex);
		}
	}

	private synchronized void stopMonitor() {
		try {
			monitor.stop();
			monitor = null;
		} catch (Exception ex) {
			logger.error("error stopping config file monitor", ex);
		}
	}

	private File getFile() {
		checkArgument(hasFilesystem, "running in no-filesystem mode, logback configuration is not available");
		return new File(configDirectoryService.getConfigDirectory(), LOGBACK_FILE_NAME);
	}

	@Override
	public String getLogbackXmlConfiguration() {
		try {
			return FileUtils.readFileToString(getFile());
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void setLogbackXmlConfiguration(String xmlConfiguration) {
		try {
			checkArgument(getFile().canWrite(), "cannot write logback config file = %s", getFile().getAbsolutePath());
			ignoreNextChange.set(true);//tell monitor to ignore next detected change
			String backupConfig = getLogbackXmlConfiguration();
			try {
				FileUtils.writeStringToFile(getFile(), xmlConfiguration);
				logbackConfigurator.configureLogback(getFile());
			} catch (Exception ex) {
				logger.error("error configuring logback, recovering previous config", ex);
				try {
					FileUtils.writeStringToFile(getFile(), backupConfig);
				} catch (Exception exx) {
				}
				logbackConfigurator.configureLogback(getFile());
				throw ex;
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
