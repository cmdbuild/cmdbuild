/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp.log;

import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.ext.spring.LogbackConfigurer;
import com.google.common.eventbus.EventBus;
import java.io.File;
import java.io.FileNotFoundException;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this is a service used to configure logback loggin system. It's implemented
 * as a java static singleton and not as a spring singleton, so it can be used
 * to set logging config before spring starts.
 */
public class LogbackConfigurator {

	public final static String LOGBACK_FILE_NAME = "logback.xml";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final static LogbackConfigurator INSTANCE = new LogbackConfigurator();

	private final EventBus eventBus = new EventBus();

	private LogbackConfigurator() {
	}

	/**
	 * return service instance
	 *
	 * @return
	 */
	public static LogbackConfigurator getInstance() {
		return INSTANCE;
	}

	/**
	 * return event bus to handle {@link LogbackConfigurationReloadedEvent}
	 * events
	 *
	 * @return
	 */
	public EventBus getEventBus() {
		return eventBus;
	}

	/**
	 * configure system logback from supplied file
	 *
	 * @param logbackConfigXml
	 * @throws FileNotFoundException
	 * @throws JoranException
	 */
	public synchronized void configureLogback(File logbackConfigXml) throws FileNotFoundException, JoranException {
		logger.info("configure logback");
		LogbackConfigurer.initLogging((logbackConfigXml).getAbsolutePath());
		eventBus.post(LogbackConfigurationReloadedEvent.INSTANCE);
	}

	/**
	 * configure system logback from supplied file. Never throws exceptions,
	 * only eat and log them.
	 *
	 * @param logbackConfigXml
	 */
	public synchronized void configureLogbackSafe(File logbackConfigXml) {
		try {
			configureLogback(logbackConfigXml);
		} catch (Exception ex) {
			logger.error(marker(), "error configuring logger", ex);
		}
	}

	/**
	 * this event is generated whenever the logback configuration is reloaded.
	 * It may be used for services that need to register custom
	 * loggers/appenders <i>after</i> the static configuration has been loaded
	 * from file.
	 */
	public static enum LogbackConfigurationReloadedEvent {
		INSTANCE
	}

}
