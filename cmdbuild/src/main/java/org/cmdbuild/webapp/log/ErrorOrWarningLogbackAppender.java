/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.webapp.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.boolex.OnMarkerEvaluator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.filter.EvaluatorFilter;
import ch.qos.logback.core.spi.FilterReply;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.Subscribe;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.cmdbuild.common.error.ErrorAndWarningCollectorService;
import org.cmdbuild.common.error.ErrorOrWarningEvent;
import org.cmdbuild.common.error.ErrorOrWarningEventImpl;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.webapp.log.LogbackConfigurator.LogbackConfigurationReloadedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * intercept logger messages marker with {@link #marker() } and attach them to
 * {@link ErrorAndWarningCollectorService} (to be possibly tracked or returner
 * to the user).
 */
@Component
public class ErrorOrWarningLogbackAppender {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ErrorAndWarningCollectorService errorAndWarningCollectorService;
	private final MyEventListener listener = new MyEventListener();

	public ErrorOrWarningLogbackAppender(ErrorAndWarningCollectorService errorAndWarningCollectorService) {
		this.errorAndWarningCollectorService = checkNotNull(errorAndWarningCollectorService);
	}

	@PostConstruct
	public void init() {
		logger.info("init");
		LogbackConfigurator.getInstance().getEventBus().register(listener);
		registerAppender();
	}

	@PreDestroy
	public void cleanup() {
		LogbackConfigurator.getInstance().getEventBus().unregister(listener);
	}

	private class MyEventListener {

		@Subscribe
		public void handleLogbackConfigurationReloadedEvent(LogbackConfigurationReloadedEvent event) {
			registerAppender();
		}
	}

	private void registerAppender() {
		logger.info("register custom logback appender for log event collecting");
		try {
			LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

			OnMarkerEvaluator evaluator = new OnMarkerEvaluator();
			evaluator.setContext(loggerContext);
			evaluator.addMarker(marker().getName());

			EvaluatorFilter<ILoggingEvent> filter = new EvaluatorFilter<>();
			filter.setContext(loggerContext);
			filter.setEvaluator(evaluator);
			filter.setOnMatch(FilterReply.ACCEPT);
			filter.setOnMismatch(FilterReply.DENY);

			AppenderBase<ILoggingEvent> customAppender = new AppenderBase<ILoggingEvent>() {
				@Override
				protected void append(ILoggingEvent eventObject) {
					ErrorOrWarningEvent.Level level;
					if (eventObject.getLevel().isGreaterOrEqual(Level.ERROR)) {
						level = ErrorOrWarningEvent.Level.ERROR;
					} else if (eventObject.getLevel().isGreaterOrEqual(Level.WARN)) {
						level = ErrorOrWarningEvent.Level.WARNING;
					} else {
						level = ErrorOrWarningEvent.Level.INFO;
					}
					errorAndWarningCollectorService.getCurrentRequestEventCollector().addEvent(new ErrorOrWarningEventImpl(
							eventObject.getFormattedMessage(),
							level,
							getException(eventObject.getThrowableProxy())));
				}

			};
			customAppender.setContext(loggerContext);
			customAppender.setName("ErrorOrWarningEventCollector");
			customAppender.addFilter(filter);

			evaluator.start();
			filter.start();
			customAppender.start();

			loggerContext.getLogger(Logger.ROOT_LOGGER_NAME).addAppender(customAppender);
		} catch (Exception ex) {
			logger.error("error registering custom appender", ex);
		}
	}

	private static @Nullable
	Exception getException(@Nullable IThrowableProxy proxy) {
		if (proxy == null || !(proxy instanceof ThrowableProxy)) {
			return null;
		} else {
			if (((ThrowableProxy) proxy).getThrowable() instanceof Exception) {
				return (Exception) ((ThrowableProxy) proxy).getThrowable();
			} else {
				return new RuntimeException(((ThrowableProxy) proxy).getThrowable());
			}
		}
	}

}
