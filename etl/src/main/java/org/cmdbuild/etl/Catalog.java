package org.cmdbuild.etl;

import org.cmdbuild.etl.Type;

public interface Catalog {

	Iterable<Type> getTypes();

	<T extends Type> T getType(String name, Class<T> type);

}
