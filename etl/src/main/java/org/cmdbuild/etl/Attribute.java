package org.cmdbuild.etl;

public interface Attribute {

	String getName();

	boolean isKey();

}
