package org.cmdbuild.etl.sql;

public interface AttributeMapping {

	String from();

	String to();

}
