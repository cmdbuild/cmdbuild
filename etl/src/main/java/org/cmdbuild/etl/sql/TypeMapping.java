package org.cmdbuild.etl.sql;

import org.cmdbuild.etl.ClassType;

public interface TypeMapping {

	ClassType getType();

	Iterable<AttributeMapping> getAttributeMappings();

}
