package org.cmdbuild.etl.sql;

public interface TableOrViewMapping {

	String getName();

	Iterable<TypeMapping> getTypeMappings();

}
