package org.cmdbuild.etl;

public interface Type {

	void accept(TypeVisitor visitor);

	String getName();

	Iterable<Attribute> getAttributes();

	Attribute getAttribute(String name);

}
