package org.cmdbuild.etl;

public interface TypeVisitor {

	void visit(ClassType type);

}
