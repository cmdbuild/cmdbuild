package org.cmdbuild.service.rest.v3.cxf.filter;

import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;

import org.cmdbuild.logic.data.access.filter.model.All;
import org.cmdbuild.logic.data.access.filter.model.Attr;
import org.cmdbuild.logic.data.access.filter.model.CompositeElement;
import org.cmdbuild.logic.data.access.filter.model.Element;
import org.cmdbuild.logic.data.access.filter.model.ElementVisitor;
import org.cmdbuild.logic.data.access.filter.model.Not;
import org.cmdbuild.logic.data.access.filter.model.OneOf;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

public abstract class ElementPredicate<T> extends ForwardingPredicate<T> implements ElementVisitor {

	private Predicate<T> predicate;

	public ElementPredicate(final Element element) {
		element.accept(this);
	}

	@Override
	protected Predicate<T> delegate() {
		return predicate;
	}

	@Override
	public void visit(final All element) {
		predicate = Predicates.<T>and(subPredicatesOf(element));
	}

	@Override
	public void visit(final Attr element) {
		predicate = predicateOf(element);
	}

	@Override
	public void visit(final Not element) {
		predicate = not(predicateOf(element));
	}

	@Override
	public void visit(final OneOf element) {
		predicate = Predicates.<T>or(subPredicatesOf(element));
	}

	private Collection<Predicate<? super T>> subPredicatesOf(final CompositeElement composite) {
		final Collection<Predicate<? super T>> elements = newArrayList();
		for (final Element element : composite.getElements()) {
			elements.add(predicateOf(element));
		}
		return elements;
	}

	protected abstract Predicate<T> predicateOf(Attr element);

	protected abstract Predicate<T> predicateOf(Element element);

}
