package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import org.cmdbuild.common.utils.PagedElements;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_BEGINDATE;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptionsImpl;
import org.cmdbuild.dao.history.CardHistoryService;
import static org.cmdbuild.data.filter.SorterElement.SorterElementDirection.DESC;
import org.springframework.stereotype.Component;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.utils.date.DateUtils.toIsoDateTime;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.service.rest.v3.cxf.serialization.CardSerializationHelper;
import org.cmdbuild.service.rest.v3.cxf.serialization.ToProcessStatus;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.PROCESS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.PROCESS_INSTANCE_ID;
import org.cmdbuild.service.rest.v3.model.ProcessStatus;
import org.cmdbuild.workflow.core.LookupHelper;

@Component("v3_processInstances_history")
@Path("processes/{" + PROCESS_ID + "}/instances/{" + PROCESS_INSTANCE_ID + "}/history")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class ProcessInstancesHistoryWs {

	private final CardHistoryService service;
	private final CardSerializationHelper helper;
	private final LookupHelper lookupHelper;

	public ProcessInstancesHistoryWs(CardHistoryService service, CardSerializationHelper helper, LookupHelper lookupHelper) {
		this.service = checkNotNull(service);
		this.helper = checkNotNull(helper);
		this.lookupHelper = checkNotNull(lookupHelper);
	}

	@GET
	@Path("")
	public Object getHistory(@PathParam(PROCESS_ID) String classId, @PathParam(PROCESS_INSTANCE_ID) Long cardId, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {
		DaoQueryOptionsImpl query = DaoQueryOptionsImpl.builder()
				.withPaging(offset, limit)
				.orderBy(ATTR_BEGINDATE, DESC)
				.build();
		PagedElements<Card> history = service.getHistory(classId, cardId, query);

		return response(history.stream().map((record) -> map(
				"_type", record.getType().getName(),
				"_id", record.getId(),
				"_endDate", toIsoDateTime(record.getEndDate()),
				"_beginDate", toIsoDateTime(record.getBeginDate()),
				"_user", record.getUser(),
				"_status", record.getCardStatus().name())), history.totalSize());
	}

	@GET
	@Path("{recordId}/")
	public Object getHistoryRecord(@PathParam(PROCESS_ID) String classId, @PathParam(PROCESS_INSTANCE_ID) Long id, @PathParam("recordId") Long recordId) {
		Card record = service.getHistoryRecord(classId, recordId);
		ProcessStatus processStatus = lookupHelper.getFlowStatusLookup(record).transform(ToProcessStatus::toProcessStatus).orNull();
		checkArgument(equal(record.getCurrentId(), id));
		return response(helper.serializeCard(record).with(
				"_endDate", toIsoDateTime(record.getEndDate()),
				"_status", record.getCardStatus().name(),
				"status", processStatus != null ? processStatus.getId() : null,
				"_status_description", processStatus == null ? null : processStatus.getDescription()));
	}

}
