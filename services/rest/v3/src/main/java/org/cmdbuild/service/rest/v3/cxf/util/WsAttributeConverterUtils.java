package org.cmdbuild.service.rest.v3.cxf.util;

import java.util.Optional;
import org.cmdbuild.dao.beans.IdAndDescription;
import org.cmdbuild.utils.date.DateUtils;

import static org.cmdbuild.dao.utils.AttributeConversionUtils.rawToSystem;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

public class WsAttributeConverterUtils {

	public static Object toClient(CardAttributeType<?> attributeType, Object value) {
		switch (attributeType.getName()) {
			case DATE:
				return DateUtils.toIsoDate(value);
			case TIME:
				return DateUtils.toIsoTime(value);
			case TIMESTAMP:
				return DateUtils.toIsoDateTime(value);
			case REFERENCE:
			case FOREIGNKEY:
			case LOOKUP:
				return Optional.ofNullable((IdAndDescription) rawToSystem(attributeType, value)).map(IdAndDescription::getId).orElse(null);
			default:
				return value;
		}
	}

}
