package org.cmdbuild.service.rest.v3.cxf;

import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.gis.GisService;
import org.cmdbuild.gis.GisValue;
import org.cmdbuild.navtree.NavTreeNode;
import org.springframework.stereotype.Component;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.AREA;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ATTRIBUTE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.cmdbuild.service.rest.v3.cxf.util.WsSerializationUtils;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component("v3_geovalues")
@Path("classes/_ANY/cards/_ANY/geovalues/")
@Produces(APPLICATION_JSON)
public class GeoValueWs {

	private final GisService gis;

	public GeoValueWs(GisService service) {
		this.gis = checkNotNull(service);
	}

	@GET
	@Path(EMPTY)
	public Object query(@QueryParam(ATTRIBUTE) Set<Long> attrs, @QueryParam(AREA) String area, @QueryParam("attach_nav_tree") boolean attachNavTree) {
		if (attachNavTree) {
			Pair<List<GisValue>, List<NavTreeNode>> geoValuesAndNavTree = gis.getGeoValuesAndNavTree(attrs, area);
			return geometriesToResponse(geoValuesAndNavTree.getLeft()).accept((m) -> {
				FluentMap<String, Object> meta = (FluentMap<String, Object>) m.get("meta");
				meta.put("nav_tree_items", geoValuesAndNavTree.getRight().stream().map((n) -> map(
						"_id", n.getCardId(),
						"type", n.getClassId(),
						"description", n.getDescription(),
						"parentid", n.getParentCardId(),
						"parenttype", n.getParentClassId()
				)).collect(toList()));
			});
		} else {
			return geometriesToResponse(gis.getGeoValues(attrs, area));
		}
	}

	private static FluentMap<String, Object> geometriesToResponse(List<GisValue> geometries) {
		return response(geometries.stream().map(WsSerializationUtils::serializeGeometry));
	}
}
