/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WsConfig {

	@Bean("v3_jacksonJaxbJsonProvider")
	public JacksonJaxbJsonProvider jacksonJaxbJsonProvider() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		return new JacksonJaxbJsonProvider(mapper, JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
//		return new JacksonJaxbJsonProvider(null, JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
//		return new JacksonJaxbJsonProvider();
		JacksonJaxbJsonProvider jacksonJaxbJsonProvider = new JacksonJaxbJsonProvider(){
			{
				setMapper(mapper);
				setAnnotationsToUse(JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
			}
		};
//		jacksonJaxbJsonProvider.setMapper(mapper);
		return  jacksonJaxbJsonProvider;
	}
}
