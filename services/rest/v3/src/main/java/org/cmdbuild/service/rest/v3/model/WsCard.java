package org.cmdbuild.service.rest.v3.model;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.VALUES;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.cmdbuild.service.rest.v3.model.adapter.CardAdapter;

@XmlRootElement
@XmlJavaTypeAdapter(CardAdapter.class)
public class WsCard extends AbstractCardModel {

	private Values values;
	private String forDomain;
	private boolean forDomainAvailable;

	public void setForDomain(String forDomain) {
		this.forDomain = forDomain;
	}

	public void setForDomainAvailable(boolean forDomainAvailable) {
		this.forDomainAvailable = forDomainAvailable;
	}

	@XmlTransient
	public String getForDomain() {
		return forDomain;
	}

	@XmlTransient
	public boolean getForDomainAvailable() {
		return forDomainAvailable;
	}

	@XmlElement(name = VALUES)
	public Values getValues() {
		return values;
	}

	void setValues(final Values values) {
		this.values = values;
	}

	@Override
	protected boolean doEquals(final Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WsCard)) {
			return false;
		}

		final WsCard other = WsCard.class.cast(obj);
		return new EqualsBuilder() //
				.append(this.getType(), other.getType()) //
				.append(this.getId(), other.getId()) //
				.append(this.values, other.values) //
				.isEquals();
	}

	@Override
	protected int doHashCode() {
		return new HashCodeBuilder() //
				.append(getType()) //
				.append(getId()) //
				.append(values) //
				.toHashCode();
	}

}
