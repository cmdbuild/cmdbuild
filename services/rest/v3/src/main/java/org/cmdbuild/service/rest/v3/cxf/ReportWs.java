package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Predicates.alwaysTrue;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import javax.activation.DataHandler;

import org.cmdbuild.logic.data.access.filter.json.JsonParser;
import org.cmdbuild.logic.data.access.filter.model.Element;
import org.cmdbuild.logic.data.access.filter.model.Filter;
import org.cmdbuild.logic.data.access.filter.model.Parser;
import org.cmdbuild.service.rest.v3.cxf.filter.ReportElementPredicate;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import static com.google.common.collect.MoreCollectors.onlyElement;
import static java.util.Collections.emptyMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import org.apache.commons.io.FilenameUtils;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.cmdbuild.common.utils.PagedElements;
import static org.cmdbuild.common.utils.PagedElements.isPaged;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.report.ReportConst.ReportExtension;
import org.cmdbuild.report.ReportData;
import org.cmdbuild.report.ReportInfo;
import org.cmdbuild.report.ReportInfoImpl;
import org.cmdbuild.report.ReportService;
import static org.cmdbuild.report.ReportUtils.reportExtFromString;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.EXTENSION;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.PARAMETERS;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.REPORT_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DETAILED;
import org.cmdbuild.translation.ObjectTranslationService;
import org.cmdbuild.utils.io.CmdbuildIoUtils;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.readToString;
import static org.cmdbuild.utils.io.CmdbuildZipUtils.unzipDataAsMap;
import static org.cmdbuild.utils.json.CmJsonUtils.MAP_OF_OBJECTS;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.security.access.prepost.PreAuthorize;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;
import org.cmdbuild.service.rest.v3.cxf.serialization.AttributeTypeConversionService;

@Component("v3_reports")
@Path("reports/")
@Produces(APPLICATION_JSON)
public class ReportWs {

	private final ReportService reportService;
	private final AttributeTypeConversionService toAttributeDetail;
	private final ObjectTranslationService translation;

	public ReportWs(ReportService reportService, AttributeTypeConversionService toAttributeDetail, ObjectTranslationService translation) {
		this.reportService = checkNotNull(reportService);
		this.toAttributeDetail = checkNotNull(toAttributeDetail);
		this.translation = checkNotNull(translation);
	}

	@GET
	@Path(EMPTY)
	public Object readAll(@QueryParam(FILTER) String filter, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset, @QueryParam(DETAILED) Boolean detailed) {
		Predicate<org.cmdbuild.report.ReportInfo> predicate;
		if (isNotBlank(filter)) {
			Parser parser = new JsonParser(filter);
			Filter filterModel = parser.parse();
			Optional<Element> element = filterModel.attribute();
			if (element.isPresent()) {
				predicate = new ReportElementPredicate(element.get());
			} else {
				predicate = alwaysTrue();
			}
		} else {
			predicate = alwaysTrue();
		}

		List list = reportService.getAll().stream().filter(predicate).map(defaultIfNull(detailed, false) ? this::serializeDetailedReport : this::serializeMinimalReport).collect(toList());
		if (isPaged(offset, limit)) {
			PagedElements paged = paged(list, offset, limit);
			return response(paged);
		} else {
			return response(list);
		}
	}

	@GET
	@Path("{" + REPORT_ID + "}/")
	public Object read(@PathParam(REPORT_ID) String reportId) {
		ReportInfo report = reportService.getByIdOrCode(reportId);
		return response(serializeDetailedReport(report));
	}

	@GET
	@Path("{" + REPORT_ID + "}/attributes/")
	public Object readAllAttributes(@PathParam(REPORT_ID) String reportId, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {
		Iterable<Attribute> elements = reportService.getParamsById(reportService.getByIdOrCode(reportId).getId());
		return response(paged(elements, toAttributeDetail::serializeAttributeType, offset, limit));
	}

	@GET
	@Path("{" + REPORT_ID + "}/{file: [^/]+}")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler download(@PathParam(REPORT_ID) String reportId, @QueryParam(EXTENSION) String extension, @QueryParam(PARAMETERS) String parametersStr) {
		Map<String, Object> parameters = isBlank(parametersStr) ? emptyMap() : fromJson(parametersStr, MAP_OF_OBJECTS);
		return reportService.executeReportAndDownload(reportId, reportExtFromString(extension), parameters);
	}

	@POST
	@Path("")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object createReport(List<Attachment> attachments) {
		checkNotNull(attachments);
		WsReportData data = getData(attachments);
		Map<String, byte[]> files = getFiles(attachments);
		ReportData reportData = reportService.createReport(data.toReportInfo().build(), files);
		return response(serializeDetailedReport(reportData));
	}

	@PUT
	@Path("{" + REPORT_ID + "}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Object updateReport(@PathParam(REPORT_ID) String reportId, List<Attachment> attachments) {
		WsReportData wsData = getData(attachments);
		Map<String, byte[]> files = getFiles(attachments);
		ReportInfo info = wsData.toReportInfo().withId(reportService.getByIdOrCode(reportId).getId()).build();
		ReportData reportData;
		if (files.isEmpty()) {
			reportData = reportService.updateReportInfo(info);
		} else {
			reportData = reportService.updateReport(info, files);
		}
		return response(serializeDetailedReport(reportData));
	}

	@PUT
	@Path("{" + REPORT_ID + "}/template")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object updateReportTemplate(@PathParam(REPORT_ID) String reportId, List<Attachment> attachments) {
		checkNotNull(attachments);
		Map<String, byte[]> files = getFiles(attachments);
		ReportData reportData = reportService.updateReportTemplate(reportService.getByIdOrCode(reportId).getId(), files);
		return response(serializeDetailedReport(reportData));
	}

	@GET
	@Path("{" + REPORT_ID + "}/template{x:/?[^/]*}")
	@Produces(APPLICATION_OCTET_STREAM)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public DataHandler downloadTemplateFiles(@PathParam(REPORT_ID) Long reportId) {
		return reportService.executeReportAndDownload(reportId.toString(), ReportExtension.ZIP);
	}

	@DELETE
	@Path("{" + REPORT_ID + "}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object deleteReport(@PathParam(REPORT_ID) Long reportId) {
		reportService.deleteReport(reportId);
		return success();
	}

	private FluentMap<String, Object> serializeMinimalReport(ReportInfo report) {
		return map(
				"_id", report.getId(),
				"code", report.getCode(),
				"description", report.getDescription(),
				"_description_translation", translation.translateReportDesciption(report.getCode(), report.getDescription()),
				"active", report.isActive()
		);
	}

	private Object serializeDetailedReport(ReportInfo report) {
		ReportData reportData = report instanceof ReportData ? ((ReportData) report) : reportService.getReportData(report.getId());
		return serializeMinimalReport(report).with(
				"title", report.getCode(),
				"query", reportData.getQuery()
		//				"groups", reportData.getGroups().stream().sorted(Ordering.natural()).collect(toList())
		//				"type", report.getType()
		);
	}

	private static WsReportData getData(List<Attachment> attachments) {
		return fromJson(readToString(attachments.stream().filter((a) -> a.getContentType().isCompatible(MediaType.APPLICATION_JSON_TYPE)).collect(onlyElement()).getDataHandler()), WsReportData.class);
	}

	private static Map<String, byte[]> getFiles(List<Attachment> attachments) {
		return unpackZipFiles(attachments.stream()
				.filter((a) -> !a.getContentType().isCompatible(MediaType.APPLICATION_JSON_TYPE))
				.map(Attachment::getDataHandler).collect(toMap(DataHandler::getName, CmdbuildIoUtils::toByteArray)));
	}

	private static Map<String, byte[]> unpackZipFiles(Map<String, byte[]> files) {
		Map<String, byte[]> map = map();
		files.forEach((name, data) -> {
			if (FilenameUtils.getExtension(name).equalsIgnoreCase("zip")) {
				map.putAll(unzipDataAsMap(data));
			} else {
				map.put(name, data);
			}
		});
		return map;
	}

	public static class WsReportData {

		private final String description, code;
		private final boolean isActive;

		public WsReportData(@JsonProperty("description") String description,
				@JsonProperty("code") String code,
				@JsonProperty("active") Boolean isActive) {
			this.description = description;
			this.code = checkNotBlank(code, "missing code param");
			this.isActive = isActive;
		}

		public ReportInfoImpl.ReportInfoImplBuilder toReportInfo() {
			return ReportInfoImpl.builder()
					.withActive(isActive)
					.withCode(code)
					.withDescription(description);
		}
	}
}
