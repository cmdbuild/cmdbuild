package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import org.cmdbuild.auth.session.SessionService;
import org.springframework.stereotype.Component;
import org.cmdbuild.menu.MenuService;
import org.cmdbuild.menu.MenuTreeNode;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.cmdbuild.service.rest.v3.cxf.serialization.MenuSerializationHelper;

@Component("v3_sessionMenu")
@Path("sessions/{sessionId}/menu")
@Produces(APPLICATION_JSON)
public class SessionMenuWs extends SessionWsCommons {

	private final MenuService menuService;
	private final MenuSerializationHelper helper;

	public SessionMenuWs(MenuService menuService, MenuSerializationHelper helper, SessionService sessionService) {
		super(sessionService);
		this.menuService = checkNotNull(menuService);
		this.helper = checkNotNull(helper);
	}

	@GET
	@Path("")
	public Object read(@PathParam("sessionId") String sessionId) {
		checkIsCurrent(sessionId);
		MenuTreeNode menu = menuService.getMenuForCurrentUser();
		return response(helper.serializeMenu(menu));
	}

}
