/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.service.rest.v3.cxf.serialization;

import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

public interface AttributeConverter {

		Object convert(CardAttributeType<?> attributeType, Object value);
}
