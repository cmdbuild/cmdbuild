package org.cmdbuild.service.rest.v3.cxf;

import com.google.common.base.Function;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.toImmutableList;
import com.google.common.collect.ImmutableMap;
import static com.google.common.collect.Lists.transform;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import org.cmdbuild.service.rest.v3.model.WsCard;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.cardfilter.CardFilterService;
import org.cmdbuild.classe.UserCardAccess;
import org.cmdbuild.classe.UserCardService;
import static org.cmdbuild.common.utils.PagedElements.hasLimit;
import static org.cmdbuild.common.utils.PagedElements.isPaged;
import org.cmdbuild.dao.core.q3.DaoService;
import org.springframework.stereotype.Component;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CARD_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.POSITION_OF;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SORT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.utils.CmdbFilterUtils;
import org.cmdbuild.data.filter.utils.CmdbSorterUtils;
import org.cmdbuild.dao.beans.Card;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_BEGINDATE;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_USER;
import static org.cmdbuild.dao.core.q3.WhereOperator.EQ;
import org.cmdbuild.data.filter.CmdbSorter;
import org.cmdbuild.data.filter.beans.CmdbSorterImpl;
import org.cmdbuild.data.filter.beans.SorterElementImpl;
import org.cmdbuild.service.rest.v3.cxf.serialization.CardSerializationHelper;
import static org.cmdbuild.service.rest.v3.cxf.serialization.CardSerializationHelper.ExtendedCardOptions.INCLUDE_MODEL;
import static org.cmdbuild.service.rest.v3.cxf.serialization.CardSerializationHelper.ExtendedCardOptions.NONE;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNotNullAndGtZero;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDTENANT;

@Component("v3_cards")
@Path("{a:classes}/{" + CLASS_ID + "}/{b:cards}/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class CardWs {

	private final UserCardService cardService;
	private final DaoService dao;
	private final CardFilterService filterService;
	private final CardSerializationHelper helper;

	public CardWs(UserCardService cardService, DaoService dao, CardFilterService filterService, CardSerializationHelper helper) {
		this.cardService = checkNotNull(cardService);
		this.dao = checkNotNull(dao);
		this.filterService = checkNotNull(filterService);
		this.helper = checkNotNull(helper);
	}

	@GET
	@Path("{" + CARD_ID + "}/")
	public Object readOne(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @QueryParam("includeModel") Boolean includeModel) {
		UserCardAccess cardAccess = cardService.getUserCardAccess(classId);
		Card card = cardAccess.addCardAccessPermissionsFromSubfilterMark(dao.selectAll().from(classId)
				.accept(cardAccess.addSubsetFilterMarkersToQueryVisitor())
				.where(cardAccess.getWholeClassFilter())
				.where(ATTR_ID, EQ, checkNotNull(cardId))
				.getCard());
		checkArgument(card.getType().hasServiceReadPermission(), "user not authorized to access card %s.%s", classId, cardId);
		return response(helper.serializeCard(card, defaultIfNull(includeModel, false) ? INCLUDE_MODEL : NONE));
	}

	@GET
	@Path(EMPTY)
	public Object readMany(
			@PathParam(CLASS_ID) String classId,
			@QueryParam(FILTER) String filterStr,
			@QueryParam(SORT) String sort,
			@QueryParam(LIMIT) Long limit,
			@QueryParam(START) Long offset,
			@QueryParam(POSITION_OF) Long positionOfCard,
			@QueryParam("positionOf_goToPage") Boolean goToPage,
			@QueryParam("forDomain_name") String forDomainName,
			@QueryParam("forDomain_direction") String forDomainDirection,
			@QueryParam("forDomain_originId") Long forDomainOriginId) {

		CmdbFilter filter = CmdbFilterUtils.fromNullableJson(getFilterOrNull(filterStr));//TODO map filter attribute names
		CmdbSorter sorter = mapSorterAttributeNames(CmdbSorterUtils.fromNullableJson(sort));

		UserCardAccess cardAccess = cardService.getUserCardAccess(classId);
		CmdbFilter cardAccessFilter = cardAccess.getWholeClassFilter();

		filter = filter.and(cardAccessFilter);

		FluentMap meta = map();

		if (isNotNullAndGtZero(positionOfCard)) {
			checkArgument(hasLimit(limit), "must set valid 'limit' along with 'positionOf'");
			offset = firstNonNull(offset, 0l);
			Long rowNumber = dao.selectRowNumber().where(ATTR_ID, EQ, positionOfCard).then()
					.from(classId)
					.orderBy(sorter)
					.where(filter)
					.build().getRowNumberOrNull();
			Map positionMeta;
			if (rowNumber == null) {
				positionMeta = map("found", false);
			} else {
				long positionInPage = rowNumber % limit;
				long pageOffset = rowNumber - positionInPage;
				if (defaultIfNull(goToPage, true)) {
					offset = pageOffset;
				}
				positionMeta = map("found", true,
						"positionInPage", positionInPage,
						"positionInTable", rowNumber,
						"pageOffset", pageOffset);
			}
			meta.put("positions", map(positionOfCard, positionMeta), START, offset, LIMIT, limit);
		}

		List<Card> cards = dao.selectAll()
				.from(classId)
				.orderBy(sorter)
				.where(filter)
				.accept(cardAccess.addSubsetFilterMarkersToQueryVisitor())
				.paginate(offset, limit)
				.getCards();

		long total;
		if (isPaged(offset, limit)) {
			total = dao.selectCount()
					.from(classId)
					.where(filter)
					.getCount();
		} else {
			total = cards.size();
		}

		cards = list(transform(cards, cardAccess::addCardAccessPermissionsFromSubfilterMark));

		return response(cards.stream().map(helper::serializeCard).collect(toList()), total, meta);
	}

	@POST
	@Path(EMPTY)
	public Object create(@PathParam(CLASS_ID) String classId, WsCard data) {
		return response(helper.serializeCard(cardService.createCard(classId, mapValues(data.getValues()))));
	}

	@PUT
	@Path("{" + CARD_ID + "}/")
	public Object update(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, WsCard data) {
		return response(helper.serializeCard(cardService.updateCard(classId, cardId, mapValues(data.getValues()))));
	}

	@DELETE
	@Path("{" + CARD_ID + "}/")
	public Object delete(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId) {
		cardService.deleteCard(classId, cardId);
		return success();
	}

	private Map<String, Object> mapValues(Map<String, Object> values) {
		return values.entrySet().stream().collect(toMap(e -> SYSTEM_ATTR_NAME_MAPPING.getOrDefault(e.getKey(), e.getKey()), Entry::getValue));
	}

	private final static Map<String, String> SYSTEM_ATTR_NAME_MAPPING = ImmutableMap.of("_id", ATTR_ID,
			"_type", ATTR_IDCLASS,
			"_user", ATTR_USER,
			"_tenant", ATTR_IDTENANT,
			"_beginDate", ATTR_BEGINDATE
	);

	public static CmdbSorter mapSorterAttributeNames(CmdbSorter sorter) {//TODO use this everywhere
		return new CmdbSorterImpl(sorter.getElements().stream().map((e) -> new SorterElementImpl(firstNonNull(SYSTEM_ATTR_NAME_MAPPING.get(e.getProperty()), e.getProperty()), e.getDirection())).collect(toImmutableList()));
	}

	@Nullable
	private String getFilterOrNull(@Nullable String filter) {
		return getFilterOrNull(filter, (id) -> filterService.getById(id).getConfiguration());
	}

	@Nullable
	public static String getFilterOrNull(@Nullable String filter, Function<Long, String> filterRepo) {
		if (isBlank(filter)) {
			return null;
		} else {
			JsonPrimitive filterId = new JsonParser().parse(filter).getAsJsonObject().getAsJsonPrimitive("_id");
			if (filterId != null && !filterId.isJsonNull()) {
				return filterRepo.apply(filterId.getAsLong());
			} else {
				return filter;
			}
		}
	}

}
