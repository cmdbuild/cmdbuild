package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ComparisonChain;
import java.util.Collections;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.cmdbuild.dao.beans.CMRelation;
import static org.cmdbuild.dao.beans.CardIdAndClassNameImpl.card;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.*;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.entrytype.Domain;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component("v3_cardRelations")
@Path("{a:classes|processes}/{" + CLASS_ID + "}/{b:cards|instances}/{" + CARD_ID + "}/relations/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class CardRelationWs {


	private final DaoService dao;

	public CardRelationWs(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@GET
	@Path(EMPTY)
	public Object read(@PathParam(CLASS_ID) String className, @PathParam(CARD_ID) Long cardId) {
		List<CMRelation> relations = list(dao.getRelationsInfoForCard(card(className, cardId)));

		Collections.sort(relations, (r1, r2) -> {
			Domain d1 = r1.getDomainWithThisRelationDirection(),
					d2 = r2.getDomainWithThisRelationDirection();

			return ComparisonChain.start()
					.compare(d1.getIndexForSource(), d2.getIndexForSource())//TODO check this
					.compare(d1.getName(), d2.getName())
					.compare(r1.getTargetCard().getClassName(), r2.getTargetCard().getClassName())
					.result();

		});

		return response(relations.stream().map((r) -> map(
				"_type", r.getType().getName(),
				"_id", r.getId(),
				"_destinationType", r.getTargetCard().getClassName(),
				"_destinationId", r.getTargetCard().getId(),
				"_destinationCode", r.getTargetCode(),
				"_destinationDescription", r.getTargetDescription()
		)));
	}

}
