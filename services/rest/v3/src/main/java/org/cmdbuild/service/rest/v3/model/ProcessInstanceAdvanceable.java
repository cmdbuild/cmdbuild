package org.cmdbuild.service.rest.v3.model;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ACTIVITY;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ADVANCE;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.cmdbuild.service.rest.v3.model.adapter.ProcessInstanceAdvanceableAdapter;

@XmlRootElement
@XmlJavaTypeAdapter(ProcessInstanceAdvanceableAdapter.class)
public class ProcessInstanceAdvanceable extends ProcessInstance {

	private String activity;
	private boolean advance;
//	private Collection<Widget> widgets;

	ProcessInstanceAdvanceable() {
		// package visibility
	}

	@XmlAttribute(name = ACTIVITY)
	public String getActivity() {
		return activity;
	}

	void setActivity(final String activityId) {
		this.activity = activityId;
	}

	@XmlAttribute(name = ADVANCE)
	public boolean isAdvance() {
		return advance;
	}

	void setAdvance(final boolean advance) {
		this.advance = advance;
	}

}
