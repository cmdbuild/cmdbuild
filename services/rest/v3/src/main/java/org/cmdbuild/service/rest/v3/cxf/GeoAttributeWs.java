package org.cmdbuild.service.rest.v3.cxf;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.gson.Gson;
import static java.util.Collections.emptyMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.common.utils.PagedElements;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.cmdbuild.gis.GisService;
import org.springframework.stereotype.Component;
import org.cmdbuild.gis.GisAttributeImpl;
import org.cmdbuild.service.rest.v3.model.WsGeoAttribute;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import org.cmdbuild.gis.GisAttribute;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ATTRIBUTE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DETAILED;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import org.cmdbuild.easyupload.EasyuploadItem;
import org.cmdbuild.easyupload.EasyuploadService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;

@Component("v3_geoattributes")
@Path("classes/{" + CLASS_ID + "}/geoattributes/")
@Produces(APPLICATION_JSON)
public class GeoAttributeWs {

	private final GisService service;
	private final EasyuploadService easyuploadService;

	public GeoAttributeWs(GisService service, EasyuploadService easyuploadService) {
		this.service = checkNotNull(service);
		this.easyuploadService = checkNotNull(easyuploadService);
	}

	@GET
	@Path(EMPTY)
	public Object readAllAttributes(@PathParam(CLASS_ID) String classId, @QueryParam(START) Integer offset, @QueryParam(LIMIT) Integer limit, @QueryParam(DETAILED) boolean detailed, @QueryParam("visible") boolean visible) {
		List<GisAttribute> elements;
		if (visible) {
			elements = service.getVisibleGeoAttributesForClass(classId);
		} else {
			elements = service.getGeoAttributeByOwnerClass(classId);
		}
		PagedElements<GisAttribute> paged = paged(elements, offset, limit);
		return response(paged.stream().map(this::layerToWsGeoAttribute).collect(toList()), paged.totalSize());
	}

	@GET
	@Path("{" + ATTRIBUTE + "}/")
	public Object readAttribute(@PathParam(CLASS_ID) String classId, @PathParam(ATTRIBUTE) String attributeId) {
		GisAttribute layer = service.getLayerByClassAndName(classId, attributeId);
		return response(layerToWsGeoAttribute(layer));
	}

	@POST
	@Path(EMPTY)
	public Object create(@PathParam(CLASS_ID) String classId, WsGeoAttribute attributeData) {
		GisAttribute layer = toGisAttribute(classId, attributeData);
		layer = service.createGeoAttribute(layer);
		return response(layerToWsGeoAttribute(layer));
	}

	@PUT
	@Path("{attrId}/")
	public Object update(@PathParam(CLASS_ID) String classId, @PathParam("attrId") String attrId, WsGeoAttribute attributeData) {
		GisAttribute layer = toGisAttribute(classId, attributeData);
		checkArgument(equal(layer.getLayerName(), attrId));
		layer = service.updateGeoAttribute(layer);
		return response(layerToWsGeoAttribute(layer));
	}

	@DELETE
	@Path("{attrId}/")
	public Object delete(@PathParam(CLASS_ID) String classId, @PathParam("attrId") String attrId) {
		service.deleteGeoAttribute(classId, attrId);
		return success();
	}

	private WsGeoAttribute layerToWsGeoAttribute(GisAttribute layer) {
		WsGeoAttribute attribute = new WsGeoAttribute();
		attribute.setOwnerClass(layer.getOwnerClassId());
		attribute.setType("geometry");
		attribute.setSubtype(layer.getType());
		attribute.setDescription(layer.getDescription());
		attribute.setId(Long.toString(layer.getId()));
		attribute.setIndex(layer.getIndex());
		attribute.setName(layer.getLayerName());
		attribute.setVisibility(list(layer.getVisibility()));
		attribute.setZoomMin(layer.getMinimumZoom());
		attribute.setZoomMax(layer.getMaximumZoom());
		attribute.setZoomDef(layer.getDefaultZoom());
		attribute.setStyle(map((Map<String, Object>) new Gson().fromJson(layer.getMapStyle(), Map.class)).accept((m) -> {
			String icon = toStringOrNull(m.get("externalGraphic"));
			if (isNotBlank(icon)) {
				m.put("_icon", Optional.ofNullable(easyuploadService.getOrNull(icon)).map(EasyuploadItem::getId).orElse(null));
			}
		}));
		return attribute;
	}

	private GisAttribute toGisAttribute(String classId, WsGeoAttribute attributeData) {
		return GisAttributeImpl.builder()
				.withOwnerClassId(classId)
				.withLayerName(attributeData.getName())
				.withDescription(attributeData.getDescription())
				.withType(attributeData.getSubtype())
				.withIndex(attributeData.getIndex())
				.withMapStyle(toJson(defaultIfNull(attributeData.getStyle(), emptyMap())))
				.withMinimumZoom(attributeData.getZoomMin())
				.withDefaultZoom(attributeData.getZoomDef())
				.withMaximumZoom(attributeData.getZoomMax())
				.withVisibility(set(attributeData.getVisibility()))
				.build();
	}
}
