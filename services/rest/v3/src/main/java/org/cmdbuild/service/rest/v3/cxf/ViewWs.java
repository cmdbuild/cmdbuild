package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.cmdbuild.view.ViewService;
import org.cmdbuild.view.View;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.translation.ObjectTranslationService;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.lang.Visitor;
import org.cmdbuild.view.ViewImpl;
import org.cmdbuild.view.ViewImpl.ViewImplBuilder;
import org.cmdbuild.view.ViewType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;

@Component("v3_views")
@Path("views/")
@Produces(APPLICATION_JSON)
public class ViewWs {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ViewService viewService;
	private final ObjectTranslationService translationService;

	public ViewWs(ViewService viewService, ObjectTranslationService translationService) {
		this.viewService = checkNotNull(viewService);
		this.translationService = checkNotNull(translationService);
	}

	@GET
	@Path(EMPTY)
	public Object list() {
		logger.debug("list all views");
		List<View> views = viewService.getUserViews();
		logger.trace("processing views = {}", views);
		return response(views.stream().map(this::serializeView).collect(toList()));
	}

	@GET
	@Path("{viewId}")
	public Object getOne(@PathParam("viewId") Long viewId) {
		View view = viewService.get(viewId);//TODO apply user permissions
		return response(serializeView(view));
	}

	@POST
	@Path("")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object create(WsViewData data) {
		View view = viewService.create(ViewImpl.builder()
				.accept(data.copyDataToViewVisitor())
				.build());
		return response(serializeView(view));
	}

	@PUT
	@Path("{viewId}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam("viewId") Long viewId, WsViewData data) {
		View view = viewService.get(viewId);
		view = viewService.update(ViewImpl.copyOf(view)
				.accept(data.copyDataToViewVisitor())
				.build());
		return response(serializeView(view));
	}

	@DELETE
	@Path("{viewId}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object delete(@PathParam("viewId") Long viewId) {
		viewService.delete(viewId);
		return success();
	}

	private Object serializeView(View view) {
		return map(
				"_id", view.getId(),
				"name", view.getName(),
				"type", view.getType().name(),
				"description", view.getDescription(),
				"_description_translation", translationService.translateViewDesciption(view.getName(), view.getDescription()),
				"filter", view.getFilter(),
				"sourceClassName", view.getSourceClass(),
				"sourceFunction", view.getSourceFunction()
		);
	}

	public static class WsViewData {

		private final String name;
		private final String description;
		private final String sourceClassName;
		private final String sourceFunction;
		private final String filter;
		private final ViewType type;

		public WsViewData(
				@JsonProperty("name") String name,
				@JsonProperty("description") String description,
				@JsonProperty("sourceClassName") String sourceClassName,
				@JsonProperty("sourceFunction") String sourceFunction,
				@JsonProperty("filter") String filter,
				@JsonProperty("type") String type) {
			this.name = checkNotBlank(name);
			this.description = description;
			this.sourceClassName = sourceClassName;
			this.sourceFunction = sourceFunction;
			this.filter = filter;
			this.type = ViewType.valueOf(checkNotBlank(type).toUpperCase());
		}

		public Visitor<ViewImplBuilder> copyDataToViewVisitor() {
			return (b) -> b
					.withName(name)
					.withDescription(description)
					.withFilter(filter)
					.withSourceClass(sourceClassName)
					.withSourceFunction(sourceFunction)
					.withType(type);
		}

	}
}
