package org.cmdbuild.service.rest.v3.cxf;

//import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.springframework.stereotype.Component;
import org.cmdbuild.auth.session.SessionService;
import org.cmdbuild.auth.session.model.Session;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.config.CoreConfiguration;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ID;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.userconfig.UserConfigService;

/**
 * handles preferences (config) within user sessions
 */
@Path("sessions/{" + ID + "}/preferences")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@Component("v3_sessionPreferences")
public class SessionPreferencesWs extends SessionWsCommons {

	private final UserConfigService userPreferencesStore;
	private final CoreConfiguration coreConfiguration;

	public SessionPreferencesWs(SessionService sessionService, UserConfigService userPreferencesStore, CoreConfiguration coreConfiguration) {
		super(sessionService);
		this.userPreferencesStore = checkNotNull(userPreferencesStore);
		this.coreConfiguration = checkNotNull(coreConfiguration);
	}

	/**
	 *
	 * return preferences for this user (returned data will include defaults and
	 * global variables inherited from above, as well as user-specific and
	 * session-specific stuff).
	 *
	 * Example output:
	 *
	 * <pre><code>
	 *
	 * {
	 *   "data": {
	 *     "test": "test_value"
	 *   }
	 * }
	 *
	 * </code></pre>
	 *
	 *
	 * @param sessionId session id
	 * @return configuration values
	 */
	@GET
	@Path(EMPTY)
	public Object read(@PathParam(ID) String sessionId) {
		logger.debug("read preferences for session = {}", sessionId);
		sessionId = sessionIdOrCurrent(sessionId);
		//TODO validate sessionId = current session id OR isAdmin()
		Session session = sessionService.getSessionById(sessionId);
		Map<String, Object> data = getUserConfig(session.getOperationUser());
		return response(data);
	}

	/**
	 * return config key
	 *
	 * TODO: this will be changed to return full config detail (inheritance,
	 * default, last modified, etc, etc)
	 *
	 * @param sessionId
	 * @param key
	 * @return
	 */
	@GET
	@Path("{key}")
	public String getUserConfig(@PathParam(ID) String sessionId, @PathParam("key") String key) {
		sessionId = sessionIdOrCurrent(sessionId);
		//TODO validate sessionId = current session id OR isAdmin()
		Session session = sessionService.getSessionById(sessionId);
		return userPreferencesStore.getByUsernameOrNull(session.getOperationUser().getLoginUser().getUsername(), key);
	}

	/**
	 * set-update a single config value
	 *
	 * @param sessionId
	 * @param key
	 * @param value
	 */
	@PUT
	@Path("{key}")
	@Consumes(TEXT_PLAIN)
	public Object updateUserConfigValue(@PathParam(ID) String sessionId, @PathParam("key") String key, String value) {
		checkNonSystem(key);
		sessionId = sessionIdOrCurrent(sessionId);
		//TODO validate sessionId = current session id OR isAdmin()
		Session session = sessionService.getSessionById(sessionId);
		userPreferencesStore.setByUsername(session.getOperationUser().getLoginUser().getUsername(), key, value);
		return success();
	}

	/**
	 * delete a single config value
	 *
	 * @param sessionId
	 * @param key
	 */
	@DELETE
	@Path("{key}")
	public Object deleteSystemConfigValue(@PathParam(ID) String sessionId, @PathParam("key") String key) {
		checkNonSystem(key);
		sessionId = sessionIdOrCurrent(sessionId);
		//TODO validate sessionId = current session id OR isAdmin()
		Session session = sessionService.getSessionById(sessionId);
		userPreferencesStore.deleteByUsername(session.getOperationUser().getLoginUser().getUsername(), key);
		return success();
	}

	private void checkNonSystem(String key) {
		checkArgument(!key.startsWith("cm_ui_"), "cannot set system preferences");
	}

	private Map<String, Object> getUserConfig(OperationUser operationUser) {
		Map<String, String> userConfig = userPreferencesStore.getByUsername(operationUser.getUsername());//TODO merge lang and starting class from here
		String initialPage = userConfig.get("cm_user_initialPage");
		if (initialPage == null && operationUser.hasDefaultGroup() && operationUser.getDefaultGroup().hasStartingClass()) {
			initialPage = operationUser.getDefaultGroup().getStartingClass();
		}
		if (initialPage == null) {
			initialPage = coreConfiguration.getStartingClassName();
		}
		return (Map) map()
				.with("cm_ui_startingClass", initialPage)
				.accept((m) -> {
					if (operationUser.hasDefaultGroup()) {
						m.put(
								//								"cm_ui_disabledModules", configuration.getDisabledModules(),
								//								"cm_ui_disabledCardTabs", configuration.getDisabledCardTabs(),
								//								"cm_ui_disabledProcessTabs", configuration.getDisabledProcessTabs(),
								//								"cm_ui_hideSidePanel", configuration.isHideSidePanel(),
								//								"cm_ui_fullScreenMode", configuration.isFullScreenMode(),
								//								"cm_ui_simpleHistoryModeForCard", configuration.isSimpleHistoryModeForCard(),
								//								"cm_ui_simpleHistoryModeForProcess", configuration.isSimpleHistoryModeForProcess(),
								"cm_ui_processWidgetAlwaysEnabled", operationUser.getDefaultGroup().getConfig().getProcessWidgetAlwaysEnabled());
					}
				})
				.with(userConfig);
	}
}
