package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.cardfilter.CardFilter;
import org.cmdbuild.cardfilter.CardFilterImpl;
import org.cmdbuild.cardfilter.CardFilterImpl.CardFilterImplBuilder;
import org.cmdbuild.cardfilter.CardFilterService;
import org.cmdbuild.common.utils.PagedElements;
import static org.cmdbuild.common.utils.PagedElements.paged;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.lang.Visitor;
import org.springframework.stereotype.Component;

@Component("v3_classFilters")
@Path("{a:classes|processes}/{" + CLASS_ID + "}/filters/")
@Produces(APPLICATION_JSON)
public class ClassFilterWs {

	private final CardFilterService filterService;
	private final OperationUserSupplier userStore;

	public ClassFilterWs(CardFilterService filterService, OperationUserSupplier userStore) {
		this.filterService = checkNotNull(filterService);
		this.userStore = checkNotNull(userStore);
	}

	@POST
	@Path(EMPTY)
	public Object create(@PathParam(CLASS_ID) String classId, WsFilterData element) {
		CardFilter filter = filterService.create(element.toCardFilter().accept(setCurrentUserForNonSharedFiltersVisitor(element)).build());
		return response(serializeFilter(filter));
	}

	@GET
	@Path(EMPTY)
	public Object readAll(@PathParam(CLASS_ID) String classId, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset, @QueryParam("shared") boolean sharedOnly) {
		List<CardFilter> list;
		if (equal(classId, "_ALL")) {
			list = filterService.readAllSharedFilters();
		} else if (sharedOnly) {
			list = filterService.readSharedForCurrentUser(classId);
		} else {
			list = filterService.readAllForCurrentUser(classId);
		}
		PagedElements<CardFilter> res = paged(list, offset, limit);
		return response(res.stream().map(ClassFilterWs::serializeFilter).collect(toList()), res.totalSize());
	}

	@GET
	@Path("{" + FILTER_ID + "}/")
	public Object read(@PathParam(CLASS_ID) String classId, @PathParam(FILTER_ID) Long filterId) {
		return response(serializeFilter(filterService.getById(filterId)));
	}

	@PUT
	@Path("{" + FILTER_ID + "}/")
	public Object update(@PathParam(CLASS_ID) String classId, @PathParam(FILTER_ID) Long filterId, WsFilterData element) {
		CardFilter filter = filterService.update(element.toCardFilter().withId(filterId).accept(setCurrentUserForNonSharedFiltersVisitor(element)).build());
		return response(serializeFilter(filter));
	}

	@DELETE
	@Path("{" + FILTER_ID + "}/")
	public Object delete(@PathParam(CLASS_ID) String classId, @PathParam(FILTER_ID) Long filterId) {
		filterService.delete(filterId);
		return success();
	}

	public static FluentMap<String, Object> serializeFilter(CardFilter filter) {
		return map(
				"_id", filter.getId(),
				"name", filter.getName(),
				"description", filter.getDescription(),
				"target", filter.getClassName(),
				"configuration", filter.getConfiguration(),
				"shared", filter.isShared()
		);
	}

	private Visitor<CardFilterImplBuilder> setCurrentUserForNonSharedFiltersVisitor(WsFilterData data) {
		return (b) -> {
			if (!data.shared) {
				b.withUserId(userStore.getUser().getLoginUser().getId());
			}
		};
	}

	public static class WsFilterData {
		private final String name;
		private final String description;
		private final String target;
		private final String configuration;
		private final boolean shared;

		public WsFilterData(@JsonProperty("name") String name,
				@JsonProperty("description") String description,
				@JsonProperty("target") String target,
				@JsonProperty("configuration") String configuration,
				@JsonProperty("shared") Boolean shared) {
			this.name = checkNotBlank(name, "missing required param 'name'");
			this.description = description;
			this.target = checkNotBlank(target, "missing required param 'target'");
			this.configuration = configuration;
			this.shared = shared;
		}

		public CardFilterImplBuilder toCardFilter() {
			return CardFilterImpl.builder()
					.withClassName(target)
					.withConfiguration(configuration)
					.withDescription(description)
					.withName(name)
					.withShared(shared);
		}
	}
}
