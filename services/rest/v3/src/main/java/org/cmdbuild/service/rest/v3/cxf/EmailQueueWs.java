package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.WILDCARD;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_SYSTEM_ACCESS_AUTHORITY;
import org.springframework.stereotype.Component;
import org.cmdbuild.email.EmailService;
import org.cmdbuild.email.queue.EmailQueueService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.springframework.security.access.prepost.PreAuthorize;

@Component("v3_email_queue")
@Path("email/queue/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class EmailQueueWs {

	private final EmailService emailService;
	private final EmailQueueService queueService;

	public EmailQueueWs(EmailService emailService, EmailQueueService queueService) {
		this.emailService = checkNotNull(emailService);
		this.queueService = checkNotNull(queueService);
	}

	@POST
	@Path("trigger")
	@Consumes(WILDCARD)
	@PreAuthorize(HAS_SYSTEM_ACCESS_AUTHORITY)
	public Object triggerQueue() {
		queueService.triggerEmailQueue();
		return success();
	}

}
