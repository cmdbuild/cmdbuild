package org.cmdbuild.service.rest.v3.cxf;

import java.util.List;
import org.cmdbuild.service.rest.v3.model.WsRelationData;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.cmdbuild.common.utils.PagedElements.isPaged;
import org.cmdbuild.dao.beans.CMRelation;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.utils.CmdbFilterUtils;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DETAILED;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DOMAIN_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.RELATION_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.beans.RelationImpl;
import org.cmdbuild.dao.core.q3.DaoService;

@Component("v3_relations")
@Path("domains/{" + DOMAIN_ID + "}/relations/")
@Produces(APPLICATION_JSON)
public class RelationWs {

	private final DaoService dao;

	public RelationWs(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@GET
	@Path(EMPTY)
	public Object read(@PathParam(DOMAIN_ID) String domainId, @QueryParam(FILTER) String filterStr, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset, @QueryParam(DETAILED) boolean detailed) {
		Domain domain = dao.getDomain(domainId);
//		String _filter = defaultString(filter);
//		// TODO do it better
//		// <<<<<
//		String regex_1 = "\"attribute\"[\\w]*:[\\w]*\"" + UNDERSCORED_SOURCE_ID + "\"";
//		String replacement_1 = "\"attribute\":\"IdObj1\"";
//		_filter = _filter.replaceAll(regex_1, replacement_1);
//
//		String regex_2 = "\"attribute\"[\\w]*:[\\w]*\"" + UNDERSCORED_DESTINATION_ID + "\"";
//		String replacement_2 = "\"attribute\":\"IdObj2\"";
//		_filter = _filter.replaceAll(regex_2, replacement_2);
//		// <<<<<
		CmdbFilter filter = CmdbFilterUtils.fromNullableJson(filterStr);
//		filter = CmdbFilterUtils.merge(filter, CmdbFilterImpl.builder().withAttributeFilter(AttributeFilterImpl.simple(AttributeFilterConditionImpl.eq("_Src", "_1"))).build());

		List<CMRelation> relations = dao.selectAll().from(domain).where(filter).paginate(offset, limit).getRelations();

		long total;
		if (isPaged(offset, limit)) {
			total = dao.selectCount().from(domain).where(filter).getCount();
		} else {
			total = relations.size();
		}

		return response(relations.stream().map(detailed ? this::serializeDetailedRelation : this::serializeMinimalRelation).collect(toList()), total);
	}

	@GET
	@Path("{" + RELATION_ID + "}/")
	public Object read(@PathParam(DOMAIN_ID) String domainId, @PathParam(RELATION_ID) Long relationId) {
		CMRelation relation = dao.getRelation(domainId, relationId);
		return response(serializeDetailedRelation(relation));
	}

	@POST
	@Path(EMPTY)
	public Object create(@PathParam(DOMAIN_ID) String domainId, WsRelationData relationData) {

		CMRelation relation = RelationImpl.builder()
				.withType(dao.getDomain(domainId))
				.withSourceCard(dao.getCard(relationData.getSourceClassId(), relationData.getSourceCardId()))
				.withTargetCard(dao.getCard(relationData.getDestinationClassId(), relationData.getDestinationCardId()))
				.addAttributes(relationData.getValues())
				.build();

		relation = dao.create(relation);

		return response(serializeDetailedRelation(relation));
	}

	@PUT
	@Path("{" + RELATION_ID + "}/")
	public Object update(@PathParam(DOMAIN_ID) String domainId, @PathParam(RELATION_ID) Long relationId, WsRelationData relationData) {
		CMRelation relation = dao.getRelation(domainId, relationId);

		relation = RelationImpl.copyOf(relation)
				.withSourceCard(dao.getCard(relationData.getSourceClassId(), relationData.getSourceCardId()))
				.withTargetCard(dao.getCard(relationData.getDestinationClassId(), relationData.getDestinationCardId()))
				.addAttributes(relationData.getValues())
				.build();

		relation = dao.update(relation);

		return response(serializeDetailedRelation(relation));
	}

	@DELETE
	@Path("{" + RELATION_ID + "}/")
	public Object delete(@PathParam(DOMAIN_ID) String domainId, @PathParam(RELATION_ID) Long relationId) {
		CMRelation relation = dao.getRelation(domainId, relationId);
		dao.delete(relation);
		return success();
	}

	private FluentMap<String, Object> serializeMinimalRelation(CMRelation relation) {
		return map("_id", relation.getId(),
				"_type", relation.getType().getName(),
				"_sourceType", relation.getType().getSourceClass().getName(),
				"_sourceId", relation.getSourceId(),
				"_destinationType", relation.getType().getTargetClass().getName(),
				"_destinationId", relation.getTargetId()
		);
	}

	private FluentMap<String, Object> serializeDetailedRelation(CMRelation relation) {
		return serializeMinimalRelation(relation)
				.with(
						"_sourceDescription", relation.getSourceDescription(),//TODO translation
						"_sourceCode", relation.getSourceCode(),
						"_destinationDescription", relation.getTargetDescription(),//TODO translation
						"_destinationCode", relation.getTargetCode()
				).accept((map) -> {
					relation.getType().getCoreAttributes().forEach((a) -> {
						map.put(a.getName(), relation.get(a.getName()));//TODO map value to client
					});
				});
	}
}
