/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ATTRIBUTE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DIRECTION;

@XmlRootElement
public class AttributeOrder extends AbstractModel {

	private String attribute;
	private String direction;

	AttributeOrder() {
		// package visibility
	}

	@XmlAttribute(name = ATTRIBUTE)
	public String getAttribute() {
		return attribute;
	}

	void setAttribute(final String attribute) {
		this.attribute = attribute;
	}

	@XmlAttribute(name = DIRECTION)
	public String getDirection() {
		return direction;
	}

	void setDirection(final String direction) {
		this.direction = direction;
	}

	@Override
	protected boolean doEquals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof AttributeOrder)) {
			return false;
		}
		final AttributeOrder other = AttributeOrder.class.cast(obj);
		return new EqualsBuilder() //
				.append(this.attribute, other.attribute) //
				.append(this.direction, other.direction) //
				.isEquals();
	}

	@Override
	protected int doHashCode() {
		return new HashCodeBuilder() //
				.append(attribute) //
				.append(direction) //
				.toHashCode();
	}

}
