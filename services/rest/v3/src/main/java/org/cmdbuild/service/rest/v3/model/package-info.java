@XmlSchema(namespace = NAMESPACE, elementFormDefault = QUALIFIED)
package org.cmdbuild.service.rest.v3.model;

import static javax.xml.bind.annotation.XmlNsForm.QUALIFIED;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.NAMESPACE;

import javax.xml.bind.annotation.XmlSchema;
