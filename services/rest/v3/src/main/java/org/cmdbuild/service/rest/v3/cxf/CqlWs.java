package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Maps.transformEntries;
import static org.cmdbuild.service.rest.v3.model.Models.newCard;

import java.util.Map;
import java.util.Map.Entry;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.cmdbuild.common.data.QueryOptions;

import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.service.rest.v3.model.WsCard;

import org.cmdbuild.data2.api.DataAccessService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsAttributeConverterUtils.toClient;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.utils.CmdbFilterUtils;
import org.cmdbuild.data.filter.utils.CmdbSorterUtils;
import org.cmdbuild.ecql.EcqlService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SORT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;

@Component("v3_cql")//TODO is this endpoint duplicate of class ws? if so, remove this
@Path("cql/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class CqlWs {

	private final DataAccessService dataAccessLogic;
	private final EcqlService ecqlService;

	public CqlWs(EcqlService ecqlService, DataAccessService dataAccessLogic) {
		this.dataAccessLogic = checkNotNull(dataAccessLogic);
		this.ecqlService = checkNotNull(ecqlService);
	}

	@GET
	@Path(EMPTY)
	public Object read(@QueryParam(FILTER) String filter, @QueryParam(SORT) String sort, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {

		CmdbFilter cmdbFilter = CmdbFilterUtils.fromNullableJson(filter);

//		if (cmdbFilter.hasEcqlFilter()) {
//			String cqExprFromEcqlId = ecqlService.prepareCqlExpression(cmdbFilter.getEcqlFilter().getEcqlId(), cmdbFilter.getEcqlFilter().getJsContext());
//			cmdbFilter = CmdbFilterImpl
//					.copyOf(cmdbFilter)
//					.withEcqlFilter(null)
//					.withCqlFilter(new CqlFilterImpl(cqExprFromEcqlId))
//					.build();
//		}
		QueryOptions queryOptions = QueryOptionsImpl.builder()
				.filter(cmdbFilter)
				.orderBy(CmdbSorterUtils.fromNullableJson(sort))
				.limit(limit)
				.offset(offset)
				.build();
		PagedElements<org.cmdbuild.dao.beans.Card> response = dataAccessLogic.fetchCards(null, queryOptions);//TODO check this
		Iterable<WsCard> elements = from(response.elements())
				.transform((org.cmdbuild.dao.beans.Card input) -> newCard()
				.withType(input.getType().getName())
				.withId(input.getId())
				.withValues(adaptOutputValues(input.getType(), input.getAllValuesAsMap()))
				.build());
		return response(elements);
	}

	private Iterable<? extends Entry<String, Object>> adaptOutputValues(Classe targetClass, Map<String, Object> values) {
		return transformEntries(values, (String key, Object value) -> {
			Attribute attribute = targetClass.getAttributeOrNull(key);
			if (attribute == null) {
				return value;
			} else {
				CardAttributeType<?> attributeType = attribute.getType();
				return toClient(attributeType, value);
			}
		}).entrySet();
	}

}
