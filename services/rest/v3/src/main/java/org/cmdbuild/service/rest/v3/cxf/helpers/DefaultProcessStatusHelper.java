package org.cmdbuild.service.rest.v3.cxf.helpers;

import static com.google.common.collect.FluentIterable.from;

import org.cmdbuild.service.rest.v3.cxf.serialization.ToProcessStatus;
import org.cmdbuild.service.rest.v3.model.ProcessStatus;
import org.cmdbuild.workflow.core.LookupHelper;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import org.springframework.stereotype.Component;

@Component
public class DefaultProcessStatusHelper implements ProcessStatusHelper {

	private static final Predicate<ProcessStatus> VALID_STATUSES = (ProcessStatus input) -> (input.getValue() != null);

	private final LookupHelper lookupHelper;

	public DefaultProcessStatusHelper(LookupHelper lookupHelper) {
		this.lookupHelper = checkNotNull(lookupHelper);
	}

	@Override
	public Iterable<ProcessStatus> allValues() {
		return from(lookupHelper.allLookups()).transform(ToProcessStatus::toProcessStatus).filter(VALID_STATUSES);
	}

}
