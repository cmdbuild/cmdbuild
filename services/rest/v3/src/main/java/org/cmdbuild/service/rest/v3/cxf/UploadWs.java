package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import java.util.List;

import javax.activation.DataHandler;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.cmdbuild.easyupload.EasyuploadItem;
import org.cmdbuild.easyupload.EasyuploadItemInfo;
import org.cmdbuild.easyupload.EasyuploadService;
import static org.cmdbuild.easyupload.EasyuploadUtils.toDataHandler;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.toByteArray;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;

@Component("v3_upload")
@Path("uploads/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class UploadWs {

	private final EasyuploadService easyuploadService;

	public UploadWs(EasyuploadService easyuploadService) {
		this.easyuploadService = checkNotNull(easyuploadService);
	}

	@GET
	@Path("")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object readMany(@QueryParam("path") @Nullable String dir) {
		List<EasyuploadItemInfo> list;
		if (isNotBlank(dir)) {
			list = easyuploadService.getByDir(dir);
		} else {
			list = easyuploadService.getAll();
		}
		return response(list.stream()
				.sorted(Ordering.natural().onResultOf(EasyuploadItemInfo::getPath))
				.map(this::serializeItem));
	}

	@GET
	@Path("{fileId}")
	public Object readFile(@PathParam("fileId") Long fileId) {
		EasyuploadItem item = easyuploadService.getById(fileId);
		return response(serializeItem(item));
	}

	@GET
	@Path("{fileId}/{file:[^/]+}")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler downloadFile(@PathParam("fileId") Long fileId) {
		EasyuploadItem item = easyuploadService.getById(fileId);
		return toDataHandler(item);
	}

	@GET
	@Path("_MANY/{file:[^/]+.zip}")
	@Produces(APPLICATION_OCTET_STREAM)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public DataHandler downloadManyFiles(@QueryParam("path") String dir) {
		return new DataHandler(easyuploadService.getUploadsAsZipFile(dir));
	}

	@GET
	@Path("_ALL/{file:[^/]+.zip}")
	@Produces(APPLICATION_OCTET_STREAM)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public DataHandler downloadAllFiles(@QueryParam("path") String dir) {
		return new DataHandler(easyuploadService.getAllUploadsAsZipFile());
	}

	@POST
	@Path("")
	@Consumes(MULTIPART_FORM_DATA)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object loadFile(@Multipart("file") DataHandler dataHandler, @Multipart("path") String dir) {
		EasyuploadItem item;
		if (isBlank(dir)) {
			item = easyuploadService.create(dataHandler);
		} else {
			item = easyuploadService.create(dir, dataHandler);
		}
		return response(serializeItem(item));
	}

	@POST
	@Path("_MANY")
	@Consumes(MULTIPART_FORM_DATA)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object loadZipFile(@Multipart("file") DataHandler dataHandler) {
		easyuploadService.uploadZip(toByteArray(dataHandler));
		return success();
	}

	@PUT
	@Path("{fileId}")
	@Consumes(MULTIPART_FORM_DATA)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object updateFile(@PathParam("fileId") Long fileId, @Multipart("file") DataHandler dataHandler) {
		EasyuploadItem item = easyuploadService.update(fileId, toByteArray(dataHandler));//TODO check this
		return response(serializeItem(item));
	}

	@DELETE
	@Path("{fileId}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object deleteFile(@PathParam("fileId") Long fileId) {
		easyuploadService.delete(fileId);
		return success();
	}

	private Object serializeItem(EasyuploadItemInfo item) {
		return map(
				"_id", item.getId(),
				"path", item.getPath(),
				"name", item.getFileName(),
				"folder", item.getFolder()
		);
	}

}
