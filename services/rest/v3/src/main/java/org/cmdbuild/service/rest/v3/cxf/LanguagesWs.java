package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.cmdbuild.common.localization.LanguageInfo;
import org.cmdbuild.common.localization.LanguageService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;

@Component("v3_languages")
@Path("languages")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class LanguagesWs {

	private final LanguageService languageService;

	public LanguagesWs(LanguageService languageService) {
		this.languageService = checkNotNull(languageService);
	}

	@GET
	@Path("")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object readLanguages(@QueryParam("active") Boolean activeOnly) {
		Collection<LanguageInfo> list = firstNonNull(activeOnly, false) ? languageService.getEnabledLanguagesInfo() : languageService.getAllLanguages();
		return response(list.stream().map(LanguagesConfigurationWs::languageInfoToResponse).collect(toList()));
	}

}
