package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import org.cmdbuild.service.rest.v3.model.WsCard;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.cmdbuild.cardfilter.CardFilterService;
import static org.cmdbuild.common.utils.PagedElements.hasLimit;
import static org.cmdbuild.common.utils.PagedElements.isPaged;
import org.cmdbuild.dao.core.q3.DaoService;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Classe;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CARD_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.POSITION_OF;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SORT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.data.filter.utils.CmdbSorterUtils;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import static org.cmdbuild.dao.core.q3.WhereOperator.EQ;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.CmdbSorter;
import org.cmdbuild.data.filter.utils.CmdbFilterUtils;
import static org.cmdbuild.service.rest.v3.cxf.CardWs.mapSorterAttributeNames;
import org.cmdbuild.service.rest.v3.cxf.serialization.CardSerializationHelper;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNotNullAndGtZero;
import org.cmdbuild.view.View;
import org.cmdbuild.view.ViewService;
import org.cmdbuild.view.ViewType;

@Component("v3_viewCards")
@Path("views/{viewId}/cards/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class ViewCardWs {

	private final DaoService dao;
	private final CardSerializationHelper helper;
	private final ViewService viewService;
	private final CardFilterService filterService;

	public ViewCardWs(DaoService dao, CardSerializationHelper helper, ViewService viewService, CardFilterService filterService) {
		this.dao = checkNotNull(dao);
		this.helper = checkNotNull(helper);
		this.viewService = checkNotNull(viewService);
		this.filterService = checkNotNull(filterService);
	}

	@POST
	@Path(EMPTY)
	public Object create(@PathParam("viewId") Long viewId, WsCard data) {
		View view = viewService.get(viewId);
		checkViewIsFilterView(view);
		Classe targetClass = dao.getClasse(view.getSourceClass());
		Card card = CardImpl.buildCard(targetClass, data.getValues());
		card = dao.create(card);//TODO check filter
		return response(helper.serializeCard(card));
	}

	@GET
	@Path("{" + CARD_ID + "}/")
	public Object readOne(@PathParam("viewId") Long viewId, @PathParam(CARD_ID) Long cardId) {
		View view = viewService.get(viewId);
		checkViewIsFilterView(view);
		Card card = dao.getCard(view.getSourceClass(), cardId);//TODO apply filter
		return response(helper.serializeCard(card));
	}

	@GET
	@Path(EMPTY)
	public Object readMany(
			@PathParam("viewId") Long viewId,
			@QueryParam(FILTER) String filterStr,
			@QueryParam(SORT) String sort,
			@QueryParam(LIMIT) Long limit,
			@QueryParam(START) Long offset,
			@QueryParam(POSITION_OF) Long positionOf,
			@QueryParam("forDomain_name") String forDomainName,
			@QueryParam("forDomain_direction") String forDomainDirection,
			@QueryParam("forDomain_originId") Long forDomainOriginId
	) {

		View view = viewService.get(viewId);

		CmdbFilter filterParam = CmdbFilterUtils.fromNullableJson(getFilterOrNull(filterStr));//TODO map filter attribute names; 
		CmdbSorter sorter = mapSorterAttributeNames(CmdbSorterUtils.fromNullableJson(sort));

		switch (view.getType()) {
			case FILTER: {
				String classId = view.getSourceClass();
				CmdbFilter viewFilter = CmdbFilterUtils.fromJson(view.getFilter());
				CmdbFilter filter = CmdbFilterUtils.merge(viewFilter, filterParam);

				FluentMap meta = map(); //TODO duplicate code from CardWs.

				if (isNotNullAndGtZero(positionOf)) {
					checkArgument(hasLimit(limit), "must set valid 'limit' along with 'positionOf'");
					long rowNumber = dao.selectRowNumber().where(ATTR_ID, EQ, positionOf).then()
							.from(classId)
							.orderBy(sorter)
							.where(filter)
							.build().getRowNumberOrNull();
					long positionInPage = rowNumber % limit;
					offset = rowNumber - positionInPage;
					meta.put("positions", map(positionOf, positionInPage), START, offset, LIMIT, limit);
				}

				List<Card> cards = dao.selectAll()
						.from(classId)
						.orderBy(sorter)
						.where(filter)
						.paginate(offset, limit)
						.getCards();

				long total;
				if (isPaged(offset, limit)) {
					total = dao.selectCount()
							.from(classId)
							.where(filter)
							.getCount();
				} else {
					total = cards.size();
				}

				return response(cards.stream().map(helper::serializeCard).collect(toList()), total, meta);
			}
			case SQL:
				throw new UnsupportedOperationException("not supported yet");//TODO
			default:
				throw unsupported("unsupported view type = %s", view.getType());
		}
	}

	@PUT
	@Path("{" + CARD_ID + "}/")
	public Object update(@PathParam("viewId") Long viewId, @PathParam(CARD_ID) Long cardId, WsCard data) {
		View view = viewService.get(viewId);
		checkViewIsFilterView(view);
		Classe classe = dao.getClasse(view.getSourceClass());
		Card card = CardImpl.builder()
				.withType(classe)
				.withAttributes(data.getValues())
				.withId(cardId)
				.build();
		card = dao.update(card);//TODO check filter
		return response(helper.serializeCard(card));
	}

	@DELETE
	@Path("{" + CARD_ID + "}/")
	public Object delete(@PathParam("viewId") Long viewId, @PathParam(CARD_ID) Long cardId) {
		View view = viewService.get(viewId);
		checkViewIsFilterView(view);
		dao.delete(view.getSourceClass(), cardId);//TODO check filter
		return success();
	}

	private void checkViewIsFilterView(View view) {
		checkArgument(view.isOfType(ViewType.FILTER), "this action is available only for 'filter' type views, this view is of type = %s", view.getType());
	}

	@Nullable//TODO duplicate code
	private String getFilterOrNull(@Nullable String filter) {
		return CardWs.getFilterOrNull(filter, (id) -> filterService.getById(id).getConfiguration());
	}
}
