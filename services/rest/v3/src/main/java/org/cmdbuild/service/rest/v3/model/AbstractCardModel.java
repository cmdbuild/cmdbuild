package org.cmdbuild.service.rest.v3.model;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.UNDERSCORED_TYPE;

import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class AbstractCardModel extends ModelWithLongId {

	private String type;

	protected AbstractCardModel() {
		// usable by subclasses only
	}

	@XmlAttribute(name = UNDERSCORED_TYPE)
	@JsonProperty(UNDERSCORED_TYPE)
	public String getType() {
		return type;
	}

	void setType(final String type) {
		this.type = type;
	}

}
