package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.service.rest.v3.cxf.helpers.IdGenerator;

import com.google.common.collect.Ordering;
import java.util.Collection;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.function.Consumer;
import static java.util.stream.Collectors.toList;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.cmdbuild.classe.ExtendedClass;
import org.cmdbuild.service.rest.v3.cxf.serialization.ClassSerializationHelper;
import org.cmdbuild.service.rest.v3.cxf.serialization.ClassSerializationHelper.WsClassData;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.utils.date.DateUtils;
import org.cmdbuild.workflow.WorkflowService;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ACTIVE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import org.cmdbuild.workflow.model.XpdlInfo;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.toDataSource;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import org.cmdbuild.workflow.model.Process;
import org.springframework.security.access.prepost.PreAuthorize;
import org.cmdbuild.classe.UserClassService;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;

@Component("v3_processes")
@Path("{a:processes}/")
@Produces(APPLICATION_JSON)
public class ProcessWs {

	private final WorkflowService workflowService;
	private final UserClassService classService;
	private final IdGenerator idGenerator;
	private final ClassSerializationHelper helper;

	public ProcessWs(WorkflowService workflowService, UserClassService classService, IdGenerator idGenerator, ClassSerializationHelper helper) {
		this.workflowService = checkNotNull(workflowService);
		this.classService = checkNotNull(classService);
		this.idGenerator = checkNotNull(idGenerator);
		this.helper = checkNotNull(helper);
	}

	@GET
	@Path(EMPTY)
	public Object readAll(@QueryParam(ACTIVE) boolean activeOnly, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {
		Collection<Process> all = activeOnly ? workflowService.getActiveProcessClasses() : workflowService.getAllProcessClasses();
		List<Process> ordered = Ordering.natural().onResultOf(Process::getName).sortedCopy(all);
		List elements = ordered.stream()
				.skip(firstNonNull(offset, 0))
				.limit(firstNonNull(limit, Integer.MAX_VALUE))
				.map(this::minimalResponse).collect(toList());
		return map("success", true, "data", elements, "meta", map("total", ordered.size()));
	}

	@GET
	@Path("{processId}/")
	public Object read(@PathParam("processId") String processId) {
		Process classe = workflowService.getProcess(processId);
		return response(detailedResponse(classe));
	}

	@POST
	@Path(EMPTY)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object create(WsClassData data) {
		ExtendedClass classe = classService.createClass(helper.extendedClassDefinitionForNewClass(data));
		return read(classe.getClasse().getName());
	}

	@PUT
	@Path("{processId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam("processId") String classId, WsClassData data) {
		ExtendedClass classe = classService.updateClass(helper.extendedClassDefinitionForExistingClass(classId, data));
		return read(classe.getClasse().getName());
	}

	@DELETE
	@Path("{processId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object delete(@PathParam("processId") String classId) {
		classService.deleteClass(classId);
		return success();
	}

	@GET
	@Path("{processId}/generate_id")
	public Object generateId(@PathParam("processId") String processId) {
		return response(idGenerator.generate());
	}

	@POST
	@Path("{processId}/versions")
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	public Object uploadNewXpdlVersion(@PathParam("processId") String processId, @Multipart(FILE) DataHandler dataHandler) {
		XpdlInfo xpdlInfo = workflowService.addXpdl(processId, toDataSource(dataHandler));
		return response(xpdlInfoToResponse(xpdlInfo));
	}

	@POST
	@Path("{processId}/migration")
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	public Object uploadXpdlVersionAndMigrateProcessToNewProvider(@PathParam("processId") String processId, @QueryParam("provider") String provider, @Multipart(FILE) DataHandler dataHandler) {
		workflowService.addXpdl(processId, provider, toDataSource(dataHandler));
		workflowService.migrateFlowInstancesToNewProvider(processId, provider);
		return success();
	}

	@GET
	@Path("{processId}/versions")
	public Object getAllXpdlVersions(@PathParam("processId") String processId) {
		List<XpdlInfo> versions;
		if (workflowService.isWorkflowEnabled()) {
			versions = workflowService.getXpdlInfosOrderByVersionDesc(processId);
		} else {
			versions = emptyList();
		}
		return response(versions.stream().map(ProcessWs::xpdlInfoToResponse).collect(toList()));
	}

	@GET
	@Path("{processId}/versions/{planId}/file")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler getXpdlVersionFile(@PathParam("processId") String processId, @PathParam("planId") String planId) {
		DataSource dataSource = workflowService.getXpdlByClasseIdAndPlanId(processId, planId);
		return new DataHandler(dataSource);
	}

	@GET
	@Path("{processId}/template")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler getXpdlTemplateFile(@PathParam("processId") String processId) {
		DataSource dataSource = workflowService.getXpdlTemplate(processId);
		return new DataHandler(dataSource);
	}

	private static Object xpdlInfoToResponse(XpdlInfo version) {
		return map(
				"_id", version.getPlanId(),
				"provider", version.getProvider(),
				"version", version.getVersion(),
				"planId", version.getPlanId(),
				"default", version.isDefault(),
				"lastUpdate", DateUtils.toIsoDateTime(version.getLastUpdate()));
	}

	private FluentMap<String, Object> minimalResponse(Process p) {
		return helper.buildBasicResponse(p).accept(processSpecificDataMapConsumer(p));
	}

	private Consumer<FluentMap<String, Object>> processSpecificDataMapConsumer(Process p) {
		return (m) -> m.put(
				"flowStatusAttr", p.getFlowStatusLookup(),
				"messageAttr", p.getMetadata().getMessageAttr(),
				"enableSaveButton", p.isFlowSaveButtonEnabled(),
				"stoppableByUser", p.getMetadata().isUserStoppable(),//TODO add user permissions here ??
				"engine", p.getProviderOrNull()//TODO provider must be not null
		);
	}

	private FluentMap<String, Object> detailedResponse(Process process) {
		return helper.buildFullDetailResponse(process).accept(processSpecificDataMapConsumer(process)).with( //				"statuses",//TODO
				//				"defaultStatus",//TODO
				);

	}

}
