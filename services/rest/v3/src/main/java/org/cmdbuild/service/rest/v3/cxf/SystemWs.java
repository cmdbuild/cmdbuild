package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import static com.google.common.collect.Maps.newLinkedHashMap;
import com.google.common.collect.Ordering;
import java.io.File;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static javax.ws.rs.core.MediaType.WILDCARD;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.io.FileUtils;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.cmdbuild.auth.multitenant.api.MultitenantService;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.common.logging.LoggerConfigurationService;
import org.cmdbuild.common.logging.SimpleLoggerConfig;
import org.cmdbuild.audit.RequestTrackingService;
import org.cmdbuild.cache.CmCacheStats;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.common.logging.LoggerConfig;
import org.cmdbuild.config.RequestTrackingConfiguration;
import org.cmdbuild.dao.ConfigurableDataSource;
import org.cmdbuild.dao.config.inner.PatchManager;
import org.cmdbuild.scheduler.SchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.dao.driver.postgres.DumpService;
import org.cmdbuild.river.task.scriptexecutors.BeanshellScriptExecutor;
import org.cmdbuild.scheduler.ScheduledJobInfo;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationUtils.serializePatchInfo;
import org.cmdbuild.utils.date.DateUtils;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempFile;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.toByteArray;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import org.springframework.security.access.prepost.PreAuthorize;
import org.cmdbuild.debuginfo.BugReportService;
import org.cmdbuild.debuginfo.BugReportInfo;
import static org.cmdbuild.debuginfo.SystemStatusUtils.getIpAddr;
import static org.cmdbuild.utils.date.DateUtils.systemDate;
import static org.cmdbuild.utils.date.DateUtils.toIsoDateTime;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_SYSTEM_ACCESS_AUTHORITY;
import org.cmdbuild.clustering.ClusteringService;
import org.cmdbuild.platform.PlatformService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILE;
import org.cmdbuild.utils.io.CmdbuildIoUtils;
import static org.cmdbuild.utils.io.CmdbuildNetUtils.getHostname;

@Component("v3_system")
@Path("system/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@PreAuthorize(HAS_SYSTEM_ACCESS_AUTHORITY)
public class SystemWs {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ConfigurableDataSource dataSource;
	private final CacheService cacheService;
	private final RequestTrackingService requestTrackingService;
	private final PatchManager patchManager;
	private final SchedulerService schedulerService;
	private final MultitenantService multitenantService;
	private final LoggerConfigurationService loggerConfigurationService;
	private final BugReportService bugreportService;
	private final DumpService dumpService;
	private final ClusteringService clusteringService;
	private final PlatformService platformService;

	public SystemWs(ConfigurableDataSource dataSource, CacheService cacheService, RequestTrackingService requestTrackingService, PatchManager patchManager, SchedulerService schedulerService, MultitenantService multitenantService, LoggerConfigurationService loggerConfigurationService, BugReportService bugreportService, DumpService dumpService, ClusteringService clusteringService, PlatformService platformService) {
		this.dataSource = checkNotNull(dataSource);
		this.cacheService = checkNotNull(cacheService);
		this.requestTrackingService = checkNotNull(requestTrackingService);
		this.patchManager = checkNotNull(patchManager);
		this.schedulerService = checkNotNull(schedulerService);
		this.multitenantService = checkNotNull(multitenantService);
		this.loggerConfigurationService = checkNotNull(loggerConfigurationService);
		this.bugreportService = checkNotNull(bugreportService);
		this.dumpService = checkNotNull(dumpService);
		this.clusteringService = checkNotNull(clusteringService);
		this.platformService = checkNotNull(platformService);
	}

	@GET
	@Path("status")
	public Object status() {
		return response(map(
				"hostname", getHostname(),
				"hostaddress", getIpAddr(),
				"runtime", Runtime.getRuntime().toString(),
				"server_time", toIsoDateTime(systemDate())).accept((map) -> {
			try {
				BasicDataSource basicDataSource = dataSource.getInner();
				map.put("datasource_active_connections", String.valueOf(basicDataSource.getNumActive()));
				map.put("datasource_idle_connections", String.valueOf(basicDataSource.getNumIdle()));
				map.put("datasource_max_active_connections", String.valueOf(basicDataSource.getMaxTotal()));
				map.put("datasource_max_idle_connections", String.valueOf(basicDataSource.getMaxIdle()));
			} catch (Exception ex) {
				logger.warn(marker(), "error retrieving datasource info", ex);
			}
		}));
	}

	@GET
	@Path("cluster/status")
	public Object getClusterStatus() {
		return response(map(
				"running", clusteringService.isRunning(),
				"nodes", clusteringService.getClusterNodes().stream().map(n -> map("address", n.getAddress(), "nodeId", n.getNodeId(), "thisNode", n.isThisNode())).collect(toList()))
		);
	}

	@POST
	@Path("cache/drop")
	@Consumes(WILDCARD)
	public Object dropCache() {
		logger.info("drop system cache");
		cacheService.invalidateAll();
		return success();
	}

	@POST
	@Path("cache/{cacheId}/drop")
	@Consumes(WILDCARD)
	public Object dropCache(@PathParam("cacheId") String cacheId) {
		logger.info("drop cache = {}", cacheId);
		cacheService.invalidate(cacheId);
		return success();
	}

	@GET
	@Path("cache/stats")
	public Object getCacheStats() {
		Map<String, CmCacheStats> stats = cacheService.getStats();
		return success().with("data", list().accept((l) -> {
			stats.forEach((key, value) -> {
				l.add(map("name", key, "objectsCount", value.getSize(), "objectsSize", value.getEstimateMemSize(), "_objectsSize_description", FileUtils.byteCountToDisplaySize(value.getEstimateMemSize())));
			});
		}));
	}

	@POST
	@Path("stop")
	@Consumes(WILDCARD)
	public Object stopSystem() {
		logger.info("stop cmdbuild");
		platformService.stopContainer();
		return success();
	}

	@POST
	@Path("restart")
	@Consumes(WILDCARD)
	public Object restartSystem() {
		logger.info("restart cmdbuild");
		platformService.restartContainer();
		return success();
	}

	@POST
	@Path("upgrade")
	@Consumes(MULTIPART_FORM_DATA)
	public Object upgradeSystem(@Multipart(FILE) DataHandler dataHandler) {
		logger.info("upgrade cmdbuild");
		platformService.upgradeWebapp(CmdbuildIoUtils.toByteArray(dataHandler));
		return success();
	}

	/**
	 * drop all data collected by audit process (request tracking)
	 *
	 * this is mostly useful for debug/devel, or to clear data after we've
	 * disabled tracking
	 *
	 */
	@POST
	@Path("audit/drop")
	@Consumes(WILDCARD)

	public void dropAudit() {
		logger.info("drop audit data");
		requestTrackingService.dropAllData();
	}

	/**
	 * run cleanup process for data collected by audit process (request
	 * tracking); cleanup process will run as configured in
	 * {@link RequestTrackingConfiguration}
	 *
	 */
	@POST
	@Path("audit/cleanup")
	@Consumes(WILDCARD)

	public void cleanupAudit() {
		logger.info("cleanup audit data");
		requestTrackingService.cleanupRequestTable();
	}

	@GET
	@Path("patches")
	public Object getAllPatches() {
		return ImmutableMap.of("patches", patchManager.getAllPatches().stream()
				.sorted((a, b) -> ComparisonChain.start().compareFalseFirst(a.isApplied(), b.isApplied()).compare(firstNonNull(a.getApplyDate(), 0), firstNonNull(b.getApplyDate(), 0)).compare(b.getComparableVersion(), a.getComparableVersion()).result())
				.map((patch) -> serializePatchInfo(patch).accept((map) -> {
			map.put("applied", patch.isApplied());
			if (patch.isApplied()) {
				map.put("appliedOnDate", DateUtils.toIsoDateTime(patch.getApplyDate()));
			}
			if (!isBlank(patch.getHash())) {
				map.put("hash", patch.getHash());
			}
			List<String> warnings = Lists.newArrayList();
			if (patch.hashMismatch()) {
				warnings.add("hash mismatch: the hash on db does not match the hash on file");
			}
			if (patch.isApplied() && !patch.hasPatchOnFile()) {
				warnings.add("orphan patch: this patch does not exisit on file");
			}
			if (!warnings.isEmpty()) {
				map.put("warning", warnings);
			}
		})).collect(toList()));
	}

	/**
	 * return all active tenants
	 *
	 * @return
	 */
	@GET
	@Path("tenants")

	public List<Object> getAllTenants() {
		return multitenantService.getAllActiveTenants().stream().map((tenant) -> {
			Map map = newLinkedHashMap();
			map.put("id", tenant.getId());
			map.put("description", tenant.getDescription());
			return map;
		}).collect(toList());
	}

	@GET
	@Path("scheduler/status")
	public Object getSchedulerStatus() {
		return ImmutableMap.of("success", true, "data", ImmutableMap.of("status", schedulerService.isRunning() ? "RUNNING" : "STOPPED"));
	}

	@GET
	@Path("scheduler/jobs")
	public Object getSchedulerJobs() {
		return ImmutableMap.of("success", true, "data", schedulerService.getScheduledJobs().stream()
				.sorted(Ordering.natural().onResultOf(ScheduledJobInfo::getName))
				.map((job) -> ImmutableMap.of("name", job.getName(), "trigger", job.getTrigger())).collect(toList()));
	}

	@GET
	@Path("loggers")
	public Object getAllLoggers() {
		return ImmutableMap.of("success", true, "data", loggerConfigurationService.getAllLoggerConfig().stream()
				.sorted(Ordering.natural().onResultOf(LoggerConfig::getCategory))
				.map((item) -> ImmutableMap.of("category", item.getCategory(), "level", item.getLevel())).collect(toList()));
	}

	@POST
	@Path("loggers/{key}")
	@Consumes(TEXT_PLAIN)
	public void updateLoggerLevel(@PathParam("key") String loggerCategory, String loggerLevel) {
		loggerConfigurationService.updateLoggerConfig(new SimpleLoggerConfig(loggerCategory, loggerLevel));
	}

	@PUT
	@Path("loggers/{key}")
	@Consumes(TEXT_PLAIN)
	public void addLoggerLevel(@PathParam("key") String loggerCategory, String loggerLevel) {
		loggerConfigurationService.addLoggerConfig(new SimpleLoggerConfig(loggerCategory, loggerLevel));
	}

	@DELETE
	@Path("loggers/{key}")
	public void deleteLoggerLevel(@PathParam("key") String loggerCategory) {
		loggerConfigurationService.removeLoggerConfig(loggerCategory);
	}
//
//	@POST
//	@Path("dms/import")
//	@Consumes(WILDCARD)
//	public void importFromDms() {
//		logger.info("import documents from dms");
//		documentImportService.startImportFromDmsBatchProcess();
//	}

	@POST
	@Path("eval")
	@Consumes(APPLICATION_FORM_URLENCODED)
	public Object eval(@FormParam("script") String script) {
		//TODO move this to service

		new BeanshellScriptExecutor(script).execute(map());

		return map("success", true);
	}

	@GET
	@Path("database/dump")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler dumpDatabase() {
		File tempFile = tempFile();
		try {
			dumpService.dumpDatabaseToFile(tempFile);
			return new DataHandler(new ByteArrayDataSource(toByteArray(tempFile), APPLICATION_OCTET_STREAM) {
				{
					setName("dump.backup");
				}
			});
		} finally {
			deleteQuietly(tempFile);
		}
	}

	@GET
	@Path("debuginfo/download")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler generateDebugInfo() {
		return new DataHandler(bugreportService.generateBugReport());
	}

	@POST
	@Consumes(WILDCARD)
	@Path("debuginfo/send")
	public Object sendBugReport(@QueryParam("message") String message) {
		BugReportInfo debugInfo = bugreportService.sendBugReport(message);
		return response(map("fileName", debugInfo.getFileName()));
	}

}
