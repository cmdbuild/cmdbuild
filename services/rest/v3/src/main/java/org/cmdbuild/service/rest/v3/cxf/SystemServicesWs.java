package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.WILDCARD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.springframework.security.access.prepost.PreAuthorize;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_SYSTEM_ACCESS_AUTHORITY;
import org.cmdbuild.services.SystemService;
import static org.cmdbuild.services.SystemServiceStatusUtils.serializeSystemServiceStatus;
import org.cmdbuild.services.SystemServicesService;

@Component("v3_system_services")
@Path("system_services/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@PreAuthorize(HAS_SYSTEM_ACCESS_AUTHORITY)
public class SystemServicesWs {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SystemServicesService servicesStatusService;

	public SystemServicesWs(SystemServicesService servicesStatusService) {
		this.servicesStatusService = checkNotNull(servicesStatusService);
	}

	@GET
	@Path("")
	public Object getServicesStatus() {
		return response(servicesStatusService.getSystemServices().stream().sorted(Ordering.natural().onResultOf(SystemService::getServiceId)).map(this::serializeServiceStatus));
	}

	@GET
	@Path("{serviceId}")
	public Object getServiceStatus(@PathParam("serviceId") String serviceId) {
		return response(serializeServiceStatus(servicesStatusService.getSystemService(serviceId)));
	}

	@POST
	@Path("{serviceId}/start")
	@Consumes(WILDCARD)
	public Object startService(@PathParam("serviceId") String serviceId) {
		servicesStatusService.startSystemService(serviceId);
		return response(serializeServiceStatus(servicesStatusService.getSystemService(serviceId)));
	}

	@POST
	@Path("{serviceId}/stop")
	@Consumes(WILDCARD)
	public Object stopService(@PathParam("serviceId") String serviceId) {
		servicesStatusService.stopSystemService(serviceId);
		return response(serializeServiceStatus(servicesStatusService.getSystemService(serviceId)));
	}

	private Object serializeServiceStatus(SystemService systemService) {
		return map(
				"_id", systemService.getServiceId(),
				"name", systemService.getServiceName(),
				"status", serializeSystemServiceStatus(systemService.getServiceStatus()),
				"_can_start", systemService.canStart(),
				"_can_stop", systemService.canStop()
		);
	}

}
