/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.dao.entrytype.AttributeDefinition;
import org.cmdbuild.dao.entrytype.AttributeDefinitionImpl;
import static org.cmdbuild.dao.entrytype.DaoPermissionUtils.parseAttributePermissionMode;
import org.cmdbuild.dao.entrytype.attributetype.AttributeTypeName;
import org.cmdbuild.dao.entrytype.attributetype.AttributeUtils;
import org.cmdbuild.dao.entrytype.attributetype.DecimalAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.IpAddressAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.StringAttributeType;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.dao.entrytype.EntryType;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.service.rest.v3.cxf.serialization.AttributeTypeConversionService.TYPE_NAME_MAP;

@JsonDeserialize(builder = WsAttributeRequestData.WsAttributeRequestDataBuilder.class)//TODO change this, replace with new deserialization
public class WsAttributeRequestData {

	private final String type;
	private final String name;
	private final String description;
	private final boolean showInGrid;
	private final String domain;
	private final boolean unique;
	private final boolean mandatory;
	private final boolean active;
	private final Integer index;
	private final String defaultValue;
	private final String group;
	private final Integer precision;
	private final Integer scale;
	private final String targetClass;
	private final String targetType;
	private final Integer maxLength;
	private final String editorType;
	private final String lookupTypeName;
	private final String filter;
	private final String mode;
	private final Map<String, String> metadata;
	private final Integer classOrder;
	private final String ipType;

	private WsAttributeRequestData(WsAttributeRequestDataBuilder builder) {
		this.type = checkNotBlank(builder.type, "attr type cannote be null");
		this.name = checkNotBlank(builder.name, "attr name cannot be null");
		this.description = checkNotNull(builder.description, "attr description cannot be null");
		this.showInGrid = checkNotNull(builder.showInGrid, "attr showInGrid cannot be null");
		this.domain = (builder.domain);
		this.unique = checkNotNull(builder.unique, "attr unique cannot be null");
		this.mandatory = checkNotNull(builder.mandatory, "attr mandatory cannot be null");
		this.active = checkNotNull(builder.active, "attr active cannot be null");
		this.index = (builder.index);
		this.defaultValue = (builder.defaultValue);
		this.group = trimToNull(builder.group);
		this.precision = (builder.precision);
		this.scale = (builder.scale);
		this.targetClass = (builder.targetClass);
		this.targetType = (builder.targetType);
		this.maxLength = (builder.maxLength);
		this.editorType = (builder.editorType);
		this.lookupTypeName = (builder.lookupTypeName);
		this.filter = (builder.filter);
		this.mode = checkNotBlank(builder.mode, "attr mode cannot be null");
		this.metadata = builder.metadata == null ? emptyMap() : map(builder.metadata).immutable();
		this.classOrder = (builder.classOrder);
		this.ipType = (builder.ipType);
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean showInGrid() {
		return showInGrid;
	}

	public String getDomain() {
		return domain;
	}

	public boolean isUnique() {
		return unique;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public String getMode() {
		return mode;
	}

	public boolean isActive() {
		return active;
	}

	public Integer getIndex() {
		return index;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public String getGroup() {
		return group;
	}

	public Integer getPrecision() {
		return precision;
	}

	public Integer getScale() {
		return scale;
	}

	public String getTargetClass() {
		return targetClass;
	}

	public String getTargetType() {
		return targetType;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public String getEditorType() {
		return editorType;
	}

	public String getLookupType() {
		return lookupTypeName;
	}

	public String getFilter() {
		return filter;
	}

	public Map<String, String> getMetadata() {
		return metadata;
	}

	public Integer getClassOrder() {
		return classOrder;
	}

	public String getIpType() {
		return ipType;
	}

	public static WsAttributeRequestDataBuilder builder() {
		return new WsAttributeRequestDataBuilder();
	}

	public static WsAttributeRequestDataBuilder copyOf(WsAttributeRequestData source) {
		return new WsAttributeRequestDataBuilder()
				.withType(source.getType())
				.withName(source.getName())
				.withDescription(source.getDescription())
				.withShowInGrid(source.showInGrid())
				.withDomain(source.getDomain())
				.withUnique(source.isUnique())
				.withMandatory(source.isMandatory())
				.withMode(source.getMode())
				.withActive(source.isActive())
				.withIndex(source.getIndex())
				.withDefaultValue(source.getDefaultValue())
				.withGroup(source.getGroup())
				.withPrecision(source.getPrecision())
				.withScale(source.getScale())
				.withTargetClass(source.getTargetClass())
				.withTargetType(source.getTargetType())
				.withMaxLength(source.getMaxLength())
				.withEditorType(source.getEditorType())
				.withLookupType(source.getLookupType())
				.withFilter(source.getFilter())
				.withMetadata(source.getMetadata())
				.withClassOrder(source.getClassOrder())
				.withIpType(source.getIpType());
	}

	public static class WsAttributeRequestDataBuilder implements Builder<WsAttributeRequestData, WsAttributeRequestDataBuilder> {

		private String type;
		private String name;
		private String description;
		private Boolean showInGrid;
		private String domain;
		private Boolean unique;
		private Boolean mandatory;
		private Boolean active;
		private Integer index;
		private String defaultValue;
		private String group;
		private Integer precision;
		private Integer scale;
		private String targetClass;
		private String targetType;
		private Integer maxLength;
		private String editorType;
		private String lookupTypeName;
		private String filter;
		private String mode;
		private Map<String, String> metadata;
		private Integer classOrder;
		private String ipType;

		public WsAttributeRequestDataBuilder withType(String type) {
			this.type = type;
			return this;
		}

		public WsAttributeRequestDataBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public WsAttributeRequestDataBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public WsAttributeRequestDataBuilder withShowInGrid(Boolean showInGrid) {
			this.showInGrid = showInGrid;
			return this;
		}

		public WsAttributeRequestDataBuilder withDomain(String domainName) {
			this.domain = domainName;
			return this;
		}

		public WsAttributeRequestDataBuilder withUnique(Boolean unique) {
			this.unique = unique;
			return this;
		}

		public WsAttributeRequestDataBuilder withMandatory(Boolean mandatory) {
			this.mandatory = mandatory;
			return this;
		}

		public WsAttributeRequestDataBuilder withMode(String mode) {
			this.mode = mode;
			return this;
		}

		public WsAttributeRequestDataBuilder withActive(Boolean active) {
			this.active = active;
			return this;
		}

		public WsAttributeRequestDataBuilder withIndex(Integer index) {
			this.index = index;
			return this;
		}

		public WsAttributeRequestDataBuilder withDefaultValue(String defaultValue) {
			this.defaultValue = defaultValue;
			return this;
		}

		public WsAttributeRequestDataBuilder withGroup(String group) {
			this.group = group;
			return this;
		}

		public WsAttributeRequestDataBuilder withPrecision(Integer precision) {
			this.precision = precision;
			return this;
		}

		public WsAttributeRequestDataBuilder withScale(Integer scale) {
			this.scale = scale;
			return this;
		}

		public WsAttributeRequestDataBuilder withTargetClass(String targetClass) {
			this.targetClass = targetClass;
			return this;
		}

		public WsAttributeRequestDataBuilder withTargetType(String targetType) {
			this.targetType = targetType;
			return this;
		}

		public WsAttributeRequestDataBuilder withMaxLength(Integer length) {
			this.maxLength = length;
			return this;
		}

		public WsAttributeRequestDataBuilder withEditorType(String editorType) {
			this.editorType = editorType;
			return this;
		}

		public WsAttributeRequestDataBuilder withLookupType(String lookupTypeName) {
			this.lookupTypeName = lookupTypeName;
			return this;
		}

		public WsAttributeRequestDataBuilder withFilter(String filter) {
			this.filter = filter;
			return this;
		}

		public WsAttributeRequestDataBuilder withMetadata(Map<String, String> metadata) {
			this.metadata = metadata;
			return this;
		}

		public WsAttributeRequestDataBuilder withClassOrder(Integer classOrder) {
			this.classOrder = classOrder;
			return this;
		}

		public WsAttributeRequestDataBuilder withIpType(String ipType) {
			this.ipType = ipType;
			return this;
		}

		@Override
		public WsAttributeRequestData build() {
			return new WsAttributeRequestData(this);
		}

	}

	public AttributeDefinition toAttrDefinition(EntryType owner) {
		return AttributeDefinitionImpl.builder()
				.withOwner(owner)
				.withActive(isActive())
				.withClassOrder(getClassOrder())
				.withDefaultValue(getDefaultValue())
				.withDescription(getDescription())
				.withEditorType(getEditorType())
				.withFilter(getFilter())//TODO check this
				.withGroup(getGroup())
				.withIndex(getIndex() == null ? null : (getIndex()))
				.withMode(parseAttributePermissionMode(getMode()))
				.withName(getName())
				.withRequired(isMandatory())
				.withShowInGrid(showInGrid())
				.withTargetClass(getTargetClass())
				.withType(getAttrType())
				.withUnique(isUnique())
				.withMetadata(getMetadata())
				.build();
	}

	private CardAttributeType getAttrType() {
		AttributeTypeName attributeTypeName = checkNotNull(TYPE_NAME_MAP.inverse().get(getType()), "unknown attr type = %s", getType());
		switch (attributeTypeName) {
			case DECIMAL:
				return new DecimalAttributeType(getPrecision(), getScale());
			case FOREIGNKEY:
				return new ForeignKeyAttributeType(getTargetClass());
			case INET:
				return new IpAddressAttributeType(isBlank(getIpType()) ? IpAddressAttributeType.IpType.IPV4 : IpAddressAttributeType.IpType.valueOf(getIpType().toUpperCase()));
			case LOOKUP:
				return new LookupAttributeType(getLookupType());
			case REFERENCE:
				return new ReferenceAttributeType(getDomain());
			case STRING:
				return new StringAttributeType(getMaxLength());
			default:
				return AttributeUtils.newAttributeTypeByName(attributeTypeName);
		}
	}
}
