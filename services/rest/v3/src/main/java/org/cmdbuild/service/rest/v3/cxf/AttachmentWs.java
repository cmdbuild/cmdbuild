package org.cmdbuild.service.rest.v3.cxf;

import javax.activation.DataHandler;

import org.cmdbuild.service.rest.v3.model.Attachment;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import org.apache.commons.codec.binary.Base64;
import static org.apache.commons.io.IOUtils.toByteArray;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.apache.cxf.jaxrs.model.URITemplate;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.springframework.stereotype.Component;
import org.cmdbuild.dms.inner.DocumentInfoAndDetail;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.dms.DocumentDataImpl;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ATTACHMENT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ATTACHMENT_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CARD_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.service.rest.v3.model.Models.newAttachment;
import static org.cmdbuild.utils.date.DateUtils.toIsoDateTime;

@Component("v3_attachments")
//@Path("{a:(classes|processes)}/{" + CLASS_ID + "}/{b:(cards|instances)}/{" + CARD_ID + "}/attachments/")
//@Path("{a: classes|processes}/{" + CLASS_ID + "}/{b: cards|instances}/{" + CARD_ID + "}/attachments/")
@Path("{a:processes|classes}/{" + CLASS_ID + "}/{b:instances|cards}/{" + CARD_ID + "}/attachments/")

//	http://localhost:8080/cmdbuild/services/rest/v3/classes/InternalEmployee/cards/6083/attachments
//	http://localhost:8080/cmdbuild/services/rest/v3/processes/IncidentMgt/instances/100000009636/attachments
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class AttachmentWs {

	private final DmsService documentService;
	private final OperationUserSupplier userSupplier;

	public AttachmentWs(OperationUserSupplier userSupplier, DmsService documentService) {
		this.documentService = checkNotNull(documentService);
		this.userSupplier = checkNotNull(userSupplier);

//		URITemplate.class;
	}

	@POST
	@Path(EMPTY)
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	public Object create(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @Multipart(value = ATTACHMENT, required = false) @Nullable Attachment attachment, @Multipart(FILE) DataHandler dataHandler) throws IOException {
		checkCanRead(classId, cardId);
		DocumentInfoAndDetail document = documentService.create(classId, cardId, DocumentDataImpl.builder()
				.withAuthor(userSupplier.getUser().getLoginUser().getUsername())
				.withData(dataHandler.getInputStream())
				.withFilename(dataHandler.getName())
				.accept((b) -> {
					if (attachment != null) {
						b.withCategory(attachment.getCategory()).withDescription(attachment.getDescription());
					} else {
						b.withDescription("");
					}
				})
				.withMajorVersion(true)
				.build());
		return response(serializeAttachment(document));
	}

	@GET
	@Path(EMPTY)
	public Object read(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId) {
		checkCanRead(classId, cardId);
		List<DocumentInfoAndDetail> list = documentService.getCardAttachments(classId, cardId);
		List<Attachment> attachments = list.stream().map(this::serializeAttachment).collect(toList());
		return response(attachments);
	}

	@GET
	@Path("{" + ATTACHMENT_ID + "}/")
	public Object read(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(ATTACHMENT_ID) String attachmentId) {
		checkCanRead(classId, cardId, attachmentId);
		DocumentInfoAndDetail document = documentService.getCardAttachmentById(classId, cardId, attachmentId);
		return response(serializeAttachment(document));
	}

	@GET
	@Path("{" + ATTACHMENT_ID + "}/{file: [^/]+}")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler download(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(ATTACHMENT_ID) String attachmentId) {
		checkCanRead(classId, cardId);
		return documentService.download(attachmentId);//TODO permission check
	}

	@GET
	@Path("{" + ATTACHMENT_ID + "}/preview")
	public Object preview(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(ATTACHMENT_ID) String attachmentId) {
		checkCanRead(classId, cardId);
		Optional<DataHandler> preview = documentService.preview(attachmentId);//TODO permission check
		return map("success", true, "data", map().accept((map) -> {
			map.put("hasPreview", preview.isPresent());
			if (preview.isPresent()) {
				map.put("dataUrl", toDataUrl(preview.get()));
			}
		}));
	}

	private String toDataUrl(DataHandler dataHandler) {
		try {
			StringBuilder dataUrl = new StringBuilder("data:");
			String mediaType = dataHandler.getContentType();
			dataUrl.append(mediaType);
			dataUrl.append(";base64,");
			String base64payload = Base64.encodeBase64String(toByteArray(dataHandler.getInputStream()));
			dataUrl.append(base64payload);
			return dataUrl.toString();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@PUT
	@Path("{" + ATTACHMENT_ID + "}/")
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	public Object update(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(ATTACHMENT_ID) String attachmentId, @Nullable @Multipart(value = ATTACHMENT, required = false) Attachment attachment, @Nullable @Multipart(value = FILE, required = false) DataHandler dataHandler) {
		checkCanRead(classId, cardId);
		DocumentInfoAndDetail document = documentService.updateDocumentWithAttachmentId(classId, cardId, attachmentId, DocumentDataImpl.builder()
				.withAuthor(userSupplier.getUser().getLoginUser().getUsername())
				.accept((b) -> {
					if (attachment != null) {
						b.withCategory(attachment.getCategory()).withDescription(attachment.getDescription());
					} else {
						b.withDescription("");
					}
				})
				.withData(dataHandler)
				.withMajorVersion(true)
				.build());
		return response(serializeAttachment(document));
	}

	@DELETE
	@Path("{" + ATTACHMENT_ID + "}/")
	public Object delete(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(ATTACHMENT_ID) String attachmentId) {
		checkCanRead(classId, cardId);
		documentService.delete(attachmentId);//TODO permission check
		return success();
	}

	@GET
	@Path("{" + ATTACHMENT_ID + "}/history")
	public Object getAttachmentHistory(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(ATTACHMENT_ID) String attachmentId) {
		checkCanRead(classId, cardId, attachmentId);
		List<DocumentInfoAndDetail> versions = documentService.getCardAttachmentVersions(classId, cardId, attachmentId);
		List<Attachment> attachments = versions.stream().map(this::serializeAttachment).collect(toList());
		return response(attachments);
	}

	@GET
	@Path("{" + ATTACHMENT_ID + "}/history/{version}/{file: [^/]+}")
	@Produces(APPLICATION_OCTET_STREAM)
	public DataHandler downloadPreviousVersion(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(ATTACHMENT_ID) String attachmentId, @PathParam("version") String versionId) {
		checkCanRead(classId, cardId, attachmentId);
		return documentService.download(attachmentId, versionId);//TODO permission check
	}

	private void checkCanRead(String classId, Long cardId) {//TODO
//		Classe targetClass = dataAccessLogic.findClass(classId);
//		if (targetClass == null) {
//			errorHandler.classNotFound(classId);
//		}
//		try {
//			dataAccessLogic.fetchCMCard(classId, cardId);
//		} catch (NoSuchElementException e) {
//			errorHandler.cardNotFound(cardId);
//		}
	}

	private void checkCanRead(String classId, Long cardId, String attachmentId) {//TODO
//		Classe targetClass = dataAccessLogic.findClass(classId);
//		if (targetClass == null) {
//			errorHandler.classNotFound(classId);
//		}
//		try {
//			dataAccessLogic.fetchCMCard(classId, cardId);
//		} catch (NoSuchElementException e) {
//			errorHandler.cardNotFound(cardId);
//		}
//		if (isBlank(attachmentId)) {
//			errorHandler.missingAttachmentId();
//		}
	}

	private Attachment serializeAttachment(DocumentInfoAndDetail input) {
		return newAttachment() //
				.withId(input.getDocumentId()) //
				.withName(input.getFileName()) //
				.withCategory(input.getCategory()) //
				.withDescription(input.getDescription()) //
				.withVersion(input.getVersion()) //
				.withAuthor(input.getAuthor()) //
				.withCreated(toIsoDateTime(input.getCreated())) //
				.withModified(toIsoDateTime(input.getModified())) //
				//				.withMetadata(metadata(input.getMetadata())) //
				.build();
	}

}
