package org.cmdbuild.service.rest.v3.model;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CODE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESCRIPTION;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CodeDescriptionRecord {

	private String code;
	private String description;

	CodeDescriptionRecord() {
		// package visibility
	}

	@XmlAttribute(name = CODE)
	public String getCode() {
		return code;
	}

	void setCode(final String code) {
		this.code = code;
	}

	@XmlAttribute(name = DESCRIPTION)
	public String getDescription() {
		return description;
	}

	void setDescription(final String description) {
		this.description = description;
	}

}
