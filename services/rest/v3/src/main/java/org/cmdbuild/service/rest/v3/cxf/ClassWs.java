package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Functions.compose;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.cmdbuild.classe.ExtendedClass;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Classe;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ACTIVE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.cmdbuild.service.rest.v3.cxf.serialization.ClassSerializationHelper;
import org.cmdbuild.service.rest.v3.cxf.serialization.ClassSerializationHelper.WsClassData;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DETAILED;
import org.cmdbuild.classe.UserClassService;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;

@Component("v3_classes")
@Path("{a:classes}/")
@Produces(APPLICATION_JSON)
public class ClassWs {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final UserClassService classService;
	private final ClassSerializationHelper helper;

	public ClassWs(UserClassService classService, ClassSerializationHelper helper) {
		this.classService = checkNotNull(classService);
		this.helper = checkNotNull(helper);
	}

	@GET
	@Path(EMPTY)
	public Object readAll(@QueryParam(ACTIVE) Boolean activeOnly, @QueryParam(DETAILED) boolean detailed, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {
		List<Classe> all = defaultIfNull(activeOnly, true) ? classService.getActiveUserClasses() : classService.getAllUserClasses();
		List res = paged(all, offset, limit).elements().stream()
				.map(detailed ? compose(helper::buildFullDetailExtendedResponse, classService::getExtendedClass) : helper::buildBasicResponse)
				.collect(toList());
		return response(res, all.size());
	}

	@GET
	@Path("{classId}/")
	public Object read(@PathParam("classId") String classId) {
		ExtendedClass classe = classService.getExtendedUserClass(classId);
		return buildResponse(classe);
	}

	@POST
	@Path(EMPTY)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object create(WsClassData data) {
		logger.debug("create classe with data = {}", data);
		ExtendedClass classe = classService.createClass(helper.extendedClassDefinitionForNewClass(data));
		return buildResponse(classe);
	}

	@PUT
	@Path("{classId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam("classId") String classId, WsClassData data) {
		logger.debug("update classe = {} with data = {}", classId, data);
		ExtendedClass classe = classService.updateClass(helper.extendedClassDefinitionForExistingClass(classId, data));
		return buildResponse(classe);
	}

	@DELETE
	@Path("{classId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object delete(@PathParam("classId") String classId) {
		classService.deleteClass(classId);
		return success();
	}

	private Object buildResponse(ExtendedClass classe) {
		return response(helper.buildFullDetailExtendedResponse(classe));
	}

}
