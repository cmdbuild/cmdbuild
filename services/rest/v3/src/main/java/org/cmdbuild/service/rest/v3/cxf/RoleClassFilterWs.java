package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;
import org.cmdbuild.auth.role.Role;
import org.cmdbuild.auth.role.RoleRepository;
import org.cmdbuild.cardfilter.CardFilter;
import org.cmdbuild.cardfilter.CardFilterService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component("v3_rolesClassFilters")
@Path("roles/{roleId}/filters")
@Produces(APPLICATION_JSON)
@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
public class RoleClassFilterWs {

	private final RoleRepository roleRepository;
	private final CardFilterService filterService;

	public RoleClassFilterWs(RoleRepository roleRepository, CardFilterService filterService) {
		this.roleRepository = checkNotNull(roleRepository);
		this.filterService = checkNotNull(filterService);
	}

	@GET
	@Path("")
	public Object read(@PathParam("roleId") String roleId) {
		Role role = roleRepository.getByNameOrId(roleId);
		List<CardFilter> filters = filterService.getFiltersForRole(role.getId());
		return response(filters.stream().map(ClassFilterWs::serializeFilter).collect(toList()));
	}

	@POST
	@Path("")
	public Object updateWithPost(@PathParam("roleId") String roleId, List<WsFilterIdData> filters) {
		return doUpdate(roleId, filters);
	}

	@PUT
	@Path("")
	public Object updateWithPut(@PathParam("roleId") String roleId, List<WsFilterIdData> filters) {
		return doUpdate(roleId, filters);
	}

	private Object doUpdate(String roleId, List<WsFilterIdData> filters) {
		Role role = roleRepository.getByNameOrId(roleId);
		Set<Long> filterIds = filters.stream().map(WsFilterIdData::getId).collect(toSet());
		filterService.setFiltersForRole(role.getId(), filterIds);
		return read(roleId);
	}

	public static class WsFilterIdData {

		private final long id;

		public WsFilterIdData(@JsonProperty("_id") Long id) {
			this.id = id;
		}

		public long getId() {
			return id;
		}

	}
}
