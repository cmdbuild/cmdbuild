/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import java.util.Collection;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import org.cmdbuild.bim.BimLayer;
import org.cmdbuild.bim.BimLayerRepository;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.springframework.stereotype.Component;

@Component("v3_bim_layers")
@Path("bim/layers/")
@Produces(APPLICATION_JSON)
public class BimLayerWs {

	private final BimLayerRepository layerRepository;

	public BimLayerWs(BimLayerRepository layerRepository) {
		this.layerRepository = checkNotNull(layerRepository);
	}

	@GET
	@Path("")
	public Object getAll() {
		Collection<BimLayer> layers = layerRepository.getAll();
		return response(layers.stream().sorted(Ordering.natural().onResultOf(BimLayer::getOwnerClassId)).map((l) -> map(
				"_id", l.getId(),
				"owner", l.getOwnerClassId(),
				"active", l.isActive(),
				"root", l.isRoot()).skipNullValues().with(
				"rootReferenceAttr", l.getRootReference()
		)));
	}

}
