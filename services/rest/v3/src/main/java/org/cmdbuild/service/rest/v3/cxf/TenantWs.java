package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.auth.multitenant.api.MultitenantService;
import org.cmdbuild.auth.multitenant.api.TenantInfo;
import org.cmdbuild.common.utils.PagedElements;
import static org.cmdbuild.common.utils.PagedElements.isPaged;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;
import org.cmdbuild.auth.multitenant.config.MultitenantConfiguration;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MULTITENANT_CONFIG_PROPERTY_MODE;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MULTITENANT_CONFIG_PROPERTY_TENANT_CLASS;
import org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MultitenantMode;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MultitenantMode.DB_FUNCTION;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.parseEnum;

@Component("v3_tenant")
@Path("tenants/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class TenantWs {

	private final MultitenantConfiguration configuration;
	private final MultitenantService service;

	public TenantWs(MultitenantConfiguration configuration, MultitenantService service) {
		this.configuration = checkNotNull(configuration);
		this.service = checkNotNull(service);
	}

	@GET
	@Path("")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object getAll(@Nullable @QueryParam(LIMIT) Integer limit, @Nullable @QueryParam(START) Integer offset) {
		checkArgument(configuration.isMultitenantEnabled(), "multitenant is not enabled");
		List<TenantInfo> list = service.getAllActiveTenants();
		int total;
		if (isPaged(offset, limit)) {
			PagedElements<TenantInfo> paged = PagedElements.paged(list, offset, limit);
			total = paged.totalSize();
			list = paged.elements();
		} else {
			total = list.size();
		}
		return response(list.stream().map((t) -> map("_id", t.getId(), "description", t.getDescription())).collect(toList()), total);
	}

	@POST
	@Path("configure")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object configureMultitenant(WsTenantConfig configData) {
		switch (configData.multitenantMode) {
			case DISABLED:
				service.disableMultitenant();
				return success();
			case CMDBUILD_CLASS:
				service.enableMultitenantClassMode(configData.tenantClass);
				return success();
			case DB_FUNCTION:
				service.enableMultitenantFunctionMode();
				return success();
			default:
				throw new IllegalArgumentException("unsupported multitenant mode = " + configData.multitenantMode);
		}
	}

	public static class WsTenantConfig {

		public final MultitenantMode multitenantMode;
		public final String tenantClass;

		public WsTenantConfig(
				@JsonProperty(MULTITENANT_CONFIG_PROPERTY_MODE) String multitenantMode,
				@JsonProperty(MULTITENANT_CONFIG_PROPERTY_TENANT_CLASS) String tenantClass) {
			this.multitenantMode = parseEnum(multitenantMode, MultitenantMode.class);
			this.tenantClass = trimToNull(tenantClass);
		}

	}

}
