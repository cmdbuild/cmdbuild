package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static java.util.stream.Collectors.toList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.auth.userrole.UserRole;
import org.cmdbuild.auth.role.RoleRepository;
import org.cmdbuild.auth.user.UserData;
import org.cmdbuild.auth.user.UserDataImpl;
import org.cmdbuild.auth.user.UserRepository;
import org.cmdbuild.auth.multitenant.api.MultitenantService;
import org.cmdbuild.auth.multitenant.api.UserAvailableTenantContext;
import org.springframework.stereotype.Component;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SORT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.utils.CmdbFilterUtils;
import org.cmdbuild.data.filter.utils.CmdbSorterUtils;
import org.cmdbuild.data.filter.CmdbSorter;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DETAILED;
import org.cmdbuild.utils.crypto.legacy.digest.LegacyPasswordUtils;
import static org.cmdbuild.utils.date.DateUtils.toIsoDateTime;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBooleanOrDefault;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;
import org.cmdbuild.userconfig.UserConfigService;
import static org.cmdbuild.userconfig.UserConfigService.USER_CONFIG_KEY_MULTIGROUP;

@Component("v3_users")
@Path("users/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class UserWs {

	private final UserRepository repository;
	private final MultitenantService multitenantService;
	private final RoleRepository groupRepository;
	private final UserConfigService userPreferencesStore;

	public UserWs(UserRepository repository, MultitenantService multitenantService, RoleRepository groupRepository, UserConfigService userPreferencesStore) {
		this.repository = checkNotNull(repository);
		this.multitenantService = checkNotNull(multitenantService);
		this.groupRepository = checkNotNull(groupRepository);
		this.userPreferencesStore = checkNotNull(userPreferencesStore);
	}

	@GET
	@Path(EMPTY)
	public Object readMany(@QueryParam(FILTER) String filterStr, @QueryParam(SORT) String sort, @QueryParam(LIMIT) Long limit, @QueryParam(START) Long offset, @QueryParam(DETAILED) Boolean detailed) {
		CmdbFilter filter = CmdbFilterUtils.fromNullableJson(filterStr);
		CmdbSorter sorter = CmdbSorterUtils.fromNullableJson(sort);
		return response(repository.getMany(filter, sorter, offset, limit).map(defaultIfNull(detailed, false) ? this::serializeDetailedUser : this::serializeUser));
	}

	@GET
	@Path("{userId}/")
	public Object readOne(@PathParam("userId") Long id) {
		UserData user = repository.get(id);
		return response(serializeDetailedUser(user));
	}

	@POST
	@Path(EMPTY)
	public Object create(WsUserData data) {
		UserData user = data.toUserData().build();
		user = repository.create(user);
		updatePrefs(user.getUsername(), data);
		updateRoles(user, data);
		return response(serializeDetailedUser(user));
	}

	@PUT
	@Path("{userId}/")
	public Object update(@PathParam("userId") Long id, WsUserData data) {
		UserData user = data.toUserData().withId(id).build();
		user = repository.update(user);
		updatePrefs(user.getUsername(), data);
		updateRoles(user, data);
		return response(serializeDetailedUser(user));
	}

	private void updatePrefs(String username, WsUserData data) {
		userPreferencesStore.setByUsernameDeleteIfNull(username, "cm_user_initialPage", trimToNull(data.initialPage));
		userPreferencesStore.setByUsernameDeleteIfNull(username, "cm_user_multiTenant", toStringOrNull(data.multiTenant));
		userPreferencesStore.setByUsernameDeleteIfNull(username, USER_CONFIG_KEY_MULTIGROUP, toStringOrNull(data.multiGroup));
	}

	private void updateRoles(UserData user, WsUserData data) {
		groupRepository.setUserGroups(user.getId(), data.userGroups.stream().map(WsRoleOrTenantData::getId).collect(toList()), data.defaultRole);
	}

	private Object serializeDetailedUser(UserData user) {
		UserAvailableTenantContext tenantContext = multitenantService.getAvailableTenantContextForUser(user.getId());
		List<UserRole> userGroups = groupRepository.getUserGroups(user.getId());
		List roles = userGroups.stream().map(UserRole::getRole).map((r) -> map(
				"_id", r.getId(),
				"name", r.getName(),
				"description", r.getDescription(),
				"_description_translation", r.getDescription()//TODO
		)).collect(toList());
		UserRole defaultGroup = userGroups.stream().filter(UserRole::isDefault).collect(toOptional()).orElse(null);
		List tenants = multitenantService.getAvailableUserTenants(tenantContext).stream().map((t) -> map(
				"_id", t.getId(),
				"name", t.getDescription(),
				"description", t.getDescription(),
				"_description_translation", t.getDescription()//TODO
		)).collect(toList());
		Map<String, String> prefs = userPreferencesStore.getByUsername(user.getUsername());
		return serializeUser(user).with(
				"userTenants", tenants,
				"defaultUserTenant", tenantContext.getDefaultTenantId(),
				"userGroups", roles,
				"defaultUserGroup", Optional.ofNullable(defaultGroup).map(UserRole::getId).orElse(null),
				"_defaultUserGroup_description", Optional.ofNullable(defaultGroup).map(UserRole::getDescription).orElse(null),
				"language", prefs.get("cm_user_language"),
				"initialPage", prefs.get("cm_user_initialPage"),
				"multiTenant", toBooleanOrDefault(prefs.get("cm_user_multiTenant"), false),
				"multiGroup", toBooleanOrDefault(prefs.get(USER_CONFIG_KEY_MULTIGROUP), false));
	}

	private FluentMap<String, Object> serializeUser(UserData user) {
		return serializeMinimalUser(user).with(
				"email", user.getEmail(),
				"active", user.isActive(),
				"service", user.isService(),
				"passwordExpiration", toIsoDateTime(user.getPasswordExpiration()),
				"lastPasswordChange", toIsoDateTime(user.getLastPasswordChange()),
				"lastExpiringNotification", toIsoDateTime(user.getLastExpiringNotification())
		);
	}

	public static FluentMap<String, Object> serializeMinimalUser(UserData user) {
		return map(
				"_id", user.getId(),
				"username", user.getUsername(),
				"description", user.getDescription()
		);
	}

	public static class WsRoleOrTenantData {

		private final long id;

		public WsRoleOrTenantData(@JsonProperty("_id") Long id) {
			this.id = id;
		}

		public long getId() {
			return id;
		}

	}

	public static class WsUserData {

		private final Long id, defaultRole;
		private final String username, description, email, password, initialPage;
		private final ZonedDateTime passwordExpiration, lastPasswordChange, lastExpiringNotification;
		private final Boolean isActive, isService, multiTenant, multiGroup;
		private final List<WsRoleOrTenantData> userTenants, userGroups;

		public WsUserData(@JsonProperty("_id") Long id,
				@JsonProperty("username") String username,
				@JsonProperty("description") String description,
				@JsonProperty("email") String email,
				@JsonProperty("password") String password,
				@JsonProperty("initialPage") String initialPage,
				@JsonProperty("passwordExpiration") ZonedDateTime passwordExpiration,
				@JsonProperty("lastPasswordChange") ZonedDateTime lastPasswordChange,
				@JsonProperty("lastExpiringNotification") ZonedDateTime lastExpiringNotification,
				@JsonProperty("active") Boolean isActive,
				@JsonProperty("service") Boolean isService,
				@JsonProperty("multiGroup") Boolean multiGroup,
				@JsonProperty("multiTenant") Boolean multiTenant,
				@JsonProperty("defaultUserGroup") Long defaultRole,
				@JsonProperty("userTenants") List<WsRoleOrTenantData> userTenants,
				@JsonProperty("userGroups") List<WsRoleOrTenantData> userGroups) {
			this.id = id;
			this.username = checkNotBlank(username);
			this.password = trimToNull(password);
			this.description = description;
			this.email = email;
			this.passwordExpiration = passwordExpiration;
			this.lastPasswordChange = lastPasswordChange;
			this.lastExpiringNotification = lastExpiringNotification;
			this.isActive = isActive;
			this.isService = isService;
			this.initialPage = initialPage;
			this.multiGroup = multiGroup;
			this.multiTenant = multiTenant;
			this.userTenants = userTenants;
			this.userGroups = checkNotNull(userGroups);
			this.defaultRole = defaultRole;
		}

		private UserDataImpl.UserDataImplBuilder toUserData() {
			return UserDataImpl.builder()
					.withId(id)
					.withUsername(username)
					.withDescription(description)
					.withEmail(email)
					.withPassword(isBlank(password) ? null : LegacyPasswordUtils.encrypt(password))
					.withPasswordExpiration(passwordExpiration)
					.withLastPasswordChange(lastPasswordChange)
					.withLastExpiringNotification(lastExpiringNotification)
					.withActive(isActive)
					.withService(isService);
		}

	}

}
