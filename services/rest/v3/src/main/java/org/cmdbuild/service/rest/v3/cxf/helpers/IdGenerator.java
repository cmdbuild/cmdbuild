package org.cmdbuild.service.rest.v3.cxf.helpers;

public interface IdGenerator {

	Long generate();

	boolean isGenerated(Long id);

}
