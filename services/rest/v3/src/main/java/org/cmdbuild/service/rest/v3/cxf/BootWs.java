package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.WILDCARD;
import org.cmdbuild.dao.ConfigurableDataSource;
import org.cmdbuild.dao.config.inner.PatchManager;
import org.cmdbuild.dao.config.inner.Patch;
import org.cmdbuild.startup.BootService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.dao.driver.postgres.utils.JdbcUtils.doTestConnection;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.service.rest.v3.cxf.util.WsSerializationUtils;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component("v3_boot")
@Path("boot/")
@Produces(APPLICATION_JSON)
public class BootWs {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final BootService bootService;
	private final ConfigurableDataSource dataSource;
	private final PatchManager patchManager;

	public BootWs(BootService bootService, ConfigurableDataSource dataSource, PatchManager patchManager) {
		this.bootService = checkNotNull(bootService);
		this.dataSource = checkNotNull(dataSource);
		this.patchManager = checkNotNull(patchManager);
	}

	@GET
	@Path("status")
	public Object status() {
		Map map = map(success());
		map.put("status", bootService.getSystemStatus().name());
		if (bootService.isWaitingForUser()) {
			if (!dataSource.isConfigured()) {
				map.put("operationRequired", "configureDatasource");
			} else if (!patchManager.isUpdated()) {
				map.put("operationRequired", "applyPatch");
			}
		}
		return map;
	}

	@POST
	@Path("database/test")
	public Object testConnection(@FormParam("host") String host, @FormParam("port") Integer port, @FormParam("user") String user, @FormParam("password") String password) {
		try {
			doTestConnection(host, port, user, password);
			return success();
		} catch (Exception ex) {
			throw runtime(ex, "test connection failed");
		}
	}

	@GET
	@Path("patches")
	public Object getPendingPatches() {
		List<Patch> patches = patchManager.getAvailableCorePatches();
		return response(patches.stream().map(WsSerializationUtils::serializePatchInfo));
	}

	@POST
	@Path("patches/apply")
	@Consumes(WILDCARD)
	public Object applyPendingPatches() {
		logger.info("applyPendingPatches");
		patchManager.applyPendingPatches();
		return success();
	}

}
