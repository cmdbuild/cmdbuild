/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Map;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.cxf.jaxrs.model.ClassResourceInfo;
import org.apache.cxf.jaxrs.model.OperationResourceInfo;
import org.apache.cxf.jaxrs.model.doc.DocumentationProvider;
import org.cmdbuild.cache.CacheService;
import static org.cmdbuild.cache.CacheUtils.key;
import org.cmdbuild.cache.CmdbCache;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.readToString;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;
import org.springframework.stereotype.Component;

@Component("wadlDocumentationProvider")
public class WadlDocumentationProvider implements DocumentationProvider {

	private final CmdbCache<String, DocFile> docs;

	public WadlDocumentationProvider(CacheService cacheService) {
		this.docs = cacheService.newCache("wadl_docs");
	}

	@Override
	public String getClassDoc(ClassResourceInfo cri) {
		return getDocFileForPackage(cri.getServiceClass().getPackage().getName()).getClassDoc(cri.getServiceClass().getSimpleName());
	}

	@Override
	public String getMethodDoc(OperationResourceInfo ori) {
		Class<?> classe = ori.getAnnotatedMethod().getDeclaringClass();
		return getDocFileForPackage(classe.getPackage().getName()).getMethodDoc(classe.getSimpleName(), ori.getAnnotatedMethod().getName());
	}

	@Override
	public String getMethodResponseDoc(OperationResourceInfo ori) {
		return "";
	}

	@Override
	public String getMethodParameterDoc(OperationResourceInfo ori, int paramIndex) {
		return "";
	}

	private DocFile getDocFileForPackage(String pack) {
		return docs.get(pack, () -> loadDocFileForPackage(pack));
	}

	private DocFile loadDocFileForPackage(String pack) {
		InputStream inputStream = getClass().getResourceAsStream("/" + pack.replaceAll("[.]", "/") + "/wadl_docs.txt");
		String data;
		if (inputStream == null) {
			data = "";
		} else {
			data = readToString(inputStream);
		}
		return new DocFile(data);
	}

	private class DocFile {

		private final Map<String, String> classDocs = map();
		private final Map<String, String> methodDocs = map();

		private DocFile(String data) {
			parseData(data);
		}

		public String getClassDoc(String simpleClassName) {
			return firstNonNull(classDocs.get(simpleClassName), "");
		}

		public String getMethodDoc(String simpleClassName, String methodName) {
			return firstNonNull(methodDocs.get(key(simpleClassName, methodName)), "");
		}

		private String currentClass = null;
		private String currentMethod = null;
		private String currentDoc = "";

		private void parseData(String data) {
			String line;
			try (BufferedReader reader = new BufferedReader(new StringReader(data))) {
				while ((line = reader.readLine()) != null) {
					parseLine(line);
				}
				closeBlock();
			} catch (IOException ex) {
				throw runtime(ex);
			}
		}

		private void parseLine(String line) {
			if (line.startsWith("**")) {
				closeBlock();
				checkNotBlank(currentClass);
				currentMethod = trimAndCheckNotBlank(line.replaceFirst("^[*][*]", ""));
			} else if (line.startsWith("*")) {
				closeBlock();
				currentClass = trimAndCheckNotBlank(line.replaceFirst("^[*]", ""));
			} else {
				currentDoc += line + "\n";
			}
		}

		private void closeBlock() {
			if (isNotBlank(currentDoc)) {
				if (isNotBlank(currentMethod)) {
					methodDocs.put(key(currentClass, currentMethod), currentDoc);
				} else if (isNotBlank(currentClass)) {
					classDocs.put(currentClass, currentDoc);
				}
				currentDoc = "";
			}
		}

	}
}
