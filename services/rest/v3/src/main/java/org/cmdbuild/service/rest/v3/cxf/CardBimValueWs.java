/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import org.cmdbuild.bim.BimObject;
import org.cmdbuild.bim.BimService;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CARD_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.springframework.stereotype.Component;

@Component("v3_cards_bim_objects")
@Path("{a:processes|classes}/{" + CLASS_ID + "}/{b:cards|instances}/{" + CARD_ID + "}/bimvalue")
@Produces(APPLICATION_JSON)
public class CardBimValueWs {

	private final DaoService dao;
	private final BimService bimService;

	public CardBimValueWs(DaoService dao, BimService bimService) {
		this.dao = checkNotNull(dao);
		this.bimService = checkNotNull(bimService);
	}

	@GET
	@Path("")
	public Object getAllForCard(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @QueryParam("if_exists") boolean checkIfExists) {
		Card card = dao.getCard(classId, cardId);
		BimObject bimObject;
		if (checkIfExists) {
			bimObject = bimService.getBimObjectForCardOrNull(card);
			if (bimObject == null) {
				return response(map("exists", false));
			}
		} else {
			bimObject = bimService.getBimObjectForCard(card);
		}
		return response(map(
				"_id", bimObject.getId(),
				"projectId", bimObject.getPoid(),
				"globalId", bimObject.getGlobalId()
		).accept((m) -> {
			if (checkIfExists) {
				m.put("exists", true);
			}
		}));
	}

}
