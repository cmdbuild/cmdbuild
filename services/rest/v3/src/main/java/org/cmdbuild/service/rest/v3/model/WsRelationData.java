package org.cmdbuild.service.rest.v3.model;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESTINATION;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SOURCE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.VALUES;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.cmdbuild.service.rest.v3.model.adapter.RelationAdapter;

@XmlRootElement
@XmlJavaTypeAdapter(RelationAdapter.class)
public class WsRelationData extends AbstractCardModel {

	private WsCard source;
	private WsCard destination;
	private Values values;

	WsRelationData() {
		// package visibility
	}

	@XmlElement(name = SOURCE)
	public WsCard getSource() {
		return source;
	}

	public String getSourceClassId() {
		return getSource().getType();
	}

	public Long getSourceCardId() {
		return getSource().getId();
	}

	void setSource(final WsCard source) {
		this.source = source;
	}

	@XmlElement(name = DESTINATION)
	public WsCard getDestination() {
		return destination;
	}

	public String getDestinationClassId() {
		return getDestination().getType();
	}

	public Long getDestinationCardId() {
		return getDestination().getId();
	}

	void setDestination(final WsCard destination) {
		this.destination = destination;
	}

	@XmlElement(name = VALUES)
	public Values getValues() {
		return values;
	}

	void setValues(final Values values) {
		this.values = values;
	}

	@Override
	protected boolean doEquals(final Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WsRelationData)) {
			return false;
		}

		final WsRelationData other = WsRelationData.class.cast(obj);
		return new EqualsBuilder() //
				.append(this.getType(), other.getType()) //
				.append(this.getId(), other.getId()) //
				.append(this.source, other.source) //
				.append(this.destination, other.destination) //
				.append(this.values, other.values) //
				.isEquals();
	}

	@Override
	protected int doHashCode() {
		return new HashCodeBuilder() //
				.append(getType()) //
				.append(getId()) //
				.append(this.source) //
				.append(this.destination) //
				.append(values) //
				.toHashCode();
	}

}
