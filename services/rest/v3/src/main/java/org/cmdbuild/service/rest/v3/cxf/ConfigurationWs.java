package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import org.cmdbuild.auth.multitenant.config.MultitenantConfiguration;
import org.cmdbuild.config.BimserverConfiguration;
import org.cmdbuild.config.CoreConfiguration;
import org.cmdbuild.config.GisConfiguration;
import org.cmdbuild.config.GraphConfiguration;
import org.cmdbuild.workflow.WorkflowConfiguration;
import org.cmdbuild.dms.DmsConfiguration;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component("v3_configuration")
@Path("configuration/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class ConfigurationWs {

	private final DmsConfiguration dmsConfiguration;
	private final MultitenantConfiguration multitenantConfiguration;
	private final WorkflowConfiguration workflowConfig;
	private final GisConfiguration gisConfiguration;
	private final BimserverConfiguration bimConfiguration;
	private final CoreConfiguration coreConfiguration;
	private final GraphConfiguration graphConfiguration;

	public ConfigurationWs(DmsConfiguration dmsConfiguration, MultitenantConfiguration multitenantConfiguration, WorkflowConfiguration workflowConfig, GisConfiguration gisConfiguration, BimserverConfiguration bimConfiguration, CoreConfiguration coreConfiguration, GraphConfiguration graphConfiguration) {
		this.dmsConfiguration = checkNotNull(dmsConfiguration);
		this.multitenantConfiguration = checkNotNull(multitenantConfiguration);
		this.workflowConfig = checkNotNull(workflowConfig);
		this.gisConfiguration = checkNotNull(gisConfiguration);
		this.bimConfiguration = checkNotNull(bimConfiguration);
		this.coreConfiguration = checkNotNull(coreConfiguration);
		this.graphConfiguration = checkNotNull(graphConfiguration);
	}

	@GET
	@Path("public")
	public Object getPublicConfig() {
		return response(getPublicConfigData());
	}

	@GET
	@Path("system")
	public Object getSystemConfig() {
		return response(getPublicConfigData().with(
				"cm_system_dms_enabled", dmsConfiguration.isEnabled(),
				"cm_system_dms_category_lookup", dmsConfiguration.getDefaultDocumentCategoryLookup(),
				"cm_system_workflow_enabled", workflowConfig.isEnabled(),
				"cm_system_gis_enabled", gisConfiguration.isEnabled(),
				"cm_system_gis_geoserver_enabled", gisConfiguration.isGeoServerEnabled(),
				"cm_system_bim_enabled", bimConfiguration.isEnabled()
		).accept((m) -> {
			graphConfiguration.getConfig().forEach((k, v) -> {
				m.put(format("cm_system_relgraph_%s", k), v);
			});
		}));
	}

	private FluentMap<String, Object> getPublicConfigData() {
		return map(
				"cm_system_instance_name", coreConfiguration.getInstanceName(),
				"cm_system_language_default", coreConfiguration.getDefaultLanguage(),
				"cm_system_use_language_prompt", coreConfiguration.useLanguagePrompt(),
				"cm_system_multitenant_enabled", multitenantConfiguration.isMultitenantEnabled()
		);
	}

}
