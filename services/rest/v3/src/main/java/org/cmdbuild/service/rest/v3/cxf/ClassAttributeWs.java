package org.cmdbuild.service.rest.v3.cxf;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.AttributeDefinitionImpl;
import org.cmdbuild.service.rest.v3.cxf.serialization.AttributeTypeConversionService;
import org.cmdbuild.service.rest.v3.model.WsAttributeRequestData;
import org.cmdbuild.dao.entrytype.Attribute;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import org.cmdbuild.dao.entrytype.AttributeDefinition;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.springframework.security.access.prepost.PreAuthorize;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;
import org.cmdbuild.classe.UserClassService;

@Path("{a:classes|processes}/{" + CLASS_ID + "}/attributes/")
@Produces(APPLICATION_JSON)
@Component("v3_attributes")
public class ClassAttributeWs {

	private final UserClassService classService;
	private final AttributeTypeConversionService conversionService;

	public ClassAttributeWs(UserClassService classService, AttributeTypeConversionService conversionService) {
		this.classService = checkNotNull(classService);
		this.conversionService = checkNotNull(conversionService);
	}

	/**

	 read a single attribute data

	 @param classId
	 @param attrId

	 @return

	 */
	@GET
	@Path("{attrId}/")
	public Object read(@PathParam(CLASS_ID) String classId, @PathParam("attrId") String attrId) {
		Attribute attribute = classService.getUserAttribute(classId, attrId);
		return buildAttrResponse(attribute);
	}

	/**

	 read a list of attributes

	 @param classId
	 @param activeOnly true/false, default is false
	 @param limit
	 @param offset

	 @return

	 */
	@GET
	@Path(EMPTY)
	public Object readAll(@PathParam(CLASS_ID) String classId, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {
		List list = classService.getUserAttributes(classId).stream().map(conversionService::serializeAttributeType).collect(toList());
		return response(paged(list, offset, limit));
	}

	/**

	 create attribute

	 @param classId
	 @param attributeData

	 @return new attribute data (including id and default values)

	 */
	@POST
	@Path(EMPTY)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object create(@PathParam(CLASS_ID) String classId, WsAttributeRequestData data) {
		checkNotNull(data);
		Classe classe = classService.getUserClass(classId);
//		AttributeDefinition attributeDefinition = data.toAttrDefinition(classe);
		Attribute attribute = classService.createAttribute(data.toAttrDefinition(classe));//TODO check metadata persistence , check authorization
		return buildAttrResponse(attribute);
	}

	/**

	 update attribute data

	 @param classId
	 @param attrId
	 @param attributeData

	 @return updated attribute data

	 */
	@PUT
	@Path("{attrId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam(CLASS_ID) String classId, @PathParam("attrId") String attrId, WsAttributeRequestData data) {
		checkNotNull(data);
		Classe classe = classService.getUserClass(classId);
//		Classe classe = classService.getClasse(classId);
//		Attribute attribute = classe.getAttribute(attrId);
//		attribute.checkPermission(PS_SERVICE, AP_MODIFY);
//		checkArgument(equal(attrId, data.getName()), "data attr name = %s does not match with path attr id = %s", data.getName(), attrId);
//		AttributeDefinition attributeDefinition = data.toAttrDefinition(classe);
		Attribute attribute = classService.updateAttribute(data.toAttrDefinition(classe));//TODO check metadata persistence
		return buildAttrResponse(attribute);
	}

	@DELETE
	@Path("{attrId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object delete(@PathParam(CLASS_ID) String classId, @PathParam("attrId") String attrId) {
//		Classe classe = classService.getClasse(classId);
//		Attribute attribute = classe.getAttribute(attrId);
//		attribute.checkPermission(PS_SERVICE, AP_MODIFY);
		classService.deleteAttribute(classId, attrId);
		return success();
	}

	@POST
	@Path("order")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object reorder(@PathParam(CLASS_ID) String classId, List<String> attrOrder) {
		checkNotNull(attrOrder);

		Classe classe = classService.getUserClass(classId);

		classService.updateAttributes(prepareAttributesToUpdateForOrder(classe::getAttribute, attrOrder));
		classe = classService.getUserClass(classId);

		return response(attrOrder.stream().map(classe::getAttribute).map(conversionService::serializeAttributeType).collect(toList()), attrOrder.size());
	}

	public static List<AttributeDefinition> prepareAttributesToUpdateForOrder(Function<String, Attribute> attributeFun, List<String> attributes) {
		checkArgument(set(attributes).size() == attributes.size());
		List<Object> list = list();

		for (int i = 0; i < attributes.size(); i++) {
			Attribute attribute = checkNotNull(attributeFun.apply(attributes.get(i)));
//			attribute.checkPermission(SERVICE, MODIFY);
			int newIndex = i + 1;
			if (attribute.getIndex() != newIndex) {
				list.add(AttributeDefinitionImpl.copyOf(attribute).withIndex(newIndex).build());
			} else {
				list.add(attribute);
			}
		}

		return list.stream().filter(AttributeDefinition.class::isInstance).map(AttributeDefinition.class::cast).collect(toList());
	}

	private Object buildAttrResponse(Attribute attribute) {
		return response(conversionService.serializeAttributeType(attribute));
	}

}
