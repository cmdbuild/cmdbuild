/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.gis.GeoserverLayer;
import org.cmdbuild.gis.GisService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CARD_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ZOOM_DEF;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ZOOM_MAX;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ZOOM_MIN;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.springframework.stereotype.Component;

@Component("v3_geolayers")
@Path("classes/{" + CLASS_ID + "}/geolayers/")
@Produces(APPLICATION_JSON)
public class GeoLayerWs {

	private final GisService service;
	private final DaoService dao;

	public GeoLayerWs(GisService service, DaoService dao) {
		this.service = checkNotNull(service);
		this.dao = checkNotNull(dao);
	}

	@GET
	@Path("{" + CARD_ID + "}/")
	public Object getAllForCard(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId) {
		Classe classe = dao.getClasse(classId);
		return response(service.getGeoServerLayersForCard(classId, cardId).stream().map((l) -> layerToWsBean(l).with(
				"owner_type", classe.getName(),
				"owner_id", cardId)
		));
	}

	@GET
	@Path("")
	public Object getAll(@PathParam(CLASS_ID) String classId, @QueryParam("visible") Boolean isVisible) {
		List<GeoserverLayer> list;
		if (firstNonNull(isVisible, false)) {
			list = service.getGeoLayersVisibleFromClass(classId);
		} else {
			list = service.getGeoLayersOwnedByClass(classId);
		}
		return response(list.stream().map((l) -> layerToWsBean(l).with(
				"owner_type", l.getOwnerClassId(),
				"owner_id", l.getOwnerCardId())
		));
	}

	private FluentMap layerToWsBean(GeoserverLayer l) {
		return map(
				"_id", l.getLayerName(),
				"name", l.getLayerName(),
				"type", l.getType(),
				"index", l.getIndex(),
				"geoserver_name", l.getGeoserverName(),
				ZOOM_MIN, l.getMinimumZoom(),
				ZOOM_DEF, l.getDefaultZoom(),
				ZOOM_MAX, l.getMaximumZoom(),
				"visibility", l.getVisibility());
	}

}
