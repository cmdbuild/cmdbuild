/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf.serialization;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import static com.google.common.collect.Lists.transform;
import static java.lang.String.format;
import java.util.List;
import java.util.function.BiConsumer;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.IdAndDescription;
import org.cmdbuild.dao.beans.LookupValue;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.service.rest.v3.cxf.serialization.CardSerializationHelper.ExtendedCardOptions.INCLUDE_MODEL;
import static org.cmdbuild.service.rest.v3.cxf.util.WsAttributeConverterUtils.toClient;
import org.cmdbuild.translation.ObjectTranslationService;
import org.cmdbuild.utils.date.DateUtils;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import org.cmdbuild.utils.lang.CmdbMapUtils;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CardSerializationHelper {

	private final List<String> CARD_ATTR_EXT_META = ImmutableList.of("_changed", "_previous");

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final ObjectTranslationService translationService;
	private final ClassSerializationHelper classSerializationHelper;
	private final AttributeTypeConversionService attributeTypeConversionService;

	public CardSerializationHelper(ObjectTranslationService translationService, ClassSerializationHelper classSerializationHelper, AttributeTypeConversionService attributeTypeConversionService) {
		this.translationService = checkNotNull(translationService);
		this.classSerializationHelper = checkNotNull(classSerializationHelper);
		this.attributeTypeConversionService = checkNotNull(attributeTypeConversionService);
	}

	public enum ExtendedCardOptions {
		NONE, INCLUDE_MODEL
	}

	public CmdbMapUtils.FluentMap serializeCard(Card card, ExtendedCardOptions... extendedCardOptions) {
		try {
			return map(
					"_id", card.getId(),
					"_type", card.getType().getName(),
					"_user", card.getUser(),
					"_beginDate", DateUtils.toIsoDateTime(card.getBeginDate())
			).accept((m) -> {
				if (card.getType().hasMultitenantEnabled()) {
					m.put("_tenant", card.getTenantId());
				}
				card.getType().getServiceAttributes().stream().filter(Attribute::hasServiceReadPermission).forEach((a) -> {
					addCardValuesAndDescriptionsAndExtras(a.getName(), a.getType(), card::get, m::put);
				});
				if (set(extendedCardOptions).contains(INCLUDE_MODEL)) {
					m.put("_model", classSerializationHelper.buildBasicResponse(card.getType())
							.with("attributes", list(transform(card.getType().getServiceAttributes(), attributeTypeConversionService::serializeAttributeType))));
				}
			});
		} catch (Exception ex) {
			throw runtime(ex, "error serializing card = %s", card);
		}
	}

	public void addCardValuesAndDescriptionsAndExtras(String name, CardAttributeType type, Function<String, Object> getter, BiConsumer<String, Object> adder) {
		Object rawValue = "<undefined>";
		try {
			rawValue = getter.apply(name);
			Object value = toClient(type, rawValue);
			adder.accept(name, value);
			switch (type.getName()) {
				case REFERENCE:
				case FOREIGNKEY:
				case LOOKUP:
					if (rawValue instanceof IdAndDescription) {
						adder.accept(format("_%s_description", name), ((IdAndDescription) rawValue).getDescription());
					}
					if (rawValue instanceof LookupValue) {
						LookupValue lookup = ((LookupValue) rawValue);
						adder.accept(format("_%s_code", name), lookup.getCode());
						adder.accept(format("_%s_description_translation", name), translationService.translateLookupDescription(lookup.getLookupType(), lookup.getCode(), lookup.getDescription()));
					}
			}
			CARD_ATTR_EXT_META.stream().map((e) -> format("_%s%s", name, e)).forEach((n) -> {
				Object v = getter.apply(n);
				if (v != null) {
					adder.accept(n, v);
				}
			});
		} catch (Exception ex) {
			throw runtime(ex, "error processing attr = %s of type = %s with value = %s", name, type, rawValue);
		}
	}

}
