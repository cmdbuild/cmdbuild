package org.cmdbuild.service.rest.v3.cxf.helpers;

import static java.lang.String.format;
import static java.net.URLEncoder.encode;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.activation.DataHandler;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SERVICES_V3;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("v3_headerResponseHandler")
@Qualifier(SERVICES_V3)
public class HeaderResponseHandler implements ContainerResponseFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		Object entity = responseContext.getEntity();
		if (entity instanceof DataHandler) {
			DataHandler dataHandler = (DataHandler) entity;
			responseContext.getHeaders().putSingle("Content-Disposition", format("inline; filename=\"%s\"", encodeFileName(dataHandler.getName())));
			String contentType = dataHandler.getContentType();
			if (isNotBlank(contentType)) {
				responseContext.getHeaders().putSingle("Content-Type", contentType);
			}
		}
	}

	private String encodeFileName(String name) {
		try {
			checkNotBlank(name);
			return encode(name, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("error encoding name", e);
			return name;
		}
	}
}
