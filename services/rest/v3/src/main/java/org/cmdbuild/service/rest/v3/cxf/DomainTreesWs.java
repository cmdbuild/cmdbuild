package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Predicates.alwaysTrue;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import org.cmdbuild.logic.data.access.filter.json.JsonParser;
import org.cmdbuild.logic.data.access.filter.model.Element;
import org.cmdbuild.logic.data.access.filter.model.Filter;
import org.cmdbuild.logic.data.access.filter.model.Parser;
import org.cmdbuild.navtree.DomainTreeNode;
import org.cmdbuild.service.rest.v3.cxf.filter.DomainTreeNodeElementPredicate;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.common.utils.PagedElements.paged;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.springframework.stereotype.Component;
import org.cmdbuild.navtree.NavigationTreeService;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component("v3_domainTrees")
@Path("domainTrees")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class DomainTreesWs {

	private final NavigationTreeService service;

	public DomainTreesWs(NavigationTreeService service) {
		this.service = checkNotNull(service);
	}

	@GET
	@Path(EMPTY)
	public Object readAll(@QueryParam(FILTER) String filter, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {
		Predicate<DomainTreeNode> predicate;
		if (isNotBlank(filter)) {
			Parser parser = new JsonParser(filter);
			Filter filterModel = parser.parse();
			Optional<Element> element = filterModel.attribute();
			if (element.isPresent()) {
				predicate = new DomainTreeNodeElementPredicate(element.get());
			} else {
				predicate = alwaysTrue();
			}
		} else {
			predicate = alwaysTrue();
		}
		List list = service.getAll(predicate).stream()
				.map((tree) -> map(
				"_id", tree.getType(),
				"description", tree.getDescription()))
				.collect(toList());
		return response(paged(list, offset, limit));
	}

	@GET
	@Path("{treeId}/")
	public Object read(@PathParam("treeId") String id) {
		DomainTreeNode root = service.getTree(id);
		List nodes = root.getThisNodeAndAllDescendants().stream().map((n) -> map(
				"_id", n.getId(),
				"parent", n.getIdParent(),
				"metadata", map(
						"filter", map("params", null, "text", n.getTargetFilter()),
						"targetClass", n.getTargetClassName(),
						"recursionEnabled", n.getEnableRecursion(),
						"domain", n.getDomainName(),
						"direction", isBlank(n.getDomainName()) ? null : (n.getDirect() ? "_1" : "_2")
				)
		)).collect(toList());
		return response(map(
				"_id", root.getType(),
				"description", root.getDescription(),
				"nodes", nodes
		));
	}

}
