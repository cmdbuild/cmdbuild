package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.cmdbuild.auth.user.OperationUserSupplier;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.PROCESS_ID;
import org.cmdbuild.service.rest.v3.cxf.serialization.FlowConverterService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.cmdbuild.workflow.WorkflowService;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.model.TaskDefinition;
import org.cmdbuild.workflow.core.utils.WorkflowUtils;
import org.cmdbuild.workflow.model.Process;

@Component("v3_processStartActivities")
@Path("processes/{" + PROCESS_ID + "}/start_activities/")
@Produces(APPLICATION_JSON)
public class ProcessStartActivityWs {

	private final WorkflowService workflowService;
	private final FlowConverterService converterService;
	private final OperationUserSupplier userSupplier;

	public ProcessStartActivityWs(OperationUserSupplier userSupplier, WorkflowService workflowService, FlowConverterService converterService) {
		this.workflowService = checkNotNull(workflowService);
		this.converterService = checkNotNull(converterService);
		this.userSupplier = checkNotNull(userSupplier);
	}

	@GET
	@Path(EMPTY)
	public Object read(@PathParam(PROCESS_ID) String processId) {
		Process planClasse = workflowService.getProcess(processId);

		TaskDefinition task = WorkflowUtils.getEntryTaskForCurrentUser(planClasse, userSupplier.getUser());

		return response(converterService.buildTaskResponseWithFullDetail(planClasse, task));
	}

//		converterService.buildTaskResponseWithFullDetail(planClasse, task)
//		ProcessActivityWithBasicDetails element = ToProcessActivityWithBasicDetailsFromCMActivity.newInstance().build().apply(task);
//		ProcessActivityWithFullDetails element = ToProcessActivityDefinition.newInstance().withWritableStatus(true).build().apply(task);
//	@GET
//	@Path("{" + PROCESS_ACTIVITY_ID + "}")
//	public Object read(@PathParam(PROCESS_ID) String processId, @PathParam(PROCESS_ACTIVITY_ID) String activityId) {
//		PlanClasse planClasse = workflowService.getPlanClasse(processId);
//
//		TaskDefinition task = WorkflowUtils.getEntryTaskForCurrentUser(planClasse, userSupplier.getUser());
//
////		checkArgument(task.getId().equals(activityId));//TODO return 404 in case of error
//
////		ProcessActivityWithFullDetails element = ToProcessActivityDefinition.newInstance().withWritableStatus(true).build().apply(task);
////		return response(element);
//		return response(converterService.buildTaskResponseWithFullDetail(planClasse, task));
//	}
}
