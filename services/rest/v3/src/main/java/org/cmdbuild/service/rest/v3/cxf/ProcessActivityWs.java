package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.data.filter.utils.CmdbSorterUtils;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SORT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;

import org.cmdbuild.service.rest.v3.cxf.serialization.FlowConverterService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.workflow.model.Task;

@Component("v3_processActivities")
@Path("processes/{processId}/instance_activities/")
@Produces(APPLICATION_JSON)
public class ProcessActivityWs {

	private final WorkflowService workflowService;
	private final FlowConverterService converterService;

	public ProcessActivityWs(WorkflowService workflowService, FlowConverterService converterService) {
		this.workflowService = checkNotNull(workflowService);
		this.converterService = checkNotNull(converterService);
	}

	@GET
	@Path("")
	public Object getAllActivities(@PathParam("processId") String processId, @QueryParam(START) Integer offset, @QueryParam(LIMIT) Integer limit, @QueryParam(SORT) String sort) {

		PagedElements<Task> tasklist = workflowService.getTaskListForCurrentUserByClassId(processId, offset, limit, CmdbSorterUtils.fromNullableJson(sort));

		List response = tasklist.stream().map((task) -> converterService.serializeFlow(task.getProcessInstance()).with(
				"_activity_id", task.getId(),
				//			resp.getValues().put("_activity_writable", task.);TODO
				"_activity_description", task.getDefinition().getDescription())
		).collect(toList());
		return response(response, tasklist.size());
	}

}
