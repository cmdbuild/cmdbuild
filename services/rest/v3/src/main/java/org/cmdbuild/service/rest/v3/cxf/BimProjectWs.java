/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import java.io.IOException;
import java.util.Collection;
import javax.activation.DataHandler;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.cmdbuild.bim.BimObject;
import org.cmdbuild.bim.BimProject;
import org.cmdbuild.bim.BimService;
import static org.cmdbuild.bim.utils.BimConstants.IFC_CONTENT_TYPE;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILE;
import static org.cmdbuild.utils.date.DateUtils.toIsoDateTime;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.stereotype.Component;

@Component("v3_bim_projects")
@Path("bim/projects/")
@Produces(APPLICATION_JSON)
public class BimProjectWs {

	private final BimService bim;

	public BimProjectWs(BimService bim) {
		this.bim = checkNotNull(bim);
	}

	@GET
	@Path("")
	public Object getAll() {
		Collection<BimProject> projects = bim.getAllProjects();
		return response(projects.stream().sorted(Ordering.natural().onResultOf(BimProject::getName))
				.map((b) -> bim.getProjectAndObject(b.getId()))//TODO this is _very_ inefficent, just a quick fix; change bim service to run a join query or something else
				.map(this::serializeProjectAndObject));
	}

	@GET
	@Path("{id}")
	public Object getOne(@PathParam("id") Long id) {
		Pair<BimProject, BimObject> projectAndObject = bim.getProjectAndObject(id);
		return response(serializeProjectAndObject(projectAndObject));
	}

	@GET
	@Path("{id}/file")
	@Produces(IFC_CONTENT_TYPE)
	public DataHandler downloadIfcFile(@PathParam("id") Long id, @QueryParam("ifcFormat") @Nullable String ifcFormat) {
		DataHandler data = bim.downloadIfcFile(id, ifcFormat);
		return data;
	}

	@POST
	@Path("{id}/file")
	@Consumes(MULTIPART_FORM_DATA)
	@Produces(APPLICATION_JSON)
	public Object uploadIfcFile(@PathParam("id") Long id, @Multipart(FILE) DataHandler dataHandler, @QueryParam("ifcFormat") @Nullable String ifcFormat) throws IOException {
		bim.uploadIfcFile(id, dataHandler, ifcFormat);
		return success();
	}

	private FluentMap<String, Object> serializeProject(BimProject p) {
		return map(
				"_id", p.getId(),
				"name", p.getName(),
				"description", p.getDescription(),
				"lastCheckin", toIsoDateTime(p.getLastCheckin()),
				"projectId", p.getPoid(),
				"active", p.isActive()
		);
	}

	private Object serializeProjectAndObject(Pair<BimProject, BimObject> projectAndObject) {
		return serializeProject(projectAndObject.getLeft()).with(
				"ownerClass", projectAndObject.getRight().getOwnerClassId(),
				"ownerCard", projectAndObject.getRight().getOwnerCardId()
		);
	}

	public static class WsProject {

		private final String name, description;
		private final Boolean active;

		public WsProject(@JsonProperty("name") String name, @JsonProperty("description") String description, @JsonProperty("active") Boolean active) {
			this.name = checkNotBlank(name);
			this.description = description;
			this.active = firstNonNull(active, true);
		}

		public String getName() {
			return name;
		}

		public String getDescription() {
			return description;
		}

		public Boolean getActive() {
			return active;
		}

	}

}
