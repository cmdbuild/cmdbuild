package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import com.google.common.collect.Ordering;
import static java.util.Collections.emptyList;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.cmdbuild.menu.Menu;
import org.cmdbuild.menu.MenuItemType;
import org.cmdbuild.menu.MenuService;
import org.cmdbuild.menu.MenuTreeNode;
import org.cmdbuild.menu.MenuTreeNodeImpl;
import org.cmdbuild.service.rest.v3.cxf.serialization.MenuSerializationHelper;
import static org.cmdbuild.service.rest.v3.cxf.serialization.MenuSerializationHelper.MENU_ITEM_TYPE_WS_MAP;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;

@Component("v3_menu")
@Path("menu/")
@Produces(APPLICATION_JSON)
public class MenuWs {

	private final MenuService menuService;
	private final MenuSerializationHelper helper;

	public MenuWs(MenuService menuService, MenuSerializationHelper helper) {
		this.menuService = checkNotNull(menuService);
		this.helper = checkNotNull(helper);
	}

	@GET
	@Path("")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object readAll() {
		return response(menuService.getAllMenuInfos().stream().map((m) -> map("_id", m.getId(), "group", m.getGroup())).collect(toList()));
	}

	@GET
	@Path("/{menuId}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object read(@PathParam("menuId") Long menuId) {
		Menu menu = menuService.getMenuById(menuId);
		return menuResponse(menu);
	}

	@POST
	@Path("")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object create(MenuRootNodeWsBean data) {
		Menu menu = menuService.create(data.groupName, toMenuTreeNode(data, true));
		return menuResponse(menu);
	}

	@PUT
	@Path("/{menuId}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam("menuId") Long menuId, MenuRootNodeWsBean data) {
		Menu menu = menuService.update(menuId, toMenuTreeNode(data, false));
		return menuResponse(menu);
	}

	@DELETE
	@Path("/{menuId}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object delete(@PathParam("menuId") Long menuId) {
		menuService.delete(menuId);
		return success();
	}

	private MenuTreeNode toMenuTreeNode(MenuRootNodeWsBean data, boolean regenerateNodeCodes) {
		return MenuTreeNodeImpl.buildRoot(data.children.stream().map((n) -> toMenuTreeNode(n, regenerateNodeCodes)).collect(toList()));
	}

	private MenuTreeNode toMenuTreeNode(MenuNodeWsBean data, boolean regenerateNodeCodes) {
		return MenuTreeNodeImpl.builder()
				.withCode(regenerateNodeCodes ? randomId() : data.code)
				.withDescription(data.objectDescription)
				.withTarget(data.target)
				.withType(data.menuType)
				.withChildren(data.children.stream().map((n) -> toMenuTreeNode(n, regenerateNodeCodes)).collect(toList()))
				.build();
	}

	private Object menuResponse(Menu menu) {
		return response(map(helper.serializeMenu(menu.getRootNode())).with("_id", menu.getId(), "group", menu.getGroupName()));
	}

	public static class MenuRootNodeWsBean {

		public final String groupName;
		public final List<MenuNodeWsBean> children;

		public MenuRootNodeWsBean(@JsonProperty("group") String groupName, @JsonProperty("children") List<MenuNodeWsBean> children) {
			this.groupName = checkNotBlank(groupName);
			this.children = firstNonNull(children, emptyList());
		}

	}

	public static class MenuNodeWsBean {

		public final MenuItemType menuType;
		public final String target, objectDescription, code;
		public final List<MenuNodeWsBean> children;

		public MenuNodeWsBean(
				@JsonProperty("menuType") String menuType,
				@JsonProperty("objectTypeName") String target,
				@JsonProperty("_id") String code,
				@JsonProperty("objectDescription") String objectDescription,
				@JsonProperty("children") List<MenuNodeWsBean> children) {
			this.menuType = checkNotNull(MENU_ITEM_TYPE_WS_MAP.inverse().get(checkNotBlank(menuType)), "unknown menu type = '%s'", menuType);
			this.target = target;
			this.objectDescription = objectDescription;
			this.code = firstNonNull(emptyToNull(code), randomId());
			this.children = firstNonNull(children, emptyList());
		}

	}

}
