package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import javax.activation.DataHandler;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.custompage.CustomPageService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILE;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.toByteArray;
import org.springframework.security.access.prepost.PreAuthorize;
import org.cmdbuild.custompage.CustomPageInfo;
import org.cmdbuild.custompage.CustomPageInfoImpl;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;

@Component("v3_customPages")
@Path("custompages/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class CustomPageWs {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final CustomPageService customPageService;

	public CustomPageWs(CustomPageService customPageService) {
		this.customPageService = checkNotNull(customPageService);
	}

	@GET
	@Path(EMPTY)
	public Object list() {
		logger.debug("list all custom pages for current user");
		List<CustomPageInfo> list = customPageService.getForCurrentUser();
		return response(list.stream().map(this::serializeCustomPage));
	}

	@GET
	@Path("{id}")
	public Object get(@PathParam("id") Long id) {
		CustomPageInfo customPage = customPageService.get(id);
		return customPageResponse(customPage);
	}

	@DELETE
	@Path("{id}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object delete(@PathParam("id") Long id) {
		customPageService.delete(id);
		return success();
	}

	@POST
	@Path(EMPTY)
	@Consumes(MULTIPART_FORM_DATA)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object create(@Multipart(FILE) DataHandler dataHandler, @QueryParam("merge") boolean merge) {
		CustomPageInfo customPage;
		if (merge) {
			customPage = customPageService.createOrUpdate(toByteArray(dataHandler));
		} else {
			customPage = customPageService.create(toByteArray(dataHandler));
		}
		return customPageResponse(customPage);
	}

	@PUT
	@Path("{id}")
	@Consumes(MULTIPART_FORM_DATA)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam("id") Long id, @Multipart(FILE) DataHandler dataHandler) {
		return customPageResponse(customPageService.update(id, toByteArray(dataHandler)));
	}

	@PUT
	@Path("{id}")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam("id") Long id, WsCustomPageData data) {
		CustomPageInfo customPage = customPageService.get(id);
		customPage = CustomPageInfoImpl.copyOf(customPage).withDescription(data.description).build();
		return customPageResponse(customPageService.update(customPage));
	}

	private Object customPageResponse(CustomPageInfo customPage) {
		return response(serializeCustomPage(customPage));
	}

	private Object serializeCustomPage(CustomPageInfo customPage) {
		return map(
				"_id", customPage.getId(),
				"name", customPage.getName(),
				"description", customPage.getDescription(),
				"alias", customPage.getExtjsAlias(),
				"componentId", customPage.getExtjsComponentId());
	}

	public static class WsCustomPageData {

		private final String description;

		public WsCustomPageData(@JsonProperty("description") String description) {
			this.description = description;
		}

	}
}
