package org.cmdbuild.service.rest.v3.cxf.helpers;

import org.cmdbuild.service.rest.v3.model.ProcessStatus;

public interface ProcessStatusHelper {

	Iterable<ProcessStatus> allValues();

}
