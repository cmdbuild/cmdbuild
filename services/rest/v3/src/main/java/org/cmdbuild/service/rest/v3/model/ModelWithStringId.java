package org.cmdbuild.service.rest.v3.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.UNDERSCORED_ID;

import javax.xml.bind.annotation.XmlAttribute;

public abstract class ModelWithStringId extends AbstractModel {

	private String id;

	protected ModelWithStringId() {
		// usable by subclasses only
	}

	@XmlAttribute(name = UNDERSCORED_ID)
	@JsonProperty(UNDERSCORED_ID)
	public String getId() {
		return id;
	}

	void setId(final String id) {
		this.id = id;
	}

}
