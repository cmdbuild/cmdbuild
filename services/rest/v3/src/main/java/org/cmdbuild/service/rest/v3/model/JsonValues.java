package org.cmdbuild.service.rest.v3.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonValues extends Values {

	private static final ObjectMapper mapper = new ObjectMapper();

	public static JsonValues valueOf(final String value) throws Exception {
		@SuppressWarnings("unchecked")
		final Map<String, Object> values = mapper.readValue(value, Map.class);
		final JsonValues output = new JsonValues();
		output.putAll(values);
		return output;
	}

	JsonValues() {
		// package visibility
	}

}
