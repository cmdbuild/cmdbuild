package org.cmdbuild.service.rest.v3.model;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.collect.ForwardingMap;
import static com.google.common.collect.Maps.newLinkedHashMap;

/**
 * map-like data container
 */
@XmlRootElement
public class Values extends ForwardingMap<String, Object> implements Model {

	private final Map<String, Object> delegate;

	Values() {
		this.delegate = newLinkedHashMap();
	}

	@Override
	protected Map<String, Object> delegate() {
		return delegate;
	}

}
