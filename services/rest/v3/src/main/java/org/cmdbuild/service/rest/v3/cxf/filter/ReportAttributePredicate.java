package org.cmdbuild.service.rest.v3.cxf.filter;

import static com.google.common.base.Functions.toStringFunction;
import static com.google.common.collect.FluentIterable.from;
import static java.lang.String.format;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESCRIPTION;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.TITLE;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.cmdbuild.logic.data.access.filter.model.Attr;
import org.cmdbuild.logic.data.access.filter.model.EqualTo;
import org.cmdbuild.logic.data.access.filter.model.ForwardingPredicateVisitor;
import org.cmdbuild.logic.data.access.filter.model.In;
import org.cmdbuild.logic.data.access.filter.model.PredicateVisitor;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import org.cmdbuild.report.ReportInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportAttributePredicate extends ForwardingPredicateVisitor implements Predicate<ReportInfo> {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static final PredicateVisitor NOT_SUPPORTED = NotSupportedPredicateVisitor.getInstance();

	private final Attr attribute;
	private ReportInfo input;
	private boolean output;

	public ReportAttributePredicate(final Attr attribute) {
		this.attribute = attribute;
	}

	@Override
	protected PredicateVisitor delegate() {
		return NOT_SUPPORTED;
	}

	@Override
	public boolean apply(final ReportInfo input) {
		this.input = input;
		this.output = false;
		this.attribute.getPredicate().accept(this);
		return output;
	}

	@Override
	public void visit(final EqualTo predicate) {
		final boolean _output;
		final Object expected = predicate.getValue();
		if (TITLE.equals(attribute.getName())) {
			_output = input.getCode().equals(expected);
		} else if (DESCRIPTION.equals(attribute.getName())) {
			_output = input.getDescription().equals(expected);
		} else {
			logger.warn(format("attribute '%s' not supported", attribute.getName()));
			_output = true;
		}
		output = _output;
	}

	@Override
	public void visit(final In predicate) {
		final boolean _output;
		final FluentIterable<String> expected = from(predicate.getValues()) //
				.transform(toStringFunction());
		if (TITLE.equals(attribute.getName())) {
			_output = expected.contains(input.getCode());
		} else if (DESCRIPTION.equals(attribute.getName())) {
			_output = expected.contains(input.getDescription());
		} else {
			logger.warn(format("attribute '%s' not supported", attribute.getName()));
			_output = true;
		}
		output = _output;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, SHORT_PREFIX_STYLE) //
				.append("attribute", attribute.getName()) //
				.append("predicate", attribute.getPredicate()) //
				.build();
	}

}
