package org.cmdbuild.service.rest.v3.model.adapter;

import com.google.common.base.Predicates;
import static com.google.common.base.Predicates.containsPattern;
import static com.google.common.base.Predicates.not;
import com.google.common.collect.ComparisonChain;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.UNDERSCORED_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.UNDERSCORED_TYPE;
import static org.cmdbuild.service.rest.v3.model.Models.newCard;
import static org.cmdbuild.service.rest.v3.model.Models.newValues;

import org.cmdbuild.service.rest.v3.model.WsCard;
import org.cmdbuild.service.rest.v3.model.Values;

import static com.google.common.collect.Maps.filterKeys;
import com.google.common.collect.Ordering;
import static java.util.Collections.singleton;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.UNDERSCORED_TENANT;
import static org.cmdbuild.service.rest.v3.model.Models.ATTRIBUTE_DESCRIPTION_SUFFIX;
import static org.cmdbuild.service.rest.v3.model.Models.ATTRIBUTE_SPECIAL_PREFIX;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDTENANT;

public class CardAdapter extends ModelToValuesAdapter<WsCard> {

	private final static String DESCRIPTION_REGEX_1 = "^" + ATTRIBUTE_SPECIAL_PREFIX + ".*" + ATTRIBUTE_DESCRIPTION_SUFFIX + "$",
			DESCRIPTION_REGEX_2 = "^" + ATTRIBUTE_SPECIAL_PREFIX + "(.*" + ATTRIBUTE_DESCRIPTION_SUFFIX + ")$";

	@Override
	protected Values modelToValues(WsCard input) {
		/*
		 * predefined attributes must always be added last so they are not
		 * overwritten
		 */
		return newValues()
				.withValues(filterKeys(input.getValues(), Predicates.not(Predicates.in(singleton(ATTR_IDTENANT)))))
				.withValue(UNDERSCORED_TYPE, input.getType())
				.withValue(UNDERSCORED_ID, input.getId())
				.withValue(UNDERSCORED_TENANT, input.getValues().get(ATTR_IDTENANT))
				.accept((builder) -> {
					if (!isBlank(input.getForDomain())) {
						builder.withValue("_" + input.getForDomain() + "_available", input.getForDomainAvailable());
					}
				})
				.orderValuesByKey(Ordering.<String>from((a, b) -> ComparisonChain.start()
				.compareTrueFirst(a.startsWith(ATTRIBUTE_SPECIAL_PREFIX), b.startsWith(ATTRIBUTE_SPECIAL_PREFIX)).compare(a, b).result()).onResultOf((k) -> k.replaceFirst(DESCRIPTION_REGEX_2, "$1")))
				.build();
	}

	@Override
	protected WsCard valuesToModel(Values input) {
		return newCard()
				.withType(getAndRemove(input, UNDERSCORED_TYPE, String.class))
				.withId(getAndRemove(input, UNDERSCORED_ID, Long.class))
				.withValues(filterKeys(input, not(containsPattern(DESCRIPTION_REGEX_1))))
				.build();
	}

}
