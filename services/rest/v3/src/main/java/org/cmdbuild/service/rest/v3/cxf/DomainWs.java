package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Objects.equal;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.equalTo;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.cmdbuild.dao.beans.DomainMetadataImpl;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.DomainDefinitionImpl;
import org.cmdbuild.data.filter.AttributeFilterCondition;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.utils.CmdbFilterUtils;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ACTIVE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESTINATION;
import org.springframework.stereotype.Component;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DOMAIN_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.EXT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.FILTER;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SOURCE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import org.cmdbuild.translation.ObjectTranslationService;
import org.cmdbuild.utils.lang.CmdbCollectionUtils;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.security.access.prepost.PreAuthorize;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.data.filter.AttributeFilterProcessor;
import org.cmdbuild.utils.lang.CmdbStringUtils;
import static org.cmdbuild.auth.login.AuthorityConst.HAS_ADMIN_ACCESS_AUTHORITY;
import org.cmdbuild.classe.UserDomainService;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.driver.repository.DomainRepository;

@Component("v3_domains")
@Path("domains/")
@Produces(APPLICATION_JSON)
public class DomainWs {

	private final DomainRepository domainRepository;
	private final UserDomainService domainService;
	private final DaoService dao;
	private final ObjectTranslationService translationService;

	public DomainWs(DomainRepository domainRepository, UserDomainService domainService, DaoService dao, ObjectTranslationService translationService) {
		this.domainRepository = checkNotNull(domainRepository);
		this.domainService = checkNotNull(domainService);
		this.dao = checkNotNull(dao);
		this.translationService = checkNotNull(translationService);
	}

	/**
	 * get domains
	 *
	 * @param filterStr
	 * @param limit
	 * @param offset
	 * @param includeFullDetails if <i>true</i> return full domain details
	 * @return
	 */
	@GET
	@Path(EMPTY)
	public Object readAll(@QueryParam(FILTER) String filterStr, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset, @QueryParam(EXT) Boolean includeFullDetails) {
		List<Domain> domains = domainService.getUserDomains();
		CmdbFilter filter = CmdbFilterUtils.fromNullableJson(filterStr);
		if (filter.hasAttributeFilter()) {
			domains = AttributeFilterProcessor.builder()
					.withKeyToValueFunction((AttributeFilterProcessor.KeyToValueFunction<Domain>) (String key, Domain domain) -> {
						switch (key) {
							case SOURCE:
								return ((Domain) domain).getSourceClass();
							case DESTINATION:
								return ((Domain) domain).getTargetClass();
							case ACTIVE:
								return Boolean.toString(((Domain) domain).isActive());
							case "cardinality":
								return ((Domain) domain).getCardinality();
							default:
								throw new IllegalArgumentException("unsupported filter key = " + key);
						}
					})
					.withConditionEvaluatorFunction(new AttributeFilterProcessor.ConditionEvaluatorFunction() {

						@Override
						public boolean evaluate(AttributeFilterCondition condition, Object value) {
							switch (condition.getOperator()) {
								case EQUAL:
									return equal(valueToString(value), condition.getSingleValue());
								case IN:
									return condition.getValues().contains(valueToString(value));
								case CONTAIN:
									return checkNotNull((Classe) value).getDescendantsAndSelf().stream().map(Classe::getName).anyMatch(equalTo(condition.getSingleValue())); //TODO filter also 
								default:
									throw new IllegalArgumentException("unsupported operator = " + condition.getOperator());
							}
						}

						private String valueToString(Object value) {
							if (value instanceof Classe) {
								return ((Classe) value).getName();
							} else {
								return CmdbStringUtils.toStringOrNull(value);
							}
						}
					})
					.withFilter(filter.getAttributeFilter())
					.filter(domains);
		}
		List list = paged(domains, offset, limit).stream().map(equal(includeFullDetails, Boolean.TRUE) ? this::serializeDetailedDomain : this::serializeBasicDomain).collect(toList());
		return response(list, domains.size());
	}

	/**
	 * get domain details
	 *
	 * @param domainId
	 * @return
	 */
	@GET
	@Path("{" + DOMAIN_ID + "}/")
	public Object read(@PathParam(DOMAIN_ID) String domainId) {
		Domain domain = domainService.getUserDomain(domainId);
		return response(serializeDetailedDomain(domain));
	}

	@POST
	@Path(EMPTY)
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object create(WsDomainData data) {
		Domain domain = domainRepository.createDomain(toDomainDefinition(data).build());
		return response(serializeDetailedDomain(domain));
	}

	@PUT
	@Path("{domainId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object update(@PathParam("domainId") String domainId, WsDomainData data) {
		Domain domain = domainRepository.getDomain(domainId);
		domain = domainRepository.updateDomain(toDomainDefinition(data).withOid(domain.getId()).build());
		return response(serializeDetailedDomain(domain));
	}

	@DELETE
	@Path("{domainId}/")
	@PreAuthorize(HAS_ADMIN_ACCESS_AUTHORITY)
	public Object delete(@PathParam("domainId") String domainId) {
		domainRepository.deleteDomain(domainRepository.getDomain(domainId));
		return success();
	}

	private FluentMap<String, Object> serializeBasicDomain(Domain input) {
		return map("_id", input.getName(),
				"name", input.getName(),
				"description", input.getDescription());
//				"_description_translated",translationService.t) TODO domain description translation ???
	}

	private FluentMap<String, Object> serializeDetailedDomain(Domain input) {
		return serializeBasicDomain(input).with(
				"source", input.getSourceClass().getName(),
				"sourceProcess", input.getSourceClass().isProcess(),
				"destination", input.getTargetClass().getName(),
				"destinationProcess", input.getTargetClass().isProcess(),
				"cardinality", input.getCardinality(),
				"descriptionDirect", input.getDirectDescription(),
				"_descriptionDirect_translation", translationService.translateDomainDirectDescription(input.getName(), input.getDirectDescription()),
				"descriptionInverse", input.getInverseDescription(),
				"_descriptionInverse_translation", translationService.translateDomainInverseDescription(input.getName(), input.getInverseDescription()),
				"indexDirect", input.getIndexForSource(),
				"indexInverse", input.getIndexForTarget(),
				"descriptionMasterDetail", input.getMasterDetailDescription(),
				"_descriptionMasterDetail_translation", translationService.translateDomainMasterDetailDescription(input.getName(), input.getMasterDetailDescription()),
				"filterMasterDetail", input.getMasterDetailFilter(),
				"isMasterDetail", input.isMasterDetail(),
				"active", input.isActive(),
				"disabledSourceDescendants", CmdbCollectionUtils.toList(input.getDisabledSourceDescendants()),
				"disabledDestinationDescendants", CmdbCollectionUtils.toList(input.getDisabledTargetDescendants()));
	}

	private DomainDefinitionImpl.DomainDefinitionImplBuilder toDomainDefinition(WsDomainData domainData) {
		return DomainDefinitionImpl.builder()
				.withName(domainData.name)
				.withSourceClass(dao.getClasse(domainData.source))
				.withTargetClass(dao.getClasse(domainData.destination))
				.withMetadata(DomainMetadataImpl.builder()
						.withCardinality(domainData.cardinality)
						.withDescription(domainData.description)
						.withDirectDescription(domainData.descriptionDirect)
						.withInverseDescription(domainData.descriptionInverse)
						.withIsActive(domainData.isActive)
						.withIsMasterDetail(domainData.isMasterDetail)
						.withDisabledSourceDescendants(domainData.disabledSourceDescendants)
						.withDisabledTargetDescendants(domainData.disabledDestinationDescendants)
						.withMasterDetailDescription(domainData.descriptionMasterDetail)
						.withMasterDetailFilter(domainData.filterMasterDetail)
						.withSourceIndex(domainData.indexDirect)
						.withTargetIndex(domainData.indexInverse)
						.build());
	}

	public static class WsDomainData {

		private final String name, description, source, destination, cardinality, descriptionDirect, descriptionInverse, descriptionMasterDetail, filterMasterDetail;
		private final Integer indexDirect, indexInverse;
		private final boolean isActive, isMasterDetail;
		private final List<String> disabledSourceDescendants, disabledDestinationDescendants;

		public WsDomainData(@JsonProperty("source") String source,
				@JsonProperty("name") String name,
				@JsonProperty("description") String description,
				@JsonProperty("destination") String destination,
				@JsonProperty("cardinality") String cardinality,
				@JsonProperty("descriptionDirect") String descriptionDirect,
				@JsonProperty("descriptionInverse") String descriptionInverse,
				@JsonProperty("indexDirect") Integer indexDirect,
				@JsonProperty("indexInverse") Integer indexInverse,
				@JsonProperty("descriptionMasterDetail") String descriptionMasterDetail,
				@JsonProperty("filterMasterDetail") String filterMasterDetail,
				@JsonProperty("disabledSourceDescendants") List<String> disabledSourceDescendants,
				@JsonProperty("disabledDestinationDescendants") List<String> disabledDestinationDescendants,
				@JsonProperty("active") boolean isActive,
				@JsonProperty("isMasterDetail") boolean isMasterDetail) {
			this.source = checkNotBlank(source);
			this.destination = checkNotBlank(destination);
			this.cardinality = checkNotBlank(cardinality);
			this.descriptionDirect = descriptionDirect;
			this.descriptionInverse = descriptionInverse;
			this.indexDirect = indexDirect;
			this.indexInverse = indexInverse;
			this.descriptionMasterDetail = descriptionMasterDetail;
			this.filterMasterDetail = filterMasterDetail;
			this.isActive = isActive;
			this.isMasterDetail = isMasterDetail;
			this.name = name;
			this.description = description;
			this.disabledSourceDescendants = disabledSourceDescendants;
			this.disabledDestinationDescendants = disabledDestinationDescendants;
		}

	}

}
