package org.cmdbuild.service.rest.v3.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import static java.util.Collections.emptyMap;
import java.util.List;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ATTRIBUTE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESCRIPTION;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.INDEX;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.METADATA;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.NAME;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SUBTYPE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.TYPE;

import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.MAP_STYLE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ZOOM_MAX;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ZOOM_MIN;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.gis.GisAttribute;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.UNDERSCORED_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ZOOM_DEF;

@XmlRootElement(name = ATTRIBUTE)
public class WsGeoAttribute {

	private String name, id;
	private String description;
	private String type;
	private String subtype;
	private Integer index, zoomMin,zoomDef,zoomMax;
	private List<String> visibility;
	private String ownerClass;
	private Long ownerCard;
	private Map<String, Object> style;

	public WsGeoAttribute() {
	}

	public Integer getZoomMin() {
		return zoomMin;
	}

	public void setZoomMin(Integer zoomMin) {
		this.zoomMin = zoomMin;
	}

	public Integer getZoomDef() {
		return zoomDef;
	}

	public void setZoomDef(Integer zoomDef) {
		this.zoomDef = zoomDef;
	}

	public Integer getZoomMax() {
		return zoomMax;
	}

	public void setZoomMax(Integer zoomMax) {
		this.zoomMax = zoomMax;
	}

//	public static WsGeoAttribute layerToWsGeoAttribute(GeoserverLayer layer) {
//		WsGeoAttribute attribute = new WsGeoAttribute();
//		attribute.setType("geometry");
//		attribute.setSubtype(layer.getType());
//		attribute.setCardsBinding(list(layer.getCardsBinding()));
//		attribute.setDescription(layer.getDescription());
//		attribute.setId(layer.getLayerName());
//		attribute.setIndex(layer.getIndex());
//		attribute.setName(layer.getLayerName());
//		attribute.setMetadata(map(ZOOM_MIN, layer.getMinimumZoom(),
//				ZOOM_MAX, layer.getMaximumZoom()));
//		return attribute;
//	}
	@XmlAttribute(name = UNDERSCORED_ID)
	@JsonProperty(UNDERSCORED_ID)
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@XmlAttribute(name = NAME)
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@XmlAttribute(name = DESCRIPTION)
	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@XmlAttribute(name = TYPE)
	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@XmlAttribute(name = SUBTYPE)
	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(final String subtype) {
		this.subtype = subtype;
	}

	@XmlAttribute(name = INDEX)
	public Integer getIndex() {
		return index;
	}

	public void setIndex(final Integer index) {
		this.index = index;

	}

	public Map<String, Object> getStyle() {
		return style;
	}

	public void setStyle(Map<String, Object> style) {
		this.style = style;
	}

//	@XmlElement(name = METADATA, nillable = true)
//	public Map<String, Object> getMetadata() {
//		return firstNonNull(metadata, emptyMap());
//	}
//
//	public void setMetadata(final Map<String, Object> metadata) {
//		this.metadata = metadata;
//	}

	@XmlAttribute
	public List<String> getVisibility() {
		return visibility;
	}

	public void setVisibility(List<String> visibility) {
		this.visibility = visibility;
	}

	@XmlAttribute(name = "owner_type")
	public String getOwnerClass() {
		return ownerClass;
	}

	public void setOwnerClass(String ownerClass) {
		this.ownerClass = ownerClass;
	}

	@XmlAttribute(name = "owner_id")
	public Long getOwnerCard() {
		return ownerCard;
	}

	public void setOwnerCard(Long ownerCard) {
		this.ownerCard = ownerCard;
	}

}
