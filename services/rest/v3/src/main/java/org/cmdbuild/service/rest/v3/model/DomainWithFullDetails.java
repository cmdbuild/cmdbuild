package org.cmdbuild.service.rest.v3.model;

import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.ACTIVE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CARDINALITY;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESCRIPTION_DIRECT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESCRIPTION_INVERSE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESCRIPTION_MASTER_DETAIL;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESTINATION;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.DESTINATION_PROCESS;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SOURCE;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SOURCE_PROCESS;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@XmlRootElement
public class DomainWithFullDetails extends DomainWithBasicDetails {

	private String source;
	private boolean sourceProcess;
	private String destination;
	private boolean destinationProcess;
	private String cardinality;
	private String descriptionDirect;
	private String descriptionInverse;
	private String descriptionMasterDetail, filterMasterDetail;
	private boolean active, isMasterDetail;
	private Integer indexDirect, indexInverse;

	DomainWithFullDetails() {
		// package visibility
	}

	@XmlAttribute(name = SOURCE)
	public String getSource() {
		return source;
	}

	void setSource(final String source) {
		this.source = source;
	}

	@XmlAttribute(name = SOURCE_PROCESS)
	public boolean isSourceProcess() {
		return sourceProcess;
	}

	void setSourceProcess(final boolean sourceProcess) {
		this.sourceProcess = sourceProcess;
	}

	@XmlAttribute(name = DESTINATION)
	public String getDestination() {
		return destination;
	}

	void setDestination(final String source) {
		this.destination = source;
	}

	@XmlAttribute(name = DESTINATION_PROCESS)
	public boolean isDestinationProcess() {
		return destinationProcess;
	}

	void setDestinationProcess(final boolean destinationProcess) {
		this.destinationProcess = destinationProcess;
	}

	@XmlAttribute(name = CARDINALITY)
	public String getCardinality() {
		return cardinality;
	}

	void setCardinality(final String cardinality) {
		this.cardinality = cardinality;
	}

	@XmlAttribute(name = DESCRIPTION_DIRECT)
	public String getDescriptionDirect() {
		return descriptionDirect;
	}

	void setDescriptionDirect(final String descriptionDirect) {
		this.descriptionDirect = descriptionDirect;
	}

	@XmlAttribute(name = DESCRIPTION_INVERSE)
	public String getDescriptionInverse() {
		return descriptionInverse;
	}

	void setDescriptionInverse(final String descriptionInverse) {
		this.descriptionInverse = descriptionInverse;
	}

	@XmlAttribute
	public Integer getIndexDirect() {
		return indexDirect;
	}

	public void setIndexDirect(Integer indexDirect) {
		this.indexDirect = indexDirect;
	}

	@XmlAttribute
	public Integer getIndexInverse() {
		return indexInverse;
	}

	public void setIndexInverse(Integer indexInverse) {
		this.indexInverse = indexInverse;
	}

	@XmlAttribute(name = DESCRIPTION_MASTER_DETAIL)
	public String getDescriptionMasterDetail() {
		return descriptionMasterDetail;
	}

	void setDescriptionMasterDetail(final String descriptionMasterDetail) {
		this.descriptionMasterDetail = descriptionMasterDetail;
	}

	@XmlAttribute
	public String getFilterMasterDetail() {
		return filterMasterDetail;
	}

	public void setFilterMasterDetail(String filterMasterDetail) {
		this.filterMasterDetail = filterMasterDetail;
	}

	@XmlAttribute(name = ACTIVE)
	public boolean isActive() {
		return active;
	}

	void setActive(boolean active) {
		this.active = active;
	}

	@XmlAttribute
	public boolean getIsMasterDetail() {
		return isMasterDetail;
	}

	void setIsMasterDetail(boolean isMasterDetail) {
		this.isMasterDetail = isMasterDetail;
	}

	@Override
	protected boolean doEquals(final Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DomainWithFullDetails)) {
			return false;
		}

		final DomainWithFullDetails other = DomainWithFullDetails.class.cast(obj);
		return super.doEquals(obj) && new EqualsBuilder() //
				.append(this.source, other.source) //
				.append(this.destination, other.destination) //
				.append(this.cardinality, other.cardinality) //
				.append(this.descriptionDirect, other.descriptionDirect) //
				.append(this.descriptionInverse, other.descriptionInverse) //
				.append(this.descriptionMasterDetail, other.descriptionMasterDetail) //
				.append(this.active, other.active) //
				.append(this.isMasterDetail, other.isMasterDetail) //
				.isEquals();
	}

	@Override
	protected int doHashCode() {
		return new HashCodeBuilder() //
				.append(super.doHashCode()) //
				.append(source) //
				.append(destination) //
				.append(cardinality) //
				.append(descriptionDirect) //
				.append(descriptionInverse) //
				.append(descriptionMasterDetail) //
				.append(active) //
				.append(isMasterDetail) //
				.toHashCode();
	}

}
