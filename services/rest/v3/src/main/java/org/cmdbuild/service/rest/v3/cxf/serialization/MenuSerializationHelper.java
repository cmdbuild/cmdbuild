/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf.serialization;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import java.util.concurrent.atomic.AtomicInteger;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.menu.MenuItemType;
import static org.cmdbuild.menu.MenuItemType.CLASS;
import static org.cmdbuild.menu.MenuItemType.CUSTOM_PAGE;
import static org.cmdbuild.menu.MenuItemType.DASHBOARD;
import static org.cmdbuild.menu.MenuItemType.FOLDER;
import static org.cmdbuild.menu.MenuItemType.PROCESS;
import static org.cmdbuild.menu.MenuItemType.REPORT_CSV;
import static org.cmdbuild.menu.MenuItemType.REPORT_ODT;
import static org.cmdbuild.menu.MenuItemType.REPORT_PDF;
import static org.cmdbuild.menu.MenuItemType.REPORT_XML;
import static org.cmdbuild.menu.MenuItemType.ROOT;
import static org.cmdbuild.menu.MenuItemType.SYSTEM_FOLDER;
import static org.cmdbuild.menu.MenuItemType.VIEW;
import org.cmdbuild.menu.MenuTreeNode;
import org.cmdbuild.translation.ObjectTranslationService;
import org.springframework.stereotype.Component;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component
public class MenuSerializationHelper {

	public final static BiMap<MenuItemType, String> MENU_ITEM_TYPE_WS_MAP = HashBiMap.create(map(
			CLASS, "class",
			DASHBOARD, "dashboard",
			PROCESS, "processclass",
			FOLDER, "folder",
			SYSTEM_FOLDER, "system_folder",
			REPORT_CSV, "reportcsv",
			REPORT_PDF, "reportpdf",
			REPORT_ODT, "reportodt",
			REPORT_XML, "reportxml",
			VIEW, "view",
			CUSTOM_PAGE, "custompage",
			ROOT, "root"));

	private final ObjectTranslationService translationService;

	public MenuSerializationHelper(ObjectTranslationService translationService) {
		this.translationService = checkNotNull(translationService);
	}

	public FluentMap serializeMenu(MenuTreeNode item) {
		AtomicInteger index = new AtomicInteger(-1);
		return map(
				"_id", item.getCode(),
				"menuType", checkNotNull(MENU_ITEM_TYPE_WS_MAP.get(item.getType())),
				"objectDescription", item.getDescription(),
				"_objectDescription_translation", isBlank(item.getCode()) ? item.getDescription() : translationService.translateMenuitemDescription(item.getCode(), item.getDescription()),
				"children", item.getChildren().stream().map((i) -> serializeMenu(i).with("index", index.incrementAndGet())).collect(toList())
		).skipNullValues().with("objectTypeName", emptyToNull(item.getTarget())).then();
	}
}
