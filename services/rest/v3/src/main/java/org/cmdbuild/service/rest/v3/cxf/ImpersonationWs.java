package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.cmdbuild.auth.user.OperationUser;

import org.springframework.stereotype.Component;
import org.cmdbuild.auth.session.SessionService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.SESSION;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.USERNAME;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;

@Component("v3_impersonate")
@Path("sessions/{" + SESSION + "}/impersonate/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class ImpersonationWs {

	private final SessionService sessionLogic;

	public ImpersonationWs(SessionService sessionLogic) {
		this.sessionLogic = checkNotNull(sessionLogic);
	}

	/**
	 * note: username must have an automatic default group (explicit default
	 * group, or a single group), otherwise this will fail
	 *
	 * @param id
	 * @param username
	 */
	
	@PUT
	@Path("{" + USERNAME + "}/")
	public Object start(@PathParam(SESSION) String sessionId, @PathParam(USERNAME) String username) {
		checkArgument(sessionLogic.exists(sessionId));
		OperationUser operationUser = sessionLogic.getUser(sessionId);

		checkArgument(!(operationUser.hasAdminAccess() || operationUser.getLoginUser().isService()));
		sessionLogic.impersonate(sessionId, username);
		return success();
	}

	@DELETE
	@Path(EMPTY)
	public Object stop(@PathParam(SESSION) String sessionId) {
		checkArgument(sessionLogic.exists(sessionId));
		sessionLogic.impersonate(sessionId, null);
		return success();
	}

}
