package org.cmdbuild.service.rest.v3.cxf.filter;

import org.cmdbuild.logic.data.access.filter.model.Attr;
import org.cmdbuild.logic.data.access.filter.model.Element;

import com.google.common.base.Predicate;
import org.cmdbuild.report.ReportInfo;

public class ReportElementPredicate extends ElementPredicate<ReportInfo> {

	public ReportElementPredicate(final Element element) {
		super(element);
	}

	@Override
	protected Predicate<ReportInfo> predicateOf(final Attr element) {
		return new ReportAttributePredicate(element);
	}

	@Override
	protected Predicate<ReportInfo> predicateOf(final Element element) {
		return new ReportElementPredicate(element);
	}

}