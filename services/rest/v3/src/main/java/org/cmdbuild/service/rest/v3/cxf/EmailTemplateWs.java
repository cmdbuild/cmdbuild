package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.cmdbuild.email.EmailTemplate;
import org.cmdbuild.email.EmailTemplateService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.springframework.stereotype.Component;

@Component("v3_email_templates")
@Path("email/templates/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class EmailTemplateWs {

	private final EmailTemplateService service;

	public EmailTemplateWs(EmailTemplateService service) {
		this.service = checkNotNull(service);
	}

	@GET
	@Path(EMPTY)
	public Object readAll(@QueryParam(LIMIT) Long limit, @QueryParam(START) Long offset) {
		List<EmailTemplate> list = service.getAll();
		return response(paged(list, this::serializeBasicTemplate, offset, limit));
	}

	@GET
	@Path("{templateId}/")
	public Object read(@PathParam("templateId") Long id) {
		EmailTemplate element = service.getOne(id);
		return response(serializeDetailedTemplate(element));
	}

	private FluentMap<String, Object> serializeBasicTemplate(EmailTemplate t) {
		return map(
				"_id", t.getId(),
				"name", t.getName()
		);
	}

	private FluentMap<String, Object> serializeDetailedTemplate(EmailTemplate t) {
		return serializeBasicTemplate(t).with(
				"description", t.getDescription(),
				"from", t.getFrom(),
				"to", t.getTo(),
				"cc", t.getCc(),
				"bcc", t.getBcc(),
				"subject", t.getSubject(),
				"body", t.getBody(),
				"account", t.getAccount(),
				"keepSynchronization", t.getKeepSynchronization(),
				"promptSynchronization", t.getPromptSynchronization(),
				"delay", t.getDelay()
		);
	}

}
