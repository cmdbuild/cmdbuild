/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.cxf.serialization;

import com.google.common.base.Function;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Maps.transformEntries;
import static java.util.Collections.emptyMap;
import java.util.Map;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_DESCRIPTION;
import org.cmdbuild.dao.driver.postgres.relationquery.RelationInfo;
import static org.cmdbuild.service.rest.v3.cxf.util.WsAttributeConverterUtils.toClient;
import static org.cmdbuild.service.rest.v3.model.Models.newCard;
import static org.cmdbuild.service.rest.v3.model.Models.newRelation;
import org.cmdbuild.service.rest.v3.model.WsRelationData;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.EntryType;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

/**
 *
 */
public class RelationTransformer {

	private static final RelationInfoToRelation BASIC_DETAILS = RelationInfoToRelation.newInstance().includeValues(false).build();

	private static final RelationInfoToRelation FULL_DETAILS = RelationInfoToRelation.newInstance().includeValues(true).build();

	public static Function<RelationInfo, WsRelationData> getRelationInfoToRelationWithFullDetails() {
		return FULL_DETAILS;
	}

	public static Function<RelationInfo, WsRelationData> getRelationInfoToRelationWithBasicDetails() {
		return BASIC_DETAILS;
	}

	private static class RelationInfoToRelation implements Function<RelationInfo, WsRelationData> {

		public static class Builder implements org.apache.commons.lang3.builder.Builder<RelationInfoToRelation> {

			private boolean includeValues;

			private Builder() {
				// use factory method
			}

			@Override
			public RelationInfoToRelation build() {
				return new RelationInfoToRelation(this);
			}

			public Builder includeValues(final boolean includeValues) {
				this.includeValues = includeValues;
				return this;
			}

		}

		public static Builder newInstance() {
			return new Builder();
		}

		private static final Map<String, Object> NO_VALUES = emptyMap();

		private final boolean includeValues;

		private RelationInfoToRelation(final Builder builder) {
			this.includeValues = builder.includeValues;
		}

		@Override
		public WsRelationData apply(final RelationInfo input) {
			final Domain domain = input.getQueryDomain().getDomain();
			final Classe sourceType = input.getSourceType();
			final Classe targetType = input.getTargetType();
			return newRelation() //
					.withId(input.getRelationId()) //
					.withType(domain.getName()) //
					.withSource(newCard() //
							.withType(sourceType.getName()) //
							.withId(input.getSourceId()) //
							.withValue(ATTR_DESCRIPTION, input.getSourceDescription()) //
							.build()) //
					.withDestination(newCard() //
							.withType(targetType.getName()) //
							.withId(input.getTargetId()) //
							.withValue(ATTR_DESCRIPTION, input.getTargetDescription()) //
							.build()) //
					.withValues(includeValues ? adaptOutputValues( //
							domain, //
							input.getRelationAttributes() //
					)
							: NO_VALUES.entrySet()) //
					.build();
		}

		/**
		 *
		 * @param target
		 * @param entries
		 * @return
		 * @deprecated try to use
		 * {@link #getAttributeTransformer(org.cmdbuild.dao.entrytype.CMEntryType)}
		 * instead
		 */
		@Deprecated
		private Iterable<? extends Map.Entry<String, Object>> adaptOutputValues(final EntryType target,
				final Iterable<Map.Entry<String, Object>> entries) {
			final Map<String, Object> values = newHashMap();
			for (final Map.Entry<String, Object> entry : entries) {
				values.put(entry.getKey(), entry.getValue());
			}
			return transformEntries(values, (String key, Object value) -> {
				final Attribute attribute = target.getAttributeOrNull(key);
				final Object _value;
				if (attribute == null) {
					_value = value;
				} else {
					final CardAttributeType<?> attributeType = attribute.getType();
					_value = toClient(attributeType, value);
				}
				return _value;
			}).entrySet();
		}

	};

	public static Function<Map.Entry<String, Object>, Map.Entry<String, Object>> getAttributeTransformer(EntryType entryType) {
		return (entry) -> {
			Attribute attribute = entryType.getAttributeOrNull(entry.getKey());
			if (attribute == null) {
				return entry;
			} else {
				Object value = toClient(attribute.getType(), entry.getValue());
				return new Map.Entry<String, Object>() {
					@Override
					public String getKey() {
						return entry.getKey();
					}

					@Override
					public Object getValue() {
						return value;
					}

					@Override
					public Object setValue(Object v) {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}
}
