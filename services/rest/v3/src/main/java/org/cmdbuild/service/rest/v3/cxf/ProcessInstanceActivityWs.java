package org.cmdbuild.service.rest.v3.cxf;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.cmdbuild.service.rest.v3.model.ProcessActivityWithBasicDetails;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.PROCESS_ACTIVITY_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.PROCESS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.PROCESS_INSTANCE_ID;
import org.cmdbuild.service.rest.v3.cxf.serialization.FlowConverterService;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.workflow.model.Flow;

@Component("v3_processInstanceActivities")
@Path("processes/{" + PROCESS_ID + "}/instances/{" + PROCESS_INSTANCE_ID + "}/activities/")
@Produces(APPLICATION_JSON)
public class ProcessInstanceActivityWs {

	private final WorkflowService workflowService;
	private final FlowConverterService converterService;

	public ProcessInstanceActivityWs(WorkflowService workflowService, FlowConverterService converterService) {
		this.workflowService = checkNotNull(workflowService);
		this.converterService = checkNotNull(converterService);
	}

	@GET
	@Path("")
	public Object read(@PathParam(PROCESS_ID) String classId, @PathParam(PROCESS_INSTANCE_ID) Long cardId) {//TODO pagination

		List<Task> tasks = workflowService.getTaskListForCurrentUserByClassIdAndCardId(classId, cardId);

		List<ProcessActivityWithBasicDetails> res = tasks.stream().map(FlowConverterService::taskToTaskResponseWithBasicDetail).collect(toList());

		return response(res);
	}

	@GET
	@Path("{" + PROCESS_ACTIVITY_ID + "}/")
	public Object read(@PathParam(PROCESS_ID) String classId, @PathParam(PROCESS_INSTANCE_ID) Long cardId, @PathParam(PROCESS_ACTIVITY_ID) String taskId) {
		Flow card = workflowService.getFlowCard(classId, cardId);
		Task task = workflowService.getUserTask(card, taskId);
		return response(converterService.taskToTaskResponseWithFullDetail(card, task));
	}

}
