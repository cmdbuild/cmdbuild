package org.cmdbuild.service.rest.v3.cxf;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.core.q3.DaoService;
import org.springframework.stereotype.Component;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailService;
import org.cmdbuild.email.EmailStatus;
import static org.cmdbuild.email.EmailStatus.ES_DRAFT;
import org.cmdbuild.email.beans.EmailImpl;
import org.cmdbuild.email.beans.EmailImpl.EmailImplBuilder;
import static org.cmdbuild.email.utils.EmailUtils.parseEmailStatus;
import static org.cmdbuild.email.utils.EmailUtils.serializeEmailStatus;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.response;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CARD_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.CLASS_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.EMAIL_ID;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.LIMIT;
import static org.cmdbuild.service.rest.v3.cxf.util.WsSerializationAttrs.START;
import static org.cmdbuild.service.rest.v3.cxf.util.WsResponseUtils.success;
import static org.cmdbuild.utils.date.DateUtils.toIsoDateTime;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@Component("v3_cards_emails")
@Path("{a:classes|processes}/{" + CLASS_ID + "}/{b:cards|instances}/{" + CARD_ID + "}/emails")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class CardEmailWs {

	private final DaoService dao;
	private final EmailService emailService;

	public CardEmailWs(DaoService dao, EmailService emailService) {
		this.dao = checkNotNull(dao);
		this.emailService = checkNotNull(emailService);
	}

	@GET
	@Path(EMPTY)
	public Object readAll(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @QueryParam(LIMIT) Integer limit, @QueryParam(START) Integer offset) {
		Card card = dao.getCard(classId, cardId); //TODO check user permissions
		Collection<Email> list = emailService.getAllForCard(card.getId());
		return response(paged(list, CardEmailWs::serializeBasicEmail, offset, limit));
	}

	@GET
	@Path("{" + EMAIL_ID + "}/")
	public Object read(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(EMAIL_ID) Long emailId) {
		Card card = dao.getCard(classId, cardId); //TODO check user permissions
		return response(serializeDetailedEmail(emailService.getOne(emailId)));//TODO check email id
	}

	@POST
	@Path(EMPTY)
	public Object create(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, WsEmailData emailData) {
		Card card = dao.getCard(classId, cardId); //TODO check user permissions
		return response(serializeDetailedEmail(emailService.create(emailData.toEmail().withReference(card.getId()).build())));
	}

	@PUT
	@Path("{" + EMAIL_ID + "}/")
	public Object update(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(EMAIL_ID) Long emailId, WsEmailData emailData) {
		Card card = dao.getCard(classId, cardId); //TODO check user permissions
		Email current = emailService.getOne(emailId);
		checkArgument(equal(ES_DRAFT, current.getStatus()), "cannot update email with status = %s", current.getStatus());
		return response(serializeDetailedEmail(emailService.update(emailData.toEmail().withReference(card.getId()).withId(emailId).build())));//TODO check email id
	}

	@DELETE
	@Path("{" + EMAIL_ID + "}/")
	public Object delete(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam(EMAIL_ID) Long emailId) {
		Card card = dao.getCard(classId, cardId); //TODO check user permissions
		emailService.delete(emailService.getOne(emailId));//TODO check email id
		return success();
	}

	@POST
	@Path("{emailId}/sync")
	public Object updateEmailWithCardData(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam("emailId") Long emailId, String jsContext) {
		Card card = dao.getCard(classId, cardId); //TODO check user permissions
		Email email = emailService.getOne(emailId);
		//TODO check user permissions
		//TODO auto update email data from template, with current card data
//		Map<String, Object> data = CardImpl.buildCard(card.getType(), cardData.getValues()).getAllValuesAsMap();//TODO card data parsing/processing
//		Collection<Email> emails = emailService.getAllForCard(cardId).stream().map(email -> emailService.syncTemplate(email, data)).collect(toList());
		email = emailService.syncTemplate(email, jsContext);
//		return response(emails.stream().map(CardEmailWs::serializeBasicEmail).collect(toList()));
		return response(serializeDetailedEmail(email));
	}
//	@POST
//	@Path("{emailId}/sync")
//	public Object updateEmailWithCardData(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long cardId, @PathParam("emailId") Long emailId, WsCard cardData) {
//		Card card = dao.getCard(classId, cardId); //TODO check user permissions
//		Email email = emailService.getOne(emailId);
//		//TODO check user permissions
//		//TODO auto update email data from template, with current card data
//		Map<String, Object> data = CardImpl.buildCard(card.getType(), cardData.getValues()).getAllValuesAsMap();//TODO card data parsing/processing
////		Collection<Email> emails = emailService.getAllForCard(cardId).stream().map(email -> emailService.syncTemplate(email, data)).collect(toList());
//		email = emailService.syncTemplate(email, data);
////		return response(emails.stream().map(CardEmailWs::serializeBasicEmail).collect(toList()));
//		return response(serializeDetailedEmail(email));
//	}

	public static FluentMap<String, Object> serializeBasicEmail(Email email) {
		return map(
				"_id", email.getId(),
				"from", email.getFromAddress(),
				"to", email.getToAddresses(),
				"cc", email.getCcAddresses(),
				"bcc", email.getBccAddresses(),
				"subject", email.getSubject(),
				"body", email.getContent(),
				"date", toIsoDateTime(email.getDate()),
				"status", serializeEmailStatus(email.getStatus())
		);
	}

	public static Object serializeDetailedEmail(Email email) {
		return serializeBasicEmail(email).with(
				"account", email.getAccount(),
				"delay", email.getDelay(),
				"template", email.getTemplate(),
				"autoReplyTemplate", email.getAutoReplyTemplate(),
				"keepSynchronization", email.getKeepSynchronization(),
				"noSubjectPrefix", email.getNoSubjectPrefix(),
				"promptSynchronization", email.getPromptSynchronization(),
				"card", email.getReference()
		);
	}

	public static class WsEmailData {

		private final Long delay, account, template, autoReplyTemplate;
		private final String from, to, cc, bcc, subject, body;
		private final Boolean keepSynchronization, noSubjectPrefix, promptSynchronization;
		private final EmailStatus status;

		public WsEmailData(
				@JsonProperty("delay") Long delay,
				@JsonProperty("from") String from,
				@JsonProperty("to") String to,
				@JsonProperty("cc") String cc,
				@JsonProperty("bcc") String bcc,
				@JsonProperty("subject") String subject,
				@JsonProperty("body") String body,
				@JsonProperty("account") Long account,
				@JsonProperty("template") Long template,
				@JsonProperty("autoReplyTemplate") Long autoReplyTemplate,
				@JsonProperty("keepSynchronization") Boolean keepSynchronization,
				@JsonProperty("noSubjectPrefix") Boolean noSubjectPrefix,
				@JsonProperty("promptSynchronization") Boolean promptSynchronization,
				@JsonProperty("status") String status) {
			this.delay = delay;
			this.from = from;
			this.to = to;
			this.cc = cc;
			this.bcc = bcc;
			this.subject = subject;
			this.body = body;
			this.account = account;
			this.template = template;
			this.autoReplyTemplate = autoReplyTemplate;
			this.keepSynchronization = keepSynchronization;
			this.noSubjectPrefix = noSubjectPrefix;
			this.promptSynchronization = promptSynchronization;
			this.status = parseEmailStatus(checkNotBlank(status, "must set email 'status'"));
		}

		public EmailImplBuilder toEmail() {
			return EmailImpl.builder()
					.withDelay(delay)
					.withAccount(account)
					.withBccAddresses(bcc)
					.withCcAddresses(cc)
					.withContent(body)
					.withDelay(delay)
					.withFromAddress(from)
					.withKeepSynchronization(keepSynchronization)
					.withNoSubjectPrefix(noSubjectPrefix)
					.withPromptSynchronization(promptSynchronization)
					.withStatus(status)
					.withSubject(subject)
					.withTemplate(template)
					.withAutoReplyTemplate(autoReplyTemplate)
					.withToAddresses(to);
		}

	}

}
