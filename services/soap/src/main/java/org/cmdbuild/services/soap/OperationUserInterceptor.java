package org.cmdbuild.services.soap;

import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.phase.PhaseInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.cmdbuild.auth.login.AuthenticationStore;
import org.cmdbuild.auth.login.LoginDataImpl;
import org.cmdbuild.auth.user.UserType;
import org.cmdbuild.services.soap.security.LoginAndGroup;
import org.cmdbuild.services.soap.security.PasswordHandler.AuthenticationString;
import org.cmdbuild.services.soap.utils.WebserviceUtils;
import org.slf4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.w3c.dom.Element;

import com.google.common.reflect.AbstractInvocationHandler;
import com.google.common.reflect.Reflection;
import org.cmdbuild.auth.user.UserRepository;
import org.cmdbuild.auth.session.SessionService;
import org.slf4j.LoggerFactory;

public class OperationUserInterceptor extends AbstractSoapInterceptor implements ApplicationContextAware {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private AuthenticationStore authenticationStore;
	@Autowired
	private UserRepository userRepository;

	private ApplicationContext applicationContext;

	private final WSS4JInInterceptor delegate;

	public OperationUserInterceptor(final WSS4JInInterceptor delegate) {
		super(Phase.PRE_INVOKE);
		this.delegate = delegate;
	}

	@Override
	public Collection<PhaseInterceptor<? extends Message>> getAdditionalInterceptors() {
		@SuppressWarnings("unchecked")
		final PhaseInterceptor<? extends Message> delegate
				= Reflection.newProxy(PhaseInterceptor.class, new AbstractInvocationHandler() {

					@Override
					protected Object handleInvocation(final Object proxy, final Method method, final Object[] args)
							throws Throwable {
						try {
							if ("handleMessage".equals(method.getName())) {
								final SoapMessage message = SoapMessage.class.cast(args[0]);
								if (message.hasHeader(QName.valueOf("CMDBuild-Authorization"))) {
									return null;
								}
							}
							return method.invoke(OperationUserInterceptor.this.delegate, args);
						} catch (final InvocationTargetException e) {
							throw e.getCause();
						}
					}

				});
		return asList(delegate);
	}

	@Override
	public Set<QName> getUnderstoodHeaders() {
		return delegate.getUnderstoodHeaders();
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	private SessionService authenticationLogic() {
		return applicationContext.getBean(SessionService.class);
	}

	@Override
	public void handleMessage(final SoapMessage message) throws Fault {
		final Header header = message.getHeader(QName.valueOf("CMDBuild-Authorization"));
		final String token
				= (header == null) ? null : Element.class.cast(header.getObject()).getFirstChild().getTextContent();
		if (isNotBlank(token)) {
			logger.debug("token found '{}'", token);
			if (authenticationLogic().exists(token)) {
				authenticationLogic().setCurrent(token);
			} else {
				throw new IllegalArgumentException(token);
			}
		} else {
			final String authData = new WebserviceUtils().getAuthData(message);
			final AuthenticationString authenticationString = new AuthenticationString(authData);
			storeOperationUser(authenticationString);
		}
	}

	private void storeOperationUser(final AuthenticationString authenticationString) {
		logger.info("storing operation user for authentication string '{}'", authenticationString);
		final LoginAndGroup loginAndGroup = loginAndGroupFor(authenticationString);
		try {
			authenticationStore.setType(UserType.APPLICATION);
			tryLogin(loginAndGroup);
			authenticationStore.setLogin(loginAndGroup.getLogin());
		} catch (final RuntimeException e) {
			logger.warn("error logging in", e);
			if (authenticationString.shouldImpersonate()) {
				/*
				 * fallback to the authentication login, should always work
				 */
				final LoginAndGroup fallbackLogin = authenticationString.getAuthenticationLogin();
				final String current = authenticationLogic().getCurrentSessionIdOrNull();
				if (isNotBlank(current) && authenticationLogic().exists(current)) {
					authenticationLogic().deleteSession(current);
				}
				tryLogin(fallbackLogin);
				authenticationStore.setLogin(loginAndGroup.getLogin());
				authenticationStore.setType(UserType.GUEST);
			} else {
				logger.error("cannot recover this error", e);
				throw e;
			}
		}
		wrapExistingOperationUser(authenticationString);
		logger.info("operation user successfully stored");
	}

	private LoginAndGroup loginAndGroupFor(AuthenticationString authenticationString) {
		logger.debug("getting login and group for authentication string '{}'", authenticationString);
		LoginAndGroup authenticationLogin = authenticationString.getAuthenticationLogin();
		LoginAndGroup impersonationLogin = authenticationString.getImpersonationLogin();
		LoginAndGroup loginAndGroup;
		if (authenticationString.shouldImpersonate()) {
			logger.debug("should authenticate");
			/*
			 * should impersonate but authentication user can be a privileged
			 * service user
			 */
//			if (isPrivilegedServiceUser(authenticationLogin)) {//TODO fix this
//				/*
//				 * we trust that the privileged service user has one group only
//				 */
//				loginAndGroup = LoginAndGroup.newInstance(authenticationLogin.getLogin());
//			} else {
			loginAndGroup = impersonationLogin;
//			}
		} else {
			loginAndGroup = authenticationLogin;
		}
		logger.debug("login and group are '{}'", loginAndGroup);
		return loginAndGroup;
	}

	private void wrapExistingOperationUser(AuthenticationString authenticationString) {
//		String current = authenticationLogic().getCurrentSessionIdOrNull();
//		OperationUser operationUser = authenticationLogic().getUser(current);
//		OperationUser wrapperOperationUser;
		if (authenticationString.shouldImpersonate()) {
			throw new UnsupportedOperationException("TODO");
//			logger.warn
			//TODO
//			LoginUser authenticatedUser = operationUser.getAuthenticatedUser();
//			if (isPrivilegedServiceUser(authenticationString.getAuthenticationLogin())) { //TODO fix this (workflow user name)
//				logger.debug("wrapping operation user with extended username");
//				final String username = authenticationString.getImpersonationLogin().getLogin().getValue();
//				final CMGroup group;
//				if (authenticationString.impersonateForcibly()) {
//					final CMGroup _group = authenticationLogic()
//							.getGroupWithName(authenticationString.getImpersonationLogin().getGroup());
//					if (_group == null) {
//						group = operationUser.getDefaultGroupOrNull();
//					} else {
//						group = _group;
//					}
//				} else {
//					group = operationUser.getDefaultGroupOrNull();
//				}
//				wrapperOperationUser = copyOf(operationUser).withAuthenticatedUser(AuthenticatedUserWithExtendedUsername.from(authenticatedUser, username)).withDefaultGroup(group).build();
//			} 
//			else
//			if (authenticationStore.getType() == UserType.DOMAIN) {
//				/*
//				 * we don't want that a User is represented by a Card of a user
//				 * class, so we login again with the authentication user
//				 *
//				 * at the end we keep the authenticated user with the privileges
//				 * of the impersonated... it's a total mess
//				 */
//				tryLogin(authenticationString.getAuthenticationLogin());
//				final OperationUser _operationUser = authenticationLogic().getUser(current);
//				wrapperOperationUser = copyOf(operationUser).withAuthenticatedUser(AuthenticatedUserWithOtherGroups.from(_operationUser.getAuthenticatedUser(), authenticatedUser)).build();
//				authenticationStore.setType(UserType.DOMAIN);
//			} else {
//				wrapperOperationUser = operationUser;
//			}
//		} else {
//			wrapperOperationUser = operationUser;
		}
//		authenticationLogic().setUser(current, wrapperOperationUser);
	}

//	private boolean isPrivilegedServiceUser(LoginAndGroup loginAndGroup) {
//		LoginUser user = userRepository.getUserOrNull(loginAndGroup.getLogin());
//		return user != null && user.isPrivileged();
//	}
	private void tryLogin(LoginAndGroup loginAndGroup) {
		logger.debug("trying login with '{}'", loginAndGroup);
		String id = authenticationLogic().create(LoginDataImpl.builder()
				.withLoginString(loginAndGroup.getLogin().getValue())
				.withGroupName(loginAndGroup.getGroup())
				.withNoPasswordRequired()
				.allowServiceUser()
				.build());
		authenticationLogic().setCurrent(id);
	}

//	private LoginDataImpl loginFor(final LoginAndGroup loginAndGroup) {
//		return LoginDataImpl.builder() //
//				.withLoginString(loginAndGroup.getLogin().getValue()) //
//				.withGroupName(loginAndGroup.getGroup()) //
//				.withNoPasswordRequired() //
//				.build();
//	}
//	private static class AuthenticatedUserWithExtendedUsername extends ForwardingAuthenticatedUser {
//
//		public static AuthenticatedUserWithExtendedUsername from(final LoginUser authenticatedUser,
//				final String username) {
//			return new AuthenticatedUserWithExtendedUsername(authenticatedUser, username);
//		}
//
//		private static final String SYSTEM = "system";
//		private static final String FORMAT = "%s / %s";
//
//		private final LoginUser authenticatedUser;
//		private final String username;
//
//		private AuthenticatedUserWithExtendedUsername(final LoginUser authenticatedUser,
//				final String username) {
//			this.authenticatedUser = authenticatedUser;
//			this.username = username;
//		}
//
//		@Override
//		protected LoginUser delegate() {
//			return authenticatedUser;
//		}
//
//		@Override
//		public String getUsername() {
//			return String.format(FORMAT, SYSTEM, username);
//		}
//
//		@Override
//		public boolean isPasswordExpired() {
//			return authenticatedUser.isPasswordExpired();
//		}
//
//		@Override
//		public ZonedDateTime getPasswordExpirationTimestamp() {
//			return authenticatedUser.getPasswordExpirationTimestamp();
//		}
//
//		@Override
//		public ZonedDateTime getLastPasswordChangeTimestamp() {
//			return authenticatedUser.getLastPasswordChangeTimestamp();
//		}
//
//		@Override
//		public ZonedDateTime getLastExpiringNotificationTimestamp() {
//			return authenticatedUser.getLastExpiringNotificationTimestamp();
//		}
//
//	}
//
//	private static final class AuthenticatedUserWithOtherGroups extends ForwardingAuthenticatedUser {
//
//		public static AuthenticatedUserWithOtherGroups from(final LoginUser authenticatedUser,
//				final LoginUser userForGroups) {
//			return new AuthenticatedUserWithOtherGroups(authenticatedUser, userForGroups);
//		}
//
//		private final LoginUser authenticatedUser;
//		private final LoginUser userForGroups;
//
//		private AuthenticatedUserWithOtherGroups(final LoginUser authenticatedUser,
//				final LoginUser userForGroups) {
//			this.authenticatedUser = authenticatedUser;
//			this.userForGroups = userForGroups;
//		}
//
//		@Override
//		protected LoginUser delegate() {
//			return authenticatedUser;
//		}
//
//		@Override
//		public List<RoleInfo> getRoleInfos() {
//			return userForGroups.getRoleInfos();
//		}
//
//		@Override
//		public boolean isPasswordExpired() {
//			return authenticatedUser.isPasswordExpired();
//		}
//
//		@Override
//		public ZonedDateTime getPasswordExpirationTimestamp() {
//			return authenticatedUser.getPasswordExpirationTimestamp();
//		}
//
//		@Override
//		public ZonedDateTime getLastPasswordChangeTimestamp() {
//			return authenticatedUser.getLastPasswordChangeTimestamp();
//		}
//
//		@Override
//		public ZonedDateTime getLastExpiringNotificationTimestamp() {
//			return authenticatedUser.getLastExpiringNotificationTimestamp();
//		}
//
//	}
}
