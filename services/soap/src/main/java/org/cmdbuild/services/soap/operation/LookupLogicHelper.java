package org.cmdbuild.services.soap.operation;

import static com.google.common.collect.FluentIterable.from;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

import org.cmdbuild.lookup.Lookup;
import org.cmdbuild.lookup.LookupImpl;
import org.cmdbuild.lookup.LookupService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LookupLogicHelper {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static interface AttributeChecker {

		boolean check(Lookup input);

	}

	private final LookupService logic;

	public LookupLogicHelper(final LookupService lookupLogic) {
		this.logic = lookupLogic;
	}

	public long createLookup(final org.cmdbuild.services.soap.types.Lookup lookup) {
		final LookupImpl lookupDto = transform(lookup);
		return logic.createOrUpdateLookup(lookupDto).getId();
	}

	public boolean updateLookup(final org.cmdbuild.services.soap.types.Lookup lookup) {
		final LookupImpl lookupDto = transform(lookup);
		logic.createOrUpdateLookup(lookupDto);
		return true;
	}

	public boolean disableLookup(final long id) {
//		logic.disableLookup(Long.valueOf(id));
		throw new UnsupportedOperationException("TODO");
//		return true;
	}

	public org.cmdbuild.services.soap.types.Lookup getLookupById(long id) {
		final Lookup lookup = logic.getLookup(id);
		return transform(lookup, true);
	}

	public org.cmdbuild.services.soap.types.Lookup[] getLookupListByCode(final String type, final String code, final boolean parentList) {
		return getLookupListByAttribute(type, (final Lookup input) -> code.equals(input.getCode()), parentList);
	}

	public org.cmdbuild.services.soap.types.Lookup[] getLookupListByDescription(final String type,
			final String description, final boolean parentList) {
		return getLookupListByAttribute(type, (final Lookup input) -> (description == null) || description.equals(input.getDescription()), parentList);
	}

	private org.cmdbuild.services.soap.types.Lookup[] getLookupListByAttribute(final String type,
			final AttributeChecker attributeChecker, final boolean parentList) {
//		final LookupType lookupType = LookupType.newInstance() //
//				.withName(type) //
//				.build();
		final Iterable<Lookup> lookupList = logic.getAllLookup(type);
		return from(lookupList) //
				.filter((Lookup input) -> attributeChecker.check(input)) //
				.transform((Lookup input) -> transform(input, parentList)) //
				.toArray(org.cmdbuild.services.soap.types.Lookup.class);
	}

	private LookupImpl transform(final org.cmdbuild.services.soap.types.Lookup from) {
		return LookupImpl.builder() //
				.withTypeName(from.getType()) //
				.withCode(defaultIfEmpty(from.getCode(), EMPTY)) //
				.withId(from.getId()) //
				.withDescription(from.getDescription()) //
				.withNotes(from.getNotes()) //
				.withParentId(from.getParentId()) //
				.withIndex(from.getPosition()) //
				//				.withActiveStatus(true) //
				.build();
	}

	private org.cmdbuild.services.soap.types.Lookup transform(final Lookup from, final boolean parentList) {
		logger.debug("serializing lookup '{}'", from);
		final org.cmdbuild.services.soap.types.Lookup to = new org.cmdbuild.services.soap.types.Lookup();
		to.setId(from.getId());
		to.setCode(from.getCode());
		to.setDescription(from.getDescription());
		to.setNotes(from.getNotes());
		to.setType(from.getType().getName());
		to.setPosition(from.getIndex());
		if (from.getParent() != null) {
			to.setParentId(from.getParentId());
		}
		if (parentList && from.getParent() != null) {
			to.setParent(transform(from.getParent(), true));
		}
		return to;
	}

}
