package org.cmdbuild.services.soap.security;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.cmdbuild.auth.grant.AuthorizationException;
import org.cmdbuild.auth.login.LoginUserIdentity;
import org.springframework.beans.factory.annotation.Autowired;
import org.cmdbuild.auth.login.AuthenticationService;
import org.cmdbuild.auth.user.LoginUser;
import org.cmdbuild.auth.login.AuthenticationConfiguration;

/**
 * PasswordHandler class is used only with WSSecurity. This class verifies if
 * username and password in SOAP Message Header match with stored CMDBuild
 * credentials.
 */
public class PasswordHandler implements CallbackHandler {

	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private AuthenticationConfiguration authConfiguration;

	@Override
	public void handle(final Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (final Callback callback : callbacks) {
			if (callback instanceof WSPasswordCallback) {
				final WSPasswordCallback pwcb = (WSPasswordCallback) callback;
				final AuthenticationString wsAuthString = new AuthenticationString(pwcb.getIdentifier());
				final LoginUser user = login(pwcb, wsAuthString.getAuthenticationLogin().getLogin());
				if (user == null) {
					throw new UnsupportedCallbackException(pwcb);
				}
			}
		}
	}

	private @Nullable LoginUser login(WSPasswordCallback pwcb, LoginUserIdentity login) throws UnsupportedCallbackException {
		if (pwcb.getUsage() == WSPasswordCallback.USERNAME_TOKEN) {
			pwcb.setPassword(authenticationService.getPassword(login));
			return null;
		} else {
			if (authConfiguration.getForceWSPasswordDigest()) {
				throw new UnsupportedCallbackException(pwcb, "Unsupported authentication method");
			}
			return authenticationService.checkPasswordAndGetUser(login, pwcb.getPassword());
		}
	}

	public static class AuthenticationString {

		private static final String PATTERN = "([^@#!]+(@[^\\.]+\\.[^@#]+)?)((#|!)([^@]+(@[^\\.]+\\.[^@]+)?))?(@([^@\\.]+))?";

		private final LoginAndGroup authenticationLogin;
		private final LoginAndGroup impersonationLogin;
		private final boolean impersonateForcibly;

		public AuthenticationString(final String username) {
			final Pattern pattern = Pattern.compile(PATTERN);
			final Matcher matcher = pattern.matcher(username);
			if (!matcher.find()) {
				// FIXME
				throw new AuthorizationException("login failed");
			}
			final String userOrServiceUser = matcher.group(1);
			final String impersonate = matcher.group(4);
			final String impersonatedUser = matcher.group(5);
			final String group = matcher.group(8);

			authenticationLogin = LoginAndGroup.newInstance(LoginUserIdentity.builder() //
					.withValue(userOrServiceUser) //
					.build(), group);
			impersonateForcibly = defaultIfBlank(impersonate, "#").equals("!");
			if (isNotEmpty(impersonatedUser)) {
				impersonationLogin = LoginAndGroup.newInstance(LoginUserIdentity.builder() //
						.withValue(impersonatedUser) //
						.build(), group);
			} else {
				impersonationLogin = null;
			}
		}

		public LoginAndGroup getAuthenticationLogin() {
			return authenticationLogin;
		}

		public LoginAndGroup getImpersonationLogin() {
			return impersonationLogin;
		}

		public boolean shouldImpersonate() {
			return impersonationLogin != null;
		}

		public boolean impersonateForcibly() {
			return impersonateForcibly;
		}

		@Override
		public String toString() {
			return reflectionToString(this, SHORT_PREFIX_STYLE);
		}

	}
}
