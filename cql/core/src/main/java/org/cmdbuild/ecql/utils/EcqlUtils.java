/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.ecql.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.notNull;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import static com.google.common.collect.ImmutableList.copyOf;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import static java.util.regex.Pattern.quote;
import static java.util.stream.Collectors.toList;
import org.cmdbuild.cql.EcqlException;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.easytemplate.EasytemplateProcessor;
import org.cmdbuild.easytemplate.EasytemplateProcessorImpl;
import org.cmdbuild.ecql.EcqlBindingInfo;
import org.cmdbuild.ecql.EcqlExpression;
import org.cmdbuild.ecql.EcqlId;
import org.cmdbuild.ecql.EcqlSource;
import org.cmdbuild.ecql.inner.EcqlBindingInfoImpl;
import org.cmdbuild.ecql.inner.EcqlExpressionImpl;
import static org.cmdbuild.utils.json.CmJsonUtils.LIST_OF_STRINGS;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.lang.CmdbStringUtils;
import static org.cmdbuild.utils.hash.CmdbuildHashUtils.encodeString;
import static org.cmdbuild.utils.hash.CmdbuildHashUtils.decodeString;

public class EcqlUtils {

	private final static BiMap<EcqlSource, String> MINIFIED_ECQL_SOURCE_KEY_MAP = HashBiMap.create(map(
			EcqlSource.CLASS_ATTRIBUTE, "C",
			EcqlSource.EMBEDDED, "E"
	));

	public static String buildEcqlId(EcqlSource source, Object... sourceId) {
		List<String> keys = asList(checkNotNull(sourceId)).stream().map(CmdbStringUtils::toStringOrNull).collect(toList());
		return buildEcqlId(source, keys.toArray(new String[]{}));
	}

	public static String buildClassAttrEcqlId(Attribute attribute) {
		return buildEcqlId(EcqlSource.CLASS_ATTRIBUTE, attribute.getOwner().getName(), attribute.getName());
	}

//	public static String buildEmbeddedEcqlId(String cqlExpr) {
//		return buildEmbeddedEcqlId(cqlExpr, emptyMap());
//	}
	public static String buildEmbeddedEcqlId(String cqlExpr) {
		return buildEcqlId(EcqlSource.EMBEDDED, checkNotBlank(cqlExpr));
	}

//	public static String buildEcqlId(EcqlSource source, String... sourceId) {
//		return buildEcqlId(source, emptyMap(), sourceId);
//	}
	public static String buildEcqlId(EcqlSource source, String... sourceId) {
		checkArgument(asList(checkNotNull(sourceId)).stream().allMatch(notNull()));
		String ecqlRawId = format("%s%s",
				checkNotNull(MINIFIED_ECQL_SOURCE_KEY_MAP.get(source)),
				//				toJson(map("i", sourceId, "c", context)));
				toJson(sourceId));
		String encodedId = encodeString(ecqlRawId);
		return encodedId;
	}

	public static EcqlId parseEcqlId(String encodedId) {
		try {
			String decodedId = decodeString(encodedId);
			checkArgument(decodedId.length() >= 2);
			String key = decodedId.substring(0, 1),
					value = checkNotBlank(decodedId.substring(1));
			EcqlSource source = checkNotNull(MINIFIED_ECQL_SOURCE_KEY_MAP.inverse().get(key), "ecql source not found for key = '%s'", key);
//			EcqlJsonModel ecqlIdData = fromJson(value, EcqlJsonModel.class);
			List<String> idList = fromJson(value, LIST_OF_STRINGS);//ecqlIdData.idList;
//			Map<String, Object> context = ecqlIdData.context;
			return new EcqlIdImpl(source, idList);
		} catch (Exception ex) {
			throw new EcqlException(ex, "error parsing ecqlId = '%s'", encodedId);
		}
	}

//	private static class EcqlJsonModel {
//
//		private final List<String> idList;
//		private final Map<String, Object> context;
//
//		public EcqlJsonModel(@JsonProperty("i") List<String> idList, @JsonProperty("c") Map<String, Object> context) {
//			this.idList = ImmutableList.copyOf(idList);
//			this.context = firstNonNull(context, emptyMap());
//		}
//
//	}
	public static EcqlBindingInfo getEcqlBindingInfoForExpr(EcqlExpression expression) {
		return getEcqlBindingInfoForExpr(expression.getEcql(), expression.getContext());
	}

	public static String resolveEcqlXa(String ecql, Map<String, Object> xaContext) {
		return EasytemplateProcessorImpl.builder()
				.withResolver("xa", xaContext::get)
				.build().processExpression(ecql);
	}

	public static EcqlBindingInfo getEcqlBindingInfoForExpr(String ecql, Map<String, Object> context) {
		checkNotBlank(ecql, "ecql expression is null or blank");

		List<String> clientBindings = list(),
				serverBindings = list(),
				xaBindings = list();

		AtomicReference<EasytemplateProcessor> processor = new AtomicReference<>();

		processor.set(EasytemplateProcessorImpl.builder()
				.withResolver("client", (key) -> clientBindings.add(key))
				.withResolver("server", (key) -> serverBindings.add(key))
				.withResolver("xa", (key) -> xaBindings.add(key))
				.withResolver("js", (key) -> processor.get().processExpression(getJsExprFromContext(key, context)))
				.build());

		processor.get().processExpression(ecql);

		return EcqlBindingInfoImpl.builder()
				.withClientBindings(clientBindings)
				.withServerBindings(serverBindings)
				.withXaBindings(xaBindings)
				.build();
	}

	public static String getJsExprFromContext(String key, Map<String, Object> context) {
		return checkNotBlank((String) context.get(key), "value not found in context for js key = '%s'", key);
	}

	public static EcqlExpression getEcqlExpressionFromClassAttributeFilter(Attribute attribute) {
		String filter = attribute.getFilter();
		checkNotBlank(filter);
		//TODO check syntax??

		Map<String, Object> context = map(attribute.getMetadata().getAll());
		attribute.getMetadata().getAll().entrySet().stream()
				.filter((e) -> e.getKey().startsWith("system.template."))
				.forEach((e) -> context.put(e.getKey().replaceFirst(quote("system.template."), ""), e.getValue()));

		return new EcqlExpressionImpl(filter, context);
	}

	private final static class EcqlIdImpl implements EcqlId<List<String>> {

		private final EcqlSource source;
		private final List<String> id;
//		private final Map<String, Object> context;

//		public EcqlIdImpl(EcqlSource source, List<String> id, Map<String, Object> context) {
		public EcqlIdImpl(EcqlSource source, List<String> id) {
			this.source = checkNotNull(source);
			this.id = copyOf(checkNotNull(id));
//			this.context = map(checkNotNull(context)).immutable();
//			checkArgument(id.stream().allMatch(not(StringUtils::isBlank)));
		}

		@Override
		public EcqlSource getSource() {
			return source;
		}

		@Override
		public List<String> getId() {
			return id;
		}

//		@Override
//		public Map<String, Object> getContext() {
//			return context;
//		}
		@Override
		public String toString() {
			return "EcqlIdImpl{" + "source=" + source + ", id=" + id + '}';
		}

	}

}
