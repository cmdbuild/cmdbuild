package org.cmdbuild.cql;

import java.util.Map;

public interface CqlService {

	void compileAndAnalyze(String query, Map<String, Object> context, CqlProcessingCallback callback);

}
