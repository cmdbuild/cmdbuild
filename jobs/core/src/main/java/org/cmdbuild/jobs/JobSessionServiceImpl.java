/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.jobs;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.auth.login.LoginDataImpl;
import org.cmdbuild.auth.session.SessionService;
import static org.cmdbuild.jobs.JobExecutorService.JOBUSER_NOBODY;
import static org.cmdbuild.jobs.JobExecutorService.JOBUSER_SYSTEM;
import org.cmdbuild.requestcontext.RequestContextService;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.stereotype.Component;

@Component
public class JobSessionServiceImpl implements JobSessionService {

	private final RequestContextService requestContextService;
	private final SessionService sessionService;

	public JobSessionServiceImpl(RequestContextService requestContextService, SessionService sessionService) {
		this.requestContextService = checkNotNull(requestContextService);
		this.sessionService = checkNotNull(sessionService);
	}

	@Override
	public void createJobSessionContext(String user, String context) {
		requestContextService.initCurrentRequestContext(context);
		createSession(user);
	}

	@Override
	public void destroyJobSessionContext() {
		sessionService.deleteCurrentSessionIfExists();
		requestContextService.destroyCurrentRequestContext();//TODO destroy current session, if any
	}

	private void createSession(String user) {
		switch (checkNotBlank(user)) {
			case JOBUSER_NOBODY:
				break;
			case JOBUSER_SYSTEM:
				sessionService.createAndSet(LoginDataImpl.builder().withLoginString("admin").withNoPasswordRequired().build()); //TODO use fake system user - not admin
				break;
			default:
				sessionService.createAndSet(LoginDataImpl.builder().withLoginString(user).withNoPasswordRequired().build());
		}
	}

}
