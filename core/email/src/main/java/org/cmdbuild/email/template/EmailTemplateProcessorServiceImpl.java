/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.template;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import static java.util.stream.Collectors.joining;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.math.NumberUtils.isNumber;
import org.cmdbuild.auth.role.Role;
import org.cmdbuild.auth.role.RoleRepository;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.auth.user.UserData;
import org.cmdbuild.auth.user.UserRepository;
import static org.cmdbuild.cql.CqlUtils.getCqlSelectElements;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.data.filter.beans.CqlFilterImpl;
import org.cmdbuild.easytemplate.EasytemplateProcessor;
import org.cmdbuild.easytemplate.EasytemplateProcessorImpl;
import org.cmdbuild.easytemplate.EasytemplateService;
import org.cmdbuild.easytemplate.store.EasytemplateRepository;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailTemplate;
import org.cmdbuild.email.beans.EmailImpl;
import static org.cmdbuild.utils.date.DateUtils.toUserReadableDateTime;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toLong;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import static org.cmdbuild.utils.lang.CmdbStringUtils.mapToLoggableStringLazy;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EmailTemplateProcessorServiceImpl implements EmailTemplateProcessorService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EasytemplateRepository easytemplateRepository;
	private final EasytemplateService easytemplateService;
	private final UserRepository userRepository;
	private final OperationUserSupplier userSupplier;
	private final RoleRepository roleRepository;
	private final DaoService daoService;

	public EmailTemplateProcessorServiceImpl(EasytemplateRepository easytemplateRepository, EasytemplateService easytemplateService, UserRepository userRepository, OperationUserSupplier userSupplier, RoleRepository roleRepository, DaoService daoService) {
		this.easytemplateRepository = checkNotNull(easytemplateRepository);
		this.easytemplateService = checkNotNull(easytemplateService);
		this.userRepository = checkNotNull(userRepository);
		this.userSupplier = checkNotNull(userSupplier);
		this.roleRepository = checkNotNull(roleRepository);
		this.daoService = checkNotNull(daoService);
	}

	@Override
	public Email applyEmailTemplate(Email email, EmailTemplate template, String jsContenxt) {
		return new EmailTemplateProcessor(emptyMap(), template, null, checkNotBlank(jsContenxt)).processEmail(email);
	}

	@Override
	public Email applyEmailTemplate(Email email, EmailTemplate template, Card card, Email receivedEmail) {
		return new EmailTemplateProcessor(card.getAllValuesAsMap(), template, checkNotNull(receivedEmail), null).processEmail(email);
	}

	private class EmailTemplateProcessor {

		private final Map<String, Object> data;
		private final EmailTemplate template;
		private final EasytemplateProcessor processor;
		private final Email receivedEmail;

		public EmailTemplateProcessor(Map<String, Object> data, EmailTemplate template, @Nullable Email receivedEmail, @Nullable String jsContext) {
			this.data = checkNotNull(data);
			this.template = checkNotNull(template);
			this.receivedEmail = receivedEmail;

			Map<String, Object> dataForResolver = map(data)//TODO
					.with("CurrentRole", Optional.ofNullable(userSupplier.getUser().getDefaultGroupOrNull()).map(Role::getId).orElse(null));

			processor = (isBlank(jsContext) ? EasytemplateProcessorImpl.builder() : EasytemplateProcessorImpl.copyOf(easytemplateService.getDefaultProcessorWithJsContext(jsContext)))
					.withResolver("user", this::processUserEmailExpr)
					.withResolver("group", this::processGroupEmailExpr)
					.withResolver("groupUsers", this::processGroupUsersExpr)
					.withResolver("email", this::processReceivedEmailExpr)
					.withResolver("card", (k) -> toStringOrEmpty(dataForResolver.get(k)))
					.withResolver("cql", this::processCqlExpr)
					.withResolver("dbtmpl", easytemplateRepository::getTemplate)
					.build();

			/*
			TODO:
			
							.withEngine(emptyStringOnNull(nullOnError(map( //
										EngineBasedMapper.newInstance() //
												.withText(email.getContent()) //
												.withEngine(task.getMapperEngine()) //
												.build() //
												.map() //
										))), //
										MAPPER_PREFIX) //
			 */
		}

		public Email processEmail(Email email) {
			logger.debug("processing email = {} with template = {}", email, template);
			logger.trace("data for email template = \n\n{}\n", mapToLoggableStringLazy(data));
			email = EmailImpl.copyOf(email).accept((builder) -> {
				processEmailTemplate(builder::withFromAddress, template.getFrom());
				processEmailTemplate(builder::withBccAddresses, template.getBcc());
				processEmailTemplate(builder::withContent, template.getBody());
				processEmailTemplate(builder::withBccAddresses, template.getCc());
				processEmailTemplate(builder::withSubject, template.getSubject());
				processEmailTemplate(builder::withToAddresses, template.getTo());
			}).withAccount(template.getAccount())
					.withDelay(template.getDelay())
					.withKeepSynchronization(template.getKeepSynchronization())
					.withPromptSynchronization(template.getPromptSynchronization())
					.build();

			return email;
		}

		private void processEmailTemplate(Consumer<String> setter, @Nullable String expression) {
			if (isBlank(expression)) {
				setter.accept(expression);
			} else {
				String value = processEmailTemplateValue(expression);
				setter.accept(value);
			}
		}

		private String processEmailTemplateValue(String expression) {
			logger.debug("process email template expr = {}", abbreviate(expression));
			String value = processor.processExpression(expression);
			logger.debug("processsed email template expr, output value = {}", abbreviate(value));
			return value;
		}

		private String processUserEmailExpr(String username) {
			checkNotBlank(username, "username expr is blank");
			return userRepository.getUserByUsername(username).getEmail();
		}

		private String processGroupEmailExpr(String group) {
			checkNotBlank(group, "group expr is blank");
			return roleRepository.getByNameOrId(group).getEmail();
		}

		private String processGroupUsersExpr(String group) {
			checkNotBlank(group, "groupUsers expr is blank");
			long roleId;
			if (isNumber(group)) {
				roleId = toLong(group);
			} else {
				roleId = roleRepository.getGroupWithName(group).getId();
			}
			return userRepository.getAllWithRole(roleId).stream().map(UserData::getEmail).filter(StringUtils::isNotBlank).distinct().sorted().collect(joining(","));
		}

		private String processReceivedEmailExpr(String expr) {
			checkNotNull(receivedEmail, "invalid email expr - no received email is available");
			checkNotBlank(expr, "email expr is blank");
			switch (expr) {
				case "from":
					return receivedEmail.getFromAddress();
				case "to":
					return receivedEmail.getToAddresses();
				case "cc":
					return receivedEmail.getCcAddresses();
				case "bcc":
					return receivedEmail.getBccAddresses();
				case "date":
					return toUserReadableDateTime(receivedEmail.getDate());
				case "subject":
					return receivedEmail.getSubject();
				case "content":
					return receivedEmail.getContent();
				default:
					throw new IllegalArgumentException(format("unsupported email expr = %s", expr));
			}
		}

		private String processCqlExpr(@Nullable String expr) {
			logger.debug("process email template cql expr = {}", expr);
			Card card = daoService.selectAll().where(CqlFilterImpl.build(expr).toCmdbFilter()).getCard();
			String field = getOnlyElement(getCqlSelectElements(expr));
			return card.get(field, String.class, "");
		}
	}
}
