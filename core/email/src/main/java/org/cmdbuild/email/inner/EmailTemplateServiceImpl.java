/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.inner;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_CODE;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.QueryBuilder.EQ;
import org.cmdbuild.email.EmailTemplate;
import org.cmdbuild.email.EmailTemplateService;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.stereotype.Component;

@Component
public class EmailTemplateServiceImpl implements EmailTemplateService {

	private final DaoService dao;

	public EmailTemplateServiceImpl(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@Override
	public List<EmailTemplate> getAll() {
		return dao.selectAll().from(EmailTemplate.class).orderBy(ATTR_CODE).asList();
	}

	@Override
	public EmailTemplate getOne(long id) {
		return dao.getById(EmailTemplate.class, id).toModel();
	}

	@Override
	public EmailTemplate getByName(String name) {
		return checkNotNull(dao.selectAll().from(EmailTemplate.class).where(ATTR_CODE, EQ, checkNotBlank(name)).getOneOrNull(), "email template not found for name = %s", name);
	}

}
