/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.template;

import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailTemplate;

public interface EmailTemplateProcessorService {

	Email applyEmailTemplate(Email email, EmailTemplate template, String jsContext);

	Email applyEmailTemplate(Email email, EmailTemplate template, Card data, Email receivedEmail);

}
