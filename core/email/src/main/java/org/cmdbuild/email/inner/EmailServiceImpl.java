/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.inner;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.EventBus;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailAttachment;
import org.cmdbuild.email.beans.EmailAttachmentImpl;
import org.cmdbuild.email.EmailService;
import static org.cmdbuild.email.EmailStatus.ES_OUTGOING;
import org.cmdbuild.email.EmailTemplate;
import org.cmdbuild.email.EmailTemplateService;
import org.cmdbuild.email.beans.EmailImpl;
import static org.cmdbuild.email.beans.EmailImpl.EMAIL_CLASS_NAME;
import org.cmdbuild.email.data.EmailRepository;
import org.cmdbuild.email.template.EmailTemplateProcessorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceImpl implements EmailService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus = new EventBus();
	private final EmailRepository repository;
	private final EmailTemplateService templateRepository;
	private final EmailTemplateProcessorService templateProcessorService;
	private final DmsService dmsService;

	public EmailServiceImpl(EmailRepository repository, EmailTemplateService templateRepository, EmailTemplateProcessorService templateProcessorService, DmsService dmsService) {
		this.repository = checkNotNull(repository);
		this.templateRepository = checkNotNull(templateRepository);
		this.templateProcessorService = checkNotNull(templateProcessorService);
		this.dmsService = checkNotNull(dmsService);
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public List<Email> getAllForCard(long reference) {
		return repository.getAllForCard(reference);
	}

	@Override
	public Email getOne(long emailId) {
		return repository.getOne(emailId);
	}

	@Override
	public Email create(Email email) {
		logger.debug("create email = {}", email);
		email = repository.create(email);
		checkOutgoing(email);
		return email;
	}

	@Override
	public Email update(Email email) {
		logger.debug("update email = {}", email);
		email = repository.update(email);
		checkOutgoing(email);
		return email;
	}

	@Override
	public void delete(Email email) {
		logger.debug("delete email = {}", email);
		repository.delete(email);
	}

	@Override
	public Email syncTemplate(Email email, String jsContext) {
		logger.debug("sync template for email = {}", email);
		checkArgument(email.hasTemplate(), "unable to sync email without template");
		EmailTemplate template = templateRepository.getOne(email.getTemplate());
		email = templateProcessorService.applyEmailTemplate(email, template, jsContext);
		email = update(email);
		return email;
	}

	@Override
	public List<Email> getAllForOutgoingProcessing() {
		return repository.getAllForOutgoingProcessing();
	}

	@Override
	public List<EmailAttachment> getEmailAttachments(long emailId) {
		return dmsService.getCardAttachments(EMAIL_CLASS_NAME, emailId).stream().map(a
				-> EmailAttachmentImpl.builder()
						.withFileName(a.getFileName())
						.withMimeType(a.getMimeType())
						.withData(dmsService.getDocumentContent(a.getDocumentId()))
						.build()).collect(toList());
	}

	@Override
	public Email loadAttachments(Email email) {
		if (email.getId() == null) {
			return email;
		} else {
			return EmailImpl.copyOf(email).withAttachments(getEmailAttachments(email.getId())).build();
		}
	}

	private void checkOutgoing(Email email) {
		if (equal(email.getStatus(), ES_OUTGOING)) {
			logger.debug("outgoing email processed, trigger email queue (email = {})", email);
			eventBus.post(NewOutgoingEmailEvent.INSTANCE);
		}
	}

}
