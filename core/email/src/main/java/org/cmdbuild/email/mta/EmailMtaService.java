package org.cmdbuild.email.mta;

import java.util.List;
import java.util.function.Consumer;
import javax.annotation.Nullable;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailAccount;

public interface EmailMtaService {

	void send(Email email);

	void receive(EmailAccount account, String incomingFolder, @Nullable String receivedFolder, @Nullable String rejectedFolder, Consumer<Email> callback);

	List<Email> receive(EmailAccount account, String incomingFolder, @Nullable String receivedFolder);

}
