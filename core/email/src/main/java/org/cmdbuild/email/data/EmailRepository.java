package org.cmdbuild.email.data;

import java.util.List;
import org.cmdbuild.email.Email;

public interface EmailRepository {

	Email create(Email email);

	List<Email> getAllForCard(long reference);

	Email getOne(long emailId);

	Email update(Email email);

	void delete(Email email);

	List<Email> getAllForOutgoingProcessing();

}
