/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.beans;

import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailStatus;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import java.time.ZonedDateTime;
import static java.util.Collections.emptyList;
import java.util.List;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.abbreviate;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_BEGINDATE;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import org.cmdbuild.email.EmailAttachment;
import static org.cmdbuild.email.beans.EmailImpl.EMAIL_CLASS_NAME;
import static org.cmdbuild.email.utils.EmailUtils.parseEmailStatus;
import static org.cmdbuild.email.utils.EmailUtils.serializeEmailStatus;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.firstNotNull;

@CardMapping(EMAIL_CLASS_NAME)
public class EmailImpl implements Email {

	public static final String EMAIL_CLASS_NAME = "Email";

	private final Long id, reference, delay, account, template, autoReplyTemplate;
	private final String fromAddress, toAddresses, ccAddresses, bccAddresses, subject, content;
	private final EmailStatus status;
	private final boolean noSubjectPrefix, keepSynchronization, promptSynchronization;
	private final ZonedDateTime date;
	private final int errorCount;
	private final List<EmailAttachment> attachments;

	private EmailImpl(EmailImplBuilder builder) {
		this.id = builder.id;
		this.reference = builder.reference;
		this.fromAddress = trimToNull(builder.fromAddress);
		this.toAddresses = trimToNull(builder.toAddresses);
		this.ccAddresses = trimToNull(builder.ccAddresses);
		this.bccAddresses = trimToNull(builder.bccAddresses);
		this.subject = builder.subject;
		this.content = builder.content;
		this.account = builder.account;
		this.template = builder.template;
		this.autoReplyTemplate = builder.autoReplyTemplate;
		this.delay = builder.delay;
		this.status = checkNotNull(builder.status);
		this.noSubjectPrefix = firstNotNull(builder.noSubjectPrefix, false);
		this.keepSynchronization = firstNotNull(builder.keepSynchronization, false);
		this.promptSynchronization = firstNotNull(builder.promptSynchronization, false);
		this.date = builder.date;
		this.attachments = ImmutableList.copyOf(firstNotNull(builder.attachments, emptyList()));
		this.errorCount = firstNotNull(builder.errorCount, 0);
	}

	@CardAttr(ATTR_ID)
	@Override
	@Nullable
	public Long getId() {
		return id;
	}

	@Nullable
	@Override
	@CardAttr("Card")
	public Long getReference() {
		return reference;
	}

	@Nullable
	@Override
	@CardAttr
	public String getFromAddress() {
		return fromAddress;
	}

	@Nullable
	@Override
	@CardAttr
	public String getToAddresses() {
		return toAddresses;
	}

	@Nullable
	@Override
	@CardAttr
	public String getCcAddresses() {
		return ccAddresses;
	}

	@Nullable
	@Override
	@CardAttr
	public String getBccAddresses() {
		return bccAddresses;
	}

	@Nullable
	@Override
	@CardAttr
	public String getSubject() {
		return subject;
	}

	@Nullable
	@Override
	@CardAttr
	public String getContent() {
		return content;
	}

	@Nullable
	@Override
	@CardAttr
	public Long getAccount() {
		return account;
	}

	@Nullable
	@Override
	@CardAttr
	public Long getTemplate() {
		return template;
	}

	@Nullable
	@Override
	@CardAttr("NotifyWith")
	public Long getAutoReplyTemplate() {
		return autoReplyTemplate;
	}

	@Nullable
	@Override
	@CardAttr
	public Long getDelay() {
		return delay;
	}

	@Override
	public EmailStatus getStatus() {
		return status;
	}

	@CardAttr("EmailStatus")
	public String getStatusAsString() {
		return serializeEmailStatus(getStatus());
	}

	@Override
	@CardAttr
	public boolean getNoSubjectPrefix() {
		return noSubjectPrefix;
	}

	@Override
	@CardAttr
	public boolean getKeepSynchronization() {
		return keepSynchronization;
	}

	@Override
	@CardAttr
	public boolean getPromptSynchronization() {
		return promptSynchronization;
	}

	@Override
	@CardAttr(value = ATTR_BEGINDATE, writeToDb = false)
	@Nullable
	public ZonedDateTime getDate() {
		return date;
	}

	@Override
	@CardAttr
	public int getErrorCount() {
		return errorCount;
	}

	@Override
	public List<EmailAttachment> getAttachments() {
		return attachments;
	}

	@Override
	public String toString() {
		return "EmailImpl{" + "id=" + id + ", reference=" + reference + ", subject=" + abbreviate(subject, 40) + ", status=" + status + ", errorCount=" + errorCount + '}';
	}

	public static EmailImplBuilder builder() {
		return new EmailImplBuilder();
	}

	public static EmailImplBuilder copyOf(Email source) {
		return new EmailImplBuilder()
				.withId(source.getId())
				.withReference(source.getReference())
				.withFromAddress(source.getFromAddress())
				.withToAddresses(source.getToAddresses())
				.withCcAddresses(source.getCcAddresses())
				.withBccAddresses(source.getBccAddresses())
				.withSubject(source.getSubject())
				.withContent(source.getContent())
				.withAccount(source.getAccount())
				.withTemplate(source.getTemplate())
				.withAutoReplyTemplate(source.getAutoReplyTemplate())
				.withDelay(source.getDelay())
				.withDate(source.getDate())
				.withStatus(source.getStatus())
				.withNoSubjectPrefix(source.getNoSubjectPrefix())
				.withKeepSynchronization(source.getKeepSynchronization())
				.withPromptSynchronization(source.getPromptSynchronization())
				.withErrorCount(source.getErrorCount())
				.withAttachments(source.getAttachments());
	}

	public static class EmailImplBuilder implements Builder<EmailImpl, EmailImplBuilder> {

		private Long id;
		private Long reference, account, template, autoReplyTemplate;
		private String fromAddress;
		private String toAddresses;
		private String ccAddresses;
		private String bccAddresses;
		private String subject;
		private String content;
		private Long delay;
		private EmailStatus status;
		private Boolean noSubjectPrefix;
		private Boolean keepSynchronization;
		private Boolean promptSynchronization;
		private ZonedDateTime date;
		private Integer errorCount;
		private List<EmailAttachment> attachments;

		public EmailImplBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public EmailImplBuilder withReference(Long reference) {
			this.reference = reference;
			return this;
		}

		public EmailImplBuilder withFromAddress(String fromAddress) {
			this.fromAddress = fromAddress;
			return this;
		}

		public EmailImplBuilder withToAddresses(String toAddresses) {
			this.toAddresses = toAddresses;
			return this;
		}

		public EmailImplBuilder withCcAddresses(String ccAddresses) {
			this.ccAddresses = ccAddresses;
			return this;
		}

		public EmailImplBuilder withBccAddresses(String bccAddresses) {
			this.bccAddresses = bccAddresses;
			return this;
		}

		public EmailImplBuilder withSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public EmailImplBuilder withContent(String content) {
			this.content = content;
			return this;
		}

		public EmailImplBuilder withAccount(Long account) {
			this.account = account;
			return this;
		}

		public EmailImplBuilder withTemplate(Long template) {
			this.template = template;
			return this;
		}

		public EmailImplBuilder withAutoReplyTemplate(Long autoReplyTemplate) {
			this.autoReplyTemplate = autoReplyTemplate;
			return this;
		}

		public EmailImplBuilder withDelay(Long delay) {
			this.delay = delay;
			return this;
		}

		public EmailImplBuilder withDate(ZonedDateTime date) {
			this.date = date;
			return this;
		}

		public EmailImplBuilder withStatus(EmailStatus status) {
			this.status = status;
			return this;
		}

		public EmailImplBuilder withErrorCount(Integer errorCount) {
			this.errorCount = errorCount;
			return this;
		}

		public EmailImplBuilder withStatusAsString(String status) {
			return this.withStatus(parseEmailStatus(status));
		}

		public EmailImplBuilder withNoSubjectPrefix(Boolean noSubjectPrefix) {
			this.noSubjectPrefix = noSubjectPrefix;
			return this;
		}

		public EmailImplBuilder withKeepSynchronization(Boolean keepSynchronization) {
			this.keepSynchronization = keepSynchronization;
			return this;
		}

		public EmailImplBuilder withPromptSynchronization(Boolean promptSynchronization) {
			this.promptSynchronization = promptSynchronization;
			return this;
		}

		public EmailImplBuilder withAttachments(List<EmailAttachment> attachments) {
			this.attachments = attachments;
			return this;
		}

		@Override
		public EmailImpl build() {
			return new EmailImpl(this);
		}

	}
}
