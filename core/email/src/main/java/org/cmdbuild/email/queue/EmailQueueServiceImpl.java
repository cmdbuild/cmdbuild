/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.queue;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.Subscribe;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailService;
import org.cmdbuild.email.EmailService.NewOutgoingEmailEvent;
import static org.cmdbuild.email.EmailStatus.ES_ERROR;
import static org.cmdbuild.email.EmailStatus.ES_SENT;
import org.cmdbuild.email.beans.EmailImpl;
import org.cmdbuild.email.mta.EmailMtaService;
import org.cmdbuild.jobs.JobExecutorService;
import static org.cmdbuild.jobs.JobExecutorService.JOBUSER_SYSTEM;
import org.cmdbuild.lock.ItemLock;
import org.cmdbuild.lock.LockService;
import org.cmdbuild.scheduler.spring.ScheduledJob;
import static org.cmdbuild.scheduler.spring.ScheduledJobClusterMode.CM_RUN_ON_SINGLE_NODE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.EmailQueueConfiguration;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_NOTRUNNING;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;

@Component
public class EmailQueueServiceImpl implements EmailQueueService, SystemService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LockService lockService;
	private final JobExecutorService jobExecutorService;
	private final EmailMtaService mtaService;
	private final EmailService emailService;
	private final EmailQueueConfiguration config;
	private final AtomicReference<SystemServiceStatus> status = new AtomicReference<>(SS_NOTRUNNING);

	public EmailQueueServiceImpl(EmailMtaService mtaService, EmailService emailService, LockService lockService, JobExecutorService jobExecutorService, EmailQueueConfiguration emailConfiguration) {
		this.mtaService = checkNotNull(mtaService);
		this.emailService = checkNotNull(emailService);
		this.lockService = checkNotNull(lockService);
		this.jobExecutorService = checkNotNull(jobExecutorService);
		this.config = checkNotNull(emailConfiguration);
		emailService.getEventBus().register(new Object() {
			@Subscribe
			public void handleNewOutgoingEmailEvent(NewOutgoingEmailEvent event) {
				if (isRunning()) {
					triggerEmailQueue();//TODO do not hard fail if queue lock is not available
				}
			}
		});
		if (config.isQueueProcessingEnabled()) {
			status.set(SS_READY);
		}
	}

	@Override
	public String getServiceName() {
		return "Email Queue";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		return status.get();
	}

	private boolean isRunning() {
		return SS_READY.equals(getServiceStatus());
	}

	@Override
	public void startService() {
		checkArgument(config.isQueueProcessingEnabled());
		status.set(SS_READY);
		triggerEmailQueue();
	}

	@Override
	public void stopService() {
		checkArgument(isRunning());
		status.set(SS_NOTRUNNING);
	}

	@Override
	public void triggerEmailQueue() {
		jobExecutorService.executeJobAs(this::doProcessEmailQueue, JOBUSER_SYSTEM);
	}

	@ScheduledJob(value = "0 10 * * * ?", clusterMode = CM_RUN_ON_SINGLE_NODE, user = JOBUSER_SYSTEM)//run every 10 minutes
	public void processEmailQueue() {
		if (isRunning()) {
			doProcessEmailQueue();
		} else {
			logger.debug("email queue processing is disabled - skipping");
		}
	}

	private void doProcessEmailQueue() {
		try {
			logger.debug("processing email queue");
			ItemLock lock = lockService.aquireLockOrFail("org.cmdbuild.email.QUEUE", LockService.LockScope.SESSION);//TODO expire this lock eventually
			try {
				List<Email> outgoing = emailService.getAllForOutgoingProcessing();
				if (!outgoing.isEmpty()) {
					logger.info("processing {} outgoing mail", outgoing.size());
					outgoing.forEach(this::sendMail);
				}
			} finally {
				lockService.releaseLock(lock);
			}
		} catch (Exception ex) {
			logger.error("error processing email queue", ex);
		}
	}

	private void sendMail(Email email) {
		try {
			logger.info("sending mail = {}", email);
			mtaService.send(emailService.loadAttachments(email));
			emailService.update(EmailImpl.copyOf(email).withStatus(ES_SENT).build());
		} catch (Exception ex) {
			logger.error("error sending mail = {}", email, ex);
			emailService.update(EmailImpl.copyOf(email).withStatus(ES_ERROR).build());
		}
	}

}
