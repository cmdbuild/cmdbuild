/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.inner;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.onlyElement;
import static com.google.common.collect.MoreCollectors.toOptional;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.email.EmailAccount;
import org.cmdbuild.email.EmailAccountService;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.stereotype.Component;

@Component
public class EmailAccountServiceImpl implements EmailAccountService {

	private final DaoService dao;
	private final Holder<List<EmailAccount>> allAccountCache;
	private final CmdbCache<String, Optional<EmailAccount>> accountByNameCache;
	private final CmdbCache<String, EmailAccount> accountByIdCache;

	public EmailAccountServiceImpl(DaoService dao, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		allAccountCache = cacheService.newHolder("email_account_all");
		accountByNameCache = cacheService.newCache("email_account_by_name");
		accountByIdCache = cacheService.newCache("email_account_by_id");
	}

	private void invalidateCache() {
		allAccountCache.invalidate();
		accountByNameCache.invalidateAll();
		accountByIdCache.invalidateAll();
	}

	@Override
	public List<EmailAccount> getAll() {
		return allAccountCache.get(this::doGetAll);
	}

	@Override
	public @Nullable
	EmailAccount getAccountOrNull(String name) {
		return accountByNameCache.get(checkNotBlank(name), () -> Optional.ofNullable(doGetAccountOrNull(name))).orElse(null);
	}

	@Override
	public EmailAccount getAccount(long id) {
		return accountByIdCache.get(Long.toString(id), () -> doGetAccount(id));
	}

	@Override
	public EmailAccount create(EmailAccount account) {
		account = dao.create(account);
		invalidateCache();
		return account;
	}

	@Override
	public EmailAccount update(EmailAccount account) {
		account = dao.update(account);
		invalidateCache();
		return account;
	}

	@Override
	public void delete(long accountId) {
		dao.delete(EmailAccount.class, accountId);
		invalidateCache();
	}

	private List<EmailAccount> doGetAll() {
		return dao.selectAll().from(EmailAccount.class).asList();
	}

	private @Nullable
	EmailAccount doGetAccountOrNull(String name) {
		return getAll().stream().filter((a) -> equal(a.getName(), name)).collect(toOptional()).orElse(null);
	}

	private EmailAccount doGetAccount(long id) {
		return checkNotNull(getAll().stream().filter((a) -> equal(a.getId(), id)).collect(toOptional()).orElse(null), "account not found for id = %s", id);
	}

}
