/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.utils;

import org.cmdbuild.email.EmailStatus;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.parseEnum;

public class EmailUtils {

	public static String serializeEmailStatus(EmailStatus status) {
		return status.name().toLowerCase().replaceFirst("es_", "");
	}

	public static EmailStatus parseEmailStatus(String status) {
		return parseEnum(status, EmailStatus.class);
	}

}
