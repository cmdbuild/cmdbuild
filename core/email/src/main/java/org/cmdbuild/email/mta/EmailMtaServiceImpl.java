package org.cmdbuild.email.mta;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.Lists.transform;
import static com.google.common.collect.Maps.transformValues;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import java.security.Security;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.annotation.Nullable;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.tika.Tika;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.EmailAccount;
import org.cmdbuild.email.EmailAccountService;
import org.cmdbuild.email.EmailAttachment;
import static org.cmdbuild.utils.date.DateUtils.now;
import static org.cmdbuild.utils.date.DateUtils.toJavaDate;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.newDataHandler;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import org.cmdbuild.utils.lang.CmdbMapUtils;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNotNullAndGtZero;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.mapToLoggableString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.log.LogUtils.printStreamFromLogger;

@Component
public class EmailMtaServiceImpl implements EmailMtaService {

	private static final String //
			CONTENT_TYPE_TEXT_HTML = "text/html", //
			CONTENT_TYPE_TEXT_PLAIN = "text/plain", //
			IMAPS = "imaps", //
			MAIL_DEBUG = "mail.debug", //
			MAIL_IMAP_HOST = "mail.imap.host", //
			MAIL_IMAP_PORT = "mail.imap.port", //
			MAIL_IMAPS_HOST = "mail.imaps.host", //
			MAIL_IMAP_SOCKET_FACTORY_CLASS = "mail.imap.socketFactory.class", //
			MAIL_IMAPS_PORT = "mail.imaps.port", //
			MAIL_IMAP_STARTTLS_ENABLE = "mail.imap.starttls.enable", //
			MAIL_SMPT_SOCKET_FACTORY_CLASS = "mail.smpt.socketFactory.class", //
			MAIL_SMPT_SOCKET_FACTORY_FALLBACK = "mail.smtp.socketFactory.fallback", //
			MAIL_SMTP_AUTH = "mail.smtp.auth", //
			MAIL_SMTP_HOST = "mail.smtp.host", //
			MAIL_SMTP_PORT = "mail.smtp.port", //
			MAIL_SMTPS_AUTH = "mail.smtps.auth", //
			MAIL_SMTPS_HOST = "mail.smtps.host", //
			MAIL_SMTPS_PORT = "mail.smtps.port", //
			MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable", //
			MAIL_STORE_PROTOCOL = "mail.store.protocol", //
			MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol", // 
			JAVAX_NET_SSL_SSL_SOCKET_FACTORY = "javax.net.ssl.SSLSocketFactory";

	private static final String MESSAGE_ID = "Message-ID";

	private static final String TO = "TO";
	private static final String CC = "CC";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Tika tika = new Tika();
//	private static final Iterable<String> NO_ADDRESSES = emptyList();
//	private static final String CONTENT_TYPE = "text/html; charset=UTF-8"; 
	private final EmailAccountService emailAccountService;
//	private final EmailCO

	public EmailMtaServiceImpl(EmailAccountService emailAccountService) {
		this.emailAccountService = checkNotNull(emailAccountService);

		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
		mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
		mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
		mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
		mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
		mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
		CommandMap.setDefaultCommandMap(mc);

		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
	}

	@Override
	public void send(Email email) {
		try {
			EmailAccount account;
			if (email.getAccount() == null) {
				account = checkNotNull(emailAccountService.getDefaultOrNull(), "no account supplied for email, and no default account found");
			} else {
				account = emailAccountService.getAccount(email.getAccount());
			}
			new EmailSender(account).sendEmail(email);
		} catch (MessagingException ex) {
			throw runtime(ex);
		}
	}

	@Override
	public void receive(EmailAccount account, String incomingFolder, @Nullable String receivedFolder, @Nullable String rejectedFolder, Consumer<Email> callback) {
		try {
			new EmailReader(account).receiveMails(incomingFolder, receivedFolder, rejectedFolder, callback);
		} catch (MessagingException ex) {
			throw runtime(ex);
		}
	}

	@Override
	public List<Email> receive(EmailAccount account, String incomingFolder, @Nullable String receivedFolder) {
		List<Email> list = list();
		receive(account, incomingFolder, receivedFolder, null, list::add);
		return list;
	}

	private boolean isEmailTraceEnabled() {
		return logger.isTraceEnabled();
	}

	private static Address toAddress(String emailAddress) {
		try {
			return new InternetAddress(emailAddress);
		} catch (AddressException ex) {
			throw runtime(ex);
		}
	}

	private static class MyAuthenticator extends javax.mail.Authenticator {

		private final PasswordAuthentication authentication;

		public MyAuthenticator(String username, String password) {
			authentication = new PasswordAuthentication(username, password);
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return authentication;
		}

	}

	private class EmailReader {

		private final EmailAccount account;
		private Session session;
		private Store store;

		public EmailReader(EmailAccount account) {
			this.account = checkNotNull(account);
		}

		public void receiveMails(String incomingFolder, @Nullable String receivedFolder, @Nullable String rejectedFolder, Consumer<Email> callback) throws MessagingException {
			createSession();
			try {
				Folder folder = store.getFolder(checkNotBlank(incomingFolder));
				folder.open(Folder.READ_ONLY);
				List<Message> messages = list(folder.getMessages());
				logger.debug("processing {} incoming messages for account = {} folder = {}", messages.size(), account, folder);
				messages.forEach((m) -> {
					try {
						Email email = parseEmail(m);
						logger.debug("processing message = {}", email);
						callback.accept(email);
						if (isNotBlank(receivedFolder)) {
							moveToFolder(m, receivedFolder);
						}
					} catch (Exception ex) {
						logger.warn("error processing message = {}", m);
						if (isNotBlank(rejectedFolder)) {
							moveToFolder(m, rejectedFolder);
						}
					}
				});
			} finally {
				closeSession();
			}
		}

		private Email parseEmail(Message message) {
			throw new UnsupportedOperationException("TODO");

			/*
			
			
	private class ContentExtractor {

		private static final String TEXT_PLAIN = "text/plain";

		private static final String ATTACHMENT_PREFIX = "attachment";
		private static final String ATTACHMENT_EXTENSION = ".out";

		private final Logger logger = SelectMailImpl.this.logger;

		private String content = EMPTY;
		private String plainAlternative = EMPTY;
		private final Collection<Attachment> attachments = newArrayList();

		public ContentExtractor(final Message message) throws MailException {
			try {
				handle(message);
				content = defaultString(defaultString(content, plainAlternative), "Mail content not recognized");
			} catch (final MessagingException e) {
				throw MailException.content(e);
			} catch (final IOException e) {
				throw MailException.io(e);
			}
		}

		private void handle(final Message message) throws MessagingException, IOException {
			final Object content = message.getContent();
			if (content instanceof Multipart) {
				handleMultipart(Multipart.class.cast(content));
			} else {
				handlePart(message);
			}
		}

		private void handleMultipart(final Multipart multipart) throws MessagingException, IOException {
			for (int i = 0, n = multipart.getCount(); i < n; i++) {
				handlePart(multipart.getBodyPart(i));
			}
		}

		private void handlePart(final Part part) throws MessagingException, IOException {
			final String contentType = part.getContentType();
			logger.debug("content-type for current part is '{}'", contentType);
			final String disposition = part.getDisposition();
			if (disposition == null) {
				final String tempContent = parseContent(part.getContent());
				if (!tempContent.isEmpty()) {
					content = tempContent;
				}
			} else if (ATTACHMENT.equalsIgnoreCase(disposition)) {
				logger.debug("attachment with name '{}'", part.getFileName());
				handleAttachment(part);
			} else if (INLINE.equalsIgnoreCase(disposition)) {
				logger.debug("in-line with name '{}'", part.getFileName());
				if (part.getFileName() != null) {
					handleAttachment(part);
				} else {
					content = parseContent(part.getContent());
				}
			} else {
				logger.warn("should never happen, disposition is '{}'", disposition);
			}
			if (contentType.toLowerCase().contains(TEXT_PLAIN)) {
				if (plainAlternative.isEmpty()) {
					plainAlternative = String.class.cast(part.getContent());
				}
			}
		}

		private String parseContent(final Object messageContent) throws IOException, MessagingException {
			if (messageContent == null) {
				throw new IllegalArgumentException();
			}
			String parsedMessage = EMPTY;
			if (messageContent instanceof Multipart) {
				handleMultipart(Multipart.class.cast(messageContent));
			} else if (messageContent instanceof String) {
				parsedMessage = messageContent.toString();
			}
			return parsedMessage;
		}

		private void handleAttachment(final Part part) throws MessagingException, IOException {
			final File directory = FileUtils.getTempDirectory();
			final File file;
			final String filename;
			if (part.getFileName() == null) {
				file = File.createTempFile(ATTACHMENT_PREFIX, ATTACHMENT_EXTENSION, directory);
				file.deleteOnExit();
				filename = file.getName();
			} else {
				filename = MimeUtility.decodeText(part.getFileName());
				file = File.createTempFile(filename, null, directory);
			}
			file.deleteOnExit();
			logger.trace("saving file '{}'", file.getPath());

			final InputStream is = part.getInputStream();
			FileUtils.copyInputStreamToFile(is, file);

			attachments.add(new DefaultAttachment(filename, file));
		}

		public String getContent() {
			return content;
		}

		public Iterable<Attachment> getAttachments() {
			return attachments;
		}

	}

	private static final String ADDRESS_PATTERN_REGEX = ".*<(.*)>.*";
	private static final Pattern ADDRESS_PATTERN = Pattern.compile(ADDRESS_PATTERN_REGEX);

	private GetMail transform(final Message message) throws MessagingException {
		final ContentExtractor contentExtractor = new ContentExtractor(message);
		return GetMailImpl.newInstance() //
				.withId(messageIdOf(message)) //
				.withFolder(message.getFolder().getFullName()) //
				.withSubject(message.getSubject()) //
				.withFrom(stripAddress(firstOf(message.getFrom()))) //
				.withTos(splitRecipients(headersOf(message, TO))) //
				.withCcs(splitRecipients(headersOf(message, CC))) //
				.withContent(contentExtractor.getContent()) //
				.withAttachments(contentExtractor.getAttachments()) //
				.build();
	}

	private String firstOf(final Address[] froms) {
		return isEmpty(froms) ? null : froms[0].toString();
	}

	private Iterable<String> splitRecipients(final String[] headers) {
		if ((headers != NO_HEADER_FOUND) && (headers.length > 0)) {
			final String recipients = headers[0];
			return from(asList(recipients.split(RECIPIENTS_SEPARATOR))) //
					.transform(new Function<String, String>() {

						@Override
						public String apply(final String input) {
							return trim(stripAddress(input));
						}

					});
		}
		return emptyList();
	}

	private String stripAddress(final String input) {
		final Matcher matcher = ADDRESS_PATTERN.matcher(input);
		return matcher.matches() ? matcher.group(1) : input;
	}
			
			 */
		}

		private void moveToFolder(Message message, String targetFolderName) {
			checkNotBlank(targetFolderName);
			try {
				logger.debug("moving message = {} from folder = {} to folder = {}", message, message.getFolder(), targetFolderName);
				Folder source = checkNotNull(message.getFolder());
				Folder target = store.getFolder(targetFolderName);
				if (!target.exists()) {
					target.create(Folder.HOLDS_MESSAGES);
				}
				target.open(Folder.READ_WRITE);
				source.copyMessages(new Message[]{message}, target);
				source.setFlags(new Message[]{message}, new Flags(Flags.Flag.DELETED), true);
				source.expunge();
			} catch (Exception ex) {
				logger.warn("error moving message = {} from folder = {} to folder = {}", message, message.getFolder(), targetFolderName, ex);
			}
		}

		private void createSession() throws MessagingException {
			Properties properties = new Properties(System.getProperties());
			properties.setProperty(MAIL_STORE_PROTOCOL, account.getImapSsl() ? "imaps" : "imap");
			properties.setProperty(MAIL_IMAP_STARTTLS_ENABLE, (account.getImapStartTls() ? TRUE : FALSE).toString());
			if (account.getImapSsl()) {
				properties.setProperty(MAIL_IMAPS_HOST, account.getImapServer());
				if (isNotNullAndGtZero(account.getImapPort())) {
					properties.setProperty(MAIL_IMAPS_PORT, account.getImapPort().toString());
				}
				properties.setProperty(MAIL_IMAP_SOCKET_FACTORY_CLASS, JAVAX_NET_SSL_SSL_SOCKET_FACTORY);
			} else {
				properties.setProperty(MAIL_IMAP_HOST, account.getImapServer());
				if (isNotNullAndGtZero(account.getImapPort())) {
					properties.setProperty(MAIL_IMAP_PORT, account.getImapPort().toString());
				}
			}
			Authenticator authenticator;
			if (account.isAuthenticationEnabled()) {
				authenticator = new MyAuthenticator(account.getUsername(), account.getPassword());
			} else {
				authenticator = null;
			}
			logger.trace("imap server configuration:\n{}", mapToLoggableString(properties));
			session = Session.getInstance(properties, authenticator);
			if (isEmailTraceEnabled()) {
				session.setDebugOut(printStreamFromLogger(logger::trace));
				session.setDebug(true);
			}
			store = session.getStore();
			store.connect();
		}

		private void closeSession() {
			try {
				if (store != null && store.isConnected()) {
					store.close();
				}
			} catch (Exception ex) {
				logger.warn("error closing receiver mta session", ex);
			}
			store = null;
			session = null;
		}

	}

	private class EmailSender {

		private final EmailAccount account;
		private Session session;
		private Transport transport;

		public EmailSender(EmailAccount emailAccount) {
			this.account = checkNotNull(emailAccount);
		}

		public void sendEmail(Email email) throws MessagingException {
			createSession();
			try {
				Message message = buildMessage(email);
				transport.sendMessage(message, message.getAllRecipients());
				//TODO store message in imap folter, if configured
//					if (isNotBlank(output.getOutputFolder())) {
//						new InputTemplate(input).execute(new InputTemplate.Hook() {
//
//							@Override
//							public void connected(final Store store) throws MessagingException {
//								logger.debug("storing e-mail '{}'", newMail);
//								final Folder folder = store.getFolder(output.getOutputFolder());
//								if (!folder.exists()) {
//									folder.create(Folder.HOLDS_MESSAGES);
//								}
//								folder.open(Folder.READ_WRITE);
//								folder.appendMessages(new Message[] { message });
//								message.setFlag(Flags.Flag.RECENT, true);
//							}
//
//						});
			} finally {
				closeSession();
			}
		}

		private void createSession() throws MessagingException {
			Properties properties = new Properties(System.getProperties());
			properties.setProperty(MAIL_TRANSPORT_PROTOCOL, account.getSmtpSsl() ? "smtps" : "smtp");
			properties.setProperty(MAIL_SMTP_STARTTLS_ENABLE, (account.getSmtpStartTls() ? TRUE : FALSE).toString());
			if (account.getSmtpSsl()) {
				properties.setProperty(MAIL_SMTPS_HOST, account.getSmtpServer());
				if (isNotNullAndGtZero(account.getSmtpPort())) {
					properties.setProperty(MAIL_SMTPS_PORT, account.getSmtpPort().toString());
				}
				properties.setProperty(MAIL_SMTPS_AUTH, Boolean.toString(account.isAuthenticationEnabled()));
				properties.setProperty(MAIL_SMPT_SOCKET_FACTORY_CLASS, JAVAX_NET_SSL_SSL_SOCKET_FACTORY);
				properties.setProperty(MAIL_SMPT_SOCKET_FACTORY_FALLBACK, FALSE.toString());
			} else {
				properties.setProperty(MAIL_SMTP_HOST, account.getSmtpServer());
				if (isNotNullAndGtZero(account.getSmtpPort())) {
					properties.setProperty(MAIL_SMTP_PORT, account.getSmtpPort().toString());
				}
				properties.setProperty(MAIL_SMTP_AUTH, Boolean.toString(account.isAuthenticationEnabled()));
			}
			logger.trace("smtp server configuration:\n{}", mapToLoggableString(properties));
			Authenticator authenticator;
			if (account.isAuthenticationEnabled()) {
				authenticator = new MyAuthenticator(account.getUsername(), account.getPassword());
			} else {
				authenticator = null;
			}
			session = Session.getInstance(properties, authenticator);
			if (isEmailTraceEnabled()) {
				session.setDebugOut(printStreamFromLogger(logger::trace));
				session.setDebug(true);
			}
			transport = session.getTransport();
			transport.connect();
		}

		private void closeSession() {
			try {
				if (transport != null && transport.isConnected()) {
					transport.close();
				}
			} catch (Exception ex) {
				logger.warn("error closing sender mta session", ex);
			}
			transport = null;
			session = null;
		}

		private Message buildMessage(Email email) throws MessagingException {
			Message message = new MimeMessage(session);

			if (email.getFromAddressList().isEmpty()) {
				message.setFrom(toAddress(account.getAddress()));
			} else {
				message.addFrom(transform(email.getFromAddressList(), EmailMtaServiceImpl::toAddress).toArray(new Address[]{}));
			}

			transformValues(CmdbMapUtils.<RecipientType, List<String>>map(
					Message.RecipientType.TO, email.getToAddressList(),
					Message.RecipientType.CC, email.getCcAddressList(),
					Message.RecipientType.BCC, email.getBccAddressList()),
					(a) -> transform(a, EmailMtaServiceImpl::toAddress)).forEach((t, a) -> {
						try {
							message.addRecipients(t, a.toArray(new Address[]{}));
						} catch (MessagingException ex) {
							throw runtime(ex);
						}
					});

			message.setSubject(email.getSubject());
			message.setSentDate(toJavaDate(now()));

			String emailContent = nullToEmpty(email.getContent());
			String emailContentType = getEmailContentType(emailContent);

			if (!email.hasAttachments()) {
				message.setContent(emailContent, emailContentType);
			} else {
				Multipart multipart = new MimeMultipart("mixed");
				MimeBodyPart contentPart = new MimeBodyPart();
				contentPart.setContent(emailContent, emailContentType);
				multipart.addBodyPart(contentPart);
				for (EmailAttachment a : email.getAttachments()) {
					BodyPart attachmentPart = new MimeBodyPart();
					attachmentPart.setDataHandler(newDataHandler(a.getData(), a.getMimeType(), a.getFileName()));
					attachmentPart.setFileName(a.getFileName());//TODO is this required? test
					multipart.addBodyPart(attachmentPart);
				}
			}

			return message;
		}
	}

	private String getEmailContentType(String emailContent) {
		if (tika.detect(emailContent.getBytes()).contains("html")) {
			return "text/html";
		} else {
			return "text/plain";
		}
	}

}
