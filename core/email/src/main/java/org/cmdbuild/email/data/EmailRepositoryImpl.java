/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email.data;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.QueryBuilder.EQ;
import org.cmdbuild.email.Email;
import org.cmdbuild.email.beans.EmailImpl;
import static org.cmdbuild.email.EmailStatus.ES_OUTGOING;
import static org.cmdbuild.email.utils.EmailUtils.serializeEmailStatus;
import org.springframework.stereotype.Component;

@Component
public class EmailRepositoryImpl implements EmailRepository {

	private final DaoService dao;

	public EmailRepositoryImpl(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@Override
	public List<Email> getAllForCard(long reference) {
		return dao.selectAll().from(EmailImpl.class).where("Card", EQ, reference).asList();
	}

	@Override
	public Email getOne(long emailId) {
		return dao.getById(EmailImpl.class, emailId).toModel();
	}

	@Override
	public Email create(Email email) {
		return dao.create(email);
	}

	@Override
	public Email update(Email email) {
		return dao.update(email);
	}

	@Override
	public void delete(Email email) {
		dao.delete(email);
	}

	@Override
	public List<Email> getAllForOutgoingProcessing() {
		return dao.selectAll().from(Email.class)
				.where("EmailStatus", EQ, serializeEmailStatus(ES_OUTGOING))
				.whereExpr("\"Delay\" IS NULL OR \"Delay\" <= 0 OR \"BeginDate\" < ( NOW() - ( \"Delay\"::varchar || ' milliseconds' )::interval )")
				.asList();
	}

}
