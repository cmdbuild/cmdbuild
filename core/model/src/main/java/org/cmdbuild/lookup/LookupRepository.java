package org.cmdbuild.lookup;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import java.util.Collection;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.beans.CmdbFilterImpl;

public interface LookupRepository {

	Lookup getById(long lookupId);

	Collection<Lookup> getAll();

	Collection<Lookup> getByType(String type, CmdbFilter filter);

	default Collection<Lookup> getAllByType(String type) {
		return getByType(type, CmdbFilterImpl.noopFilter());
	}

	default Collection<Lookup> getAllByType(LookupType type) {
		return getAllByType(type.getName());
	}

	Collection<LookupType> getAllTypes();

	Lookup getOneByTypeAndCode(String type, String code);

	default Lookup getOneByTypeAndId(String type, long lookupValueId) {
		Lookup lookup = getById(lookupValueId);
		checkArgument(equal(lookup.getType().getName(), type), "lookup not found for type = %s and id = %s", type, lookupValueId);
		return lookup;
	}

	Lookup createOrUpdate(Lookup lookup);

	LookupType getTypeByName(String lookupTypeName);

	LookupType createLookupType(LookupType lookupType);

	LookupType updateLookupType(String lookupTypeId, LookupType lookupType);

	void deleteLookupRecord(long lookupValueId);

	void deleteLookupType(String lookupTypeId);

}
