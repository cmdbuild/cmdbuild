package org.cmdbuild.lookup;

import org.cmdbuild.common.utils.PagedElements;

import javax.annotation.Nullable;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.utils.CmdbFilterUtils;

public interface LookupService {

	PagedElements<LookupType> getAllTypes(@Nullable Integer offset, @Nullable Integer limit, @Nullable String filter);

	default PagedElements<LookupType> getAllTypes() {
		return getAllTypes(null, null, null);
	}

//	String fetchTranslationUuid(int id);
//	void saveLookupType(LookupType newType, LookupType oldType);
	default PagedElements<Lookup> getAllLookup(String type) {
		return getAllLookup(type, null, null);
	}

	default PagedElements<Lookup> getAllLookup(LookupType type) {
		return getAllLookup(type, null, null);
	}

	PagedElements<Lookup> getAllLookup(String type, @Nullable Integer offset, @Nullable Integer limit, CmdbFilter filter);

	default PagedElements<Lookup> getAllLookup(String type, @Nullable Integer offset, @Nullable Integer limit) {
		return getAllLookup(type, offset, limit, CmdbFilterUtils.noopFilter());
	}

	default PagedElements<Lookup> getAllLookup(LookupType type, @Nullable Integer offset, @Nullable Integer limit) {
		return getAllLookup(type.getName(), offset, limit);
	}

	Lookup getLookupByTypeAndCode(String type, String code);

	Iterable<Lookup> getAllLookupOfParent(LookupType type);

	Lookup getLookup(Long id);

//	void enableLookup(Long id);
//
//	void disableLookup(Long id);
//	LookupType typeFor(String lookupTypeName);
//	Optional<LookupType> typeFor(Predicate<LookupType> predicate);
	Lookup createOrUpdateLookup(Lookup lookup);

	LookupType getLookupType(String lookupTypeId);

	LookupType createLookupType(LookupType lookupType);

	LookupType updateLookupType(String lookupTypeId, LookupType lookupType);

	void deleteLookupValue(String lookupTypeId, long lookupId);

	void deleteLookupType(String lookupTypeId);

}
