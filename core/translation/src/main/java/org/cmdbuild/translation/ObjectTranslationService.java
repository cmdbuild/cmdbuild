/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.translation;

import static com.google.common.base.Strings.nullToEmpty;
import static java.lang.String.format;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.AttributeGroup;
import org.cmdbuild.dao.entrytype.Classe;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface ObjectTranslationService {

	default String translateClassDescription(Classe classe) {
		return translateByCode(format("class.%s.description", classe.getName()), classe.getDescription());
	}

	default String translateAttributeDescription(Attribute attribute) {
		String value = translateByCode(format("attributeclass.%s.%s.description", attribute.getOwner().getName(), attribute.getName()));
		String defaultValue = attribute.getDescription();
		if (attribute.getOwner() instanceof Classe) {
			Classe classe = ((Classe) attribute.getOwner());
			String name = attribute.getName();
			while ((attribute == null || attribute.isInherited()) && value == null && classe.hasParent()) {
				classe = classe.getParent();
				attribute = classe.getAttributeOrNull(name);
				if (attribute != null) {
					value = translateByCode(format("attributeclass.%s.%s.description", attribute.getOwner().getName(), attribute.getName()));
				}
			}
		}
		return firstNonNull(value, nullToEmpty(defaultValue));
	}

	default String translateAttributeGroupDescription(AttributeGroup attributeGroup) {
		return translateByCode(format("attributegroup.%s.description", attributeGroup.getName()), attributeGroup.getDescription());
	}

	default String translateDomainDirectDescription(String domainId, String defaultValue) {
		return translateByCode(format("domain.%s.directdescription", checkNotBlank(domainId)), defaultValue);
	}

	default String translateDomainInverseDescription(String domainId, String defaultValue) {
		return translateByCode(format("domain.%s.inversedescription", checkNotBlank(domainId)), defaultValue);
	}

	default String translateDomainMasterDetailDescription(String domainId, String defaultValue) {
		return translateByCode(format("domain.%s.masterdetaillabel", checkNotBlank(domainId)), defaultValue);
	}

	default String translateLookupDescription(String lookupType, String lookupCode, String defaultValue) {
		return translateByCode(format("lookup.%s.%s.description", checkNotBlank(lookupType, "lookup type cannot be null"), checkNotBlank(lookupCode, "lookup code cannot be null")), defaultValue);
	}

	default String translateMenuitemDescription(String code, String defaultValue) {
		return translateByCode(format("menuitem.%s.description", checkNotBlank(code)), defaultValue);
	}

	default String translateViewDesciption(String classId, String defaultValue) {
		return translateByCode(format("view.%s.description", checkNotBlank(classId)), defaultValue);
	}

	default String translateReportDesciption(String reportId, String defaultValue) {
		return translateByCode(format("report.%s.description", checkNotBlank(reportId)), defaultValue);
	}

	default String translateByCode(String code, String defaultValue) {
		return firstNonNull(translateByCode(code), nullToEmpty(defaultValue));
	}

	@Nullable
	String translateByCode(String code);
}
