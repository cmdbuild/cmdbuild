/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.translation;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import static java.lang.String.format;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.common.localization.LanguageService;
import org.cmdbuild.common.utils.PagedElements;
import org.springframework.stereotype.Component;
import org.cmdbuild.translation.dao.TranslationRepository;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.translation.dao.Translation;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;

@Component
public class TranslationServiceImpl implements TranslationService, ObjectTranslationService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final TranslationRepository translationRepository;
	private final LanguageService languageService;

	public TranslationServiceImpl(TranslationRepository translationRepository, LanguageService languageService) {
		this.translationRepository = checkNotNull(translationRepository);
		this.languageService = checkNotNull(languageService);
	}

	@Override
	public String translateExpr(String source) {
		Matcher matcher = Pattern.compile("[{]translate:[^}]*[}]", Pattern.DOTALL).matcher(source);
		if (matcher.find()) {
			StringBuffer stringBuffer = new StringBuffer();
			matcher.reset();
			while (matcher.find()) {
				String from = matcher.group();
				Matcher blockMatcher = Pattern.compile("[{]translate:([^:}]*)(:[^}]*)?[}]").matcher(from);
				checkArgument(blockMatcher.find());
				String code = checkNotBlank(blockMatcher.group(1));
				String defaultValue = emptyToNull(blockMatcher.group(2));
				String value = translateByCode(code);
				if (value == null) {
					if (defaultValue != null) {
						value = defaultValue.replaceFirst(":", "");
					} else {
						logger.warn(marker(), "translation not found for code = {} and user language = {}", code, languageService.getRequestLanguage());
						value = format("missing_translation('%s')", code);
					}
				}
				matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(value));
			}
			matcher.appendTail(stringBuffer);
			return stringBuffer.toString();
		} else {
			return source;
		}
	}

	@Nullable
	@Override
	public String translateByCode(String code) {
		checkNotBlank(code);
		String lang = languageService.getRequestLanguage();
		if (isBlank(lang)) {
			return null;
		} else {
			return translationRepository.getTranslationOrNull(code, lang);
		}
	}

	@Override
	public String getTranslationForCodeAndCurrentUser(String code) {
		return checkNotNull(translateByCode(code), format("unable to find translation for code = %s and user language = %s", code, languageService.getRequestLanguage()));
	}

	@Override
	public String getTranslationValueForCodeAndLang(String code, String lang) {
		return translationRepository.getTranslation(code, lang);
	}

	@Override
	public Map<String, String> getTranslationValueMapByLangForCode(String code) {
		return translationRepository.getTranslations(code).stream().collect(toMap(Translation::getLang, Translation::getValue));
	}

	@Override
	public PagedElements<Translation> getTranslations(@Nullable String filter, @Nullable Integer offset, @Nullable Integer limit) {
		return translationRepository.getTranslations(filter, offset, limit);
	}

	@Override
	public Translation setTranslation(String code, String lang, String value) {
		return translationRepository.setTranslation(code, lang, value);
	}

	@Override
	public void deleteTranslationIfExists(String code, String lang) {
		translationRepository.deleteTranslationIfExists(code, lang);
	}

	@Override
	public void deleteTranslations(String code) {
		translationRepository.deleteTranslations(code);
	}

}
