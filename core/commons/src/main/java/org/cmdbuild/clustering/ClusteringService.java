/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.clustering;

import com.google.common.eventbus.EventBus;
import java.util.List;

/**
 *
 * @author davide
 */
public interface ClusteringService {

	void sendMessage(ClusterMessage clusterMessage);

	/**
	 * an eventbus to listen to {@link ClusterMessageReceivedEvent} events 
	 */
	EventBus getEventBus();

	boolean isRunning();

	List<ClusterNode> getClusterNodes();

}
