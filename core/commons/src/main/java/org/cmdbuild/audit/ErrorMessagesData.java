/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.audit;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;

public class ErrorMessagesData {

	private final List<ErrorMessageDataImpl> data;
	private final long version = 1;

	public ErrorMessagesData(List<ErrorMessageDataImpl> data) {
		this(1l, data);
	}

	@JsonCreator
	public ErrorMessagesData(@JsonProperty("version") Long version, @JsonProperty("data") List<ErrorMessageDataImpl> data) {
		checkArgument(version == 1);
		this.data = list(checkNotNull(data)).immutable();
	}

	@JsonProperty("data")
	public List<ErrorMessageDataImpl> getData() {
		return data;
	}

	@JsonProperty("version")
	public long getVersion() {
		return version;
	}

}
