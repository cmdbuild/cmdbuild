/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.audit;

import java.time.ZonedDateTime;
import javax.annotation.Nullable;

public interface RequestInfo {

	static final String BINARY_PAYLOAD_PREFIX = "BINARY_DATA",
			NO_ACTION_ID = "NO_ACTION_ID",
			NO_SESSION_ID = "NO_SESSION_ID",
			NO_USER_AGENT = "NO_USER_AGENT",
			PAYLOAD_TRACKING_DISABLED = "PAYLOAD_TRACKING_DISABLED",
			RESPONSE_TRACKING_DISABLED = "RESPONSE_TRACKING_DISABLED",
			NO_SESSION_USER = "NO_SESSION_USER";

	String getSessionId();

	ZonedDateTime getTimestamp();

	String getUser();

	/**
	 * return request id, as provided by client (this should be unique, but
	 * since we cannot trust client, we have to handle the case where this is
	 * duplicate or missing)
	 *
	 * @return request id
	 */
	String getRequestId();

	/**
	 * return tracking id: this is a request id generated on the server (thus
	 * guaranteeed unique)
	 *
	 * @return tracking id
	 */
	String getTrackingId();

	String getActionId();

	String getPath();

	String getMethod();

	@Nullable
	String getQuery();

	@Nullable
	String getSoapActionOrMethod();

	@Nullable
	Integer getElapsedTimeMillis();

	@Nullable
	Integer getStatusCode();

	boolean isSoap();

	default boolean isCompleted() {
		return getElapsedTimeMillis() != null;
	}

	default boolean hasError() {
		Integer statusCode = getStatusCode();
		if (statusCode != null && !statusCode.toString().matches("[23]..")) {
			return true;
		}
		return false;
	}
}
