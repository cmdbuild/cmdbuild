/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.audit;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import java.time.ZonedDateTime;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.common.error.ErrorOrWarningEvent;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.xml.CmdbuildXmlUtils.isXml;

/**
 *
 */
@CardMapping("_Request")
public class RequestDataImpl implements RequestData {

	private final Long id;
	private final String client, userAgent, path, method, payload, response;
	private final Integer statusCode, payloadSize, responseSize;
	private final ZonedDateTime timestamp;
	private final ErrorMessagesData events;
	private final String sessionId, requestId, trackingId, actionId, user, query, soapActionOrMethod;
	private final Integer elapsedTimeMillis;
	private final boolean isSoap;

	private RequestDataImpl(SimpleRequestDataBuilder builder) {
		this.sessionId = builder.sessionId;
		this.id = builder.id;
		this.requestId = builder.requestId;
		this.trackingId = builder.trackingId;
		this.actionId = builder.actionId;
		this.user = builder.user;
		this.query = builder.query;
		this.soapActionOrMethod = builder.soapActionOrMethod;
		this.isSoap = builder.isSoap;
		this.elapsedTimeMillis = (builder.elapsedTimeMillis);
		this.client = checkNotNull(builder.client);
		this.userAgent = checkNotNull(builder.userAgent);
		this.path = checkNotNull(builder.path);
		this.method = checkNotNull(builder.method);
		this.payload = (builder.payload);
		this.payloadSize = checkNotNull(builder.payloadSize);
		this.response = (builder.response);
		this.statusCode = (builder.statusCode);
		this.responseSize = (builder.responseSize);
		this.timestamp = checkNotNull(builder.timestamp);
		this.events = firstNonNull(builder.getErrorMessageData(), new ErrorMessagesData(emptyList()));
	}

	@Override
	@CardAttr
	@Nullable
	public Long getId() {
		return id;
	}

	@Override
	@CardAttr(value = "ElapsedTime", defaultValue = "0")
	public Integer getElapsedTimeMillis() {
		return elapsedTimeMillis;
	}

	@Override
	@CardAttr
	public String getSessionId() {
		return sessionId;
	}

	@Override
	@CardAttr("SessionUser")
	public String getUser() {
		return user;
	}

	@Override
	@CardAttr
	public String getRequestId() {
		return requestId;
	}

	@Override
	@CardAttr
	public String getTrackingId() {
		return trackingId;
	}

	@Override
	@CardAttr
	public String getActionId() {
		return actionId;
	}

	@Override
	@CardAttr
	public String getPath() {
		return path;
	}

	@Override
	@CardAttr
	public String getMethod() {
		return method;
	}

	@Override
	@CardAttr
	public String getQuery() {
		return query;
	}

	@Override
	public String getSoapActionOrMethod() {
		return soapActionOrMethod;
	}

	@Override
	public boolean isSoap() {
		return isSoap;
	}

	@Override
	@CardAttr
	public String getClient() {
		return client;
	}

	@Override
	@CardAttr
	public String getUserAgent() {
		return userAgent;
	}

	@Override
	@CardAttr(defaultValue = "PAYLOAD_NOT_YET_AVAILABLE")
	public String getPayload() {
		return payload;
	}

	@Override
	@CardAttr(defaultValue = "RESPONSE_NOT_YET_AVAILABLE")
	public String getResponse() {
		return response;
	}

	@Override
	@CardAttr
	public Integer getPayloadSize() {
		return payloadSize;
	}

	@Override
	@CardAttr
	public Integer getResponseSize() {
		return responseSize;
	}

	@Override
	@CardAttr("StatusCode")
	public Integer getStatusCode() {
		return statusCode;
	}

	@Override
	@CardAttr
	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	@Override
	@CardAttr(readFromDb = false)//TODO add impl to handle this param
	public boolean isCompleted() {
		return getPayload() != null && getResponse() != null && getStatusCode() != null && getElapsedTimeMillis() != null && getResponseSize() != null;
	}

	@CardAttr("Errors")
	@Override
	public ErrorMessagesData getErrorMessageData() {
		return events;
	}

	@Override
	public String toString() {
		String str = "RequestDataImpl{" + "requestId=" + getRequestId() + ", path=" + method + " " + path;
		if (getElapsedTimeMillis() != null) {
			str += format(", elapsed=%.3fs", getElapsedTimeMillis() / 1000d);
		}
		return str + "}";
	}

	public static SimpleRequestDataBuilder builder() {
		return new SimpleRequestDataBuilder();
	}

	public static SimpleRequestDataBuilder copyOf(RequestData data) {
		return new SimpleRequestDataBuilder().
				withActionId(data.getActionId()).
				withClient(data.getClient()).
				withUserAgent(data.getUserAgent()).
				withElapsedTimeMillis(data.getElapsedTimeMillis()).
				withPath(data.getPath()).
				withMethod(data.getMethod()).
				withPayload(data.getPayload()).
				withQuery(data.getQuery()).
				withRequestId(data.getRequestId()).
				withTrackingId(data.getTrackingId()).
				withResponse(data.getResponse()).
				withSessionId(data.getSessionId()).
				withUser(data.getUser()).
				withStatusCode(data.getStatusCode()).
				withPayloadSize(data.getPayloadSize()).
				withResponseSize(data.getResponseSize()).
				withTimestamp(data.getTimestamp()).
				withErrorMessageData(data.getErrorMessageData())
				.withId(data.getId())
				.withSoap(data.isSoap(), data.getSoapActionOrMethod());
	}

	public static class SimpleRequestDataBuilder implements Builder<RequestDataImpl, SimpleRequestDataBuilder>, RequestData {

		private Long id;
		private String client, userAgent, payload, response;
		private Integer statusCode, payloadSize, responseSize;
		private ZonedDateTime timestamp;
		private ErrorMessagesData events;
		private String sessionId, requestId, trackingId, actionId, user, path, method, query, soapActionOrMethod;
		private Integer elapsedTimeMillis;
		private boolean isSoap = false;

		private SimpleRequestDataBuilder() {
		}

		public SimpleRequestDataBuilder withId(@Nullable Long id) {
			this.id = id;
			return this;
		}

		public SimpleRequestDataBuilder withSessionId(String sessionId) {
			this.sessionId = checkNotNull(sessionId);
			return this;
		}

		public SimpleRequestDataBuilder withUser(String user) {
			this.user = user;
			return this;
		}

		public SimpleRequestDataBuilder withRequestId(String requestId) {
			this.requestId = checkNotNull(requestId);
			return this;
		}

		public SimpleRequestDataBuilder withTrackingId(String trackingId) {
			this.trackingId = checkNotNull(trackingId);
			return this;
		}

		public SimpleRequestDataBuilder withActionId(String actionId) {
			this.actionId = checkNotNull(actionId);
			return this;
		}

		public SimpleRequestDataBuilder withPath(String path) {
			this.path = checkNotNull(path);
			return this;
		}

		public SimpleRequestDataBuilder withMethod(String method) {
			this.method = checkNotNull(method);
			return this;
		}

		public SimpleRequestDataBuilder withQuery(String query) {
			this.query = query;
			return this;
		}

		public SimpleRequestDataBuilder withSoap(@Nullable String soapActionOrMethod) {
			this.isSoap = true;
			this.soapActionOrMethod = soapActionOrMethod;
			return this;
		}

		public SimpleRequestDataBuilder withSoapActionOrMethod(String soapActionOrMethod) {
			this.isSoap = true;
			this.soapActionOrMethod = soapActionOrMethod;
			return this;
		}

		public SimpleRequestDataBuilder withSoap(boolean isSoap, @Nullable String soapActionOrMethod) {
			this.isSoap = isSoap;
			this.soapActionOrMethod = soapActionOrMethod;
			return this;
		}

		public SimpleRequestDataBuilder withElapsedTimeMillis(Integer elapsedTimeMillis) {
			this.elapsedTimeMillis = (elapsedTimeMillis);
			return this;
		}

		@CardAttr("Errors")
		public SimpleRequestDataBuilder deserializeErrors(String serializedErrors) {
			//TODO
			return this;
		}

		@Override
		public String getClient() {
			return client;
		}

		@Override
		public String getUserAgent() {
			return userAgent;
		}

		@Override
		public String getPayload() {
			return payload;
		}

		@Override
		public String getResponse() {
			return response;
		}

		@Override
		public Integer getStatusCode() {
			return statusCode;
		}

		@Override
		public Integer getPayloadSize() {
			return payloadSize;
		}

		@Override
		public Integer getResponseSize() {
			return responseSize;
		}

		@Override
		public ZonedDateTime getTimestamp() {
			return timestamp;
		}

		public SimpleRequestDataBuilder withClient(String client) {
			this.client = checkNotNull(client);
			return this;
		}

		public SimpleRequestDataBuilder withUserAgent(String userAgent) {
			this.userAgent = checkNotNull(userAgent);
			return this;
		}

		public SimpleRequestDataBuilder withPayload(String payload) {
			this.payload = (payload);
			return this;
		}

		public SimpleRequestDataBuilder withResponse(String response) {
			this.response = (response);
			return this;
		}

		public SimpleRequestDataBuilder withStatusCode(Integer statusCode) {
			this.statusCode = (statusCode);
			return this;
		}

		public SimpleRequestDataBuilder withPayloadSize(Integer payloadSize) {
			this.payloadSize = checkNotNull(payloadSize);
			return this;
		}

		public SimpleRequestDataBuilder withResponseSize(Integer responseSize) {
			this.responseSize = (responseSize);
			return this;
		}

		public SimpleRequestDataBuilder withTimestamp(ZonedDateTime timestamp) {
			this.timestamp = checkNotNull(timestamp);
			return this;
		}

		public SimpleRequestDataBuilder withErrorsAndWarningEvents(@Nullable List<ErrorOrWarningEvent> events) {
			if (events != null) {
				this.events = new ErrorMessagesData(events.stream().map((e) -> new ErrorMessageDataImpl(e.getLevel(), e.getMessage(), e.getStackTraceAsString())).collect(toList()));
			}
			return this;
		}

		public SimpleRequestDataBuilder withErrorMessageData(@Nullable ErrorMessagesData events) {
			this.events = events;
			return this;
		}

		public SimpleRequestDataBuilder withErrorMessageDataList(List<ErrorMessageData> events) {
			this.events = new ErrorMessagesData((List) events);
			return this;
		}

		@Override
		public String getSessionId() {
			return sessionId;
		}

		@Override
		public String getRequestId() {
			return requestId;
		}

		@Override
		public String getTrackingId() {
			return trackingId;
		}

		@Override
		public String getActionId() {
			return actionId;
		}

		@Override
		public String getUser() {
			return user;
		}

		@Override
		public String getPath() {
			return path;
		}

		@Override
		public String getMethod() {
			return method;
		}

		@Override
		public String getQuery() {
			return query;
		}

		@Override
		public String getSoapActionOrMethod() {
			return soapActionOrMethod;
		}

		@Override
		public Integer getElapsedTimeMillis() {
			return elapsedTimeMillis;
		}

		@Override
		public boolean isSoap() {
			return isSoap;
		}

		@Override
		public Long getId() {
			return id;
		}

		@Override
		public ErrorMessagesData getErrorMessageData() {
			return events;
		}

		@Override
		public RequestDataImpl build() {
			isSoap = isSoapRequest(path, payload);
			if (isSoap) {
				soapActionOrMethod = getSoapActionOrMethod(payload);
			}
			return new RequestDataImpl(this);
		}

		//TODO only for orm tool, refactor and remove
		public void setCompleted(String param) {
			//do nothig
		}

		private boolean isSoapRequest(String path, @Nullable String payload) {
			return path.contains("soap") && isXml(payload);
		}

		private @Nullable
		String getSoapActionOrMethod(String payload) {
			Matcher matcher = Pattern.compile("[<][^:]+[:]Body[>][<][^:]+[:]([a-zA-Z0-9]+)").matcher(payload);
			if (matcher.find()) {
				return trimToNull(matcher.group(1));
			} else {
				return null;
			}
		}
	}
}
