/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.audit;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.nullToEmpty;
import java.util.List;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.audit.RequestDataUtils.binaryPayloadToByteArray;
import static org.cmdbuild.audit.RequestInfo.NO_SESSION_ID;

/**
 * request data (for request tracking); classes implementing this interface are
 * supposed to be immutable
 */
public interface RequestData extends RequestInfo {

	@Nullable
	Long getId();

	String getClient();

	String getUserAgent();

	ErrorMessagesData getErrorMessageData();

	default List<ErrorMessageData> getErrorOrWarningEvents() {
		return (List) getErrorMessageData().getData();
	}

	@Nullable
	String getPayload();

	/**
	 * return size of request payload
	 *
	 * @return
	 */
	Integer getPayloadSize();

	@Nullable
	String getResponse();

	/**
	 * return size of response payload
	 *
	 * @return
	 */
	@Nullable
	Integer getResponseSize();

	default boolean hasSession() {
		return !equal(getSessionId(), NO_SESSION_ID) && !isBlank(getSessionId());
	}

	default boolean hasPayload() {
		return !isBlank(getPayload());
	}

	default boolean isBinary() {
		return nullToEmpty(getPayload()).startsWith(BINARY_PAYLOAD_PREFIX);
	}

	default byte[] getBinaryPayload() {
		checkArgument(isBinary());
		return binaryPayloadToByteArray(getPayload());
	}
}
