/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.audit;

import java.io.BufferedReader;
import java.io.StringReader;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import org.apache.commons.codec.binary.Base64;
import static org.cmdbuild.audit.RequestInfo.BINARY_PAYLOAD_PREFIX;

public class RequestDataUtils {

	public static byte[] binaryPayloadToByteArray(String payload) {
		return Base64.decodeBase64(new BufferedReader(new StringReader(payload))
				.lines()
				.skip(2)
				.collect(joining()));
	}

	public static String buildBinaryPayload(String info, byte[] data) {
		return format("%s: %s\n\n%s", BINARY_PAYLOAD_PREFIX, info, Base64.encodeBase64String(data));
	}

}
