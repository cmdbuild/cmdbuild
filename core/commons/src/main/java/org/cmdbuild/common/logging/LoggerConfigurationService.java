/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.common.logging;

import java.io.File;
import java.util.List;
import org.w3c.dom.Document;

/**
 * this service allows configuration of loggers (by category and level). Log
 * configuration is reloaded on write, so changes are immediately applied in the
 * system.
 *
 * @author davide
 */
public interface LoggerConfigurationService {

	Document getDocument();

	void setDocument(Document document);

	List<File> getLogFiles();

	List<LoggerConfig> getAllLoggerConfig();

	void updateLoggerConfig(LoggerConfig loggerConfig);

	void addLoggerConfig(LoggerConfig loggerConfig);

	void removeLoggerConfig(String category);
}
