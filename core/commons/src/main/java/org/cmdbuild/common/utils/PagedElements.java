package org.cmdbuild.common.utils;

import com.google.common.collect.Iterables;
import com.google.common.collect.Streams;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.cmdbuild.utils.lang.CmdbCollectionUtils;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.lteqZeroToNull;

public class PagedElements<T> implements Iterable<T> {

	private final List<T> elements;
	private final int totalSize;//TODO change this to long

	public PagedElements(Iterable<T> elements, int totalSize) {
		this.elements = CmdbCollectionUtils.toList(elements);
		this.totalSize = totalSize;
	}

	public PagedElements(Iterable<T> elements) {
		this(elements, Iterables.size(elements));
	}

	public PagedElements<T> withTotal(int newTotal) {
		return new PagedElements<>(elements, newTotal);
	}

	public <E> PagedElements<E> map(Function<T, E> mapper) {
		return new PagedElements<>(elements.stream().map(mapper::apply).collect(toList()), totalSize);
	}

	@Override
	public Iterator<T> iterator() {
		return elements.iterator();
	}

	public List<T> elements() {
		return elements;
	}

	public int totalSize() {
		return totalSize;
	}

	public boolean isEmpty() {
		return Iterables.isEmpty(elements);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof PagedElements)) {
			return false;
		}
		final PagedElements<?> other = PagedElements.class.cast(obj);
		return new EqualsBuilder() //
				.append(this.elements, other.elements) //
				.append(this.totalSize, other.totalSize) //
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder() //
				.append(elements) //
				.append(totalSize) //
				.toHashCode();
	}

	@Override
	public final String toString() {
		return reflectionToString(this, SHORT_PREFIX_STYLE);
	}

	public Stream<T> stream() {
		return elements.stream();
	}

	public int size() {
		return elements.size();
	}

	public static <T> PagedElements<T> empty() {
		final Iterable<T> emptyList = emptyList();
		return new PagedElements<>(emptyList, 0);
	}

	public static <T> PagedElements<T> paged(Iterable<T> elements, int total) {
		return new PagedElements<>(elements, total);
	}

	public static <T> PagedElements<T> paged(Iterable<T> elements) {
		return new PagedElements<>(elements);
	}

	public static <T> PagedElements<T> paged(Iterable<T> elements, @Nullable Integer offset, @Nullable Integer limit) {
		return paged(elements, identity(), offset, limit);
	}

	public static <T> PagedElements<T> paged(Iterable<T> elements, @Nullable Long offset, @Nullable Long limit) {
		return paged(elements, identity(), offset, limit);
	}

	public static <T, E> PagedElements<E> paged(Iterable<T> elements, Function<T, E> fun, @Nullable Integer offset, @Nullable Integer limit) {
		return paged(elements, fun, convert(offset, Long.class), convert(limit, Long.class));
	}

	public static <T, E> PagedElements<E> paged(Iterable<T> elements, Function<T, E> fun, @Nullable Long offset, @Nullable Long limit) {
		int total = Iterables.size(elements);
		return new PagedElements<>(Streams.stream(elements).skip(firstNonNull(offset, 0l)).limit(firstNonNull(lteqZeroToNull(limit), Long.MAX_VALUE)).map(fun).collect(toList()), total);
	}

	public static boolean hasOffset(@Nullable Number offset) {
		return offset != null && offset.longValue() > 0;
	}

	public static boolean hasLimit(@Nullable Number limit) {
		return limit != null && limit.longValue() > 0;
	}

	public static boolean isPaged(@Nullable Number offset, @Nullable Number limit) {
		return hasOffset(offset) || hasLimit(limit);
	}

}
