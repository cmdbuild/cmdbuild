/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.common.error;

import java.util.Comparator;
import javax.annotation.Nullable;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author davide
 */
public interface ErrorOrWarningEvent {

	String getMessage();

	@Nullable
	Exception getException();

	@Nullable
	default String getStackTraceAsString() {
		return hasException() ? ExceptionUtils.getStackTrace(getException()) : null;
	}

	Level getLevel();

	default boolean hasException() {
		return getException() != null;
	}

	enum Level {
		INFO(2), WARNING(1), ERROR(0);

		private Level(int level) {
			this.level = level;
		}

		private final int level;

	}

	enum LeveOrderErrorsFirst implements Comparator<Level> {

		INSTANCE;

		@Override
		public int compare(Level o1, Level o2) {
			return Integer.compare(o1.level, o2.level);
		}

	}
}
