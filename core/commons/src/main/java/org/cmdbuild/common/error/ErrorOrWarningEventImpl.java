/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.common.error;

import com.google.common.base.Joiner;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import javax.annotation.Nullable;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;

/**
 *
 */
public class ErrorOrWarningEventImpl implements ErrorOrWarningEvent {

	private final String message;
	private final Level level;
	private final Exception exception;

	public ErrorOrWarningEventImpl(@Nullable String message, Level level, @Nullable Exception exception) {
		checkArgument(message != null || exception != null, "must provide message or exception");
		this.message = message != null ? message : exceptionToMessage(exception);
		this.level = checkNotNull(level);
		this.exception = exception;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public Level getLevel() {
		return level;
	}

	@Override
	@Nullable
	public Exception getException() {
		return exception;
	}

	private String exceptionToMessage(Throwable ex) {
		List<String> messages = list();
		while (ex != null) {
			messages.add(ex.toString());
			ex = ex.getCause();
		}
		return Joiner.on(", caused by: ").join(messages);
	}

}
