/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.common.logging;

/**
 * this service is a spring wrapper around {@link LogbackConfigurator}. It
 * offers higher level api (and may implement more complex logic inside).
 *
 * @author davide
 */
public interface LogbackConfigurationStore {

	String getLogbackXmlConfiguration();

	void setLogbackXmlConfiguration(String xmlConfiguration);

}
