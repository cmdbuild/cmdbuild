package org.cmdbuild.email;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.isNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.MoreCollectors.toOptional;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;

public interface EmailAccountService {

	EmailAccount create(EmailAccount account);

	EmailAccount update(EmailAccount account);

	List<EmailAccount> getAll();

	@Nullable
	EmailAccount getAccountOrNull(String name);

	EmailAccount getAccount(long accountId);

	default EmailAccount getAccount(String name) {
		return checkNotNull(getAccountOrNull(name), "account not found for name = %s", name);
	}

	void delete(long accountId);

	default @Nullable
	EmailAccount getDefaultOrNull() {
		return getAll().stream().filter(EmailAccount::isDefault).collect(toOptional()).orElse(null);
	}

	default @Nullable
	EmailAccount getAccountOrDefaultOrNull(String... accounts) {
		return getAccountOrDefaultOrNull(list(accounts));
	}

	default @Nullable
	EmailAccount getAccountOrDefaultOrNull(Collection<String> accounts) {
		return accounts.stream().filter(StringUtils::isNotBlank).map(this::getAccountOrNull).filter(not(isNull())).findFirst().orElseGet(this::getDefaultOrNull);
	}

}
