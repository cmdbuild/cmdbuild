package org.cmdbuild.email;

import com.google.common.eventbus.EventBus;
import java.util.List;
import java.util.Map;

public interface EmailService {

	Email create(Email email);

	List<Email> getAllForCard(long reference);

	Email getOne(long emailId);

	Email update(Email email);

	void delete(Email email);

	Email syncTemplate(Email email, String jsContext);

	EventBus getEventBus();

	List<Email> getAllForOutgoingProcessing();

	List<EmailAttachment> getEmailAttachments(long emailId);

	Email loadAttachments(Email email);

	enum NewOutgoingEmailEvent {
		INSTANCE
	}
}
