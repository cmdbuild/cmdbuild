/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email;

import java.util.List;

public interface EmailTemplateService {

	List<EmailTemplate> getAll();

	EmailTemplate getOne(long id);

	EmailTemplate getByName(String name);

}
