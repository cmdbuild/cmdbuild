package org.cmdbuild.email;

import javax.annotation.Nullable;

public interface EmailTemplate {

	@Nullable
	Long getId();

	String getName();

	@Nullable
	String getDescription();

	@Nullable
	String getFrom();

	@Nullable
	String getTo();

	@Nullable
	String getCc();

	@Nullable
	String getBcc();

	@Nullable
	String getSubject();

	@Nullable
	String getBody();

	@Nullable
	Long getAccount();

	@Nullable
	Boolean getKeepSynchronization();

	@Nullable
	Boolean getPromptSynchronization();

	@Nullable
	Long getDelay();

}
