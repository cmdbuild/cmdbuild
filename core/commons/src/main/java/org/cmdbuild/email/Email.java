/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.email;

import com.google.common.base.Splitter;
import static com.google.common.base.Strings.nullToEmpty;
import java.time.ZonedDateTime;
import java.util.List;
import javax.annotation.Nullable;

public interface Email {

	@Nullable
	Long getId();

	@Nullable
	String getFromAddress();

	@Nullable
	String getToAddresses();

	@Nullable
	String getCcAddresses();

	@Nullable
	String getBccAddresses();

	default List<String> getFromAddressList() {
		return Splitter.on(",").omitEmptyStrings().trimResults().splitToList(nullToEmpty(getFromAddress()));
	}

	default List<String> getToAddressList() {
		return Splitter.on(",").omitEmptyStrings().trimResults().splitToList(nullToEmpty(getToAddresses()));
	}

	default List<String> getCcAddressList() {
		return Splitter.on(",").omitEmptyStrings().trimResults().splitToList(nullToEmpty(getCcAddresses()));
	}

	default List<String> getBccAddressList() {
		return Splitter.on(",").omitEmptyStrings().trimResults().splitToList(nullToEmpty(getBccAddresses()));
	}

	@Nullable
	String getSubject();

	@Nullable
	String getContent();

	@Nullable
	ZonedDateTime getDate();

	EmailStatus getStatus();

	@Nullable
	Long getReference();

	@Nullable
	Long getAutoReplyTemplate();

	boolean getNoSubjectPrefix();

	@Nullable
	Long getAccount();

	@Nullable
	Long getTemplate();

	boolean getKeepSynchronization();

	boolean getPromptSynchronization();

	@Nullable
	Long getDelay();

	int getErrorCount();

	List<EmailAttachment> getAttachments();

	default boolean hasAttachments() {
		return !getAttachments().isEmpty();
	}

	default boolean hasTemplate() {
		return getTemplate() != null;
	}

}
