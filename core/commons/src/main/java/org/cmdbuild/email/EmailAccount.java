package org.cmdbuild.email;

import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public interface EmailAccount {

	@Nullable
	Long getId();

	String getName();

	boolean isDefault();

	@Nullable
	String getUsername();

	@Nullable
	String getPassword();

	String getAddress();

	String getSmtpServer();

	Integer getSmtpPort();

	boolean getSmtpSsl();

	boolean getSmtpStartTls();

	default boolean isSmtpConfigured() {
		return isNotBlank(getSmtpServer());
	}

	String getOutputFolder();

	String getImapServer();

	Integer getImapPort();

	boolean getImapSsl();

	boolean getImapStartTls();

	default boolean isImapConfigured() {
		return isNotBlank(getImapServer());
	}

	default boolean isAuthenticationEnabled() {
		return isNotBlank(getUsername());
	}

}
