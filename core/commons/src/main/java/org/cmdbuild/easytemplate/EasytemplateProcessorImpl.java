package org.cmdbuild.easytemplate;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.regex.Matcher.quoteReplacement;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EasytemplateProcessorImpl implements EasytemplateProcessor {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Map<String, Function<String, Object>> templateResolvers;
	private final Pattern pattern;

	private EasytemplateProcessorImpl(EasytemplateProcessorImplBuilder builder) {
		this.templateResolvers = builder.templateResolvers;
		String patternStr = format("[{](%s):([^{}]+)[}]", Joiner.on("|").join(templateResolvers.keySet()));
		logger.trace("using pattern = {}", patternStr);
		pattern = Pattern.compile(patternStr);
	}

	@Override
	public String processExpression(@Nullable String expression) {
		return (expression == null) ? null : doResolve(expression);
	}

	@Override
	public Map<String, Function<String, Object>> getResolvers() {
		return templateResolvers;
	}

	private String doResolve(String expression) {
		logger.debug("trying to resolve expr = {}", expression);
		String resolved = expression;
		Matcher matcher = pattern.matcher(resolved);
		while (matcher.find()) {
			logger.debug("found match = {}", matcher.group(0));
			String resolver = matcher.group(1);
			String expr = matcher.group(2);
			Object value = resolveExpression(resolver, expr);
			logger.debug("replacing match with = {}", value);
			resolved = matcher.replaceFirst(quoteReplacement(String.valueOf(value)));
			matcher = pattern.matcher(resolved);
		}
		return resolved;
	}

	private Object resolveExpression(String resolver, String expression) {
		logger.debug("evaluating expr = {} with resolver = {}", expression, resolver);
		Function<String, Object> engine = templateResolvers.get(resolver);
		if (engine != null) {
			Object value = engine.apply(expression);
			if (value == null) {
				logger.warn("expr = {} resolved to null", expression);
			}
			return value;
		} else {
			logger.warn("expr resolver = {} not found", resolver);
			return null;
		}
	}

	public static EasytemplateProcessorImplBuilder builder() {
		return new EasytemplateProcessorImplBuilder();
	}

	public static EasytemplateProcessorImplBuilder copyOf(EasytemplateProcessor processor) {
		return builder().withResolvers(processor.getResolvers());
	}

	public static class EasytemplateProcessorImplBuilder implements Builder<EasytemplateProcessorImpl, EasytemplateProcessorImplBuilder> {

		protected final Map<String, Function<String, Object>> templateResolvers = map();

		private EasytemplateProcessorImplBuilder() {
		}

		public EasytemplateProcessorImplBuilder withResolver(Function<String, Object> engine, String... prefixes) {
			return EasytemplateProcessorImplBuilder.this.withResolver(engine, asList(prefixes));
		}

		public EasytemplateProcessorImplBuilder withResolver(Function<String, Object> engine, Iterable<String> prefixes) {
			for (String p : prefixes) {
				templateResolvers.put(p, engine);
			}
			return this;
		}

		public EasytemplateProcessorImplBuilder withResolver(String prefix, Function<String, Object> engine) {
			templateResolvers.put(prefix, engine);
			return this;
		}

		public EasytemplateProcessorImplBuilder withResolvers(Map<String, Function<String, Object>> resolvers) {
			templateResolvers.putAll(resolvers);
			return this;
		}

		@Override
		public EasytemplateProcessorImpl build() {
			return new EasytemplateProcessorImpl(this);
		}

	}

}
