package org.cmdbuild.easytemplate;

import com.google.common.base.Function;
import java.util.Map;

public interface EasytemplateProcessor {

	String processExpression(String expression);

	Map<String, Function<String, Object>> getResolvers();
}
