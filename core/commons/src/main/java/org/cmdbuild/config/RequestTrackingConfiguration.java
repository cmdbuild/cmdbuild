/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config;

import javax.annotation.Nullable;

/**
 *
 * @author davide
 */
public interface RequestTrackingConfiguration {

	/**
	 * return true to enable request tracking, false to disable it
	 *
	 * @return
	 */
	boolean isRequestTrackingEnabled();

	/**
	 * return true to save request payload for each request, false otherwise
	 *
	 * @return
	 */
	boolean includeRequestPayload();

	/**
	 * return true to save response payload for each request, false otherwise
	 *
	 * @return
	 */
	boolean includeResponsePayload();

	/**
	 * return max payload size to store (longer payloads will be truncated to
	 * this size). This is used for both request and response payload.
	 *
	 * @return
	 */
	int getMaxPayloadLength();

	/**
	 * configure how many records to keep in request tracking table; return less
	 * than 0 or null to disable size-based record cleanup; return 0 to disable
	 * record collection
	 *
	 * @return
	 */
	@Nullable
	Integer getMaxRecordsToKeep();

	/**
	 * configure how long to keep records; return less than 0 or null to disable
	 * age-based record cleanup; return 0 to disable record collection
	 *
	 * @return
	 */
	@Nullable
	Integer getMaxRecordAgeToKeepSeconds();

	/**
	 * return a regex to be used to set request paths to track (all paths that
	 * matches this regex will be tracked, and others will be not); null will
	 * track everything.
	 *
	 * includes will be precessed before excludes (as in: track_this_request =
	 * request_is_included and not(request_is_excluded) )
	 *
	 * default to "^/services"
	 *
	 * @return
	 */
	@Nullable
	String getRegexForPathsToInclude();

	/**
	 * return regex for request paths to exclude from tracking (all paths that
	 * matches this request will not be tracked).
	 *
	 * excludes will be precessed after includes (as in: track_this_request =
	 * request_is_included and not(request_is_excluded) )
	 *
	 * default to null	*
	 *
	 * @return
	 */
	@Nullable
	String getRegexForPathsToExclude();

}
