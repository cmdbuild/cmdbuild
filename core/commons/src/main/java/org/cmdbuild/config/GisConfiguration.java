package org.cmdbuild.config;

import com.google.common.eventbus.EventBus;

public interface GisConfiguration {

	EventBus getEventBus();

	boolean isEnabled();

	boolean isGeoServerEnabled();

	boolean isYahooEnabled();

	boolean isGoogleEnabled();

	boolean isOsmEnabled();

	String getGoogleKey();

	String getYahooKey();

	String getGeoServerUrl();

	String getGeoServerWorkspace();

	String getGeoServerAdminUser();

	String getGeoServerAdminPassword();

}
