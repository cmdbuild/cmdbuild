/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

import com.google.common.eventbus.EventBus;

/**
 *
 */
public interface ConfigAccesService {

	/**
	 * get config object to access config values within a namespace (this is the
	 * usual entry point to config values from within cmdbuild).
	 *
	 * Config object are linked to config service eventbus, so they consume
	 * resources that cannot be released. Config objects are meant as singleton,
	 * and reused through the whole application lifecycle.
	 *
	 * @param namespace
	 * @return
	 */
	NamespacedConfigService getConfig(String namespace);

	/**
	 * get event bus; mostly used to handle {@link ConfigUpdateEvent} events.
	 *
	 * @return
	 */
	EventBus getEventBus();
}
