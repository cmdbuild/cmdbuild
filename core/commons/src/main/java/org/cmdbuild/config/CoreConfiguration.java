package org.cmdbuild.config;

import java.util.Set;
import org.cmdbuild.common.localization.LanguageConfiguration;

public interface CoreConfiguration extends LanguageConfiguration {

	boolean isWorkingForYouModeEnabled();

	String getWorkingForYouModePasstoken();

	boolean allowConfigUpdateViaWs();

	/**
	 * amount of time (in seconds) required between session.accessTime and
	 * session.saveTime in order to trigger a session save on db. In other
	 * words, a session access will be persisted only if access-save >
	 * {@link #getSessionPersistDelay()}.
	 *
	 * Note than any change to session (update of session data etc) will instead
	 * be persisted immediately.
	 *
	 * @return delay in seconds
	 */
	default int getSessionPersistDelay() {
		return 600; // default 10 minutes
	}

	default boolean disableReplayAttackCheck() {
		return false;
	}

	boolean useLanguagePrompt();

	String getStartingClassName();

	String getDemoModeAdmin();

	String getInstanceName();

	String getTabsPosition();

	int getSessionTimeoutOrDefault();

	boolean getLockCard();

	boolean getLockCardUserVisible();

	int getLockCardTimeOut();

	/**
	 * persist only locks with last persist date older than this (unless lock
	 * data has changed). This is to avoid multiple write on db for repeated
	 * lock acquisitions within the same method or method sequence; only the
	 * first write will be committed, others will be discarded for
	 * {@link #getLockCardPersistDelay()} seconds.
	 *
	 * @return
	 */
	default int getLockCardPersistDelay() {
		return 10; //seconds
	}

	Set<String> getEnabledLanguages();

	Set<String> getLoginLanguages();

	String getRedirectOnLogout();

	boolean isImportCsvOneByOne();

	int getSessionActivePeriodForStatistics();

}
