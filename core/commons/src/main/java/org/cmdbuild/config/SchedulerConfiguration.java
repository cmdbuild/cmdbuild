package org.cmdbuild.config;

import com.google.common.eventbus.EventBus;
import java.util.Map;

public interface SchedulerConfiguration {

	Map<String, String> getQuartzProperties();

	EventBus getEventBus();
}
