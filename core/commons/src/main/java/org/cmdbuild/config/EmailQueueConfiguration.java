package org.cmdbuild.config;

public interface EmailQueueConfiguration {

	boolean isQueueProcessingEnabled();

//	void setEnabled(boolean enabled);
	long getQueueTime();

//	void setQueueTime(long value);
}
