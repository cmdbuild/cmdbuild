/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public interface ConfigDefinition {

	String getKey();

	@Nullable
	String getDefaultValue();

	String getDescription();

	boolean isProtected();

	default boolean hasDescription() {
		return isNotBlank(getDescription());
	}
	
	ConfigLocation getLocation();

}
