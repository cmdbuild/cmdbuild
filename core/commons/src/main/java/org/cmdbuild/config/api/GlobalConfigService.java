/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

import com.google.common.eventbus.EventBus;
import java.util.Map;
import javax.annotation.Nullable;

/**
 *
 */
public interface GlobalConfigService extends ConfigAccesService {

	/**
	 * return explicit config values as map
	 *
	 * @return
	 */
	Map<String, String> getConfigAsMap();

	/**
	 * return all config values as map (include defaults)
	 *
	 * @return
	 */
	Map<String, String> getConfigOrDefaultsAsMap();

	Map<String, ConfigDefinition> getConfigDefinitions();

	/**
	 * set config value
	 *
	 * @param key
	 * @param value
	 */
	void putString(String key, @Nullable String value);

	/**
	 * set many config values at once
	 *
	 * @param map
	 */
	void putStrings(Map<String, String> map);

	/**
	 * delete config value by key
	 *
	 * @param key
	 */
	void delete(String key);

	/**
	 * set config value
	 *
	 * @param key
	 * @param value
	 * @param encrypt if true, encrypt value
	 */
	void putString(String key, String value, boolean encrypt);

	/**
	 *
	 * @param key
	 * @return
	 */
	@Nullable
	String getString(String key);

	/**
	 *
	 * @param key
	 * @return
	 */
	@Nullable
	String getStringOrDefault(String key);

	/**
	 * reload all config data from file/db
	 */
	void reload();

	default @Nullable
	ConfigDefinition getConfigDefinitionOrNull(String key) {
		return getConfigDefinitions().get(key);
	}

	default boolean isProtected(String key) {
		ConfigDefinition configDefinition = getConfigDefinitionOrNull(key);
		if (configDefinition == null) {
			return false;
		} else {
			return configDefinition.isProtected();
		}
	}
}
