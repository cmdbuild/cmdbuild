/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config;

import com.google.common.eventbus.EventBus;

/**
 *
 * @author davide
 */
public interface UiFilterConfiguration {

	final static String AUTO_URL = "auto";

	/**
	 * this should be equal to <url-pattern> in <filter-mapping> (example:
	 * '/ui/cmdbuild/app.js')
	 *
	 * @return
	 */
	default String getRequestUrlPattern() {
		return "^(.*)/ui[^/]*/cmdbuild/[^/.]*[.]js$";//TODO get filter path from web.xml (?)
	}

	String getBaseUrl();

	EventBus getEventBus();
}
