package org.cmdbuild.config;

import com.google.common.eventbus.EventBus;

public interface BimserverConfiguration {

	String getUrl();

	String getUsername();

	String getPassword();

	boolean isEnabled();

	EventBus getEventBus();
}
