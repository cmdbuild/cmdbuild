/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

import static com.google.common.base.MoreObjects.firstNonNull;
import java.io.File;
import static java.util.Arrays.asList;
import javax.annotation.Nullable;

/**
 *
 * @author davide
 */
public interface DirectoryService {

	DeploymentMode getDeploymentMode();

	File getConfigDirectory();

	File getCatalinaBaseDirectory();

	File getWebappRootDirectory();

	default File getWebappLibDirectory() {
		return new File(getWebappRootDirectory(), "WEB-INF/lib");
	}

	/**
	 * get file from lib dir, by regexp pattern (match against whole name)
	 */
	default @Nullable
	File getLibByPattern(String pattern) {
		File libDir = getWebappLibDirectory();
		return libDir == null ? null : asList(firstNonNull(libDir.listFiles(), new File[]{})).stream().filter((file) -> file.isFile() && file.getName().matches(pattern)).findFirst().orElse(null);
	}

	default boolean hasFilesystem() {
		switch (getDeploymentMode()) {
			case TOMCAT:
				return true;
			case DUMMY:
			default:
				return false;
		}
	}

	enum DeploymentMode {
		TOMCAT, DUMMY
	}
}
