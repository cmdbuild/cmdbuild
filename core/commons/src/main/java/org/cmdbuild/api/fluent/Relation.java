package org.cmdbuild.api.fluent;

public class Relation {

	private final String domainName;
	private CardDescriptor card1;
	private CardDescriptor card2;

	public Relation(String domainName) {
		this.domainName = domainName;
	}

	public Relation(String domainName, CardDescriptor card1, CardDescriptor card2) {
		this.domainName = domainName;
		this.card1 = card1;
		this.card2 = card2;
	}

	public String getDomainName() {
		return domainName;
	}

	public String getClassName1() {
		return card1.getClassName();
	}

	public long getCardId1() {
		return card1.getId();
	}

	public Relation setCard1(String className, long id) {
		this.card1 = new CardDescriptor(className, id);
		return this;
	}

	public String getClassName2() {
		return card2.getClassName();
	}

	public long getCardId2() {
		return card2.getId();
	}

	public Relation setCard2(String className, long id) {
		this.card2 = new CardDescriptor(className, id);
		return this;
	}

}
