package org.cmdbuild.api.fluent;

public class ExistingRelation extends ActiveRelation {

	ExistingRelation(FluentApiExecutor executor, String domainName) {
		super(executor, domainName);
	}

	public ExistingRelation withCard1(String className, long cardId) {
		super.setCard1(className, cardId);
		return this;
	}

	public ExistingRelation withCard2(String className, long cardId) {
		super.setCard2(className, cardId);
		return this;
	}

	public void delete() {
		executor().delete(this);
	}

}
