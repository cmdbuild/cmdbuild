package org.cmdbuild.api.fluent;

import org.cmdbuild.api.fluent.FluentApiExecutor.AdvanceProcess;

public class NewProcessInstance extends ActiveCard {

	NewProcessInstance(FluentApiExecutor executor, String className) {
		super(executor, className, null);
	}

	public NewProcessInstance withDescription(String value) {
		super.setDescription(value);
		return this;
	}

	public NewProcessInstance with(String name, Object value) {
		return withAttribute(name, value);
	}

	public NewProcessInstance withAttribute(String name, Object value) {
		super.set(name, value);
		return this;
	}

	public ProcessInstanceDescriptor start() {
		return executor().createProcessInstance(this, AdvanceProcess.NO);
	}

	public ProcessInstanceDescriptor startAndAdvance() {
		return executor().createProcessInstance(this, AdvanceProcess.YES);
	}
}
