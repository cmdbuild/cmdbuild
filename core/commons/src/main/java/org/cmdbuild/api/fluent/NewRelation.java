package org.cmdbuild.api.fluent;

public class NewRelation extends ActiveRelation {

	NewRelation(FluentApiExecutor executor, String domainName) {
		super(executor, domainName);
	}

	public NewRelation withCard1(String className, long cardId) {
		super.setCard1(className, cardId);
		return this;
	}

	public NewRelation withCard2(String className, long cardId) {
		super.setCard2(className, cardId);
		return this;
	}

	public void create() {
		executor().create(this);
	}

}
