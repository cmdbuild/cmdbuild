package org.cmdbuild.workflow.type;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Objects.equal;
import java.io.Serializable;
import static java.lang.Math.toIntExact;

import org.cmdbuild.common.annotations.Legacy;

/**
 * legacy code. Probably serialized around. DO NOT CHANGE CLASS CODE. DO NOT
 * CHANGE CLASS PACKAGE.
 */
@Legacy("Kept for backward compatibility")
public class ReferenceType implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	int id;
	int idClass;
	String description;

	private Long longId;

	public ReferenceType() {
		id = -1;
		idClass = -1;
		description = "";
		longId = null;
	}

	public ReferenceType(int id, int idClass, String description) {
		super();
		this.id = id;
		this.idClass = idClass;
		this.description = description;
	}

	public ReferenceType(long id, long idClass, String description) {
		super();
		this.id = Integer.MAX_VALUE;
		this.longId = id;
		this.idClass = toIntExact(idClass);
		this.description = description;
	}

	public long getId() {
		return firstNonNull(longId, (long) id);
	}

	public void setId(long id) {
		this.id = Integer.MAX_VALUE;
		this.longId = id;
	}

	public int getIdClass() {
		return idClass;
	}

	public void setIdClass(long idClass) {
		setIdClass(toIntExact(idClass));
	}

	public void setIdClass(int idClass) {
		this.idClass = idClass;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean checkValidity() {
		return getId() > 0;
	}

	@Override
	public String toString() {
		return "ReferenceType{" + "id=" + getId() + ", idClass=" + idClass + ", description=" + description + '}';
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof ReferenceType && equal(obj.toString(), this.toString());
	}
}
