/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.startup;

import org.cmdbuild.startup.SystemEvents.AppContextReadyEvent;
import org.cmdbuild.startup.SystemEvents.SystemReadyEvent;
import org.cmdbuild.startup.SystemEvents.SystemStartingServicesEvent;

public enum SystemStatus {
	/**
	 * application context is starting
	 */
	STARTING_APP_CONTEXT {
		@Override
		public SystemStatusChangedEvent buildSystemStatusChangedEvent() {
			return AppContextReadyEvent.INSTANCE;
		}

	},
	/**
	 * system is beginning to start, and initializing boot services
	 */
	STARTING_SYSTEM,
	/**
	 * system is running configuration check (db, patch, etc)
	 */
	CHECKING_DATABASE,
	/**
	 * system is waiting for the user to perform an operation, before resuming
	 * startup
	 */
	WAITING_FOR_USER,
	/**
	 * system has started app context and db; automatic startup/init methods may
	 * now run for services, schedulers, loading stuff, etch
	 */
	STARTING_SERVICES {
		@Override
		public SystemStatusChangedEvent buildSystemStatusChangedEvent() {
			return SystemStartingServicesEvent.INSTANCE;
		}

	},
	/**
	 * error in system startup; system is left in unspecified state
	 */
	ERROR,
	/**
	 * system is ready/application context is ready
	 */
	READY {
		@Override
		public SystemStatusChangedEvent buildSystemStatusChangedEvent() {
			return SystemReadyEvent.INSTANCE;
		}

	};

	public SystemStatusChangedEvent buildSystemStatusChangedEvent() {
		return new SystemStatusChangedEventImp(this);
	}
}
