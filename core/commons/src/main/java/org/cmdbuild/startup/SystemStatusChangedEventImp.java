/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.startup;

import static com.google.common.base.Preconditions.checkNotNull;

class SystemStatusChangedEventImp implements SystemStatusChangedEvent {

	private final SystemStatus systemStatus;

	public SystemStatusChangedEventImp(SystemStatus systemStatus) {
		this.systemStatus = checkNotNull(systemStatus);
	}

	@Override
	public SystemStatus getSystemStatus() {
		return systemStatus;
	}

}
