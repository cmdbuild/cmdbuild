/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.startup;

public interface SystemEvents {

	/**
	 * system has been started; services that need to run custom startup code
	 * post-initialization should listen for this event
	 */
	enum SystemStartingServicesEvent implements SystemStatusChangedEvent {
		INSTANCE;

		@Override
		public SystemStatus getSystemStatus() {
			return SystemStatus.STARTING_SERVICES;
		}
	}

	/**
	 * system is ready; all startup operations are completed
	 */
	enum SystemReadyEvent implements SystemStatusChangedEvent {
		INSTANCE;

		@Override
		public SystemStatus getSystemStatus() {
			return SystemStatus.READY;
		}
	}

	/**
	 * application context is ready (system is still starting)
	 */
	enum AppContextReadyEvent implements SystemStatusChangedEvent {
		INSTANCE;

		@Override
		public SystemStatus getSystemStatus() {
			return SystemStatus.STARTING_SYSTEM;
		}
	}
}
