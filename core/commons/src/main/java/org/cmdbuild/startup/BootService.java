/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.startup;

import static com.google.common.base.Objects.equal;

/**
 *
 * this spring service run all required checks at startup. It manage startup
 * lifecycle by running these operations in sequence:<ul>
 * <li>start boot rest ws (minimal ws set that may be used to handle
 * configuration, patch, monitor startup process etc)</li>
 * <li>block system startup while it run its boot check (check db, check config,
 * check patch, etc)</li>
 * <li>if the system is not ready (for example, the database is not configured,
 * or there are patches to run) it will hold the spring initialization process
 * while waiting for user input (via boot services)</li>
 * <li>after all check pass (with or without user intervention) the service
 * complete and spring will go on initializing all the other services</li>
 * </ul>
 *
 * @author davide
 */
public interface BootService {

	/**
	 * return application context status
	 *
	 * @return
	 */
	SystemStatus getSystemStatus();

	/**
	 *
	 * @return true if system is ready
	 */
	default boolean isSystemReady() {
		return equal(getSystemStatus(), SystemStatus.READY);
	}

	default boolean isWaitingForUser() {
		return equal(getSystemStatus(), SystemStatus.WAITING_FOR_USER);
	}

}
