/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.lock;

import java.util.List;
import javax.annotation.Nullable;

/**
 *
 * @author davide
 */
public interface LockRepository {

	void storeLock(ItemLock itemLock);

	@Nullable
	ItemLock getLockByItemId(String itemId);

	List<ItemLock> getAllLocks();

	void removeAllLocks();

	void removeLock(ItemLock currentLock);
	
	int getMaxItemIdSize();

}
