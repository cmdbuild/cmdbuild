/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.services;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import java.util.List;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface SystemServicesService {

	List<SystemService> getSystemServices();

	default SystemService getSystemService(String id) {
		checkNotBlank(id);
		return checkNotNull(getSystemServices().stream().filter(s -> equal(s.getServiceId(), id)).collect(toOptional()).orElse(null), "system service not found for id = %s", id);
	}

	default void startSystemService(String id) {
		getSystemService(id).startService();
	}

	default void stopSystemService(String id) {
		getSystemService(id).stopService();
	}

}
