/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.services;

public class SystemServiceStatusUtils {

	public static String serializeSystemServiceStatus(SystemServiceStatus status) {
		return status.name().replaceFirst("SS_", "").toLowerCase();
	}

}
