/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.services;

import java.util.EnumSet;
import static org.cmdbuild.services.SystemServiceStatus.SS_ERROR;
import static org.cmdbuild.services.SystemServiceStatus.SS_NOTRUNNING;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;

public interface SystemService {

	String getServiceName();

	default String getServiceId() {
		return getServiceName().toLowerCase().replaceAll("[^a-z]+", "_");
	}

	SystemServiceStatus getServiceStatus();

	default boolean canControlState() {
		return true;
	}

	default boolean canStart() {
		return canControlState() && EnumSet.of(SS_NOTRUNNING, SS_ERROR).contains(getServiceStatus());
	}

	default boolean canStop() {
		return canControlState() && EnumSet.of(SS_READY).contains(getServiceStatus());
	}

	default void startService() {
		throw new UnsupportedOperationException("manual start is not supported for this service");
	}

	default void stopService() {
		throw new UnsupportedOperationException("manual stop is not supported for this service");
	}

}
