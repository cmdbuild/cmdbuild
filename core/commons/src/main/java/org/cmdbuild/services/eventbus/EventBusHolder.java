/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.services.eventbus;

import com.google.common.eventbus.EventBus;

/**
 * TODO: replace static variable with spring injection
 */
public class EventBusHolder {

	private final static EventBusHolder INSTANCE = new EventBusHolder();
	private final EventBus eventBus = new EventBus();

	public static EventBus getSystemEventBus() {
		return INSTANCE.getEventBus();
	}

	public EventBus getEventBus() {
		return eventBus;
	}

}
