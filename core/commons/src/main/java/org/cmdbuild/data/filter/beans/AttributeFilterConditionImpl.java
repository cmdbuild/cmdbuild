/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.data.filter.beans;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
import java.util.Collection;
import static java.util.Collections.emptyList;
import java.util.List;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.data.filter.AttributeFilterCondition;
import org.cmdbuild.data.filter.AttributeFilterCondition.ConditionOperator;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.toList;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.lang.CmdbStringUtils;

public class AttributeFilterConditionImpl implements AttributeFilterCondition {

	private final ConditionOperator operator;
	private final String key, className;
	private final List<String> values;

	private AttributeFilterConditionImpl(AttributeFilterConditionBuilder builder) {
		this.operator = checkNotNull(builder.operator);
		this.key = checkNotBlank(builder.key);
		this.className = builder.className;
		switch (operator) {
			case ISNULL:
			case ISNOTNULL:
				this.values = emptyList();
				break;
			default:
				//TODO validate values for other operators
				this.values = ImmutableList.copyOf(transform(checkNotNull(builder.values), CmdbStringUtils::toStringOrNull));
		}
	}

	public AttributeFilterImpl toAttributeFilter() {
		return AttributeFilterImpl.simple(this);
	}

	@Override
	public boolean hasClassName() {
		return !isBlank(className);
	}

	@Override
	public String getClassName() {
		return checkNotBlank(className);
	}

	@Override
	public ConditionOperator getOperator() {
		return operator;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public List<String> getValues() {
		return values;
	}

	public static AttributeFilterConditionBuilder builder() {
		return new AttributeFilterConditionBuilder();
	}

	public static AttributeFilterConditionBuilder copyOf(AttributeFilterCondition source) {
		return new AttributeFilterConditionBuilder()
				.withOperator(source.getOperator())
				.withKey(source.getKey())
				.withClassName(source.hasClassName() ? source.getClassName() : null)
				.withValues(source.getValues());
	}

	public static AttributeFilterConditionImpl eq(String key, Object... values) {
		return builder().eq().withKey(key).withValues(values).build();
	}

	public static class AttributeFilterConditionBuilder implements Builder<AttributeFilterConditionImpl, AttributeFilterConditionBuilder> {

		private ConditionOperator operator;
		private String key, className;
		private Collection<Object> values;

		public AttributeFilterConditionBuilder withOperator(ConditionOperator operator) {
			this.operator = operator;
			return this;
		}

		public AttributeFilterConditionBuilder eq() {
			return this.withOperator(ConditionOperator.EQUAL);
		}

		public AttributeFilterConditionBuilder withKey(String key) {
			this.key = key;
			return this;
		}

		public AttributeFilterConditionBuilder withClassName(String className) {
			this.className = className;
			return this;
		}

		public AttributeFilterConditionBuilder withValues(Object... values) {
			return this.withValues(asList(values));
		}

		public AttributeFilterConditionBuilder withValues(Iterable values) {
			this.values = toList(values);
			return this;
		}

		@Override
		public AttributeFilterConditionImpl build() {
			return new AttributeFilterConditionImpl(this);
		}

	}
}
