/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.data.filter;

import java.util.List;

public interface CmdbSorter {

	List<SorterElement> getElements();

	default int count() {
		return getElements().size();
	}

	default boolean isNoop() {
		return count() == 0;
	}
}
