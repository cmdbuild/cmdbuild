/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.data.filter.beans;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import java.util.List;
import org.cmdbuild.data.filter.FunctionFilter;
import org.cmdbuild.data.filter.FunctionFilterEntry;

public class FunctionFilterImpl implements FunctionFilter {

	private final List<FunctionFilterEntry> functions;

	public FunctionFilterImpl(List<FunctionFilterEntry> functions) {
		this.functions = ImmutableList.copyOf(checkNotNull(functions));
	}

	@Override
	public List<FunctionFilterEntry> getFunctions() {
		return functions;
	}

}
