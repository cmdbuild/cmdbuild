/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.cache;

import com.google.common.cache.CacheStats;
import java.lang.instrument.Instrumentation;
import java.util.Map;
import java.util.concurrent.Callable;
import javax.annotation.Nullable;
import org.slf4j.LoggerFactory;
import static org.cmdbuild.cache.InstrumentationUtils.getInstrumentationOrNull;

public interface CmdbCache<K, V> {

	String getName();

	@Nullable
	V getIfPresent(K key);

	V get(K key, Callable<? extends V> loader);

	Map<K, V> getAllPresent(Iterable<?> keys);

	void put(K key, V value);

	void putAll(Map<? extends K, ? extends V> m);

	void invalidate(K key);

	void invalidateAll(Iterable<? extends K> keys);

	void invalidateAll();

	long size();

	CacheStats stats();

	Map<K, V> asMap();

	void cleanUp();

	default long approxMemSize() {
		Instrumentation instrumentation = getInstrumentationOrNull();
		if (instrumentation == null) {
			LoggerFactory.getLogger(getClass()).warn("unable to estimate precise cache mem size: instrumentation not found");
			return size() * 1024;//really rough estimate
		} else {
			return asMap().values().stream().map((o) -> {
				return instrumentation.getObjectSize(o); //TODO check if this is recursive; if not, replace this with something recursive
			}).mapToLong(Long.class::cast).sum();
		}
	}
}
