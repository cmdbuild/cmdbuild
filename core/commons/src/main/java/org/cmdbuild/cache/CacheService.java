/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.cache;

import com.google.common.base.Supplier;
import java.util.Map;

public interface CacheService {

	default <V> CmdbCache<String, V> newCache(String cacheName) {
		return CacheService.this.newCache(cacheName, CacheConfig.DEFAULT);
	}

	<V> CmdbCache<String, V> newCache(String cacheName, CacheConfig cacheConfig);

	default <V> Holder<V> newHolder(String cacheName) {
		return newHolder(cacheName, CacheConfig.DEFAULT);
	}

	default <V> Holder<V> newHolder(String cacheName, CacheConfig cacheConfig) {
		CmdbCache<String, V> cache = CacheService.this.newCache(cacheName, cacheConfig);
		return new Holder<V>() {
			private final static String DEFAULT = "default";

			@Override
			public V getIfPresent() {
				return cache.getIfPresent(DEFAULT);
			}

			@Override
			public boolean isPresent() {
				return getIfPresent() != null;
			}

			@Override
			public V get(Supplier<V> loader) {
				return cache.get(DEFAULT, () -> loader.get());
			}

			@Override
			public void invalidate() {
				cache.invalidateAll();
			}
		};
	}

	<V> CmdbCache<String, V> getCache(String cacheName);

	Map<String, CmCacheStats> getStats();

	void invalidateAll();

	default void invalidate(String cacheId) {
		getCache(cacheId).invalidateAll();
	}

	enum CacheConfig {

		/**
		 * default cache config (1h timeout)
		 */
		DEFAULT,
		/**
		 * never expire entries, store locally (use with system objects, such as
		 * db model)
		 */
		SYSTEM_OBJECTS
	}
}
