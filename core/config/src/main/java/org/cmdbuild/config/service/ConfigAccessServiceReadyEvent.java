/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import org.cmdbuild.config.api.ConfigAccesService;
import org.cmdbuild.config.api.ReadonlyConfigStore;

public interface ConfigAccessServiceReadyEvent {

	ConfigAccesService getConfigAccessService();

	ReadonlyConfigStore getConfigStore();

//	ConfigLocation getConfigLocation();

}
