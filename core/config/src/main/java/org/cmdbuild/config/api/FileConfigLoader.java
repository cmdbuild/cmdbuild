/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

/**
 * file config loader (used to load config from files)
 *
 * @author davide
 */
public interface FileConfigLoader {

	void loadConfigFiles();

}
