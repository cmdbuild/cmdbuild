/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Maps;
import java.util.Map;
import javax.annotation.Nullable;

public interface ConfigDefinitionStore {

	void put(ConfigDefinition configDefinition);

	@Nullable
	ConfigDefinition getOrNull(String key);

	default ConfigDefinition get(String key) {
		return checkNotNull(getOrNull(key), "unable to find config definition for key = %s", key);
	}

	Map<String, ConfigDefinition> getAll();

	default Map<String, String> getAllDefaults() {
		return Maps.transformValues(getAll(), ConfigDefinition::getDefaultValue);
	}

	@Nullable
	default String getDefaultOrNull(String key) {
		ConfigDefinition definition = getOrNull(key);
		return definition == null ? null : definition.getDefaultValue();
	}

}
