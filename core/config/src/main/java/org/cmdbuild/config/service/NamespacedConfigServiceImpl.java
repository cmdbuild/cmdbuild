/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Maps;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import java.util.Map;
import java.util.Optional;
import static java.util.function.Function.identity;
import static java.util.stream.Stream.concat;
import javax.annotation.Nullable;
import org.cmdbuild.common.error.ErrorAndWarningCollectorService;
import org.cmdbuild.config.api.ConfigDefinition;
import org.cmdbuild.config.api.ConfigDefinitionStore;
import org.cmdbuild.config.api.ConfigEvent;
import org.cmdbuild.config.api.ConfigDefinitionImpl;
import static org.cmdbuild.config.service.ConfigUtils.addNamespaceToKey;
import static org.cmdbuild.config.service.ConfigUtils.stripNamespaceFromKey;
import org.cmdbuild.utils.lang.CmdbConvertUtils;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.config.api.ReadonlyConfigStore;

public class NamespacedConfigServiceImpl implements UpdatableNamespacedConfigService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String namespace;
	private final EventBus eventBus = new EventBus();
	private ReadonlyConfigStore store;
	private final ConfigDefinitionStore configDefinitionStore;

	/**
	 * note: do not use this constructor, use the factory method in GlobalConfigService instead
	 */
	public NamespacedConfigServiceImpl(String namespace, ReadonlyConfigStore store, ConfigDefinitionStore configDefinitionStore) {
		this.store = checkNotNull(store);
		this.configDefinitionStore = checkNotNull(configDefinitionStore);
		this.namespace = checkNotBlank(namespace);
	}

	@Override
	public void setConfigServiceAndRegisterEventBus(ReadonlyConfigStore store, EventBus eventBus) {
		this.store = checkNotNull(store);
		eventBus.register(this);
	}

	@Subscribe
	public void handleConfigEvent(ConfigEvent event) {
		if (event.impactNamespace(namespace)) {
			eventBus.post(event);
		}
	}

	@Override
	@Nullable
	public String getString(String key) {
		return store.getOrNull(addNamespaceToKey(namespace, key));
	}

	@Override
	@Nullable
	public String getStringOrDefault(String key) {
		Optional<String> value = store.get(addNamespaceToKey(namespace, key));
		if (value != null) {
			return value.orElse(null);
		} else {
			return configDefinitionStore.getDefaultOrNull(addNamespaceToKey(namespace, key));
		}
	}

	@Override
	public <T> T getOrDefault(String key, Class<T> valueType) {
		String value = getStringOrDefault(key);
		try {
			return CmdbConvertUtils.convert(value, valueType);
		} catch (Exception ex) {
			logger.error(ErrorAndWarningCollectorService.marker(), String.format("error converting config value = '%s' for key = %s.%s to type = %s; returning default value", value, namespace, key, valueType), ex);
			return CmdbConvertUtils.convert(getDefault(key), valueType);
		}
	}

	@Override
	public String getNamespace() {
		return namespace;
	}

	@Override
	public Map<String, String> getAsMap() {
		Map<String, String> map = map();
		concat(configDefinitionStore.getAllDefaults().entrySet().stream(), store.getAll().entrySet().stream())
				.filter((entry) -> entry.getKey().startsWith(namespace + "."))
				.forEach((java.util.Map.Entry<java.lang.String, java.lang.String> entry) -> map.put(stripNamespaceFromKey(namespace, entry.getKey()), entry.getValue()));
		return map;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public ConfigDefinition getDefinition(String key) {
		ConfigDefinition namespacedConfigDefinition = configDefinitionStore.get(addNamespaceToKey(namespace, key));
		ConfigDefinition localConfigDefinition = ConfigDefinitionImpl.copyOf(namespacedConfigDefinition).withKey(stripNamespaceFromKey(namespace, namespacedConfigDefinition.getKey())).build();
		return localConfigDefinition;
	}

	@Override
	public Map<String, ConfigDefinition> getAllDefinitions() {
		return Maps.filterKeys(configDefinitionStore.getAll(), (key) -> key.startsWith(namespace + ".")).values().stream()
				.map(c -> ConfigDefinitionImpl.copyOf(c).withKey(stripNamespaceFromKey(namespace, c.getKey())).build())
				.collect(toMap(ConfigDefinition::getKey, identity()));
	}

	@Override
	public ConfigDefinition addDefinition(ConfigDefinition configDefinition) {
		ConfigDefinition namespacedConfigDefinition = ConfigDefinitionImpl.copyOf(configDefinition).withKey(addNamespaceToKey(namespace, configDefinition.getKey())).build();
		configDefinitionStore.put(namespacedConfigDefinition);
		return namespacedConfigDefinition;
	}

}
