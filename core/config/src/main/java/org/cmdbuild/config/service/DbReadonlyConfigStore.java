/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.Holder;
import static org.cmdbuild.config.api.ReadonlyConfigStore.DB_CONFIG_STORE;
import org.cmdbuild.dao.config.inner.DatabaseStatusService;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import org.cmdbuild.utils.crypto.CmdbuildCryptoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.jdbc.core.JdbcTemplate;
import org.cmdbuild.config.api.ReadonlyConfigStore;

/**
 * note: read operations are safe (will log errors and return null/empty values.
 * Write operations instead will throw exceptions in case of error.
 */
@Component
@Qualifier(DB_CONFIG_STORE)
public class DbReadonlyConfigStore implements ReadonlyConfigStore {

	public static final String CONFIG_CLASS_NAME = "_SystemConfig", ATTR_VALUE = "Value";
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final JdbcTemplate jdbcTemplate;
	private final DatabaseStatusService databaseStatusService;
	private final Supplier<CmdbuildCryptoUtils> cryptoUtilsProvider = () -> CmdbuildCryptoUtils.defaultUtils();
	private final Holder<Map<String, String>> allConfig;

	public DbReadonlyConfigStore(@Qualifier(SYSTEM_LEVEL_ONE) JdbcTemplate jdbcTemplate, @Qualifier(SYSTEM_LEVEL_ONE) CacheService cacheService, DatabaseStatusService databaseStatusService) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.databaseStatusService = checkNotNull(databaseStatusService);
		allConfig = cacheService.newHolder("all_config_values", CacheService.CacheConfig.SYSTEM_OBJECTS);
	}

	@Override
	public void invalidateCache() {
		allConfig.invalidate();
	}

	@Override
	@Nullable
	public Optional<String> get(String key) {
		try {
			checkReady();
			Map<String, String> config = getAll();
			if (config.containsKey(key)) {
				return Optional.of(config.get(key));
			} else {
				return null;
			}
		} catch (DatabaseNotReadyException ex) {
			logger.warn("unable to read system config for key = {}, database is not ready", key);
			return null;
		} catch (Exception ex) {
			logger.error("unable to read system config for key = " + key, ex);
			return null;
		}
	}

	@Override
	public Map<String, String> getAll() {
		try {
			checkReady();
			return allConfig.get(this::doGetAll);
		} catch (DatabaseNotReadyException ex) {
			logger.warn("unable to read system config from database, database is not ready");
			return Collections.emptyMap();
		} catch (Exception ex) {
			logger.error("unable to read system config from database", ex);
			return Collections.emptyMap();
		}
	}

	private Map<String, String> doGetAll() {
		CmdbuildCryptoUtils cryptoUtils = cryptoUtilsProvider.get();
		Map<String, String> map = map();
		jdbcTemplate.query("SELECT \"Code\", \"Value\" FROM \"_SystemConfig\" WHERE \"Status\" = 'A'", (ResultSet rs) -> {
			map.put(checkNotBlank(rs.getString("Code")), cryptoUtils.decryptValue(trimToNull(rs.getString("Value"))));
		});
		return map;
	}

	private void checkReady() {
		if (!databaseStatusService.isReady()) {
			throw new DatabaseNotReadyException();
		}
	}

	private class DatabaseNotReadyException extends RuntimeException {

		public DatabaseNotReadyException() {
			super("database is not ready");
		}
	}
}
