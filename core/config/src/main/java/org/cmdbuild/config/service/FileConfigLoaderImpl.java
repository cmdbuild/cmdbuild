/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Objects.equal;
import org.cmdbuild.config.api.FileConfigLoader;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.config.api.ConfigDefinition;
import static org.cmdbuild.config.api.ConfigLocation.CL_FILE_ONLY;
import org.cmdbuild.config.service.FileConfigStore.ConfigFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.GlobalConfigService;
import static org.cmdbuild.config.service.ConfigUtils.addNamespaceToKey;
import org.cmdbuild.startup.PostStartup;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBooleanOrDefault;
import org.cmdbuild.config.api.DirectoryService;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import org.cmdbuild.utils.lang.CmdbMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

/**
 *
 */
@Component
public class FileConfigLoaderImpl implements FileConfigLoader {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DirectoryService directoryService;
	private final GlobalConfigService configService;
	private final LegacyConfigNameMapper mapper = new LegacyConfigNameMapper();

	public FileConfigLoaderImpl(DirectoryService directoryService, GlobalConfigService configService) {
		this.directoryService = checkNotNull(directoryService);
		this.configService = checkNotNull(configService);
	}

	@PostStartup
	public void loadConfigFilesSafe() {
		try {
			loadConfigFiles();
		} catch (Exception ex) {
			logger.error("error loading config files", ex);
		}
	}

	@Override
	public synchronized void loadConfigFiles() {
		if (!directoryService.hasFilesystem()) {
			logger.warn("filesystem is not available, skipping config file import");
		} else {
			logger.info("check and load config files");
			File configDirectory = directoryService.getConfigDirectory();
			if (configDirectory == null || !configDirectory.isDirectory() || !configDirectory.canRead()) {
				logger.warn("unable to load config files from config directory = {}", configDirectory);
			} else {
				Map<String, String> loadedConfigs = map();
				List<File> toDelete = list();
				List<Pair<ConfigFile, Map<String, String>>> toRegenerate = list();
				for (File file : configDirectory.listFiles((File file, String name) -> !name.matches("database.(conf|properties)") && name.matches(".*[.](conf|properties)$"))) {
					try {
						logger.info("processing config file {}", file.getAbsolutePath());
						String namespace = "org.cmdbuild." + FilenameUtils.getBaseName(file.getName()).toLowerCase();
						ConfigFile configFile = new ConfigFile(namespace, file);
						Map<String, String> fileonlyConfigs = map();
						configFile.readConfigFromFileUnsafe().entrySet().forEach((entry) -> {
							logger.debug("loading config key = {}.{} value = {}", namespace, entry.getKey(), entry.getValue());
							String key = entry.getKey();
							if (!hasNamespace(key)) {
								key = addNamespaceToKey(namespace, entry.getKey());
							}

							ConfigDefinition configDefinition = configService.getConfigDefinitionOrNull(key);
							if (configDefinition != null && equal(CL_FILE_ONLY, configDefinition.getLocation())) {
								fileonlyConfigs.put(entry.getKey(), entry.getValue());
							} else {
								loadedConfigs.put(key, entry.getValue());
							}
						});
						logger.debug("this file will be deleted later = {}", file);
						toDelete.add(file);
						if (!fileonlyConfigs.isEmpty()) {
							toRegenerate.add(Pair.of(configFile, fileonlyConfigs));
						}
					} catch (Exception ex) {
						logger.error("error reading config from file = " + file.getAbsolutePath(), ex);
					}
				}
				Map<String, String> translatedConfigs = mapper.translateConfigNames(loadedConfigs);
				logger.debug("loading {} config values from files = {}", translatedConfigs.size(), toDelete);
				configService.putStrings(translatedConfigs);
				toDelete.forEach((file) -> {//delete only if config put succeed
					String keepFileParamKey = "org.cmdbuild." + FilenameUtils.getBaseName(file.getName()) + ".keepconfigfile";
					boolean keepFile = toBooleanOrDefault(configService.getString(keepFileParamKey), false);
					if (keepFile) {
						logger.info("keep file {} ( param {} is true )", file, keepFileParamKey);
					} else {
						deleteQuietly(file);
						if (file.exists()) {
							logger.warn("unable to delete legacy config file = '{}'; you should delete it manually", file.getAbsolutePath());
						}
					}
				});
				toRegenerate.forEach((p) -> {
					if (!p.getLeft().getFile().exists()) {
						try {
							p.getLeft().writeConfigToFile(p.getRight());
						} catch (Exception ex) {
							logger.error("error regenerating fileOnly config file = " + p.getLeft().getFile().getAbsolutePath(), ex);
						}
					}
				});
			}
			logger.debug("loadConfigFiles END");
		}
	}

	private boolean hasNamespace(String key) {
		return key.startsWith("org.cmdbuild.");//TODO move const somewhere else
	}
}
