/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Strings.nullToEmpty;
import javax.annotation.Nullable;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_CODE;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@CardMapping("_SystemConfig")
public class ConfigDataRecordImpl implements ConfigDataRecord {

	private final Long id;
	private final String code, value;

	private ConfigDataRecordImpl(ConfigDataRecordImplBuilder builder) {
		this.id = builder.id;
		this.code = checkNotBlank(builder.code);
		this.value = nullToEmpty(builder.value);
	}

	@Nullable
	@CardAttr(ATTR_ID)
	@Override
	public Long getId() {
		return id;
	}

	@CardAttr(ATTR_CODE)
	@Override
	public String getCode() {
		return code;
	}

	@CardAttr("Value")
	@Override
	public String getValue() {
		return value;
	}

	public static ConfigDataRecordImplBuilder builder() {
		return new ConfigDataRecordImplBuilder();
	}

	public static ConfigDataRecordImplBuilder copyOf(ConfigDataRecord source) {
		return new ConfigDataRecordImplBuilder()
				.withId(source.getId())
				.withCode(source.getCode())
				.withValue(source.getValue());
	}

	public static class ConfigDataRecordImplBuilder implements Builder<ConfigDataRecordImpl, ConfigDataRecordImplBuilder> {

		private Long id;
		private String code;
		private String value;

		public ConfigDataRecordImplBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public ConfigDataRecordImplBuilder withCode(String code) {
			this.code = code;
			return this;
		}

		public ConfigDataRecordImplBuilder withValue(String value) {
			this.value = value;
			return this;
		}

		@Override
		public ConfigDataRecordImpl build() {
			return new ConfigDataRecordImpl(this);
		}

	}
}
