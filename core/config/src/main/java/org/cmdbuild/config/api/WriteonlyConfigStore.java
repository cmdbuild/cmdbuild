/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

import javax.annotation.Nullable;

/**
 *
 */
public interface WriteonlyConfigStore  {

	/**
	 * put config value on db by key (value may be null)
	 *
	 * @param key
	 * @param value
	 */
	void put(String key, @Nullable String value);

	/**
	 * delete config value on db
	 *
	 * @param key
	 */
	void delete(String key);

}
