/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.api;

import java.util.Map;
import java.util.Optional;
import javax.annotation.Nullable;

/**
 *
 */
public interface ReadonlyConfigStore {

	static String DB_CONFIG_STORE = "dbConfigStore", FILE_CONFIG_STORE = "fileConfigStore";

	default @Nullable
	String getOrNull(String key) {
		Optional<String> optional = get(key);
		return optional == null ? null : optional.orElse(null);
	}

	/**
	 * get config value from db by key.
	 *
	 * NOTE: this method may return:
	 * <ul>
	 * <li>null: record not present on db</li>
	 * <li>Optional.of(null): record present on db, with null value</li>
	 * <li>Optional.of(something): record present on db, with non-null
	 * value</li>
	 * </ul>
	 *
	 * @param key
	 * @return
	 */
	@Nullable
	Optional<String> get(String key);

	/**
	 * return all config values by key
	 *
	 * @return
	 */
	Map<String, String> getAll();

	default void invalidateCache() {
	}

}
