/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Objects.equal;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.utils.io.CmdbuildPropertyUtils.loadProperties;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LegacyConfigNameMapper {

	private final static String SKIP = "SKIP";

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Map<String, String> legacyConfigPropertyMapping;

	public LegacyConfigNameMapper() {
		legacyConfigPropertyMapping = loadProperties(getClass().getResourceAsStream("legacy_config_property_mapping.properties"));
	}

	public Map<String, String> translateConfigNames(Map<String, String> loadedConfigs) {
		Map<String, String> map = map();
		loadedConfigs.entrySet().forEach((entry) -> {
			String key = entry.getKey();
			key = translateConfigName(key);
			if (!equal(key, SKIP)) {
				map.put(key, entry.getValue());
			}
		});
		return map;
	}

	private String translateConfigName(String key) {
		for (Entry<String, String> entry : legacyConfigPropertyMapping.entrySet()) {
			Matcher matcher = Pattern.compile(entry.getKey()).matcher(key);
			String to = entry.getValue();
			if (matcher.matches()) {
				if (isBlank(to) || to.equalsIgnoreCase(SKIP)) {
					return SKIP;
				} else {
					String translated = matcher.replaceFirst(to);
					translated = translateConfigName(translated);
					return translated;
				}
			}
		}
		return key;
	}
}
