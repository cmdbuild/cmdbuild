/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.Subscribe;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import javax.annotation.Nullable;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.cmdbuild.config.api.ConfigValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigUpdateEvent;
import org.cmdbuild.config.api.ConfigDefinitionImpl;
import static org.cmdbuild.utils.lang.CmdbPreconditions.firstNotBlank;

@Component
public class ConfigAnnotationProcessorService implements BeanPostProcessor {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ConfigHelperService helperService;

	public ConfigAnnotationProcessorService(ConfigHelperService configService) {
		this.helperService = checkNotNull(configService);
		logger.debug("ready");
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (bean.getClass().isAnnotationPresent(ConfigComponent.class)) {
			new ConfigAnnotationProcessor(bean, beanName).processBean();
		}
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	private class ConfigAnnotationProcessor {

		private final Object bean;
		private final String beanName;
		private UpdatableNamespacedConfigService localConfigService;
		private String namespace;

		private final Map<String, Field> fieldsToAutowire = map();
		private final Map<String, Method> methodsToAutowire = map();

		public ConfigAnnotationProcessor(Object bean, String beanName) {
			this.bean = checkNotNull(bean);
			this.beanName = checkNotNull(beanName);
		}

		public void processBean() {
			logger.trace("processing bean {} {}", beanName, bean);
			ConfigComponent classAnnotation = bean.getClass().getAnnotation(ConfigComponent.class);
			namespace = trimAndCheckNotBlank(classAnnotation.value());
			logger.debug("autowiring config in bean = {} with namespace = {}", beanName, namespace);

			localConfigService = helperService.getConfig(namespace);

			parseMethodsAndFieldsAndLoadDefaults();

			loadConfigs();//load defaults

			localConfigService.getEventBus().register(new Object() {

				@Subscribe
				public void handleConfigUpdateEvent(ConfigUpdateEvent event) {
					loadConfigs();
				}
			});
			localConfigService.getEventBus().register(bean);

			helperService.getEventBus().register(new Object() {

				@Subscribe
				public void handleConfigServiceReadyEvent(ConfigAccessServiceReadyEvent event) {
					localConfigService.setConfigServiceAndRegisterEventBus(event.getConfigStore(), event.getConfigAccessService().getEventBus());
				}

			});
		}

		private void parseMethodsAndFieldsAndLoadDefaults() {
			ReflectionUtils.doWithFields(bean.getClass(), (field) -> {
				if (field.isAnnotationPresent(ConfigValue.class)) {
					ConfigValue fieldAnnotation = field.getAnnotation(ConfigValue.class);
					String key = addDefinitionAndReturnKey(fieldAnnotation);
					logger.debug("autowiring field = {}.{} for key = {}", beanName, field.getName(), key);
					fieldsToAutowire.put(key, field);
				} else if (field.isAnnotationPresent(ConfigService.class)) {
					logger.debug("autowiring local config service field = {}.{} for namespace = {}", beanName, field.getName(), namespace);
					FieldUtils.writeField(field, bean, localConfigService, true);
				}
			});
			ReflectionUtils.doWithMethods(bean.getClass(), (method) -> {
				if (method.isAnnotationPresent(ConfigValue.class)) {
					checkArgument(method.getParameterCount() == 1, "config value methods must have one and only one parameter; invalid method = %s", method);
					ConfigValue methodAnnotation = method.getAnnotation(ConfigValue.class);
					String key = addDefinitionAndReturnKey(methodAnnotation);
					logger.debug("autowiring method = {}.{} for key = {}", beanName, method.getName(), key);
					methodsToAutowire.put(key, method);
				}
			});
		}

		private void loadConfigs() {
			try {
				fieldsToAutowire.entrySet().forEach((entry) -> {
					Field field = entry.getValue();
					String key = entry.getKey();
					Object value = localConfigService.getOrDefault(key, field.getType());
					logger.debug("set config field = {}.{} to value = {} for key = {}", beanName, field.getName(), value, key);
					try {
						FieldUtils.writeField(field, bean, value, true);
					} catch (IllegalArgumentException | IllegalAccessException ex) {
						throw new RuntimeException(ex);
					}
				});
				methodsToAutowire.entrySet().forEach((entry) -> {
					Method method = entry.getValue();
					String key = entry.getKey();
					Object value = localConfigService.getOrDefault(key, method.getParameterTypes()[0]);
					logger.debug("invoke config method = {}.{} with value = {} for key = {}", beanName, method.getName(), value, key);
					try {
						method.invoke(bean, value);
					} catch (IllegalArgumentException | IllegalAccessException ex) {
						throw new RuntimeException(ex);
					} catch (InvocationTargetException ex) {
						logger.error("error setting config value for method = " + method + " and value = " + value, ex.getCause());
					}
				});
			} catch (Exception ex) {
				logger.error("error setting config value", ex);
			}
		}

		private String addDefinitionAndReturnKey(ConfigValue annotation) {
			String key = firstNotBlank(nullIfEqualToNullConst(annotation.key()), nullIfEqualToNullConst(annotation.value()));
			localConfigService.addDefinition(ConfigDefinitionImpl.builder()
					.withKey(key)
					.withDescription(annotation.description())
					.withDefaultValue(nullIfEqualToNullConst(annotation.defaultValue()))
					.withProtected(annotation.isProtected())
					.withLocation(annotation.location())
					.build());
			return key;
		}

	}

	private static @Nullable
	String nullIfEqualToNullConst(@Nullable String value) {
		return (value == null || ConfigValue.NULL.equals(value)) ? null : value;
	}
}
