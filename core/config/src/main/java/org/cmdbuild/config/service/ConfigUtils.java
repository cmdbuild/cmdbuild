/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkArgument;

public class ConfigUtils {

	public static String addNamespaceToKey(String namespace, String key) {
		if (key.startsWith(namespace + ".")) {
			return key;
		} else {
			return namespace + "." + key;
		}
	}

	public static String stripNamespaceFromKey(String namespace, String key) {
		checkArgument(key.startsWith(namespace + "."));
		return key.substring(namespace.length() + 1);
	}
}
