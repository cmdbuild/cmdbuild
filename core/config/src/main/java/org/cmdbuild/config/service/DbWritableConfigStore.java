/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nullable;
import static org.cmdbuild.config.api.ReadonlyConfigStore.DB_CONFIG_STORE;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_CODE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.QueryBuilder.EQ;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.config.api.WriteonlyConfigStore;
import org.cmdbuild.config.api.ReadonlyConfigStore;

/**
 * note: read operations are safe (will log errors and return null/empty values.
 * Write operations instead will throw exceptions in case of error.
 */
@Component
@Qualifier(DB_CONFIG_STORE)
public class DbWritableConfigStore implements WriteonlyConfigStore {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;
	private final ReadonlyConfigStore configStore;

	public DbWritableConfigStore(DaoService dao, @Qualifier(DB_CONFIG_STORE) ReadonlyConfigStore configStore) {
		this.dao = checkNotNull(dao);
		this.configStore = checkNotNull(configStore);
	}

	@Override
	public void put(String key, @Nullable String value) {
		logger.info("update system config key = {} value = {}", key, value);
		checkNotBlank(key);
		ConfigDataRecord record = dao.selectAll().from(ConfigDataRecord.class).where(ATTR_CODE, EQ, key).getOneOrNull();
		if (record == null) {
			dao.createOnly(ConfigDataRecordImpl.builder().withCode(key).withValue(value).build());
		} else {
			dao.updateOnly(ConfigDataRecordImpl.copyOf(record).withValue(value).build());
		}
		configStore.invalidateCache();
	}

	@Override
	public void delete(String key) {
		logger.info("delete system config key = {}", key);
		ConfigDataRecord record = dao.selectAll().from(ConfigDataRecord.class).where(ATTR_CODE, EQ, key).getOneOrNull();
		if (record != null) {
			dao.delete(record);
			configStore.invalidateCache();
		}
	}

}
