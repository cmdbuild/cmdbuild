/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import static java.util.Arrays.asList;
import java.util.Map;
import java.util.Optional;
import javax.inject.Provider;
import org.cmdbuild.config.api.ConfigAccesService;
import org.cmdbuild.config.api.ConfigDefinition;
import org.cmdbuild.config.api.ConfigReloadEventImpl;
import org.cmdbuild.config.api.ConfigDefinitionStore;
import org.cmdbuild.config.api.ConfigLocation;
import static org.cmdbuild.config.api.ConfigLocation.CL_DEFAULT;
import static org.cmdbuild.config.api.ConfigLocation.CL_FILE_ONLY;
import static org.cmdbuild.config.api.ReadonlyConfigStore.DB_CONFIG_STORE;
import org.cmdbuild.config.api.ConfigUpdateEventImpl;
import org.cmdbuild.dao.config.inner.DatabaseStatusService;
import org.cmdbuild.dao.config.inner.DatabaseStatusService.DatabaseBecomeReadyEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.NamespacedConfigService;
import org.cmdbuild.config.api.ReadonlyConfigStore;
import static org.cmdbuild.config.api.ReadonlyConfigStore.FILE_CONFIG_STORE;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

/**
 *
 */
@Component
public class ConfigAccessServiceImpl implements ConfigAccesService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus = new EventBus();

	private final ReadonlyConfigStore smartConfigStore;
	private final ConfigDefinitionStore configDefinitionStore;
//	private final DatabaseStatusService databaseStatusService;

	public ConfigAccessServiceImpl(@Qualifier(FILE_CONFIG_STORE) Provider<ReadonlyConfigStore> fileConfigStore, @Qualifier(DB_CONFIG_STORE) Provider<ReadonlyConfigStore> dbConfigStore, DatabaseStatusService databaseStatusService, ConfigDefinitionStore configDefinitionStore, ConfigHelperService configHelperService) {
		this.smartConfigStore = new SmartConfigStore(fileConfigStore, dbConfigStore);
		this.configDefinitionStore = checkNotNull(configDefinitionStore);
//		this.databaseStatusService = checkNotNull(databaseStatusService);
//		configHelperService.getEventBus().post(new ConfigAccessServiceReadyEvent() {
//			@Override
//			public ConfigAccesService getConfigAccessService() {
//				return ConfigAccessServiceImpl.this;
//			}
//
//			@Override
//			public ReadonlyConfigStore getConfigStore() {
//				return fileConfigStore.get();
//			}
//
//			@Override
//			public ConfigLocation getConfigLocation() {
//				return CL_FILE_ONLY;
//			}
//		});
//		eventBus.post(new ConfigUpdateEventImpl());
		databaseStatusService.getEventBus().register(new Object() {

			@Subscribe
			public void handleDatabaseBecomeReadyEvent(DatabaseBecomeReadyEvent event) {
				smartConfigStore.invalidateCache();
				logger.info("config service ready");
				configHelperService.getEventBus().post(new ConfigAccessServiceReadyEvent() {
					@Override
					public ConfigAccesService getConfigAccessService() {
						return ConfigAccessServiceImpl.this;
					}

					@Override
					public ReadonlyConfigStore getConfigStore() {
						return smartConfigStore;
					}

//					@Override
//					public ConfigLocation getConfigLocation() {
//						return CL_DEFAULT;
//					}
				});
				eventBus.post(new ConfigUpdateEventImpl());
				eventBus.post(new ConfigReloadEventImpl());
			}
		});
	}

	@Override
	public NamespacedConfigService getConfig(String namespace) {
		NamespacedConfigServiceImpl config = new NamespacedConfigServiceImpl(namespace, smartConfigStore, configDefinitionStore);
		eventBus.register(config);
		return config;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	private class SmartConfigStore implements ReadonlyConfigStore {

		private final Provider<ReadonlyConfigStore> fileConfigStore, dbConfigStore;

		public SmartConfigStore(Provider<ReadonlyConfigStore> fileConfigStore, Provider<ReadonlyConfigStore> dbConfigStore) {
			this.fileConfigStore = checkNotNull(fileConfigStore);
			this.dbConfigStore = checkNotNull(dbConfigStore);
		}

		@Override
		public Optional<String> get(String key) {
			ConfigLocation configLocation = Optional.ofNullable(configDefinitionStore.getOrNull(key)).map(ConfigDefinition::getLocation).orElse(CL_DEFAULT);
			switch (configLocation) {
				case CL_DEFAULT:
//					return databaseStatusService.isReady() ? dbConfigStore.get().get(key) : null;
					return dbConfigStore.get().get(key);
				case CL_FILE_ONLY:
					return fileConfigStore.get().get(key);
				default:
					throw new IllegalArgumentException("unsupported config location = " + configLocation);
			}
		}

		@Override
		public Map<String, String> getAll() {
//			return databaseStatusService.isReady() ? map(fileConfigStore.get().getAll()).with(dbConfigStore.get().getAll()) : fileConfigStore.get().getAll();
			return map(fileConfigStore.get().getAll()).with(dbConfigStore.get().getAll());
		}

		@Override
		public void invalidateCache() {
			fileConfigStore.get().invalidateCache();
//			if (databaseStatusService.isReady()) {
			dbConfigStore.get().invalidateCache();
//			}
		}

	}

}
