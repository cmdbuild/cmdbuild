/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.EventBus;
import static java.util.Collections.emptyMap;
import java.util.Map;
import java.util.Optional;
import org.cmdbuild.config.api.ConfigDefinitionStore;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.ReadonlyConfigStore;
import static org.cmdbuild.config.api.ReadonlyConfigStore.FILE_CONFIG_STORE;
import org.springframework.beans.factory.annotation.Qualifier;

@Component
public class ConfigHelperServiceImpl implements ConfigHelperService {

	private final ConfigDefinitionStore configDefinitionStore;
	private final ReadonlyConfigStore basicConfigStore;
	private final EventBus eventBus = new EventBus();

	public ConfigHelperServiceImpl(@Qualifier(FILE_CONFIG_STORE) ReadonlyConfigStore fileConfigStore, ConfigDefinitionStore configDefinitionStore) {
		this.configDefinitionStore = checkNotNull(configDefinitionStore);
		this.basicConfigStore = checkNotNull(fileConfigStore);
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public UpdatableNamespacedConfigService getConfig(String namespace) {
		return new NamespacedConfigServiceImpl(namespace, basicConfigStore, configDefinitionStore);
	}

	private class DummyConfigStore implements ReadonlyConfigStore {

		@Override
		public Optional<String> get(String key) {
			return null;
		}

		@Override
		public Map<String, String> getAll() {
			return emptyMap();
		}

	}

}
