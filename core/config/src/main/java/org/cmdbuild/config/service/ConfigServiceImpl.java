/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Collection;
import static java.util.Collections.singleton;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.clustering.ClusterMessageImpl;
import org.cmdbuild.clustering.ClusteringService;
import org.cmdbuild.clustering.ClusterMessageReceivedEvent;
import org.cmdbuild.config.api.ConfigAccesService;
import org.cmdbuild.config.api.ConfigReloadEventImpl;
import org.cmdbuild.config.api.ConfigDefinition;
import org.cmdbuild.config.api.ConfigDefinitionStore;
import org.cmdbuild.config.api.ConfigLocation;
import static org.cmdbuild.config.api.ConfigLocation.CL_DEFAULT;
import static org.cmdbuild.config.api.ConfigLocation.CL_FILE_ONLY;
import static org.cmdbuild.config.api.ReadonlyConfigStore.DB_CONFIG_STORE;
import static org.cmdbuild.config.api.ReadonlyConfigStore.FILE_CONFIG_STORE;
import org.cmdbuild.config.api.ConfigUpdateEventImpl;
import org.cmdbuild.utils.crypto.CmdbuildCryptoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.GlobalConfigService;
import org.cmdbuild.config.api.NamespacedConfigService;
import org.cmdbuild.config.api.WriteonlyConfigStore;
import org.cmdbuild.config.api.ReadonlyConfigStore;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotEmpty;

/**
 *
 */
@Component
public class ConfigServiceImpl implements GlobalConfigService {

	private final static String CONFIG_RELOAD_KEYS = "org.cmdbuild.config.RELOAD_KEYS", CONFIG_RELOAD_ALL = "org.cmdbuild.config.RELOAD_ALL";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus;

	private final Supplier<CmdbuildCryptoUtils> cryptoUtilsProvider = () -> CmdbuildCryptoUtils.defaultUtils();

	private final Map<ConfigLocation, WriteonlyConfigStore> writeonlyConfigStores;
	private final Map<ConfigLocation, ReadonlyConfigStore> readonlyConfigStores;
	private final ConfigAccesService configAccesService;
	private final ConfigDefinitionStore configDefinitionStore;
	private final ClusteringService clusteringService;

	public ConfigServiceImpl(ClusteringService clusteringService, @Qualifier(DB_CONFIG_STORE) WriteonlyConfigStore dbWriteonlyConfigStore, @Qualifier(FILE_CONFIG_STORE) WriteonlyConfigStore fileWriteonlyConfigStore, @Qualifier(DB_CONFIG_STORE) ReadonlyConfigStore dbReadonlyConfigStore, @Qualifier(FILE_CONFIG_STORE) ReadonlyConfigStore fileReadonlyConfigStore, ConfigAccesService configAccesService, ConfigDefinitionStore configDefinitionStore) {
		this.writeonlyConfigStores = ImmutableMap.of(CL_FILE_ONLY, fileWriteonlyConfigStore, CL_DEFAULT, dbWriteonlyConfigStore);
		this.readonlyConfigStores = ImmutableMap.of(CL_FILE_ONLY, fileReadonlyConfigStore, CL_DEFAULT, dbReadonlyConfigStore);
		this.configDefinitionStore = checkNotNull(configDefinitionStore);
		this.configAccesService = checkNotNull(configAccesService);
		this.clusteringService = checkNotNull(clusteringService);
		this.eventBus = configAccesService.getEventBus();
		clusteringService.getEventBus().register(new Object() {
			@Subscribe
			public void handleClusterMessageReceivedEvent(ClusterMessageReceivedEvent event) {
				if (event.isOfType(CONFIG_RELOAD_ALL)) {
					logger.debug("invalidate all config in response to a cluster message");
					doReload();
				} else if (event.isOfType(CONFIG_RELOAD_KEYS)) {
					List<String> keys = checkNotEmpty(event.getData("keys"));
					logger.debug("invalidate config keys in response to a cluster message, keys = {}", keys);
					invalidateCaches();
					postUpdate(keys);
				}
			}
		});
	}

	private void postUpdate(String... keys) {
		postUpdate(asList(keys));
	}

	private void postUpdate(Iterable<String> keys) {
		eventBus.post(new ConfigUpdateEventImpl(keys));
		eventBus.post(new ConfigReloadEventImpl(keys));
	}

	@Override
	public NamespacedConfigService getConfig(String namespace) {
		return configAccesService.getConfig(namespace);
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public @Nullable
	String getString(String key) {
		Optional<String> optional = doGetConfig(key);
		return optional != null && optional.isPresent() ? optional.get() : null;
	}

	@Override
	public @Nullable
	String getStringOrDefault(String key) {
		return Optional.ofNullable(doGetConfig(key)).orElse(Optional.ofNullable(configDefinitionStore.get(key).getDefaultValue())).orElse(null);
	}

	private Optional<String> doGetConfig(String key) {
		return readonlyConfigStores.get(getConfigLocation(key)).get(key);
	}

	private ConfigLocation getConfigLocation(String key) {
		return Optional.ofNullable(configDefinitionStore.getOrNull(key)).map(ConfigDefinition::getLocation).orElse(CL_DEFAULT);
	}

	@Override
	public void putString(String key, String value, boolean encrypt) {
		if (encrypt) {
			value = cryptoUtilsProvider.get().encryptValue(value);
		}
		putString(key, value);
	}

	@Override
	public void putString(String key, @Nullable String value) {
		checkNotNull(trimToNull(key));
		writeonlyConfigStores.get(getConfigLocation(key)).put(key, value);
		postUpdate(key);
		notifyConfigUpdate(singleton(key));
	}

	@Override
	public void putStrings(Map<String, String> map) {
		if (!map.isEmpty()) {
			map.keySet().forEach((key) -> checkNotNull(trimToNull(key)));
			map.forEach((key, value) -> {
				logger.debug("set config key = {} value = {}", key, value);
				writeonlyConfigStores.get(getConfigLocation(key)).put(key, value);
			});
			postUpdate(map.keySet());
			notifyConfigUpdate(map.keySet());
		}
	}

	@Override
	public void delete(String key) {
		checkNotNull(trimToNull(key));
		writeonlyConfigStores.get(getConfigLocation(key)).delete(key);
		postUpdate(key);
		notifyConfigUpdate(singleton(key));
	}

	@Override
	public Map<String, String> getConfigAsMap() {
		return (Map) map().accept((m) -> list(CL_FILE_ONLY, CL_DEFAULT).stream().map(readonlyConfigStores::get).map(ReadonlyConfigStore::getAll).forEach(m::putAll));
	}

	@Override
	public Map<String, String> getConfigOrDefaultsAsMap() {
		return map(configDefinitionStore.getAllDefaults()).with(getConfigAsMap());
	}

	@Override
	public Map<String, ConfigDefinition> getConfigDefinitions() {
		return configDefinitionStore.getAll();
	}

	@Override
	public synchronized void reload() {
		doReload();
		notifyConfigReload();
	}

	private synchronized void doReload() {
		invalidateCaches();
		postUpdate();
	}

	private synchronized void invalidateCaches() {
		readonlyConfigStores.values().forEach(ReadonlyConfigStore::invalidateCache);
	}

	private void notifyConfigReload() {
		clusteringService.sendMessage(ClusterMessageImpl.builder().withMessageType(CONFIG_RELOAD_ALL).build());
	}

	private void notifyConfigUpdate(Collection<String> keys) {
		clusteringService.sendMessage(ClusterMessageImpl.builder()
				.withMessageType(CONFIG_RELOAD_KEYS)
				.withMessageData(ImmutableMap.of("keys", new ArrayList<>(keys)))
				.build());
	}

}
