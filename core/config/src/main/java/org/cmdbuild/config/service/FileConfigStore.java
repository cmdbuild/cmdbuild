/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import com.google.common.base.Splitter;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.MoreCollectors.onlyElement;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.String.format;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import static java.util.stream.Collectors.joining;
import javax.annotation.Nullable;
import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FilenameUtils;
import static org.cmdbuild.config.api.ReadonlyConfigStore.FILE_CONFIG_STORE;
import static org.cmdbuild.config.service.ConfigUtils.addNamespaceToKey;
import static org.cmdbuild.config.service.ConfigUtils.stripNamespaceFromKey;
import org.cmdbuild.utils.crypto.CmdbuildCryptoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.DirectoryService;
import org.cmdbuild.config.api.WriteonlyConfigStore;
import org.cmdbuild.config.api.ReadonlyConfigStore;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbStringUtils.mapToLoggableString;

/**
 * file-backed config store impl (used for database config)
 */
@Component
@Qualifier(FILE_CONFIG_STORE)
public class FileConfigStore implements ReadonlyConfigStore, WriteonlyConfigStore {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DirectoryService directoryService;
	private final Map<String, ConfigFile> configFilesByNamespace = map();
	private Map<String, String> allConfig;

	public FileConfigStore(DirectoryService directoryService) {
		this.directoryService = checkNotNull(directoryService);
		reload();
	}

	@Override
	public void invalidateCache() {
		reload();
	}

	private synchronized void reload() {
		if (!directoryService.hasFilesystem()) {
			logger.warn("filesystem is not available, skipping config file load");
		} else {
			File configDirectory = directoryService.getConfigDirectory();
			if (configDirectory == null || !configDirectory.isDirectory() || !configDirectory.canRead()) {
				logger.warn("unable to load config files from config directory = {}", configDirectory);
			} else {
				logger.info("load config files from config directory = {}", configDirectory.getAbsolutePath());
				configFilesByNamespace.clear();
				list(configDirectory.listFiles()).stream().filter(f -> f.isFile() && f.getName().matches(".*[.](conf|properties)$")).forEach(file -> {
					String namespace = "org.cmdbuild." + FilenameUtils.getBaseName(file.getName()).toLowerCase();
					logger.debug("load config file for namespace = {} file = {}", namespace, file);
					ConfigFile configFile = new ConfigFile(namespace, file);
					configFilesByNamespace.put(namespace, configFile);
				});
			}
		}
		Map<String, String> map = map();
		configFilesByNamespace.values().forEach((configFile) -> {
			configFile.readConfigFromFile(true).entrySet().forEach((entry) -> {
				map.put(addNamespaceToKey(configFile.getNamespace(), entry.getKey()), entry.getValue());
			});
		});
		allConfig = map;
	}

	private ConfigFile getConfigFileForKeyCreateIfMissing(String key) {
		ConfigFile configFile = configFilesByNamespace.keySet().stream().filter((namespace) -> key.startsWith(namespace)).findAny().map((namespace) -> configFilesByNamespace.get(namespace)).orElse(null);
		if (configFile != null) {
			return configFile;
		} else {
			try {
				String namespace = Splitter.on(".").splitToList(key).stream().limit(3).collect(joining("."));
				String fileName = format("%s.conf", Splitter.on(".").splitToList(namespace).stream().skip(2).collect(onlyElement()));
				File file = new File(directoryService.getConfigDirectory(), fileName);
				file.createNewFile();
				reload();
				return checkNotNull(getConfigFileForKeyOrNull(key));
			} catch (Exception ex) {
				throw runtime(ex, "error creating config file for key = %s", key);
			}
		}
	}

	private @Nullable
	ConfigFile getConfigFileForKeyOrNull(String key) {
		return configFilesByNamespace.keySet().stream().filter((namespace) -> key.startsWith(namespace)).findAny().map((namespace) -> configFilesByNamespace.get(namespace)).orElse(null);
	}

	@Override
	public Optional<String> get(String key) {
		if (allConfig.containsKey(key)) {
			return Optional.ofNullable(allConfig.get(key));
		} else {
			return null;
		}
	}

	@Override
	public void put(String key, String value) {
		allConfig.put(key, value);
		ConfigFile configFile = getConfigFileForKeyCreateIfMissing(key);
		Map<String, String> map = Maps.newHashMap(configFile.readConfigFromFile(false));
		map.put(stripNamespaceFromKey(configFile.getNamespace(), key), value);
		configFile.writeConfigToFile(map);
	}

	@Override
	public void delete(String key) {
		allConfig.remove(key);
		ConfigFile configFile = getConfigFileForKeyCreateIfMissing(key);
		Map<String, String> map = Maps.newHashMap(configFile.readConfigFromFile(false));
		map.remove(stripNamespaceFromKey(configFile.getNamespace(), key));
		configFile.writeConfigToFile(map);
	}

	@Override
	public synchronized Map<String, String> getAll() {
		return Collections.unmodifiableMap(allConfig);
	}

	protected static class ConfigFile {

		private final Logger logger = LoggerFactory.getLogger(getClass());
		private final Supplier<CmdbuildCryptoUtils> cryptoUtilsProvider = () -> CmdbuildCryptoUtils.defaultUtils();

		private PropertiesConfiguration properties;
		private final File file;
		private final String namespace;
		private Map<String, String> config;

		public ConfigFile(String namespace, File file) {
			this.file = checkNotNull(file);
			this.namespace = checkNotNull(namespace);
		}

		public File getFile() {
			return file;
		}

		/**
		 * safe read, log error if it fails; return cached instance if present
		 * (and forceReload is false).
		 *
		 * @param forceReload
		 * @return
		 */
		public Map<String, String> readConfigFromFile(boolean forceReload) {
			if (config == null || forceReload) {
				if (file == null || !file.isFile() || !file.canRead()) {
					logger.info("cannot read config from file = {}, skipping", file);
					config = Collections.emptyMap();
				} else {
					logger.info("read config from file = {}", file);
					try {
						readConfigFromFileUnsafe();
					} catch (Exception ex) {
						logger.error("error reading config from file = " + file, ex);
						config = Collections.emptyMap();
					}
				}
			}
			return config;
		}

		public synchronized Map<String, String> readConfigFromFileUnsafe() throws FileNotFoundException, ConfigurationException, IOException {
			CmdbuildCryptoUtils cryptoUtils = cryptoUtilsProvider.get();
			try (InputStream in = new FileInputStream(file)) {
				properties = new PropertiesConfiguration();
				properties.setDelimiterParsingDisabled(true);
				properties.load(in);
				config = ImmutableMap.copyOf((Map) transformValues(ConfigurationConverter.getMap(properties), (value) -> cryptoUtils.decryptValue((String) value)));
			}
			return config;
		}

		public synchronized void writeConfigToFile(Map<String, String> config) {
			logger.info("write config to file = {}, config = \n\n{}\n", file, mapToLoggableString(config));
			try {
				this.config = ImmutableMap.copyOf(config);
				try (OutputStream out = new FileOutputStream(file)) {
					if (properties == null) {
						properties = new PropertiesConfiguration();
					}
					ConfigurationConverter.getMap(properties).keySet().stream().filter(not(config::containsKey)).map(String.class::cast).forEach(properties::clearProperty);
					config.forEach(properties::setProperty);
					properties.save(out);
				}
			} catch (Exception ex) {
				logger.error("error storing config to file = {}", file, ex);
			}
		}

		public String getNamespace() {
			return namespace;
		}
	}

}
