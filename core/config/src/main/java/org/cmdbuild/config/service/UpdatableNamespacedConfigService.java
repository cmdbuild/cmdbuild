/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.service;

import com.google.common.eventbus.EventBus;
import org.cmdbuild.config.api.ConfigDefinition;
import org.cmdbuild.config.api.NamespacedConfigService;
import org.cmdbuild.config.api.ReadonlyConfigStore;

public interface UpdatableNamespacedConfigService extends NamespacedConfigService {

	void setConfigServiceAndRegisterEventBus(ReadonlyConfigStore store, EventBus eventBus);

	ConfigDefinition addDefinition(ConfigDefinition configDefinition);
}
