/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.userconfig;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collections;
import java.util.Map;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.utils.json.CmJsonUtils.MAP_OF_STRINGS;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

/**
 *
 */
@Component
public class UserConfigServiceImpl implements UserConfigService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;
	private final CmdbCache<String, Map<String, String>> configByUsername;

	public UserConfigServiceImpl(DaoService dao, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		configByUsername = cacheService.newCache("user_preferences_by_username");
	}

	@Override
	public Map<String, String> getByUsername(String username) {
		checkNotBlank(username);
		return configByUsername.get(username, () -> doGetByUsername(username));
	}

	@Override
	public void setByUsername(String username, Map<String, String> data) {
		checkNotBlank(username);
		checkNotNull(data);
		dao.getJdbcTemplate().queryForObject("SELECT _cm3_user_config_set(?,?::jsonb)", Object.class, username, toJson(data));
		configByUsername.invalidate(username);
	}

	@Override
	public @Nullable
	Optional<String> getByUsername(String username, String key) {
		logger.debug("get config by usename = {} key = {}", username, key);
		Map<String, String> config = getByUsername(username);
		return config == null ? null : Optional.fromNullable(config.get(key));
	}

	@Override
	public void setByUsername(String username, String key, @Nullable String value) {
		logger.info("set config by usename = {} key = {} value = {}", username, key, value);
		Map<String, String> config = getByUsername(username);
		setByUsername(username, map(config).with(key, value));
	}

	@Override
	public void deleteByUsername(String username, String key) {
		logger.info("delete config by usename = {} key = {}", username, key);
		Map<String, String> config = getByUsername(username);
		setByUsername(username, map(config).withoutKey(key));
	}

	private Map<String, String> doGetByUsername(String username) {
		logger.debug("get config by usename = {}", username);
		return Collections.unmodifiableMap(fromJson(dao.getJdbcTemplate().queryForObject("SELECT _cm3_user_config_get(?)", String.class, username), MAP_OF_STRINGS));
	}

}
