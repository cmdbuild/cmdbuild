/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.authorization;

import com.google.common.base.Preconditions;
import org.cmdbuild.cardfilter.CardFilter;
import org.cmdbuild.auth.grant.PrivilegeSubjectWithInfo;

public class CardFilterAsPrivilegeAdapter implements PrivilegeSubjectWithInfo {

	private final CardFilter filter;

	public CardFilterAsPrivilegeAdapter(CardFilter filter) {
		this.filter = Preconditions.checkNotNull(filter);
	}

	@Override
	public Long getId() {
		return filter.getId();
	}

	@Override
	public String getName() {
		return filter.getName();
	}

	@Override
	public String getDescription() {
		return filter.getDescription();
	}

	@Override
	public String getPrivilegeId() {
		return "Filter:" + getId(); //TODO check this
	}

}
