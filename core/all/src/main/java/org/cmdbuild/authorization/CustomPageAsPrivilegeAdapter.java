package org.cmdbuild.authorization;

import static java.lang.String.format;

import org.cmdbuild.custompage.CustomPageData;
import org.cmdbuild.custompage.CustomPageInfo;
import org.cmdbuild.auth.grant.PrivilegeSubjectWithInfo;

public class CustomPageAsPrivilegeAdapter implements PrivilegeSubjectWithInfo {

	private final long id;
	private final String name, description;

	public CustomPageAsPrivilegeAdapter(CustomPageInfo delegate) {
		this.id = delegate.getId();
		this.name = delegate.getName();
		this.description = delegate.getDescription();
	}

	public CustomPageAsPrivilegeAdapter(CustomPageData delegate) {
		this.id = delegate.getId();
		this.name = delegate.getName();
		this.description = delegate.getDescription();
	}

	@Override
	public String getPrivilegeId() {
		return format("CustomPage:%d", getId());
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
