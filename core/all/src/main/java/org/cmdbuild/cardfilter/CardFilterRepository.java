package org.cmdbuild.cardfilter;

import java.util.List;
import java.util.Set;

public interface CardFilterRepository extends SharedCardFilterRepository, UserCardFilterRepository {

	List<CardFilter> getFiltersForRole(long roleId);

	void setFiltersForRole(long roleId, Set<Long> newFilters);

}
