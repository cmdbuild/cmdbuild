package org.cmdbuild.cardfilter;

import java.util.List;
import javax.annotation.Nullable;

public interface SharedCardFilterRepository {

	List<CardFilter> readSharedFilters(@Nullable String className);

	List<CardFilter> getAllSharedFilters();
}
