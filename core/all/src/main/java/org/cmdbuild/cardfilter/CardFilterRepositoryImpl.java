package org.cmdbuild.cardfilter;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import java.util.Set;
import static java.util.stream.Collectors.toSet;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import static org.cmdbuild.cardfilter.CardFilterConst.CLASS_ID;
import static org.cmdbuild.cardfilter.CardFilterConst.DB_REPO;
import static org.cmdbuild.cardfilter.CardFilterConst.SHARED;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.cardfilter.CardFilterConst.USER_ID;
import static org.cmdbuild.cardfilter.CardFilterImpl.FILTER_CLASS_NAME;
import static org.cmdbuild.common.Constants.ROLE_CLASS_NAME;
import static org.cmdbuild.dao.beans.CardIdAndClassNameImpl.card;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_CODE;
import org.springframework.context.annotation.Primary;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.WhereOperator.EQ;
import static org.cmdbuild.data.filter.SorterElement.SorterElementDirection.ASC;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@Primary
@Component(DB_REPO)
public class CardFilterRepositoryImpl implements CardFilterRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;

	private final CmdbCache<String, CardFilter> filtersById;

	public CardFilterRepositoryImpl(DaoService dao, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		filtersById = cacheService.newCache("card_filters_by_id");
	}

	@Override
	@Nullable
	public CardFilter readOrNull(Long filterId) {
		return filtersById.get(filterId.toString(), () -> dao.getById(CardFilterImpl.class, filterId).toModel());
	}

	@Override
	public List<CardFilter> readNonSharedFilters(String className, long userId) {
		return dao.selectAll().from(CardFilterImpl.class)
				.where(SHARED, EQ, false)
				.where(USER_ID, EQ, userId)
				.where(CLASS_ID, EQ, checkNotBlank(className))
				.orderBy(ATTR_CODE, ASC)
				.asList();
	}

	@Override
	public List<CardFilter> readSharedFilters(String className) {
		return dao.selectAll().from(CardFilterImpl.class)
				.where(SHARED, EQ, true)
				.where(CLASS_ID, EQ, checkNotBlank(className))
				.orderBy(ATTR_CODE, ASC)
				.asList();
	}

	@Override
	public CardFilter create(CardFilter filter) {
		return dao.create(CardFilterImpl.copyOf(filter).build());
	}

	@Override
	public CardFilter update(CardFilter filter) {
		filtersById.invalidate(filter.getId().toString());
		return dao.update(CardFilterImpl.copyOf(filter).build());
	}

	@Override
	public void delete(long filterId) {
		filtersById.invalidate(String.valueOf(filterId));
		dao.delete(CardFilterImpl.class, filterId);
	}

	@Override
	public List<CardFilter> getFiltersForRole(long roleId) {
		return dao.selectAll().from(CardFilterImpl.class)
				.where(SHARED, EQ, true)
				.whereExpr("\"Id\" IN ( SELECT \"IdObj1\" FROM \"Map_FilterRole\" WHERE \"IdObj2\" = ? AND \"Status\" = 'A' )", roleId)
				.orderBy(ATTR_CODE, ASC)
				.asList();
	}

	@Override
	public List<CardFilter> getAllSharedFilters() {
		return dao.selectAll().from(CardFilterImpl.class).where(SHARED, EQ, true).orderBy(ATTR_CODE, ASC).asList();
	}

	@Override
	public void setFiltersForRole(long roleId, Set<Long> newFilters) {//TODO validate than all filters for a role have different classes (add constraint on db ??)
		Set<Long> current = getFiltersForRole(roleId).stream().map(CardFilter::getId).collect(toSet());
		Set<Long> toAdd = set(newFilters).without(current),
				toRemove = set(current).without(newFilters);
		toRemove.forEach((f) -> dao.deleteRelation("FilterRole", f, roleId));
		toAdd.forEach((f) -> dao.createRelation("FilterRole", card(FILTER_CLASS_NAME, f), card(ROLE_CLASS_NAME, roleId)));
	}

}
