package org.cmdbuild.cardfilter;

import org.cmdbuild.auth.user.OperationUser;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import java.util.List;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.cmdbuild.auth.user.OperationUserSupplier;
import static org.cmdbuild.cardfilter.CardFilterConst.DB_REPO;
import static org.cmdbuild.cardfilter.CardFilterConst.SESSION_REPO;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CardFilterServiceImpl implements CardFilterService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final CardFilterRepository dbRepo;
	private final UserCardFilterRepository sessionRepo;
	private final OperationUserSupplier userStore;

	public CardFilterServiceImpl(@Qualifier(DB_REPO) CardFilterRepository store, @Qualifier(SESSION_REPO) UserCardFilterRepository sessionRepo, OperationUserSupplier userStore) { //TODO use also temp filter repo
		this.dbRepo = checkNotNull(store);
		this.sessionRepo = checkNotNull(sessionRepo);
		this.userStore = checkNotNull(userStore);
	}

	@Override
	public CardFilter create(CardFilter filter) {
		return dbRepo.create(filter);
	}

	@Override
	public CardFilter readOrNull(long filterId) {
		return firstNonNull(dbRepo.readOrNull(filterId), sessionRepo.readOrNull(filterId), null);
	}

	@Override
	public CardFilter update(CardFilter filter) {
		return dbRepo.update(filter);
	}

	@Override
	public void delete(long filterId) {
		dbRepo.delete(filterId);
	}

	@Override
	public List<CardFilter> readAllForCurrentUser(String className) {
		return list(readSharedForCurrentUser(className)).with(dbRepo.readNonSharedFilters(className, userStore.getUser().getLoginUser().getId()))
				.stream().sorted(Ordering.natural().onResultOf(CardFilter::getName)).collect(toList());
	}

	@Override
	public List<CardFilter> readSharedForCurrentUser(String className) {
		OperationUser operationUser = userStore.getUser();
		return dbRepo.readSharedFilters(className).stream().filter(operationUser::hasReadAccess).collect(toList());
	}

	@Override
	public List<CardFilter> readAllSharedFilters() {
		return dbRepo.getAllSharedFilters();
	}

	@Override
	public List<CardFilter> getFiltersForRole(long roleId) {
		return dbRepo.getFiltersForRole(roleId);
	}

	@Override
	public void setFiltersForRole(long roleId, Set<Long> newFilters) {
		dbRepo.setFiltersForRole(roleId, newFilters);
	}

}
