package org.cmdbuild.cardfilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.auth.user.OperationUserSupplier;

import org.cmdbuild.auth.session.inner.SessionDataService;
import static org.cmdbuild.cardfilter.CardFilterConst.SESSION_REPO;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.stereotype.Component;

@Component(SESSION_REPO)
public class SessionUserCardFilterRepository implements UserCardFilterRepository {

	private final static String SESSION_FILTERS_KEY = "org.cmdbuild.SESSION_FILTERS";

	private final Random random = new Random();
	private final ObjectMapper objectMapper = new ObjectMapper();

	private final SessionDataService sessionData;
	private final OperationUserSupplier user;

	public SessionUserCardFilterRepository(SessionDataService sessionLogic, OperationUserSupplier user) {
		this.sessionData = checkNotNull(sessionLogic);
		this.user = checkNotNull(user);
	}

	@Override
	public CardFilter create(CardFilter filter) {
		Long id = newId();
		filter = CardFilterImpl.copyOf(filter).withId(id).build();
		setCurrentSessionFilters(list(getCurrentSessionFilters()).with(filter));
		return filter;
	}

	@Override
	@Nullable
	public CardFilter readOrNull(Long filterId) {
		checkNotNull(filterId);
		return getCurrentSessionFilters().stream().filter((s) -> equal(s.getId(), filterId)).findAny().orElse(null);
	}

	@Override
	public CardFilter update(CardFilter filter) {
		filter = CardFilterImpl.copyOf(filter).build();
		long id = filter.getId();
		setCurrentSessionFilters(list(getCurrentSessionFilters())
				.without((f) -> f.getId() == id)
				.with(filter));
		return filter;

	}

	@Override
	public void delete(long filterId) {
		setCurrentSessionFilters(list(getCurrentSessionFilters()).without((f) -> Objects.equals(f.getId(), filterId)));
	}

	@Override
	public List<CardFilter> readNonSharedFilters(String className, long userId) {
		checkNotBlank(className);
		checkArgument(userId == user.getUser().getLoginUser().getId());
		return getCurrentSessionFilters().stream().filter((f) -> equal(f.getClassName(), className)).collect(toList());
	}

	private long newId() {
		return random.nextLong();
	}

	private List<CardFilter> getCurrentSessionFilters() {
		String filters = sessionData.getCurrentSessionDataSafe().get(SESSION_FILTERS_KEY);
		if (isBlank(filters)) {
			return emptyList();
		} else {
			return checkNotNull(fromJson(filters, new TypeReference<List<CardFilter>>() {
			}));
		}
	}

	private void setCurrentSessionFilters(List<CardFilter> filters) {
		if (isEmpty(filters)) {
			sessionData.getCurrentSessionDataSafe().remove(SESSION_FILTERS_KEY);
		} else {
			try {
				String str = objectMapper.writeValueAsString(filters);
				sessionData.getCurrentSessionDataSafe().put(SESSION_FILTERS_KEY, str);
			} catch (JsonProcessingException ex) {
				throw runtime(ex);
			}
		}
	}
}
