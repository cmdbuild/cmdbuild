package org.cmdbuild.cardfilter;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Optional;

import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;

public interface CardFilterService {

	CardFilter create(CardFilter filter);

	default Optional<CardFilter> read(CardFilter filter) {
		CardFilter res = readOrNull(filter.getId());
		return Optional.ofNullable(res);
	}

	@Nullable
	CardFilter readOrNull(long filterId);

	default CardFilter getById(long filterId) {
		return checkNotNull(readOrNull(filterId), "filter not found for id = %s", filterId);
	}

	CardFilter update(CardFilter filter);

	default void delete(CardFilter filter) {
		delete(filter.getId());
	}

	void delete(long filterId);

	List<CardFilter> readAllForCurrentUser(String className);

	List<CardFilter> readAllSharedFilters();

	List<CardFilter> readSharedForCurrentUser(String className);

	List<CardFilter> getFiltersForRole(long roleId);

	void setFiltersForRole(long roleId, Set<Long> newFilters);

}
