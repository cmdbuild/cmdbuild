package org.cmdbuild.cardfilter;

import java.util.List;
import javax.annotation.Nullable;

public interface UserCardFilterRepository {

	@Nullable
	CardFilter readOrNull(Long filterId);

	List<CardFilter> readNonSharedFilters(String className, long userId);

//	List<CardFilter> readNonSharedFilters(String className);

	CardFilter create(CardFilter filter);

	CardFilter update(CardFilter filter);

	void delete(long filterId);
}
