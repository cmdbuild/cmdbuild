/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.clustering.jgroups;

import com.google.common.eventbus.EventBus;
import java.util.List;

public interface ClusterConfiguration {

	boolean isClusterEnabled();

	EventBus getEventBus();

	List<String> getClusterNodes();

	int getTcpPort();

	String getClusterNodeId();

	String getClusterName();

}
