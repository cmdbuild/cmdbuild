package org.cmdbuild.clustering.jgroups;

import com.google.common.base.Joiner;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ComparisonChain;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import java.io.ByteArrayInputStream;
import static java.util.Collections.emptyList;
import static java.lang.String.format;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import javax.annotation.PreDestroy;

import org.cmdbuild.clustering.ClusterMessage;
import static org.cmdbuild.clustering.ClusterMessage.THIS_INSTANCE_ID;
import org.cmdbuild.clustering.ClusterMessageImpl;
import org.cmdbuild.clustering.ClusterMessageReceivedEvent;
import org.cmdbuild.clustering.ClusterNode;
import org.cmdbuild.clustering.ClusteringService;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.startup.PostStartup;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.Message.Flag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.ConfigReloadEvent;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_DISABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_NOTRUNNING;
import org.cmdbuild.utils.date.DateUtils;
import static org.cmdbuild.utils.date.DateUtils.now;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.readToString;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.jgroups.Address;
import org.jgroups.Event;
import org.jgroups.PhysicalAddress;
import org.jgroups.Receiver;
import org.jgroups.View;
import org.jgroups.logging.CustomLogFactory;
import org.jgroups.logging.Log;
import org.jgroups.logging.LogFactory;
import org.jgroups.logging.Slf4jLogImpl;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;

@Component
public class ClusteringServiceImpl implements ClusteringService, SystemService {

	private static final String TIMESTAMP_ATTR = "timestamp",
			SOURCE_ID_ATTR = "sourceId",
			MESSAGE_TYPE_ATTR = "type",
			MESSAGE_ID_ATTR = "messageId",
			DATA_ATTR = "data";
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus = new EventBus();

	private final ClusterConfiguration clusterConfig;

	private JChannel channel;

	public ClusteringServiceImpl(ClusterConfiguration clusterConfig) {
		this.clusterConfig = checkNotNull(clusterConfig);
		clusterConfig.getEventBus().register(new Object() {
			@Subscribe
			public void handleConfigUpdateEvent(ConfigReloadEvent event) {
				stopIfRunning();
				startIfEnabled();
			}
		});
	}

	@Override
	public String getServiceName() {
		return "Clustering";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (!isEnabled()) {
			return SS_DISABLED;
		} else if (isRunning()) {
			return SS_READY;
		} else {
			return SS_NOTRUNNING;
		}
	}

	@Override
	public void startService() {
		checkArgument(isEnabled());
		startIfEnabled();
	}

	@Override
	public void stopService() {
		checkArgument(isRunning());
		stopIfRunning();
	}

	@Override
	public List<ClusterNode> getClusterNodes() {
		checkArgument(isEnabled(), "clustering is not enabled");
		try {
			View view = channel.getView();
			return getClusterNodes(view);
		} catch (Exception ex) {
			logger.error(marker(), "error retrieving cluster members", ex);
			return emptyList();//TODO return this node
		}
	}

	private List<ClusterNode> getClusterNodes(View view) {
		Address thisAddress = channel.getAddress();
		return view.getMembers().stream().map((n) -> {
			String nodeId = n.toString();
			String address = getPhisicalAddressSafe(n);
			return new ClusterNodeImpl(nodeId, address, equal(n, thisAddress));
		}).sorted((a, b) -> ComparisonChain.start().compareTrueFirst(a.isThisNode(), b.isThisNode()).compare(a.getNodeId(), b.getNodeId()).result()).collect(toList());
	}

	private String getPhisicalAddressSafe(Address address) {
		try {
			PhysicalAddress physicalAddress = (PhysicalAddress) channel.down(new Event(Event.GET_PHYSICAL_ADDRESS, address));
			return physicalAddress.printIpAddress();
		} catch (Exception ex) {
			logger.debug("error retrieving phisical addres from addr = {}", address, ex);
			return "<unknown address>";
		}
	}

	@PostStartup
	public synchronized void init() {
		startIfEnabled();
	}

	private boolean isEnabled() {
		return clusterConfig.isClusterEnabled();
	}

	private synchronized void startIfEnabled() {
		if (isEnabled()) {
			start();
		} else {
			logger.info("clustering is disabled");
		}
	}

	@PreDestroy
	public synchronized void cleanup() {
		stopIfRunning();
	}

	@Override
	public boolean isRunning() {
		return channel != null && !channel.isClosed();
	}

	private synchronized void start() {
		try {
			startUnsafe();
		} catch (Exception ex) {
			logger.error(marker(), "unable to start clustering service", ex);
		}

	}

	public synchronized void startUnsafe() throws Exception {
		stopIfRunning();
		logger.info("starting clustering service");
		logger.debug("create new jgroups channel");
		try {
			LogFactory.setCustomLogFactory(new CustomLogFactory() {
				@Override
				public Log getLog(Class clazz) {
					return getLog(clazz.getName());
				}

				@Override
				public Log getLog(String category) {
					return new Slf4jLogImpl(format("org.cmdbuild.clustering.jgroups.%s", category));
				}
			});
			String config = readToString(getClass().getResourceAsStream("/org/cmdbuild/clustering/jgroups/jgroups.xml"));
			config = config.replaceAll(Pattern.quote("${tcp.port}"), String.valueOf(clusterConfig.getTcpPort())); //TODO use some template engine
			config = config.replaceAll(Pattern.quote("${tcpping.initial_hosts}"), Joiner.on(",").skipNulls().join(clusterConfig.getClusterNodes()));//TODO use some template engine
			channel = new JChannel(new ByteArrayInputStream(config.getBytes()));
			channel.setName(clusterConfig.getClusterNodeId());
			channel.setDiscardOwnMessages(true);
			channel.setReceiver(new MyReceiver());
			channel.connect(clusterConfig.getClusterName());
			logger.info("clustering service started");
		} catch (Exception ex) {
			stopIfRunning();
			throw ex;
		}
	}

	private class MyReceiver implements Receiver {

		@Override
		public void receive(Message message) {
			try {
				logger.info("received jgroups message = {}", message);
				Map<String, Object> payload = checkNotNull(message.getObject());
				ClusterMessage clusterMessage = ClusterMessageImpl.builder()
						.withTimestamp(Instant.ofEpochMilli((Long) payload.get(TIMESTAMP_ATTR)).atZone(DateUtils.getUserTimezoneOffset()))
						.withSourceInstanceId((String) payload.get(SOURCE_ID_ATTR))
						.withMessageType((String) payload.get(MESSAGE_TYPE_ATTR))
						.withMessageId((String) payload.get(MESSAGE_ID_ATTR))
						.withMessageData((Map<String, Object>) payload.get(DATA_ATTR))
						.build();
				logger.info("received cluster message = {}", clusterMessage);
				checkArgument(!equal(clusterMessage.getSourceInstanceId(), clusterConfig.getClusterNodeId()), "received cluster message with source node id = this node id");
				eventBus.post(new ClusterMessageReceivedEventImpl(clusterMessage));
			} catch (Exception ex) {
				logger.error("error processing jgroups message = {}", message, ex);
			}
		}

		@Override
		public void viewAccepted(View view) {
			try {
				logger.info("new cluster view received = \n\n{}\n", getClusterNodes(view).stream().map(n -> format("\t%s  %-10s    %s", n.isThisNode() ? "(this node)" : "           ", n.getNodeId(), n.getAddress())).collect(joining("\n")));
			} catch (Exception ex) {
				logger.error("error processing jgroups view = {}", view, ex);
			}
		}

	}

	private synchronized void stopIfRunning() {
		if (channel != null) {
			logger.info("stopping clustering service");
			try {
				channel.close();
			} catch (Exception ex) {
				logger.warn("error closing channel", ex);
			} finally {
				channel = null;
			}
			logger.info("clustering service stopped");
		}
	}

	@Override
	public void sendMessage(ClusterMessage clusterMessage) {
		if (isEnabled()) {
			try {
				doSendMessage(clusterMessage);
			} catch (Exception ex) {
				logger.warn(marker(), "error sending cluster message", ex);
			}
		}
	}

	private void doSendMessage(ClusterMessage clusterMessage) throws Exception {
		checkNotNull(channel, "jgroups channel is null (jgroups startup failed)");
		logger.info("send cluster message = {}", clusterMessage);
		checkArgument(equal(clusterMessage.getSourceInstanceId(), THIS_INSTANCE_ID));

		Map<String, Object> payload = map(
				SOURCE_ID_ATTR, clusterConfig.getClusterNodeId(),
				TIMESTAMP_ATTR, now().toInstant().toEpochMilli(),
				MESSAGE_ID_ATTR, clusterMessage.getMessageId(),
				MESSAGE_TYPE_ATTR, clusterMessage.getMessageType(),
				DATA_ATTR, new LinkedHashMap<>(clusterMessage.getMessageData()));

		Message message = new Message(null, payload);
		if (clusterMessage.requireRsvp()) {
			message.setFlag(Flag.RSVP);
		}
		logger.info("send jgroups message = {}", message);
		channel.send(message);
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	private class ClusterMessageReceivedEventImpl implements ClusterMessageReceivedEvent {

		final ClusterMessage clusterMessage;

		public ClusterMessageReceivedEventImpl(ClusterMessage clusterMessage) {
			this.clusterMessage = checkNotNull(clusterMessage);
		}

		@Override
		public ClusterMessage getClusterMessage() {
			return clusterMessage;
		}

	}

	private class ClusterNodeImpl implements ClusterNode {

		private final String address, nodeId;
		private final boolean isThisNode;

		public ClusterNodeImpl(String nodeId, String address, boolean isThisNode) {
			this.address = checkNotBlank(address);
			this.nodeId = checkNotBlank(nodeId);
			this.isThisNode = isThisNode;
		}

		@Override
		public String getAddress() {
			return address;
		}

		@Override
		public String getNodeId() {
			return nodeId;
		}

		@Override
		public boolean isThisNode() {
			return isThisNode;
		}

	}

}
