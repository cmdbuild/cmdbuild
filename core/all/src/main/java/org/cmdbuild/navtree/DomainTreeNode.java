package org.cmdbuild.navtree;

import com.google.common.collect.Iterables;
import static java.util.Collections.emptyList;
import java.util.List;
import javax.annotation.Nullable;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;

public interface DomainTreeNode {

	Long getId();

	String getTargetClassName();

	String getTargetClassDescription();

	@Nullable
	String getDomainName();

	String getType();

	@Nullable
	Long getIdParent();

	@Nullable
	Long getIdGroup();

	boolean getDirect();

	boolean getBaseNode();

	List<DomainTreeNode> getChildNodes();

	boolean hasChildNodes();

	@Nullable
	String getTargetFilter();

	String getDescription();

	boolean getEnableRecursion();

	default List<DomainTreeNode> getThisNodeAndAllDescendants() {
		return list(this)
				.with(getChildNodes().stream().map(DomainTreeNode::getThisNodeAndAllDescendants).map(Iterable.class::cast).reduce(emptyList(), (a, b) -> Iterables.concat(a, b)));
	}

}
