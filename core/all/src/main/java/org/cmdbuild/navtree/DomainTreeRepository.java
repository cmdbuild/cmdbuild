package org.cmdbuild.navtree;



import com.google.common.base.Predicate;
import java.util.List;

public interface DomainTreeRepository {

	List<DomainTreeInfo> getAll(Predicate<DomainTreeNode> predicate);

	DomainTreeNode getDomainTree(String type);

	void removeTree(String treeType);

}
