/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.navtree;

import javax.annotation.Nullable;

public interface NavTreeNode {

	String getClassId();

	String getDescription();

	long getCardId();

	@Nullable
	String getParentClassId();

	@Nullable
	Long getParentCardId();
}
