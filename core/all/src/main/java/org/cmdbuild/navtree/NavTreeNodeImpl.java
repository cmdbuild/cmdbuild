/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.navtree;

import static com.google.common.base.Strings.nullToEmpty;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class NavTreeNodeImpl implements NavTreeNode {

	private final String classId, description, parentClassId;
	private final long cardId;
	private final Long parentCardId;

	private NavTreeNodeImpl(NavTreeNodeImplBuilder builder) {
		this.classId = checkNotBlank(builder.classId);
		this.description = nullToEmpty(builder.description);
		this.parentClassId = (builder.parentClassId);
		this.cardId = builder.cardId;
		this.parentCardId = builder.parentCardId;
	}

	@Override
	public String getClassId() {
		return classId;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getParentClassId() {
		return parentClassId;
	}

	@Override
	public long getCardId() {
		return cardId;
	}

	@Override
	public Long getParentCardId() {
		return parentCardId;
	}

	public static NavTreeNodeImplBuilder builder() {
		return new NavTreeNodeImplBuilder();
	}

	public static NavTreeNodeImplBuilder copyOf(NavTreeNode source) {
		return new NavTreeNodeImplBuilder()
				.withClassId(source.getClassId())
				.withDescription(source.getDescription())
				.withParentClassId(source.getParentClassId())
				.withCardId(source.getCardId())
				.withParentCardId(source.getParentCardId());
	}

	public static class NavTreeNodeImplBuilder implements Builder<NavTreeNodeImpl, NavTreeNodeImplBuilder> {

		private String classId;
		private String description;
		private String parentClassId;
		private long cardId;
		private Long parentCardId;

		public NavTreeNodeImplBuilder withClassId(String classId) {
			this.classId = classId;
			return this;
		}

		public NavTreeNodeImplBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public NavTreeNodeImplBuilder withParentClassId(String parentClassId) {
			this.parentClassId = parentClassId;
			return this;
		}

		public NavTreeNodeImplBuilder withCardId(long cardId) {
			this.cardId = cardId;
			return this;
		}

		public NavTreeNodeImplBuilder withParentCardId(Long parentCardId) {
			this.parentCardId = parentCardId;
			return this;
		}

		@Override
		public NavTreeNodeImpl build() {
			return new NavTreeNodeImpl(this);
		}

	}
}
