package org.cmdbuild.navtree;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;
import com.google.common.collect.ImmutableList;
import static java.util.Collections.emptyList;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.firstNotNull;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@CardMapping("_DomainTreeNavigation")
public class DomainTreeNodeImpl implements DomainTreeNode, DomainTreeInfo {

	private final String targetClassName, targetClassDescription, domainName, type, targetFilter, description;
	private final Long idParent, idGroup, id;
	private final boolean direct, baseNode;
	private final List<DomainTreeNode> childNodes;
	private final boolean enableRecursion;

	private DomainTreeNodeImpl(DomainTreeNodeImplBuilder builder) {
		this.targetClassName = checkNotBlank(builder.targetClassName);
		this.targetClassDescription = checkNotNull(builder.targetClassDescription);
		this.domainName = builder.domainName;
		this.type = checkNotBlank(builder.type);
		this.targetFilter = builder.targetFilter;
		this.description = nullToEmpty(builder.description);
		this.idParent = builder.idParent;
		this.idGroup = builder.idGroup;
		this.id = builder.id;
		this.direct = checkNotNull(builder.direct);
		this.baseNode = checkNotNull(builder.baseNode);
		this.childNodes = builder.childNodes == null ? emptyList() : ImmutableList.copyOf(builder.childNodes);
		this.enableRecursion = firstNotNull(builder.enableRecursion, false);
	}

	@Override
	@CardAttr
	public String getTargetClassName() {
		return targetClassName;
	}

	@Override
	@CardAttr
	public String getTargetClassDescription() {
		return targetClassDescription;
	}

	@Override
	@CardAttr
	public String getDomainName() {
		return domainName;
	}

	@Override
	@CardAttr
	public String getType() {
		return type;
	}

	@Override
	@CardAttr
	public String getTargetFilter() {
		return targetFilter;
	}

	@Override
	@CardAttr
	public String getDescription() {
		return description;
	}

	@Override
	@CardAttr
	public Long getIdParent() {
		return idParent;
	}

	@Override
	@CardAttr
	public Long getIdGroup() {
		return idGroup;
	}

	@Override
	@CardAttr
	public Long getId() {
		return checkNotNull(id);
	}

	@Override
	@CardAttr
	public boolean getDirect() {
		return direct;
	}

	@Override
	@CardAttr
	public boolean getBaseNode() {
		return baseNode;
	}

	@Override
	public boolean hasChildNodes() {
		return !childNodes.isEmpty();
	}

	@Override
	public List<DomainTreeNode> getChildNodes() {
		return childNodes;
	}

	@Override
	@CardAttr
	public boolean getEnableRecursion() {
		return enableRecursion;
	}

	@Override
	public String toString() {
		return "DomainTreeNodeImpl{" + "targetClassName=" + targetClassName + ", domainName=" + domainName + ", type=" + type + ", targetFilter=" + targetFilter + ", idParent=" + idParent + ", idGroup=" + idGroup + ", id=" + id + ", direct=" + direct + '}';
	}

	public static DomainTreeNodeImplBuilder builder() {
		return new DomainTreeNodeImplBuilder();
	}

	public static DomainTreeNodeImplBuilder copyOf(DomainTreeNode source) {
		return new DomainTreeNodeImplBuilder()
				.withTargetClassName(source.getTargetClassName())
				.withTargetClassDescription(source.getTargetClassDescription())
				.withDomainName(source.getDomainName())
				.withType(source.getType())
				.withTargetFilter(source.getTargetFilter())
				.withDescription(source.getDescription())
				.withIdParent(source.getIdParent())
				.withIdGroup(source.getIdGroup())
				.withId(source.getId())
				.withDirect(source.getDirect())
				.withBaseNode(source.getBaseNode())
				.withChildNodes(source.getChildNodes())
				.withEnableRecursion(source.getEnableRecursion());
	}

	public static class DomainTreeNodeImplBuilder implements Builder<DomainTreeNodeImpl, DomainTreeNodeImplBuilder> {

		private String targetClassName;
		private String targetClassDescription;
		private String domainName;
		private String type;
		private String targetFilter;
		private String description;
		private Long idParent;
		private Long idGroup;
		private Long id;
		private boolean direct;
		private boolean baseNode;
		private final List<DomainTreeNode> childNodes = list();
		private Boolean enableRecursion;

		public DomainTreeNodeImplBuilder withTargetClassName(String targetClassName) {
			this.targetClassName = targetClassName;
			return this;
		}

		public DomainTreeNodeImplBuilder withTargetClassDescription(String targetClassDescription) {
			this.targetClassDescription = targetClassDescription;
			return this;
		}

		public DomainTreeNodeImplBuilder withDomainName(String domainName) {
			this.domainName = domainName;
			return this;
		}

		public DomainTreeNodeImplBuilder withType(String type) {
			this.type = type;
			return this;
		}

		public DomainTreeNodeImplBuilder withTargetFilter(String targetFilter) {
			this.targetFilter = targetFilter;
			return this;
		}

		public DomainTreeNodeImplBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public DomainTreeNodeImplBuilder withIdParent(Long idParent) {
			this.idParent = idParent;
			return this;
		}

		public DomainTreeNodeImplBuilder withIdGroup(Long idGroup) {
			this.idGroup = idGroup;
			return this;
		}

		public DomainTreeNodeImplBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public DomainTreeNodeImplBuilder withDirect(boolean direct) {
			this.direct = direct;
			return this;
		}

		public DomainTreeNodeImplBuilder withBaseNode(boolean baseNode) {
			this.baseNode = baseNode;
			return this;
		}

		public DomainTreeNodeImplBuilder withChildNodes(List<DomainTreeNode> childNodes) {
			this.childNodes.clear();
			this.childNodes.addAll(childNodes);
			return this;
		}

		public DomainTreeNodeImplBuilder addChild(DomainTreeNode node) {
			childNodes.add(checkNotNull(node));
			return this;
		}

		public DomainTreeNodeImplBuilder withEnableRecursion(Boolean enableRecursion) {
			this.enableRecursion = enableRecursion;
			return this;
		}

		@Override
		public DomainTreeNodeImpl build() {
			return new DomainTreeNodeImpl(this);
		}

	}
}
