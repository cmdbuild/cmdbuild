package org.cmdbuild.navtree;

import static com.google.common.base.Preconditions.checkNotNull;


import com.google.common.base.Predicate;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class NavigationTreeServiceImpl implements NavigationTreeService {

	private final DomainTreeRepository store;

	public NavigationTreeServiceImpl(DomainTreeRepository domainTreeStore) {
		this.store = checkNotNull(domainTreeStore);
	}

	@Override
	public List<DomainTreeInfo> getAll(Predicate<DomainTreeNode> predicate) {
		return store.getAll(predicate);
	}

	@Override
	public DomainTreeNode getTreeOrNull(String name) {
		return store.getDomainTree(name);
	}

	@Override
	public void delete(String name) {
		store.removeTree(name);
	}

}
