package org.cmdbuild.navtree;

import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.Ordering;
import java.util.List;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.WhereOperator.EQ;
import static org.cmdbuild.dao.core.q3.WhereOperator.ISNULL;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@Component
public class DomainTreeRepositoryImpl implements DomainTreeRepository {

	private final DaoService dao;
	private final CmdbCache<String, DomainTreeNode> treeByType;

	public DomainTreeRepositoryImpl(DaoService dao, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		this.treeByType = cacheService.newCache("domain_tree_by_type");
	}

	private void invalidateCache() {
		treeByType.invalidateAll();
	}

	@Override
	public List<DomainTreeInfo> getAll(Predicate<DomainTreeNode> predicate) {
		return dao.selectAll().from(DomainTreeNodeImpl.class)
				.where("IdParent", ISNULL)
				.asList(DomainTreeNode.class).stream()
				.filter((d) -> predicate.apply(d))
				.map(DomainTreeInfo.class::cast)
				.sorted(Ordering.natural().onResultOf(DomainTreeInfo::getType))
				.collect(toList());
	}

	@Override
	public DomainTreeNode getDomainTree(String type) {
		return treeByType.get(type, () -> doGetDomainTree(type));
	}

	private DomainTreeNode doGetDomainTree(String type) {
		return new DomainTreeHelper(type).buildDomainTree();
	}

	private class DomainTreeHelper {

		private final String type;
		private Long rootId;
		private Map<Long, Pair<DomainTreeNode, List<Long>>> nodesById;

		public DomainTreeHelper(String type) {
			this.type = checkNotBlank(type);
		}

		public DomainTreeNode buildDomainTree() {

			nodesById = dao.selectAll().from(DomainTreeNodeImpl.class)
					.where("Type", EQ, type)
					.asList(DomainTreeNode.class).stream().collect(toMap(DomainTreeNode::getId, (n) -> Pair.of(n, list())));

			checkArgument(!nodesById.isEmpty(), "domain tree not found for type = %s", type);

			nodesById.values().forEach((node) -> {
				if (node.getLeft().getIdParent() == null) {
					checkArgument(rootId == null, "duplicate root node found for type = %s, n1 = %s, n2 = %s", type, rootId, node.getLeft().getId());
					rootId = node.getLeft().getId();
				} else {
					Pair<DomainTreeNode, List<Long>> parent = checkNotNull(nodesById.get(node.getLeft().getIdParent()));
					parent.getRight().add(node.getLeft().getId());
				}
			});

			checkNotNull(rootId, "root node not found for domain tree = %s", type);

			return nodeIdToExpandedNode(rootId);
		}

		private DomainTreeNode nodeIdToExpandedNode(long id) {
			Pair<DomainTreeNode, List<Long>> pair = checkNotNull(nodesById.get(id));
			return DomainTreeNodeImpl.copyOf(pair.getLeft()).accept((b) -> pair.getRight().stream().map(this::nodeIdToExpandedNode).forEach(b::addChild)).build();
		}
	}

	@Override
	public void removeTree(String treeType) {
		getDomainTree(treeType).getThisNodeAndAllDescendants().forEach(dao::delete);
		invalidateCache();
	}
}
