package org.cmdbuild.navtree;

public interface DomainTreeInfo {

	String getType();

	String getDescription();

}
