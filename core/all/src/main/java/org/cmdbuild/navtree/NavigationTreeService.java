package org.cmdbuild.navtree;

import static com.google.common.base.Preconditions.checkNotNull;


import com.google.common.base.Predicate;
import java.util.List;
import javax.annotation.Nullable;

public interface NavigationTreeService {

	List<DomainTreeInfo> getAll(Predicate<DomainTreeNode> predicate);

	@Nullable
	DomainTreeNode getTreeOrNull(String name);

	default DomainTreeNode getTree(String name) {
		return checkNotNull(getTreeOrNull(name), "domain tree not found for name = %s", name);
	}

	void delete(String name);

}
