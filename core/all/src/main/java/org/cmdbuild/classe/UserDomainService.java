/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.classe;

import java.util.List;
import org.cmdbuild.dao.entrytype.Domain;

public interface UserDomainService {

	public List<Domain> getUserDomains();

	public Domain getUserDomain(String domainId);

}
