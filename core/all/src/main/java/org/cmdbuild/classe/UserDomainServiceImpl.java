/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.classe;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.isNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Maps.uniqueIndex;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.cmdbuild.classe.UserClassUtils.ROLE_PRIVILEGES_TO_CLASS_PERMISSIONS;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.entrytype.ClassPermission;
import org.cmdbuild.dao.entrytype.ClassPermissions;
import org.cmdbuild.dao.entrytype.ClassPermissionsImpl;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.DomainImpl;
import static org.cmdbuild.dao.entrytype.PermissionScope.PS_SERVICE;
import org.cmdbuild.dao.user.UserDaoHelperService;
import org.springframework.stereotype.Component;

@Component
public class UserDomainServiceImpl implements UserDomainService {

	private final UserDaoHelperService userHelper;
	private final UserClassService userClassService;
	private final DaoService dao;

	public UserDomainServiceImpl(UserDaoHelperService userHelper, UserClassService userClassService, DaoService dao) {
		this.userHelper = checkNotNull(userHelper);
		this.userClassService = checkNotNull(userClassService);
		this.dao = checkNotNull(dao);
	}

	@Override
	public List<Domain> getUserDomains() {
		UserDomainPermissionHelper helper = new UserDomainPermissionHelper();
		return dao.getAllDomains().stream().map(helper::toUserDomain).filter(Domain::hasServiceListPermission).collect(toList());
	}

	@Override
	public Domain getUserDomain(String domainId) {
		Domain userDomain = new UserDomainPermissionHelper().toUserDomain(dao.getDomain(domainId));
		checkArgument(userDomain.hasServiceListPermission(), "user is not allowed to read domain = %s", userDomain);
		return userDomain;
	}

	private class UserDomainPermissionHelper {

		private final Map<String, Classe> userClasses;

		public UserDomainPermissionHelper() {
			this.userClasses = uniqueIndex(userClassService.getAllUserClasses(), Classe::getName);
		}

		public Domain toUserDomain(Domain domain) {
			Set<ClassPermission> rolePermissions = userHelper.getRolePrivileges().stream().map(ROLE_PRIVILEGES_TO_CLASS_PERMISSIONS::get).filter(not(isNull())).collect(toSet());

			ClassPermissions sourcePermissions = domain.getSourceClasses().stream().map(Classe::getName).map(userClasses::get).filter(not(isNull())).map(ClassPermissions.class::cast).reduce((a, b) -> ClassPermissionsImpl.copyOf(a).addPermissions(b).build()).orElse(ClassPermissionsImpl.none());
			ClassPermissions targetPermissions = domain.getTargetClasses().stream().map(Classe::getName).map(userClasses::get).filter(not(isNull())).map(ClassPermissions.class::cast).reduce((a, b) -> ClassPermissionsImpl.copyOf(a).addPermissions(b).build()).orElse(ClassPermissionsImpl.none());

			ClassPermissions userPermissions = ClassPermissionsImpl.copyOf(sourcePermissions).intersectPermissions(targetPermissions).addPermissions(PS_SERVICE, rolePermissions).build();

			return DomainImpl.copyOf(domain).withPermissions(ClassPermissionsImpl.copyOf(domain).intersectPermissions(userPermissions).build()).build();
		}

	}
}
