/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.classe;

import static com.google.common.base.Objects.equal;
import static java.lang.String.format;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import static java.util.stream.Collectors.toSet;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import org.cmdbuild.dao.core.q3.QueryBuilder;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.ClasseImpl;
import org.cmdbuild.data.filter.CmdbFilter;

public interface UserCardAccess {

	CmdbFilter getWholeClassFilter();

	Map<String, UserCardAccessWithFilter> getSubsetFiltersByName();

	Classe getUserClass(Set<String> activeFilters);

	default boolean hasWholeClassFilter() {
		return !getWholeClassFilter().isNoop();
	}

	default Consumer<QueryBuilder> addSubsetFilterMarkersToQueryVisitor() {
		return (q) -> {
			getSubsetFiltersByName().forEach((key, a) -> {
				q.selectMatchFilter(format("cm_filter_mark_%s", key), a.getFilter());
			});
		};
	}

	default Card addCardAccessPermissionsFromSubfilterMark(Card card) {
		Set<String> activeFilters = getSubsetFiltersByName().keySet().stream().filter((k) -> card.get(format("cm_filter_mark_%s", k), Boolean.class) == true).collect(toSet());
		Classe userClass = getUserClass(activeFilters);
		if (!equal(userClass.getName(), card.getType().getName())) {
			userClass = ClasseImpl.copyOf(card.getType())
					.withAttributes(userClass.getAllAttributes())
					.withPermissions(userClass)
					.build();
		}
		return CardImpl.copyOf(card).withType(userClass).build();
	}
}
