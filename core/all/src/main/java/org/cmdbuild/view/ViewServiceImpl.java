package org.cmdbuild.view;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.onlyElement;

import java.util.List;
import static java.util.stream.Collectors.toList;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.auth.user.OperationUser;

import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.core.q3.DaoService;
import org.springframework.stereotype.Component;
import static org.cmdbuild.view.ViewImpl.toImpl;

@Component
public class ViewServiceImpl implements ViewService {

	private final DaoService dao;
	private final OperationUserSupplier userStore;
	private final Holder<List<View>> viewCache;

	public ViewServiceImpl(DaoService dao, OperationUserSupplier userStore, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		this.userStore = checkNotNull(userStore);
		viewCache = cacheService.newHolder("view_all");
	}

	private void invalidateChache() {
		viewCache.invalidate();
	}

	@Override
	public List<View> getUserViews() {
		OperationUser user = userStore.getUser();
		return getAllViews().stream().filter((v) -> {
			return (user.hasAdminAccess() || user.hasReadAccess(v))
					&& (v.isOfType(ViewType.FILTER) ? isActive(v.getSourceClass()) : true);
		}).collect(toList());
	}

	@Override
	public List<View> getAllViews() {
		return viewCache.get(this::doGetAllViews);
	}

	private List<View> doGetAllViews() {
		return dao.selectAll().from(ViewImpl.class).asList();
	}

	private boolean isActive(String classId) {
		return dao.getClasse(classId).isActive();
	}

	@Override
	public List<View> read(ViewType type) {
		return getUserViews().stream().filter((v) -> v.isOfType(type)).collect(toList());
	}

	@Override
	public View get(long id) {
		return getAllViews().stream().filter((v) -> v.getId() == id).collect(onlyElement());
	}

	@Override
	public View create(View view) {
		checkArgument(view.getId() == null);
		view = dao.create(toImpl(view));
		invalidateChache();
		return view;
	}

	@Override
	public View update(View view) {
		view = dao.update(toImpl(view));
		invalidateChache();
		return view;
	}

	@Override
	public void delete(long id) {
		dao.delete(ViewImpl.class, id);
		invalidateChache();
	}

}
