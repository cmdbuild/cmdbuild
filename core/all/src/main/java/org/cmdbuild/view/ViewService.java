package org.cmdbuild.view;

import java.util.List;

public interface ViewService {

	List<View> getAllViews();

	List<View> getUserViews();

	List<View> read(ViewType type);

	View get(long id);

	View create(View view);

	View update(View view);

	void delete(long id);

}
