/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.platform;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import static java.lang.String.format;
import org.cmdbuild.config.api.DirectoryService;
import org.cmdbuild.debuginfo.BuildInfo;
import org.cmdbuild.debuginfo.BuildInfoService;
import static org.cmdbuild.debuginfo.BuildInfoUtils.parseBuildInfo;
import static org.cmdbuild.utils.exec.CmdbProcessUtils.executeProcess;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.sysTmpDir;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.loadProperties;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.readToString;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.writeToFile;
import static org.cmdbuild.utils.io.CmdbuildZipUtils.getZipFileContentByName;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PlatformServiceImpl implements PlatformService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DirectoryService directoryService;
	private final BuildInfoService buildInfoService;

	public PlatformServiceImpl(DirectoryService directoryService, BuildInfoService buildInfoService) {
		this.directoryService = checkNotNull(directoryService);
		this.buildInfoService = checkNotNull(buildInfoService);
	}

	@Override
	public void stopContainer() {
		executeCommand("stop");
	}

	@Override
	public void restartContainer() {
		executeCommand("restart");
	}

	@Override
	public void upgradeWebapp(byte[] newWarData) {
		//TODO validate war data before upgrade; check version
		BuildInfo buildInfo = parseBuildInfo(loadProperties(getZipFileContentByName(newWarData, "git.properties")));

		logger.info("upgrade cmdbuild, cur version = {}, new version = {}", buildInfoService.getBuildInfo().getCommitInfo(), buildInfo.getCommitInfo());

		File newWarFile = new File(sysTmpDir(), format("%s.war", randomId()));
		writeToFile(newWarData, newWarFile);
		executeCommand("upgrade", directoryService.getWebappRootDirectory().getAbsolutePath(), newWarFile.getAbsolutePath());
	}

	private void executeCommand(String command, String... otherParams) {
		checkNotBlank(command);
		String tomcatDir = directoryService.getCatalinaBaseDirectory().getAbsolutePath();
		String scriptContent = readToString(getClass().getResourceAsStream("/org/cmdbuild/platform/scripts/cmdbuild_platform_helper.sh"));
		File file = new File(sysTmpDir(), format("%s_script.sh", randomId()));
		file.getParentFile().mkdirs();
		writeToFile(scriptContent, file);
		executeProcess(list("/bin/bash", "-l", file.getAbsolutePath(), tomcatDir, command).with(otherParams).toArray(new String[]{}));
	}

}
