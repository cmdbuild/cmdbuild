/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.debuginfo;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.DirectoryService;
import static org.cmdbuild.debuginfo.BuildInfoUtils.parseBuildInfo;

@Component
public class BuildInfoServiceImpl implements BuildInfoService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final BuildInfo buildInfo;

	public BuildInfoServiceImpl(DirectoryService directoryService) {
		BuildInfo b;
		try {
			Properties gitProperties = new Properties();
			try (FileInputStream in = new FileInputStream(new File(directoryService.getWebappRootDirectory(), "WEB-INF/classes/git.properties"))) {
				gitProperties.load(in);
			}
			b = parseBuildInfo(gitProperties);
		} catch (Exception ex) {
			logger.warn(marker(), "unable to get git info for this build", ex);
			b = null;
		}
		buildInfo = b;
	}

	@Override
	public BuildInfo getBuildInfo() {
		return checkNotNull(buildInfo, "build info not available!");
	}

	@Override
	public boolean hasBuildInfo() {
		return buildInfo != null;
	}

}
