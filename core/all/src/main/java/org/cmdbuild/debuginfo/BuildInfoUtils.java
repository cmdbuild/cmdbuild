/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.debuginfo;

import static java.lang.String.format;
import java.time.ZonedDateTime;
import java.util.Properties;
import org.cmdbuild.utils.date.DateUtils;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class BuildInfoUtils {

	public static BuildInfo parseBuildInfo(Properties properties) {
		return new BuildInfoImpl(properties);
	}

	private static class BuildInfoImpl implements BuildInfo {

		private final String commitId, branch;
		private final ZonedDateTime timestamp;

		private BuildInfoImpl(Properties gitProperties) {
			commitId = checkNotBlank(gitProperties.getProperty("git.commit.id.abbrev"));
			branch = checkNotBlank(gitProperties.getProperty("git.branch"));
			timestamp = DateUtils.toDateTime(checkNotBlank(gitProperties.getProperty("git.commit.time")));
		}

		@Override
		public String getCommitInfo() {
			return format("%s/%s (%s)", commitId, branch, DateUtils.toIsoDateTime(timestamp));
		}

	}
}
