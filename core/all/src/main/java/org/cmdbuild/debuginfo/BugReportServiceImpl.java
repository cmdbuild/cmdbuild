/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.debuginfo;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.Math.toIntExact;
import static java.lang.String.format;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Nullable;
import javax.mail.util.ByteArrayDataSource;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import org.apache.commons.io.IOUtils;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.cmdbuild.common.logging.LoggerConfigurationService;
import static org.cmdbuild.config.api.ReadonlyConfigStore.FILE_CONFIG_STORE;
import org.cmdbuild.config.api.GlobalConfigService;
import org.cmdbuild.dao.driver.postgres.DumpService;
import org.cmdbuild.report.ReportConst;
import org.cmdbuild.report.ReportService;
import org.cmdbuild.utils.date.DateUtils;
import org.cmdbuild.utils.io.CmdbuildFileUtils;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempFile;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.toByteArray;
import org.cmdbuild.utils.io.CmdbuildIoUtils;
import static org.cmdbuild.utils.io.CmdbuildNetUtils.getHostname;
import static org.cmdbuild.utils.io.CmdbuildPropertyUtils.serializeMapAsProperties;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.codehaus.plexus.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.ReadonlyConfigStore;

@Component
public class BugReportServiceImpl implements BugReportService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LoggerConfigurationService loggerService;
	private final ReportService reportService;
	private final DumpService dumpService;
	private final GlobalConfigService configService;
	private final ReadonlyConfigStore databaseConfig;

	public BugReportServiceImpl(LoggerConfigurationService loggerService, ReportService reportService, DumpService dumpService, GlobalConfigService configService, @Qualifier(FILE_CONFIG_STORE) ReadonlyConfigStore databaseConfig) {
		this.loggerService = checkNotNull(loggerService);
		this.reportService = checkNotNull(reportService);
		this.dumpService = checkNotNull(dumpService);
		this.configService = checkNotNull(configService);
		this.databaseConfig = checkNotNull(databaseConfig);
	}

	@Override
	public DataSource generateBugReport() {
		File zip = generateBugReportFile();
		byte[] zipData = toByteArray(zip);
		deleteQuietly(zip);
		String zipName = generateZipName();
		logger.info("return debug info zip {} {}", zipName, FileUtils.byteCountToDisplaySize(zipData.length));
		return new ByteArrayDataSource(zipData, ContentType.APPLICATION_OCTET_STREAM.getMimeType()) {
			{
				setName(zipName);
			}
		};
	}

	private File generateBugReportFile() {
		return generateBugReportFile(null);
	}

	private File generateBugReportFile(@Nullable String message) {

		logger.info("building debug info zip file");
		List<Pair<String, byte[]>> list = list();

		List<File> files = loggerService.getLogFiles();
		files.stream().filter(File::exists).forEach((file) -> {
			logger.debug("load log file = {}", file.getAbsolutePath());
			list.add(Pair.of(file.getName(), CmdbuildFileUtils.toByteArray(file)));
		});

		list.add(toPair(reportService.executeReportFromFile("system_status_log_report", ReportConst.ReportExtension.PDF)));
		list.add(toPair(reportService.executeReportFromFile("system_status_log_report", ReportConst.ReportExtension.CSV)));

		list.add(Pair.of("system_config.properties", serializeMapAsProperties(configService.getConfigAsMap()).getBytes()));
		list.add(Pair.of("database_config.properties", serializeMapAsProperties(databaseConfig.getAll()).getBytes()));

		if (isNotBlank(message)) {
			list.add(Pair.of("message.txt", message.getBytes()));
		}

		File dump = tempFile();
		try {
			try {
				dumpService.dumpDatabaseToFile(dump);
			} catch (Exception ex) {
				logger.error("error creating database dump", ex);
			}
			if (dump.exists() && dump.length() > 0) {
				list.add(Pair.of("database.backup", toByteArray(dump)));
			}
		} finally {
			deleteQuietly(dump);
		}

		File zip = tempFile();

		try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip))) {
			for (Pair<String, byte[]> record : list) {
				logger.debug("add zip entry = {} {}", record.getLeft(), FileUtils.byteCountToDisplaySize(record.getRight().length));
				ZipEntry zipEntry = new ZipEntry(checkNotBlank(record.getLeft()));
				out.putNextEntry(zipEntry);
				out.write(record.getRight(), 0, record.getRight().length);
				out.closeEntry();
			}
		} catch (IOException ex) {
			throw runtime(ex);
		}

		return zip;
	}

	private String generateZipName() {
		return format("cmdbuild_bugreport_%s_%s.zip", getHostname().toLowerCase().replaceAll("[^a-z0-9]", ""), DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm").format(DateUtils.now()));
	}

	private Pair<String, byte[]> toPair(DataHandler dataHandler) {
		return Pair.of(dataHandler.getName(), CmdbuildIoUtils.toByteArray(dataHandler));
	}

	@Override
	public BugReportInfo sendBugReport(@Nullable String message) {
		File zip = generateBugReportFile(message);
		String zipName = generateZipName();
		try {
			logger.info("begin upload of bug report zip file {} {}", zipName, FileUtils.byteCountToDisplaySize(toIntExact(zip.length())));
			uploadBugreport(zip, zipName);//TODO get report server via config
			logger.info("completed upload of bug report zip file {} {}", zipName, FileUtils.byteCountToDisplaySize(toIntExact(zip.length())));
			return new DebugInfoImpl(zipName);
		} finally {
			deleteQuietly(zip);
		}
	}

	private void uploadBugreport(File reportfile, String filename) {
		checkNotNull(reportfile);
		checkArgument(reportfile.exists() && reportfile.length() > 0);
		CloseableHttpClient client = HttpClientBuilder.create().build();
		try {
			HttpEntity payload = MultipartEntityBuilder.create()
					.addBinaryBody("file", reportfile, ContentType.APPLICATION_OCTET_STREAM, filename)
					.build();
			HttpPost request = new HttpPost("http://10.0.0.106:8013/bugreport");
			request.setEntity(payload);
			String response = IOUtils.toString(client.execute(request).getEntity().getContent());
			//TODO handle response
		} catch (IOException ex) {
			throw runtime(ex);
		} finally {
			try {
				client.close();
			} catch (IOException ex) {
			}
		}
	}
}
