/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.audit;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import javax.annotation.PreDestroy;
import static org.apache.commons.lang3.StringUtils.abbreviate;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.audit.RequestInfo.NO_SESSION_USER;
import org.cmdbuild.audit.RequestDataImpl.SimpleRequestDataBuilder;
import org.cmdbuild.scheduler.spring.ScheduledJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.RequestTrackingConfiguration;
import org.cmdbuild.auth.session.SessionService;
import org.cmdbuild.requestcontext.RequestContextService;
import org.cmdbuild.startup.BootService;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

/**
 *
 */
@Component
public class RequestTrackingServiceImpl implements RequestTrackingService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final RequestTrackingConfiguration config;
	private final RequestTrackingWritableRepository store;
	private final SessionService sessionService;

	private final ScheduledExecutorService scheduledExecutorService;
	private final Cache<String, OngoingRequestStatus> ongoingRequests = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.DAYS).build();

	private final BootService bootService;

	public RequestTrackingServiceImpl(RequestTrackingConfiguration config, RequestTrackingWritableRepository store, SessionService sessionService, RequestContextService contextService, BootService bootService) {
		logger.debug("init");
		this.config = checkNotNull(config);
		this.store = checkNotNull(store);
		this.sessionService = checkNotNull(sessionService);
		this.bootService = checkNotNull(bootService);
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(); // if we use a single thread executor we can avoid a bunch of synchronization later. Also, this act as a performance control.
		scheduledExecutorService.submit(() -> {
			contextService.initCurrentRequestContext("request tracking background job");
			//TODO set admin user
		});
	}

	private static class OngoingRequestStatus {

		public final String trackingId;
		public final boolean alreadyPersisted;
		public final ScheduledFuture persistOngoingRequestJob;

		public OngoingRequestStatus(String trackingId, boolean alreadyPersisted, @Nullable ScheduledFuture persistOngoingRequestJob) {
			this.trackingId = checkNotBlank(trackingId);
			this.alreadyPersisted = alreadyPersisted;
			this.persistOngoingRequestJob = persistOngoingRequestJob;
		}

	}

	@PreDestroy
	public void cleanup() {
		logger.info("cleanup");
		scheduledExecutorService.shutdown();
		try {
			scheduledExecutorService.awaitTermination(3, TimeUnit.SECONDS);
		} catch (InterruptedException ex) {
		}
		if (!scheduledExecutorService.isTerminated()) {
			logger.warn("executor service failed to stop in time");
		}
	}

	@ScheduledJob("0 0 * * * ?") //run every hour
	public void doCleanupRequestTable() {
		cleanupRequestTable();
	}

	@Override
	public void cleanupRequestTable() {
		Integer maxRecordAgeToKeepSeconds = config.getMaxRecordAgeToKeepSeconds();
		if (maxRecordAgeToKeepSeconds != null && maxRecordAgeToKeepSeconds > 0) {
			store.cleanupRequestTableForMaxAge(maxRecordAgeToKeepSeconds);
		}
		Integer maxRecordsToKeep = config.getMaxRecordsToKeep();
		if (maxRecordsToKeep != null && maxRecordsToKeep > 0) {
			store.cleanupRequestTableForMaxRecords(maxRecordsToKeep);
		}
	}

	@Override
	public void dropAllData() {
		store.dropAll();
		//note: this may cause inconsistencies with ongoingRequestsNotYetPersisted and possible pending requests: use with care!
	}

	private boolean isDbPersistEnabled() {
		return (config.getMaxRecordsToKeep() == null || config.getMaxRecordsToKeep() != 0) && (config.getMaxRecordAgeToKeepSeconds() == null || config.getMaxRecordAgeToKeepSeconds() != 0) && bootService.isSystemReady();
	}

	@Override
	public void requestBegin(RequestData data) {
		RequestData thisData = processRequestData(data);
		logger.debug("request begin = {}", thisData);
		if (isDbPersistEnabled()) {
			scheduledExecutorService.submit(() -> {
				ScheduledFuture persistOngoingRequestJob = scheduledExecutorService.schedule(() -> {
					try {
						ongoingRequests.put(thisData.getTrackingId(), new OngoingRequestStatus(thisData.getTrackingId(), true, null));
						store.create(thisData);
					} catch (Exception ex) {
						logger.error("error processing request begin, request = {}", thisData);
						logger.error("error processing request begin", ex);
					}
				}, 10, TimeUnit.SECONDS);
				ongoingRequests.put(thisData.getTrackingId(), new OngoingRequestStatus(thisData.getTrackingId(), false, persistOngoingRequestJob));
			});
		} else {
			logger.trace("db persist disabled, skipping...");
		}
	}

	@Override
	public void requestComplete(RequestData data) {
		try {
			RequestData thisData = processRequestData(data);
			logger.debug("request complete = {}", thisData);
			checkArgument(thisData.isCompleted());
			if (isDbPersistEnabled()) {
				scheduledExecutorService.submit(() -> {
					try {
						OngoingRequestStatus ongoingRequestStatus = ongoingRequests.getIfPresent(data.getTrackingId());
						if (ongoingRequestStatus == null) {
							store.create(thisData);
						} else {
							if (ongoingRequestStatus.persistOngoingRequestJob != null) {
								ongoingRequestStatus.persistOngoingRequestJob.cancel(true);
							}
							if (ongoingRequestStatus.alreadyPersisted) {
								store.update(thisData);
							} else {
								store.create(thisData);
							}
							ongoingRequests.invalidate(data.getTrackingId());
						}
					} catch (Exception ex) {
						logger.error("error processing request completion, request = {}", thisData);
						logger.error("error processing request completion", ex);
					}
				});
			} else {
				logger.trace("db persist disabled, skipping...");
			}
		} catch (Exception ex) {
			throw runtime(ex);
		}
	}

	private RequestData processRequestData(RequestData data) {
		SimpleRequestDataBuilder builder = RequestDataImpl.copyOf(data);
		parseSessionTokenFromSoapMessage(builder);
		addUsername(builder);
		trimPayload(builder);
		return builder.build();
	}

	private void parseSessionTokenFromSoapMessage(SimpleRequestDataBuilder builder) {
		if (builder.hasPayload() && !builder.hasSession()) {
			Matcher matcher = Pattern.compile("CMDBuild-Authorization[>]([^<]+)[<]/CMDBuild-Authorization").matcher(builder.getPayload());
			if (matcher.find()) {
				builder.withSessionId(matcher.group(1));
			}
		}
	}

	private void addUsername(SimpleRequestDataBuilder builder) {
		String username = NO_SESSION_USER;
		try {
			if (builder.hasSession()) {
				username = sessionService.getSessionById(builder.getSessionId()).getOperationUser().getLoginUser().getUsername();
			}
		} catch (Exception ex) {
			logger.debug("unable to retrieve username for session = " + builder.getSessionId(), ex);
		}
		builder.withUser(username);
	}

	private void trimPayload(SimpleRequestDataBuilder builder) {
		if (builder.hasPayload()) {
			builder.withPayload(trimPayload(builder.getPayload()));
		}
		if (!isBlank(builder.getResponse())) {
			builder.withResponse(trimPayload(builder.getResponse()));
		}
	}

	private String trimPayload(String payload) {
		if (config.getMaxPayloadLength() > 0 && payload.length() > config.getMaxPayloadLength()) {
			String suffix = " PAYLOAD_TRIMMED_TO_" + config.getMaxPayloadLength() + "_BYTES";
			int maxLenMinusSuffix = config.getMaxPayloadLength() - suffix.length();
			return maxLenMinusSuffix > 0
					? (abbreviate(payload, maxLenMinusSuffix) + suffix)
					: abbreviate(payload, config.getMaxPayloadLength());
		} else {
			return payload;
		}
	}

}
