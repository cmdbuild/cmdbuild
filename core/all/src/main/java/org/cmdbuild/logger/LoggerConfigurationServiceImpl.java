/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.logger;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.common.logging.LogbackConfigurationStore;
import org.cmdbuild.common.logging.LoggerConfig;
import org.cmdbuild.common.logging.LoggerConfigurationService;
import org.cmdbuild.common.logging.SimpleLoggerConfig;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.cmdbuild.config.api.DirectoryService;

/**
 *
 * @author davide
 */
@Component
public class LoggerConfigurationServiceImpl implements LoggerConfigurationService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LogbackConfigurationStore repository;
	private final DirectoryService directoryService;

	public LoggerConfigurationServiceImpl(LogbackConfigurationStore repository, DirectoryService directoryService) {
		this.repository = checkNotNull(repository);
		this.directoryService = checkNotNull(directoryService);
	}

	@Override
	public Document getDocument() {
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setNamespaceAware(false);
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			return documentBuilder.parse(new InputSource(new StringReader(repository.getLogbackXmlConfiguration())));
		} catch (ParserConfigurationException | SAXException | IOException ex) {
			throw runtime(ex);
		}
	}

	@Override
	public void setDocument(Document document) {
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(document), new StreamResult(writer));
			repository.setLogbackXmlConfiguration(writer.toString());
		} catch (TransformerException ex) {
			throw runtime(ex);
		}
	}

	@Override
	public List<File> getLogFiles() {
		try {
			List<File> list = list();
			NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().compile("//*[local-name()='appender']/*[local-name()='file']/text()").evaluate(getDocument(), XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				String fileName = nodeList.item(i).getNodeValue();
				if (isNotBlank(fileName)) {
					fileName = fileName.replaceFirst(Pattern.quote("${catalina.base}"), directoryService.getCatalinaBaseDirectory().getAbsolutePath());
					list.add(new File(fileName));
				}
			}
			list.add(new File(directoryService.getCatalinaBaseDirectory(), "logs/catalina.out"));
			return list;
		} catch (XPathExpressionException ex) {
			throw runtime(ex);
		}
	}

	@Override
	public List<LoggerConfig> getAllLoggerConfig() {
		logger.info("getAllLoggerConfig");
		return asList(getDocument().getElementsByTagName("logger")).stream().map((node) -> new SimpleLoggerConfig(((Element) node).getAttribute("name"), ((Element) node).getAttribute("level"))).collect(toList());
	}

	@Override
	public void updateLoggerConfig(LoggerConfig loggerConfig) {
		setLoggerConfig(loggerConfig);
	}

	@Override
	public void addLoggerConfig(LoggerConfig simpleLoggerConfig) {
		setLoggerConfig(simpleLoggerConfig);
	}

	private void setLoggerConfig(LoggerConfig loggerConfig) {
		logger.info("setLoggerConfig = {}", loggerConfig);
		Document document = getDocument();
		Stream<Element> stream = asList(document.getElementsByTagName("logger")).stream().map((Node n) -> ((Element) n));

		Optional<Element> thisLogger = stream.filter((element) -> equal(element.getAttribute("name"), loggerConfig.getCategory())).findFirst();
		if (thisLogger.isPresent()) {
			logger.debug("logger config already present, update element = {}", thisLogger.get());
			thisLogger.get().setAttribute("level", loggerConfig.getLevel());
		} else {
			logger.debug("logger config not present, insert new logger element before root logger element, for category = {}", loggerConfig.getCategory());
			Element rootLogger = (Element) document.getElementsByTagName("root").item(0);
			Element newLogger = document.createElement("logger");
			newLogger.setAttribute("name", loggerConfig.getCategory());
			newLogger.setAttribute("level", loggerConfig.getLevel());
			rootLogger.getParentNode().insertBefore(newLogger, rootLogger);
			rootLogger.getParentNode().insertBefore(document.createTextNode("\n\n\t"), rootLogger);
		}

		setDocument(document);
	}

	@Override
	public void removeLoggerConfig(String category) {
		logger.info("removeLoggerConfig = {}", category);
		Document document = getDocument();
		asList(document.getElementsByTagName("logger")).stream().filter((node) -> equal(((Element) node).getAttribute("name"), category)).forEach((node) -> {
			node.getParentNode().removeChild(node);
		});

		setDocument(document);
	}

	private static List<Node> asList(NodeList nodeList) {//TODO move this to common xml lib
		List<Node> list = newArrayList();
		for (int i = 0; i < nodeList.getLength(); i++) {
			list.add(nodeList.item(i));
		}
		return list;
	}

}
