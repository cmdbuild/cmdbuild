/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.cache;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheStats;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
@Qualifier(SYSTEM_LEVEL_ONE)
public class LocalCacheServiceImpl implements CacheService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Map<String, CmdbCache> caches = map();

	@Override
	public synchronized <V> CmdbCache<String, V> getCache(String cacheName) {
		return checkNotNull(caches.get(cacheName), "cache not found for name = %s", cacheName);
	}

	@Override
	public synchronized <V> CmdbCache<String, V> newCache(String cacheName, CacheConfig cacheConfig) {
		logger.debug("create cache {} (configuration {})", cacheName, cacheConfig);
		checkNotBlank(cacheName);
		checkNotNull(cacheConfig);
		checkArgument(!caches.containsKey(cacheName), "already created a cache for name = %s", cacheName);
		CacheBuilder builder = CacheBuilder.newBuilder();
		switch (cacheConfig) {
			case DEFAULT:
				builder.expireAfterWrite(1, TimeUnit.HOURS);
				break;
			case SYSTEM_OBJECTS:
				//nothing to do
				break;
			default:
				throw new IllegalArgumentException();
		}
		Cache<String, V> inner = builder.build();
		CmdbCache<String, V> cmdbCache = new CmdbCache<String, V>() {
			@Override
			public V getIfPresent(String key) {
				return inner.getIfPresent(key);
			}

			@Override
			public V get(String key, Callable<? extends V> loader) {
				try {
					return inner.get(key, loader);
				} catch (ExecutionException ex) {
					throw runtime(ex);
				}
			}

			@Override
			public ImmutableMap<String, V> getAllPresent(Iterable<?> keys) {
				return inner.getAllPresent(keys);
			}

			@Override
			public void put(String key, V value) {
				inner.put(key, value);
			}

			@Override
			public void putAll(Map<? extends String, ? extends V> m) {
				inner.putAll(m);
			}

			@Override
			public void invalidate(String key) {
				inner.invalidate(key);
			}

			@Override
			public void invalidateAll(Iterable<? extends String> keys) {
				inner.invalidateAll(keys);
			}

			@Override
			public void invalidateAll() {
				inner.invalidateAll();
			}

			@Override
			public long size() {
				return inner.size();
			}

			@Override
			public CacheStats stats() {
				return inner.stats();
			}

			@Override
			public Map<String, V> asMap() {
				return inner.asMap();
			}

			@Override
			public void cleanUp() {
				inner.cleanUp();
			}

			@Override
			public String getName() {
				return cacheName;
			}
		};
		caches.put(cacheName, cmdbCache);
		return cmdbCache;
	}

	@Override
	public synchronized void invalidateAll() {
		logger.debug("invalidate all local caches");
		caches.values().forEach((cache) -> {
			cache.invalidateAll();
		});
	}

	@Override
	public synchronized Map<String, CmCacheStats> getStats() {
		return CmCacheUtils.getStats(caches);
	}

}
