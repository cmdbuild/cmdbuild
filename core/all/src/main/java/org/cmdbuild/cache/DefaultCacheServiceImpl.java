/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.cache;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.isNull;
import com.google.common.cache.CacheStats;
import com.google.common.collect.ImmutableMap;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Lists.newArrayList;
import com.google.common.eventbus.Subscribe;
import static java.util.Arrays.asList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.clustering.ClusterMessageImpl;
import org.cmdbuild.clustering.ClusteringService;
import org.cmdbuild.clustering.ClusterMessageReceivedEvent;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_TWO;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
@Primary
@Qualifier(SYSTEM_LEVEL_TWO)
public class DefaultCacheServiceImpl implements CacheService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String CACHE_INVALIDATE_KEYS = "org.cmdbuild.cache.CacheService.INVALIDATE_KEYS",
			CACHE_INVALIDATE_ALL = "org.cmdbuild.cache.CacheService.INVALIDATE_ALL";

	private final CacheService localCacheService;
	private final ClusteringService clusteringService;

	private final Map<String, MyCache> caches = map();

	public DefaultCacheServiceImpl(@Qualifier(SYSTEM_LEVEL_ONE) CacheService localCacheService, ClusteringService clusteringService) {
		this.localCacheService = checkNotNull(localCacheService);
		this.clusteringService = checkNotNull(clusteringService);
		clusteringService.getEventBus().register(new Object() {

			@Subscribe
			public void handleClusterMessageReceivedEvent(ClusterMessageReceivedEvent event) {
				try {
					if (event.isOfType(CACHE_INVALIDATE_KEYS)) {
						String cache = checkNotNull(event.getData("cache")), key = checkNotNull(event.getData("key"));
						logger.debug("received cache invalidate from cluster for cache = {} key = {}", cache, key);
						checkNotNull(caches.get(cache), "cache not found for name = {}", cache).getInner().invalidate(key);
					} else if (event.isOfType(CACHE_INVALIDATE_ALL)) {
						String cache = checkNotNull(event.getData("cache"));
						logger.debug("received cache invalidate from cluster for cache = {}, all keys", cache);
						checkNotNull(caches.get(cache), "cache not found for name = {}", cache).getInner().invalidateAll();
					}
				} catch (Exception ex) {
					logger.error("error processing cluster message event " + event, ex);
				}
			}
		});
	}

	@Override
	public synchronized <V> CmdbCache<String, V> newCache(String cacheName, CacheConfig cacheConfig) {
		logger.debug("create cache {} (configuration {})", cacheName, cacheConfig);
		checkNotNull(trimToNull(cacheName));
		checkNotNull(cacheConfig);
		checkArgument(!caches.containsKey(cacheName), "cache name '%s' already used", cacheName);
		CmdbCache<String, V> cache = localCacheService.newCache(cacheName + "_CLUSTERED", cacheConfig);
		caches.put(cacheName, new MyCache(cacheName, cache));
		return cache;
	}

	private void invalidateAllOnCluster(String cache) {
		logger.debug("invalidate all cache entries on cluster for cache = {}", cache);
		checkNotNull(cache);
		clusteringService.sendMessage(ClusterMessageImpl.builder()
				.withMessageType(CACHE_INVALIDATE_ALL)
				.withMessageData(ImmutableMap.of("cache", cache))
				.build());
	}

	private void invalidateOneOnCluster(String cache, String key) {
		invalidateManyOnCluster(cache, asList(key));
	}

	private void invalidateManyOnCluster(String cache, Iterable<String> keys) {
		logger.debug("invalidate many cache entries on cluster for cache = {} keys = {}", cache, keys);
		checkNotNull(keys);
		checkNotNull(cache);
		checkArgument(!any(keys, isNull()));
		clusteringService.sendMessage(ClusterMessageImpl.builder()
				.withMessageType(CACHE_INVALIDATE_KEYS)
				.withMessageData(ImmutableMap.of("cache", cache, "keys", newArrayList(keys)))
				.build());
	}

	@Override
	public synchronized void invalidateAll() {
		logger.info("invalidate all caches");
		caches.values().forEach((cache) -> {
			cache.invalidateAll();
		});
	}

	@Override
	public synchronized <V> CmdbCache<String, V> getCache(String cacheName) {
		return checkNotNull(caches.get(cacheName), "cache not found for name = %s", cacheName);
	}

	@Override
	public Map<String, CmCacheStats> getStats() {
		return CmCacheUtils.getStats((Map) caches);
	}

	private class MyCache<V> implements CmdbCache<String, V> {

		private final String name;
		private final CmdbCache<String, V> inner;

		public MyCache(String name, CmdbCache<String, V> inner) {
			this.name = checkNotBlank(name);
			this.inner = checkNotNull(inner);
		}

		@Override
		public String getName() {
			return name;
		}

		public CmdbCache<String, V> getInner() {
			return inner;
		}

		@Override
		public V getIfPresent(String key) {
			return inner.getIfPresent(key);
		}

		@Override
		public V get(String key, Callable<? extends V> loader) {
			return inner.get(key, loader);
		}

		@Override
		public Map<String, V> getAllPresent(Iterable<?> keys) {
			return inner.getAllPresent(keys);
		}

		@Override
		public void put(String key, V value) {
			inner.put(key, value);
			invalidateOneOnCluster(name, key);
		}

		@Override
		public void putAll(Map<? extends String, ? extends V> m) {
			inner.putAll(m);
			invalidateManyOnCluster(name, (Iterable) m.keySet());
		}

		@Override
		public void invalidate(String key) {
			inner.invalidate(key);
			invalidateOneOnCluster(name, (String) key);
		}

		@Override
		public void invalidateAll(Iterable<? extends String> keys) {
			inner.invalidateAll(keys);
			invalidateManyOnCluster(name, (Iterable) keys);
		}

		@Override
		public void invalidateAll() {
			inner.invalidateAll();
			invalidateAllOnCluster(name);
		}

		@Override
		public long size() {
			return inner.size();
		}

		@Override
		public CacheStats stats() {
			return inner.stats();
		}

		@Override
		public Map<String, V> asMap() {
			return Collections.unmodifiableMap(inner.asMap());
		}

		@Override
		public void cleanUp() {
			inner.cleanUp();
		}

	}

}
