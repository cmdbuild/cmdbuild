package org.cmdbuild.config.levelone;

import org.cmdbuild.dao.config.DatabaseConfiguration;
import com.google.common.eventbus.EventBus;
import org.cmdbuild.config.api.ConfigComponent;
import static org.cmdbuild.config.api.ConfigLocation.CL_FILE_ONLY;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import static org.cmdbuild.config.api.ConfigValue.TRUE;
import org.cmdbuild.config.api.NamespacedConfigService;
import org.springframework.stereotype.Component;

@Component("databaseConfiguration")
@ConfigComponent("org.cmdbuild.database")
public final class DatabaseConfigurationImpl implements DatabaseConfiguration {

	public static final String DEFAULT_DB_DRIVER_CLASS_NAME = "org.postgresql.Driver";

	@ConfigValue(key = "db.url", description = "database url", defaultValue = "", location = CL_FILE_ONLY, isProtected = true)
	private String url;

	@ConfigValue(key = "db.username", description = "database username", defaultValue = "", location = CL_FILE_ONLY, isProtected = true)
	private String username;

	@ConfigValue(key = "db.password", description = "database password", defaultValue = "", location = CL_FILE_ONLY, isProtected = true)
	private String password;

	@ConfigValue(key = "db.driverClassName", description = "database driver", defaultValue = DEFAULT_DB_DRIVER_CLASS_NAME, location = CL_FILE_ONLY, isProtected = true)
	private String driverClassName;

	@ConfigValue(key = "db.admin.username", description = "database admin username", defaultValue = "", location = CL_FILE_ONLY, isProtected = true)
	private String adminUsername;

	@ConfigValue(key = "db.admin.password", description = "database admin password", defaultValue = "", location = CL_FILE_ONLY, isProtected = true)
	private String adminPassword;

	@ConfigValue(key = "db.autopatch.enable", description = "enable db auto patch", defaultValue = FALSE, location = CL_FILE_ONLY, isProtected = true)
	private boolean enableAutoPatch;

	@ConfigValue(key = "checkConnectionAtStartup", description = "check db connection at startup", defaultValue = TRUE, location = CL_FILE_ONLY, isProtected = true)
	private boolean checkDbAtStartup;

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public String getDatabaseUrl() {
		return url;
	}

	@Override
	public String getDatabaseUser() {
		return username;
	}

	@Override
	public String getDatabasePassword() {
		return password;
	}

	@Override
	public String getDatabaseAdminUsername() {
		return adminUsername;
	}

	@Override
	public String getDatabaseAdminPassword() {
		return adminPassword;
	}

	@Override
	public String getDriverClassName() {
		return driverClassName;
	}

	@Override
	public boolean enableAutoPatch() {
		return enableAutoPatch;
	}

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

	@Override
	public boolean enableDatabaseConnectionEagerCheck() {
		return checkDbAtStartup;
	}

}
