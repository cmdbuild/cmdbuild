package org.cmdbuild.config.leveltwo;

import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.EmailQueueConfiguration;

@Component
@ConfigComponent("org.cmdbuild.email")
public final class EmailConfigurationImpl implements EmailQueueConfiguration {

	@ConfigValue(key = "queue.enabled", description = "", defaultValue = FALSE)
	private boolean queueEnabled;

	@ConfigValue(key = "queue.time", description = "", defaultValue = "0")
	private int queueTime;

	@Override
	public boolean isQueueProcessingEnabled() {
		return queueEnabled;
	}

	@Override
	public long getQueueTime() {
		return queueTime;
	}

}
