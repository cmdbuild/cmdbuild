package org.cmdbuild.config.leveltwo;

import com.google.common.base.Splitter;
import static com.google.common.base.Strings.nullToEmpty;
import java.util.Collection;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import static org.cmdbuild.config.api.ConfigValue.TRUE;
import org.springframework.stereotype.Component;
import org.cmdbuild.auth.login.AuthenticationConfiguration;

@Component
@ConfigComponent("org.cmdbuild.auth")
public final class AuthConfigurationImpl implements AuthenticationConfiguration {

	@ConfigValue(key = "force.ws.password.digest", description = "", defaultValue = TRUE)
	private boolean forceWsPasswordDigest;

	@ConfigValue(key = "ldap.use.ssl", description = "", defaultValue = FALSE)
	private boolean ldapUseSsl;

	@ConfigValue(key = "case.insensitive", description = "", defaultValue = TRUE)
	private boolean authCaseInsensitive;

	@ConfigValue(key = "methods", description = "auth methods", defaultValue = "DBAuthenticator,file")
	private String authMethods;

	@ConfigValue(key = "header.attribute.name", description = "", defaultValue = "username")
	private String headerAttributeName;

	@ConfigValue(key = "cas.server.url", description = "", defaultValue = "")
	private String casServerUrl;

	@ConfigValue(key = "cas.login.page", description = "", defaultValue = "/login")
	private String casLoginPage;

	@ConfigValue(key = "cas.ticket.param", description = "", defaultValue = "ticket")
	private String casTicketParam;

	@ConfigValue(key = "cas.service.param", description = "", defaultValue = "service")
	private String casServiceParam;

	@ConfigValue(key = "ldap.basedn", description = "", defaultValue = "")
	private String ldapBaseDn;

	@ConfigValue(key = "ldap.server.address", description = "", defaultValue = "")
	private String ldapServerAddress;

	@ConfigValue(key = "ldap.server.port", description = "", defaultValue = "389")
	private String ldapServerPort;

	@ConfigValue(key = "ldap.bind.attribute", description = "", defaultValue = "")
	private String ldapBindAttribute;

	@ConfigValue(key = "ldap.search.filter", description = "", defaultValue = "")
	private String ldapSearchFilter;

	@ConfigValue(key = "ldap.search.auth.method", description = "", defaultValue = "")
	private String ldapAuthenticationMethod;

	@ConfigValue(key = "ldap.search.auth.principal", description = "", defaultValue = "")
	private String ldapAuthenticationPrincipal;

	@ConfigValue(key = "ldap.search.auth.password", description = "", defaultValue = "")
	private String ldapAuthenticationPassword;

	public boolean isLdapConfigured() {
		return !(isBlank(getLdapBindAttribute()) || isBlank(getLdapBaseDN()) || isBlank(getLdapServerAddress()));
	}

	public boolean isHeaderConfigured() {
		return !(isBlank(getHeaderAttributeName()));
	}

	public boolean isCasConfigured() {
		return !(isBlank(getCasServerUrl()));
	}

	@Override
	public boolean isCaseInsensitive() {
		return authCaseInsensitive;
	}

	@Override
	public boolean getForceWSPasswordDigest() {
		return forceWsPasswordDigest;
	}

	@Override
	public String getHeaderAttributeName() {
		return headerAttributeName;
	}

	@Override
	public String getCasServerUrl() {
		return casServerUrl;
	}

	@Override
	public String getCasLoginPage() {
		return casLoginPage;
	}

	@Override
	public String getCasTicketParam() {
		return casTicketParam;
	}

	@Override
	public String getCasServiceParam() {
		return casServiceParam;
	}

	@Override
	public String getLdapUrl() {
		return String.format("%s://%s:%s", getLdapProtocol(), getLdapServerAddress(), getLdapServerPort());
	}

	private String getLdapServerAddress() {
		return ldapServerAddress;
	}

	private String getLdapServerPort() {
		return ldapServerPort;
	}

	private String getLdapProtocol() {
		return getLdapUseSsl() ? "ldaps" : "ldap";
	}

	private boolean getLdapUseSsl() {
		return ldapUseSsl;
	}

	@Override
	public String getLdapBaseDN() {
		return ldapBaseDn;
	}

	@Override
	public String getLdapBindAttribute() {
		return ldapBindAttribute;
	}

	@Override
	public String getLdapSearchFilter() {
		return ldapSearchFilter;
	}

	@Override
	public String getLdapAuthenticationMethod() {
		return ldapAuthenticationMethod;
	}

	@Override
	public String getLdapPrincipal() {
		return ldapAuthenticationPrincipal;
	}

	@Override
	public String getLdapPrincipalCredentials() {
		return ldapAuthenticationPassword;
	}

	@Override
	public Collection<String> getActiveAuthenticators() {
		return Splitter.on(",").omitEmptyStrings().trimResults().splitToList(nullToEmpty(authMethods));
	}

}
