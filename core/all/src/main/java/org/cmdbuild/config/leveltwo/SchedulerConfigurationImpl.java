package org.cmdbuild.config.leveltwo;

import com.google.common.collect.ImmutableMap;
import static com.google.common.collect.Maps.filterKeys;
import com.google.common.eventbus.EventBus;
import java.util.Map;
import org.cmdbuild.config.SchedulerConfiguration;
import org.cmdbuild.config.api.ConfigComponent;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.NamespacedConfigService;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component
@ConfigComponent("org.cmdbuild.scheduler")
public final class SchedulerConfigurationImpl implements SchedulerConfiguration {

	private final static Map<String, String> QUARTZ_DEFAULT_CONFIG_PARAMS = ImmutableMap.<String, String>builder()
			.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool")
			.put("org.quartz.threadPool.threadCount", "5")
			.put("org.quartz.threadPool.threadPriority", "4")
			.put("org.quartz.scheduler.skipUpdateCheck", "true")
			.put("org.quartz.scheduler.instanceId", "AUTO")
			.put("org.quartz.scheduler.instanceName", "CMDBuildScheduler")
			.put("org.quartz.jobStore.useProperties", "true")
			.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.PostgreSQLDelegate")
			.put("org.quartz.jobStore.tablePrefix", "quartz.qrtz_")
			.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX")
			.put("org.quartz.jobStore.isClustered", "true")
			.build();

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

	@Override
	public Map<String, String> getQuartzProperties() {
		return map(QUARTZ_DEFAULT_CONFIG_PARAMS).with(filterKeys(config.getAsMap(), k -> k.startsWith("org.quartz.")));
	}

}
