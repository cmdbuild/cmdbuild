/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.leveltwo;

import com.google.common.eventbus.EventBus;
import org.cmdbuild.config.UiFilterConfiguration;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import org.cmdbuild.config.api.NamespacedConfigService;
import org.springframework.stereotype.Component;

@Component
@ConfigComponent("org.cmdbuild.ui")
public class UiFilterConfigurationImpl implements UiFilterConfiguration {

	@ConfigValue(key = "uiServiceBaseUrl", description = "ui service base url", defaultValue = AUTO_URL)
	private String uiServiceBaseUrl;

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public String getBaseUrl() {
		return uiServiceBaseUrl;
	}

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}
}
