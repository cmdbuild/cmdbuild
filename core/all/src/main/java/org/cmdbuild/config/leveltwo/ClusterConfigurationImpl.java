/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.config.leveltwo;

import com.google.common.base.Splitter;
import static com.google.common.base.Strings.nullToEmpty;
import com.google.common.eventbus.EventBus;
import static java.lang.String.format;
import java.util.List;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.clustering.jgroups.ClusterConfiguration;
import org.cmdbuild.config.api.ConfigComponent;
import static org.cmdbuild.config.api.ConfigLocation.CL_FILE_ONLY;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.NamespacedConfigService;
import static org.cmdbuild.utils.io.CmdbuildNetUtils.getHostname;
import static org.cmdbuild.utils.lang.CmRuntimeUtils.getCurrentPidOrRuntimeId;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@Component
@ConfigComponent("org.cmdbuild.cluster")
public class ClusterConfigurationImpl implements ClusterConfiguration {

	@ConfigValue(key = "enabled", description = "enable clustering features (jgroups)", defaultValue = FALSE)
	private boolean isEnabled;

	@ConfigValue(key = "name", description = "cluster name; all cmdbuild instances with the same cluster name will try to connect and form a cluster", defaultValue = "CMDBuild-Cluster")
	private String clusterName;

	@ConfigValue(key = "tcp.port", description = "default jgroups tcp port", defaultValue = "7800")
	private int tcpPort;

	@ConfigValue(key = "thisNode.tcp.port", description = "jgroups tcp port (override for this node); you usually need to change this only if you want to start a cluster with multiple nodes on the same host", defaultValue = "", location = CL_FILE_ONLY)
	private Integer nodeTcpPort;

	@ConfigValue(key = "nodes", description = "cluster nodes, example: '10.0.0.1[7800],10.0.0.2[7800]' ", defaultValue = "")
	private String clusterNodes;

	@ConfigValue(key = "thisNode.nodeId", description = "this cluster node id, default to a random generated id", defaultValue = "", location = CL_FILE_ONLY)
	private String nodeId;

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public String getClusterName() {
		return checkNotBlank(clusterName, "error: cluster name is null");
	}

	@Override
	public List<String> getClusterNodes() {
		return Splitter.on(",").trimResults().omitEmptyStrings().splitToList(nullToEmpty(clusterNodes));
	}

	@Override
	public int getTcpPort() {
		return firstNonNull(nodeTcpPort, tcpPort);
	}

	@Override
	public boolean isClusterEnabled() {
		return isEnabled;
	}

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

	@Override
	public String getClusterNodeId() {
		if (isNotBlank(nodeId)) {
			return nodeId;
		} else {
			return format("cmdbuild_%s_%s", getHostname(), getCurrentPidOrRuntimeId());
		}
	}
}
