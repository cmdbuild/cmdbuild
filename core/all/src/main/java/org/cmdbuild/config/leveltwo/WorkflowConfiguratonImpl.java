package org.cmdbuild.config.leveltwo;

import com.google.common.eventbus.EventBus;
import java.util.Set;
import org.cmdbuild.workflow.WorkflowConfiguration;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import static org.cmdbuild.config.api.ConfigValue.TRUE;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.workflow.WorkflowCommonConst.RIVER;
import static org.cmdbuild.workflow.WorkflowCommonConst.SHARK;
import org.cmdbuild.config.api.NamespacedConfigService;

@Component
@ConfigComponent("org.cmdbuild.workflow")
public final class WorkflowConfiguratonImpl implements WorkflowConfiguration {

	@ConfigValue(key = "enabled", defaultValue = FALSE)
	private boolean isEnabled;

	@ConfigValue(key = "disableSynchronizationOfMissingVariables", defaultValue = FALSE)
	private boolean disableSynchronizationOfMissingVariables;

	@ConfigValue(key = "enableAddAttachmentOnClosedActivities", defaultValue = FALSE)
	private boolean enableAddAttachmentOnClosedActivities;

	@ConfigValue(key = "endpoint", defaultValue = "http://localhost:8080/shark")
	private String endpoint;

	@ConfigValue(key = "user", defaultValue = "admin")
	private String adminUsername;

	@ConfigValue(key = "password", defaultValue = "enhydra")
	private String adminPassword;

	@ConfigValue(key = "provider", defaultValue = "shark", description = "workflow service provider; valid values include 'shark' and 'river'")
	private String workflowProvider;

	@ConfigValue(key = "shark.enabled", defaultValue = TRUE)
	private boolean sharkProviderEnabled;

	@ConfigValue(key = "river.enabled", defaultValue = TRUE)
	private boolean riverProviderEnabled;

	@ConfigValue(key = "taskParamValidation.enabled", defaultValue = FALSE, description = "enforce validation of user task parameters, on submit")
	private boolean validateUserTaskParameters;

	@ConfigValue(key = "userCanDisable", defaultValue = FALSE)
	private boolean userCanDisable;//TODO use this

	@ConfigValue(key = "hideSaveButton", defaultValue = FALSE)
	private boolean hideSaveButton;//TODO use this

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public boolean isEnabled() {
		return isEnabled && (sharkProviderEnabled || riverProviderEnabled);
	}

	@Override
	public String getServerUrl() {
		return endpoint;
	}

	@Override
	public String getUsername() {
		return adminUsername;
	}

	@Override
	public String getPassword() {
		return adminPassword;
	}

	@Override
	public boolean isUserTaskParametersValidationEnabled() {
		return validateUserTaskParameters;
	}

	@Override
	public boolean isSynchronizationOfMissingVariablesDisabled() {
		return disableSynchronizationOfMissingVariables;
	}

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

	@Override
	public String getDefaultWorkflowProvider() {
		return workflowProvider;
	}

	@Override
	public Set<String> getEnabledWorkflowProviders() {
		Set<String> set = set();//TODO preload this
		if (sharkProviderEnabled) {
			set.add(SHARK);
		}
		if (riverProviderEnabled) {
			set.add(RIVER);
		}
		return set;
	}

}
