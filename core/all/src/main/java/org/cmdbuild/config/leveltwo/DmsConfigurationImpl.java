package org.cmdbuild.config.leveltwo;

import com.google.common.eventbus.EventBus;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import org.cmdbuild.dms.cmis.CmisDmsConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.NamespacedConfigService;

@Component
@ConfigComponent("org.cmdbuild.dms")
public final class DmsConfigurationImpl implements CmisDmsConfiguration {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static final String ENABLED = "enabled";
	private static final String CATEGORY_LOOKUP = "category.lookup";

	private static final String CMIS_URL = "service.cmis.url";
	private static final String CMIS_USER = "service.cmis.user";
	private static final String CMIS_PASSWORD = "service.cmis.password";
	private static final String CMIS_PATH = "service.cmis.path";

	@ConfigValue(key = "service.type", description = "dms service (cmis, postgres); cmis is a standard protocol used, for example, by Alfresco dms; postgres is an embedded dms implementation that relies upon cmdbuild postgres db", defaultValue = "postgres")
	private String service;

	@ConfigValue(key = ENABLED, description = "", defaultValue = FALSE)
	private boolean isEnabled;

	@ConfigValue(key = CATEGORY_LOOKUP, description = "", defaultValue = "AlfrescoCategory")
	private String categoryLookup;

	@ConfigValue(key = "lookupDescriptionMode", description = "valid values: visble | optional | mandatory", defaultValue = "visble")
	private String lookupDescriptionMode;//TODO use this

	@ConfigValue(key = CMIS_URL, description = "", defaultValue = "http://localhost:10080/alfresco/api/-default-/public/cmis/versions/1.1/atom")
	private String cmisUrl;

	@ConfigValue(key = CMIS_USER, description = "", defaultValue = "admin")
	private String cmisUser;

	@ConfigValue(key = CMIS_PASSWORD, description = "", defaultValue = "admin")
	private String cmisPassword;

	@ConfigValue(key = CMIS_PATH, description = "", defaultValue = "/User Homes/cmdbuild")
	private String cmdisPath;

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public boolean isEnabled() {
		return isEnabled;
	}

	@Override
	public String getService() {
		return service;
	}

	@Override
	public String getDefaultDocumentCategoryLookup() {
		return categoryLookup;
	}

	@Override
	public String getCmisUrl() {
		return cmisUrl;
	}

	@Override
	public String getCmisUser() {
		return cmisUser;
	}

	@Override
	public String getCmisPassword() {
		return cmisPassword;
	}

	@Override
	public String getCmisPath() {
		return cmdisPath;
	}

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

}
