package org.cmdbuild.config.leveltwo;

import com.google.common.eventbus.EventBus;
import org.cmdbuild.config.GisConfiguration;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.NamespacedConfigService;

@Component
@ConfigComponent("org.cmdbuild.gis")
public final class GisConfigurationImpl implements GisConfiguration {

	@ConfigValue(key = "enabled", defaultValue = FALSE)
	private boolean isEnabled;

	@ConfigValue(key = "yahoo", defaultValue = FALSE)
	private boolean isYahooEnabled;

	@ConfigValue(key = "google", defaultValue = FALSE)
	private boolean isGoogleEnabled;

	@ConfigValue(key = "geoserver", defaultValue = FALSE)
	private boolean isGeoserverEnabled;

	@ConfigValue(key = "osm", defaultValue = FALSE)
	private boolean isOsmEnabled;

	@ConfigValue(key = "google_key", defaultValue = "")
	private String googleKey;

	@ConfigValue(key = "yahoo_key", defaultValue = "")
	private String yahooKey;

	@ConfigValue(key = "geoserver_url", defaultValue = "http://localhost:8080/geoserver")
	private String geoserverUrl;

	@ConfigValue(key = "geoserver_workspace", defaultValue = "cmdbuild")
	private String geoserverWorkspace;

	@ConfigValue(key = "geoserver_admin_user", defaultValue = "")
	private String geoserverAdminUser;

	@ConfigValue(key = "geoserver_admin_password", defaultValue = "")
	private String geoserverAdminPassword;

	@ConfigValue(key = "initialLongitude", defaultValue = "0")
	private int initialLongitude;//TODO use this

	@ConfigValue(key = "initialLatitude", defaultValue = "0")
	private int initialLatitude;//TODO use this

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

	@Override
	public boolean isEnabled() {
		return isEnabled;
	}

	@Override
	public boolean isGeoServerEnabled() {
		return isGeoserverEnabled;
	}

	@Override
	public boolean isYahooEnabled() {
		return isYahooEnabled;
	}

	@Override
	public boolean isGoogleEnabled() {
		return isGoogleEnabled;
	}

	@Override
	public boolean isOsmEnabled() {
		return isOsmEnabled;
	}

	@Override
	public String getGoogleKey() {
		return googleKey;
	}

	@Override
	public String getYahooKey() {
		return yahooKey;
	}

	@Override
	public String getGeoServerUrl() {
		return geoserverUrl.replaceFirst("[/]$", "");
	}

	@Override
	public String getGeoServerWorkspace() {
		return geoserverWorkspace;
	}

	@Override
	public String getGeoServerAdminUser() {
		return geoserverAdminUser;
	}

	@Override
	public String getGeoServerAdminPassword() {
		return geoserverAdminPassword;
	}

}
