package org.cmdbuild.custompage;

import java.util.List;

public interface CustomPageService {

	List<CustomPageInfo> getAll();

	List<CustomPageInfo> getForCurrentUser();

	CustomPageInfo get(long id);

	CustomPageInfo create(byte[] data);

	CustomPageInfo createOrUpdate(byte[] data);

	CustomPageInfo update(long id, byte[] data);

	CustomPageInfo update(CustomPageInfo customPage);

	byte[] getCustomPageFile(String code, String path);

	void delete(long id);

	CustomPageInfo getByName(String code);

}
