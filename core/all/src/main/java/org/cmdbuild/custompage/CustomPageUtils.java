/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.custompage;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import com.google.common.collect.Ordering;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.Result;
import com.google.javascript.jscomp.SourceFile;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.SeekableInMemoryByteChannel;
import org.apache.commons.io.FilenameUtils;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.commons.lang3.tuple.Pair;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.toByteArray;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class CustomPageUtils {

	public static CustomPageData parseCustomPageData(byte[] data) {
		return parseCustomPageBytes(data).toCustomPageData();
	}

	public static CustomPageParsedData parseCustomPageBytes(byte[] data) {
		return new ZipFileProcessor(data);
	}

	public static byte[] uglifyJs(byte[] data) {
		CompilerOptions options = new CompilerOptions();
		com.google.javascript.jscomp.Compiler compiler = new com.google.javascript.jscomp.Compiler();
		SourceFile sourceFile = SourceFile.fromCode("file.js", new String(data));
		Result result = compiler.compile(emptyList(), singletonList(sourceFile), options);
		checkArgument(result.success, "error compressing javascript");
		return compiler.toSource().getBytes();
	}

	public static interface CustomPageParsedData {

		CustomPageData toCustomPageData();

		CustomPageInfoImpl.CustomPageInfoImplBuilder toCustomPageInfo();
	}

	private static class ZipFileProcessor implements CustomPageParsedData {

		private final byte[] inputFile;
		private ZipFile zipFile;
		private String cpName, alias, mainExtClass, dirPrefix;
		private byte[] data;
		private boolean foundMainFile = false;

		public ZipFileProcessor(byte[] inputFile) {
			this.inputFile = checkNotNull(inputFile);
			processFile();
		}

		private void processFile() {
			try {
				zipFile = new ZipFile(new SeekableInMemoryByteChannel(inputFile));//TODO zip file processing
				List<Pair<String, byte[]>> files = list();
				Collections.list(zipFile.getEntries()).stream().filter(not(ZipArchiveEntry::isDirectory)).forEach((entry) -> {
					try {
						byte[] file = toByteArray(zipFile.getInputStream(entry));
						String normalizedFile = normalizeFile(file);
						if (normalizedFile.matches(".*mixins:\\[[^\\]]*['\"]CMDBuildUI.mixins.CustomPage['\"].*")) {
							checkArgument(!foundMainFile, "duplicate main file found in custom page data");
							foundMainFile = true;
							{
								Matcher matcher = Pattern.compile("Ext.define[(]['\"](CMDBuildUI\\.view\\.custompages\\.([^.]+)\\.[^.'\"]+)['\"]").matcher(normalizedFile);
								checkArgument(matcher.find(), "unable to find component id tag");
								mainExtClass = checkNotBlank(matcher.group(1));
								cpName = checkNotBlank(matcher.group(2));
							}
							{
								Matcher matcher = Pattern.compile("alias:['\"]([^'\"]+)['\"]").matcher(normalizedFile);
								checkArgument(matcher.find(), "unable to find alias tag");
								alias = checkNotBlank(matcher.group(1));
							}
							dirPrefix = FilenameUtils.getPath(entry.getName());
						}
						files.add(Pair.of(entry.getName(), file));
					} catch (Exception ex) {
						throw new CustomPageException(ex, "error processing custom page file = %s", entry.getName());
					}
				});
				checkArgument(foundMainFile, "main file not found in custom page data");

				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				try (ZipOutputStream zip = new ZipOutputStream(byteArrayOutputStream)) {
					files.stream().sorted(Ordering.natural().onResultOf(Pair::getLeft)).forEach((p) -> {
						try {
							String name = p.getLeft();
							byte[] file = p.getRight();
							if (isNotBlank(dirPrefix)) {
								checkArgument(name.startsWith(dirPrefix));
								name = name.replaceFirst(Pattern.quote(dirPrefix), "");
							}
							ZipEntry entry = new ZipEntry(name);
							zip.putNextEntry(entry);
							zip.write(file);
							zip.closeEntry();
						} catch (IOException ex) {
							throw runtime(ex);
						}
					});
				}

				data = byteArrayOutputStream.toByteArray();
			} catch (IOException ex) {
				throw new CustomPageException(ex, "error processing custom page data");
			}
		}

		@Override
		public CustomPageData toCustomPageData() {
			return CustomPageDataImpl.builder()
					.withName(cpName)
					.withDescription(cpName)
					.withData(data)
					.build();
		}

		@Override
		public CustomPageInfoImpl.CustomPageInfoImplBuilder toCustomPageInfo() {
			return CustomPageInfoImpl.builder()
					.withName(cpName)
					.withDescription(cpName)
					.withExtjsAlias(alias)
					.withExtjsComponentId(mainExtClass);
		}

		private String normalizeFile(byte[] file) {
			return new String(file).replaceAll("[\n\r\t ]", "");
		}

	}
}
