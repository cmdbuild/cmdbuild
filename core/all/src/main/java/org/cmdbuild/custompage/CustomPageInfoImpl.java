/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.custompage;

import static com.google.common.base.Strings.nullToEmpty;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class CustomPageInfoImpl implements CustomPageInfo {

	private final long id;
	private final String name, description, extjsComponentId, extjsAlias;

	private CustomPageInfoImpl(CustomPageInfoImplBuilder builder) {
		this.id = builder.id;
		this.name = checkNotBlank(builder.name);
		this.description = nullToEmpty(builder.description);
		this.extjsComponentId = checkNotBlank(builder.extjsComponentId);
		this.extjsAlias = checkNotBlank(builder.extjsAlias);
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getExtjsComponentId() {
		return extjsComponentId;
	}

	@Override
	public String getExtjsAlias() {
		return extjsAlias;
	}

	@Override
	public String toString() {
		return "CustomPageInfoImpl{" + "id=" + id + ", name=" + name + '}';
	}

	public static CustomPageInfoImplBuilder builder() {
		return new CustomPageInfoImplBuilder();
	}

	public static CustomPageInfoImplBuilder copyOf(CustomPageInfo source) {
		return new CustomPageInfoImplBuilder()
				.withId(source.getId())
				.withName(source.getName())
				.withDescription(source.getDescription())
				.withExtjsComponentId(source.getExtjsComponentId())
				.withExtjsAlias(source.getExtjsAlias());
	}

	public static class CustomPageInfoImplBuilder implements Builder<CustomPageInfoImpl, CustomPageInfoImplBuilder> {

		private long id;
		private String name;
		private String description;
		private String extjsComponentId;
		private String extjsAlias;

		public CustomPageInfoImplBuilder withId(long id) {
			this.id = id;
			return this;
		}

		public CustomPageInfoImplBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public CustomPageInfoImplBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public CustomPageInfoImplBuilder withExtjsComponentId(String extjsComponentId) {
			this.extjsComponentId = extjsComponentId;
			return this;
		}

		public CustomPageInfoImplBuilder withExtjsAlias(String extjsAlias) {
			this.extjsAlias = extjsAlias;
			return this;
		}

		@Override
		public CustomPageInfoImpl build() {
			return new CustomPageInfoImpl(this);
		}

	}
}
