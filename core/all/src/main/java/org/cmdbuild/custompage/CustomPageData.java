package org.cmdbuild.custompage;

import javax.annotation.Nullable;

public interface CustomPageData {

	static final String CLASSNAME = "_CustomPage";

	@Nullable
	Long getId();

	String getName();

	String getDescription();

	byte[] getData();
}
