/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.custompage;

import com.google.common.eventbus.EventBus;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.TRUE;
import org.cmdbuild.config.api.NamespacedConfigService;
import org.springframework.stereotype.Component;

@Component
@ConfigComponent("org.cmdbuild.custompage")
public class CustomPageConfigurationImpl implements CustomPageConfiguration {

	@ConfigValue(key = "js.compression.enabled", description = "enable compression of js files", defaultValue = TRUE)
	private boolean jsCompressionEnabled;

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public boolean isJsCompressionEnabled() {
		return jsCompressionEnabled;
	}

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

}
