package org.cmdbuild.custompage;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.onlyElement;
import com.google.common.eventbus.Subscribe;
import java.util.List;
import static java.util.stream.Collectors.toList;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.SeekableInMemoryByteChannel;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.authorization.CustomPageAsPrivilegeAdapter;
import org.cmdbuild.cache.CacheService;
import static org.cmdbuild.cache.CacheUtils.key;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.config.api.ConfigReloadEvent;
import static org.cmdbuild.custompage.CustomPageUtils.parseCustomPageBytes;
import static org.cmdbuild.custompage.CustomPageUtils.uglifyJs;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.toByteArray;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CustomPageServiceImpl implements CustomPageService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final CustomPageRepository repository;
	private final OperationUserSupplier userStore;
	private final Holder<List<CustomPageInfo>> allInfo;
	private final CmdbCache<String, CustomPageInfo> infoById, infoByName;
	private final CmdbCache<String, byte[]> processedFileByIdPath;
	private final CustomPageConfiguration config;

	public CustomPageServiceImpl(CustomPageRepository repository, OperationUserSupplier userStore, CacheService cacheService, CustomPageConfiguration config) {
		this.repository = checkNotNull(repository);
		this.userStore = checkNotNull(userStore);
		this.config = checkNotNull(config);
		allInfo = cacheService.newHolder("custom_page_all_info");
		infoById = cacheService.newCache("custom_page_info_by_id");
		infoByName = cacheService.newCache("custom_page_info_by_name");
		processedFileByIdPath = cacheService.newCache("custom_page_file_by_id_path");
		config.getEventBus().register(new Object() {

			@Subscribe
			public void handleConfigReloadEvent(ConfigReloadEvent event) {
				processedFileByIdPath.invalidateAll();
			}
		});
	}

	private void invalidateCache() {
		allInfo.invalidate();
		infoById.invalidateAll();
		infoByName.invalidateAll();
		processedFileByIdPath.invalidateAll();
	}

	@Override
	public List<CustomPageInfo> getAll() {
		return allInfo.get(this::doGetAll);
	}

	private List<CustomPageInfo> doGetAll() {
		return repository.getAll().stream().map(this::toCustomPageInfo).collect(toList());
	}

	@Override
	public List<CustomPageInfo> getForCurrentUser() {
		return getAll().stream().filter(this::isAccessible).collect(toList());
	}

	@Override
	public CustomPageInfo get(long id) {
		CustomPageInfo customPage = infoById.get(String.valueOf(id), () -> doReadOne(id));
		checkArgument(isAccessible(customPage), "unable to access custom page = %s: permission denied", id);
		return customPage;
	}

	@Override
	public CustomPageInfo getByName(String code) {
		CustomPageInfo customPage = infoByName.get(code, () -> doReadOne(code));
		checkArgument(isAccessible(customPage), "unable to access custom page = %s: permission denied", code);
		return customPage;
	}

	@Override
	public CustomPageInfo create(byte[] data) {
		CustomPageData customPage = parseCustomPageBytes(data).toCustomPageData();
		checkArgument(repository.getByNameOrNull(customPage.getName()) == null, "cannot create custom page with name = '%s', a custom page with this name already exists", customPage.getName());
		return create(customPage);
	}

	@Override
	public CustomPageInfo createOrUpdate(byte[] data) {
		CustomPageData newData = parseCustomPageBytes(data).toCustomPageData();
		CustomPageData current = repository.getByNameOrNull(newData.getName());
		if (current == null) {
			return create(newData);
		} else {
			newData = CustomPageDataImpl.copyOf(newData)
					.withId(current.getId())
					.withDescription(current.getDescription())
					.build();
			return update(newData);
		}
	}

	@Override
	public CustomPageInfo update(long id, byte[] data) {
		CustomPageInfo current = get(id);
		CustomPageData newData = parseCustomPageBytes(data).toCustomPageData();
		checkArgument(equal(newData.getName(), current.getName()), "uploaded custom page name = '%s' does not match name = '%s' of this custom page with id = %s", newData.getName(), current.getName(), id);
		newData = CustomPageDataImpl.copyOf(newData)
				.withId(current.getId())
				.withDescription(current.getDescription())
				.build();
		return update(newData);
	}

	@Override
	public CustomPageInfo update(CustomPageInfo customPage) {
		CustomPageData data = repository.getById(customPage.getId());
		data = CustomPageDataImpl.copyOf(data)
				.withDescription(customPage.getDescription())//description is the only mutable attr
				.build();
		return update(data);
	}

	@Override
	public void delete(long id) {
		repository.delete(id);
		invalidateCache();
	}

	@Override
	public byte[] getCustomPageFile(String code, String path) {
		CustomPageData customPage = repository.getByName(code);
		checkArgument(isAccessible(customPage), "unable to access custom page = %s: permission denied", code);
		return processedFileByIdPath.get(key(String.valueOf(customPage.getId()), path), () -> doGetCustomPageFile(customPage, path));
	}

	private CustomPageInfo update(CustomPageData data) {
		logger.info("update custom page = {}", data);
		repository.update(data);
		invalidateCache();
		return get(data.getId());
	}

	private CustomPageInfo doReadOne(long id) {
		try {
			return getAll().stream().filter((i) -> i.getId() == id).collect(onlyElement());
		} catch (Exception ex) {
			throw new CustomPageException(ex, "custom page not found for id = %s", id);
		}
	}

	private boolean isAccessible(CustomPageInfo value) {
		return isAccessible(new CustomPageAsPrivilegeAdapter(value));
	}

	private boolean isAccessible(CustomPageData value) {
		return isAccessible(new CustomPageAsPrivilegeAdapter(value));
	}

	private boolean isAccessible(CustomPageAsPrivilegeAdapter value) {
		return userStore.getUser().getPrivilegeContext().hasReadAccess(value);
	}

	private CustomPageInfo create(CustomPageData customPage) {
		logger.info("create custom page = {}", customPage);
		customPage = repository.create(customPage);
		invalidateCache();
		return get(customPage.getId());
	}

	private CustomPageInfo doReadOne(String code) {
		checkNotBlank(code);
		try {
			return getAll().stream().filter((i) -> i.getName().equals(code)).collect(onlyElement());
		} catch (Exception ex) {
			throw new CustomPageException(ex, "custom page not found for name = %s", code);
		}
	}

	private byte[] doGetCustomPageFile(CustomPageData customPage, String path) {
		try {
			ZipFile zipFile = new ZipFile(new SeekableInMemoryByteChannel(customPage.getData()));
			ZipArchiveEntry entry = checkNotNull(zipFile.getEntry(path), "entry not found for path = %s");
			byte[] data = toByteArray(zipFile.getInputStream(entry));
			if (path.endsWith(".js") && config.isJsCompressionEnabled()) {
				try {
					data = uglifyJs(data);
				} catch (Exception ex) {
					logger.warn("error minfying custom page = {} file = {}", customPage, path, ex);
				}
			}
			return data;
		} catch (Exception ex) {
			throw new CustomPageException(ex, "error processing custom page data for page = %s", customPage);
		}
	}

	private CustomPageInfo toCustomPageInfo(CustomPageData data) {
		return parseCustomPageBytes(data.getData()).toCustomPageInfo()
				.withId(data.getId())
				.withDescription(data.getDescription())
				.build();
	}

}
