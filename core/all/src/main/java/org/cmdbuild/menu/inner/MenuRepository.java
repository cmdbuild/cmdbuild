/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.menu.inner;

import java.util.List;
import javax.annotation.Nullable;
import org.cmdbuild.menu.MenuInfo;

public interface MenuRepository {

	@Nullable
	MenuData getMenuDataForGroupOrNull(String groupName);

	List<MenuInfo> getAllMenuInfos();

	MenuData getMenuDataById(long menuId);

	MenuData updateMenuData(MenuData menuData);

	MenuData createMenuData(MenuData menuData);

	void delete(long id);
}
