/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.startup;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import static java.lang.String.format;
import java.util.concurrent.CompletableFuture;
import static java.util.stream.Collectors.joining;
import org.cmdbuild.dao.ConfigurableDataSource;
import org.cmdbuild.dao.ConfigurableDataSource.DatasourceConfiguredEvent;
import org.cmdbuild.dao.config.DatabaseConfiguration;
import org.cmdbuild.dao.config.inner.PatchManager;
import org.cmdbuild.dao.config.inner.PatchManager.AllPatchAppliedAndDatabaseReadyEvent;
import org.cmdbuild.debuginfo.BuildInfoService;
import org.cmdbuild.requestcontext.RequestContextService;
import org.cmdbuild.services.SystemService;
import static org.cmdbuild.services.SystemServiceStatus.SS_ENABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_ERROR;
import static org.cmdbuild.services.SystemServiceStatusUtils.serializeSystemServiceStatus;
import org.cmdbuild.services.SystemServicesService;
import org.cmdbuild.system.SystemEventService;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.listOf;

/**
 *
 * this spring service run all required checks at startup. Cmdbuild startup
 * works like this:<ul>
 * <li>spring application context startup (all spring beans should NOT access
 * db, start stuff or do anything heavy in their constructor and PostConstruct
 * methods; nothing important should happen before the whole spring app context
 * is ready)</li>
 * <li>once spring is ready, we'll have a boot rest ws responding (minimal ws
 * set that may be used to handle configuration, patch, monitor startup process
 * etc); all other ws should be blocked</li>
 * <li>this bean will run boot check (check db, check config, check patch,
 * etc)</li>
 * <li>if the system requires user input (for example, the database is not
 * configured, or there are patches to run) it will hold the spring
 * initialization process while waiting for user input (via boot services)</li>
 * <li>after all check pass (with or without user intervention) the service will
 * go on starting other services (cluster, scheduler, etc); note: config load
 * from db will be triggered before, once patch manager completes its
 * operation</li>
 * </ul>
 *
 * @author davide
 */
@Component
public class BootServiceImpl implements BootService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ConfigurableDataSource dataSource;
	private final DatabaseConfiguration databaseConfiguration;
	private final PatchManager patchManager;
	private final RequestContextService requestContextService;
	private final EventBus eventBus;
	private final BuildInfoService buildInfoService;
	private final SystemServicesService servicesStatusService;

	private SystemStatus bootStatus = SystemStatus.STARTING_APP_CONTEXT;

	public BootServiceImpl(SystemServicesService servicesStatusService, BuildInfoService buildInfoService, DatabaseConfiguration databaseConfiguration, ConfigurableDataSource dataSource, PatchManager patchManager, RequestContextService requestContextService, SystemEventService systemEventService) {
		this.dataSource = checkNotNull(dataSource);
		this.patchManager = checkNotNull(patchManager);
		this.requestContextService = checkNotNull(requestContextService);
		this.databaseConfiguration = checkNotNull(databaseConfiguration);
		this.buildInfoService = checkNotNull(buildInfoService);
		this.servicesStatusService = checkNotNull(servicesStatusService);
		eventBus = systemEventService.getEventBus();
	}

	@EventListener
	public void handleContextRefresh(ContextRefreshedEvent event) {
		logger.info("spring context is ready");
		new Thread(() -> {
			requestContextService.initCurrentRequestContext("system startup");
			//TODO set user
			startSystem();
		}).start();
	}

	private void setBootStatus(SystemStatus systemStatus) {
		this.bootStatus = checkNotNull(systemStatus);
		switch (bootStatus) {
			case READY:
				//TODO dynamic version
				//			
				//   ___ __  __ ___  ___      _ _    _ 
				//  / __|  \/  |   \| _ )_  _(_) |__| |
				// | (__| |\/| | |) | _ \ || | | / _` |
				//  \___|_|  |_|___/|___/\_,_|_|_\__,_|
				//                
				String asciiArtBanner = "\t   ___ __  __ ___  ___      _ _    _ \n\t  / __|  \\/  |   \\| _ )_  _(_) |__| |\n\t | (__| |\\/| | |) | _ \\ || | | / _` |\n\t  \\___|_|  |_|___/|___/\\_,_|_|_\\__,_|\n\t                     v3.0 READY";
				String servicesStatus = buildServicesStatusInfoMessage();
				logger.info("\n\n\n\n{}\n\n\n\n{}\n\n\n", asciiArtBanner, servicesStatus);
				logger.info("source code version info: {}", buildInfoService.hasBuildInfo() ? buildInfoService.getCommitInfo() : "<build info not available>");
				break;
			default:
				logger.info("\n\n\n\n\tsystem is {}\n\n\n", bootStatus);
		}
		eventBus.post(getSystemStatus().buildSystemStatusChangedEvent());
	}

	private String buildServicesStatusInfoMessage() {
		return servicesStatusService.getSystemServices().stream().sorted(Ordering.natural().onResultOf(SystemService::getServiceName))
				.map(s -> format("\t%-24s\t%s   %s", s.getServiceName(), map(SS_READY, "*", SS_ENABLED, "*", SS_ERROR, "E").getOrDefault(s.getServiceStatus(), " "), serializeSystemServiceStatus(s.getServiceStatus()))).collect(joining("\n"));
	}

	@Override
	public SystemStatus getSystemStatus() {
		return bootStatus;
	}

	private void startSystem() {
		try {
			logger.info("startSystem BEGIN");
			setBootStatus(SystemStatus.STARTING_SYSTEM);//trigger AppContextReadyEvent

			setBootStatus(SystemStatus.CHECKING_DATABASE);

			//check database
			if (!dataSource.isConfigured()) {
				setBootStatus(SystemStatus.WAITING_FOR_USER);
				logger.info("database is not configured, waiting for user input");
				CompletableFuture future = new CompletableFuture();
				dataSource.getEventBus().register(new Object() {
					@Subscribe
					public void handleDatasourceConfiguredEvent(DatasourceConfiguredEvent event) {
						future.complete(null);
						dataSource.getEventBus().unregister(this);
					}
				});
				future.get();
				setBootStatus(SystemStatus.CHECKING_DATABASE);
			} else {
				logger.info("database is ready");
			}

			// check patch manager
			logger.info("check patch manager");
			if (patchManager.getAllPatchesOnDb().isEmpty()) {
				patchManager.reloadPatches();
			}

			if (databaseConfiguration.enableAutoPatch() && patchManager.hasPendingPatched()) {
				try {
					patchManager.applyPendingPatches();
				} catch (Exception ex) {
					logger.error("error during auto-patching", ex);
				}
			}

			if (patchManager.hasPendingPatched()) {
				setBootStatus(SystemStatus.WAITING_FOR_USER);
				logger.info("patch manager is not ready, waiting for user input");
				CompletableFuture future = new CompletableFuture();
				patchManager.getEventBus().register(new Object() {
					@Subscribe
					public void handleAllPatchAppliedAndDatabaseReadyEvent(AllPatchAppliedAndDatabaseReadyEvent event) {
						future.complete(null);
						patchManager.getEventBus().unregister(this);
					}
				});
				future.get();
				setBootStatus(SystemStatus.CHECKING_DATABASE);
			} else {
				logger.info("patch manager is ready, last applied patch is = {}", patchManager.getLastPatchOnDbKeyOrNull());
			}

			setBootStatus(SystemStatus.STARTING_SERVICES); // STARTED event will trigger startup of many annotate services (scheduler etc), which will happen between STARTED and READY events

			setBootStatus(SystemStatus.READY);
			logger.info("startSystem END");

		} catch (Exception ex) {
			logger.error("system startup error", ex);
			setBootStatus(SystemStatus.ERROR);
		}
	}

}
