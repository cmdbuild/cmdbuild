/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.easyupload;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Nullable;
import javax.mail.util.ByteArrayDataSource;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import org.apache.http.entity.ContentType;
import org.apache.tika.Tika;
import static org.cmdbuild.easyupload.EasyuploadUtils.normalizePath;
import static org.cmdbuild.utils.hash.CmdbuildHashUtils.hash;
import org.cmdbuild.utils.io.CmdbuildFileUtils;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempFile;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.toByteArray;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EasyuploadServiceImpl implements EasyuploadService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EasyuploadRepository repository;
	private final Tika tika = new Tika();

	public EasyuploadServiceImpl(EasyuploadRepository repository) {
		this.repository = checkNotNull(repository);
	}

	@Override
	public EasyuploadItem create(String path, byte[] data) {
		EasyuploadItem item = buildItem(path, data).build();
		logger.info("create easyupload item = {}", item);
		repository.create(item);
		return item;
	}

	@Override
	public EasyuploadItem create(DataHandler dataHandler) {
		String path = normalizePath("other", randomId(), dataHandler.getName());
		return create(path, toByteArray(dataHandler));
	}

	@Override
	public EasyuploadItem create(String dir, DataHandler dataHandler) {
		checkNotBlank(dir);
		if (dir.matches(".*[^/][.][^/]+$")) {//has extension, so is file
			dir = normalizePath(dir, dataHandler.getName());
		} else {
			dir = normalizePath(dir);
		}
		return create(dir, toByteArray(dataHandler));
	}

	@Override
	public EasyuploadItem update(long fileId, byte[] data) {
		EasyuploadItem item = getById(fileId);
		item = buildItem(item.getPath(), data)
				.withId(fileId)
				.build();
		return repository.update(item);
	}

	private EasyuploadItemImpl.EasyuploadItemImplBuilder buildItem(String path, byte[] data) {
		path = normalizePath(path);
		String mimeType = tika.detect(data);
		return EasyuploadItemImpl.builder()
				.withContent(data)
				.withFileName(new File(path).getName())
				.withHash(hash(data))
				.withMimeType(mimeType)
				.withPath(path)
				.withSize(data.length);
	}

	@Override
	public void delete(long fileId) {
		repository.delete(fileId);
	}

	@Override
	@Nullable
	public EasyuploadItem getOrNull(String path) {
		return repository.getByPathOrNull(path);
	}

	@Override
	public EasyuploadItem get(String path) {
		return repository.getByPath(path);
	}

	@Override
	public EasyuploadItem getById(long fileId) {
		return repository.getItemById(fileId);
	}

	@Override
	public List<EasyuploadItemInfo> getAll() {
		return repository.getAllInfo();
	}

	@Override
	public List<EasyuploadItemInfo> getByDir(String dir) {
		return repository.getInfoByDir(dir);
	}

	@Override
	public DataSource getUploadsAsZipFile(String dir) {
		return toZip(repository.getAllByDir(dir));
	}

	@Override
	public DataSource getAllUploadsAsZipFile() {
		return toZip(repository.getAll());
	}

	private DataSource toZip(List<EasyuploadItem> items) {
		File zip = tempFile();

		try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip))) {
			for (EasyuploadItem item : items) {
				ZipEntry zipEntry = new ZipEntry(item.getPath());
				out.putNextEntry(zipEntry);
				out.write(item.getContent());
				out.closeEntry();
			}
		} catch (IOException ex) {
			throw runtime(ex);
		}

		byte[] zipData = CmdbuildFileUtils.toByteArray(zip);
		deleteQuietly(zip);

		return new ByteArrayDataSource(zipData, ContentType.APPLICATION_OCTET_STREAM.getMimeType()) {
			{
				setName("upload.zip");
			}
		};
	}

	@Override
	public void uploadZip(byte[] toByteArray) {
		try (ZipInputStream in = new ZipInputStream(new ByteArrayInputStream(toByteArray))) {
			ZipEntry entry;
			while ((entry = in.getNextEntry()) != null) {
				String path = normalizePath(entry.getName());
				byte[] data = toByteArray(in);
				in.closeEntry();
				EasyuploadItem item = repository.getByPathOrNull(path);
				if (item == null) {
					create(path, data);
				} else {
					update(item.getId(), data);
				}
			}
		} catch (IOException ex) {
			throw runtime(ex);
		}
	}

}
