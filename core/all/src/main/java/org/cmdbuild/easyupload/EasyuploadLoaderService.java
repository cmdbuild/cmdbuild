/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.easyupload;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import static java.util.Arrays.asList;
import java.util.Collection;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import static org.cmdbuild.easyupload.EasyuploadUtils.normalizePath;
import org.cmdbuild.scheduler.spring.ScheduledJob;
import org.cmdbuild.startup.PostStartup;
import static org.cmdbuild.utils.hash.CmdbuildHashUtils.hash;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.toByteArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.DirectoryService;

@Component
public class EasyuploadLoaderService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DirectoryService directoryService;
	private final EasyuploadService easyuploadService;

	public EasyuploadLoaderService(DirectoryService directoryService, EasyuploadService easyuploadService) {
		this.directoryService = checkNotNull(directoryService);
		this.easyuploadService = checkNotNull(easyuploadService);
	}

	@ScheduledJob("*/10 * * * * ?")//run every 10 seconds //TODO: run on every node in cluster
	@PostStartup
	public synchronized void checkUploadsFolderAndLoadContent() {
		if (directoryService.hasFilesystem()) {
			logger.debug("checkUploadsFolderAndLoadContent");
			asList(new File(directoryService.getConfigDirectory(), "uploads"),
					new File(directoryService.getConfigDirectory(), "upload"),
					new File(directoryService.getWebappRootDirectory(), "uploads"),
					new File(directoryService.getWebappRootDirectory(), "upload"))
					.forEach((dir) -> {
						if (dir.exists() && dir.isDirectory() && dir.canRead()) {
							scanDir(dir);
						}
					});
		}
	}

	private void scanDir(File dir) {
		logger.debug("scan dir = {}", dir);
		Collection<File> files = FileUtils.listFiles(dir, FileFilterUtils.fileFileFilter(), FileFilterUtils.directoryFileFilter());
		if (!files.isEmpty()) {
			logger.info("processing {} files from upload dir = {}", files.size(), dir);
			files.stream().forEach((file) -> loadFileAndDelete(dir, file));
		}

//		logger.debug("cleanup of leftover subdirs in upload dir = {}", dir);
//		FileUtils.listFilesAndDirs(dir, FileFilterUtils.directoryFileFilter(), FileFilterUtils.directoryFileFilter()).stream()
//				.filter(not(equalTo(dir)))
//				.forEach((d) -> FileUtils.deleteQuietly(d));
	}

	private void loadFileAndDelete(File dir, File file) {
		logger.info("load file = {}", file.getAbsolutePath());
		String path = normalizePath(dir.toPath().relativize(file.toPath()).toString());
		EasyuploadItem item = easyuploadService.getOrNull(path);//TODO get only info, not content
		byte[] data = toByteArray(file);
		String hash = hash(data);
		if (item != null) {
			if (equal(hash, item.getHash())) {
				logger.info("skip file {}, already present in db with same content", file.getAbsolutePath());
			} else {
				easyuploadService.update(item.getId(), data);
			}
		} else {
			easyuploadService.create(path, data);
		}
		FileUtils.deleteQuietly(file);
	}

}
