/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.easyupload;

import java.util.List;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Nullable;

public interface EasyuploadService {

	static final String EASYUPLOAD_CLASS_ICONS = "class_icons",
			EASYUPLOAD_GIS_ATTR_ICONS = "gis_attribute_icons";

	EasyuploadItem create(DataHandler dataHandler);

	EasyuploadItem create(String dir, DataHandler dataHandler);

	EasyuploadItem create(String path, byte[] data);

	EasyuploadItem update(long fileId, byte[] data);

	List<EasyuploadItemInfo> getByDir(String dir);

	List<EasyuploadItemInfo> getAll();

	void delete(long fileId);

	@Nullable
	EasyuploadItem getOrNull(String path);

	EasyuploadItem get(String path);

	EasyuploadItem getById(long fileId);

	DataSource getAllUploadsAsZipFile();

	DataSource getUploadsAsZipFile(String dir);

	void uploadZip(byte[] toByteArray);

}
