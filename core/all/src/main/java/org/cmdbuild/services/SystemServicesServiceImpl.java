/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.services;

import com.google.common.collect.ImmutableList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class SystemServicesServiceImpl implements SystemServicesService {

	private final List<SystemService> systemServices;

	public SystemServicesServiceImpl(List<SystemService> systemServices) {
		this.systemServices = ImmutableList.copyOf(systemServices);
	}

	@Override
	public List<SystemService> getSystemServices() {
		return systemServices;
	}

}
