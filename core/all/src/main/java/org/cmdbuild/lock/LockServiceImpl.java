package org.cmdbuild.lock;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.time.temporal.ChronoUnit.SECONDS;
import java.util.List;
import javax.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.lock.ItemLockImpl.copy;
import org.cmdbuild.auth.session.SessionService;
import org.cmdbuild.auth.session.model.Session;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.config.CoreConfiguration;
import static org.cmdbuild.utils.date.DateUtils.now;
import static org.cmdbuild.utils.hash.CmdbuildHashUtils.hashIfLongerThan;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;

@Component
public class LockServiceImpl implements LockService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LockRepository lockStore;
	private final SessionService sessionService;
	private final CoreConfiguration configuration;

	public LockServiceImpl(LockRepository lockStore, SessionService sessionService, CoreConfiguration configuration) {
		this.lockStore = checkNotNull(lockStore);
		this.sessionService = checkNotNull(sessionService);
		this.configuration = checkNotNull(configuration);
	}

	@Override
	public LockResponse aquireLock(String itemId, LockScope lockScope) {
		logger.debug("aquire lock for item id = {}", itemId);
		itemId = checkItemIdAndShrinkIfNecessary(itemId);
		ItemLock lock = getLockOrNull(itemId);
		Session session = sessionService.getCurrentSession();
		switch (lockScope) {
			case SESSION:
				if (lock != null && isLockedByOther(session, lock)) {
					return new UnsuccessfullLockResponse();
				}
				break;
			case REQUEST:
				if (lock != null) {
					return new UnsuccessfullLockResponse();
				}
				break;
			default:
				throw unsupported("unsupported lock scope = %s", lockScope);
		}
		ItemLock itemLock = aquireLock(lock, session, itemId);
		return new SuccessfulLockResponse(itemLock);
	}

	private ItemLock aquireLock(@Nullable ItemLock lock, Session session, String itemId) {
		if (lock == null) {
			lock = ItemLockImpl.builder()
					.withItemId(itemId)
					.withSessionId(session.getSessionId())
					.build();
		} else {
			lock = copy(lock).updateLastActiveDate().build();
		}
		lockStore.storeLock(lock);
		return lock;
	}

	@Override
	public @Nullable
	ItemLock getLockOrNull(String itemId) {
		logger.trace("get lock for item id = {}", itemId);
		itemId = checkItemIdAndShrinkIfNecessary(itemId);
		ItemLock lock = lockStore.getLockByItemId(itemId);
		if (lock != null && lock.getLastActiveDate().until(now(), SECONDS) > configuration.getLockCardTimeOut()) {
			logger.debug("lock {} is expired, return null", lock);
			// we could clean up expired locks here, but it's not really necessary... it's already in cache, so performance-wise there is non a real gain
			return null;
		} else {
			logger.trace("found lock = {}", lock);
			return lock;
		}
	}

	@Override
	public void releaseLock(ItemLock lock) {
		logger.debug("release lock = {}", lock);
		ItemLock currentLock = getNotLockedByOther(lock.getItemId());
		if (currentLock != null) {
			lockStore.removeLock(currentLock);
		}
	}

	@Override
	public void deleteLock(String lockId) {
		lockStore.removeLock(getLock(lockId));
	}

	private @Nullable
	ItemLock getNotLockedByOther(String itemId) {
		return checkNotLockedByOthers(getLockOrNull(itemId));
	}

	private @Nullable
	ItemLock checkNotLockedByOthers(@Nullable ItemLock lock) {
		if (lock != null) {
			checkArgument(!isLockedByOther(sessionService.getCurrentSession(), lock), "item = %s is locked by another session", lock.getItemId());
		}
		return lock;
	}

	private boolean isLockedByOther(Session session, ItemLock lock) {
		return !equal(lock.getSessionId(), session.getSessionId());
	}

	@Override
	public void releaseAllLocks() {
		logger.info("release all locks");
		lockStore.removeAllLocks();
	}

	@Override
	public List<ItemLock> getAllLocks() {
		logger.debug("get all locks");
		return lockStore.getAllLocks();
	}

	@Override
	public void requireNotLockedByOthers(String itemId) {
		logger.trace("requireNotLockedByOthers for item id = {}", itemId);
		itemId = checkItemIdAndShrinkIfNecessary(itemId);
		getNotLockedByOther(itemId);
	}

	@Override
	public void requireLockedByCurrent(String itemId) {
		logger.trace("requireLockedByCurrent for item id = {}", itemId);
		itemId = checkItemIdAndShrinkIfNecessary(itemId);
		aquireLockOrFail(itemId);
	}

	private String checkItemIdAndShrinkIfNecessary(String itemId) {
		return hashIfLongerThan(checkNotBlank(itemId), getMaxItemIdSize());
	}

	private int getMaxItemIdSize() {
		return lockStore.getMaxItemIdSize(); //TODO if necessary, cache this for performance
	}

	private static class UnsuccessfullLockResponse implements LockResponse {

		@Override
		public boolean isAquired() {
			return false;
		}

		@Override
		public ItemLock aquired() {
			throw new IllegalStateException("lock not aquired");
		}

	}

	private static class SuccessfulLockResponse implements LockResponse {

		private final ItemLock itemLock;

		public SuccessfulLockResponse(ItemLock itemLock) {
			this.itemLock = checkNotNull(itemLock);
		}

		@Override
		public boolean isAquired() {
			return true;
		}

		@Override
		public ItemLock aquired() {
			return itemLock;
		}

	}

}
