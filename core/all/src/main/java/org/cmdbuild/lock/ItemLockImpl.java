/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.lock;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import java.time.ZonedDateTime;
import org.apache.commons.lang3.builder.Builder;
import static org.cmdbuild.utils.date.DateUtils.now;

/**
 *
 */
public class ItemLockImpl implements ItemLock {

	private final String itemId, sessionId;
	private final ZonedDateTime beginDate, lastActiveDate;

	private ItemLockImpl(String itemId, String sessionId, ZonedDateTime beginDate, ZonedDateTime lastActiveDate) {
		this.itemId = checkNotNull(emptyToNull(itemId));
		this.sessionId = checkNotNull(emptyToNull(sessionId));
		this.beginDate = checkNotNull(beginDate);
		this.lastActiveDate = checkNotNull(lastActiveDate);
	}

	@Override
	public String getItemId() {
		return itemId;
	}

	@Override
	public String getSessionId() {
		return sessionId;
	}

	@Override
	public ZonedDateTime getBeginDate() {
		return beginDate;
	}

	@Override
	public ZonedDateTime getLastActiveDate() {
		return lastActiveDate;
	}

	public static ItemLockBuilder builder() {
		return new ItemLockBuilder();
	}

	public static ItemLockBuilder copy(ItemLock lock) {
		return new ItemLockBuilder()
				.withItemId(lock.getItemId())
				.withSessionId(lock.getSessionId())
				.withBeginDate(lock.getBeginDate())
				.withLastActiveDate(lock.getLastActiveDate());
	}

	public static class ItemLockBuilder implements Builder<ItemLock> {

		private String itemId, sessionId;
		private ZonedDateTime beginDate = now(), lastActiveDate = now();

		public ItemLockBuilder withItemId(String itemId) {
			this.itemId = checkNotNull(emptyToNull(itemId));
			return this;
		}

		public ItemLockBuilder withSessionId(String sessionId) {
			this.sessionId = checkNotNull(emptyToNull(sessionId));
			return this;
		}

		public ItemLockBuilder withBeginDate(ZonedDateTime beginDate) {
			this.beginDate = checkNotNull(beginDate);
			return this;
		}

		public ItemLockBuilder withLastActiveDate(ZonedDateTime lastActiveDate) {
			this.lastActiveDate = checkNotNull(lastActiveDate);
			return this;
		}

		public ItemLockBuilder updateLastActiveDate() {
			return this.withLastActiveDate(now());
		}

		@Override
		public ItemLock build() {
			return new ItemLockImpl(itemId, sessionId, beginDate, lastActiveDate);
		}

	}
}
