/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.lock;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.dao.entrytype.attributetype.StringAttributeType;
import org.cmdbuild.scheduler.spring.ScheduledJob;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.CoreConfiguration;
import org.cmdbuild.cache.CmdbCache;
import org.springframework.jdbc.core.JdbcTemplate;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.WhereOperator.EQ;
import static org.cmdbuild.utils.date.DateUtils.toDateTime;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

/**
 *
 */
@Component
public class LockRepositoryImpl implements LockRepository {

	private final static String LOCK_CLASS_NAME = "_Lock", ITEM_ID_ATTRIBUTE = "ItemId", SESSION_ID_ATTRIBUTE = "SessionId", LAST_ACTIVE_DATE_ATTRIBUTE = "LastActiveDate";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;
	private final JdbcTemplate jdbcTemplate;
	private final CoreConfiguration configuration;

	private CmdbCache<String, ItemLockHolder> itemLocksByItemId;

	public LockRepositoryImpl(DaoService dao, JdbcTemplate jdbcTemplate, CoreConfiguration configuration, CacheService cacheService) {
		logger.info("init");
		this.dao = checkNotNull(dao);
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.configuration = checkNotNull(configuration);
		itemLocksByItemId = cacheService.newCache("item_lock_cache");
	}

	@Override
	public void storeLock(ItemLock itemLock) {
		ItemLockHolder currentHolder = itemLocksByItemId.getIfPresent(itemLock.getItemId());
		if (currentHolder == null
				|| currentHolder.isInvalid()
				|| !equal(currentHolder.getItemLock().getSessionId(), itemLock.getSessionId())
				|| Seconds.secondsBetween(currentHolder.getLastPersistedDate(), DateTime.now()).getSeconds() > configuration.getLockCardPersistDelay()) {
			logger.debug("current holder is missing or significant update to lock, persisting on db");
			doUpdateLockOnDb(itemLock);
		} else {
			logger.debug("current holder is almost up to date, skip persist on db");
		}
	}

	/**
	 * evict expired locks from db
	 *
	 * scheduled to run once every hour
	 */
	@ScheduledJob("0 10 * * * ?")
	public void cleanupLockTable() {
		logger.debug("deleting expired locks (older than {})", configuration.getLockCardTimeOut());
		// cleanup in cache is handled by cache expiration and lazy invalidation check on read
		int res = jdbcTemplate.update("DELETE FROM \"_Lock\" WHERE \"LastActiveDate\" < NOW() - interval '" + configuration.getLockCardTimeOut() + " seconds'"); //TODO use java time and not pg system time?
		logger.debug("deleteted {} expired locks (older than {})", res, configuration.getLockCardTimeOut());
	}

	@Override
	public @Nullable
	ItemLock getLockByItemId(String itemId) {
		logger.trace("get lock for item id = {}", itemId);
		ItemLockHolder holder = itemLocksByItemId.getIfPresent(itemId);
		if (holder == null) {
			logger.trace("lock not found in cache");
			Card card = findLockCardByItemId(itemId);
			if (card == null) {
				logger.trace("lock not found on db, return missingLock()");
				holder = missingLock();
			} else {
				logger.trace("found lock on db");
				holder = new ItemLockHolder(cardToLock.apply(card), DateTime.now());
			}
			itemLocksByItemId.put(itemId, holder);
		}
		ItemLock lock = holder.getItemLock();
		logger.trace("returning lock = {}", lock);
		return lock;
	}

	private final Function<Card, ItemLock> cardToLock = (card) -> ItemLockImpl.builder()
			.withBeginDate(toDateTime(card.getBeginDate()))
			.withLastActiveDate(card.get(LAST_ACTIVE_DATE_ATTRIBUTE, ZonedDateTime.class))
			.withItemId(card.get(ITEM_ID_ATTRIBUTE, String.class))
			.withSessionId(card.get(SESSION_ID_ATTRIBUTE, String.class))
			.build();

	@Override
	public List<ItemLock> getAllLocks() {
		logger.debug("get all locks");
		return dao.selectAll().from(LOCK_CLASS_NAME).getCards().stream().map(cardToLock).collect(toList());
	}

	@Override
	public void removeAllLocks() {
		logger.debug("remove all locks");
		jdbcTemplate.update("DELETE FROM \"_Lock\"");
		itemLocksByItemId.invalidateAll();
	}

	@Override
	public void removeLock(ItemLock lock) {
		logger.debug("remove lock = {}", lock);
		jdbcTemplate.update("DELETE FROM \"_Lock\" WHERE \"ItemId\" = ?", lock.getItemId());
		itemLocksByItemId.invalidate(lock.getItemId());
	}

	private void doUpdateLockOnDb(ItemLock lock) {
		logger.debug("doUpdateLockOnDb, lock = {}", lock);
		Map attrsToSet = map(LAST_ACTIVE_DATE_ATTRIBUTE, lock.getLastActiveDate(), SESSION_ID_ATTRIBUTE, lock.getSessionId());
		Card card = findLockCardByItemId(lock.getItemId());
		if (card == null) {
			dao.createOnly(CardImpl.buildCard(dao.getClasse(LOCK_CLASS_NAME), map(attrsToSet).with(ITEM_ID_ATTRIBUTE, lock.getItemId())));
		} else {
			dao.updateOnly(CardImpl.copyOf(card).withAttributes(attrsToSet).build());
		}
		itemLocksByItemId.put(lock.getItemId(), new ItemLockHolder(lock, DateTime.now()));
		logger.debug("updated lock on db, lock = {}", lock);
	}

	private @Nullable
	Card findLockCardByItemId(String itemId) {
		return dao.selectAll().from(LOCK_CLASS_NAME).where(ITEM_ID_ATTRIBUTE, EQ, itemId).getCardOrNull();
	}

	@Override
	public int getMaxItemIdSize() {
		return ((StringAttributeType) dao.getClasse(LOCK_CLASS_NAME).getAttribute(ITEM_ID_ATTRIBUTE).getType()).getLength();
	}

	private class ItemLockHolder {

		private final ItemLock itemLock;
		private final DateTime lastPersistedDate;

		public ItemLockHolder(ItemLock itemLock, DateTime lastPersistedDate) {
			this.itemLock = checkNotNull(itemLock);
			this.lastPersistedDate = checkNotNull(lastPersistedDate);
		}

		private ItemLockHolder() {
			this.itemLock = null;
			this.lastPersistedDate = null;
		}

		/**
		 * return null only for missing/invalid lock holder
		 *
		 * @return
		 */
		public @Nullable
		ItemLock getItemLock() {
			return itemLock;
		}

		/**
		 * return null only for missing/invalid lock holder
		 *
		 * @return
		 */
		public @Nullable
		DateTime getLastPersistedDate() {
			return lastPersistedDate;
		}

		public boolean isInvalid() {
			return getItemLock() == null;
		}

	}
	private final ItemLockHolder MISSING_LOCK = new ItemLockHolder();

	private ItemLockHolder missingLock() {
		return MISSING_LOCK;
	}

}
