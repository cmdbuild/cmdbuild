/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.easytemplate;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import javax.annotation.Nullable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.auth.user.OperationUser;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.isLong;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toLong;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EasytemplateServiceImpl implements EasytemplateService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

	private final OperationUserSupplier userSupplier;

	public EasytemplateServiceImpl(OperationUserSupplier userSupplier) {
		this.userSupplier = checkNotNull(userSupplier);
	}

	@Override
	public EasytemplateProcessor getDefaultProcessorWithJsContext(String jsContext) {
		return new EasytemplateProcessorHelper(jsContext).getEasytemplateProcessor();
	}

	private class EasytemplateProcessorHelper {

		private final String jsContext;

		public EasytemplateProcessorHelper(String jsContext) {
			this.jsContext = jsContext;
		}

		private EasytemplateProcessor getEasytemplateProcessor() {
			OperationUser user = userSupplier.getUser();
			return EasytemplateProcessorImpl.builder()
					.withResolver("client", this::evalClient)
					.withResolver("server", this::evalServer)
					.withResolver("user", (key) -> {
						switch (key.toLowerCase()) {
							case "id":
								return user.getLoginUser().getId();
							case "name":
								return user.getLoginUser().getUsername();
							default:
								throw unsupported("unsupported 'user:' key = %s", key);
						}
					})
					.withResolver("group", (key) -> {
						switch (key.toLowerCase()) {
							case "id":
								return user.hasDefaultGroup() ? user.getDefaultGroup().getId() : null;
							case "name":
								return user.hasDefaultGroup() ? user.getDefaultGroup().getName() : null;
							default:
								throw unsupported("unsupported 'group:' key = %s", key);
						}
					})
					.build();
		}

		private Object evalClient(String expr) {
			return evalJsSubcontext("client", expr);
		}

		private Object evalServer(String expr) {
			return evalJsSubcontext("server", expr);
		}

		private Object evalJsSubcontext(String sc, String expr) {
			return evalJavascriptCode(format("var context = %s;\n\nvar result;\n\nwith(context){\n\tresult = %s[\"%s\"];\n};\n\nresult;", jsContext, sc, expr));
		}

		private Object evalJavascriptCode(String jsScript) {
			logger.debug("execute js script = \n\n{}\n", jsScript);
			try {
				return jsToSystem(engine.eval(jsScript));
			} catch (Exception ex) {
				throw runtime(ex, "error processing js script = '%s'", abbreviate(jsScript));
			}
		}

		private @Nullable
		Object jsToSystem(@Nullable Object value) {
			if (value == null) {
				return null;
			} else if (value instanceof Number && isLong((Number) value)) {
				return toLong(value);
			} else {
				return value;
			}
		}

	}

}
