package org.cmdbuild.easytemplate.resolvers;

import com.google.common.base.Function;
import static org.cmdbuild.common.Constants.ID_ATTRIBUTE;

import org.apache.commons.lang3.Validate;
import org.cmdbuild.dao.beans.IdAndDescriptionImpl;
import org.cmdbuild.dao.entrytype.attributetype.CMAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForwardingAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.NullAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.beans.Card;

public class EasytemplateCardResolver implements Function<String, Object> {

	private final Card card;

	private EasytemplateCardResolver(final Builder builder) {
		this.card = builder.card;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static EasytemplateCardResolver forCard(Card card) {
		return builder().withCard(card).build();
	}

	@Override
	public Object apply(String expression) {
		if (ID_ATTRIBUTE.equalsIgnoreCase(expression)) {
			return card.getId();
		}
		return new ForwardingAttributeTypeVisitor() {

			private final CMAttributeTypeVisitor DELEGATE = NullAttributeTypeVisitor.getInstance();

			private Object adapted;

			@Override
			protected CMAttributeTypeVisitor delegate() {
				return DELEGATE;
			}

			public Object adapt(final Object value) {
				adapted = value;
				card.getType().getAttributeOrNull(expression).getType().accept(this);
				return adapted;
			}

			@Override
			public void visit(final ForeignKeyAttributeType attributeType) {
				adapted = IdAndDescriptionImpl.class.cast(adapted).getId();
			}

			@Override
			public void visit(final LookupAttributeType attributeType) {
				adapted = IdAndDescriptionImpl.class.cast(adapted).getId();
			}

			;

			@Override
			public void visit(final ReferenceAttributeType attributeType) {
				adapted = IdAndDescriptionImpl.class.cast(adapted).getId();
			}

		}.adapt(card.get(expression));
	}

	public static class Builder implements org.apache.commons.lang3.builder.Builder<EasytemplateCardResolver> {

		private Card card;

		private Builder() {
			// use factory method
		}

		@Override
		public EasytemplateCardResolver build() {
			validate();
			return new EasytemplateCardResolver(this);
		}

		private void validate() {
			Validate.notNull(card, "missing '{}'", Card.class);
		}

		public Builder withCard(final Card card) {
			this.card = card;
			return this;
		}

	}

}
