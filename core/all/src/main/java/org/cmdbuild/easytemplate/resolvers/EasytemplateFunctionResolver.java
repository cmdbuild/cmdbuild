package org.cmdbuild.easytemplate.resolvers;

import static com.google.common.base.Functions.identity;
import static com.google.common.collect.FluentIterable.from;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;
import static org.cmdbuild.common.Constants.DATETIME_PRINTING_PATTERN;
import static org.joda.time.format.DateTimeFormat.forPattern;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.joda.time.DateTime;

import com.google.common.base.Function;
import com.google.common.base.Splitter;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.dao.function.StoredFunction;
import org.cmdbuild.dao.function.StoredFunctionParameter;
import org.cmdbuild.dao.function.StoredFunctionOutputParameter;
import org.cmdbuild.dao.view.DataView;

public class EasytemplateFunctionResolver implements Function<String, Object> {

	public static final Function<Object, Object> DEFAULT_CONVERTER = (final Object input) -> {
		final Object output;
		if (input instanceof DateTime) {
			output = forPattern(DATETIME_PRINTING_PATTERN).print(DateTime.class.cast(input));
		} else {
			output = input;
		}
		return output;
	};

	private static final Pattern PATTERN = Pattern.compile("^([^()]+)\\((.*)\\)$");
	private static final String SEPARATOR = ",";

	/**
	 * @deprecated enclose within a logic
	 */
	@Deprecated
	private final DataView dataView;
	private final DataAccessService dataAccessLogic;
	private final Function<Object, Object> converter;

	private EasytemplateFunctionResolver(final Builder builder) {
		this.dataView = builder.dataView;
		this.dataAccessLogic = builder.dataAccessLogic;
		this.converter = builder.converter;
	}

	@Override
	public Object apply(final String expression) {
		final Matcher matcher = PATTERN.matcher(expression);
		isTrue(matcher.matches(), "invalid expression '%s'", expression);
		final String name = matcher.group(1);
		final StoredFunction function = notNull(dataView.findFunctionByName(name), "missing function '%s'", name);
		final String parameters = matcher.group(2);
		final List<String> values = isBlank(parameters) ? emptyList()
				: Splitter.on(SEPARATOR) //
						.trimResults() //
						.splitToList(parameters);
		final List<StoredFunctionParameter> inputs = function.getInputParameters();
		isTrue(inputs.size() == values.size(), "values (%d) mismatch input parameters (%d)", values.size(),
				inputs.size());
		final Map<String, Object> params = new HashMap<>();
		final Iterator<String> valuesItr = values.iterator();
		inputs.forEach(input -> params.put(input.getName(), valuesItr.next()));
		final Collection<StoredFunctionOutputParameter> outputs = function.getOutputParameters();
		isTrue(outputs.size() == 1, "only one output parameter supported");
		final StoredFunctionParameter output = outputs.iterator().next();
		return from(dataAccessLogic.fetchSQLCards(name,
				QueryOptionsImpl.builder() //
						.parameters(params) //
						.limit(1) //
						.build())) //
				.first() //
				.transform(input -> input.get(output.getName())) //
				.transform(converter) //
				.get();
	}

	public static Builder newInstance() {
		return new Builder();
	}

	public static class Builder implements org.apache.commons.lang3.builder.Builder<EasytemplateFunctionResolver> {

		private DataView dataView;
		private DataAccessService dataAccessLogic;
		private Function<Object, Object> converter;

		private Builder() {
			// use factory method
		}

		@Override
		public EasytemplateFunctionResolver build() {
			notNull(dataView, "missing '%s'", DataView.class);
			notNull(dataAccessLogic, "missing '%s'", DataAccessService.class);
			converter = defaultIfNull(converter, identity());
			return new EasytemplateFunctionResolver(this);
		}

		public Builder withDataView(final DataView dataView) {
			this.dataView = dataView;
			return this;
		}

		public Builder withDataAccessLogic(final DataAccessService dataAccessLogic) {
			this.dataAccessLogic = dataAccessLogic;
			return this;
		}

		public Builder withConverter(final Function<Object, Object> converter) {
			this.converter = converter;
			return this;
		}

	}

}
