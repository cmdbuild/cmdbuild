package org.cmdbuild.easytemplate.resolvers;

import com.google.common.base.Function;
import static org.cmdbuild.dao.query.clause.AnyAttribute.anyAttribute;
import static org.cmdbuild.dao.query.clause.QueryAliasAttribute.attribute;
import static org.cmdbuild.dao.query.clause.where.EqualsOperatorAndValue.eq;
import static org.cmdbuild.dao.query.clause.where.SimpleWhereClause.condition;

import org.apache.commons.lang3.Validate;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.view.DataView;

public class EasytemplateUserEmailResolver implements Function<String, Object> {

	private static final String USER_CLASSNAME = "User";
	private static final String USERNAME_ATTRIBUTE = "Username";
	private static final String EMAIL_ATTRIBUTE = "Email";

	public static class Builder implements org.apache.commons.lang3.builder.Builder<EasytemplateUserEmailResolver> {

		private DataView dataView;

		private Builder() {
			// use factory method
		}

		@Override
		public EasytemplateUserEmailResolver build() {
			validate();
			return new EasytemplateUserEmailResolver(this);
		}

		private void validate() {
			Validate.notNull(dataView, "missing data view");
		}

		public Builder withDataView(final DataView dataView) {
			this.dataView = dataView;
			return this;
		}

	}

	public static Builder newInstance() {
		return new Builder();
	}

	private final DataView dataView;

	private EasytemplateUserEmailResolver(final Builder builder) {
		this.dataView = builder.dataView;
	}

	@Override
	public Object apply(final String expression) {
		final Classe userClass = dataView.findClasse(USER_CLASSNAME);
		Validate.notNull(userClass, "user class not visible");
		final Card card = dataView.select(anyAttribute(userClass)) //
				.from(userClass) //
				.where(condition(attribute(userClass, USERNAME_ATTRIBUTE), eq(expression))) //
				.limit(1) //
				.skipDefaultOrdering() //
				.run() //
				.getOnlyRow() //
				.getCard(userClass);
		return card.get(EMAIL_ATTRIBUTE, String.class);
	}

}
