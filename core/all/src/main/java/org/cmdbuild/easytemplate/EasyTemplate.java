package org.cmdbuild.easytemplate;

import javax.annotation.Nullable;

public interface EasyTemplate {

	@Nullable
	Long getId();

	String getKey();

	String getValue();
}
