package org.cmdbuild.easytemplate;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nullable;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import static org.cmdbuild.easytemplate.EasyTemplateImpl.TEMPLATES_TABLE;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@CardMapping(TEMPLATES_TABLE)
public class EasyTemplateImpl implements EasyTemplate {

	public static final String TEMPLATES_TABLE = "_Templates";
	public static final String TEMPLATE_NAME = "Name";
	public static final String TEMPLATE_DEFINITION = "Template";

	private final Long id;
	private final String key;
	private final String value;

	private EasyTemplateImpl(SimpleTemplateBuilder builder) {
		this.id = builder.id;
		this.key = checkNotBlank(builder.key);
		this.value = checkNotNull(builder.value);
	}

	private EasyTemplateImpl(String key, String value) {
		this.id = null;
		this.key = key;
		this.value = value;
	}

	public static EasyTemplateImpl of(String key) {
		return new EasyTemplateImpl(key, null);
	}

	public static EasyTemplateImpl of(String key, String value) {
		return new EasyTemplateImpl(key, value);
	}

	@CardAttr(ATTR_ID)
	@Override
	@Nullable
	public Long getId() {
		return id;
	}

	@CardAttr(TEMPLATE_NAME)
	@Override
	public String getKey() {
		return key;
	}

	@CardAttr(TEMPLATE_DEFINITION)
	@Override
	public String getValue() {
		return value;
	}

	public static SimpleTemplateBuilder builder() {
		return new SimpleTemplateBuilder();
	}

	public static SimpleTemplateBuilder copyOf(EasyTemplateImpl source) {
		return new SimpleTemplateBuilder()
				.withId(source.getId())
				.withKey(source.getKey())
				.withValue(source.getValue());
	}

	public static class SimpleTemplateBuilder implements Builder<EasyTemplateImpl, SimpleTemplateBuilder> {

		private Long id;
		private String key;
		private String value;

		public SimpleTemplateBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public SimpleTemplateBuilder withKey(String key) {
			this.key = key;
			return this;
		}

		public SimpleTemplateBuilder withValue(String value) {
			this.value = value;
			return this;
		}

		@Override
		public EasyTemplateImpl build() {
			return new EasyTemplateImpl(this);
		}

	}
}
