package org.cmdbuild.easytemplate.resolvers;

import com.google.common.base.Function;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.cmdbuild.dao.query.clause.AnyAttribute.anyAttribute;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Triple;
import org.cmdbuild.dao.query.CMQueryRow;
import org.cmdbuild.dao.query.QuerySpecsBuilder;
import org.cmdbuild.dao.query.clause.AnyAttribute;
import org.cmdbuild.dao.query.clause.QueryAliasAttribute;
import org.cmdbuild.dao.query.clause.QueryAttributeVisitor;
import org.cmdbuild.dao.query.clause.alias.Alias;
import org.cmdbuild.dao.query.clause.join.Over;
import org.cmdbuild.dao.query.clause.where.WhereClause;

import com.google.common.collect.Lists;
import org.cmdbuild.dao.query.QueryResult;
import static org.cmdbuild.spring.SpringIntegrationUtils.applicationContext;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.cql.CqlService;
import org.cmdbuild.cql.CqlProcessingCallback;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.view.DataView;

public class EasytemplateCqlResolver implements Function<String, Object> {

	private static final Map<String, Object> EMPTY_CONTEXT = Collections.emptyMap();

	private final DataView dataView;

	private EasytemplateCqlResolver(final Builder builder) {
		this.dataView = builder.dataView;
	}

	@Override
	public Object apply(final String expression) {
		final QueryCallback callback = new QueryCallback(dataView);
		applicationContext().getBean(CqlService.class).compileAndAnalyze(expression, EMPTY_CONTEXT, callback);
		final QueryResult result = callback.execute();
		final CMQueryRow row = result.getOnlyRow();
		final QueryAliasAttribute attribute = getOnlyElement(callback.attributes);
		attribute.accept(new QueryAttributeVisitor() {

			@Override
			public void accept(final AnyAttribute value) {
				throw new IllegalArgumentException();
			}

			@Override
			public void visit(final QueryAliasAttribute value) {
			}

		});
		final Card card = row.getCard(callback.source);
		final Object value;
		if (org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID.equals(attribute.getName())) {
			value = card.getId();
		} else {
			value = card.get(attribute.getName());
		}
		return value;
	}

	public static Builder newInstance() {
		return new Builder();
	}

	public static class Builder implements org.apache.commons.lang3.builder.Builder<EasytemplateCqlResolver> {

		private DataView dataView;

		private Builder() {
			// use factory method
		}

		@Override
		public EasytemplateCqlResolver build() {
			validate();
			return new EasytemplateCqlResolver(this);
		}

		private void validate() {
			Validate.notNull(dataView, "missing '%s'", DataView.class);
		}

		public Builder withDataView(final DataView dataView) {
			this.dataView = dataView;
			return this;
		}

	}

	private static final class QueryCallback implements CqlProcessingCallback {

		final DataView dataView;

		public QueryCallback(final DataView dataView) {
			this.dataView = dataView;
		}

		Classe source;
		Iterable<QueryAliasAttribute> attributes;
		private boolean distinct;
		private final Collection<Triple<Classe, Alias, Over>> leftJoins = Lists.newArrayList();
		private final Collection<Triple<Classe, Alias, Over>> joins = Lists.newArrayList();
		private final Collection<WhereClause> whereClauses = Lists.newArrayList();

		@Override
		public void from(final Classe source) {
			this.source = source;
		}

		@Override
		public void attributes(final Iterable<QueryAliasAttribute> attributes) {
			this.attributes = attributes;
		}

		@Override
		public void distinct() {
			distinct = true;
		}

		@Override
		public void leftJoin(final Classe target, final Alias alias, final Over over) {
			leftJoins.add(Triple.of(target, alias, over));
		}

		@Override
		public void join(final Classe target, final Alias alias, final Over over) {
			joins.add(Triple.of(target, alias, over));
		}

		@Override
		public void where(final WhereClause clause) {
			whereClauses.add(clause);
		}

		public QueryResult execute() {
			final QuerySpecsBuilder querySpecsBuilder = dataView.select(anyAttribute(source)) //
					.from(source);
			if (distinct) {
				querySpecsBuilder.distinct();
			}
			for (final Triple<Classe, Alias, Over> leftJoin : leftJoins) {
				querySpecsBuilder.leftJoin(leftJoin.getLeft(), leftJoin.getMiddle(), leftJoin.getRight());
			}
			for (final Triple<Classe, Alias, Over> join : joins) {
				querySpecsBuilder.join(join.getLeft(), join.getMiddle(), join.getRight());
			}
			for (final WhereClause whereClause : whereClauses) {
				querySpecsBuilder.where(whereClause);
			}
			return querySpecsBuilder.run();
		}

	}
}
