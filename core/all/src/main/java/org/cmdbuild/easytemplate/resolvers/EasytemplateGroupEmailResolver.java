package org.cmdbuild.easytemplate.resolvers;

import com.google.common.base.Function;
import static org.cmdbuild.common.Constants.ROLE_CLASS_NAME;
import static org.cmdbuild.dao.query.clause.AnyAttribute.anyAttribute;
import static org.cmdbuild.dao.query.clause.QueryAliasAttribute.attribute;
import static org.cmdbuild.dao.query.clause.where.EqualsOperatorAndValue.eq;
import static org.cmdbuild.dao.query.clause.where.SimpleWhereClause.condition;

import org.apache.commons.lang3.Validate;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.view.DataView;

public class EasytemplateGroupEmailResolver implements Function<String,Object> {

	private static final String CODE_ATTRIBUTE = "Code";
	private static final String EMAIL_ATTRIBUTE = "Email";

	public static class Builder implements org.apache.commons.lang3.builder.Builder<EasytemplateGroupEmailResolver> {

		private DataView dataView;

		private Builder() {
			// use factory method
		}

		@Override
		public EasytemplateGroupEmailResolver build() {
			validate();
			return new EasytemplateGroupEmailResolver(this);
		}

		private void validate() {
			Validate.notNull(dataView, "missing data view");
		}

		public Builder withDataView(final DataView dataView) {
			this.dataView = dataView;
			return this;
		}

	}

	public static Builder newInstance() {
		return new Builder();
	}

	private final DataView dataView;

	private EasytemplateGroupEmailResolver(final Builder builder) {
		this.dataView = builder.dataView;
	}

	@Override
	public Object apply(final String expression) {
		final Classe roleClass = dataView.findClasse(ROLE_CLASS_NAME);
		Validate.notNull(roleClass, "role class not visible");
		final Card card = dataView.select(anyAttribute(roleClass)) //
				.from(roleClass) //
				.where(condition(attribute(roleClass, CODE_ATTRIBUTE), eq(expression))) //
				.limit(1) //
				.skipDefaultOrdering() //
				.run() //
				.getOnlyRow() //
				.getCard(roleClass);
		return card.get(EMAIL_ATTRIBUTE, String.class);
	}

}
