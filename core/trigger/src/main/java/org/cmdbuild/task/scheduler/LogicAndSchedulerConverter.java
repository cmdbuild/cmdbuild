package org.cmdbuild.task.scheduler;

import org.cmdbuild.logic.taskmanager.ScheduledTask;
import org.cmdbuild.scheduler.JobWithTask;

public interface LogicAndSchedulerConverter {

	interface LogicAsSourceConverter {

		LogicAsSourceConverter withNoExecution();

		JobWithTask toJob();

	}

	LogicAsSourceConverter from(ScheduledTask source);

}