package org.cmdbuild.task.scheduler;

import static org.cmdbuild.scheduler.command.Commands.nullCommand;

import org.cmdbuild.logic.taskmanager.ScheduledTask;
import org.cmdbuild.scheduler.command.BuildableCommandBasedJob;
import org.cmdbuild.scheduler.command.Command;
import org.slf4j.Logger;
import org.cmdbuild.scheduler.JobWithTask;
import org.slf4j.LoggerFactory;

public abstract class AbstractJobFactory<T extends ScheduledTask> implements JobFactory<T> {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	protected abstract Class<T> getType();

	@Override
	public final JobWithTask create(final ScheduledTask task, final boolean execution) {
		final T specificTask = getType().cast(task);
		return BuildableCommandBasedJob.newInstance() //
				.withName(name(specificTask)) //
				.withCommand(execution ? command(specificTask) : nullCommand()) //
				.withTaskId(task.getId())
				.build();
	}

	private String name(final T task) {
		return task.getId().toString();
	}

	protected abstract Command command(T task);

}
