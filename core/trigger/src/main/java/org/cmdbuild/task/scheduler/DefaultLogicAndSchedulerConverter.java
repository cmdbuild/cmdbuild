package org.cmdbuild.task.scheduler;

import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.cmdbuild.logic.taskmanager.ScheduledTask;

import com.google.common.collect.Maps;
import org.cmdbuild.task.wizardconnector.ConnectorTask;
import org.cmdbuild.task.wizardconnector.ConnectorTaskJobFactory;
import org.cmdbuild.task.reademail.ReadEmailTask;
import org.cmdbuild.task.reademail.ReadEmailTaskJobFactory;
import org.cmdbuild.task.asyncevent.AsynchronousEventTask;
import org.cmdbuild.task.asyncevent.AsynchronousEventTaskJobFactory;
import org.cmdbuild.task.generictask.GenericTask;
import org.cmdbuild.task.generictask.GenericTaskJobFactory;
import org.cmdbuild.task.startworkflow.StartWorkflowTask;
import org.cmdbuild.task.startworkflow.StartWorkflowTaskJobFactory;
import org.cmdbuild.scheduler.JobWithTask;
import org.springframework.stereotype.Component;

@Component
public class DefaultLogicAndSchedulerConverter implements LogicAndSchedulerConverter {

	private final Map<Class<? extends ScheduledTask>, JobFactory<? extends ScheduledTask>> factories;

	public DefaultLogicAndSchedulerConverter(AsynchronousEventTaskJobFactory asynchronousEventTaskJobFactory, ConnectorTaskJobFactory connectorTaskJobFactory, GenericTaskJobFactory genericTaskJobFactory, ReadEmailTaskJobFactory readEmailTaskJobFactory, StartWorkflowTaskJobFactory startWorkflowTaskJobFactory) {
		factories = Maps.newHashMap();
		register(AsynchronousEventTask.class, asynchronousEventTaskJobFactory);
		register(ConnectorTask.class, connectorTaskJobFactory);
		register(GenericTask.class, genericTaskJobFactory);
		register(ReadEmailTask.class, readEmailTaskJobFactory);
		register(StartWorkflowTask.class, startWorkflowTaskJobFactory);
	}

	private <T extends ScheduledTask> void register(Class<T> type, JobFactory<T> factory) {
		factories.put(type, factory);
	}

	@Override
	public LogicAsSourceConverter from(ScheduledTask source) {
		return new DefaultLogicAsSourceConverter(factories, source, true);
	}

	private static class DefaultLogicAsSourceConverter implements LogicAsSourceConverter {

		private final Map<Class<? extends ScheduledTask>, JobFactory<? extends ScheduledTask>> factories;
		private final ScheduledTask source;
		private final boolean execution;

		private JobWithTask job;

		public DefaultLogicAsSourceConverter(Map<Class<? extends ScheduledTask>, JobFactory<? extends ScheduledTask>> factories, ScheduledTask source, boolean execution) {
			this.factories = factories;
			this.source = source;
			this.execution = execution;
		}

		@Override
		public LogicAsSourceConverter withNoExecution() {
			return new DefaultLogicAsSourceConverter(factories, source, false);
		}

		@Override
		public JobWithTask toJob() {
			if (source.isScheduledTask()) {
				job = factories.get(source.getClass()).create(source.asScheduledTask(), execution);
			} else {
				throw new UnsupportedOperationException("invalid task " + source);
			}
			Validate.notNull(job, "conversion error");
			return job;
		}

	}
}
