package org.cmdbuild.task.scheduler;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Throwables.propagate;

import org.cmdbuild.logic.taskmanager.ScheduledTask;
import org.cmdbuild.scheduler.ForwardingJob;
import org.cmdbuild.scheduler.RecurringTrigger;
import org.cmdbuild.scheduler.SchedulerService;
import org.slf4j.Logger;
import org.cmdbuild.scheduler.JobWithTask;
import org.cmdbuild.scheduler.JobTrigger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SchedulerFacadeImpl implements SchedulerFacade {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LogicAndSchedulerConverter converter;
	private final SchedulerService schedulerService;

	public SchedulerFacadeImpl(LogicAndSchedulerConverter converter, SchedulerService schedulerService) {
		this.converter = checkNotNull(converter);
		this.schedulerService = checkNotNull(schedulerService);
	}

	@Override
	public void create(ScheduledTask task, Callback callback) { //TODO make sure this works as expected in clustered environment
		logger.info("creating a new scheduled task '{}'", task);
		if (task.isActive()) {
			JobWithTask job = new SuppressedExceptionJob((JobWithTask) jobFrom(task, callback));
			JobTrigger trigger = RecurringTrigger.at(addSecondsField(task.getCronExpression()));
			schedulerService.add(job, trigger);
		}
	}

	private String addSecondsField(String cronExpression) {
		return "0 " + cronExpression;
	}

	@Override
	public void delete(ScheduledTask task) {
		logger.info("deleting an existing scheduled task '{}'", task);
		if (!task.isActive()) {
			JobWithTask job = converter.from(task).withNoExecution().toJob();
			schedulerService.remove(job);
		}
	}

	@Override
	public void execute(ScheduledTask task, Callback callback) {
		logger.info("executing an existing scheduled task '{}'", task);
		jobFrom(task, callback).execute();
	}

	private JobWithTask jobFrom(ScheduledTask task, Callback callback) {
		JobWithTask job = (JobWithTask) converter.from(task).toJob();
		JobWithTask jobWithCallback = new JobWithCallback(job, callback);
		JobWithTask jobWithLogging = new JobWithCallback((JobWithTask) jobWithCallback, LoggingCallback.of(jobWithCallback));
		return jobWithLogging;
	}

	private class SuppressedExceptionJob extends ForwardingJob {

		private final JobWithTask delegate;

		private SuppressedExceptionJob(JobWithTask delegate) {
			this.delegate = delegate;
		}

		@Override
		protected JobWithTask delegate() {
			return delegate;
		}

		@Override
		public void execute() {
			try {
				delegate().execute();
			} catch (Throwable e) {
				logger.warn("error executing job", e);
			}
		}

	}

	private class JobWithCallback extends ForwardingJob {

		private final JobWithTask delegate;
		private final Callback callback;

		public JobWithCallback(JobWithTask delegate, Callback callback) {
			this.delegate = delegate;
			this.callback = callback;
		}

		@Override
		protected JobWithTask delegate() {
			return delegate;
		}

		@Override
		public void execute() {
			callback.start();
			try {
				super.execute();
			} catch (Throwable e) {
				callback.error(e);
				propagate(e);
			}
			callback.stop();
		}

	}
}
