package org.cmdbuild.task.scheduler;

import org.cmdbuild.task.scheduler.SchedulerFacade.Callback;
import org.slf4j.Logger;
import org.cmdbuild.scheduler.JobWithTask;
import org.slf4j.LoggerFactory;

public class LoggingCallback implements Callback {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static LoggingCallback of(final JobWithTask job) {
		return new LoggingCallback(job);
	}

	private final JobWithTask job;

	private LoggingCallback(final JobWithTask job) {
		this.job = job;
	}

	@Override
	public void start() {
		logger.info("starting job '{}'", job.getName());
	}

	@Override
	public void stop() {
		logger.info("stopping job '{}'", job.getName());
	}

	@Override
	public void error(final Throwable e) {
		final String message = String.format("error on job '%s'", job.getName());
		logger.error(message, e);
	}

}
