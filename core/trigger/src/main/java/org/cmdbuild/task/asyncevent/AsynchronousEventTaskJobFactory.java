package org.cmdbuild.task.asyncevent;

import static com.google.common.base.Suppliers.memoize;
import static com.google.common.collect.FluentIterable.from;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.easytemplate.EasytemplateUtils.emptyStringOnNull;
import static org.cmdbuild.easytemplate.EasytemplateUtils.nullOnError;
import static org.cmdbuild.dao.guava.Functions.toCard;
import static org.cmdbuild.dao.query.clause.AnyAttribute.anyAttribute;
import static org.cmdbuild.dao.query.clause.ClassHistory.history;
import static org.cmdbuild.dao.query.clause.QueryAliasAttribute.attribute;
import static org.cmdbuild.dao.query.clause.where.OperatorAndValues.eq;
import static org.cmdbuild.dao.query.clause.where.OperatorAndValues.lt;
import static org.cmdbuild.dao.query.clause.where.WhereClauses.and;
import static org.cmdbuild.dao.query.clause.where.WhereClauses.condition;
import static org.cmdbuild.easytemplate.EasytemplateResolverNames.CARD_PREFIX;
import static org.cmdbuild.easytemplate.EasytemplateResolverNames.CQL_PREFIX;
import static org.cmdbuild.easytemplate.EasytemplateResolverNames.DB_TEMPLATE;
import static org.cmdbuild.easytemplate.EasytemplateResolverNames.FUNCTION_PREFIX;
import static org.cmdbuild.easytemplate.resolvers.EasytemplateFunctionResolver.DEFAULT_CONVERTER;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

import javax.activation.DataHandler;

import org.cmdbuild.easytemplate.EasytemplateProcessorImpl;
import org.cmdbuild.task.dao.TaskStore;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.data2.impl.QuerySpecsBuilderService;
import org.cmdbuild.logic.mapping.json.JsonFilterHelper;
import org.cmdbuild.task.scheduler.AbstractJobFactory;
import org.cmdbuild.task.dao.LogicAndStoreConverter;
import org.cmdbuild.task.util.CardIdFilterElementGetter;
import org.cmdbuild.scheduler.command.Command;
import org.cmdbuild.easytemplate.resolvers.EasytemplateCardResolver;
import org.cmdbuild.easytemplate.resolvers.EasytemplateCqlResolver;
import org.cmdbuild.easytemplate.resolvers.EasytemplateFunctionResolver;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Optional;
import com.google.common.base.Supplier;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.dao.query.QueryResult;
import org.cmdbuild.easytemplate.EasytemplateProcessor;
import org.cmdbuild.dao.entrytype.Classe;
import static org.cmdbuild.spring.SpringIntegrationUtils.applicationContext;
import org.cmdbuild.report.ReportService;
import static org.cmdbuild.report.ReportUtils.reportExtFromString;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_TWO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.report.ReportInfo;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.view.DataView;
import org.cmdbuild.email.EmailAccount;
import org.cmdbuild.email.EmailAccountService;
import org.cmdbuild.email.EmailTemplate;
import org.cmdbuild.email.EmailTemplateService;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;

@Component
public class AsynchronousEventTaskJobFactory extends AbstractJobFactory<AsynchronousEventTask> {

	private static final Comparator<Card> BEGIN_DATE_DESCENDING = (Card o1, Card o2) -> o2.getBeginDate().compareTo(o1.getBeginDate());

	private final DataView dataView;
	private final DataAccessService dataAccessLogic;
	private final EmailAccountService emailAccountFacade;
	private final EmailTemplateService emailTemplateLogic;
	private final TaskStore taskStore;
	private final LogicAndStoreConverter logicAndStoreConverter;
	private final EasytemplateProcessor databaseEngine;
	private final ReportService reportLogic;

	public AsynchronousEventTaskJobFactory(@Qualifier(SYSTEM_LEVEL_TWO) DataView dataView, final DataAccessService dataAccessLogic,
			final EmailAccountService emailAccountFacade, final EmailTemplateService emailTemplateLogic,
			final TaskStore taskStore, final LogicAndStoreConverter logicAndStoreConverter, final EasytemplateProcessor databaseEngine,
			final ReportService reportLogic) {
		this.dataView = dataView;
		this.dataAccessLogic = dataAccessLogic;
		this.emailAccountFacade = emailAccountFacade;
		this.emailTemplateLogic = emailTemplateLogic;
		this.taskStore = taskStore;
		this.logicAndStoreConverter = logicAndStoreConverter;
		this.databaseEngine = databaseEngine;
		this.reportLogic = reportLogic;
	}

	@Override
	protected Class<AsynchronousEventTask> getType() {
		return AsynchronousEventTask.class;
	}

	@Override
	protected Command command(final AsynchronousEventTask task) {
		return new Command() {

			@Override
			public void execute() {
				final String classname = task.getTargetClassname();
				final String filter = task.getFilter();
				final DateTime lastExecution = taskStore.read(logicAndStoreConverter.taskToTaskData(task)).getLastExecution();

				logger.debug("checking class '{}' with filter '{}'", classname, filter);

				final String _filter = EasytemplateProcessorImpl.builder() //
						.withResolver(EasytemplateFunctionResolver.newInstance() //
								.withDataView(dataView) //
								.withDataAccessLogic(dataAccessLogic) //
								.withConverter(DEFAULT_CONVERTER) //
								.build(), //
								FUNCTION_PREFIX) //
						.build() //
						.processExpression(filter);
				final JSONObject jsonFilter = isBlank(_filter) ? new JSONObject() : toJsonObject(_filter);

				final Iterable<Card> cards = currentCardsMatchingFilter(classname, jsonFilter);
				for (final Card card : cards) {
					final Classe cardType = card.getType();
					if (createdAfterLastExecution(card, lastExecution)) {
						logger.debug("history card not found");
						sendEmail(task, card);
					} else {
						final Optional<Card> lastHistoryCardWithNoFilter = historyCardBeforeLastExecutionWithNoFilter(
								cardType, card.getId(), lastExecution);
						if (lastHistoryCardWithNoFilter.isPresent()) {
							final Card historyCardWithNoFilter = lastHistoryCardWithNoFilter.get();
							logger.debug("found history card with id '{}'", historyCardWithNoFilter.getId());
							final Optional<Card> historyCardWithFilter = historyCardBeforeLastExecutionWithFilter(
									cardType, historyCardWithNoFilter, jsonFilter);
							if (!historyCardWithFilter.isPresent()) {
								logger.debug("filtered history card not found");
								sendEmail(task, card);
							}
						}
					}
				}
			}

			private boolean createdAfterLastExecution(final Card card, final DateTime lastExecution) {
				logger.debug("checking if card has been created after last execution");
				return (lastExecution == null) ? true : card.getBeginDate().compareTo(lastExecution) > 0;
			}

			private JSONObject toJsonObject(final String filter) {
				try {
					return new JSONObject(filter);
				} catch (final JSONException e) {
					throw new IllegalArgumentException(e);
				}
			}

			private Iterable<Card> currentCardsMatchingFilter(String classname, JSONObject jsonFilter) {
				logger.debug("getting current cards matching filter");
				Classe sourceClass = dataView.findClasse(classname);
				QueryOptionsImpl queryOptions = QueryOptionsImpl.builder().filter(jsonFilter).build();
				QueryResult result = applicationContext().getBean(QuerySpecsBuilderService.class).withQueryOptions(queryOptions).withNullableEntryType(sourceClass).builder().run();
				return from(result).transform(toCard(sourceClass));
			}

			private Optional<Card> historyCardBeforeLastExecutionWithNoFilter(final Classe type, final Long id,
					final DateTime lastExecution) {
				logger.debug("getting last history for card of type '{}' and with id '{}'", type.getName(), id);
				final Classe sourceClass = history(type);
				final QueryResult result = dataView.select(anyAttribute(sourceClass)) //
						.from(sourceClass) //
						.where(and( //
								condition(attribute(sourceClass, "CurrentId"), //
										eq(id)), //
								condition(attribute(sourceClass, "BeginDate"), //
										lt(lastExecution.toDate())))) //
						.run();
				final Iterable<Card> cards = from(result) //
						.transform(toCard(sourceClass));
				final Iterable<Card> sortedCards = Ordering.from(BEGIN_DATE_DESCENDING) //
						.sortedCopy(cards);
				return from(sortedCards) //
						.limit(1) //
						.first();
			}

			private Optional<Card> historyCardBeforeLastExecutionWithFilter(final Classe type, final Card historyCard, final JSONObject jsonFilter) {
				logger.debug("getting last history for card of type '{}' and id '{}', with filter '{}'",
						type.getName(), historyCard.getId(), jsonFilter);
				try {
					QueryOptionsImpl queryOptions = QueryOptionsImpl.builder() //
							.filter(new JsonFilterHelper(jsonFilter) //
									.merge(CardIdFilterElementGetter.of(historyCard))) //
							.build();
					Classe sourceClass = history(type);
					QueryResult result = applicationContext().getBean(QuerySpecsBuilderService.class).builder(queryOptions, sourceClass).run();
					return from(result) //
							.transform(toCard(sourceClass)) //
							.limit(1) //
							.first();
				} catch (final JSONException e) {
					logger.warn("error with json format", e);
					return Optional.absent();
				}
			}

		};
	}

	private void sendEmail(final AsynchronousEventTask task, final Card card) {
		if (task.isNotificationActive()) {
			final Supplier<EmailTemplate> emailTemplateSupplier = memoize(() -> {
				final String name = defaultString(task.getNotificationTemplate());
				return emailTemplateLogic.getByName(name);
			});
			EmailAccount emailAccount = emailAccountFacade.getAccountOrDefaultOrNull(toStringOrNull(emailTemplateSupplier.get().getAccount()), task.getNotificationAccount());//TODO check themplate.getAccount, should return valid account name/id
//			final Optional<EmailAccount> account = emailAccountFacade.
//					.firstOfOrDefault(asList(emailTemplateSupplier.get().getAccount(), task.getNotificationAccount()));
//			final Supplier<EmailAccount> emailAccountSupplier = account.isPresent() ? ofInstance(account.get()) : null;
			final EasytemplateProcessorImpl templateResolver = EasytemplateProcessorImpl.builder() //
					.withResolver(emptyStringOnNull(nullOnError(EasytemplateCardResolver.builder() //
							.withCard(card) //
							.build())), //
							CARD_PREFIX) //
					.withResolver(emptyStringOnNull(nullOnError(EasytemplateCqlResolver.newInstance() //
							.withDataView(dataView) //
							.build())), //
							CQL_PREFIX) //
					.withResolver(
							emptyStringOnNull(nullOnError( //
									(input) -> databaseEngine.processExpression(input))), //
							DB_TEMPLATE)
					.withResolver(EasytemplateFunctionResolver.newInstance() //
							.withDataView(dataView) //
							.withDataAccessLogic(dataAccessLogic) //
							.withConverter(DEFAULT_CONVERTER) //
							.build(), //
							FUNCTION_PREFIX) //
					.build();
			final Collection<Supplier<? extends DataHandler>> attachments = new ArrayList<>();
			if (task.isReportActive()) {
				attachments.add(new Supplier<DataHandler>() {

					@Override
					public DataHandler get() {
						final ReportInfo report = from(reportLogic.getAll()) //
								.filter(input -> input.getCode().equals(task.getReportName())) //
								.limit(1) //
								.first() //
								.get();
						return reportLogic.executeReportAndDownload(report.getId(), reportExtFromString(task.getReportExtension()),
								resolve((Map) task.getReportParameters()));
					}

					private Map<String, ? extends Object> resolve(final Map<String, String> input) {
						return Maps.transformValues(input, (final String input1) -> templateResolver.processExpression(input1));
					}

				});
			}
			throw new UnsupportedOperationException("TODO");
//			emailTemplateSenderFactory.queued() //
//					.withAccount(emailAccount) //
//					.withTemplate(emailTemplateSupplier) //
//					.withTemplateResolver(templateResolver) //
//					.withReference(task.getId()) //
//					.withAttachments(attachments) //
//					.build() //
//					.execute();
		}
	}

}
