package org.cmdbuild.task.reademail;

import org.cmdbuild.task.scheduler.AbstractJobFactory;
import org.cmdbuild.scheduler.command.Command;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.easytemplate.EasytemplateProcessor;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_TWO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.dao.function.FunctionCallService;
import org.cmdbuild.dao.view.DataView;

@Component
public class ReadEmailTaskJobFactory extends AbstractJobFactory<ReadEmailTask> {

	private static final String //
			CONTENT = "content", //
			CC_ADDRESSES = "ccAddresses", //
			FROM_ADDRESS = "fromAddress", //
			SEPARATOR = ",", //
			SUBJECT = "subject", //
			TO_ADDRESSES = "toAddresses";

	private static final String //
			FUNCTION = "function", //
			NONE = "none", //
			REGEX = "regex";

//	private final EmailAccountFacade emailAccountFacade;
//	private final EmailServiceFactory emailServiceFactory;
//	private final SubjectHandler subjectHandler;
//	private final Store<StorableEmail> emailStore;
	private final WorkflowService workflowLogic;
	private final DmsService dmsLogic;
	private final DataView dataView;
	private final FunctionCallService functionService;
//	private final EmailTemplateLogic emailTemplateLogic;
	private final EasytemplateProcessor databaseEngine;
//	private final EmailTemplateSenderFactory emailTemplateSenderFactory;

	public ReadEmailTaskJobFactory(FunctionCallService functionService,
			//			final EmailAccountFacade emailAccountFacade,
			//			final EmailServiceFactory emailServiceFactory, final SubjectHandler subjectHandler,
			//			final Store<StorableEmail> emailStore, 
			final WorkflowService workflowLogic,
			final DmsService dmsLogic, @Qualifier(SYSTEM_LEVEL_TWO) DataView dataView,
			//			final EmailTemplateLogic emailTemplateLogic,
			final EasytemplateProcessor databaseEngine
	//			final EmailTemplateSenderFactory emailTemplateSenderFactory
	) {
//		this.emailAccountFacade = emailAccountFacade;
//		this.emailServiceFactory = emailServiceFactory;
//		this.subjectHandler = subjectHandler;
//		this.emailStore = emailStore;
		this.workflowLogic = workflowLogic;
		this.dmsLogic = dmsLogic;
		this.dataView = dataView;
//		this.emailTemplateLogic = emailTemplateLogic;
		this.databaseEngine = databaseEngine;
//		this.emailTemplateSenderFactory = emailTemplateSenderFactory;
		this.functionService = functionService;
	}

	@Override
	protected Class<ReadEmailTask> getType() {
		return ReadEmailTask.class;
	}

	@Override
	protected Command command(final ReadEmailTask task) {
//		final String emailAccountName = task.getEmailAccount();
//		final EmailAccount selectedEmailAccount = emailAccountFor(emailAccountName).get();
//		final EmailMtaService service = emailServiceFactory.create(ofInstance(selectedEmailAccount));
		throw new UnsupportedOperationException("TODO");
//		return ReadEmailCommand.newInstance() //
//				.withEmailService(service) //
//				.withIncomingFolder(task.getIncomingFolder()) //
//				.withProcessedFolder(task.getProcessedFolder()) //
//				.withRejectedFolder(task.getRejectedFolder()) //
//				.withRejectNotMatching(task.isRejectNotMatching()) //
//				.withEmailStore(emailStore) //
//				.withAction(safe(sendNotification(task))) //
//				.withAction(safe(storeAttachments(task))) //
//				.withAction(safe(startProcess(task))) //
//				.build();
	}

//	private Optional<EmailAccount> emailAccountFor(final String name) {
//		logger.debug("getting email account for name '{}'", name);
//		return emailAccountFacade.firstOfOrDefault(asList(name));
//	}

//	private Action sendNotification(final ReadEmailTask task) { 
//		logger.info("adding notification action");
//		final Predicate<Email> condition = and(notificationActive(task), filter(task), subjectMatches());
//		return new ConditionalAction( //
//				condition, //
//				new Action() {
//
//			@Override
//			public void execute(final Email email, final Storable storable) {
//				doAdapt(email, storable);
//				doExecute(email, storable);
//			}
//
//			private void doAdapt(final Email email, final Storable storable) {
//				final ParsedSubject parsedSubject = subjectHandler.parse(email.getSubject());
//				Validate.isTrue(parsedSubject.hasExpectedFormat(), "invalid subject format");
//				final StorableEmail parent = emailStore.read(storableOf(parsedSubject
//						.getEmailId()));
//				final StorableEmail stored = emailStore.read(storable);
//				stored.setSubject(parsedSubject.getRealSubject());
//				stored.setReference(parent.getReference());
//				stored.setNotifyWith(parent.getNotifyWith());
//				emailStore.update(stored);
//			}
//
//			private void doExecute(final Email email, final Storable storable) {
//				final StorableEmail stored = emailStore.read(storable);
//				final Supplier<Template> emailTemplateSupplier = memoize(new Supplier<Template>() {
//
//					@Override
//					public Template get() {
//						final String name = defaultString(defaultIfBlank(task.getNotificationTemplate(),
//								stored.getNotifyWith()));
//						return emailTemplateLogic.read(name);
//					}
//
//				});
//				final Optional<EmailAccount> account = emailAccountFacade.firstOfOrDefault(asList(emailTemplateSupplier
//						.get().getAccount(), task.getEmailAccount()));
//				final Supplier<EmailAccount> emailAccountSupplier = account.isPresent() ? ofInstance(account
//						.get()) : null;
//				final Card genericProcessCard = workflowLogic.getFlowCard(dataView.getActivityClass()
//						.getName(), stored.getReference());
//				final Card processCard = workflowLogic.getFlowCard(genericProcessCard.getType()
//						.getName(), stored.getReference());
//				final Email htmlEmail = new ForwardingEmail() {
//
//					@Override
//					protected Email delegate() {
//						return email;
//					}
//
//					@Override
//					public String getContent() {
//						String content = super.getContent();
//
//						Pattern p = Pattern.compile(".*<[^>]+>.*");
//						Matcher m = p.matcher(content);
//
//						//FIXME - Temporary workaround: handle the content-type in the Email
//						return m.find() ? content : addLineBreakForHtml(content);
//					}
//
//				};
//				final EasytemplateProcessorImpl templateResolver = EasytemplateProcessorImpl.builder() //
//						.withResolver(emptyStringOnNull(nullOnError(EasytemplateUserEmailResolver.newInstance() //
//								.withDataView(dataView) //
//								.build())), //
//								USER_PREFIX) //
//						.withResolver(emptyStringOnNull(nullOnError(EasytemplateGroupEmailResolver.newInstance() //
//								.withDataView(dataView) //
//								.build())), //
//								GROUP_PREFIX) //
//						.withResolver(emptyStringOnNull(nullOnError(EasytemplateGroupUsersEmailResolver.newInstance() //
//								.withDataView(dataView) //
//								.withSeparator(EmailConstants.ADDRESSES_SEPARATOR) //
//								.build() //
//						)), //
//								GROUP_USERS_PREFIX) //
//						.withResolver(emptyStringOnNull(nullOnError(EasytemplateEmailResolver.newInstance() //
//								.withEmail(htmlEmail) //
//								.build())), //
//								EMAIL_PREFIX) //
//						.withResolver(emptyStringOnNull(nullOnError(Functions.forMap(//
//								EngineBasedMapper.newInstance() //
//										.withText(email.getContent()) //
//										.withEngine(task.getMapperEngine()) //
//										.build() //
//										.map() //
//								,
//								 null))), //
//								MAPPER_PREFIX) //
//						.withResolver(emptyStringOnNull(nullOnError(EasytemplateCardResolver.builder() //
//								.withCard(processCard) //
//								.build() //
//						)), //
//								CARD_PREFIX) //
//						.withResolver(emptyStringOnNull(nullOnError(EasytemplateCqlResolver.newInstance() //
//								.withDataView(dataView) //
//								.build() //
//						)), //
//								CQL_PREFIX) //
//						.withResolver(emptyStringOnNull(nullOnError( //
//								(input) -> databaseEngine.resolve(input) //
//						)), //
//								DB_TEMPLATE) //
//						.build();
//				emailTemplateSenderFactory.queued() //
//						.withAccount(emailAccountSupplier) //
//						.withTemplate(emailTemplateSupplier) //
//						.withTemplateResolver(templateResolver) //
//						.withReference(task.getId()) //
//						.build() //
//						.execute();
//			}
//
//		});
////	}
//	private Predicate<Email> notificationActive(final ReadEmailTask task) {
//		return new Predicate<Email>() {
//
//			@Override
//			public boolean apply(final Email input) {
//				return task.isNotificationActive();
//			}
//
//		};
//	}
//
//	private Predicate<Email> subjectMatches() {
//		return new Predicate<Email>() {
//
//			@Override
//			public boolean apply(final Email email) {
//				final ParsedSubject parsedSubject = subjectHandler.parse(email.getSubject());
//				if (!parsedSubject.hasExpectedFormat()) {
//					return false;
//				}
//
//				try {
//					emailStore.read(storableOf(parsedSubject.getEmailId()));
//				} catch (final Exception e) {
//					return false;
//				}
//
//				return true;
//			}
//
//		};
//	}

//	private Action storeAttachments(final ReadEmailTask task) {
//		logger.info("adding attachments action");
//		final Predicate<Email> condition = and(attachmentsActive(task), filter(task), hasAttachments());
//		return new ConditionalAction( //
//				condition, //
//				(final Email email, final Storable storable) -> {
//					final StorableEmail stored = emailStore.read(storable);
//					StoreDocumentExecutable.newInstance() //
//							.withDmsLogic(dmsLogic) //
//							.withClassName(EMAIL_CLASS_NAME) //
//							.withCardId(stored.getId()) //
//							.withCategory(task.getAttachmentsCategory()) //
//							.withDocuments(documentsFrom(email.getAttachments())) //
//							.build() //
//							.execute();
//				});
//	}
//	private Predicate<Email> attachmentsActive(final ReadEmailTask task) {
//		return (final Email input) -> task.isAttachmentsActive();
//
//	}
//
//	private Predicate<Email> hasAttachments() {
//		return (final Email email) -> !isEmpty(email.getAttachments());
//	}

//	private Action startProcess(final ReadEmailTask task) {
//		logger.info("adding start process action");
//		final Predicate<Email> condition = and(workflowActive(task), filter(task));
//		return new ConditionalAction( //
//				condition, //
//				(final Email email, final Storable storable) -> {
//					final StorableEmail stored = emailStore.read(storable);
//					final EasytemplateProcessor templateResolver = EasytemplateProcessorImpl.builder() //
//							.withResolver(emptyStringOnNull(nullOnError(EasytemplateEmailResolver.newInstance() //
//									.withEmail(email) //
//									.build())), //
//									EMAIL_PREFIX)
//							.withResolver(emptyStringOnNull(nullOnError(Functions.forMap(( //
//									EngineBasedMapper.newInstance() //
//											.withText(email.getContent()) //
//											.withEngine(task.getMapperEngine()) //
//											.build() //
//											.map() //
//									), null))), MAPPER_PREFIX)
//							.withResolver(emptyStringOnNull(nullOnError(EasytemplateCqlResolver.newInstance() //
//									.withDataView(dataView) //
//									.build() //
//							)), CQL_PREFIX)
//							.withResolver(emptyStringOnNull(nullOnError(
//									(input) -> databaseEngine.resolve(input)
//							)), DB_TEMPLATE)
//							.build();
//					StartProcessAction.newInstance() //
//							.withWorkflowLogic(workflowLogic) //
//							.withHook(new StartProcessActionCallback() {
//
//								@Override
//								public void created(final Flow userProcessInstance) {
//									stored.setReference(userProcessInstance.getCardId());
//									emailStore.update(stored);
//
//									if (task.isWorkflowAttachments()) {
//										StoreDocumentExecutable.newInstance() //
//												.withDmsLogic(dmsLogic) //
//												.withClassName(task.getWorkflowClassName()) //
//												.withCardId(userProcessInstance.getCardId()) //
//												.withCategory(task.getWorkflowAttachmentsCategory()) //
//												.withDocuments(documentsFrom(email.getAttachments())) //
//												.build() //
//												.execute();
//									}
//								}
//
//								@Override
//								public void advanced(final Flow userProcessInstance) {
//									// nothing to do
//								}
//
//							}) //
//							.withTemplateResolver(templateResolver) //
//							.withClassName(task.getWorkflowClassName()) //
//							.withAttributes(task.getWorkflowAttributes()) //
//							.withAdvanceStatus(task.isWorkflowAdvanceable()) //
//							.build() //
//							.execute();
//				});
//	}
//	private Predicate<Email> workflowActive(final ReadEmailTask task) {
//		return (final Email input) -> task.isWorkflowActive();
//	}
//
//	private Predicate<Email> filter(final ReadEmailTask task) {
//		final Predicate<Email> output;
//		final String value = task.getFilterType();
//		if (REGEX.equalsIgnoreCase(value)) {
//			output = addressAndSubjectRespectFilter(task);
//		} else if (FUNCTION.equalsIgnoreCase(value)) {
//			output = functionFilter(task);
//		} else if (NONE.equalsIgnoreCase(value)) {
//			output = alwaysTrue();
//		} else {
//			logger.warn("filter type '{}' is not expected, ignoring it", value);
//			output = alwaysTrue();
//		}
//		return output;
//	}
//
//	private Predicate<Email> addressAndSubjectRespectFilter(final ReadEmailTask task) {
//		logger.debug("creating main filter for email");
//		return and(fromAddressRespectsFilter(task), subjectRespectsFilter(task));
//	}
//
//	private Predicate<Email> fromAddressRespectsFilter(final ReadEmailTask task) {
//		return (final Email email) -> {
//			logger.debug("checking from address");
//			if (isEmpty(task.getRegexFromFilter())) {
//				logger.debug("no from address filters");
//				return true;
//			}
//			for (final String regex : task.getRegexFromFilter()) {
//				final Pattern fromPattern = Pattern.compile(regex);
//				final Matcher fromMatcher = fromPattern.matcher(email.getFromAddress());
//				if (fromMatcher.matches()) {
//					logger.debug("from address matches regex '{}'", regex);
//					return true;
//				}
//			}
//			logger.debug("from address not matching");
//			return false;
//		};
//	}
//
//	private Predicate<Email> subjectRespectsFilter(final ReadEmailTask task) {
//		return (final Email email) -> {
//			logger.debug("checking subject");
//			if (isEmpty(task.getRegexSubjectFilter())) {
//				logger.debug("no subject filters");
//				return true;
//			}
//			for (final String regex : task.getRegexSubjectFilter()) {
//				final Pattern subjectPattern = Pattern.compile(regex);
//				final Matcher subjectMatcher = subjectPattern.matcher(email.getSubject());
//				if (subjectMatcher.matches()) {
//					logger.debug("subject matches regex '{}'", regex);
//					return true;
//				}
//			}
//			logger.debug("subject not matching");
//			return false;
//		};
//	}
//
//	private Predicate<Email> functionFilter(final ReadEmailTask task) {
//		final String name = task.getFilterFunction();
//		logger.debug("creating filter for function '{}'", name);
//		final StoredFunction function = checkNotNull(dataView.findFunctionByName(name), "missing function '%s'", name);
//		final Map<String, StoredFunctionParameter> map = Maps.uniqueIndex(function.getInputParameters(), (final StoredFunctionParameter input) -> input.getName());
//		for (final String element : asList(FROM_ADDRESS, TO_ADDRESSES, CC_ADDRESSES, SUBJECT, CONTENT)) {
//			checkArgument(map.containsKey(element), "missing input parameter '%s'", element);
//			final CardAttributeType<?> type = map.get(element).getType();
//			checkArgument(type instanceof TextAttributeType, "invalid type '%s' for input parameter '%s' ", type,
//					element);
//		}
//		final Iterable<StoredFunctionOutputParameter> outputParameters = function.getOutputParameters();
//		checkArgument(size(outputParameters) == 1, "output parameter must be one");
//		final StoredFunctionParameter outputParameter = get(outputParameters, 0);
//		checkArgument(outputParameter.getType() instanceof BooleanAttributeType, "output parameter must be boolean");
//		return (email) -> {
//			try {
//				return toBooleanOrDefault(functionService.callFunction(function, map(
//						FROM_ADDRESS, email.getFromAddress(),
//						TO_ADDRESSES, Joiner.on(",").skipNulls().join(email.getToAddresses()),
//						CC_ADDRESSES, Joiner.on(",").skipNulls().join(email.getCcAddresses()),
//						SUBJECT, email.getSubject(),
//						CONTENT, email.getContent()
//				)).get(outputParameter.getName()), false);
//			} catch (Exception e) {
//				logger.error("error calling function", e);
//				return false;
//			}
//		};
//	}
//
//	private Iterable<Document> documentsFrom(final Iterable<Attachment> attachments) {
//		return from(attachments) //
//				.transform((final Attachment input) -> new Document() {
//
//			@Override
//			public String getName() {
//				return input.getName();
//			}
//
//			@Override
//			public DataHandler getDataHandler() {
//				return input.getDataHandler();
//			}
//		});
//	}

//	private static class ConditionalAction implements Action {
//
//		private final Predicate<Email> predicate;
//		private final Action delegate;
//
//		public ConditionalAction(final Predicate<Email> predicate, final Action delegate) {
//			this.predicate = predicate;
//			this.delegate = delegate;
//		}
//
//		@Override
//		public void execute(final Email email, final Storable storable) {
//			if (predicate.apply(email)) {
//				delegate.execute(email, storable);
//			}
//		}
//
//	}
}
