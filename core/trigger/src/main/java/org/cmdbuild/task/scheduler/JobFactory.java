package org.cmdbuild.task.scheduler;

import org.cmdbuild.logic.taskmanager.ScheduledTask;
import org.cmdbuild.scheduler.JobWithTask;

public interface JobFactory<T extends ScheduledTask> {

	JobWithTask create(ScheduledTask task, boolean execution);

}