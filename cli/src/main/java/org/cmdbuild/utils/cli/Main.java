/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.cli;

import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.ext.spring.LogbackConfigurer;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.uniqueIndex;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import static org.apache.commons.lang3.StringUtils.trim;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.utils.cli.commands.CryptoCommandRunner;
import org.cmdbuild.utils.cli.commands.DbconfigCommandRunner;
import org.cmdbuild.utils.cli.commands.InstallCommandRunner;
import org.cmdbuild.utils.cli.commands.PostgresCommandRunner;
import org.cmdbuild.utils.cli.commands.RestCommandRunner;
import org.cmdbuild.utils.cli.commands.RiverCommandRunner;
import org.cmdbuild.utils.cli.commands.SoapCommandRunner;
import org.cmdbuild.utils.cli.commands.TomcatCommandRunner;
import org.cmdbuild.utils.cli.commands.UninstallCommandRunner;
import org.cmdbuild.utils.cli.commands.UpgradeCommandRunner;
import static org.cmdbuild.utils.hash.CmdbuildHashUtils.hash;

/**
 *
 * @author davide
 */
public class Main {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static File warFile;

	public static void setWarFile(File warFile) {
		Main.warFile = warFile;
	}

	public static File getWarFile() {
		return warFile;
	}

	public static boolean isRunningFromWebappDir() {
		return getWarFile() != null && getWarFile().isDirectory();
	}

	public static boolean isRunningFromWarFile() {
		return !isRunningFromWebappDir();
	}

	public static String getExecNameForHelpMessage() {
		return "java -jar " + getWarFile().getName();
	}

	public static void main(String[] args) throws Exception {
		new Main().runMain(args);
	}

	private final Map<String, CliCommandRunner> commandRunners = uniqueIndex(asList(
			new PostgresCommandRunner(),
			new TomcatCommandRunner(),
			new DbconfigCommandRunner(),
			new CryptoCommandRunner(),
			new SoapCommandRunner(),
			new InstallCommandRunner(),
			new UpgradeCommandRunner(),
			new UninstallCommandRunner(),
			new RestCommandRunner(),
			new RiverCommandRunner()), CliCommandRunner::getName);

	private void runMain(String[] args) throws Exception {
		enableDefaultLogging();
		CliSessionVariables.getInstance().configure(new File(System.getProperty("java.io.tmpdir"), "cli_session_vars_" + hash(getWarFile().getAbsolutePath(), 8) + ".properties"));//TODO use war file fingerprint + current date
		try {
			List<String> argList = newArrayList(args);
			boolean checkTime = false, printHelp = args.length == 0;
			for (Iterator<String> iterator = argList.iterator(); iterator.hasNext();) {
				String key = iterator.next();
				if (key.toLowerCase().matches("-+(v|verbose)")) {
					iterator.remove();
					enableVerboseLogging();
				} else if (key.toLowerCase().matches("-+(t|time)")) {
					iterator.remove();
					checkTime = true;
				} else if (key.toLowerCase().matches("-+(h|help)")) {
					iterator.remove();
					printHelp = true;
				}
			}
			logger.debug("running from war {} = {}", getWarFile().isFile() ? "file" : "dir", getWarFile().getCanonicalPath());
			if (printHelp) {
				if (argList.isEmpty()) {
					printHelp();
					return;
				} else {
					argList.add("-h");
				}
			}
			args = argList.toArray(new String[]{});
			CliCommandRunner cliService = commandRunners.get(trim(args[0]).toLowerCase());
			checkNotNull(cliService, "%s is not a valid cli mode", args[0]);
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();
			cliService.exec(Arrays.copyOfRange(args, 1, args.length));
			stopWatch.stop();
			if (checkTime) {
				System.err.printf("\ntotal time: %.3fs\n", stopWatch.getTime() / 1000d);
			}

		} catch (IllegalArgumentException ex) {
			logger.debug("error", ex);
			System.err.println("ERROR: " + ex.getMessage());
			printHelp();
			System.exit(1);
		} finally {
			CliSessionVariables.getInstance().store();
		}
	}

	private void printHelp() {
		Options options = new Options();
		options.addOption("h", "help", false, "print help");
		options.addOption("t", "time", false, "print total operation time");
		options.addOption("v", "verbose", false, "verbose output (enable debug logs)");
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(getExecNameForHelpMessage(), options, true);
		for (CliCommandRunner cliService : commandRunners.values()) {
			System.out.println("\t\t" + cliService.getName() + "\t" + cliService.getDescription());
		}
	}

	private void enableDefaultLogging() throws FileNotFoundException, JoranException {
		LogbackConfigurer.initLogging("classpath:cli_logback_default.xml");
	}

	private void enableVerboseLogging() throws FileNotFoundException, JoranException {
		LogbackConfigurer.initLogging("classpath:cli_logback_verbose.xml");
	}

}
