/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.cli.commands;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import static java.lang.String.format;
import jline.console.ConsoleReader;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.cmdbuild.utils.cli.Main.getWarFile;
import static org.cmdbuild.utils.date.DateUtils.dateTimeFileSuffix;
import static org.cmdbuild.utils.tomcatmanager.TomcatManagerUtils.execSafe;

public class UpgradeCommandRunner extends AbstractCommandRunner {

	public UpgradeCommandRunner() {
		super("upgrade", "upgrade cmdbuild webapp in existing tomcat instance");
	}

	@Override
	protected Options buildOptions() {
		Options options = super.buildOptions();
		options.addOption("d", "dir", true, "tomcat install dir");
		return options;
	}

	@Override
	protected void exec(CommandLine cmd) throws Exception {
		String locationString = trimToNull(cmd.hasOption("d") ? cmd.getOptionValue("d") : (cmd.getArgList().isEmpty() ? null : cmd.getArgList().get(0)));
		checkNotNull(locationString, "must set location of tomcat instance to upgrade");
		File tomcatLocation = new File(locationString);
		checkArgument(tomcatLocation.isDirectory() && new File(tomcatLocation, "conf/server.xml").isFile(), "cannot find existing tomcat at location %s", locationString);
		System.out.println("\n upgrade tomcat instance at " + tomcatLocation.getAbsolutePath() + "; are you sure?");
		ConsoleReader consoleReader = new ConsoleReader();
		consoleReader.readLine();
		System.out.println("stop tomcat (if running)");
		Runtime.getRuntime().exec(new String[]{tomcatLocation.getAbsolutePath() + "/bin/shutdown.sh", "-force"}).waitFor();

		String backupFile = format("cmdbuild_backup_%s.tar.gz", dateTimeFileSuffix());
		System.out.printf("backup old cmdbuild to webapps/%s\n", backupFile);
		execSafe(new File(tomcatLocation, "webapps"), "/bin/bash", "-c", format("tar -czf '%s' cmdbuild && rm -rf cmdbuild", backupFile));

		System.out.printf("unpack new cmdbuild webapp from file = %s\n", getWarFile().getAbsolutePath());
		execSafe(new File(tomcatLocation, "webapps"), "/bin/bash", "-c", format("mkdir cmdbuild && unzip -q '%s' -d cmdbuild", getWarFile().getAbsolutePath()));

		System.out.println("done!");
	}

}
