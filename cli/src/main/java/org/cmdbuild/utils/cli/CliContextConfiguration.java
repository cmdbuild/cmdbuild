package org.cmdbuild.utils.cli;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.cmdbuild.auth.multitenant.config.MultitenantConfiguration;
import static org.cmdbuild.auth.multitenant.config.SimpleMultitenantConfiguration.multitenantDisabled;
import org.cmdbuild.dao.ConfigurableDataSource;

import org.cmdbuild.dao.config.inner.FilesStoreRepository;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.dao.config.inner.DatabaseCreatorConfigImpl;
import org.cmdbuild.dao.datasource.DummyConfigurableDataSource;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempDir;
import static org.cmdbuild.utils.io.CmdbuildZipUtils.unzipToDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.cmdbuild.dao.config.inner.PatchFileRepository;

@ImportResource({"classpath:cli-application-context.xml"})
public class CliContextConfiguration {

	public static final String MULTITENANT_CONFIGURATION = "multitenantConfiguration";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private DatabaseCreator databaseConfigurator;
	private final File tempDir;
	private File sqlPatchesFile;

	public CliContextConfiguration() {
		tempDir = tempDir("sqlsourcesforcmd_");
	}

	@PostConstruct
	public void init() throws IOException, ZipException {
		if (Main.getWarFile().isDirectory()) {
			sqlPatchesFile = new File(Main.getWarFile(), "WEB-INF/patches/");
		} else {
			unzipToDir(Main.getWarFile(), tempDir);//TODO unzip only sql subpath
			sqlPatchesFile = new File(tempDir, "WEB-INF/patches/");
		}
		checkArgument(new File(sqlPatchesFile, "../sql/sample_schemas/demo_schema.sql").isFile());
		databaseConfigurator = new DatabaseCreator(DatabaseCreatorConfigImpl.builder().withSqlPath(new File(sqlPatchesFile, "../sql").getAbsolutePath()).build());
	}

	@PreDestroy
	public void cleanup() {
		FileUtils.deleteQuietly(tempDir);
	}

	@Bean
	public PatchFileRepository getPatchRepository() {
		return new FilesStoreRepository(checkNotNull(sqlPatchesFile), null);
	}

	@Bean
	public DatabaseCreator getDatabaseConfigurator() {
		return checkNotNull(databaseConfigurator);
	}

//	@Bean(MULTITENANT_CONFIGURATION)
//	public MultitenantConfiguration getMultitenantConfiguration() {
//		return multitenantDisabled();
//	}

	@Bean
	public ConfigurableDataSource getDataSource() {
		return new DummyConfigurableDataSource(() -> getDatabaseConfigurator().cmdbuildDataSource());
	}

}
