/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.cli.utils;

import java.io.File;
import java.io.FileInputStream;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.dao.config.inner.DatabaseCreatorConfig;
import org.cmdbuild.dao.config.inner.DatabaseCreatorConfigImpl;
import org.cmdbuild.dao.config.inner.PatchManager;
import org.cmdbuild.spring.utils.SpringContextInitializer;
import org.cmdbuild.utils.cli.CliContextConfiguration;

public class DatabaseUtils {

	public static void dropDatabase(File configFile) throws Exception {
		DatabaseCreator databaseConfigurator = new DatabaseCreator(DatabaseCreatorConfigImpl.builder().withConfig(new FileInputStream(configFile)).build());
		System.err.println("dropping database " + databaseConfigurator.getConfig().getDatabaseName());
		databaseConfigurator.dropDatabase();
		System.err.println("done");
	}

	public static void createDatabase(DatabaseCreatorConfig databaseCreatorConfig) throws Exception {
		createDatabase(databaseCreatorConfig, true);
	}

	public static void createDatabase(DatabaseCreatorConfig databaseCreatorConfig, boolean applyPatches) throws Exception {
		SpringContextInitializer springContextInitializer = new SpringContextInitializer(CliContextConfiguration.class);
		springContextInitializer.init();
		try {

			DatabaseCreator databaseCreator = springContextInitializer.getBean(DatabaseCreator.class);
			databaseCreator.setConfig(DatabaseCreatorConfigImpl.copyOf(databaseCreatorConfig)
					.withSqlPath(databaseCreator.getConfig().getSqlPath())
					.withUseSharkSchema(true)//TODO check if source file contains shark schema, auto enable if required
					.build());

			System.err.println("create database " + databaseCreator.getConfig().getDatabaseName() + " " + databaseCreator.getConfig().getDatabaseType());
			databaseCreator.configureDatabase();
			if (applyPatches) {
				springContextInitializer.getBean(PatchManager.class).reloadAndApplyPendingPatches();
			}
			System.err.println("done");
		} finally {
			springContextInitializer.cleanup();
		}
	}
}
