/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.cli.commands;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Iterables.getFirst;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FileUtils;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.dao.config.inner.DatabaseCreatorConfigImpl;
import org.cmdbuild.utils.cli.utils.DatabaseUtils;
import static org.cmdbuild.utils.cli.utils.DatabaseUtils.dropDatabase;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempFile;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.readToString;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;

public class DbconfigCommandRunner extends AbstractCommandRunner {

	public DbconfigCommandRunner() {
		super("dbconfig", "configure cmdbuild database");
	}

	@Override
	protected Options buildOptions() {
		Options options = super.buildOptions();
		options.addOption("configfile", true, "cmdbuild database config file (es: database.conf); default to conf/cmdbuild/database.conf");
//		options.addOption("dbtype", true, "cmdbuild database type (es: 'empty','demo',...)");
		options.addOption("skippatches", false, "skip patches (do not apply patches)");
		return options;
	}

	@Override
	protected void printAdditionalHelp() {
		System.out.println("\nactions:\n\tdrop: drop database\n\tcreate <database type|dump to import>: create database\n\trecreate <database type|dump to import>: drop database, then create database");
		System.out.println("\nconfig file example:\n");
		System.out.println(readToString(getClass().getResourceAsStream("/database.conf_cli_example")));
	}

	@Override
	protected void exec(CommandLine cmd) throws Exception {
		String action = getFirst(cmd.getArgList(), "");
		if (isBlank(action)) {
			System.err.println("no action supplied!");
		} else {
			File configFile = new File("conf/cmdbuild/database.conf");
			if (!configFile.isFile() || cmd.hasOption("configfile")) {
				configFile = getFile(cmd, "configfile", true, "must set valid 'configfile'");
			}
			switch (action.trim().toLowerCase()) {
				case "drop":
					dropDatabase(configFile);
					break;
				case "create":
					createDatabase(cmd, configFile);
					break;
				case "recreate":
					dropDatabase(configFile);
					createDatabase(cmd, configFile);
					break;
				default:
					throw new IllegalArgumentException("unknown action: " + action);
			}
		}
	}

	private void createDatabase(CommandLine cmd, File configFile) throws Exception {
		String databaseType = trimAndCheckNotBlank(cmd.getArgList().stream().skip(1).findFirst().orElse(null), "must set non-null 'dbtype' (es: 'empty','demo',...)");

		File toDelete = null;

		if (databaseType.endsWith(".zip")) {
			databaseType = extractDatabaseFromZipFile(databaseType);
			toDelete = new File(databaseType);
		}

		try {
			DatabaseUtils.createDatabase(DatabaseCreatorConfigImpl.builder().withDatabaseType(databaseType).withConfig(new FileInputStream(configFile)).build(), !cmd.hasOption("skippatches"));
		} finally {
			deleteQuietly(toDelete);
		}
	}

	private String extractDatabaseFromZipFile(String zipFileName) {
		File file = new File(zipFileName);
		checkArgument(file.isFile(), "invalid zip file = %s", zipFileName);
		try (ZipFile zipFile = new ZipFile(file)) {
			Optional<? extends ZipEntry> entry = zipFile.stream().filter((e) -> e.getName().endsWith(".backup")).findAny();
			checkArgument(entry.isPresent(), "database backup not found in zip file = %s", file.getAbsolutePath());
			logger.info("selected database backup file = {}", entry.get().getName());
			File dump = tempFile(null, "backup");
			FileUtils.copyInputStreamToFile(zipFile.getInputStream(entry.get()), dump);
			return dump.getAbsolutePath();
		} catch (IOException ex) {
			throw runtime(ex);
		}
	}

}
