/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.cli;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Properties;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CliSessionVariables {

	private static final CliSessionVariables INSTANCE = new CliSessionVariables();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private File file;
	private Map<String, String> map;

	public static CliSessionVariables getInstance() {
		return INSTANCE;
	}

	private CliSessionVariables() {
	}

	public void configure(File file) {
		this.file = checkNotNull(file);
		Properties properties = new Properties();
		if (file.exists()) {
			try {
				try (FileInputStream in = new FileInputStream(file)) {
					properties.load(in);
				}
			} catch (Exception ex) {
				logger.error("error reading cli session variables", ex);
			}
		}
		map = map(properties);
	}

	public void store() {
		Properties properties = new Properties();
		properties.putAll(map);
		try {
			try (FileOutputStream out = new FileOutputStream(file)) {
				properties.store(out, null);
			}
		} catch (Exception ex) {
			logger.error("error writing cli session variables", ex);
		}
	}

	public Map<String, String> get() {
		return map;
	}

}
