/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.cli.utils;

import java.io.File;
import java.io.IOException;
import static java.lang.String.format;
import static java.lang.Thread.sleep;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.io.FileUtils.touch;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.readToString;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempFile;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.writeToFile;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class FileEditor {

	public static String editFile(String content, String prefix, String ext) {
		File file = tempFile(checkNotBlank(prefix), "." + checkNotBlank(ext));
		File lock = tempFile(checkNotBlank(prefix), ".lock");
		try {
			writeToFile(file, content);
			touch(lock);

			Process process = new ProcessBuilder("x-terminal-emulator", "-e", format("bash -c 'vim %s && rm %s'", file.getAbsolutePath(), lock.getAbsolutePath())).start();
			process.waitFor();

			while (lock.exists()) {
				sleep(100);
			}

			content = readToString(file);
			return content;
		} catch (IOException | InterruptedException ex) {
			throw runtime(ex);
		} finally {
			deleteQuietly(file);
			deleteQuietly(lock);
		}
	}

}
