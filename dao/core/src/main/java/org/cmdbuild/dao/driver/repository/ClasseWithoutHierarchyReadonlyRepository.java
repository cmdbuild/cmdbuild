/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.repository;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.ClasseWithoutHierarchy;

public interface ClasseWithoutHierarchyReadonlyRepository<T extends ClasseWithoutHierarchy> {

	List<T> getAllClasses();

	@Nullable
	T getClasseOrNull(long oid);

	@Nullable
	T getClasseOrNull(String name);

	default T getClasse(long oid) {
		return checkNotNull(getClasseOrNull(oid), "classe not found for oid = %s", oid);
	}

	default T getClasse(String name) {
		return checkNotNull(getClasseOrNull(name), "classe not found for name = %s", name);
	}

}
