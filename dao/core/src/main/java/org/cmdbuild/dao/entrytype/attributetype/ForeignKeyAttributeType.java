package org.cmdbuild.dao.entrytype.attributetype;

import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.EntryType;

public class ForeignKeyAttributeType extends AbstractReferenceAttributeType {

	private final String targetClassName;

	public ForeignKeyAttributeType(@Nullable String destinationClassName) {
		this.targetClassName = destinationClassName;
	}

	public ForeignKeyAttributeType(EntryType entryType) {
		this(entryType.getName());
	}

	@Override
	public void accept(CMAttributeTypeVisitor visitor) {
		visitor.visit(this);
	}

	public String getForeignKeyDestinationClassName() {
		return targetClassName;
	}

	@Override
	public AttributeTypeName getName() {
		return AttributeTypeName.FOREIGNKEY;
	}

}
