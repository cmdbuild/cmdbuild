package org.cmdbuild.dao.entrytype;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.not;
import static java.lang.String.format;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_BEGINDATE;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ENDDATE;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS1;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS2;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDDOMAIN;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ1;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ2;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_STATUS;
import static org.cmdbuild.dao.entrytype.EntryType.EntryTypeType.DOMAIN;
import org.cmdbuild.dao.entrytype.attributetype.DateTimeAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.LongAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.RegclassAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.StringAttributeType;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotEmpty;

public interface Domain extends EntryType, ClassPermissions, DomainDefinition {

	static final String DOMAIN_MANY_TO_ONE = "N:1",
			DOMAIN_ONE_TO_MANY = "1:N";

	@Override
	default void accept(CMEntryTypeVisitor visitor) {
		visitor.visit(this);
	}

	default String getDirectDescription() {
		return getMetadata().getDirectDescription();
	}

	default String getInverseDescription() {
		return getMetadata().getInverseDescription();
	}

	default String getCardinality() {
		return getMetadata().getCardinality();
	}

	default boolean isMasterDetail() {
		return getMetadata().isMasterDetail();
	}

	default String getMasterDetailDescription() {
		return getMetadata().getMasterDetailDescription();
	}

	default String getMasterDetailFilter() {
		return getMetadata().getMasterDetailFilter();
	}

	@Override
	default boolean hasHistory() {
		return true;
	}

	default Collection<String> getDisabledSourceDescendants() {
		return getMetadata().getDisabledSourceDescendants();
	}

	default Collection<String> getDisabledTargetDescendants() {
		return getMetadata().getDisabledTargetDescendants();
	}

	default Collection<Classe> getSourceClasses() {
		return getSourceClass().getDescendantsAndSelf().stream().filter(not(this::isDisabledSourceDescendant)).collect(toList());
	}

	default Collection<Classe> getTargetClasses() {
		return getTargetClass().getDescendantsAndSelf().stream().filter(not(this::isDisabledTargetDescendant)).collect(toList());
	}

	default boolean isDisabledSourceDescendant(Classe classe) {
		return getDisabledSourceDescendants().contains(classe.getName());
	}

	default boolean isDisabledTargetDescendant(Classe classe) {
		return getDisabledTargetDescendants().contains(classe.getName());
	}

	/**
	 * return optional ordering for domains, on the side of {@link #getSourceClass()}
	 * (used to order instances of {@link #getSourceClass()} as seen in the detail
	 * (relations) view of a single instance of {@link #getTargetClass()})
	 *
	 * @return ordering, or {@link DEFAULT_INDEX_VALUE} if not set
	 */
	default int getIndexForSource() {
		return getMetadata().getIndexForSource();
	}

	/**
	 * return optional ordering for domains, on the side of {@link #getTargetClass()}
	 * (used to order instances of {@link #getTargetClass()} as seen in the detail
	 * (relations) view of a single instance of {@link #getSourceClass()})
	 *
	 * @return ordering, or {@link DEFAULT_INDEX_VALUE} if not set
	 */
	default int getIndexForTarget() {
		return getMetadata().getIndexForTarget();
	}

	@Override
	default Long getOid() {
		return getId();
	}

	static int DEFAULT_INDEX_VALUE = -1;

	@Override
	default String getPrivilegeId() {
		return String.format("Domain:%d", getId());
	}

	@Override
	default EntryTypeType getEtType() {
		return DOMAIN;
	}

	@Override
	default boolean isActive() {
		return EntryType.super.isActive();
	}

	@Override
	default String getDescription() {
		return EntryType.super.getDescription();
	}

	default Classe getReferenceTarget() {
		switch (getCardinality()) {
			case DOMAIN_MANY_TO_ONE:
				return getTargetClass();
			case DOMAIN_ONE_TO_MANY:
				return getSourceClass();
			default:
				throw new IllegalStateException(format("cannot get reference target for domain = %s with cardinality = %s", getName(), getCardinality()));
		}
	}

	default Classe getReferencedClass(Attribute attribute) {
		if (attribute.getOwnerClass().equalToOrHasAncestor(getTargetClass())) {
			return getSourceClass();
		} else {
			checkArgument(attribute.getOwnerClass().equalToOrHasAncestor(getSourceClass()));
			return getTargetClass();
		}
	}

	default boolean isDomainForClasse(Classe classe) {
		return isDomainForSourceClasse(classe) || isDomainForTargetClasse(classe);
	}

	default boolean isDomainForSourceClasse(Classe classe) {
		return getSourceClass().isAncestorOf(classe) && !getDisabledSourceDescendants().contains(classe.getName());
	}

	default boolean isDomainForTargetClasse(Classe classe) {
		return getTargetClass().isAncestorOf(classe) && !getDisabledTargetDescendants().contains(classe.getName());
	}

	/**
	 * @return reoriented domain for this classe (so that this classe is on the 'source' side); if this classe is both a valid source and target, return two records (one for each direction); otherwise return one record
	 */
	default List<Domain> getThisDomainDirectAndOrReversedForClass(Classe classe) {
		List<Domain> list = list();
		if (isDomainForSourceClasse(classe)) {
			list.add(this);
		}
		if (isDomainForTargetClasse(classe)) {
			list.add(ReverseDomain.of(this));
		}
		return checkNotEmpty(list, "this domain = %s is not a valid domain for class = %s", this, classe);
	}

	@Override
	@Nullable
	default Attribute getAttributeOrNull(String name) {//TODO duplcate code with class, refactor-merge
		Attribute attr = EntryType.super.getAttributeOrNull(name);
		if (attr == null) {
			switch (name) {
				case ATTR_ID:
					return new SystemAttributeImpl(name, this, LongAttributeType.INSTANCE, false);
				case ATTR_IDDOMAIN:
					return new SystemAttributeImpl(name, this, RegclassAttributeType.INSTANCE, false);
				case ATTR_BEGINDATE:
					return new SystemAttributeImpl(name, this, new DateTimeAttributeType(), false);
				case ATTR_ENDDATE:
					return new SystemAttributeImpl(name, this, new DateTimeAttributeType(), false);
				case ATTR_STATUS:
					return new SystemAttributeImpl(name, this, new StringAttributeType(), false);
				case ATTR_IDCLASS1:
				case ATTR_IDCLASS2:
					return new SystemAttributeImpl(name, this, RegclassAttributeType.INSTANCE, false);
				case ATTR_IDOBJ1:
				case ATTR_IDOBJ2:
					return new SystemAttributeImpl(name, this, LongAttributeType.INSTANCE, false);
			}
		}
		return attr;
	}
}
