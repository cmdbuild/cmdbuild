/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.beans;

import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

public enum NullAttributeMetadata implements CardAttributeType.Meta {
	INSTANCE;

	@Override
	public boolean isLookup() {
		return false;
	}

	@Override
	public String getLookupType() {
		return null;
	}

	@Override
	public boolean isReference() {
		return false;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public boolean isForeignKey() {
		return false;
	}

	@Override
	public String getForeignKeyDestinationClassName() {
		return null;
	}
}
