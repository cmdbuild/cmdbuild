/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao;

import com.google.common.eventbus.EventBus;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 */
public interface ConfigurableDataSource extends DataSource {

	EventBus getEventBus();

	boolean isConfigured();

	BasicDataSource getInner();

	enum DatasourceConfiguredEvent {
		INSTANCE
	}
}
