package org.cmdbuild.dao.entrytype.attributetype;

import java.util.NoSuchElementException;

public class IpAddressAttributeType implements CardAttributeType<String> {

	public static enum IpType {
		IPV4, IPV6, ANY;

		/**
		 * Returns the enum constant with the specified name (case-insensitive).
		 *
		 * @throws IllegalArgumentException if no enum corresponds with the
		 * specified name
		 */
		public static IpType of(final String name) {
			return of(name, null);
		}

		/**
		 * Returns the enum constant with the specified name (case-insensitive).
		 *
		 * @param name
		 * @param defaultValue
		 *
		 * @throws IllegalArgumentException if no enum corresponds with the
		 * specified name and no default value has been specified.
		 */
		public static IpType of(final String name, final IpType defaultValue) {
			for (final IpType value : values()) {
				if (value.name().toLowerCase().equals(name)) {
					return value;
				}
			}
			if (defaultValue == null) {
				throw new NoSuchElementException(name);
			}
			return defaultValue;
		}

	}

	private final IpType type;

	public IpAddressAttributeType(final IpType type) {
		this.type = type;
	}

	public IpType getType() {
		return type;
	}

	@Override
	public void accept(final CMAttributeTypeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AttributeTypeName getName() {
		return AttributeTypeName.INET;
	}

}
