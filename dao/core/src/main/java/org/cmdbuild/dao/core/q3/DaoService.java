/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.core.q3;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nullable;
import org.cmdbuild.dao.beans.CMRelation;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardIdAndClassName;
import org.cmdbuild.dao.beans.RelationImpl;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ1;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ2;
import static org.cmdbuild.dao.core.q3.QueryBuilder.EQ;
import org.cmdbuild.dao.driver.repository.AttributeRepository;
import org.cmdbuild.dao.driver.repository.ClasseReadonlyRepository;
import org.cmdbuild.dao.driver.repository.DomainRepository;
import org.cmdbuild.dao.driver.repository.FunctionRepository;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.function.StoredFunction;
import org.cmdbuild.dao.function.StoredFunctionOutputParameter;
import org.springframework.jdbc.core.JdbcTemplate;

public interface DaoService extends QueryBuilderService, AttributeRepository, ClasseReadonlyRepository, FunctionRepository, DomainRepository {

	static final String ALL = "*", COUNT = "card_count", ROW_NUMBER = "row_number";

	<T> T create(T model);

	<T> T update(T model);

	<T> long createOnly(T model);

	<T> void updateOnly(T model);

	long createOnly(Card card);

	void updateOnly(Card card);

	void delete(Class model, long cardId);

	Card create(Card card);

	Card update(Card card);

	default CMRelation getRelation(String domainId, long relationId) {
		return getRelation(getDomain(domainId), relationId);
	}

	default CMRelation getRelation(Domain domain, long relationId) {
		return selectAll().from(domain).where(ATTR_ID, EQ, relationId).getRelation();
	}

	CMRelation create(CMRelation relation);

	CMRelation update(CMRelation relation);

	void delete(CMRelation relation);

	default CMRelation getRelation(String domain, long sourceId, long targetId) {
		return selectAll().from(getDomain(domain)).where(ATTR_IDOBJ1, EQ, sourceId).where(ATTR_IDOBJ2, EQ, targetId).getRelation();
	}

	default void deleteRelation(String domain, long sourceId, long targetId) {
		delete(getRelation(domain, sourceId, targetId));
	}

	default CMRelation createRelation(String domain, CardIdAndClassName source, CardIdAndClassName target) {
		return create(RelationImpl.builder().withType(getDomain(domain)).withSourceCard(source).withTargetCard(target).build());
	}

	JdbcTemplate getJdbcTemplate();

	default QueryBuilder selectAll() {
		return select(ALL);
	}

	default QueryBuilder selectCount() {
		return query().selectCount();
	}

	default QueryBuilder.RowNumberQueryBuilder selectRowNumber() {
		return query().selectRowNumber();
	}

	default @Nullable
	ResultRow getByIdOrNull(Class model, long cardId) {
		return getOnlyElement(selectAll().from(model).where(ATTR_ID, EQ, cardId).run(), null);
	}

	default ResultRow getById(Classe classe, long cardId) {
		return getOnlyElement(selectAll().from(classe).where(ATTR_ID, EQ, cardId).run(), null);
	}

	default ResultRow getById(Class model, long cardId) {
		return checkNotNull(getByIdOrNull(model, cardId));
	}

	default ResultRow getById(String classId, long cardId) {
		return getById(getClasse(classId), cardId);
	}

	default QueryBuilder select(Collection<String> attrs) {
		return query().select(attrs);
	}

	default QueryBuilder select(String... attrs) {
		return query().select(attrs);
	}

	Domain getDomain(String domainId);

	default Card getCardOrNull(Classe classe, long cardId) {
		return selectAll().from(classe).where(ATTR_ID, WhereOperator.EQ, cardId).getCardOrNull();
	}

	default Card getCard(Classe thisClass, long cardId) {
		return checkNotNull(getCardOrNull(thisClass, cardId), "card not found for class = %s cardId = %s", thisClass, cardId);
	}

	default Card getCard(String classId, long cardId) {
		return getCard(getClasse(classId), cardId);
	}

	void delete(Card card);

	void delete(Object model);

	default void delete(Classe classe, long cardId) {
		delete(getCard(classe, cardId));
	}

	default void delete(String classId, long cardId) {
		delete(getCard(getClasse(classId), cardId));
	}

	List<CMRelation> getRelationsInfoForCard(CardIdAndClassName card);

	PreparedQuery selectFunction(StoredFunction function, List<Object> input, List<StoredFunctionOutputParameter> outputParameters);

	default PreparedQuery callFunction(StoredFunction function, List input) {
		return selectFunction(function, input, function.getOutputParameters());
	}

}
