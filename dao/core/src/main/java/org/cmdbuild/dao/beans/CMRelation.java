package org.cmdbuild.dao.beans;

import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.ReverseDomain;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;

/**
 * Immutable relation.
 */
public interface CMRelation extends DatabaseRecord {

	static String ATTR_DESCRIPTION1 = "_description1", ATTR_DESCRIPTION2 = "_description2";
	static String ATTR_CODE1 = "_code1", ATTR_CODE2 = "_code2";

	@Override
	Domain getType();

	Long getSourceId();

	Long getTargetId();

	default CardIdAndClassName getSourceCard() {
		throw new UnsupportedOperationException("TODO");
	}

	default CardIdAndClassName getTargetCard() {
		throw new UnsupportedOperationException("TODO");
	}

	default Direction getDirection() {
		throw new UnsupportedOperationException("TODO");
	}

	default @Nullable
	String getSourceDescription() {
		return getString(ATTR_DESCRIPTION1);
	}

	default @Nullable
	String getTargetDescription() {
		return getString(ATTR_DESCRIPTION2);
	}

	default @Nullable
	String getSourceCode() {
		return getString(ATTR_CODE1);
	}

	default @Nullable
	String getTargetCode() {
		return getString(ATTR_CODE2);
	}

	default Domain getDomainWithThisRelationDirection() {
		switch (getDirection()) {
			case DIRECT:
				return getType();
			case INVERSE:
				return ReverseDomain.of(getType());
			default:
				throw unsupported("unsupported direction = %s", getDirection());
		}
	}

	enum Direction {
		DIRECT, INVERSE
	}

}
