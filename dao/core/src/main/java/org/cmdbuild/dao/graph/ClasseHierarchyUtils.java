/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.graph;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.toImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import javax.annotation.Nullable;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.ClasseImpl;
import org.cmdbuild.dao.entrytype.ClasseWithoutHierarchy;
import static org.cmdbuild.dao.graph.ClasseHierarchy.isLeaf;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.multimap;

public class ClasseHierarchyUtils {

	public static List<Classe> attachClassHierarchy(List<ClasseWithoutHierarchy> classes) {
		return new ClasseHierarchyHelper(classes).buildClassHierarchy();
	}

	private static ClasseGraph buildGraph(List<ClasseWithoutHierarchy> sourceClasses) {
		List<ClasseHierarchyInfo> classes = sourceClasses.stream().map((classe) -> new ClasseHierarchyInfo(classe.getOid(), classe.getParentOidOrNull())).collect(toList());
		return new ClasseGraph(classes);
	}

	private static class ClasseHierarchyHelper {

		private final ClasseGraph graph;

		private final Map<Long, ClasseWithoutHierarchy> toProcess;

		private final Map<Long, Classe> classes = map();
		private final List<Pair<ClasseWithoutHierarchy, ClasseHierarchyImpl>> hierachyList = list();

		public ClasseHierarchyHelper(List<ClasseWithoutHierarchy> sourceClasses) {
			this.graph = buildGraph(sourceClasses);
			toProcess = sourceClasses.stream().collect(toMap(ClasseWithoutHierarchy::getId, identity()));
		}

		public List<Classe> buildClassHierarchy() {

			while (!toProcess.isEmpty()) {
				processClass(toProcess.keySet().iterator().next());
			}

			completeHierarchy();

			return classes.values().stream().sorted(Ordering.natural().onResultOf(Classe::getName)).collect(toImmutableList());
		}

		private void completeHierarchy() {
			hierachyList.forEach((p) -> {
				long classeId = p.getKey().getOid();
				Classe classe = getProcessedClass(classeId);
				Collection<Classe> children = graph.getChildren(classeId).stream().map(this::getProcessedClass).collect(toImmutableList());
				Collection<Classe> descendants = graph.getDescendants(classeId).stream().map(this::getProcessedClass).collect(toImmutableList());
				Collection<Classe> leaves;
				List<Classe> descLeaves = graph.getDescendants(classeId).stream().map(this::getProcessedClass).filter(ClasseHierarchy::isLeaf).collect(toImmutableList());
				if (isLeaf(classe)) {
					leaves = list(classe).with(descLeaves).immutable();
				} else {
					leaves = descLeaves;
				}
				p.getValue().complete(children, descendants, leaves);
			});
		}

		private Classe getProcessedClass(long classOid) {
			return checkNotNull(classes.get(classOid));
		}

		private void processClass(long classOid) {
			ClasseWithoutHierarchy sourceClass = checkNotNull(toProcess.remove(classOid), "class to process not found for oid = %s", classOid);
			try {
				Long parentId = graph.getParentOrNull(classOid);
				Classe parent;
				if (parentId == null) {
					parent = null;
				} else {
					if (!classes.containsKey(parentId)) {
						processClass(parentId);
					}
					parent = getProcessedClass(parentId);
				}
				ClasseHierarchyImpl hierarchy = new ClasseHierarchyImpl(sourceClass, parent);
				hierachyList.add(Pair.of(sourceClass, hierarchy));

				Classe classe = ClasseImpl.copyOf(sourceClass).withHierarchy(hierarchy).build();
				classes.put(classOid, classe);
			} catch (Exception ex) {
				throw new DaoException(ex, "error processing class = %s", sourceClass);
			}
		}

	}

	private static class ClasseHierarchyImpl implements ClasseHierarchy {

		private final ClasseWithoutHierarchy inner;
		private final Classe parent;

		private Collection<Classe> children, descendants, leaves;

		public ClasseHierarchyImpl(ClasseWithoutHierarchy inner, @Nullable Classe parent) {
			this.inner = checkNotNull(inner);
			this.parent = parent;
		}

		private void complete(Collection<Classe> children, Collection<Classe> descendants, Collection<Classe> leaves) {
			this.children = checkNotNull(children);
			this.descendants = checkNotNull(descendants);
			this.leaves = checkNotNull(leaves);
		}

		@Override
		@Nullable
		public Classe getParentOrNull() {
			return parent;
		}

		@Override
		public Collection<Classe> getChildren() {
			return checkNotNull(children, "error accessing hierarchy: class hierarchy is not ready");
		}

		@Override
		public Collection<Classe> getLeaves() {
			return checkNotNull(leaves, "error accessing hierarchy: class hierarchy is not ready");
		}

		@Override
		public Collection<Classe> getDescendants() {
			return checkNotNull(descendants, "error accessing hierarchy: class hierarchy is not ready");
		}

		@Override
		public boolean isAncestorOf(Classe other) {
			return equal(other.getOid(), inner.getOid()) || getDescendants().stream().anyMatch((des) -> equal(des.getOid(), other.getOid()));
		}

	}

	private static class ClasseGraph {

		private final Collection<ClasseHierarchyInfo> classes;
		private final Map<Long, GraphNode> graphNodesByClasseId = map();
		private final Multimap<Long, GraphNode> graphNodesByParentId = multimap();

		public ClasseGraph(Collection<ClasseHierarchyInfo> classes) {
			this.classes = classes;
			buildGraph();
		}

		public @Nullable
		Long getParentOrNull(Long classeId) {
			return graphNodesByClasseId.get(classeId).getParentId();
		}

		public Collection<Long> getChildren(Long classeId) {
			return graphNodesByClasseId.get(classeId).childs.stream().map(GraphNode::getClasseId).collect(toList());
		}

		public Collection<Long> getDescendants(Long classeId) {
			return getDescendantNodes(classeId).stream().map(GraphNode::getClasseId).collect(toList());
		}

		public Collection<Long> getLeaves(Long classeId) {
			return getDescendantNodes(classeId).stream().filter(GraphNode::isLeaf).map(GraphNode::getClasseId).collect(toList());
		}

		private Collection<GraphNode> getDescendantNodes(Long classeId) {
			Queue<GraphNode> queue = new ConcurrentLinkedQueue<>();
			queue.addAll(graphNodesByClasseId.get(classeId).childs);
			List<GraphNode> res = list();
			while (!queue.isEmpty()) {
				GraphNode node = queue.poll();
				res.add(node);
				queue.addAll(node.childs);
			}
			return res;
		}

		private void buildGraph() {
			indexGraphNodes();
			addParents();
			addChilds();
		}

		private void indexGraphNodes() {
			classes.forEach((classe) -> {
				GraphNode node = new GraphNode(classe.getId(), classe.getParentId());
				graphNodesByClasseId.put(node.getClasseId(), node);
			});
		}

		private void addParents() {
			graphNodesByClasseId.values().forEach((node) -> {
				if (node.hasParent()) {
					graphNodesByParentId.put(node.getParentId(), node);
				}
			});
		}

		private void addChilds() {
			graphNodesByClasseId.values().forEach((node) -> {
				node.childs = graphNodesByParentId.get(node.getClasseId());
			});
		}

	}

	private static class GraphNode {

		private final Long classeId, parentId;
		private Collection<GraphNode> childs;

		public GraphNode(Long classeId, @Nullable Long parentId) {
			this.classeId = checkNotNull(classeId);
			this.parentId = parentId;
		}

		public Long getClasseId() {
			return classeId;
		}

		public @Nullable
		Long getParentId() {
			return parentId;
		}

		public boolean hasParent() {
			return parentId != null;
		}

		public boolean isLeaf() {
			return childs.isEmpty();
		}

	}

	private static class ClasseHierarchyInfo {

		private final Long id, parentId;

		public ClasseHierarchyInfo(Long id, @Nullable Long parentId) {
			this.id = checkNotNull(id);
			this.parentId = parentId;
		}

		public Long getId() {
			return id;
		}

		@Nullable
		public Long getParentId() {
			return parentId;
		}

	}
}
