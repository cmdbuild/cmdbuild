package org.cmdbuild.dao.entrytype;

import static java.util.Collections.emptyMap;
import java.util.Map;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

/**
 * Attribute definition used for creating or updating attributes.
 */
public interface AttributeDefinition {

	String getName();

	EntryType getOwner();//TODO remove this, use typeAndName bean

	CardAttributeType<?> getType();

	String getDescription();

	String getDefaultValue();

	Boolean isDisplayableInList();

	boolean isMandatory();

	boolean isUnique();

	Boolean isActive();

	AttributePermissionMode getMode();

	Integer getIndex();

	@Nullable
	String getGroupId();

	Integer getClassOrder();

	String getEditorType();

	String getFilter();

	String getForeignKeyDestinationClassName();

	default Map<String, String> getMetadata() {
		return emptyMap();
	}

	default boolean hasGroup() {
		return isNotBlank(getGroupId());
	}

}
