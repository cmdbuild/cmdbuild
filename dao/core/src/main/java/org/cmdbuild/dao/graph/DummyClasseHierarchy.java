/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.graph;

import java.util.Collection;
import static java.util.Collections.emptyList;
import org.cmdbuild.dao.entrytype.Classe;

public enum DummyClasseHierarchy implements ClasseHierarchy {

	INSTANCE;

	@Override
	public Classe getParentOrNull() {
		return null;
	}

	@Override
	public Collection<Classe> getChildren() {
		return emptyList();
	}

	@Override
	public Collection<Classe> getLeaves() {
		return emptyList();
	}

	@Override
	public Collection<Classe> getDescendants() {
		return emptyList();
	}

	@Override
	public boolean isAncestorOf(Classe classe) {
		return false;
	}

}
