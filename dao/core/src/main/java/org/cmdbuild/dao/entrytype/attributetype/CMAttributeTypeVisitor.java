package org.cmdbuild.dao.entrytype.attributetype;

import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;

public interface CMAttributeTypeVisitor {

	void visit(BooleanAttributeType attributeType);

	void visit(CharAttributeType attributeType);

	void visit(DateAttributeType attributeType);

	void visit(DateTimeAttributeType attributeType);

	void visit(DecimalAttributeType attributeType);

	void visit(DoubleAttributeType attributeType);

	void visit(RegclassAttributeType attributeType);

	void visit(ForeignKeyAttributeType attributeType);

	void visit(IntegerAttributeType attributeType);

	default void visit(LongAttributeType attributeType) {
		throw unsupported("unsupported processing of long attribute");
	}

	void visit(IpAddressAttributeType attributeType);

	void visit(LookupAttributeType attributeType);

	void visit(ReferenceAttributeType attributeType);

	void visit(StringArrayAttributeType attributeType);

	void visit(StringAttributeType attributeType);

	void visit(TextAttributeType attributeType);

	void visit(TimeAttributeType attributeType);

	default void visit(ByteArrayAttributeType attributeType) {
		throw new UnsupportedOperationException("byte array attribute type not yet supported here");//TODO add support everywhere
	}

	default void visit(ReferenceArrayAttributeType attributeType) {
		throw new UnsupportedOperationException("reference array attribute type not yet supported here");//TODO add support everywhere
	}

}
