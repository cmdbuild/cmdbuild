/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.entrytype;

import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface ClassMetadata extends EntryTypeMetadata {

	static final String SUPERCLASS = "cm_superclass";
	static final String CLASS_TYPE = "cm_class_type";
	static final String CLASS_TYPE_SIMPLE = "simpleclass";
	static final String CLASS_TYPE_STANDARD = "class";
	static final String USER_STOPPABLE = "cm_stoppable";
	static final String WORKFLOW_STATUS_ATTR = "cm_workflow_status_attr";
	static final String WORKFLOW_MESSAGE_ATTR = "cm_workflow_message_attr";
	static final String WORKFLOW_ENABLE_SAVE_BUTTON = "cm_workflow_enable_save_button";
	static final String WORKFLOW_PROVIDER = "cm_workflow_provider";
	static final String ATTACHMENT_TYPE_LOOKUP_TYPE = "cm_attachment_type_lookup";
	static final String ATTACHMENT_DESCRIPTION_MODE = "cm_attachment_description_mode";
	static final String DEFAULT_FILTER = "cm_default_filter";
	static final String NOTE_INLINE = "cm_note_inline";
	static final String NOTE_INLINE_CLOSED = "cm_note_inline_closed";
	static final String VALIDATION_RULE = "cm_validation_rule";
	static final String CLASS_ICON = "cm_class_icon";
	static final String PROCESS_ENGINE = "cm_process_engine";
	static final String MULTITENANT_MODE = "cm_multitenant_mode";

	boolean isSuperclass();

	ClassType getClassType();

	default boolean holdsHistory() {
		return ClassType.STANDARD.equals(getClassType());
	}

	boolean isUserStoppable();

	@Nullable
	String getFlowStatusAttr();

	@Nullable
	String getFlowProviderOrNull();

	@Nullable
	String getMessageAttr();

	boolean isFlowSaveButtonEnabled();

	@Nullable
	String getAttachmentTypeLookupTypeOrNull();

	AttachmentDescriptionMode getAttachmentDescriptionMode();

	@Nullable
	String getDefaultFilterOrNull();

	@Nullable
	String getValidationRuleOrNull();

	boolean getNoteInline();

	boolean getNoteInlineClosed();
	
	ClassMultitenantMode getMultitenantMode();

	default boolean hasIcon() {
		return isNotBlank(getAll().get(CLASS_ICON));
	}

	default String getIcon() {
		return checkNotBlank(getAll().get(CLASS_ICON));
	}

}
