package org.cmdbuild.dao.entrytype.attributetype;

import java.math.BigDecimal;

import org.apache.commons.lang3.Validate;

public class DecimalAttributeType implements CardAttributeType<BigDecimal> {

	public final Integer precision;
	public final Integer scale;

	public DecimalAttributeType() {
		this.precision = null;
		this.scale = null;
	}

	public DecimalAttributeType(final Integer precision, final Integer scale) {
		Validate.isTrue(precision > 0);
		Validate.isTrue(scale >= 0 && precision >= scale);
		this.precision = precision;
		this.scale = scale;
	}

	@Override
	public void accept(final CMAttributeTypeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AttributeTypeName getName() {
		return AttributeTypeName.DECIMAL;
	}

	public Integer getPrecision() {
		return precision;
	}

	public Integer getScale() {
		return scale;
	}

	public boolean hasPrecisionAndScale() {
		return precision != null && scale != null;
	}

}
