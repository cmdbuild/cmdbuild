/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.core.q3;

import java.util.Map;
import org.cmdbuild.dao.beans.CMRelation;
import org.cmdbuild.dao.beans.Card;

public interface ResultRow {

	<T> T toModel(Class<T> type);

	Map<String, Object> asMap();

	<T> T toModel();

	Card toCard();

	CMRelation toRelation();

}
