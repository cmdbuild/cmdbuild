package org.cmdbuild.dao.entrytype;

public interface CMEntryTypeVisitor {

	void visit(Classe type);

	void visit(Domain type);

	void visit(CMFunctionCall type);

}
