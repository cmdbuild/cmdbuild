/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.entrytype;

import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

public interface AttributeMetadata extends AbstractMetadata, CardAttributeType.Meta {

	static final String BASEDSP = "cm_basedsp";
	static final String CLASSORDER = "cm_classorder";
	static final String DEFAULT = "cm_default";
	static final String EDITOR_TYPE = "cm_text_editor_type";
	static final String FILTER = "cm_filter";
	static final String GROUP = "cm_group";
	static final String INDEX = "cm_index";
	static final String INHERITED = "cm_inherited";
	static final String IP_TYPE = "cm_ip_type";
	static final String LOOKUP_TYPE = "cm_lookup_type";
	static final String MANDATORY = "cm_mandatory";
	static final String REFERENCE_DIRECT = "cm_reference_direct";//TODO check this
	static final String REFERENCE_DOMAIN = "cm_reference_domain";
	static final String REFERENCE_TYPE = "cm_reference_type";
	static final String UNIQUE = "cm_unique";
	static final String FK_TARGET_CLASS = "cm_fk_target_class";

	boolean isDisplayableInList();

	boolean isMandatory();

	boolean isUnique();

	boolean isInherited();

	int getIndex();

	String getDefaultValue();

	@Nullable
	String getGroup();

	int getClassOrder();

	String getEditorType();

	String getFilter();

	AttributePermissionMode getMode();

	Map<PermissionScope, Set<AttributePermission>> getPermissionMap();

}
