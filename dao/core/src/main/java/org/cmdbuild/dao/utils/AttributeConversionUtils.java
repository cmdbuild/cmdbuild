/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.utils;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Streams.stream;
import static java.lang.String.format;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.dao.beans.IdAndDescription;
import static org.cmdbuild.utils.date.DateUtils.toDate;
import static org.cmdbuild.utils.date.DateUtils.toDateTime;
import static org.cmdbuild.utils.date.DateUtils.toTime;
import org.cmdbuild.dao.beans.IdAndDescriptionImpl;
import org.cmdbuild.dao.beans.LookupValue;
import org.cmdbuild.dao.beans.LookupValueImpl;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.attributetype.IpAddressAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.StringAttributeType;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBigDecimalOrNull;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBooleanOrNull;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toDoubleOrNull;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toIntegerOrNull;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toLongOrNull;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toLong;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.getClassOfNullable;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNotNullAndGtZero;
import org.cmdbuild.workflow.type.ReferenceType;
import static org.cmdbuild.workflow.type.utils.WorkflowTypeUtils.emptyToNull;

public class AttributeConversionUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(AttributeConversionUtils.class);

	public static <T> T rawToSystem(Attribute attribute, @Nullable Object value) {
		return (T) rawToSystem((CardAttributeType) attribute.getType(), value);
	}

	public static <T> T rawToSystem(CardAttributeType<T> attributeType, @Nullable Object value) {
		try {
			return doRawToSystem(attributeType, value);
		} catch (Exception ex) {
			throw runtime(ex, "error converting raw value = '%s' for attribute type = %s", abbreviate(value), attributeType);
		}
	}

	private static <T> T doRawToSystem(CardAttributeType<T> attributeType, @Nullable Object value) {
		if (value == null) {
			return null;
		} else {
			switch (attributeType.getName()) {
				case BOOLEAN:
					return (T) toBooleanOrNull(value);
				case CHAR:
					return (T) convertTextValue(value, 1);
				case DATE:
					return (T) toDate(value);
				case TIMESTAMP:
					return (T) toDateTime(value);
				case DECIMAL:
					return (T) toBigDecimalOrNull(value);
				case DOUBLE:
					return (T) toDoubleOrNull(value);
				case REGCLASS:
//					return (T) toLongOrNull(value);
					return (T) unescapeRegclassName(toStringOrNull(value));
				case REFERENCE:
				case FOREIGNKEY:
					return (T) convertReference(value);
				case REFERENCEARRAY:
					return (T) convertReferenceArray(value);
				case INTEGER:
					return (T) toIntegerOrNull(value);
				case LONG:
					return (T) toLongOrNull(value);
				case LOOKUP: {
					if (value instanceof LookupValue) {
						return (T) value;
					}
					if (value instanceof IdAndDescription) {
						return (T) new LookupValueImpl(((IdAndDescription) value).getId(), ((IdAndDescription) value).getDescription(), ((LookupAttributeType) attributeType).getLookupTypeName());
					}

					if (value instanceof Number) {
						if (((Number) value).longValue() == 0) {
							return null;
						} else {
							return (T) new LookupValueImpl( //
									Number.class.cast(value).longValue(), StringUtils.EMPTY, ((LookupAttributeType) attributeType).getLookupTypeName());
						}
					} else if (value instanceof String) {
						String s = String.class.cast(value);
						if (isNotBlank(s)) {
							return (T) new LookupValueImpl(Long.parseLong(s), StringUtils.EMPTY, ((LookupAttributeType) attributeType).getLookupTypeName());
						} else {
							return null;
						}
					} else {
						throw new IllegalArgumentException(format("invalid value = '%s' for lookup attr type", value));
					}
				}
				case STRINGARRAY:
					return (T) convert(value, String[].class);
				case BYTEAARRAY:
					return (T) convert(value, byte[][].class);
				case STRING:
					return (T) convertTextValue(value, ((StringAttributeType) attributeType).getLength());
				case TEXT:
					return (T) convertTextValue(value, null);
				case TIME:
					return (T) toTime(value);
				case BYTEARRAY:
					return (T) convertByteArrayValue(value);
				case INET:
					return (T) convertIpAddr((IpAddressAttributeType) attributeType, value);
				case UNKNOWN:
				default:
					return (T) value;
			}
		}
	}

	private static @Nullable
	IdAndDescriptionImpl convertReference(@Nullable Object value) {
		if (value == null) {
			return null;
		} else if (value instanceof IdAndDescriptionImpl) {
			return IdAndDescriptionImpl.class.cast(value);
		} else if (value instanceof ReferenceType) {
			ReferenceType reference = emptyToNull((ReferenceType) value);
			return reference == null ? null : new IdAndDescriptionImpl(reference.getId(), reference.getDescription());
		} else if (value instanceof Number) {
			if (isNotNullAndGtZero((Number) value)) {
				return new IdAndDescriptionImpl(toLong(value), StringUtils.EMPTY);
			} else {
				return null;
			}
		} else if (value instanceof String) {
			Long converted = toLong(value);
			return new IdAndDescriptionImpl(converted, StringUtils.EMPTY);
		} else if (value instanceof Map) {
			Map map = (Map) value;
			Long id = toLongOrNull(map.get("_id"));
			String description = toStringOrNull(map.get("_description"));
			String code = toStringOrNull(map.get("_code"));
			return isNotNullAndGtZero(id) ? new IdAndDescriptionImpl(id, description, code) : null;
		} else {
			throw new IllegalArgumentException(format("invalid value = '%s' for reference attr type", value));
		}
	}

	private static @Nullable
	IdAndDescription[] convertReferenceArray(@Nullable Object value) {
		if (value == null) {
			return null;
		} else {
			if (value.getClass().isArray()) {
				value = convert(value, List.class);
			}
			checkArgument(value instanceof Iterable, "expect some sort of iterable, found %s (%s)", value, getClassOfNullable(value));
			return ((List<IdAndDescription>) stream((Iterable) value).map(AttributeConversionUtils::convertReference).collect(toList())).toArray(new IdAndDescription[]{});
		}
	}

	private static @Nullable
	String convertTextValue(@Nullable Object value, @Nullable Integer maxLen) {
		if (value == null) {
			return null;
		}
		String stringValue = value.toString();
		if (isBlank(stringValue)) {
			return null;
		}
		if (maxLen != null) {
			checkArgument(stringValue.length() <= maxLen, "string '%s' exceed maximum attr len of %s", abbreviate(stringValue), maxLen);
		}

		return stringValue;
	}

	private static @Nullable
	byte[] convertByteArrayValue(@Nullable Object value) {
		if (value == null) {
			return null;
		}
		checkArgument(value instanceof byte[], "expected argument of type byte[], found instead %s (%s)", value.getClass(), value);
		return (byte[]) value;
	}

	private static final String IPV4SEG = "(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])";
	private static final String IPV4ADDR = "(" + IPV4SEG + "\\.){3,3}" + IPV4SEG;
	private static final String IPV6SEG = "[0-9a-fA-F]{1,4}";
	private static final String IPV6ADDR = "(" //
			+ "(" + IPV6SEG + ":){7,7}" + IPV6SEG + "|" //
			+ "(" + IPV6SEG + ":){1,7}:|" //
			+ "(" + IPV6SEG + ":){1,6}:" + IPV6SEG + "|" //
			+ "(" + IPV6SEG + ":){1,5}(:" + IPV6SEG + "){1,2}|" //
			+ "(" + IPV6SEG + ":){1,4}(:" + IPV6SEG + "){1,3}|" //
			+ "(" + IPV6SEG + ":){1,3}(:" + IPV6SEG + "){1,4}|" //
			+ "(" + IPV6SEG + ":){1,2}(:" + IPV6SEG + "){1,5}|" //
			+ IPV6SEG + ":((:" + IPV6SEG + "){1,6})|" //
			+ ":((:" + IPV6SEG + "){1,7}|:)|" //
			+ "fe80:(:" + IPV6SEG + "){0,4}%[0-9a-zA-Z]{1,}|" //
			+ "::(ffff(:0{1,4}){0,1}:){0,1}" + IPV4ADDR + "|" //
			+ "(" + IPV6SEG + ":){1,4}:" + IPV4ADDR //
			+ ")";
	private static final String CLASS_SEPARATOR_REGEX = "/";
	private static final String IPV4_CLASS_REGEX = "(3[0-2]|[1-2][0-9]|[8-9])";
	private static final Pattern IPV4_PATTERN = Pattern.compile(EMPTY //
			+ "^" //
			+ IPV4ADDR + "(" + CLASS_SEPARATOR_REGEX + IPV4_CLASS_REGEX + ")*"//
			+ "$" //
	);
	private static final String IPV6_CLASS_REGEX = "([0-9]|[1-9][0-9]|1[0-1][0-9]|12[0-8])";
	private static final Pattern IPV6_PATTERN = Pattern.compile(EMPTY //
			+ "^" //
			+ IPV6ADDR + "(" + CLASS_SEPARATOR_REGEX + IPV6_CLASS_REGEX + ")*"//
			+ "$" //
	);

	private static @Nullable
	String convertIpAddr(IpAddressAttributeType attributeType, @Nullable Object value) {
		String stringValue = trimToNull(toStringOrNull(value));
		if (stringValue == null) {
			return null;
		} else if ((attributeType.getType() == IpAddressAttributeType.IpType.IPV4) && IPV4_PATTERN.matcher(stringValue).matches()) {
			return stringValue;
		} else if ((attributeType.getType() == IpAddressAttributeType.IpType.IPV6) && IPV6_PATTERN.matcher(stringValue).matches()) {
			return stringValue;
		} else {
			throw new IllegalArgumentException(format("invalid value = '%s' for attr type = %s", value, attributeType));
		}
	}

	private static @Nullable
	String unescapeRegclassName(@Nullable String name) {
		if (isBlank(name)) {
			return null;
		} else {
			return name.replaceAll("^\"|\"$", "");
		}
	}

}
