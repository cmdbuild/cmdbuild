package org.cmdbuild.dao.beans;

import java.util.Objects;
import javax.annotation.Nullable;

public class IdAndDescriptionImpl implements IdAndDescription {

	private final Long id;
	private final String description, code;

	public IdAndDescriptionImpl(@Nullable Long id, @Nullable String description) {
		this(id, description, null);
	}

	public IdAndDescriptionImpl(@Nullable Long id, @Nullable String description, @Nullable String code) {
		this.id = id;
		this.description = description;
		this.code = code;
	}

	@Nullable
	@Override
	public Long getId() {
		return id;
	}

	@Nullable
	@Override
	public String getDescription() {
		return description;
	}

	@Nullable
	@Override
	public String getCode() {
		return code;
	}

	@Override
	public boolean equals(final Object o) {
		if (o == null) {
			return id == null;
		}

		if (o instanceof IdAndDescriptionImpl) {
			Long otherId = ((IdAndDescriptionImpl) o).getId();
			if (id == null) {
				return otherId == null;
			} else {
				return id.equals(otherId);
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 73 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public String toString() {
		return "IdAndDescription{" + "id=" + id + ", description=" + description + '}';
	}

}
