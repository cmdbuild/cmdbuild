/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.entrytype;

import static com.google.common.base.Preconditions.checkArgument;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyMap;
import java.util.Map;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

public class AttributeDefinitionImpl implements AttributeDefinition {

	private final String name, description, defaultValue, editorType, filter, targetClass;
	private final boolean showInGrid, required, unique, active;
	private final AttributePermissionMode mode;
	private final String group;
	private final Integer index, classOrder;
	private final EntryType owner;
	private final CardAttributeType type;
	private final Map<String, String> metadata;

	private AttributeDefinitionImpl(SimpleAttributeDefinitionBuilder builder) {
		this.name = checkNotBlank(builder.name);
		this.owner = checkNotNull(builder.owner);
		this.type = checkNotNull(builder.type);
		this.description = (builder.description);
		this.defaultValue = (builder.defaultValue);
		this.group = (builder.group);
		this.editorType = (builder.editorType);
		this.filter = (builder.filter);
		this.targetClass = (builder.targetClass);
		this.showInGrid = (builder.showInGrid);
		this.required = (builder.required);
		this.unique = (builder.unique);
		this.active = (builder.active);
		this.mode = (builder.mode);
		this.index = (builder.index);
		this.classOrder = (builder.classOrder);
		this.metadata = map(firstNonNull(builder.metadata, emptyMap())).immutable();

		if (hasGroup()) {
			checkArgument(owner instanceof Classe, "only class attributes may have a GROUP");
		}
		
//		switch(type.getName()){
//			case REFERENCE:
//				metadata.
//		}
	}

	@Override
	public Map<String, String> getMetadata() {
		return metadata;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getDefaultValue() {
		return defaultValue;
	}

	@Override
	@Nullable
	public String getGroupId() {
		return group;
	}

	@Override
	public String getEditorType() {
		return editorType;
	}

	@Override
	public String getFilter() {
		return filter;
	}

	@Override
	public String getForeignKeyDestinationClassName() {
		return targetClass;
	}

	@Override
	public Boolean isDisplayableInList() {
		return showInGrid;
	}

	@Override
	public boolean isMandatory() {
		return required;
	}

	@Override
	public boolean isUnique() {
		return unique;
	}

	@Override
	public Boolean isActive() {
		return active;
	}

	@Override
	public AttributePermissionMode getMode() {
		return mode;
	}

	@Override
	public Integer getIndex() {
		return index;
	}

	@Override
	public Integer getClassOrder() {
		return classOrder;
	}

	@Override
	public EntryType getOwner() {
		return owner;
	}

	@Override
	public CardAttributeType getType() {
		return type;
	}

	@Override
	public String toString() {
		return "SimpleAttributeDefinition{" + "name=" + name + ", owner=" + owner + ", type=" + type + '}';
	}

	public static SimpleAttributeDefinitionBuilder builder() {
		return new SimpleAttributeDefinitionBuilder();
	}

	public static SimpleAttributeDefinitionBuilder copyOf(AttributeDefinition source) {
		return new SimpleAttributeDefinitionBuilder()
				.withName(source.getName())
				.withDescription(source.getDescription())
				.withDefaultValue(source.getDefaultValue())
				.withGroup(source.getGroupId())
				.withEditorType(source.getEditorType())
				.withFilter(source.getFilter())
				.withTargetClass(source.getForeignKeyDestinationClassName())
				.withShowInGrid(source.isDisplayableInList())
				.withRequired(source.isMandatory())
				.withUnique(source.isUnique())
				.withActive(source.isActive())
				.withMode(source.getMode())
				.withIndex(source.getIndex())
				.withClassOrder(source.getClassOrder())
				.withOwner(source.getOwner())
				.withMetadata(source.getMetadata())
				.withType(source.getType());
	}

	public static SimpleAttributeDefinitionBuilder copyOf(Attribute source) {
		return new SimpleAttributeDefinitionBuilder()
				.withName(source.getName())
				.withDescription(source.getDescription())
				.withDefaultValue(source.getDefaultValue())
				.withGroup(source.getGroupNameOrNull())
				.withEditorType(source.getEditorType())
				.withFilter(source.getFilter())
				.withTargetClass(source.getForeignKeyDestinationClassName())
				.withShowInGrid(source.isDisplayableInList())
				.withRequired(source.isMandatory())
				.withUnique(source.isUnique())
				.withActive(source.isActive())
				.withMode(source.getMode())
				.withIndex(source.getIndex())
				.withClassOrder(source.getClassOrder())
				.withOwner(source.getOwner())
				.withMetadata(source.getMetadata().getCustomMetadata())
				.withType(source.getType());
	}

	public static class SimpleAttributeDefinitionBuilder implements Builder<AttributeDefinitionImpl, SimpleAttributeDefinitionBuilder> {

		private String name;
		private String description;
		private String defaultValue;
		private String group;
		private String editorType;
		private String filter;
		private String targetClass;
		private Boolean showInGrid;
		private Boolean required;
		private Boolean unique;
		private Boolean active;
		private AttributePermissionMode mode;
		private Integer index;
		private Integer classOrder;
		private EntryType owner;
		private CardAttributeType type;
		private Map<String, String> metadata;

		public SimpleAttributeDefinitionBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withMetadata(Map<String, String> metadata) {
			this.metadata = metadata;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withDefaultValue(String defaultValue) {
			this.defaultValue = defaultValue;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withGroup(String group) {
			this.group = group;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withEditorType(String editorType) {
			this.editorType = editorType;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withFilter(String filter) {
			this.filter = filter;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withTargetClass(String targetClass) {
			this.targetClass = targetClass;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withShowInGrid(Boolean showInGrid) {
			this.showInGrid = showInGrid;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withRequired(Boolean required) {
			this.required = required;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withUnique(Boolean unique) {
			this.unique = unique;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withActive(Boolean active) {
			this.active = active;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withMode(AttributePermissionMode mode) {
			this.mode = mode;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withIndex(Integer index) {
			this.index = index;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withClassOrder(Integer classOrder) {
			this.classOrder = classOrder;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withOwner(EntryType owner) {
			this.owner = owner;
			return this;
		}

		public SimpleAttributeDefinitionBuilder withType(CardAttributeType type) {
			this.type = type;
			return this;
		}

		@Override
		public AttributeDefinitionImpl build() {
			return new AttributeDefinitionImpl(this);
		}

	}
}
