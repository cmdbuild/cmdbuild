/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.repository;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.AttributeGroup;

public interface AttributeGroupRepository {

	List<AttributeGroup> getAll();

	default AttributeGroup get(String groupId) {
		return checkNotNull(getOrNull(groupId), "attribute group not found for id = '%s'", groupId);
	}

	@Nullable
	AttributeGroup getOrNull(String groupId);

	default boolean exists(String groupId) {
		return getOrNull(groupId) != null;
	}

	AttributeGroup create(AttributeGroup group);

	AttributeGroup update(AttributeGroup group);

}
