package org.cmdbuild.dao.entrytype;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;
import static java.util.Collections.singletonList;
import javax.annotation.Nullable;
import org.cmdbuild.dao.graph.ClasseHierarchy;
import static org.cmdbuild.common.Constants.BASE_PROCESS_CLASS_NAME;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;

public interface Classe extends ClasseWithoutHierarchy, ClassPermissions {

	@Nullable
	default Classe getParentOrNull() {
		return getHierarchy().getParentOrNull();
	}

	default Classe getParent() {
		return checkNotNull(getParentOrNull());
	}

	default Collection<Classe> getChildren() {
		return getHierarchy().getChildren();
	}

	default Collection<Classe> getLeaves() {
		return getHierarchy().getLeaves();
	}

	default Collection<Classe> getDescendants() {
		return getHierarchy().getDescendants();
	}

	default Collection<Classe> getDescendantsAndSelf() {
		return list(this).with(getDescendants());
	}

	default Collection<Classe> getAncestorsAndSelf() {
		return hasParent() ? list(getParent().getAncestorsAndSelf()).with(this) : singletonList(this);
	}

	default boolean isAncestorOf(Classe classe) {
		return getHierarchy().isAncestorOf(classe);
	}

	ClasseHierarchy getHierarchy();

	default boolean isProcess() {
		return isProcess(getName(), getParentOrNull());
	}

	default boolean hasAncestor(Classe ancestor) {
		return ancestor.isAncestorOf(this);
	}

	default boolean equalToOrHasAncestor(Classe other) {
		return equal(getId(), other.getId()) || other.isAncestorOf(this);
	}

	public static boolean isProcess(String className, @Nullable Classe parent) {
		return equal(className, BASE_PROCESS_CLASS_NAME) || (parent != null && parent.isProcess());
	}

}
