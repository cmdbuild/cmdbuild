/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.repository;

import static org.cmdbuild.common.Constants.BASE_CLASS_NAME;
import org.cmdbuild.dao.entrytype.Classe;

public interface ClasseReadonlyRepository extends ClasseWithoutHierarchyReadonlyRepository<Classe> {

	default Classe getRootClass() {
		return getClasse(BASE_CLASS_NAME);
	}
}
