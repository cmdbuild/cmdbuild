package org.cmdbuild.dao.entrytype;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import org.cmdbuild.dao.beans.AttributeMetadataImpl;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.CLASSORDER;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.firstNotNull;

public class AttributeImpl implements Attribute {

	private final EntryType owner;
	private final CardAttributeType type;
	private final String name;
	private final AttributeMetadata meta;
	private final AttributeGroup group;
	private final AttributePermissions permissions;

	private AttributeImpl(AttributeImplBuilder builder) {
		this.owner = checkNotNull(builder.owner);
		this.group = builder.group;
		this.type = checkNotNull(builder.type);
		this.name = checkNotBlank(builder.name);
		this.meta = checkNotNull(builder.meta);
		this.permissions = firstNotNull(builder.permissions, AttributePermissionsImpl.builder().withPermissions(meta.getPermissionMap()).build());
	}

	@Override
	public EntryType getOwner() {
		return owner;
	}

	@Override
	public CardAttributeType getType() {
		return type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public AttributeMetadata getMetadata() {
		return meta;
	}

	@Override
	@Nullable
	public AttributeGroup getGroupOrNull() {
		return group;
	}

	@Override
	public String toString() {
		return "AttributeImpl{" + "name=" + name + '}';
	}

	@Override
	public Map<PermissionScope, Set<AttributePermission>> getPermissionMap() {
		return permissions.getPermissionMap();
	}

	public static AttributeImplBuilder builder() {
		return new AttributeImplBuilder();
	}

	public static AttributeImplBuilder copyOf(AttributeWithoutOwner source) {
		return new AttributeImplBuilder()
				.withType(source.getType())
				.withName(source.getName())
				.withGroup(source.getGroupOrNull())
				.withPermissions(source)
				.withMeta(source.getMetadata());
	}

	public static AttributeImplBuilder copyOf(Attribute source) {
		return new AttributeImplBuilder()
				.withOwner(source.getOwner())
				.withType(source.getType())
				.withName(source.getName())
				.withGroup(source.getGroupOrNull())
				.withPermissions(source)
				.withMeta(source.getMetadata());
	}

	public static class AttributeImplBuilder implements Builder<AttributeImpl, AttributeImplBuilder> {

		private EntryType owner;
		private CardAttributeType type;
		private String name;
		private AttributeMetadata meta;
		private AttributeGroup group;
		private AttributePermissions permissions;

		public AttributeImplBuilder withOwner(EntryType owner) {
			this.owner = owner;
			return this;
		}

		public AttributeImplBuilder withType(CardAttributeType type) {
			this.type = type;
			return this;
		}

		public AttributeImplBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public AttributeImplBuilder withMeta(AttributeMetadata meta) {
			this.meta = meta;
			return this;
		}

		public AttributeImplBuilder withPermissions(AttributePermissions permissions) {
			this.permissions = permissions;
			return this;
		}

		private AttributeImplBuilder withGroup(AttributeGroup group) {
			this.group = group;
			return this;
		}

		public AttributeImplBuilder withClassOrderInMeta(int classorder) {
			return withMeta(new AttributeMetadataImpl(map(meta.getAll()).with(CLASSORDER, String.valueOf(classorder))));
		}

		@Override
		public AttributeImpl build() {
			return new AttributeImpl(this);
		}

	}
}
