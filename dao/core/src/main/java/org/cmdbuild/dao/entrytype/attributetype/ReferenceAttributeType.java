package org.cmdbuild.dao.entrytype.attributetype;

import javax.annotation.Nullable;

public class ReferenceAttributeType extends AbstractReferenceAttributeType {

	private final String targetClassName;

	public ReferenceAttributeType(@Nullable String domain) {
		this.targetClassName = domain;
	}

	public String getDomainName() {
		return targetClassName;
	}

	@Override
	public void accept(CMAttributeTypeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AttributeTypeName getName() {
		return AttributeTypeName.REFERENCE;
	}

}
