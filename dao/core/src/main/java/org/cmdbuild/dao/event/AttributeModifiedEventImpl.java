/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.event;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwner;
import org.cmdbuild.dao.entrytype.EntryType;

public class AttributeModifiedEventImpl implements AttributeModifiedEvent {

	private final AttributeWithoutOwner attribute;
	private final EntryType owner;

	public AttributeModifiedEventImpl(AttributeWithoutOwner attribute, EntryType owner) {
		this.attribute = checkNotNull(attribute);
		this.owner = checkNotNull(owner);
	}

	@Override
	public AttributeWithoutOwner getAttribute() {
		return attribute;
	}

	@Override
	public EntryType getOwner() {
		return owner;
	}

}
