/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.graph;

import java.util.Collection;
import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.Classe;

public interface ClasseHierarchy {

	@Nullable
	Classe getParentOrNull();

	Collection<Classe> getChildren();

	Collection<Classe> getLeaves();

	Collection<Classe> getDescendants();

	boolean isAncestorOf(Classe classe);

	static boolean isLeaf(Classe classe) {
		return !classe.isSuperclass();
	}

}
