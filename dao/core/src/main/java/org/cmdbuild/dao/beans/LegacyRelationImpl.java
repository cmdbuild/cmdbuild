package org.cmdbuild.dao.beans;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.driver.PostgresService;

@Deprecated
public class LegacyRelationImpl extends DatabaseEntryImpl implements CMRelation, RelationDefinition {

	public static final String _1 = "_1";
	public static final String _2 = "_2";

	private final Map<String, Card> cards;
	private Long card1Id;
	private Long card2Id;

	public static LegacyRelationImpl newInstance(PostgresService driver, Domain type) {
		return new LegacyRelationImpl(driver, type, null);
	}

	public static LegacyRelationImpl newInstance(PostgresService driver, Domain type, Long id) {
		return new LegacyRelationImpl(driver, type, id);
	}

	public static LegacyRelationImpl newInstance(PostgresService driver, CMRelation existentRelation) {
		return new LegacyRelationImpl(driver, existentRelation.getType(), existentRelation.getId());
	}

	private LegacyRelationImpl(PostgresService driver, Domain type, Long id) {
		super(driver, type, id);
		cards = Maps.newHashMap();
	}

	@Override
	public Domain getType() {
		return (Domain) super.getType();
	}

	public Card getCard1() {
		return cards.get(_1);
	}

	@Override
	public LegacyRelationImpl setSourceCard(Card card) {
		cards.put(_1, card);
		card1Id = card.getId();
		return this;
	}

	public LegacyRelationImpl setCard1Id(Long card1Id) {
		this.card1Id = card1Id;
		return this;
	}

	public Card getCard2() {
		return cards.get(_2);
	}

	public LegacyRelationImpl setCard2Id(Long card2Id) {
		this.card2Id = card2Id;
		return this;
	}

	@Override
	public LegacyRelationImpl setTargetCard(Card card) {
		cards.put(_2, card);
		card2Id = card.getId();
		return this;
	}

	@Override
	public LegacyRelationImpl set(String key, Object value) {
		if (_1.equals(key)) {
			setSourceCard(Card.class.cast(value));
		} else if (_2.equals(key)) {
			setTargetCard(Card.class.cast(value));
		} else {
			setOnly(key, value);
		}
		return this;
	}

	@Override
	public LegacyRelationImpl set(Iterable<? extends Entry<String, ? extends Object>> keysAndValues) {
		for (Entry<String, ? extends Object> entry : keysAndValues) {
			set(entry.getKey(), entry.getValue());
		}
		return this;
	}

	@Override
	public LegacyRelationImpl save() {
		saveOnly();
		return this;
	}

	@Override
	public CMRelation create() {
		return save();
	}

	@Override
	public CMRelation update() {
		super.updateOnly();
		return this;
	}

	@Override
	public void delete() {
		super.delete();
	}

	@Override
	public Long getSourceId() {
		return card1Id;
	}

	@Override
	public Long getTargetId() {
		return card2Id;
	}

}
