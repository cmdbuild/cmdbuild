/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.q3;

import static com.google.common.base.MoreObjects.firstNonNull;
import javax.annotation.Nullable;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_BEGINDATE;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.CmdbSorter;
import org.cmdbuild.data.filter.SorterElement;
import static org.cmdbuild.data.filter.SorterElement.SorterElementDirection.DESC;
import org.cmdbuild.data.filter.beans.CmdbSorterImpl;
import org.cmdbuild.data.filter.beans.SorterElementImpl;
import static org.cmdbuild.data.filter.utils.CmdbFilterUtils.noopFilter;
import static org.cmdbuild.data.filter.utils.CmdbSorterUtils.noopSorter;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;

public class DaoQueryOptionsImpl implements DaoQueryOptions {

	private final Integer offset, limit;
	private final CmdbSorter sorter;
	private final CmdbFilter filter;

	private DaoQueryOptionsImpl(DaoQueryOptionsImplBuilder builder) {
		this.offset = builder.offset;
		this.limit = builder.limit;
		this.sorter = firstNonNull(builder.sorter, noopSorter());
		this.filter = firstNonNull(builder.filter, noopFilter());
	}

	@Override
	@Nullable
	public Integer getOffset() {
		return offset;
	}

	@Override
	@Nullable
	public Integer getLimit() {
		return limit;
	}

	@Override
	public CmdbSorter getSorter() {
		return sorter;
	}

	@Override
	public CmdbFilter getFilter() {
		return filter;
	}

	public static DaoQueryOptions emptyOptions() {
		return builder().build();
	}

	public static DaoQueryOptionsImplBuilder builder() {
		return new DaoQueryOptionsImplBuilder();
	}

	public static DaoQueryOptionsImplBuilder copyOf(DaoQueryOptions source) {
		return new DaoQueryOptionsImplBuilder()
				.withOffset(source.getOffset())
				.withLimit(source.getLimit())
				.withSorter(source.getSorter())
				.withFilter(source.getFilter());
	}

	public static class DaoQueryOptionsImplBuilder implements Builder<DaoQueryOptionsImpl, DaoQueryOptionsImplBuilder> {

		private Integer offset;
		private Integer limit;
		private CmdbSorter sorter;
		private CmdbFilter filter;

		public DaoQueryOptionsImplBuilder withPaging(@Nullable Integer offset, @Nullable Integer limit) {
			return this.withOffset(offset).withLimit(limit);
		}

		public DaoQueryOptionsImplBuilder withOffset(@Nullable Integer offset) {
			this.offset = offset;
			return this;
		}

		public DaoQueryOptionsImplBuilder withLimit(@Nullable Integer limit) {
			this.limit = limit;
			return this;
		}

		public DaoQueryOptionsImplBuilder withSorter(CmdbSorter sorter) {
			this.sorter = sorter;
			return this;
		}

		public DaoQueryOptionsImplBuilder withFilter(CmdbFilter filter) {
			this.filter = filter;
			return this;
		}

		@Override
		public DaoQueryOptionsImpl build() {
			return new DaoQueryOptionsImpl(this);
		}

		public DaoQueryOptionsImplBuilder orderBy(String attr, SorterElement.SorterElementDirection dir) {
			SorterElementImpl element = new SorterElementImpl(ATTR_BEGINDATE, DESC);
			if (sorter == null) {
				sorter = new CmdbSorterImpl(element);
			} else {
				sorter = new CmdbSorterImpl(list(sorter.getElements()).with(element));
			}
			return this;
		}

	}
}
