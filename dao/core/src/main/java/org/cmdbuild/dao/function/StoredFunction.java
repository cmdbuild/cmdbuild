package org.cmdbuild.dao.function;

import static com.google.common.base.Objects.equal;
import static com.google.common.collect.MoreCollectors.onlyElement;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface StoredFunction {

	String getName();

	Long getId();

	List<StoredFunctionParameter> getInputParameters();

	List<StoredFunctionOutputParameter> getOutputParameters();

	boolean returnsSet();

	Iterable<Category> getCategories();

	Map<String, Object> getMetadata();

	@Nullable
	FunctionMetadata getFunctionMetadata();

	default StoredFunctionOutputParameter getOutputParameter(String name) {
		checkNotBlank(name);
		return getOutputParameters().stream().filter((p) -> equal(p.getName(), name)).collect(onlyElement());
	}

	enum Category {
		SYSTEM, //
		UNDEFINED, //
		;

		public static Category of(String text) {
			for (Category category : values()) {
				if (category.name().equalsIgnoreCase(text)) {
					return category;
				}
			}
			return UNDEFINED;
		}
	}

}
