/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.core.q3;

import java.util.Collection;
import java.util.function.Consumer;
import javax.annotation.Nullable;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptions;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.CmdbSorter;
import org.cmdbuild.data.filter.SorterElement;
import org.cmdbuild.data.filter.beans.CmdbSorterImpl;
import org.cmdbuild.data.filter.beans.SorterElementImpl;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;

public interface QueryBuilder extends PreparedQuery {

	static final WhereOperator EQ = WhereOperator.EQ;
	static final WhereOperator NOTEQ = WhereOperator.NOTEQ;

	default QueryBuilder select(String... attrs) {
		return select(list(attrs));
	}

	RowNumberQueryBuilder selectRowNumber();

	QueryBuilder select(Collection<String> attrs);

	QueryBuilder selectExpr(String name, String expr);

	QueryBuilder selectMatchFilter(String name, CmdbFilter filter);

	QueryBuilder selectCount();

	QueryBuilder from(String classe);

	QueryBuilder from(Classe classe);

	QueryBuilder from(Class model);

	QueryBuilder from(Domain model);

	QueryBuilder includeHistory();

	QueryBuilder where(String attr, WhereOperator operator, Object... params);

	QueryBuilder whereExpr(String expr, Object... params);

	QueryBuilder whereExpr(String expr, Collection params);

	default QueryBuilder where(WhereOperator operator, String attr, Object... params) {
		return QueryBuilder.this.where(attr, operator, params);
	}

	QueryBuilder where(CmdbFilter filter);

	QueryBuilder where(PreparedQuery query);

	default QueryBuilder where(QueryBuilder query) {
		return where(query.build());
	}

	QueryBuilder orderBy(CmdbSorter sort);

	default QueryBuilder orderBy(String property, SorterElement.SorterElementDirection direction) {
		return orderBy(new CmdbSorterImpl(new SorterElementImpl(property, direction)));
	}

	default QueryBuilder orderBy(String property1, SorterElement.SorterElementDirection direction1, String property2, SorterElement.SorterElementDirection direction2) {
		return orderBy(new CmdbSorterImpl(new SorterElementImpl(property1, direction1), new SorterElementImpl(property2, direction2)));
	}

	default QueryBuilder orderBy(String property) {
		return orderBy(property, SorterElement.SorterElementDirection.ASC);
	}

	QueryBuilder offset(@Nullable Long offset);

	QueryBuilder limit(@Nullable Long limit);

	default QueryBuilder paginate(@Nullable Integer offset, @Nullable Integer limit) {
		return this.offset(convert(offset, Long.class)).limit(convert(limit, Long.class));
	}

	default QueryBuilder paginate(@Nullable Long offset, @Nullable Long limit) {
		return this.offset(offset).limit(limit);
	}

	default QueryBuilder withOptions(DaoQueryOptions queryOptions) {
		return this
				.where(queryOptions.getFilter())
				.orderBy(queryOptions.getSorter())
				.paginate(queryOptions.getOffset(), queryOptions.getLimit());
	}

	default QueryBuilder accept(Consumer<QueryBuilder> visitor) {
		visitor.accept(this);
		return this;
	}

	PreparedQuery build();

	@Override
	default int getCount() {//TODO change count to long
		return build().getCount();
	}

	interface RowNumberQueryBuilder {

		RowNumberQueryBuilder where(String attr, WhereOperator operator, Object... params);

		QueryBuilder then();

	}

}
