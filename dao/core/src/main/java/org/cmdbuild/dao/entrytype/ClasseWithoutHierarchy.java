package org.cmdbuild.dao.entrytype;

import static com.google.common.base.Functions.compose;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.dao.entrytype.EntryType.EntryTypeType.CLASSE;
import org.cmdbuild.auth.grant.PrivilegeSubjectWithInfo;
import static org.cmdbuild.dao.entrytype.ClassMultitenantMode.CMM_NEVER;

public interface ClasseWithoutHierarchy extends EntryType, PrivilegeSubjectWithInfo {

	@Override
	default EntryTypeType getEtType() {
		return CLASSE;
	}

	@Override
	String getName();

	@Nullable
	ClasseInfo getParentInfoOrNull();

	default ClasseInfo getParentInfo() {
		return checkNotNull(getParentInfoOrNull());
	}

	default boolean hasParent() {
		return getParentInfoOrNull() != null;
	}

	default @Nullable
	Long getParentOidOrNull() {
		if (hasParent()) {
			return getParentInfo().getOid();
		} else {
			return null;
		}
	}

	@Nullable
	default String getParentNameOrNull() {
		if (hasParent()) {
			return getParentInfo().getName();
		} else {
			return null;
		}
	}

	@Override
	default String getDescription() {
		return getMetadata().getDescription();
	}

	default Long getOid() {
		return getId();
	}

	default boolean isSuperclass() {
		return getMetadata().isSuperclass();
	}

	@Override
	default boolean hasHistory() {
		return getMetadata().holdsHistory();
	}

	default boolean isSimpleClass() {
		return !hasHistory();
	}

	default boolean isStandardClass() {
		return !isSimpleClass();
	}

	default ClassType getClassType() {
		return isSimpleClass() ? ClassType.SIMPLE : ClassType.STANDARD;
	}

	@Deprecated//workflow specific, remove from class interface
	default boolean isUserStoppable() {
		return getMetadata().isUserStoppable();
	}

	default ClassMultitenantMode getMultitenantMode() {
		return getMetadata().getMultitenantMode();
	}

	default boolean hasMultitenantEnabled() {
		return !equal(getMultitenantMode(), CMM_NEVER);
	}

	@Override
	ClassMetadata getMetadata();

	@Override
	default String getPrivilegeId() {
		return String.format("Class:%s", getName());
	}

	default boolean hasAttachmentTypeLookupType() {
		return !isBlank(getAttachmentTypeLookupTypeOrNull());
	}

	default String getAttachmentTypeLookupType() {
		return checkNotBlank(getAttachmentTypeLookupTypeOrNull());
	}

	@Nullable
	default String getAttachmentTypeLookupTypeOrNull() {
		return getMetadata().getAttachmentTypeLookupTypeOrNull();
	}

	default AttachmentDescriptionMode getAttachmentDescriptionMode() {
		return getMetadata().getAttachmentDescriptionMode();
	}

	default List<Attribute> getAttributesForDefaultOrder() {
		return getCoreAttributes().stream().filter((a) -> a.getClassOrder() != 0).sorted(Ordering.natural().onResultOf(compose(Math::abs, Attribute::getClassOrder))).collect(toList());
	}
}
