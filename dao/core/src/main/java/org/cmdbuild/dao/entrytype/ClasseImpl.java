package org.cmdbuild.dao.entrytype;

import org.cmdbuild.dao.graph.ClasseHierarchy;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;

import com.google.common.collect.Lists;
import static java.util.Collections.emptyMap;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import org.apache.commons.lang3.builder.Builder;
import org.cmdbuild.dao.beans.ClassMetadataImpl;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.toList;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.firstNotNull;

public class ClasseImpl extends EntryTypeImpl implements Classe {

	private final ClassPermissions classPermissions;
	private final ClassMetadata meta;
	private final ClasseHierarchy hierarchy;
	private final ClasseInfo parentInfo;
	private final boolean isProcess;

	private ClasseImpl(ClasseBuilder builder) {
		super(builder.name, builder.id, builder.attributes);
		this.meta = checkNotNull(builder.metadata, "classe meta cannot be null");
		this.hierarchy = checkNotNull(builder.hierarchy, "classe hierarchy cannot be null");
		this.parentInfo = builder.parent;

		this.isProcess = Classe.isProcess(builder.name, builder.hierarchy.getParentOrNull());
		this.classPermissions = firstNotNull(builder.permissions, ClassPermissionsImpl.builder().withPermissions(meta.getPermissions()).build());
	}

	@Override
	public boolean isProcess() {
		return isProcess;
	}

	@Override
	public ClasseInfo getParentInfoOrNull() {
		return parentInfo;
	}

	@Override
	public void accept(CMEntryTypeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public ClassMetadata getMetadata() {
		return meta;
	}

	@Override
	public ClasseHierarchy getHierarchy() {
		return hierarchy;
	}

	@Override
	public Map<PermissionScope, Set<ClassPermission>> getPermissionsMap() {
		return classPermissions.getPermissionsMap();
	}

	@Override
	public String toString() {
		return "ClasseImpl{" + "name=" + getName() + '}';
	}

	public static ClasseBuilder builder() {
		return new ClasseBuilder();
	}

	public static ClasseBuilder copyOf(ClasseWithoutHierarchy classe) {
		return new ClasseBuilder()
				.withId(classe.getOid())
				.withName(classe.getName())
				.withParent(classe.getParentInfoOrNull())
				.withMetadata(classe.getMetadata())
				.withAttributes(classe.getAllAttributes());
	}

	public static ClasseBuilder copyOf(Classe classe) {
		return copyOf((ClasseWithoutHierarchy) classe)
				.withPermissions(classe)
				.withHierarchy(classe.getHierarchy());
	}

	public static class ClasseBuilder implements Builder<ClasseImpl> {

		private List<AttributeWithoutOwner> attributes;

		private String name;
		private Long id;
		private ClassMetadata metadata;
		private ClasseHierarchy hierarchy;
		private ClasseInfo parent;
		private ClassPermissions permissions;

		private ClasseBuilder() {
			metadata = new ClassMetadataImpl(emptyMap());//TODO check this
			attributes = Lists.newArrayList();
		}

		public ClasseBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public ClasseBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public ClasseBuilder withMetadata(ClassMetadata metadata) {
			this.metadata = metadata;
			return this;
		}

		public ClasseBuilder withPermissions(ClassPermissions permissions) {
			this.permissions = permissions;
			return this;
		}

		public ClasseBuilder withAttributes(Iterable<? extends AttributeWithoutOwner> attributes) {
			this.attributes = (List) toList(attributes);
			return this;
		}

		public ClasseBuilder withHierarchy(ClasseHierarchy classeHierarchy) {
			this.hierarchy = classeHierarchy;
			return this;
		}

		public ClasseBuilder withParent(@Nullable ClasseInfo parent) {
			this.parent = parent;
			return this;
		}

		@Override
		public ClasseImpl build() {
			return new ClasseImpl(this);
		}

	}
}
