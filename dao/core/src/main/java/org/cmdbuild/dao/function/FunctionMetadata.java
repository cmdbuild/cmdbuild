/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.function;

import java.util.Collection;
import java.util.Set;
import org.cmdbuild.dao.entrytype.EntryTypeMetadata;

public interface FunctionMetadata extends EntryTypeMetadata {

	static final String CATEGORIES = "cm_categories";
	static final String MASTERTABLE = "cm_mastertable";
	static final String TAGS = "cm_tags";

	Collection<StoredFunction.Category> getCategories();

	Set<String> getTags();

	String getMasterTable();

}
