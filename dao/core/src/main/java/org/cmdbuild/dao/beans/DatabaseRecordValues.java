package org.cmdbuild.dao.beans;

import static com.google.common.collect.Streams.stream;
import java.util.Map;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;
import org.cmdbuild.utils.lang.CmdbMapUtils;

/**
 * Immutable value set
 */
public interface DatabaseRecordValues {

	/**
	 * Returns the value of the specified attribute.
	 *
	 * @param key the name of the attribute.
	 *
	 * @return the value of the attribute.
	 *
	 * @throws IllegalArgumentException if attribute is not present.
	 */
	Object get(String key);

	/**
	 * Returns the value of the specified attribute.
	 *
	 * @param key the name of the attribute.
	 * @param requiredType type the bean must match; can be an interface or
	 * superclass. {@code null} is disallowed.
	 *
	 * @return the value of the attribute.
	 *
	 * @throws IllegalArgumentException if attribute is not present.
	 */
	default <T> T get(String key, Class<? extends T> requiredType) {
		return convert(get(key), requiredType);
	}

	default <T> T get(String key, Class<? extends T> requiredType, T defaultValue) {
		return defaultIfNull(get(key, requiredType), defaultValue);
	}

	default String getString(String key) {
		return get(key, String.class);
	}

	default Integer getInteger(String key) {
		return get(key, Integer.class);
	}

	Iterable<Map.Entry<String, Object>> getAttributeValues();

	static DatabaseRecordValues fromMap(Map map) {
		return new DatabaseRecordValues() {
			@Override
			public Object get(String key) {
				return map.get(key);
			}

			@Override
			public Iterable<Map.Entry<String, Object>> getAttributeValues() {
				return map.values();
			}
		};
	}

	default Map<String, Object> toMap() {
		return stream(getAttributeValues()).collect(CmdbMapUtils.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

}
