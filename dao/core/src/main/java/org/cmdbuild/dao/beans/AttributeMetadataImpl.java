/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.beans;

import static com.google.common.base.Predicates.not;
import com.google.common.collect.ImmutableSet;
import static com.google.common.collect.Maps.filterKeys;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.dao.entrytype.AttributeMetadata;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.BASEDSP;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.CLASSORDER;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.DEFAULT;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.EDITOR_TYPE;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.FILTER;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.FK_TARGET_CLASS;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.GROUP;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.INDEX;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.INHERITED;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.LOOKUP_TYPE;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.MANDATORY;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.REFERENCE_DOMAIN;
import static org.cmdbuild.dao.entrytype.AttributeMetadata.UNIQUE;
import org.cmdbuild.dao.entrytype.AttributePermission;
import org.cmdbuild.dao.entrytype.AttributePermissionMode;
import org.cmdbuild.dao.entrytype.PermissionScope;
import static org.cmdbuild.dao.entrytype.DaoPermissionUtils.getDefaultPermissions;
import org.cmdbuild.dao.entrytype.AttributePermissions;
import org.cmdbuild.dao.entrytype.AttributePermissionsImpl;
import static org.cmdbuild.dao.entrytype.EntryTypeMetadata.ENTRY_TYPE_MODE;
import static org.cmdbuild.dao.entrytype.EntryTypeMetadata.PERMISSIONS;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBooleanOrDefault;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toIntegerOrDefault;
import static org.cmdbuild.dao.entrytype.DaoPermissionUtils.parseAttributePermissions;
import static org.cmdbuild.dao.entrytype.AttributePermissionMode.APM_ALL;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.parseEnumOrDefault;

public class AttributeMetadataImpl extends AbstractMetadataImpl implements AttributeMetadata {

	private static final Set<String> ATTRIBUTE_METADATA_KEYS = ImmutableSet.of(
			ENTRY_TYPE_MODE, PERMISSIONS, LOOKUP_TYPE, REFERENCE_DOMAIN, BASEDSP, MANDATORY, UNIQUE, FK_TARGET_CLASS, INHERITED, INDEX, DEFAULT, GROUP, CLASSORDER, EDITOR_TYPE, FILTER);

	private final boolean isLookup, isReference, isDisplayableInList, isMandatory, isUnique, isForeignKey, isInherited;
	private final String lookupType, domain, foreignKeyDestinationClassName, defaultValue, group, editorType, filter;
	private final int index, classOrder;
	private final AttributePermissionMode mode;
	private final AttributePermissions permissions;

	public AttributeMetadataImpl(Map<String, String> map) {
		super(map, filterKeys(map, not(ATTRIBUTE_METADATA_KEYS::contains)));

		lookupType = defaultIfBlank(map.get(LOOKUP_TYPE), null);
		isLookup = !isBlank(lookupType);
		domain = defaultIfBlank(map.get(REFERENCE_DOMAIN), null);
		isReference = !isBlank(domain);
		isDisplayableInList = toBooleanOrDefault(map.get(BASEDSP), false);
		isMandatory = toBooleanOrDefault(map.get(MANDATORY), false);
		isUnique = toBooleanOrDefault(map.get(UNIQUE), false);
		foreignKeyDestinationClassName = map.get(FK_TARGET_CLASS);
		isForeignKey = !isBlank(foreignKeyDestinationClassName);
		mode = parseEnumOrDefault(map.get(ENTRY_TYPE_MODE), AttributePermissionMode.class, APM_ALL);
		isInherited = toBooleanOrDefault(map.get(INHERITED), false);
		index = toIntegerOrDefault(map.get(INDEX), -1);
		defaultValue = map.get(DEFAULT);
		group = map.get(GROUP);
		classOrder = toIntegerOrDefault(map.get(CLASSORDER), 0);
		editorType = map.get(EDITOR_TYPE);
		filter = map.get(FILTER);

		if (isNotBlank(map.get(PERMISSIONS))) {
			permissions = AttributePermissionsImpl
					.copyOf(getDefaultPermissions(mode))
					.addPermissions(parseAttributePermissions(map.get(PERMISSIONS)))
					.build();
		} else {
			permissions = getDefaultPermissions(mode);
		}
	}

	@Override
	public Map<PermissionScope, Set<AttributePermission>> getPermissionMap() {
		return permissions.getPermissionMap();
	}

	@Override
	public boolean isLookup() {
		return isLookup;
	}

	@Override
	public boolean isReference() {
		return isReference;
	}

	@Override
	public boolean isDisplayableInList() {
		return isDisplayableInList;
	}

	@Override
	public boolean isMandatory() {
		return isMandatory;
	}

	@Override
	public boolean isUnique() {
		return isUnique;
	}

	@Override
	public boolean isForeignKey() {
		return isForeignKey;
	}

	@Override
	public boolean isInherited() {
		return isInherited;
	}

	@Override
	public String getLookupType() {
		return lookupType;
	}

	@Override
	public String getDomain() {
		return domain;
	}

	@Override
	public String getForeignKeyDestinationClassName() {
		return foreignKeyDestinationClassName;
	}

	@Override

	public String getDefaultValue() {
		return defaultValue;
	}

	@Override
	@Nullable
	public String getGroup() {
		return group;
	}

	@Override
	public String getEditorType() {
		return editorType;
	}

	@Override
	public String getFilter() {
		return filter;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public int getClassOrder() {
		return classOrder;
	}

	@Override
	public AttributePermissionMode getMode() {
		return mode;
	}

}
