package org.cmdbuild.dao.entrytype.attributetype;

import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class LookupAttributeType extends AbstractReferenceAttributeType {

	private final String lookupTypeName;

	public LookupAttributeType(String lookupTypeName) {
		this.lookupTypeName = checkNotBlank(lookupTypeName);
	}

	public String getLookupTypeName() {
		return lookupTypeName;
	}

	@Override
	public void accept(CMAttributeTypeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AttributeTypeName getName() {
		return AttributeTypeName.LOOKUP;
	}

	@Override
	public String toString() {
		return "LookupAttributeType{" + "lookupTypeName=" + lookupTypeName + '}';
	}

}
