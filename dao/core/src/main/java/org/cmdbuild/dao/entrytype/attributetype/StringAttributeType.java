package org.cmdbuild.dao.entrytype.attributetype;

import static com.google.common.base.Preconditions.checkArgument;

public class StringAttributeType implements CardAttributeType<String> {

	public final int length;
	private final boolean isJson;

	private StringAttributeType(int length, boolean isJson) {
		this.length = length;
		this.isJson = isJson;
	}

	public static StringAttributeType newJsonStringAttributeType() {
		return new StringAttributeType(Integer.MAX_VALUE, true);
	}

	public StringAttributeType() {
		this.length = Integer.MAX_VALUE;
		isJson = false;
	}

	public int getLength() {
		return length;
	}

	public StringAttributeType(Integer length) {
		checkArgument(length != null && length > 0, "invalid length value = %s", length);
		this.length = length;
		isJson = false;
	}

	public boolean isJson() {
		return isJson;
	}

	public boolean hasLength() {
		return length != Integer.MAX_VALUE;
	}

	@Override
	public void accept(CMAttributeTypeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String toString() {
		return "StringAttributeType{" + "length=" + length + '}';
	}

	@Override
	public AttributeTypeName getName() {
		return AttributeTypeName.STRING;
	}
}
