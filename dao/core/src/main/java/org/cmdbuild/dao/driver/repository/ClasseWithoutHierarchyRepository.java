/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.repository;

import org.cmdbuild.dao.entrytype.ClasseWithoutHierarchy;
import org.cmdbuild.dao.entrytype.ClassDefinition;

public interface ClasseWithoutHierarchyRepository<T extends ClasseWithoutHierarchy> extends ClasseWithoutHierarchyReadonlyRepository<T> {

	T createClass(ClassDefinition definition);

	T updateClass(ClassDefinition definition);

	void deleteClass(T dbClass);

}
