/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.q3;

import javax.annotation.Nullable;
import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.CmdbSorter;

public interface DaoQueryOptions {

	@Nullable
	Integer getOffset();

	@Nullable
	Integer getLimit();

	CmdbSorter getSorter();

	CmdbFilter getFilter();

	default boolean isPaged() {
		return PagedElements.isPaged(getOffset(), getLimit());
	}

	default boolean hasOffset() {
		return PagedElements.hasOffset(getOffset());
	}

	default boolean hasLimit() {
		return PagedElements.hasLimit(getLimit());
	}

}
