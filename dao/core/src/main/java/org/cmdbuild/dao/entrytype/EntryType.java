package org.cmdbuild.dao.entrytype;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.cmdbuild.dao.entrytype.EntryType.EntryTypeType.CLASSE;
import static org.cmdbuild.dao.entrytype.EntryType.EntryTypeType.DOMAIN;
import static org.cmdbuild.dao.entrytype.EntryType.EntryTypeType.OTHER;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.auth.grant.PrivilegeSubject;

public interface EntryType extends PrivilegeSubject {

	Long getId();

	String getName();

	void accept(CMEntryTypeVisitor visitor);

	default boolean isActive() {
		return getMetadata().isActive();
	}

	default String getDescription() {
		return getMetadata().getDescription();
	}

	boolean hasHistory();

	default List<Attribute> getCoreAttributes() {
		return getAllAttributes().stream().filter(Attribute::hasCoreListPermission).collect(toList());
	}

	default List<Attribute> getServiceAttributes() {
		return getAllAttributes().stream().filter(Attribute::hasServiceListPermission).collect(toList());
	}

	Map<String, Attribute> getAllAttributesAsMap();

	default Collection<Attribute> getAllAttributes() {
		return getAllAttributesAsMap().values();
	}

	@Nullable
	default Attribute getAttributeOrNull(String name) {
		return getAllAttributesAsMap().get(checkNotBlank(name));
	}

	default Attribute getAttribute(String name) {
		return checkNotNull(getAttributeOrNull(name), "attribute not found for key = %s within classe = %s", name, this);
	}

	default boolean hasAttribute(String key) {
		return getAttributeOrNull(key) != null;
	}

	EntryTypeMetadata getMetadata();

	default EntryTypeType getEtType() {
		return OTHER;
	}

	default boolean isClasse() {
		return equal(getEtType(), CLASSE);
	}

	default boolean isDomain() {
		return equal(getEtType(), DOMAIN);
	}

	enum EntryTypeType {
		CLASSE, DOMAIN, OTHER
	}

}
