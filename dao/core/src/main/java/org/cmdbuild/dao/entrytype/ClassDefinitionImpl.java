package org.cmdbuild.dao.entrytype;

import javax.annotation.Nullable;
import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class ClassDefinitionImpl implements ClassDefinition {

	private final Long oid;
	private final String name;
	private final Classe parent;
	private final ClassMetadata metadata;

	private ClassDefinitionImpl(ClassDefinitionImplBuilder builder) {
		this.oid = builder.oid;
		this.name = checkNotBlank(builder.name);
		this.parent = builder.parent;
		this.metadata = checkNotNull(builder.metadata);
	}

	@Override
	@Nullable
	public Long getOid() {
		return oid;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	@Nullable
	public Classe getParentOrNull() {
		return parent;
	}

	@Override
	public ClassMetadata getMetadata() {
		return metadata;
	}

	public static ClassDefinitionImplBuilder builder() {
		return new ClassDefinitionImplBuilder();
	}

	public static ClassDefinitionImplBuilder copyOf(Classe classe) {
		return builder()
				.withOid(classe.getOid())
				.withParent(classe.getParentOrNull())
				.withName(classe.getName())
				.withMetadata(classe.getMetadata());
	}

	public static ClassDefinitionImplBuilder copyOf(ClassDefinition source) {
		return new ClassDefinitionImplBuilder()
				.withOid(source.getOid())
				.withName(source.getName())
				.withParent(source.getParentOrNull())
				.withMetadata(source.getMetadata());
	}

	@Override
	public String toString() {
		return "ClassDefinitionImpl{" + "oid=" + oid + ", name=" + name + '}';
	}

	public static class ClassDefinitionImplBuilder implements Builder<ClassDefinitionImpl, ClassDefinitionImplBuilder> {

		private Long oid;
		private String name;
		private Classe parent;
		private ClassMetadata metadata;

		public ClassDefinitionImplBuilder withOid(Long oid) {
			this.oid = oid;
			return this;
		}

		public ClassDefinitionImplBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public ClassDefinitionImplBuilder withParent(Classe parent) {
			this.parent = parent;
			return this;
		}

		public ClassDefinitionImplBuilder withMetadata(ClassMetadata metadata) {
			this.metadata = metadata;
			return this;
		}

		@Override
		public ClassDefinitionImpl build() {
			return new ClassDefinitionImpl(this);
		}

	}
}
