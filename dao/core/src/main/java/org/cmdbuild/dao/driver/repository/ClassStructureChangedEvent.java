/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.repository;

import org.cmdbuild.dao.event.DaoEvent;

/**
 * class structure changes; this should trigger invalidation of any cached class
 * data
 */
public enum ClassStructureChangedEvent implements DaoEvent {
	INSTANCE
}
