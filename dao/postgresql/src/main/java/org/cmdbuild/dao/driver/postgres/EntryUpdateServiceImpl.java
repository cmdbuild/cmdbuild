package org.cmdbuild.dao.driver.postgres;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import static java.lang.String.format;
import java.sql.Connection;
import java.sql.PreparedStatement;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.dao.DaoException;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS1;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS2;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ1;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.beans.CMRelation;
import static org.cmdbuild.dao.constants.SystemAttributes.SYSTEM_ATTRIBUTES_NEVER_INSERTED;
import org.cmdbuild.dao.entrytype.Attribute;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.systemToSql;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.getSystemToSqlCastOrNull;
import org.cmdbuild.dao.beans.DatabaseRecord;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.entryTypeToQuotedSqlIdentifier;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.quoteSqlIdentifier;
import static org.cmdbuild.dao.entrytype.AttributePermission.AP_CREATE;
import static org.cmdbuild.dao.entrytype.AttributePermission.AP_UPDATE;

@Component
public class EntryUpdateServiceImpl implements EntryUpdateService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private final JdbcTemplate jdbcTemplate;

	public EntryUpdateServiceImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
	}

	@Override
	public long executeInsertAndReturnKey(DatabaseRecord entry) {
		return new EntryUpdateExecutor(entry).executeInsert();
	}

	@Override
	public void executeUpdate(DatabaseRecord entry) {
		new EntryUpdateExecutor(entry).executeUpdate();
	}

	private class EntryUpdateExecutor {

		protected final DatabaseRecord entry;

		protected final List<AttrAndVauelAndMarker> attrs = list();

		protected EntryUpdateExecutor(DatabaseRecord entry) {
			this.entry = checkNotNull(entry);
		}

		public long executeInsert() {
			try {
				prepareValues(false);
				KeyHolder keyHolder = new GeneratedKeyHolder();
				jdbcTemplate.update((Connection connection) -> {
					String query;
					if (attrs.isEmpty()) {
						query = format("INSERT INTO %s DEFAULT VALUES", entryTypeToQuotedSqlIdentifier(entry.getType()));
					} else {
						query = format("INSERT INTO %s (%s) VALUES (%s)",
								entryTypeToQuotedSqlIdentifier(entry.getType()),
								attrs.stream().map(AttrAndVauelAndMarker::getAttr).collect(joining(", ")),
								attrs.stream().map(AttrAndVauelAndMarker::getMarker).collect(joining(", ")));
					}
					PreparedStatement preparedStatement = connection.prepareStatement(query, new String[]{ATTR_ID});
					for (int i = 0; i < attrs.size(); i++) {
						preparedStatement.setObject(i + 1, attrs.get(i).getValue());
					}
					return preparedStatement;
				}, keyHolder);
				return checkNotNull(keyHolder.getKey(), "null id returned from insert query").longValue();
			} catch (Exception ex) {
				throw new DaoException(ex, "error executing insert for entry = %s", entry);
			}
		}

		public void executeUpdate() {
			try {
				prepareValues(true);
				if (attrs.isEmpty()) {
					logger.warn("attrs is empty, skipping update");
				} else {
					String sql = format("UPDATE %s SET %s WHERE %s = ?",
							entryTypeToQuotedSqlIdentifier(entry.getType()),
							attrs.stream().map((a) -> format("%s = %s", a.getAttr(), a.getMarker())).collect(joining(", ")),
							quoteSqlIdentifier(ATTR_ID));
					Object[] params = list(attrs.stream().map(AttrAndVauelAndMarker::getValue).collect(toList())).with(entry.getId()).toArray();
					jdbcTemplate.update(sql, params);
				}
			} catch (Exception ex) {
				throw new DaoException(ex, "error executing update for entry = %s", entry);
			}
		}

		private void prepareValues(boolean forUpdate) {
			entry.getAllValuesAsMap().forEach((key, rawValue) -> {
				Attribute attribute = entry.getType().getAttributeOrNull(key);
				if (attribute == null) {
					logger.warn("attribute not found or reserved for type = {} attr = {}, will not save value on db", entry.getType(), key);
				} else {
					if ((!attribute.hasCorePermission(AP_UPDATE) && forUpdate) || (!attribute.hasCorePermission(AP_CREATE) && !forUpdate)) {
						logger.debug("ignore new value for immutable attribute = {} val = {}", key, rawValue);
					} else if (SYSTEM_ATTRIBUTES_NEVER_INSERTED.contains(attribute.getName())) {
						//skip system attrs
					} else {
						Object sqlValue = systemToSql(attribute.getType(), rawValue);
						String marker = "?";
						String cast = getSystemToSqlCastOrNull(attribute.getType());
						if (isNotBlank(cast)) {
							marker += "::" + cast;
						}
						addValue(attribute.getName(), sqlValue, marker);
					}
				}
			});

			if (entry instanceof CMRelation) {
				CMRelation relation = (CMRelation) entry;
				addValue(ATTR_IDOBJ1, relation.getSourceId());
				addRegclassValue(ATTR_IDCLASS1, relation.getSourceCard().getClassName());
				addValue(ATTR_IDOBJ2, relation.getTargetId());
				addRegclassValue(ATTR_IDCLASS2, relation.getTargetCard().getClassName());
			}
		}

		protected void addValue(String name, Object value) {
			addValue(name, value, "?");
		}

		protected void addRegclassValue(String name, String value) {
			attrs.add(new AttrAndVauelAndMarker(quoteSqlIdentifier(name), "?::regclass", quoteSqlIdentifier(value)));
		}

		protected void addValue(String name, Object value, String marker) {
			attrs.add(new AttrAndVauelAndMarker(quoteSqlIdentifier(name), marker, value));
		}

	}

	private final static class AttrAndVauelAndMarker {

		private final String attr, marker;
		private final Object value;

		public AttrAndVauelAndMarker(String attr, String marker, @Nullable Object value) {
			this.attr = checkNotBlank(attr);
			this.marker = checkNotBlank(marker);
			this.value = value;
		}

		public String getAttr() {
			return attr;
		}

		public String getMarker() {
			return marker;
		}

		public @Nullable
		Object getValue() {
			return value;
		}

	}

}
