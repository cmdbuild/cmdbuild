/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.repository;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import org.cmdbuild.dao.function.StoredFunction;

public interface StoredFunctionRepository {

	@Nullable
	StoredFunction findFunctionByName(String name);

	List<StoredFunction> findAllFunctions();

	default StoredFunction getFunctionByName(String name) {
		return checkNotNull(findFunctionByName(name), "function not found for name = %s", name);
	}

	@Nullable
	default StoredFunction findFunctionById(long id) {
		return getOnlyElement(findAllFunctions().stream().filter((f) -> f.getId() == id).collect(toList()), null);
	}

	default StoredFunction getFunctionById(long id) {
		return checkNotNull(findFunctionById(id), "function not found for id = %s", id);
	}

}
