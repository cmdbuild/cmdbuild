/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres;

import java.util.List;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

public interface SqlType {

	SqlTypeName getType();

	default String getSqlType() {
		return getType().name();
	}

	List<String> getParams();

	CardAttributeType.Meta getMetadata();

	boolean hasSqlCast();

	String getSqlCast();

	String toSqlTypeString();

	CardAttributeType<?> toAttributeType();

	default boolean hasParams() {
		return !getParams().isEmpty();
	}

//	@Nullable
//	Object javaToSqlValue(@Nullable Object value);
//
//	@Nullable
//	Object sqlToJavaValue(@Nullable Object value);
}
