/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.listener;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.Subscribe;
import java.util.Map;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.config.api.GlobalConfigService;
import org.cmdbuild.dao.driver.postgres.listener.PostgresNotificationEventService.PostgresNotificationEvent;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PostgresCommandProcessor {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final GlobalConfigService configService;
	private final CacheService cacheService;
	private final PostgresNotificationService notificationService;

	public PostgresCommandProcessor(GlobalConfigService configService, CacheService cacheService, PostgresNotificationEventService eventService, PostgresNotificationService notificationService) {
		this.configService = checkNotNull(configService);
		this.cacheService = checkNotNull(cacheService);
		this.notificationService = checkNotNull(notificationService);
		eventService.getEventBus().register(new Object() {

			@Subscribe
			public void handlePostgresNotificationEvent(PostgresNotificationEvent event) {
				new CommandProcessor(event).handleCommand();
			}

		});
	}

	private class CommandProcessor {

		private final PostgresNotificationEvent event;
		private String action;

		public CommandProcessor(PostgresNotificationEvent event) {
			this.event = checkNotNull(event);
		}

		public void handleCommand() {
			try {
				Map<String, Object> data = event.getPayloadAsMap();
				if ("command".equalsIgnoreCase(toStringNotBlank(data.get("type")))) {
					action = toStringNotBlank(data.get("action"));
					doHandleCommand();
				}
			} catch (Exception ex) {
				logger.error("error processing postgres notification event = {}", event, ex);
				notifyError("error processing message: " + ex);
			}
		}

		private void doHandleCommand() {
			switch (action.toLowerCase()) {
				case "reload":
					logger.info("system reload requested from postgres process");
					configService.reload();
					cacheService.invalidateAll();
					notifySuccess("system reload completed");
					break;
				case "test":
					logger.info("received TEST command from postres process = {} via channel = {}; raw payload = {}", event.getServerPid(), event.getChannel(), abbreviate(event.getPayload()));
					notifySuccess("test OK");
					break;
				default:
					throw unsupported("unsupported action command = %s", action);
			}
		}

		private void notifySuccess(String message) {
			notificationService.sendInfo("CMDBuild %s command: %s", action, checkNotBlank(message));
//			notificationService.sendInfo(map("success", true, "action", action, "message", checkNotBlank(message)));
		}

		private void notifyError(String message) {
			notificationService.sendInfo("CMDBUild command error: %s", checkNotBlank(message));
//			sendInfo(message);
//			notificationService.sendInfo(map("success", false, "action", action, "message", checkNotBlank(message)));
		}
	}

}
