/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.utils;

import static com.google.common.base.Predicates.isNull;
import static com.google.common.base.Predicates.not;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import java.util.Map;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.commons.lang3.tuple.Pair;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.beans.AttributeMetadataImpl;
import org.cmdbuild.dao.beans.ClassMetadataImpl;
import org.cmdbuild.dao.beans.DomainMetadataImpl;
import org.cmdbuild.dao.beans.FunctionMetadataImpl;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_ACTIVE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_ATTACHMENT_DESCRIPTION_MODE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_ATTACHMENT_TYPE_LOOKUP;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_DESCR;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_FLOW_SAVE_BUTTON_ENABLED;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_FLOW_STATUS_ATTR;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_MODE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_MULTITENANT_MODE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_SUPERCLASS;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_TYPE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_USERSTOPPABLE;
import org.cmdbuild.dao.entrytype.AttributeMetadata;
import org.cmdbuild.dao.entrytype.ClassMetadata;
import static org.cmdbuild.dao.entrytype.ClassMetadata.CLASS_TYPE;
import static org.cmdbuild.dao.entrytype.ClassMetadata.USER_STOPPABLE;
import org.cmdbuild.dao.entrytype.DomainMetadata;
import org.cmdbuild.dao.entrytype.EntryTypeMetadata;
import static org.cmdbuild.dao.entrytype.EntryTypeMetadata.ENTRY_TYPE_MODE;
import org.cmdbuild.dao.function.FunctionMetadata;
import org.cmdbuild.utils.json.CmJsonUtils;
import static org.cmdbuild.utils.json.CmJsonUtils.MAP_OF_STRINGS;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommentUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(CommentUtils.class);

	public static ClassMetadata parseClassComment(String comment) {
		return new ClassMetadataImpl(parseComment(comment, CLASS_COMMENT_TO_METADATA_MAPPING));
	}

	private static Map<String, String> parseComment(String comment, Map<String, String> mapping) {
		return CmJsonUtils.<Map<String, String>>fromJson(comment, MAP_OF_STRINGS).entrySet().stream().map((e) -> {
			if (!mapping.containsKey(e.getKey())) {
				LOGGER.warn(marker(), "found unsupported entry type comment = {} (will be ignored)", e.getKey());
				return null;
			} else {
				return Pair.of(mapping.get(e.getKey()), e.getValue());
			}
		}).filter(not(isNull())).collect(toMap(Pair::getKey, Pair::getValue));
	}

	public static AttributeMetadata parseAttributeComment(String comment) {
		return parseAttributeComment(comment, null);
	}

	public static AttributeMetadata parseAttributeComment(String comment, @Nullable String jsonMetadata) {
		try {
			Map<String, String> map = parseComment(comment, ATTRIBUTE_COMMENT_TO_METADATA_MAPPING);
			if (isNotBlank(jsonMetadata)) {
				map = map(map).with((Map) fromJson(jsonMetadata, MAP_OF_STRINGS));
			}
			return new AttributeMetadataImpl(map);
		} catch (Exception ex) {
			throw new DaoException(ex, "error processing attribute metadata from comment = '%s' with [optional] json metadata = %s", comment, jsonMetadata);
		}
	}

	public static boolean attributeCommentHandlesMetadataKey(String metadataKey) {
		return ATTRIBUTE_COMMENT_TO_METADATA_MAPPING.inverse().containsKey(metadataKey);
	}

	public static String attributeMetadataKeyToCommentKey(String metadataKey) {
		return checkNotBlank(ATTRIBUTE_COMMENT_TO_METADATA_MAPPING.inverse().get(metadataKey));
	}

	public static DomainMetadata domainCommentToMetadata(String comment) {
		return new DomainMetadataImpl(parseComment(comment, DOMAIN_COMMENT_TO_METADATA_MAPPING));
	}

	public static FunctionMetadata functionCommentToMetadata(String comment) {
		return new FunctionMetadataImpl(parseComment(comment, FUNCTION_COMMENT_TO_METADATA_MAPPING));
	}
	private final static Map<String, String> ENTRY_TYPE_COMMENT_TO_METADATA_MAPPING = (Map) map(
			COMMENT_ACTIVE, EntryTypeMetadata.ACTIVE,
			COMMENT_MODE, ENTRY_TYPE_MODE).immutableCopy();

	private final static Map<String, String> CLASS_COMMENT_TO_METADATA_MAPPING = (Map) map(ENTRY_TYPE_COMMENT_TO_METADATA_MAPPING).with(
			COMMENT_DESCR, EntryTypeMetadata.DESCRIPTION,
			COMMENT_SUPERCLASS, ClassMetadata.SUPERCLASS,
			COMMENT_TYPE, CLASS_TYPE,
			COMMENT_MULTITENANT_MODE, ClassMetadata.MULTITENANT_MODE,
			COMMENT_USERSTOPPABLE, USER_STOPPABLE,
			COMMENT_FLOW_STATUS_ATTR, ClassMetadata.WORKFLOW_STATUS_ATTR,
			COMMENT_FLOW_SAVE_BUTTON_ENABLED, ClassMetadata.WORKFLOW_ENABLE_SAVE_BUTTON,
			COMMENT_ATTACHMENT_TYPE_LOOKUP, ClassMetadata.ATTACHMENT_TYPE_LOOKUP_TYPE,
			COMMENT_ATTACHMENT_DESCRIPTION_MODE, ClassMetadata.ATTACHMENT_DESCRIPTION_MODE
	).immutableCopy();

	private final static Map<String, String> DOMAIN_COMMENT_TO_METADATA_MAPPING = (Map) map(ENTRY_TYPE_COMMENT_TO_METADATA_MAPPING).with(
			"LABEL", EntryTypeMetadata.DESCRIPTION,
			COMMENT_TYPE, DomainMetadata.DOMAIN_TYPE,
			"CLASS1", DomainMetadata.CLASS_1,
			"CLASS2", DomainMetadata.CLASS_2,
			"DESCRDIR", DomainMetadata.DESCRIPTION_1,
			"DESCRINV", DomainMetadata.DESCRIPTION_2,
			"CARDIN", DomainMetadata.CARDINALITY,
			"MASTERDETAIL", DomainMetadata.MASTERDETAIL,
			"MDLABEL", DomainMetadata.MASTERDETAIL_DESCRIPTION,
			"MDFILTER", DomainMetadata.MASTERDETAIL_FILTER,
			"DISABLED1", DomainMetadata.DISABLED_1,
			"DISABLED2", DomainMetadata.DISABLED_2,
			"INDEX1", DomainMetadata.INDEX_1,
			"INDEX2", DomainMetadata.INDEX_2
	).immutableCopy();

	private final static Map<String, String> FUNCTION_COMMENT_TO_METADATA_MAPPING = (Map) map(ENTRY_TYPE_COMMENT_TO_METADATA_MAPPING).with(
			"CATEGORIES", FunctionMetadata.CATEGORIES,
			"MASTERTABLE", FunctionMetadata.MASTERTABLE,
			"TAGS", FunctionMetadata.TAGS
	).immutableCopy();

	private final static BiMap<String, String> ATTRIBUTE_COMMENT_TO_METADATA_MAPPING = ImmutableBiMap.copyOf(map(ENTRY_TYPE_COMMENT_TO_METADATA_MAPPING).with(
			COMMENT_DESCR, EntryTypeMetadata.DESCRIPTION,
			"BASEDSP", AttributeMetadata.BASEDSP,
			"CLASSORDER", AttributeMetadata.CLASSORDER,
			"EDITORTYPE", AttributeMetadata.EDITOR_TYPE,
			"GROUP", AttributeMetadata.GROUP,
			"INDEX", AttributeMetadata.INDEX,
			"LOOKUP", AttributeMetadata.LOOKUP_TYPE,
			"REFERENCEDIRECT", AttributeMetadata.REFERENCE_DIRECT,
			"REFERENCEDOM", AttributeMetadata.REFERENCE_DOMAIN,
			"REFERENCETYPE", AttributeMetadata.REFERENCE_TYPE,
			"FKTARGETCLASS", AttributeMetadata.FK_TARGET_CLASS,
			"FILTER", AttributeMetadata.FILTER,
			"IP_TYPE", AttributeMetadata.IP_TYPE
	));

}
