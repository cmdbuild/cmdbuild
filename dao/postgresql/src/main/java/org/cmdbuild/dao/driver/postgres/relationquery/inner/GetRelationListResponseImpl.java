/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.relationquery.inner;

import java.util.Iterator;
import java.util.List;
import org.cmdbuild.dao.driver.postgres.relationquery.DomainInfo;
import org.cmdbuild.dao.driver.postgres.relationquery.GetRelationListResponse;
import org.cmdbuild.dao.driver.postgres.relationquery.RelationInfo;

public class GetRelationListResponseImpl implements GetRelationListResponse {

	private final List<DomainInfo> domainInfos;
	private final int totalNumberOfRelations;

	public GetRelationListResponseImpl(List<RelationInfo> relationInfos, int totalNumberOfRelations) {
		this.domainInfos = aggregateRelationInfos(relationInfos);
		this.totalNumberOfRelations = totalNumberOfRelations;
	}

	private static List<DomainInfo> aggregateRelationInfos(List<RelationInfo> relationInfos) {
			throw new UnsupportedOperationException("BROKEN - TODO");
//		Map<NameAndSchema, Pair<QueryDomain, List<RelationInfo>>> map = map();
//		relationInfos.forEach((r) -> {
//			NameAndSchema domainId = r.getQueryDomain().getDomain().getIdentifier();
//			Pair<QueryDomain, List<RelationInfo>> domainInfo;
//			if (map.containsKey(domainId)) {
//				domainInfo = map.get(domainId);
//			} else {
//				domainInfo = Pair.of(r.getQueryDomain(), list());
//				map.put(domainId, domainInfo);
//			}
//			domainInfo.getRight().add(r);
//		});
//		return map.values().stream().map((di) -> new DomainInfoImpl(di.getLeft(), di.getRight())).collect(toImmutableList());
	}

	@Override
	public Iterator<DomainInfo> iterator() {
		return domainInfos.iterator();
	}

	@Override
	public int getTotalNumberOfRelations() {
		return totalNumberOfRelations;
	}

}
