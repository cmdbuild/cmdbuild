/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.dao.driver.postgres.q3;

import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.cmdbuild.utils.hash.CmdbuildHashUtils;
import org.cmdbuild.utils.lang.CmdbCollectionUtils;
import org.cmdbuild.utils.random.CmdbuildRandomUtils;

public class AliasBuilder {

	private final Set<String> uniqueAliases = CmdbCollectionUtils.set();
	private int counter = 0;

	public String buildAlias(String expr) {
		String base = expr.toLowerCase().replaceAll("[^a-z]", "");
		if (StringUtils.isBlank(base)) {
			base = CmdbuildRandomUtils.randomId(4);
		}
		if (!base.startsWith("_")) {
			base = "_" + base;
		}
		String alias = base;
		while (uniqueAliases.contains(alias)) {
			alias = base + CmdbuildHashUtils.encodeString(String.valueOf(counter++));
		}
		uniqueAliases.add(alias);
		return alias;
	}

	public void addAliasesFrom(AliasBuilder source) {
		uniqueAliases.addAll(source.uniqueAliases);
		counter = source.counter;
	}

}
