/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.Iterables.getOnlyElement;
import static java.lang.String.format;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.entrytype.Classe;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class ClassDescriptionServiceImpl implements ClassDescriptionService {

//	private final CmdbCache<String, String> descriptionsByClassAndCardId;
	private final JdbcTemplate jdbcTemplate;

//	public ClassDescriptionServiceImpl(PostgresDatabaseAccessService database, CacheService cacheService) {
	public ClassDescriptionServiceImpl(@Qualifier(SYSTEM_LEVEL_ONE) JdbcTemplate jdbcTemplate) { //TODO enable caching, configure invalidation
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
//		descriptionsByClassAndCardId = cacheService.getCache("description_by_class_and_card_id", CacheService.CacheConfig.DEFAULT);//TODO check this, invalidation etc
	}

	@Override
	public String getDescription(Classe classe, Long cardId) {
//		return descriptionsByClassAndCardId.get(key(classe.getName(), cardId.toString()), () -> doGetDescription(classe, cardId));
//	}

//	private String doGetDescription(Classe classe, Long cardId) {
		try {
			return nullToEmpty(getOnlyElement(jdbcTemplate.queryForList(format("SELECT \"Description\" FROM \"%s\" WHERE \"Id\" = ? AND \"Status\" = 'A'", classe.getName()), String.class, cardId)));
		} catch (Exception ex) {
			throw new DaoException(ex, "error retrieving card description for class = %s card = %s", classe, cardId);
		}
	}

}
