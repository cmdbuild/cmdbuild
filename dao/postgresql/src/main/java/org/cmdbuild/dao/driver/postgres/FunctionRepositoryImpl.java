package org.cmdbuild.dao.driver.postgres;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Objects.equal;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.cmdbuild.dao.function.StoredFunctionImpl;
import org.cmdbuild.dao.function.FunctionMetadata;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.isNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import java.util.Map;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.beans.FunctionMetadataImpl;
import static org.cmdbuild.dao.entrytype.attributetype.UndefinedAttributeType.isUndefined;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.cmdbuild.dao.driver.postgres.utils.CommentUtils.functionCommentToMetadata;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.createAttributeType;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.isVoidSqlType;
import org.cmdbuild.dao.driver.repository.FunctionRepository;
import org.cmdbuild.dao.function.StoredFunctionImpl.FunctionBuilder;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.function.StoredFunction;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.utils.json.CmJsonUtils.MAP_OF_STRINGS;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBooleanOrNull;
import org.cmdbuild.utils.lang.CmdbMapUtils;

@Component
@Primary
public class FunctionRepositoryImpl implements FunctionRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static final String DEFAULT_SCHEMA = "public",
			SEPARATOR = "|",
			KEY_VALUE_SEPARATOR = ": ";

	private final JdbcTemplate jdbcTemplate;

	private final Holder<List<StoredFunction>> allFunctionsCache;
	private final CmdbCache<String, StoredFunction> functionsCacheByNameAndNamespace;

	public FunctionRepositoryImpl(CacheService cacheService, @Qualifier(SYSTEM_LEVEL_ONE) JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);

		allFunctionsCache = cacheService.newHolder("dao_all_functions", CacheService.CacheConfig.SYSTEM_OBJECTS);
		functionsCacheByNameAndNamespace = cacheService.newCache("dao_functions_by_name", CacheService.CacheConfig.SYSTEM_OBJECTS);
	}

	private enum InputOutput {
		i, o, io;
	}

	@Override
	public List<StoredFunction> getAllFunctions() {
		logger.debug("getting all functions");
		return allFunctionsCache.get(this::doFindAllFunctions);
	}

	@Override
	public @Nullable
	StoredFunction getFunctionOrNull(@Nullable String name) {
		if (isBlank(name)) {
			return null;
		} else {
			return functionsCacheByNameAndNamespace.get(name, () -> doFindFunction(name));
		}
	}

	private @Nullable
	StoredFunction doFindFunction(String name) {
		return getAllFunctions().stream().filter((fun) -> equal(fun.getName(), name)).findAny().orElse(null);
	}

	private List<StoredFunction> doFindAllFunctions() {
		List<StoredFunction> functionList = newArrayList(filter(jdbcTemplate.query("SELECT * FROM _cm3_function_list_detailed() f", new RowMapper<StoredFunctionImpl>() {
			@Override
			public StoredFunctionImpl mapRow(ResultSet rs, int rowNum) throws SQLException {
				String name = "<unknown>";
				try {
					name = rs.getString("function_name");
					logger.trace("processing function {}", name);
					Long id = rs.getLong("function_id");
					boolean returnsSet = rs.getBoolean("returns_set");
					FunctionMetadata meta = new FunctionMetadataImpl(CmdbMapUtils.<String, String>map()
							.with((Map) fromJson(rs.getString("metadata"), MAP_OF_STRINGS))
							.with(functionCommentToMetadata(rs.getString("comment")).getAll()));
					return StoredFunctionImpl.builder()
							.withName(name)
							.withId(id)
							.withReturnSet(returnsSet)
							.withMetadata(meta)
							.accept((builder) -> addParameters(rs, builder))
							.build();
				} catch (Exception ex) {
					logger.error("error processing function = {} ", name);
					logger.error("error processing function ", ex);
					return null;
				}
			}

			private void addParameters(ResultSet rs, FunctionBuilder builder) {
				try {
					String[] argIo = (String[]) rs.getArray("arg_io").getArray();
					String[] argNames = (String[]) rs.getArray("arg_names").getArray();
					String[] argTypes = (String[]) rs.getArray("arg_types").getArray();
					checkArgument(argIo.length == argNames.length && argNames.length == argTypes.length, "arg io, names, types mismatch!");
					for (int i = 0; i < argIo.length; ++i) {
						logger.trace("processing function param name = {}, type = {}, io = {}", argNames[i], argTypes[i], argIo[i]);
						String name = argNames[i], sqlTypeName = argTypes[i];
						try {
							InputOutput io = InputOutput.valueOf(argIo[i]);
							boolean isBaseDsp = firstNonNull(toBooleanOrNull(builder.getFunctionMetadata().getCustomMetadata().get(format("attribute.%s.basedsp", name))), true);
							switch (io) {
								case i:
									logger.trace("add input param name = {} type = {}", name, sqlTypeName);
									builder.withInputParameter(name, getType(sqlTypeName));
									break;
								case o:
									if (!isVoidSqlType(sqlTypeName)) {
										logger.trace("add output param name = {} type = {}", name, sqlTypeName);
										builder.withOutputParameter(name, getType(sqlTypeName), isBaseDsp);
									}
									break;
								case io:
									logger.trace("add input/output param name = {} type = {}", name, sqlTypeName);
									builder.withInputParameter(name, getType(sqlTypeName));
									builder.withOutputParameter(name, getType(sqlTypeName), isBaseDsp);
									break;
							}
						} catch (Exception ex) {
							throw new DaoException(ex, "error processing function param = %s for function = %s", name, builder.getName());
						}
					}
				} catch (SQLException ex) {
					throw new DaoException(ex);
				}
			}

			private CardAttributeType getType(String sqlType) {
				CardAttributeType type = createAttributeType(sqlType);
				checkArgument(!isUndefined(type), "param type is 'undefined', sql type = %s", sqlType);
				return type;
			}

		}), not(isNull())));
		return functionList;
	}

//	@Deprecated
//	public static String nameFrom(NameAndSchema identifier) {
//		String name;
//		if (identifier.getSchema() != null) {
//			name = format("%s.%s", identifier.getSchema(), identifier.getName());
//		} else {
//			name = identifier.getName();
//		}
//		return name;
//	}
//
//	public static String schemaToNamespace(String schema) {
//		return DEFAULT_SCHEMA.equals(schema) ? NameAndSchema.DEFAULT_SCHEMA : schema;
//	}

}
