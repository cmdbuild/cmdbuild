package org.cmdbuild.dao.driver.postgres;

import org.cmdbuild.dao.beans.DatabaseRecord;

public interface EntryUpdateService {

	long executeInsertAndReturnKey(DatabaseRecord entry);

	void executeUpdate(DatabaseRecord entry);
}
