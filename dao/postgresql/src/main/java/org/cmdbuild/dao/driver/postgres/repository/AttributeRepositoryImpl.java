/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.repository;

import org.cmdbuild.dao.driver.repository.AttributeGroupRepository;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Strings.emptyToNull;
import com.google.common.collect.ImmutableList;
import static com.google.common.collect.Maps.filterKeys;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.MoreCollectors.onlyElement;
import com.google.common.eventbus.Subscribe;
import com.google.gson.Gson;
import static java.lang.String.format;
import java.sql.ResultSet;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.beans.AttributeMetadataImpl;
import org.cmdbuild.dao.driver.postgres.utils.CommentUtils;
import org.cmdbuild.dao.driver.postgres.repository.beans.AttributeGroupImpl;
import static org.cmdbuild.dao.driver.postgres.utils.CommentUtils.attributeMetadataKeyToCommentKey;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.createAttributeType;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.getSqlTypeString;
import org.cmdbuild.dao.driver.repository.ClassStructureChangedEvent;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.AttributeDefinition;
import org.cmdbuild.dao.entrytype.AttributeDefinitionImpl;
import org.cmdbuild.dao.entrytype.AttributeGroup;
import org.cmdbuild.dao.entrytype.AttributeImpl;
import org.cmdbuild.dao.entrytype.AttributeMetadata;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwner;
import org.cmdbuild.dao.entrytype.EntryTypeImpl;
import org.cmdbuild.dao.entrytype.EntryTypeMetadata;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwnerImpl;
import org.cmdbuild.dao.entrytype.attributetype.CMAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForwardingAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.IpAddressAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.NullAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.TextAttributeType;
import org.cmdbuild.dao.event.AttributeModifiedEventImpl;
import org.cmdbuild.dao.event.DaoEventServiceImpl;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.isNullOrEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.EntryType;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMultimap;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkGtZero;
import org.cmdbuild.dao.driver.repository.InnerAttributeRepository;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.entryTypeToQuotedSqlIdentifier;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.dao.driver.postgres.utils.CommentUtils.parseAttributeComment;
import org.cmdbuild.utils.lang.CmdbMapUtils;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;

@Component
@Primary
public class AttributeRepositoryImpl implements InnerAttributeRepository {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoEventServiceImpl daoEventService;
	private final JdbcTemplate jdbcTemplate;
	private final AttributeGroupRepository groupRepository;
	private final Holder<Map<Long, List<AttributeWithoutOwner>>> attributesByOwner;

	public AttributeRepositoryImpl(CacheService cacheService, AttributeGroupRepository attributeGroupRepository, DaoEventServiceImpl daoEventService, @Qualifier(SYSTEM_LEVEL_ONE) JdbcTemplate jdbcTemplate) {
		this.daoEventService = checkNotNull(daoEventService);
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.groupRepository = checkNotNull(attributeGroupRepository);
		attributesByOwner = cacheService.newHolder("entry_type_attributes_by_owner", CacheService.CacheConfig.SYSTEM_OBJECTS);
		daoEventService.getEventBus().register(new Object() {
			@Subscribe
			public void handleClassStructureChangedEvent(ClassStructureChangedEvent event) {
				invalidateOurCache();
			}
		});
	}

	private void invalidateOurCache() {
		attributesByOwner.invalidate();//TODO invalidate only affected records/class; avoid reload of everything on edit
	}

	@Override
	public Attribute createAttribute(AttributeDefinition definition) {
		logger.info("creating attribute '{}'", definition.getName());
		definition = fixAttributeGroup(definition);
		AttributeWithoutOwner attribute = doCreateAttribute(definition);
		daoEventService.post(new AttributeModifiedEventImpl(attribute, definition.getOwner()));//TODO move this in repo service
		invalidateOurCache();
		return AttributeImpl.copyOf(attribute).withOwner(definition.getOwner()).build();//TODO refresh owner after attr update and cache cleanup
	}

	@Override
	public List<Attribute> updateAttributes(List<AttributeDefinition> definitions) {
		checkArgument(!isNullOrEmpty(definitions));
		EntryType owner = definitions.stream().map(AttributeDefinition::getOwner).distinct().collect(onlyElement());
		definitions = definitions.stream().map(this::fixAttributeGroup).collect(toList());
		List<AttributeWithoutOwner> attributes = doUpdateAttributes(owner, definitions);
		attributes.forEach((attribute) -> daoEventService.post(new AttributeModifiedEventImpl(attribute, owner)));//TODO move this in repo service
		invalidateOurCache();
		return attributes.stream().map((a) -> AttributeImpl.copyOf(a).withOwner(owner).build()).collect(toList());//TODO refresh owner after attr update and cache cleanup
	}

	@Override
	public void deleteAttribute(Attribute attribute) {
		logger.info("deleting attribute '{}'", attribute.getName());
		doDeleteAttribute(attribute);
		invalidateOurCache();
		daoEventService.post(new AttributeModifiedEventImpl(attribute, attribute.getOwner()));//TODO move this in repo service
	}

	@Override
	public List<AttributeWithoutOwner> getNonReservedEntryTypeAttributesForType(long entryTypeId) {
		return firstNonNull(attributesByOwner.get(this::doGetNonReservedEntryTypeAttributesByOwner).get(entryTypeId), emptyList());
	}

	private Map<Long, List<AttributeWithoutOwner>> doGetNonReservedEntryTypeAttributesByOwner() {
		return map(transformValues(jdbcTemplate.query("select _cm3_utils_regclass_to_name(owner::regclass) _classname, owner::oid::int _classid,* FROM _cm3_attribute_list_detailed()", (ResultSet rs, int rowNum) -> {
			String name = "<unknown>", className = "<unknown>";
			Long classId = null;
			try {
				className = rs.getString("_classname");
				classId = checkGtZero(rs.getLong("_classid"));
				name = rs.getString("name");
				String comment = rs.getString("comment");
				String metadata = rs.getString("metadata");
				AttributeMetadata meta = parseAttributeComment(comment, metadata);
				meta = new AttributeMetadataImpl(map(meta.getAll()).skipNullValues().with(
						AttributeMetadata.INHERITED, Boolean.toString(rs.getBoolean("inherited")),
						AttributeMetadata.DEFAULT, rs.getString("default_value"),
						AttributeMetadata.MANDATORY, Boolean.toString(rs.getBoolean("not_null_constraint")),
						AttributeMetadata.UNIQUE, Boolean.toString(rs.getBoolean("unique_constraint"))
				));
				AttributeGroup group = isBlank(meta.getGroup()) ? null : groupRepository.get(meta.getGroup());
				CardAttributeType<?> type = createAttributeType(rs.getString("sql_type"), meta);
				return Pair.of(classId, (AttributeWithoutOwner) AttributeWithoutOwnerImpl.builder()
						.withName(name)
						.withType(type)
						.withMeta(meta)
						.withGroup(group)
						.build());
			} catch (Exception ex) {
				throw new DaoException(ex, "error processing attribute = \"%s\" of class = %s %s", name, classId, className);
			}
		}).stream().collect(toMultimap(Pair::getKey, Pair::getValue)).asMap(), ImmutableList::copyOf));
	}

	private AttributeDefinition fixAttributeGroup(AttributeDefinition definition) {
		if (definition.hasGroup() && !groupRepository.exists(definition.getGroupId())) {
			String newGroupName = format("%s - %s", definition.getOwner().getName(), definition.getGroupId());
			groupRepository.create(AttributeGroupImpl.builder().withName(newGroupName).withDescription(definition.getGroupId()).build());
			return AttributeDefinitionImpl.copyOf(definition).withGroup(newGroupName).build();
		} else {
			return definition;
		}
	}

	private AttributeWithoutOwner doCreateAttribute(AttributeDefinition definition) {
		EntryType owner = definition.getOwner();

		if (definition.getIndex() == null || definition.getIndex() < 0) {
			definition = AttributeDefinitionImpl.copyOf(definition)
					.withIndex(owner.getAllAttributes().stream().map(Attribute::getIndex).reduce(Integer::max).orElse(-1) + 1)
					.build();
		}

		jdbcTemplate.queryForObject("SELECT _cm3_attribute_create(?::regclass,?,?,?::jsonb)", Object.class, entryTypeToQuotedSqlIdentifier(owner), definition.getName(), getSqlTypeString(definition.getType()), toJson(commentFrom(definition)));
		jdbcTemplate.queryForObject("SELECT _cm3_attribute_metadata_set(?::regclass,?,?::jsonb)", Object.class, entryTypeToQuotedSqlIdentifier(owner), definition.getName(), getNonCommentAttributeMetadataAsString(definition));

		AttributeMetadata baseAttributeMetadata = parseAttributeComment(getCommentForAttribute(owner, definition.getName()));
		AttributeMetadata attributeMetadata = new AttributeMetadataImpl(map(baseAttributeMetadata.getAll())
				.with(getNonCommentAttributeMetadata(definition))
				.with(AttributeMetadata.DEFAULT, definition.getDefaultValue())
				.with(AttributeMetadata.MANDATORY, definition.isMandatory())
				.with(AttributeMetadata.UNIQUE, definition.isUnique()));

		return AttributeWithoutOwnerImpl.builder()
				.withName(definition.getName())
				.withType(definition.getType())
				.withMeta(attributeMetadata)
				.build();
	}

	private List<AttributeWithoutOwner> doUpdateAttributes(EntryType ownerParam, List<AttributeDefinition> definitions) {
		EntryTypeImpl owner = (EntryTypeImpl) ownerParam;
		return definitions.stream().map((definition) -> {
			checkArgument(equal(definition.getOwner(), owner));
			return doUpdateAttribute(definition);
		}).collect(toList());
	}

	private AttributeWithoutOwner doUpdateAttribute(AttributeDefinition definition) {
		EntryType owner = definition.getOwner();
		jdbcTemplate.queryForObject("SELECT _cm3_attribute_modify(?::regclass,?,?,?::jsonb)", Object.class, entryTypeToQuotedSqlIdentifier(owner), definition.getName(), getSqlTypeString(definition.getType()), toJson(commentFrom(definition)));
		jdbcTemplate.queryForObject("SELECT _cm3_attribute_metadata_set(?::regclass,?,?::jsonb)", Object.class, entryTypeToQuotedSqlIdentifier(owner), definition.getName(), getNonCommentAttributeMetadataAsString(definition));
		AttributeMetadata attributeMetadata = parseAttributeComment(getCommentForAttribute(owner, definition.getName()));
		attributeMetadata = new AttributeMetadataImpl(map(attributeMetadata.getAll())
				.with(getNonCommentAttributeMetadata(definition))
				.with(
						//						AttributeMetadata.DEFAULT, isDefaultValueChanged ? updatedDefaultValue : existingDefaultValue,
						AttributeMetadata.DEFAULT, definition.getDefaultValue(),
						AttributeMetadata.MANDATORY, definition.isMandatory(),
						AttributeMetadata.UNIQUE, definition.isUnique()
				));
		return AttributeWithoutOwnerImpl.builder()
				.withName(definition.getName())
				.withType(definition.getType())
				.withMeta(attributeMetadata)
				.build();
	}

	private String getCommentForAttribute(EntryType entryType, String name) {
		logger.trace("getting comment for for entry type = {} with name '{}'", entryType, name);
		return jdbcTemplate.queryForObject("SELECT _cm3_attribute_comment_get_jsonb(?::regclass, ?)", String.class, entryTypeToQuotedSqlIdentifier(entryType), name);
	}

	private void doDeleteAttribute(Attribute attribute) {
		jdbcTemplate.queryForObject("SELECT _cm3_attribute_delete(?::regclass,?)", Object.class, entryTypeToQuotedSqlIdentifier(attribute.getOwner()), attribute.getName());
	}

	private Map<String, String> getNonCommentAttributeMetadata(AttributeDefinition definition) {
		return map(filterKeys(definition.getMetadata(), not(CommentUtils::attributeCommentHandlesMetadataKey)));
	}

	private String getNonCommentAttributeMetadataAsString(AttributeDefinition definition) {
		return new Gson().toJson(getNonCommentAttributeMetadata(definition));
	}

	private Map<String, String> commentFrom(AttributeDefinition definition) {
//		List<String> parts=list();
		Map<String, String> map = new ForwardingAttributeTypeVisitor() {

			private final CMAttributeTypeVisitor DELEGATE = NullAttributeTypeVisitor.getInstance();

			private final Map<String, String> map = map();

			@Override
			protected CMAttributeTypeVisitor delegate() {
				return DELEGATE;
			}

			@Override
			public void visit(ForeignKeyAttributeType attributeType) {
				add(AttributeMetadata.FK_TARGET_CLASS, attributeType.getForeignKeyDestinationClassName());
			}

			@Override
			public void visit(IpAddressAttributeType attributeType) {
				add(AttributeMetadata.IP_TYPE, attributeType.getType().name().toLowerCase());
			}

			@Override
			public void visit(LookupAttributeType attributeType) {
				add(AttributeMetadata.LOOKUP_TYPE, attributeType.getLookupTypeName());
			}

			@Override
			public void visit(ReferenceAttributeType attributeType) {
//				NameAndSchema identifier = attributeType.getIdentifier();
//				Domain domain = domainRepository.getDomain(identifier.getName(), identifier.getSchema());

				add(AttributeMetadata.REFERENCE_DOMAIN, attributeType.getDomainName());
				{
					/*
	* TODO really needed?
					 */
					add(AttributeMetadata.REFERENCE_DIRECT, "false");
					add(AttributeMetadata.REFERENCE_TYPE, "restrict");
				}
				if (definition.getFilter() != null) {
					add(AttributeMetadata.FILTER, definition.getFilter());
				}
			}

			@Override
			public void visit(TextAttributeType attributeType) {
				if (isNotBlank(definition.getEditorType())) {
					add(AttributeMetadata.EDITOR_TYPE, definition.getEditorType());
				}
			}

			public Map<String, String> build(AttributeDefinition definition) {
				definition.getType().accept(this);
				CmdbMapUtils.<String, String>map().skipNullValues().with(
						EntryTypeMetadata.ACTIVE, toStringOrNull(definition.isActive()),
						AttributeMetadata.BASEDSP, toStringOrNull(definition.isDisplayableInList()),
						AttributeMetadata.CLASSORDER, toStringOrNull(definition.getClassOrder()),
						EntryTypeMetadata.DESCRIPTION, definition.getDescription(),
						AttributeMetadata.GROUP, emptyToNull(definition.getGroupId()),
						AttributeMetadata.INDEX, toStringOrNull(definition.getIndex()),
						EntryTypeMetadata.ENTRY_TYPE_MODE, definition.getMode().toString().toLowerCase()
				).forEach(this::add);
				return map;
			}

			private void add(String key, String value) {
				map.put(attributeMetadataKeyToCommentKey(key), value);
			}

		}
				.build(definition);

		return map(map)
				.accept((m) -> {
					if (definition.isUnique()) {
						m.put("UNIQUE", "true");
					}
					if (definition.isMandatory()) {
						m.put("NOTNULL", "true");
					}
					if (definition.getDefaultValue() != null) {
						m.put("DEFAULT", definition.getDefaultValue());
					}
				});
	}
}
