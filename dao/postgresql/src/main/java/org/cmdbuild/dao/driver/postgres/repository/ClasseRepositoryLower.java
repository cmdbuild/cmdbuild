package org.cmdbuild.dao.driver.postgres.repository;

import org.cmdbuild.dao.driver.repository.AttributeGroupRepository;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;
import org.cmdbuild.dao.entrytype.ClassMetadata;
import org.springframework.jdbc.core.JdbcTemplate;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.collect.Sets.newTreeSet;
import com.google.common.eventbus.Subscribe;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.beans.ClassMetadataImpl;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_ACTIVE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_ATTACHMENT_DESCRIPTION_MODE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_ATTACHMENT_TYPE_LOOKUP;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_DESCR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_MODE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_MULTITENANT_MODE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_SUPERCLASS;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_TYPE;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_TYPE_CLASS;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_TYPE_SIMPLECLASS;
import static org.cmdbuild.dao.driver.postgres.Const.COMMENT_USERSTOPPABLE;
import org.cmdbuild.dao.driver.postgres.repository.beans.ClasseWithoutHierarchyImpl;
import org.cmdbuild.dao.driver.repository.ClassStructureChangedEvent;
import org.cmdbuild.dao.driver.repository.ClasseWithoutHierarchyRepository;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwner;
import org.cmdbuild.dao.entrytype.ClasseInfo;
import org.cmdbuild.dao.entrytype.ClasseWithoutHierarchy;
import org.cmdbuild.dao.entrytype.SimpleClasseInfo;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.utils.lang.CmdbMapUtils;
import org.cmdbuild.dao.entrytype.ClassDefinition;
import static org.cmdbuild.dao.entrytype.ClassMetadata.DEFAULT_FILTER;
import static org.cmdbuild.dao.entrytype.ClassMetadata.NOTE_INLINE;
import static org.cmdbuild.dao.entrytype.ClassMetadata.NOTE_INLINE_CLOSED;
import static org.cmdbuild.dao.entrytype.ClassMetadata.VALIDATION_RULE;
import org.cmdbuild.dao.driver.repository.DaoEventService;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.driver.repository.InnerAttributeRepository;
import static org.cmdbuild.dao.entrytype.ClassMetadata.WORKFLOW_PROVIDER;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.quoteSqlIdentifier;
import org.cmdbuild.dao.entrytype.ClassType;
import static org.cmdbuild.utils.json.CmJsonUtils.MAP_OF_STRINGS;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.dao.driver.postgres.utils.CommentUtils.parseClassComment;
import static org.cmdbuild.dao.entrytype.ClassMultitenantMode.CMM_NEVER;
import static org.cmdbuild.dao.entrytype.ClassMultitenantModeUtils.serializeClassMultitenantMode;
import static org.cmdbuild.dao.entrytype.DaoPermissionUtils.serializeClassPermissionMode;

@Component
public class ClasseRepositoryLower implements ClasseWithoutHierarchyRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final JdbcTemplate jdbcTemplate;
	private final DaoEventService eventService;
	private final InnerAttributeRepository attributesRepository;

	private final Holder<List<ClasseWithoutHierarchy>> allClassesCache;
	private final CmdbCache<String, Optional<ClasseWithoutHierarchy>> classesCacheByOid;

	public ClasseRepositoryLower(InnerAttributeRepository attributesRepository, CacheService cacheService, @Qualifier(SYSTEM_LEVEL_ONE) JdbcTemplate jdbcTemplate, AttributeGroupRepository groupRepository, DaoEventService eventService) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.attributesRepository = checkNotNull(attributesRepository);
		allClassesCache = cacheService.newHolder("org.cmdbuild.database.classes.lower.all", CacheService.CacheConfig.SYSTEM_OBJECTS);
		classesCacheByOid = cacheService.newCache("org.cmdbuild.database.classes.lower.by_oid", CacheService.CacheConfig.SYSTEM_OBJECTS);
		this.eventService = checkNotNull(eventService);
		eventService.getEventBus().register(new Object() {
			@Subscribe
			public void handleClassStructureChangedEvent(ClassStructureChangedEvent event) {
				invalidateOurCache();
			}
		});
	}

	private void invalidateOurCache() {
		allClassesCache.invalidate();
		classesCacheByOid.invalidateAll();
	}

	private void invalidateAllCache() {
		eventService.post(ClassStructureChangedEvent.INSTANCE);
	}

	@Override
	public List<ClasseWithoutHierarchy> getAllClasses() {
		return allClassesCache.get(() -> {
			logger.debug("findAllClasses");
			List<ClasseWithoutHierarchy> list = jdbcTemplate.query("SELECT c::oid table_id"
					+ ", _cm3_utils_regclass_to_name(c) AS table_name"
					+ ", _cm3_class_parent_get(c)::oid AS parent_id"
					+ ", _cm3_utils_regclass_to_name(_cm3_class_parent_get(c)) AS parent_name"
					+ ", _cm3_class_comment_get_jsonb(c) AS table_comment"
					+ ", _cm3_class_metadata_get(c) AS table_metadata"
					+ " FROM _cm3_class_list() AS c",
					(rs, i) -> {

						Long id = null;
						String name = "<unknown>";
						try {
							id = checkNotNull(rs.getLong("table_id"));
							name = checkNotBlank(rs.getString("table_name"));
							Long parentId = (Long) rs.getObject("parent_id");
							String parentName = (String) rs.getObject("parent_name");
							Collection<AttributeWithoutOwner> attributes = attributesRepository.getNonReservedEntryTypeAttributesForType(id);
							String comment = rs.getString("table_comment");
							String metadata = checkNotBlank(rs.getString("table_metadata"));
							ClassMetadata meta = parseClassComment(comment);
							meta = new ClassMetadataImpl(CmdbMapUtils.<String, String>map()
									.with((Map) fromJson(metadata, MAP_OF_STRINGS))
									.with(meta.getAll()));
							ClasseInfo parent;
							if (parentId == null) {
								parent = null;
							} else {
								parent = new SimpleClasseInfo(parentId, parentName);
							}
							return (ClasseWithoutHierarchy) ClasseWithoutHierarchyImpl.builder()
									.withName(name)
									.withId(id)
									.withMeta(meta)
									.withAttributes(attributes)
									.withParentInfo(parent)
									.build();
						} catch (Exception ex) {
							throw new DaoException(ex, "error processing class oid = %s name = %s", id, name);

						}
					});
			logger.debug("loaded {} classes from db", list.size());
			if (logger.isTraceEnabled()) {
				logger.trace("loaded classes from db = {}", newTreeSet(list.stream().map(ClasseWithoutHierarchy::getName).collect(toList())));
			}
			return list;
		});
	}

	@Override
	public ClasseWithoutHierarchy getClasseOrNull(String name) {
		checkNotBlank(name);
		return getAllClasses().stream().filter((classe) -> equal(classe.getName(), name)).findAny().orElse(null);
	}

	@Override
	@Nullable
	public ClasseWithoutHierarchy getClasseOrNull(long oid) {
		return classesCacheByOid.get(String.valueOf(oid), () -> {
			return getAllClasses().stream().filter((classe) -> classe.getOid() == oid).findAny();
		}).orElse(null);
	}

	@Override
	public ClasseWithoutHierarchy createClass(ClassDefinition definition) {
		try {
			Classe parent = definition.getParentOrNull();
			String parentName = Optional.ofNullable(parent).map((p) -> {
				checkArgument(p.isSuperclass(), "cannot extend parent class = %s, it is not a valid superclass/prototype!", p.getName());
				return p.getName();
			}).orElse(null);
			checkArgument(!definition.getMetadata().holdsHistory() || parent != null, "a standard class must have a valid parent class");
			String classComment = buildCommentJsonString(definition);
			String name = definition.getName();
			logger.info("create class = {}", definition);
			long oid = jdbcTemplate.queryForObject("SELECT x::oid FROM _cm3_class_create(?, ?::regclass, ?::jsonb) x", Long.class, name, parentName == null ? null : quoteSqlIdentifier(parentName), classComment);
			setClassMetadata(definition);
			invalidateAllCache();
			ClasseWithoutHierarchy newClass = ClasseWithoutHierarchyImpl.builder()
					.withName(name)
					.withId(oid)
					.withMeta(parseClassComment(classComment))
					.withAttributes(attributesRepository.getNonReservedEntryTypeAttributesForType(oid))
					.withParentInfo(toInfo(parent))
					.build();
			return newClass;
		} catch (Exception ex) {
			throw new DaoException(ex, "error while creating new class from definition = %s", definition);
		}
	}

	private @Nullable
	ClasseInfo toInfo(@Nullable Classe classe) {
		return classe == null ? null : new SimpleClasseInfo(classe.getOid(), classe.getName());
	}

	@Override
	public ClasseWithoutHierarchy updateClass(ClassDefinition definition) {
		String comment = buildCommentJsonString(definition);
		logger.info("update class = {}", definition);
		jdbcTemplate.queryForObject("SELECT _cm3_class_modify(?::regclass, ?::jsonb)", Object.class, quoteSqlIdentifier(definition.getName()), comment);
		setClassMetadata(definition);
		invalidateAllCache();
		ClasseWithoutHierarchy updatedClass = ClasseWithoutHierarchyImpl.builder()
				.withName(definition.getName())
				.withId(definition.getOid())
				.withMeta(parseClassComment(comment))
				.withAttributes(attributesRepository.getNonReservedEntryTypeAttributesForType(definition.getOid()))
				.withParentInfo(toInfo(definition.getParentOrNull()))
				.build();
		return updatedClass;
	}

	private void setClassMetadata(ClassDefinition definition) {
		jdbcTemplate.queryForObject("SELECT _cm3_class_metadata_set(?::regclass, ?::jsonb)", Object.class, quoteSqlIdentifier(definition.getName()), buildMetadataJsonString(definition));
	}

	private String buildMetadataJsonString(ClassDefinition definition) {
		ClassMetadata metadata = definition.getMetadata();
		return toJson(map().skipNullValues().with(
				DEFAULT_FILTER, metadata.getDefaultFilterOrNull(),
				NOTE_INLINE, metadata.getNoteInline(),
				NOTE_INLINE_CLOSED, metadata.getNoteInlineClosed(),
				VALIDATION_RULE, metadata.getValidationRuleOrNull(),
				WORKFLOW_PROVIDER, metadata.getFlowProviderOrNull()
		));
	}

	private String buildCommentJsonString(ClassDefinition definition) {
		ClassMetadata metadata = definition.getMetadata();
		return toJson(map(
				COMMENT_DESCR, metadata.getDescription(),
				COMMENT_MODE, serializeClassPermissionMode(metadata.getMode()),
				COMMENT_SUPERCLASS, metadata.isSuperclass(),
				COMMENT_TYPE, checkNotNull(map(ClassType.SIMPLE, COMMENT_TYPE_SIMPLECLASS, ClassType.STANDARD, COMMENT_TYPE_CLASS).get(metadata.getClassType())),
				COMMENT_USERSTOPPABLE, metadata.isUserStoppable()
		).skipNullValues().with(
				COMMENT_ACTIVE, metadata.isActive() ? Boolean.TRUE.toString() : Boolean.FALSE.toString(),
				COMMENT_ATTACHMENT_TYPE_LOOKUP, emptyToNull(metadata.getAttachmentTypeLookupTypeOrNull()),
				COMMENT_ATTACHMENT_DESCRIPTION_MODE, metadata.getAttachmentDescriptionMode() == null ? null : metadata.getAttachmentDescriptionMode().name(),
				COMMENT_MULTITENANT_MODE, equal(metadata.getMultitenantMode(), CMM_NEVER) ? null : serializeClassMultitenantMode(metadata.getMultitenantMode())
		));
	}

	@Override
	public void deleteClass(ClasseWithoutHierarchy dbClass) {
		logger.info(String.format("SELECT * FROM cm_delete_class('%s');", dbClass.getName()));
		jdbcTemplate.queryForObject("SELECT * FROM cm_delete_class(?)", Object.class, dbClass.getName());
		invalidateAllCache();
	}

}
