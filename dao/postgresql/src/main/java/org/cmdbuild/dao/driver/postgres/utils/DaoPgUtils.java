package org.cmdbuild.dao.driver.postgres.utils;

import static java.lang.String.format;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.cmdbuild.dao.driver.postgres.Const.DOMAIN_PREFIX;

import org.cmdbuild.dao.driver.postgres.Const.SystemAttributes;
import org.cmdbuild.dao.query.clause.alias.Alias;
import org.cmdbuild.dao.entrytype.EntryType;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class DaoPgUtils {

	@Deprecated
	public static String quoteAttribute(Alias tableAlias, SystemAttributes attribute) {
		return quoteAttribute(tableAlias, attribute.getDBName());
	}

	@Deprecated
	public static String quoteAttribute(Alias tableAlias, String name) {
		return format("%s.%s", aliasToQuotedSql(tableAlias), quoteSqlIdentifier(name));
	}

	@Deprecated
	public static String nameForSystemAttribute(Alias alias, SystemAttributes sa) {
		return nameForUserAttribute(alias, sa.getDBName());
	}

	@Deprecated
	public static String nameForUserAttribute(Alias alias, String name) {
		return format("%s__%s", alias.asString(), name).toLowerCase();
	}

	@Deprecated
	public static String aliasToQuotedSql(Alias alias) {
		return quoteSqlIdentifier(alias.asString());
	}

	public static String entryTypeToQuotedSqlIdentifier(EntryType entryType) {
		switch (entryType.getEtType()) {
			case CLASSE:
				return quoteSqlIdentifier(entryType.getName());
			case DOMAIN:
				return quoteSqlIdentifier(domainNameToSqlTable(entryType.getName()));
			case OTHER:
			default:
				throw unsupported("unsupported entry type = %s", entryType);
		}
	}

	public static String systemAttrToQuotedSqlIdentifier(SystemAttributes attribute) {
		return quoteSqlIdentifier(attribute.getDBName());
	}

	public static String quoteSqlIdentifier(String name) {
		if (name.matches("[a-z_][a-z0-9_]*") || name.matches("^\".*\"$")) {
			return name;
		} else {
			return format("\"%s\"", name.replace("\"", "\"\""));
		}
	}

	public static String sqlDomainTableToDomainName(String sqlDomainTable) {
		return checkNotBlank(sqlDomainTable).replaceAll("\"", "").replaceFirst("^" + Pattern.quote(DOMAIN_PREFIX), "");
	}

	public static String domainNameToSqlTable(String domainName) {
		return DOMAIN_PREFIX + checkNotBlank(domainName);
	}

}
