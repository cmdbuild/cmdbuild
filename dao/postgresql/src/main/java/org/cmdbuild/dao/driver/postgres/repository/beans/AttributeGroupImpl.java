/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.repository.beans;

import org.cmdbuild.dao.entrytype.AttributeGroup;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class AttributeGroupImpl implements AttributeGroup {

	private final String name, description;

	private AttributeGroupImpl(AttributeGroupImplBuilder builder) {
		this.name = checkNotBlank(builder.name);
		this.description = checkNotNull(builder.description);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "AttributeGroupImpl{" + "name=" + name + ", description=" + description + '}';
	}

	public static AttributeGroupImplBuilder builder() {
		return new AttributeGroupImplBuilder();
	}

	public static AttributeGroupImplBuilder copyOf(AttributeGroup source) {
		return new AttributeGroupImplBuilder()
				.withName(source.getName())
				.withDescription(source.getDescription());
	}

	public static class AttributeGroupImplBuilder implements Builder<AttributeGroupImpl, AttributeGroupImplBuilder> {

		private String name;
		private String description;

		public AttributeGroupImplBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public AttributeGroupImplBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		@Override
		public AttributeGroupImpl build() {
			return new AttributeGroupImpl(this);
		}

	}
}
