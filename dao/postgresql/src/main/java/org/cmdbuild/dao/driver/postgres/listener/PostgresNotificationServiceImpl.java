/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.listener;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.EventBus;
import static java.lang.String.format;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import static java.util.Arrays.asList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.PreDestroy;
import org.cmdbuild.dao.ConfigurableDataSource;
import org.cmdbuild.dao.driver.postgres.listener.PostgresNotificationEventService.PostgresNotificationEvent;
import org.cmdbuild.startup.PostStartup;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbExecutorUtils.shutdownQuietly;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.postgresql.PGConnection;
import org.postgresql.PGNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PostgresNotificationServiceImpl implements PostgresNotificationService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final List<String> channels = asList("cmevents");

	private final ConfigurableDataSource dataSource;
	private final EventBus eventBus;
	private final ScheduledExecutorService poller = Executors.newSingleThreadScheduledExecutor((r) -> new Thread(r, "PostgresNotificationListener"));

	private Connection connection;

	public PostgresNotificationServiceImpl(ConfigurableDataSource dataSource, PostgresNotificationEventService eventService) {
		this.dataSource = checkNotNull(dataSource);
		this.eventBus = eventService.getEventBus();
		logger.debug("ready");
	}

	@PostStartup
	public void startPolling() {
		poller.scheduleAtFixedRate(this::pollDatasourceForNotifications, 1, 1, TimeUnit.SECONDS);
	}

	@PreDestroy
	public void cleanup() {
		logger.debug("shutdown");
		shutdownQuietly(poller);
		closeConnection();
	}

	@Override
	public void sendNotification(String channel, String payload) {
		try (Connection thisConnection = dataSource.getConnection(); PreparedStatement statement = thisConnection.prepareStatement("SELECT pg_notify(?,?)")) {
			statement.setString(1, channel);
			statement.setString(2, payload);
			statement.execute();
		} catch (SQLException ex) {
			throw runtime(ex);
		}
	}

	private void pollDatasourceForNotifications() {
		logger.trace("pollDatasourceForNotifications");
		try {
			if (connection == null) {
				openConnection();
			}

			if (connection != null) {
				pollNotificationsFromConnection();
			}
		} catch (Throwable ex) {
			logger.error("error polling connection for notifications from postgres", ex);
			closeConnection();
		}
	}

	private void openConnection() throws SQLException {
		if (dataSource.isConfigured()) {

			logger.debug("open sql connection for pg notification listening");
			connection = dataSource.getInner().getConnection();

			for (String channel : channels) {
				try (Statement statement = connection.createStatement()) {
					statement.execute(format("LISTEN %s", channel));
				}
			}
		}
	}

	private void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception exx) {
			}
			connection = null;
		}
	}

	private void pollNotificationsFromConnection() throws SQLException {
		logger.trace("pollNotificationsFromConnection");

		logger.trace("run dummy query");
		try (Statement statement = connection.createStatement()) {
			statement.execute("");
		}

		logger.trace("get notifications");
		PGNotification notifications[] = connection.unwrap(PGConnection.class).getNotifications();
		if (notifications != null) {
			logger.trace("received {} notifications", notifications.length);
			for (PGNotification notification : notifications) {
				logger.trace("received pg notification = {}", notification);
				try {
					PostgresNotificationEvent event = new PostgresNotificationEventImpl(notification);
					logger.info("received pg notification event = {}", event);
					eventBus.post(event);
				} catch (Exception ex) {
					logger.error(format("error processing pg notification = %s", notification), ex);
				}
			}
		} else {

			logger.trace("received 0 notifications");
		}
	}

	private class PostgresNotificationEventImpl implements PostgresNotificationEvent {

		private final int pid;
		private final String channel, payload;

		public PostgresNotificationEventImpl(PGNotification notification) {
			this.pid = notification.getPID();
			this.channel = checkNotBlank(notification.getName());
			this.payload = checkNotBlank(notification.getParameter());
		}

		@Override
		public int getServerPid() {
			return pid;
		}

		@Override
		public String getChannel() {
			return channel;
		}

		@Override
		public String getPayload() {
			return payload;
		}

		@Override
		public String toString() {
			return "PostgresNotificationEventImpl{" + "pid=" + pid + ", channel=" + channel + ", payload=" + payload + '}';
		}

	}

}
