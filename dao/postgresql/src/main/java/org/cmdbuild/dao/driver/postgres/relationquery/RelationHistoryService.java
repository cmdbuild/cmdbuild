package org.cmdbuild.dao.driver.postgres.relationquery;

import org.cmdbuild.dao.entrytype.Domain;

public interface RelationHistoryService {

	GetRelationHistoryResponse getRelationHistory(String classId, Long cardId);

	GetRelationHistoryResponse getRelationHistory(String classId, Long cardId, Domain domain);

}
