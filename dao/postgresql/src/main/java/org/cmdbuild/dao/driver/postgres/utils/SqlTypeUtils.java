/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.utils;

import com.google.common.base.Joiner;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import com.google.common.base.Splitter;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.getOnlyElement;
import static java.lang.String.format;
import java.sql.SQLException;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.utils.date.DateUtils;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.beans.NullAttributeMetadata;
import org.cmdbuild.dao.driver.postgres.PostgreSQLArray;
import org.cmdbuild.dao.driver.postgres.SqlType;
import org.cmdbuild.dao.driver.postgres.SqlTypeName;
import org.cmdbuild.dao.beans.IdAndDescriptionImpl;
import org.cmdbuild.dao.entrytype.AttributeMetadata;
import org.cmdbuild.dao.entrytype.attributetype.BooleanAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ByteArrayAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ByteaArrayAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.CharAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.DateAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.DateTimeAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.DecimalAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.DoubleAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.RegclassAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.IntegerAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.IpAddressAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.IpAddressAttributeType.IpType;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.StringArrayAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.StringAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.TextAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.TimeAttributeType;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.isNullOrEmpty;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.LongAttributeType;
import org.cmdbuild.utils.lang.CmdbConvertUtils;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;
import org.postgresql.jdbc.PgArray;
import org.postgresql.util.PGobject;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.quoteSqlIdentifier;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.isBlank;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNullOrBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringNotBlank;

public class SqlTypeUtils {

	public static final String //			SQL_CAST_REGCLASS = "regclass",
			SQL_CAST_INET = "inet";

	public static SqlType parseSqlType(String sqlTypeString) {
		return doParseSqlType(sqlTypeString, NullAttributeMetadata.INSTANCE);
	}

	public static SqlType parseSqlType(String sqlTypeString, CardAttributeType.Meta meta) {
		return doParseSqlType(sqlTypeString, checkNotNull(meta));
	}

	private static final Pattern TYPE_PATTERN = Pattern.compile("(\\w+)(\\((\\d+(,\\d+)*)\\))?");

	private static SqlType doParseSqlType(String sqlTypeString, @Nullable CardAttributeType.Meta meta) {
		Matcher typeMatcher = TYPE_PATTERN.matcher(sqlTypeString);
		checkArgument(typeMatcher.find());
		SqlTypeName type = SqlTypeName.valueOf(typeMatcher.group(1).toLowerCase());
		List<String> params = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(nullToEmpty(typeMatcher.group(3)));
		SqlType sqlType = new SimpleSqlType(type, params, meta);
		return sqlType;
	}

	public static @Nullable
	String getSystemToSqlCastOrNull(CardAttributeType attributeType) {
		return SqlTypeUtils.getSystemToSqlCastOrNull(attributeTypeToSqlTypeName(attributeType));
	}

	public static String addSqlCastIfRequired(CardAttributeType attributeType, String expr) {
		String cast = getSystemToSqlCastOrNull(attributeType);
		if (isNotBlank(cast)) {
			expr += "::" + cast;
		}
		return expr;
	}

	public static String getSqlTypeString(CardAttributeType attributeType) {
		return attributeTypeToSqlType(attributeType).toSqlTypeString();
	}

//	public static SqlValueConverter getConverter(CardAttributeType attributeType) {
//		return getConverter(attributeTypeToSqlTypeName(attributeType));
//	}
	private static Object wrapJsonForJdbc(@Nullable Object value) {
		try {
			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(toStringOrNull(value));
			return jsonObject;
		} catch (SQLException ex) {
			throw new DaoException(ex, "error wrapping to postgres json, source value = %s", value);
		}
	}

	public static CardAttributeType<?> createAttributeType(String sqlTypeString, CardAttributeType.Meta meta) {
		return parseSqlType(sqlTypeString, meta).toAttributeType();
	}

	public static boolean isVoidSqlType(String sqlTypeString) {
		return "void".equalsIgnoreCase(sqlTypeString);
	}

	public static CardAttributeType<?> createAttributeType(String sqlTypeString) {
		return parseSqlType(sqlTypeString).toAttributeType();
	}

	private static CardAttributeType<?> sqlTypeToAttributeType(SqlType sqlType) {
		try {
			CardAttributeType.Meta meta = sqlType.getMetadata();
			switch (sqlType.getType()) {
				case bool:
					return new BooleanAttributeType();
				case date:
					return new DateAttributeType();
				case float8:
					return new DoubleAttributeType();
				case inet:
					if (meta instanceof AttributeMetadata) {
						String typeValue = ((AttributeMetadata) meta).get(AttributeMetadata.IP_TYPE);
						IpType type = IpType.of(typeValue, IpType.IPV4);
						return new IpAddressAttributeType(type);
					}
					throw new UnsupportedOperationException("unsupported inet type without AttributeMetadata");//TODO is this correct?
				case int4:
					return new IntegerAttributeType();
				case int8:
					if (meta.isLookup()) {
						return new LookupAttributeType(meta.getLookupType());
					} else if (meta.isReference()) {
						return new ReferenceAttributeType(meta.getDomain());
					} else if (meta.isForeignKey()) {
						return new ForeignKeyAttributeType(meta.getForeignKeyDestinationClassName());
					} else {
						return LongAttributeType.INSTANCE;
					}
				case numeric:
					if (sqlType.getParams().size() == 2) {
						return new DecimalAttributeType(Integer.valueOf(sqlType.getParams().get(0)), Integer.valueOf(sqlType.getParams().get(1)));
					} else {
						return new DecimalAttributeType();
					}
				case regclass:
					return RegclassAttributeType.INSTANCE;
				case text:
					return new TextAttributeType();
				case time:
					return new TimeAttributeType();
				case timestamp:
					return new DateTimeAttributeType();
				case varchar:
					if (sqlType.hasParams()) {
						return new StringAttributeType(Integer.valueOf(getOnlyElement(sqlType.getParams())));
					} else {
						return new StringAttributeType();
					}
				case _varchar:
					return new StringArrayAttributeType();
				case jsonb:
					return StringAttributeType.newJsonStringAttributeType();
				case bpchar:
					if (sqlType.getParams().size() == 1) {
						int value = Integer.valueOf(sqlType.getParams().get(0));
						checkArgument(value > 0);
						if (value == 1) {
							return new CharAttributeType();
						} else {
							return new StringAttributeType(value);
						}
					} else {
						return new CharAttributeType();
					}
				case bytea:
					return new ByteArrayAttributeType();
				case _bytea:
					return new ByteaArrayAttributeType();
				default:
					throw new UnsupportedOperationException(format("unsupported conversion of sql type = %s to attribute type", sqlType.getType()));
			}
		} catch (Exception ex) {
			throw new DaoException(ex, "error converting sql type = %s to attribute type", sqlType);
		}
	}

	public static SqlTypeName attributeTypeToSqlTypeName(CardAttributeType attributeType) {
		try {
			switch (attributeType.getName()) {
				case BOOLEAN:
					return SqlTypeName.bool;
				case BYTEARRAY:
					return SqlTypeName.bytea;
				case CHAR:
					return SqlTypeName.bpchar;
				case DATE:
					return SqlTypeName.date;
				case DECIMAL:
					return SqlTypeName.numeric;
				case DOUBLE:
					return SqlTypeName.float8;
				case REGCLASS:
					return SqlTypeName.regclass;
				case INTEGER:
					return SqlTypeName.int4;
				case FOREIGNKEY:
				case LOOKUP:
				case REFERENCE:
				case LONG:
					return SqlTypeName.int8;
				case INET:
					return SqlTypeName.inet;
				case STRING:
					if (((StringAttributeType) attributeType).isJson()) {
						return SqlTypeName.jsonb;
					} else {
						return SqlTypeName.varchar;
					}
				case STRINGARRAY:
					return SqlTypeName._varchar;
				case BYTEAARRAY:
					return SqlTypeName._bytea;
				case TEXT:
					return SqlTypeName.text;
				case TIME:
					return SqlTypeName.time;
				case TIMESTAMP:
					return SqlTypeName.timestamp;
				case UNKNOWN:
					return SqlTypeName.undefined;
				default:
					throw new UnsupportedOperationException(format("unsupported conversion of attribute type = %s to sql type", attributeType.getName()));
			}
		} catch (Exception ex) {
			throw new DaoException(ex, "error converting attribute type = %s to sql type", attributeType);
		}
	}

	public static SqlType attributeTypeToSqlType(CardAttributeType attributeType) {
		try {
			SqlTypeName sqlTypeName = attributeTypeToSqlTypeName(attributeType);
			switch (sqlTypeName) {
				case bpchar:
					return new SimpleSqlType(sqlTypeName, singletonList("1"));
				case numeric:
					DecimalAttributeType decimalAttributeType = (DecimalAttributeType) attributeType;
					if (decimalAttributeType.hasPrecisionAndScale()) {
						return new SimpleSqlType(sqlTypeName, asList(decimalAttributeType.getPrecision().toString(), decimalAttributeType.getScale().toString()));
					} else {
						return new SimpleSqlType(sqlTypeName);
					}
				case varchar:
					StringAttributeType stringAttributeType = (StringAttributeType) attributeType;
					if (stringAttributeType.hasLength()) {
						return new SimpleSqlType(sqlTypeName, singletonList(Integer.toString(stringAttributeType.getLength())));
					} else {
						return new SimpleSqlType(sqlTypeName);
					}
				default:
					return new SimpleSqlType(sqlTypeName);
			}
		} catch (Exception ex) {
			throw new DaoException(ex, "error converting attribute type = %s to sql type", attributeType);
		}
	}

	private static class SimpleSqlType implements SqlType {

		private final SqlTypeName type;
		private final List<String> params;
		private final CardAttributeType.Meta metadata;
//		private final SqlValueConverter converter;
		private final String sqlCast;

		public SimpleSqlType(SqlTypeName type) {
			this(type, emptyList(), NullAttributeMetadata.INSTANCE);
		}

		public SimpleSqlType(SqlTypeName type, List<String> params) {
			this(type, params, NullAttributeMetadata.INSTANCE);
		}

		public SimpleSqlType(SqlTypeName type, List<String> params, CardAttributeType.Meta metadata) {
			this.type = checkNotNull(type);
			this.metadata = checkNotNull(metadata);
			this.params = copyOf(checkNotNull(params));
			checkArgument(params.stream().allMatch(not(StringUtils::isBlank)), "found invalid blank param in params = %s", params);
//			converter = getConverter(type);
			sqlCast = SqlTypeUtils.getSystemToSqlCastOrNull(type);
		}

		@Override
		public SqlTypeName getType() {
			return type;
		}

		@Override
		public List<String> getParams() {
			return params;
		}

		@Override
		public CardAttributeType.Meta getMetadata() {
			return metadata;
		}

		@Override
		public String toSqlTypeString() {
			if (isNullOrEmpty(params)) {
				return type.name();
			} else {
				return format("%s(%s)", type.name(), Joiner.on(",").join(params));
			}
		}

		@Override
		public String getSqlCast() {
			return checkNotBlank(sqlCast);
		}

		@Override
		public boolean hasSqlCast() {
			return sqlCast != null;
		}

		@Override
		public CardAttributeType<?> toAttributeType() {
			return sqlTypeToAttributeType(this);
		}

	}

	@Nullable
	public static String getSystemToSqlCastOrNull(SqlTypeName sqlType) {
		switch (sqlType) {
			case inet:
				return SQL_CAST_INET;
			case _bytea:
				return "bytea[]";
			case regclass:
				return "regclass";
			default:
				return null;
		}
	}

	public static @Nullable
	Object systemToSql(CardAttributeType attributeType, @Nullable Object value) {
		switch (attributeType.getName()) {
			case REGCLASS:
				return isNullOrBlank(value) ? null : quoteSqlIdentifier(toStringNotBlank(value));
			case STRING:
				if (((StringAttributeType) attributeType).isJson()) {
					return wrapJsonForJdbc(value);
				} else {
					return toStringOrNull(value);
				}
			case DATE:
				return DateUtils.toSqlDate(value);
			case TIME:
				return DateUtils.toSqlTime(value);
			case TIMESTAMP:
				return DateUtils.toSqlTimestamp(value);
			case LOOKUP:
			case REFERENCE:
			case FOREIGNKEY:
				if (value instanceof IdAndDescriptionImpl) {
					return IdAndDescriptionImpl.class.cast(value).getId();
				} else {
					return value;
				}
			case STRINGARRAY:
				return new PostgreSQLArray(CmdbConvertUtils.convert(value, String[].class));
			case BYTEAARRAY:
				return new PostgreSQLArray(CmdbConvertUtils.convert(value, byte[][].class));
			default:
				return value;
		}
	}

	@Deprecated
	public static @Nullable
	Object systemToSql(SqlTypeName type, @Nullable Object value) {
		switch (type) {
			case regclass:
				return quoteSqlIdentifier(toStringOrNull(value));
//			case STRING:
//				if (((StringAttributeType) attributeType).isJson()) {
//					return wrapJsonForJdbc(value); //TODO
//				} else {
//					return toStringOrNull(value);
//				}
			case date:
				return DateUtils.toSqlDate(value);
			case time:
				return DateUtils.toSqlTime(value);
			case timestamp:
				return DateUtils.toSqlTimestamp(value);
			case int8:
				if (value instanceof IdAndDescriptionImpl) {
					return IdAndDescriptionImpl.class.cast(value).getId();
				} else {
					return value;
				}
			case _varchar:
				return new PostgreSQLArray(CmdbConvertUtils.convert(value, String[].class));
			case _bytea:
				return new PostgreSQLArray(CmdbConvertUtils.convert(value, byte[][].class));
			default:
				return value;
		}
	}

	public static @Nullable
	Object systemToSql(@Nullable Object value) {
		if (value == null) {
			return null;
		} else if (DateUtils.isDateTime(value)) {
			return DateUtils.toSqlTimestamp(value);
		} else {
			return value;
		}
	}

//	@Deprecated
	public static @Nullable
	Object sqlToSystem(SqlTypeName type, @Nullable Object value) {
		switch (type) {
			case date:
				return DateUtils.toDate(value);
			case time:
				return DateUtils.toTime(value);
			case timestamp:
				return DateUtils.toDateTime(value);
			case _varchar:
				if (value instanceof PgArray) {
					return (String[][]) pgToJavaArray((PgArray) value);
				} else {
					return value;
				}
			case _bytea:
				if (value instanceof PgArray) {
					return (byte[][]) pgToJavaArray((PgArray) value);
				} else {
					return value;
				}
			default:
				return value;
		}
	}

	private static Object pgToJavaArray(PgArray pgArray) {
		try {
			return pgArray.getArray();
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

}
