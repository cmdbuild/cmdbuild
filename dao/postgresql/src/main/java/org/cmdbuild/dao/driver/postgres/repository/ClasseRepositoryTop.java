/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.repository;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import com.google.common.eventbus.Subscribe;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.driver.repository.ClassStructureChangedEvent;
import org.cmdbuild.dao.driver.repository.ClasseRepository;
import org.cmdbuild.dao.driver.repository.ClasseWithoutHierarchyRepository;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.event.AttributeGroupModifiedEvent;
import org.cmdbuild.dao.event.AttributeModifiedEvent;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.ClassDefinition;
import org.cmdbuild.dao.driver.repository.DaoEventService;
import org.cmdbuild.dao.entrytype.ClasseWithoutHierarchy;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.context.annotation.Primary;
import static org.cmdbuild.dao.graph.ClasseHierarchyUtils.attachClassHierarchy;

@Component
@Primary
public class ClasseRepositoryTop implements ClasseRepository {

	private final ClasseWithoutHierarchyRepository inner;
	private final DaoEventService eventService;

	private final Holder<List<Classe>> allClassesCache;
	private final CmdbCache<String, Optional<Classe>> classesCacheByName;
	private final CmdbCache<String, Optional<Classe>> classesCacheByOid;

	public ClasseRepositoryTop(CacheService cacheService, ClasseRepositoryLower inner, DaoEventService eventService) {
		this.inner = checkNotNull(inner);

		allClassesCache = cacheService.newHolder("org.cmdbuild.database.classes.upper.all", CacheService.CacheConfig.SYSTEM_OBJECTS);
		classesCacheByName = cacheService.newCache("org.cmdbuild.database.classes.upper.by_name", CacheService.CacheConfig.SYSTEM_OBJECTS);
		classesCacheByOid = cacheService.newCache("org.cmdbuild.database.classes.upper.by_oid", CacheService.CacheConfig.SYSTEM_OBJECTS);

		this.eventService = checkNotNull(eventService);
		eventService.getEventBus().register(new Object() {
			@Subscribe
			public void handleClassStructureChangedEvent(ClassStructureChangedEvent event) {
				invalidateOurCache();
			}

			@Subscribe
			public void handleAttributeModifiedEvent(AttributeModifiedEvent event) {
				if (event.getOwner() instanceof Classe) {
					invalidateAllCache();
				}
			}

			@Subscribe
			public void handleAttributeGroupModifiedEvent(AttributeGroupModifiedEvent event) {
				invalidateAllCache();
			}
		});
	}

	private void invalidateAllCache() {
		eventService.post(ClassStructureChangedEvent.INSTANCE);
	}

	private void invalidateOurCache() {
		allClassesCache.invalidate();
		classesCacheByName.invalidateAll();
		classesCacheByOid.invalidateAll();
	}

	@Override
	public List<Classe> getAllClasses() {
		return allClassesCache.get(this::doGetAllClasses);
	}

	private List<Classe> doGetAllClasses() {
		return attachClassHierarchy(inner.getAllClasses());
	}

	@Override
	public Classe createClass(ClassDefinition definition) {
		ClasseWithoutHierarchy classe = inner.createClass(definition);
		return getClasse(classe.getName());
	}

	@Override
	public Classe updateClass(ClassDefinition definition) {
		ClasseWithoutHierarchy classe = inner.updateClass(definition);
		return getClasse(classe.getName());
	}

	@Override
	public void deleteClass(Classe dbClass) {
		inner.deleteClass(dbClass);
	}

	@Override
	@Nullable
	public Classe getClasseOrNull(long oid) {
		return classesCacheByOid.get(String.valueOf(oid), () -> getAllClasses().stream().filter((c) -> equal(c.getOid(), oid)).collect(toOptional())).orElse(null);
	}

	@Override
	public @Nullable
	Classe getClasseOrNull(String name) {
		checkNotBlank(name);
		return classesCacheByName.get(name, () -> getAllClasses().stream().filter((c) -> equal(c.getName(), name)).collect(toOptional())).orElse(null);
	}

}
