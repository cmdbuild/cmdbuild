package org.cmdbuild.dao.driver.postgres.relationquery;

import com.google.common.base.Optional;
import org.cmdbuild.dao.entrytype.Domain;

public interface RelationSingleService {

	Optional<RelationInfo> getRelationInfo(Domain domain, Long id);

}
