/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.listener;

import com.google.common.eventbus.EventBus;
import java.util.Map;
import static org.cmdbuild.utils.json.CmJsonUtils.MAP_OF_OBJECTS;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;

public interface PostgresNotificationEventService {

	EventBus getEventBus();

	interface PostgresNotificationEvent {

		int getServerPid();

		String getChannel();

		String getPayload();

		default Map<String, Object> getPayloadAsMap() {
			return fromJson(getPayload(), MAP_OF_OBJECTS);
		}
	}

}
