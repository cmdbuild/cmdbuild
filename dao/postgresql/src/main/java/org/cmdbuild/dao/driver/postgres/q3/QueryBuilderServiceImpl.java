/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.q3;

import org.cmdbuild.dao.core.q3.WhereOperator;
import org.cmdbuild.dao.core.q3.ResultRow;
import org.cmdbuild.dao.core.q3.PreparedQuery;
import org.cmdbuild.dao.core.q3.QueryBuilder;
import org.cmdbuild.dao.core.q3.QueryBuilderService;
import com.google.common.base.Joiner;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.notNull;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.MoreCollectors.toOptional;
import com.google.common.collect.Ordering;
import static java.lang.String.format;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.common.utils.PagedElements.hasLimit;
import static org.cmdbuild.common.utils.PagedElements.hasOffset;
import static org.cmdbuild.cql.CqlUtils.compileAndCheck;
import org.cmdbuild.cql.compiler.impl.ClassDeclarationImpl;
import org.cmdbuild.cql.compiler.impl.CqlQueryImpl;
import org.cmdbuild.cql.compiler.impl.FieldImpl;
import org.cmdbuild.cql.compiler.impl.GroupImpl;
import org.cmdbuild.cql.compiler.impl.WhereImpl;
import org.cmdbuild.cql.compiler.where.Field;
import org.cmdbuild.dao.DaoException;
import org.cmdbuild.dao.beans.CMRelation;
import static org.cmdbuild.dao.beans.CMRelation.ATTR_CODE1;
import static org.cmdbuild.dao.beans.CMRelation.ATTR_CODE2;
import static org.cmdbuild.dao.beans.CMRelation.ATTR_DESCRIPTION1;
import static org.cmdbuild.dao.beans.CMRelation.ATTR_DESCRIPTION2;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import org.cmdbuild.dao.beans.IdAndDescription;
import org.cmdbuild.dao.beans.IdAndDescriptionImpl;
import org.cmdbuild.dao.beans.LookupValue;
import org.cmdbuild.dao.beans.LookupValueImpl;
import org.cmdbuild.dao.beans.RelationImpl;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS1;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDCLASS2;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDDOMAIN;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ1;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDOBJ2;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_STATUS;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_STATUS_A;
import static org.cmdbuild.dao.constants.SystemAttributes.DOMAIN_RESERVED_ATTRIBUTES;
import static org.cmdbuild.dao.constants.SystemAttributes.PROCESS_CLASS_RESERVED_ATTRIBUTES;
import static org.cmdbuild.dao.constants.SystemAttributes.SIMPLE_CLASS_RESERVED_ATTRIBUTES;
import static org.cmdbuild.dao.constants.SystemAttributes.STANDARD_CLASS_RESERVED_ATTRIBUTES;
import org.cmdbuild.dao.driver.PostgresService;
import static org.cmdbuild.dao.core.q3.DaoService.ALL;
import static org.cmdbuild.dao.core.q3.DaoService.COUNT;
import static org.cmdbuild.dao.core.q3.DaoService.ROW_NUMBER;
import static org.cmdbuild.dao.core.q3.QueryBuilder.EQ;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.sqlDomainTableToDomainName;
import org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.addSqlCastIfRequired;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.attributetype.AttributeTypeName;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.orm.CardMapper;
import org.cmdbuild.dao.orm.CardMapperService;
import static org.cmdbuild.dao.utils.AttributeConversionUtils.rawToSystem;
import org.cmdbuild.data.filter.AttributeFilter;
import org.cmdbuild.data.filter.AttributeFilterCondition;
import org.cmdbuild.data.filter.CmdbFilter;
import org.cmdbuild.data.filter.CmdbSorter;
import org.cmdbuild.data.filter.CqlFilter;
import org.cmdbuild.data.filter.FulltextFilter;
import org.cmdbuild.data.filter.RelationFilter;
import org.cmdbuild.data.filter.RelationFilterCardInfo;
import org.cmdbuild.data.filter.RelationFilterRule;
import org.cmdbuild.data.filter.SorterElement;
import static org.cmdbuild.data.filter.utils.CmdbSorterUtils.noopSorter;
import org.cmdbuild.ecql.EcqlService;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.systemToSql;
import org.cmdbuild.dao.entrytype.EntryType;
import static org.cmdbuild.data.filter.utils.CmdbSorterUtils.toJsonString;
import org.cmdbuild.utils.lang.CmdbCollectionUtils;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBooleanOrDefault;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.firstNotNullOrNull;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.entryTypeToQuotedSqlIdentifier;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.quoteSqlIdentifier;
import org.cmdbuild.data.filter.CompositeFilter;
import org.cmdbuild.data.filter.beans.CmdbFilterImpl;
import org.cmdbuild.data.filter.beans.CqlFilterImpl;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.queue;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.getClassOfNullable;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_IDTENANT;

@Component
public class QueryBuilderServiceImpl implements QueryBuilderService {

	private final static String WHERE_ELEMENT_FOR_ROW_NUMBER = "WHERE_ELEMENT_FOR_ROW_NUMBER";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final PostgresService database;
	private final CardMapperService mapper;
	private final EcqlService ecqlService;

	public QueryBuilderServiceImpl(PostgresService database, CardMapperService mapper, EcqlService ecqlService) {
		this.database = checkNotNull(database);
		this.mapper = checkNotNull(mapper);
		this.ecqlService = checkNotNull(ecqlService);
	}

	@Override
	public QueryBuilder query() {
		return new QueryBuilderImpl();
	}

	private class QueryBuilderImpl implements QueryBuilder {

		private final List<SelectArg> select = list();
		private EntryType from;
		private final List<WhereArg> where = list();
		private Long offset, limit;
		private CmdbSorter sorter = noopSorter();
		private final List<CmdbFilter> filters = list();
		private boolean selectRowNumber = false, activeCardsOnly = true, count = false;
		private CardMapper cardMapper;

		@Override
		public QueryBuilder select(Collection<String> attrs) {
			attrs.stream().map((a) -> new SelectArg(a, quoteSqlIdentifier(a))).forEach(select::add);
			return this;
		}

		@Override
		public QueryBuilder selectCount() {
			count = true;
			return this;
		}

		@Override
		public RowNumberQueryBuilder selectRowNumber() {
			selectRowNumber = true;
			return new RowNumberQueryBuilderImpl();
		}

		@Override
		public QueryBuilder where(String attr, WhereOperator operator, Object... params) {
			where.add(new WhereArg(attr, operator, emptyMap(), list(params)));
			return this;
		}

		private class RowNumberQueryBuilderImpl implements RowNumberQueryBuilder {

			@Override
			public RowNumberQueryBuilder where(String attr, WhereOperator operator, Object... params) {
				where.add(new WhereArg(attr, operator, map(WHERE_ELEMENT_FOR_ROW_NUMBER, true), list(params)));
				return this;
			}

			@Override
			public QueryBuilder then() {
				return QueryBuilderImpl.this;
			}

		}

		@Override
		public QueryBuilder selectExpr(String name, String expr) {
			select.add(new SelectArg(name, expr));
			return this;
		}

		@Override
		public QueryBuilder selectMatchFilter(String name, CmdbFilter filter) {
			select.add(new SelectArg(name, filter));
			return this;
		}

		@Override
		public QueryBuilder from(Class model) {
			cardMapper = mapper.getMapperForModel(model);
			return from(cardMapper.getClassId());
		}

		@Override
		public QueryBuilder from(String classe) {
			return from(database.getClasse(classe));
		}

		@Override
		public QueryBuilder from(Classe classe) {
			return doFrom(classe);
		}

		@Override
		public QueryBuilder from(Domain domain) {
			return doFrom(domain);
		}

		@Override
		public QueryBuilder where(CmdbFilter filter) {
			filters.add(checkNotNull(filter));
			return this;
		}

		@Override
		public QueryBuilder where(PreparedQuery query) {
			PreparedQueryImpl src = (PreparedQueryImpl) query;
			src.where.forEach((w) -> where.add(new WhereArg(w.getExpr(), SpecialOperators.EXPR, w.getMeta(), w.getParams())));
			return this;
		}

		@Override
		public QueryBuilder includeHistory() {
			activeCardsOnly = false;
			return this;
		}

		@Override
		public QueryBuilder whereExpr(String expr, Object... params) {
			return whereExpr(expr, list(params));
		}

		@Override
		public QueryBuilder whereExpr(String expr, Collection params) {
			where.add(new WhereArg(wrapExprWithBrackets(expr), SpecialOperators.EXPR, emptyMap(), CmdbCollectionUtils.toList(params)));
			return this;
		}

		@Override
		public QueryBuilder orderBy(CmdbSorter sort) {
			sorter = checkNotNull(sort);
			return this;
		}

		@Override
		public QueryBuilder offset(@Nullable Long offset) {
			this.offset = offset;
			return this;
		}

		@Override
		public QueryBuilder limit(@Nullable Long limit) {
			this.limit = limit;
			return this;
		}

		@Override
		public PreparedQuery build() {
			return new QueryBuilderProcessor(this).doBuild();
		}

		@Override
		public List<ResultRow> run() {
			return build().run();
		}
//

		private QueryBuilder doFrom(EntryType entryType) {
			checkNotNull(entryType);
			checkArgument(entryType.isClasse() || entryType.isDomain());
			from = entryType;
			return this;
		}

	}

	private class QueryBuilderProcessor {

		private final AliasBuilder aliasBuilder = new AliasBuilder();
		private final List<SelectElement> select = list();
		private EntryType from;
		private final String fromAlias, rowNumberSubExprAlias = aliasBuilder.buildAlias("x");
		private final List<WhereElement> where = list();
		private final Long offset, limit;
		private final CmdbSorter sorter;
		private final boolean selectRowNumber, activeCardsOnly, count, selectAll;
		private final CardMapper cardMapper;
		private final Map<String, CqlQueryImpl> processedCqlFilters = map();

		private QueryBuilderProcessor(QueryBuilderImpl source) {
			from = source.from;
			offset = source.offset;
			limit = source.limit;
			sorter = source.sorter;
			activeCardsOnly = source.activeCardsOnly;
			count = source.count;
			cardMapper = source.cardMapper;
			selectRowNumber = source.selectRowNumber;

			processCqlFilters(source.filters);

			checkNotNull(from, "from param is null");
			fromAlias = aliasBuilder.buildAlias(from.getName());

			List<SelectArg> selectArgs = list(source.select);
			selectAll = selectArgs.removeIf((s) -> equal(s.getName(), ALL));
			processSelectArgs(selectArgs);
			selectReservedAttributes();
			selectAttrsForSorter();
			selectExtendedAttrs();

			processWhereArgs(source.where);
			processFilters(source.filters);
			addStandardWhereArgs();
		}

		private void processCqlFilters(List<CmdbFilter> filters) {
			Queue<CmdbFilter> toProcess = queue(filters);
			List<CmdbFilter> allFilters = list();
			while (!toProcess.isEmpty()) {
				CmdbFilter filter = toProcess.poll();
				allFilters.add(filter);
				if (filter.hasCompositeFilter()) {
					toProcess.addAll(filter.getCompositeFilter().getElements());
				}
			}
			allFilters.stream().filter(CmdbFilter::hasCqlFilter).map(CmdbFilter::getCqlFilter).forEach(f -> processedCqlFilters.put(cqlFilterKey(f), compileCql(f)));

			List<Classe> fromsFromCql = processedCqlFilters.values().stream().map((cql) -> {
				ClassDeclarationImpl mainClass = cql.getFrom().mainClass();
				Classe cqlFrom = mainClass.getId() > 0 ? database.getClasse(mainClass.getId()) : database.getClasse(mainClass.getName());
				if (!equal(cqlFrom, from)) {
					checkArgument(from == null || (from instanceof Classe && ((Classe) from).isAncestorOf(cqlFrom)), "invalid cql from = %s (is not an ancestor of this class = %s)", cqlFrom, from);
					return cqlFrom;
				} else {
					return null;
				}
			}).filter(Objects::nonNull).distinct().collect(toList());
			checkArgument(fromsFromCql.size() <= 1, "error processing cql filter: multiple cql filters with incompatible FROM class expr = %s", fromsFromCql);
			if (!fromsFromCql.isEmpty()) {
				from = getOnlyElement(fromsFromCql);
			}
		}

		private void processSelectArgs(List<SelectArg> list) {
			list.forEach(this::processSelectArg);
		}

		private void processSelectArg(SelectArg arg) {
			if (arg.getExpr() instanceof String) {
				select.add(SelectElement.builder()
						.withAlias(aliasBuilder.buildAlias(arg.getName()))
						.withExpr((String) arg.getExpr())
						.withMeta(arg.getMeta())
						.withName(arg.getName())
						.withParams(arg.getParams())
						.build());
			} else if (arg.getExpr() instanceof CmdbFilter) {
				WhereElement whereElement = compactWhereElements(buildWheresForFilter((CmdbFilter) arg.getExpr()));
				select.add(SelectElement.builder()
						.withName(arg.getName())
						.withExpr(wrapExprWithBrackets(whereElement.getExpr()))
						.withAlias(aliasBuilder.buildAlias(arg.getName()))
						.withParams(whereElement.getParams())
						.build());
			} else {
				throw new DaoException("unsupported select arg expr type = %s (%s)", arg.getExpr(), getClassOfNullable(arg.getExpr()).getName());
			}
		}

		private void selectReservedAttributes() {
			if (count) {
				if (select.isEmpty()) {
					selectAttr(ATTR_ID);
				}
			} else {
				Collection<String> reservedAttrs;
				if (from.isClasse()) {
					if (((Classe) from).isStandardClass()) {
						if (((Classe) from).isProcess()) {
							reservedAttrs = PROCESS_CLASS_RESERVED_ATTRIBUTES;//TODO: fix this, standardize and move ws customization back to flow/commons
						} else {
							reservedAttrs = STANDARD_CLASS_RESERVED_ATTRIBUTES;
						}
					} else {
						reservedAttrs = SIMPLE_CLASS_RESERVED_ATTRIBUTES;
					}
					if (((Classe) from).hasMultitenantEnabled()) {
						reservedAttrs = set(reservedAttrs).with(ATTR_IDTENANT);
					}
				} else if (from.isDomain()) {
					reservedAttrs = DOMAIN_RESERVED_ATTRIBUTES;
				} else {
					throw unsupported("unsuppoted 'from' type = %s", from);
				}

				if (selectAll) {
					select.removeIf((a) -> equal(a.getName(), ALL));

					reservedAttrs.forEach(this::selectAttr);

					if (from.isDomain()) {
						Domain domain = (Domain) from;
						selectExpr(ATTR_DESCRIPTION1, buildReferenceDescExpr(domain.getSourceClass(), quoteSqlIdentifier(ATTR_IDOBJ1)));
						selectExpr(ATTR_DESCRIPTION2, buildReferenceDescExpr(domain.getTargetClass(), quoteSqlIdentifier(ATTR_IDOBJ2)));
						selectExpr(ATTR_CODE1, buildReferenceCodeExpr(domain.getSourceClass(), quoteSqlIdentifier(ATTR_IDOBJ1)));
						selectExpr(ATTR_CODE2, buildReferenceCodeExpr(domain.getTargetClass(), quoteSqlIdentifier(ATTR_IDOBJ2)));
					}

					from.getCoreAttributes().stream()
							.map(Attribute::getName)
							.filter(not(reservedAttrs::contains))
							.sorted(Ordering.natural())
							.forEach(this::selectAttr);
				}

				reservedAttrs.stream().filter(not(this::selectHasAttrWithName)).forEach(this::selectAttr);
			}
		}

		private SelectElement selectAttr(String attr) {
			SelectElement element = SelectElement.build(attr, quoteSqlIdentifier(attr), aliasBuilder.buildAlias(attr));
			select.add(element);
			return element;
		}

		private void selectExpr(String name, String expr) {
			select.add(SelectElement.build(name, expr, aliasBuilder.buildAlias(name)));
		}

		private void selectAttrsForSorter() {
			sorter.getElements().forEach((s) -> {
				if (!select.stream().anyMatch((a) -> a.getName().equals(s.getProperty()))) {
					selectAttr(s.getProperty());
				}
			});
		}

		private void selectExtendedAttrs() {
			list(select).forEach(this::selectExtendedAttrs);
		}

		private void selectExtendedAttrs(SelectElement a) {
			if (from.hasAttribute(a.getName())) {
				selectExtendedAttrs(from.getAttribute(a.getName()));
			}
		}

		private void selectExtendedAttrs(Attribute a) {
			switch (a.getType().getName()) {
				case REFERENCE:
				case FOREIGNKEY:
					selectExtendedAttr(buildDescAttrName(a.getName()), buildDescAttrExprForReference(a));
					break;
				case LOOKUP:
					selectExtendedAttr(buildDescAttrName(a.getName()), buildDescAttrExprForLookup(a));
					selectExtendedAttr(buildCodeAttrName(a.getName()), buildCodeAttrExprForLookup(a));
					break;
			}
		}

		private SelectElement buildExtendedAttr(String name, String expr) {
			return SelectElement.build(name, expr, aliasBuilder.buildAlias(name));
		}

		private void selectExtendedAttr(String name, String expr) {
			select.add(buildExtendedAttr(name, expr));
		}

		private void processWhereArgs(List<WhereArg> list) {
			list.forEach(this::processWhereArg);
		}

		private void processWhereArg(WhereArg arg) {
			if (arg.getOperator() instanceof WhereOperator) {
				SelectElement attr = getAttrWithAliasForWhere(arg.getExpr());

				Function<SelectElement, String> attrExprExtractor = SelectElement::getExpr;

				if (toBooleanOrDefault(arg.getMeta().get(WHERE_ELEMENT_FOR_ROW_NUMBER), false)) {
					attrExprExtractor = (a) -> format("%s.%s", rowNumberSubExprAlias, a.getAlias());
				}

				where.add(buildWhere(attr, attrExprExtractor, (WhereOperator) arg.getOperator(), arg.getMeta(), arg.getParams()));
			} else if (arg.getOperator() instanceof SpecialOperators) {
				switch ((SpecialOperators) arg.getOperator()) {
					case EXPR:
						where.add(new WhereElement(arg.getExpr(), arg.getMeta(), arg.getParams()));
						break;
					default:
						throw new DaoException("unsupported special operator = %s", arg.getOperator());
				}
			}
		}

		private void addStandardWhereArgs() {
			if (activeCardsOnly && (from.isDomain() || (from.isClasse() && ((Classe) from).isStandardClass()))) {
				where.add(buildWhere(ATTR_STATUS, EQ, ATTR_STATUS_A));
			}
		}

		private void processFilters(List<CmdbFilter> list) {
			list.forEach((filter) -> {
				if (filter.hasEcqlFilter()) {
					String cqlExpr = ecqlService.prepareCqlExpression(filter.getEcqlFilter().getEcqlId(), filter.getEcqlFilter().getJsContext());
					filter = CmdbFilterImpl.copyOf(filter).withEcqlFilter(null).withCqlFilter(new CqlFilterImpl(cqlExpr)).build();
				}
				checkArgument(!filter.hasFunctionFilter(), "TODO: function filter is not supported yet");
				buildWheresForFilter(filter).forEach(where::add);
			});
		}

		private SelectElement getAttrWithAliasForWhere(String attr) {
			SelectElement attrWithAlias = select.stream().filter((a) -> a.getName().equals(attr)).findFirst()
					.orElse(SelectElement.build(attr, quoteSqlIdentifier(attr), quoteSqlIdentifier(attr)));
			if (from.hasAttribute(attr) && from.getAttribute(attr).isOfType(AttributeTypeName.REGCLASS)) {
				attrWithAlias = SelectElement.build(attr, format("_cm3_utils_regclass_to_name(%s)", quoteSqlIdentifier(attr)), quoteSqlIdentifier(attr));//TODO improve this
			}
			return attrWithAlias;
		}

		private WhereElement buildWhere(String attr, WhereOperator operator, Object... params) {
			return buildWhere(getAttrWithAliasForWhere(attr), SelectElement::getExpr, operator, emptyMap(), params);
		}

		private WhereElement buildWhere(SelectElement attr, Function<SelectElement, String> attrExprExtractor, WhereOperator operator, Map<String, Object> meta, Object... params) {
			return buildWhere(attr, attrExprExtractor, operator, meta, list(params));
		}

		private WhereElement buildWhere(SelectElement attr, Function<SelectElement, String> attrExprExtractor, WhereOperator operator, Map<String, Object> meta, List<Object> params) {
			String attrExpr = attrExprExtractor.apply(attr);
			switch (operator) {
				case ISNULL:
					checkArgument(params.isEmpty());
					return new WhereElement(format("%s IS NULL", attrExpr), meta, emptyList());
				case EQ:
					return new WhereElement(format("%s = ?", attrExpr), meta, singletonList(getOnlyElement(params)));
				case EQ_CASE_INSENSITIVE:
					return new WhereElement(format("LOWER(%s) = LOWER(?)", attrExpr), meta, singletonList(getOnlyElement(params)));
				case NOTEQ:
					return new WhereElement(format("%s <> ?", attrExpr), meta, singletonList(getOnlyElement(params)));
				case LT:
					return new WhereElement(format("%s < ?", attrExpr), meta, singletonList(getOnlyElement(params)));
				case GT:
					return new WhereElement(format("%s > ?", attrExpr), meta, singletonList(getOnlyElement(params)));
				case IN:
					Collection in = set((Iterable) getOnlyElement(params));
					return new WhereElement(format("%s IN (%s)", attrExpr, in.stream().map((x) -> "?").collect(joining(","))), meta, in);
				case LIKE:
					return new WhereElement(format("%s LIKE ?", attrExpr), meta, singletonList(getOnlyElement(params)));
				case MATCHES_REGEXP:
					return new WhereElement(format("%s ~ ?", attrExpr), meta, singletonList(getOnlyElement(params)));
				case NOT_MATCHES_REGEXP:
					return new WhereElement(format("%s !~ ?", attrExpr), meta, singletonList(getOnlyElement(params)));
				default:
					throw new UnsupportedOperationException("unsupported operator = " + operator);
			}
		}

		public PreparedQuery doBuild() {

			checkArgument(!(count && selectRowNumber), "cannot select count AND row number with the same query");
			checkNotNull(from);

			String fromExpr = entryTypeToQuotedSqlIdentifier(from);

			String orderExpr;
			if (!sorter.isNoop()) {
				try {
					List<String> sortExprList = sorter.getElements().stream().map((s) -> {
						String name = s.getProperty();
						String direction = checkNotNull(ImmutableMap.of(
								SorterElement.SorterElementDirection.ASC, "ASC",
								SorterElement.SorterElementDirection.DESC, "DESC"
						).get(s.getDirection()));
						SelectElement attr = getAttrFromSelectByName(name);
						if (from.hasAttribute(name)) {
							Attribute a = from.getAttribute(name);
							switch (a.getType().getName()) {
								case REFERENCE:
								case FOREIGNKEY:
									attr = getAttrFromSelectByName(buildDescAttrName(name), () -> buildExtendedAttr(buildDescAttrName(name), buildDescAttrExprForReference(a)));
									break;
								case LOOKUP:
									attr = getAttrFromSelectByName(buildDescAttrName(name), () -> buildExtendedAttr(buildDescAttrName(name), buildDescAttrExprForLookup(a)));
									break;
							}
						}
						String expr;
						if (selectRowNumber) {
							expr = attr.getExpr();
						} else {
							expr = attr.getAlias();
						}
						return format("%s %s", expr, direction);
					}).collect(toList());
					orderExpr = " ORDER BY " + Joiner.on(", ").join(sortExprList);
				} catch (Exception ex) {
					throw new DaoException(ex, "error processing query ordering = %s", toJsonString(sorter));
				}
			} else {
				orderExpr = "";
			}

			List<String> selectExprs = list();

			if (selectRowNumber) {
				selectExprs.add(format("ROW_NUMBER() OVER (%s) AS %s", orderExpr, ROW_NUMBER));
			}

			select.stream().map((a) -> {
				String expr = a.getExpr();
				Attribute attr = from.getAttributeOrNull(a.getName());
				if (attr != null) {
					expr = addSqlCastIfRequired(attr.getType(), expr);
				}
				return format("%s %s", expr, a.getAlias());
			}).forEach(selectExprs::add);

			String query = format("SELECT %s FROM %s %s", Joiner.on(", ").join(selectExprs), fromExpr, fromAlias);

			where.sort((a, b) -> ComparisonChain.start().compareFalseFirst(a.forRowNumber(), b.forRowNumber()).result());
			List<WhereElement> whereElementsNotFowRowNumber = where.stream().filter(not(WhereElement::forRowNumber)).collect(toList());
			List<WhereElement> whereElementsFowRowNumber = where.stream().filter(WhereElement::forRowNumber).collect(toList());

			if (!whereElementsNotFowRowNumber.isEmpty()) {
				query += whereElementsToWhereExpr(whereElementsNotFowRowNumber);
			}

			if (selectRowNumber) {
				query = format("SELECT * FROM (%s) %s", query, rowNumberSubExprAlias);
				if (!whereElementsFowRowNumber.isEmpty()) {
					query += whereElementsToWhereExpr(whereElementsFowRowNumber);
				}
			} else {
				query += orderExpr;
			}

			if (hasOffset(offset)) {
				query += format(" OFFSET %s", offset);
			}
			if (hasLimit(limit)) {
				query += format(" LIMIT %s", limit);
			}

			List<SelectElement> preparedQuerySelect;
			if (count) {
				SelectElement countAttr = SelectElement.build(COUNT, "COUNT(*)", aliasBuilder.buildAlias(COUNT));
				preparedQuerySelect = singletonList(countAttr);
				query = format("SELECT %s %s FROM ( %s ) %s", countAttr.getExpr(), countAttr.getAlias(), query, aliasBuilder.buildAlias("subquery"));
			} else if (selectRowNumber) {
				preparedQuerySelect = list(select).with(SelectElement.build(ROW_NUMBER, ROW_NUMBER, ROW_NUMBER));
			} else {
				preparedQuerySelect = select;
			}

			return new PreparedQueryImpl(query, preparedQuerySelect, this);
		}

		private String whereElementsToWhereExpr(List<WhereElement> whereElements) {
			return " WHERE " + whereElements.stream().map(WhereElement::getExpr).collect(joining(" AND "));
		}

		private List<WhereElement> buildWheresForFilter(CmdbFilter filter) {
			List<WhereElement> list = list();
			if (filter.hasAttributeFilter()) {
				list.add(buildFilterExpr(filter.getAttributeFilter()));
			}
			if (filter.hasRelationFilter()) {
				list.add(buildFilterWhereExpr(filter.getRelationFilter()));
			}
			if (filter.hasCqlFilter()) {
				CqlQueryImpl cql = compileCql(filter.getCqlFilter());
				Optional.ofNullable(new CqlExprBuilder(cql.getWhere()).buildWhereExprOrNull()).ifPresent(list::add);
			}
			if (filter.hasFulltextFilter()) {
				list.add(buildFulltextFilterWhere(filter.getFulltextFilter()));
			}
			if (filter.hasCompositeFilter()) {
				list.addAll(buildWheresForCompositeFilter(filter.getCompositeFilter()));
			}
			return list;
		}

		private List<WhereElement> buildWheresForCompositeFilter(CompositeFilter compositeFilter) {
			switch (compositeFilter.getMode()) {
				case CFM_AND:
					return compositeFilter.getElements().stream().map(this::buildWheresForFilter).flatMap(List::stream).collect(toList());
				case CFM_OR:
					return singletonList(compactWhereElements(compositeFilter.getElements().stream().map(this::buildWheresForFilter).flatMap(List::stream).collect(toList()), "OR"));
				case CFM_NOT:
					WhereElement element = compactWhereElements(buildWheresForFilter(compositeFilter.getElement()));
					return singletonList(new WhereElement(format("NOT %s", element.getExpr()), element.getParams()));
				default:
					throw unsupported("unsupported composite filter mode = %s", compositeFilter.getMode());
			}
		}

		private WhereElement compactWhereElements(List<WhereElement> list) {
			return compactWhereElements(list, "AND");
		}

		private WhereElement compactWhereElements(List<WhereElement> list, String operator) {
			checkArgument(!list.isEmpty());
			if (list.size() == 1) {
				return getOnlyElement(list);
			} else {
				return new WhereElement(wrapExprWithBrackets(list.stream().map(WhereElement::getExpr).map(QueryBuilderServiceImpl::wrapExprWithBrackets).collect(joining(" " + operator + " "))), list.stream().map(WhereElement::getParams).flatMap(List::stream).collect(toList()));
			}
		}

		private boolean selectHasAttrWithName(String name) {
			return select.stream().map(SelectElement::getName).anyMatch(equalTo(name));
		}

		private SelectElement getAttrFromSelectByName(String name) {
			try {
				return getOptionalAttrFromSelectByName(name).get();
			} catch (Exception ex) {
				throw new DaoException(ex, "attr not found in select for name = %s", name);
			}
		}

		private SelectElement getAttrFromSelectByName(String name, Supplier<SelectElement> supplierIfNull) {
			return getOptionalAttrFromSelectByName(name).orElseGet(supplierIfNull);
		}

		private Optional<SelectElement> getOptionalAttrFromSelectByName(String name) {
			checkNotBlank(name);
			return select.stream().filter((a) -> a.getName().equals(name)).collect(toOptional());
		}

		private WhereElement buildFulltextFilterWhere(FulltextFilter fulltextFilter) {
			return buildFulltextFilterWhere(fulltextFilter.getQuery());
		}

		private WhereElement buildFulltextFilterWhere(String query) {
			List<String> parts = list();

			from.getServiceAttributes().stream().map((a) -> {
				switch (a.getType().getName()) {
					case STRING:
					case TEXT:
						return format("%s ILIKE ?", quoteSqlIdentifier(a.getName()));
					case DECIMAL:
					case INTEGER:
					case LONG:
					case DOUBLE:
						return format("%s::varchar ILIKE ?", quoteSqlIdentifier(a.getName()));
					case REFERENCE:
					case FOREIGNKEY:
						return format("%s ILIKE ?", buildDescAttrExprForReference(a));
					case LOOKUP:
						return format("%s ILIKE ?", buildDescAttrExprForLookup(a));
					default:
						return null;
				}
			}).filter(notNull()).forEach(parts::add);

			String queryVal = format("%%%s%%", checkNotBlank(query));
			List thisArgs = parts.stream().map((x) -> queryVal).collect(toList());
			String thisExpr = wrapExprWithBrackets(Joiner.on(" OR ").join(parts));
			return new WhereElement(thisExpr, thisArgs);
		}

		private class CqlExprBuilder {

			private final WhereImpl cqlWhere;
			private final List<Object> cqlArgs = list();

			public CqlExprBuilder(WhereImpl cqlWhere) {
				this.cqlWhere = checkNotNull(cqlWhere);
			}

			public @Nullable
			WhereElement buildWhereExprOrNull() {
				String whereExpr = buildWhereExprOrNull(cqlWhere);
				if (isBlank(whereExpr)) {
					return null;
				} else {
					return new WhereElement(whereExpr, cqlArgs);
				}
			}

			private @Nullable
			String buildWhereExprOrNull(org.cmdbuild.cql.compiler.where.WhereElement whereElement) {
				if (whereElement instanceof FieldImpl) {
					return buildWhereExpr((FieldImpl) whereElement);
				} else if (whereElement instanceof GroupImpl || whereElement instanceof WhereImpl) {
					return whereElement.getElements().stream().map(this::buildWhereExprOrNull).collect(joining(" AND "));
				} else {
					throw unsupported("unsupported cql where element = %s", whereElement);
				}
			}

			private String buildWhereExpr(FieldImpl field) {
				String name = field.getId().getId();
				String attrExpr;
				if (selectHasAttrWithName(name)) {
					SelectElement attr = getAttrFromSelectByName(name);
					attrExpr = attr.getExpr();
				} else {
					attrExpr = quoteSqlIdentifier(name);
				}
				Attribute attribute = from.getAttribute(name);
				switch (field.getOperator()) {
					case EQ: {
						Object value = fieldValueToSqlValue(attribute, field);
						if (value == null) {
							return format("%s IS NULL", attrExpr);
						} else {
							switch (getOnlyElement(field.getValues()).getType()) {
								case NATIVE:
									return format("%s = %s", attrExpr, wrapExprWithBrackets(toStringNotBlank(value)));
								default:
									cqlArgs.add(value);
									return format("%s = ?", attrExpr);
							}
						}
					}
					case GT: {
						Object value = checkNotNull(fieldValueToSqlValue(attribute, field));
						switch (getOnlyElement(field.getValues()).getType()) {
							case NATIVE:
								return format("%s > %s", attrExpr, wrapExprWithBrackets(toStringNotBlank(value)));
							default:
								cqlArgs.add(value);
								return format("%s > ?", attrExpr);
						}
					}
					case LT: {
						Object value = checkNotNull(fieldValueToSqlValue(attribute, field));
						switch (getOnlyElement(field.getValues()).getType()) {
							case NATIVE:
								return format("%s < %s", attrExpr, wrapExprWithBrackets(toStringNotBlank(value)));
							default:
								cqlArgs.add(value);
								return format("%s < ?", attrExpr);
						}
					}
					case IN: {
						Object value = fieldValueToSqlValue(attribute, field);
						if (value == null) {
							return format("%s IS NULL", attrExpr);
						} else {
							switch (getOnlyElement(field.getValues()).getType()) {
								case NATIVE:
									return format("%s IN %s", attrExpr, wrapExprWithBrackets(toStringNotBlank(value)));
								default:
									throw unsupported("unsupported cql field value type = %s", getOnlyElement(field.getValues()).getType());
							}
						}
					}
					default:
						throw unsupported("unsupported cql operator = %s", field.getOperator());
				}
			}

			private @Nullable
			Object fieldValueToSqlValue(Attribute attribute, FieldImpl field) {
				Field.FieldValue value = getOnlyElement(field.getValues(), null);
				if (value == null) {
					return null;
				} else {
					switch (value.getType()) {
						case BOOL:
						case DATE:
						case FLOAT:
						case INT:
						case STRING:
						case TIMESTAMP:
							return attrValueToSql(attribute, value.getValue());
						case NATIVE:
							return toStringNotBlank(value.getValue());
						default:
							throw unsupported("unsupported cql field value type = %s", value.getType());
					}
				}
			}

		}

		public WhereElement buildFilterExpr(AttributeFilter filter) {
			switch (filter.getMode()) {
				case AND:
					return filter.hasOnlyElement() ? buildFilterExpr(filter.getOnlyElement()) : joinWhereElements(filter.getElements().stream().map(this::buildFilterExpr).collect(toList()), "AND");
				case OR:
					return filter.hasOnlyElement() ? buildFilterExpr(filter.getOnlyElement()) : joinWhereElements(filter.getElements().stream().map(this::buildFilterExpr).collect(toList()), "OR").mapExpr(QueryBuilderServiceImpl::wrapExprWithBrackets);
				case SIMPLE:
					return buildFilterExpr(filter.getCondition());
				case NOT:
					return buildFilterExpr(filter.getOnlyElement()).mapExpr((x) -> format("NOT %s", x));
				default:
					throw new UnsupportedOperationException("unsupported filter mode = " + filter.getMode());
			}
		}

		private WhereElement joinWhereElements(Collection<WhereElement> elements, String operator) {
			String expr = elements.stream().map(WhereElement::getExpr).map(QueryBuilderServiceImpl::wrapExprWithBrackets).collect(joining(format(" %s ", operator)));
			List<Object> params = elements.stream().map(WhereElement::getParams).flatMap(List::stream).collect(toList());
			return new WhereElement(expr, params);
		}

		public WhereElement buildFilterExpr(AttributeFilterCondition condition) {
			return new ConditionExprBuilder(condition).build();
		}

		private class ConditionExprBuilder {

			private final AttributeFilterCondition condition;
			private final Attribute attribute;
			private final String attrExpr;

			public ConditionExprBuilder(AttributeFilterCondition condition) {
				this.condition = condition;
				attribute = from.getAttribute(condition.getKey());
				attrExpr = quoteSqlIdentifier(attribute.getName());
				//TODO validate operator for attribute type (es INET_* operators require Inet attr type)
			}

			public WhereElement build() {
				Collection<Object> args = getArgsForCondition();
				return new WhereElement(buildConditionExpr(), args);
			}

			private Collection<Object> getArgsForCondition() {
				switch (condition.getOperator()) {
					case ISNOTNULL:
					case ISNULL:
						return emptyList();
					case BETWEEN:
						checkArgument(condition.getValues().size() == 2, "between operator requires exactly two parameters");
						return condition.getValues().stream().map(this::valueToSql).collect(toList());
					case IN:
						return getArgsForInCondition();
					case BEGIN:
						return singletonList(format("%%%s", checkNotBlank(condition.getSingleValue())));
					case END:
						return singletonList(format("%s%%", checkNotBlank(condition.getSingleValue())));
					case CONTAIN:
						return singletonList(format("%%%s%%", checkNotBlank(condition.getSingleValue())));
					case NET_RELATION: {
						Object value = valueToSql(condition.getSingleValue());
						return list(value, value, value);
					}
					default:
						return singletonList(valueToSql(condition.getSingleValue()));
				}
			}

			private Collection<Object> getArgsForInCondition() {
				checkArgument(!condition.getValues().isEmpty(), "cannot execute IN <empty set>");
				return condition.getValues().stream().map(this::valueToSql).collect(toSet());
			}

			private @Nullable
			Object valueToSql(@Nullable Object value) {
				return attrValueToSql(attribute, value);
			}

			private String buildConditionExpr() {
				switch (condition.getOperator()) {
					case ISNULL:
						return buildExpr("%s IS NULL");
					case ISNOTNULL:
						return buildExpr("%s IS NOT NULL");
					case EQUAL:
						return buildExpr("%s = ?");
					case NOTEQUAL:
						return format("( %s IS NULL OR %s <> ? )", attrExpr, attrExpr);
					case IN:
						return format("%s IN (%s)", attrExpr, getArgsForInCondition().stream().map((x) -> "?").collect(joining(",")));
					case BEGIN:
					case END:
					case CONTAIN:
					case LIKE:
						return buildExpr("%s ILIKE ?");
					case NOTBEGIN:
					case NOTCONTAIN:
					case NOTEND:
						return format("( %s IS NULL OR NOT %s ILIKE ? )", attrExpr, attrExpr);
					case GREATER:
						return buildExpr("%s > ?");
					case LESS:
						return buildExpr("%s < ?");
					case BETWEEN:
						return buildExpr("%s BETWEEN ? AND ?");
					case NET_CONTAINED:
						return buildExpr("%s << ?");
					case NET_CONTAINEDOREQUAL:
						return buildExpr("%s <<= ?");
					case NET_CONTAINS:
						return buildExpr("%s >> ?");
					case NET_CONTAINSOREQUAL:
						return buildExpr("%s >>= ?");
					case NET_RELATION:
						return format("( %s <<= ? OR %s = ? OR %s >>= ? )", attrExpr, attrExpr, attrExpr);
					default:
						throw new UnsupportedOperationException("unsupported condition operator = " + condition.getOperator());
				}
			}

			private String buildExpr(String pattern) {
				return format(pattern, attrExpr);
			}

		}

		private WhereElement buildFilterWhereExpr(RelationFilter filter) {
			List<WhereElement> list = filter.getRelationFilterRules().stream().map(this::buildFilterExpr).collect(toList());
			if (list.size() == 1) {
				return getOnlyElement(list);
			} else {
				String expr = list.stream().map(WhereElement::getExpr).map(QueryBuilderServiceImpl::wrapExprWithBrackets).collect(joining(" AND "));
				List<Object> args = list.stream().map(WhereElement::getParams).flatMap(List::stream).collect(toList());
				return new WhereElement(expr, args);
			}
		}

		private WhereElement buildFilterExpr(RelationFilterRule filter) {
			Domain domain = database.getDomain(filter.getDomain());
			String srcExpr, targetExpr, srcClassExpr, targetClassExpr;
			switch (filter.getDirection()) {
				case _1:
					srcExpr = quoteSqlIdentifier(ATTR_IDOBJ1);
					targetExpr = quoteSqlIdentifier(ATTR_IDOBJ2);
					srcClassExpr = quoteSqlIdentifier(ATTR_IDCLASS1);
					targetClassExpr = quoteSqlIdentifier(ATTR_IDCLASS2);
					break;
				case _2:
					srcExpr = quoteSqlIdentifier(ATTR_IDOBJ2);
					targetExpr = quoteSqlIdentifier(ATTR_IDOBJ1);
					srcClassExpr = quoteSqlIdentifier(ATTR_IDCLASS2);
					targetClassExpr = quoteSqlIdentifier(ATTR_IDCLASS1);
					break;
				default:
					throw new UnsupportedOperationException("unsupported domain direction = " + filter.getDirection());
			}
			String fromExpr = fromAlias,
					domainExpr = entryTypeToQuotedSqlIdentifier(domain);
			String subqueryBase = format("SELECT 1 FROM %s WHERE %s = %s.\"Id\" AND %s = %s.\"IdClass\" AND \"Status\" = 'A'", domainExpr, srcExpr, fromExpr, srcClassExpr, fromExpr);
			switch (filter.getType()) {
				case ANY:
					return new WhereElement(format("EXISTS %s", wrapExprWithBrackets(subqueryBase)));
				case NOONE:
					return new WhereElement(format("NOT EXISTS %s", wrapExprWithBrackets(subqueryBase)));
				case ONEOF:
					List<String> exprs = list();
					List<Object> args = list();
					filter.getCardInfos().stream().map(RelationFilterCardInfo::getClassName).distinct().forEach((c) -> {
						args.add(database.getClasse(c).getName());
						Set<Long> ids = filter.getCardInfos().stream().filter((ci) -> ci.getClassName().equals(c)).map(RelationFilterCardInfo::getId).collect(toSet());
						String idExpr;
						if (ids.size() == 1) {
							idExpr = "= ?";
							args.add(getOnlyElement(ids));
						} else {
							idExpr = format("IN (%s)", ids.stream().map(x -> "?").collect(joining(",")));
							args.addAll(ids);
						}
						exprs.add(format("%s = _cm3_utils_name_to_regclass(?) AND %s %s", targetClassExpr, targetExpr, idExpr));
					});
					String oneOfExpr;
					if (exprs.size() == 1) {
						oneOfExpr = getOnlyElement(exprs);
					} else {
						oneOfExpr = wrapExprWithBrackets(exprs.stream().map(QueryBuilderServiceImpl::wrapExprWithBrackets).collect(joining(" OR ")));
					}
					return new WhereElement(format("EXISTS (%s AND %s)", subqueryBase, oneOfExpr), args);
				default:
					throw new UnsupportedOperationException("unsupported relation filter type = " + filter.getType());
			}
		}

		private String buildDescAttrExprForLookup(Attribute a) {
			checkArgument(a.isOfType(AttributeTypeName.LOOKUP));
			return buildLookupDescExpr(fromAlias, exprForAttribute(a));
		}

		private String buildCodeAttrExprForLookup(Attribute a) {
			checkArgument(a.isOfType(AttributeTypeName.LOOKUP));
			return buildLookupCodeExpr(fromAlias, exprForAttribute(a));
		}

		private String buildDescAttrExprForReference(Attribute a) {
			checkArgument(a.isOfType(AttributeTypeName.FOREIGNKEY, AttributeTypeName.REFERENCE));
			Classe targetClass;
			if (a.getType() instanceof ReferenceAttributeType) {
				Domain domain = database.getDomain(((ReferenceAttributeType) a.getType()).getDomainName());
				targetClass = domain.getReferencedClass(a);
			} else {
				targetClass = database.getClasse(((ForeignKeyAttributeType) a.getType()).getForeignKeyDestinationClassName());
			}
			return buildReferenceDescExpr(targetClass, exprForAttribute(a));
		}

		private String exprForAttribute(Attribute a) {
			return quoteSqlIdentifier(a.getName());
		}

	}

	private static String cqlFilterKey(CqlFilter filter) {
		return checkNotBlank(filter.getCqlExpression());
	}

	private CqlQueryImpl compileCql(CqlFilter filter) {
		logger.info("processing cql filter = '{}'", abbreviate(filter.getCqlExpression()));
		return compileAndCheck(filter.getCqlExpression());
	}

	public static String buildReferenceDescExpr(Classe targetClass, String attrExpr) {
//		return format("_cm3_card_description_get('%s'::regclass,%s)", entryTypeToQuotedSql(targetClass), attrExpr); ignore-tenant function; too slow, replaced from simple subquery below
		return format("(SELECT \"Description\" FROM %s WHERE \"Id\" = %s%s)", entryTypeToQuotedSqlIdentifier(targetClass), attrExpr, targetClass.isSimpleClass() ? "" : " AND \"Status\" = 'A'");
	}

	public static String buildReferenceCodeExpr(Classe targetClass, String attrExpr) {
		return format("(SELECT \"Code\" FROM %s WHERE \"Id\" = %s%s)", entryTypeToQuotedSqlIdentifier(targetClass), attrExpr, targetClass.isSimpleClass() ? "" : " AND \"Status\" = 'A'");
	}

	public static String buildLookupDescExpr(String classExpr, String attrExpr) {
		return format("(SELECT \"Description\" FROM \"LookUp\" WHERE \"Id\" = %s.%s AND \"Status\" = 'A')", classExpr, attrExpr);
	}

	public static String buildLookupCodeExpr(String classExpr, String attrExpr) {
		return format("(SELECT \"Code\" FROM \"LookUp\" WHERE \"Id\" = %s.%s AND \"Status\" = 'A')", classExpr, attrExpr);
	}

	private class PreparedQueryImpl implements PreparedQuery {

		private final EntryType from;
		private final String query;
		private final List<SelectElement> select;
		private final List<WhereElement> where;
		private final CardMapper cardMapper;

		private PreparedQueryImpl(String query, List<SelectElement> select, QueryBuilderProcessor processor) {
			this.query = checkNotBlank(query);
			this.from = processor.from;
			this.cardMapper = processor.cardMapper;
			this.select = ImmutableList.copyOf(select);
			this.where = ImmutableList.copyOf(processor.where);
		}

		@Override
		public List<ResultRow> run() {
			List<Object> args = list(select.stream().map(SelectElement::getParams)).with(where.stream().map(WhereElement::getParams)).stream().flatMap(List::stream).map(SqlTypeUtils::systemToSql).collect(toList());
			logger.trace("execute query:\n\tquery = {}\n\tparams = {}", query, args);
			try {
				return database.getJdbcTemplate().query(query, args.toArray(), (ResultSet rs, int rowNum) -> {
					Map<String, Object> map = select.stream().collect(toMap(SelectElement::getName, (attr) -> {
						try {
							Object value = rs.getObject(attr.getAlias());
							Attribute attribute = from.getAttributeOrNull(attr.getName());
							if (attribute != null) {
								value = rawToSystem(attribute.getType(), value);
							}
							return value;
						} catch (SQLException ex) {
							throw new DaoException(ex);
						}
					}));
					list(map.keySet()).stream().map(from::getAttributeOrNull).filter(notNull()).forEach((a) -> {
						switch (a.getType().getName()) {
							case LOOKUP:
								LookupValue lookupValue = (LookupValue) map.get(a.getName());
								if (lookupValue != null) {
									String desc = toStringOrNull(map.remove(buildDescAttrName(a.getName())));
									String code = toStringOrNull(map.remove(buildCodeAttrName(a.getName())));
									Object value = LookupValueImpl.copyOf(lookupValue).withDescription(desc).withCode(code).build();
									map.put(a.getName(), value);
								}
								break;
							case REFERENCE:
							case FOREIGNKEY:
								IdAndDescription idAndDescription = (IdAndDescription) map.get(a.getName());
								if (idAndDescription != null) {
									String desc = toStringOrNull(map.remove(buildDescAttrName(a.getName())));
									Object value = new IdAndDescriptionImpl(idAndDescription.getId(), desc);
									map.put(a.getName(), value);
								}
						}
					});
					return new ResultRowImpl(map);
				});
			} catch (Exception ex) {
				throw new DaoException(ex, "error executing query = '%s' with args = %s", query, args);
			}
		}

		@Override
		public int getCount() {
			return checkNotNull(convert(getOnlyElement(run()).asMap().get(COUNT), Integer.class), "count not found: not a count query?");
		}

		private class ResultRowImpl implements ResultRow {

			private final Map<String, Object> map;

			public ResultRowImpl(Map<String, Object> map) {
				this.map = Collections.unmodifiableMap(checkNotNull(map));
			}

			@Override
			public Map<String, Object> asMap() {
				return map;
			}

			@Override
			public <T> T toModel(Class<T> model) {
				CardMapper thisMapper;
				if (cardMapper != null) {
					thisMapper = cardMapper;
				} else {
					thisMapper = mapper.getMapperForModel(model);
				}
				return (T) thisMapper.dataToObject(map::get).build();
			}

			@Override
			public <T> T toModel() {
				CardMapper thisMapper;
				if (cardMapper != null) {
					thisMapper = cardMapper;
				} else {
					checkArgument(from instanceof Classe, "cannot map row to model: unable to retrieve model for entry type = %s", from);
					thisMapper = mapper.getMapperForClasse((Classe) from);
				}
				try {
					return (T) thisMapper.dataToObject(map::get).build();
				} catch (Exception ex) {
					throw new DaoException(ex, "error mapping record id = %s type = %s to model = %s", map.get(ATTR_ID), firstNotNullOrNull(map.get(ATTR_IDCLASS), map.get(ATTR_IDDOMAIN)), thisMapper.getTargetClass());
				}
			}

			@Override
			public Card toCard() {
				String classId = checkNotNull(convert(map.get(ATTR_IDCLASS), String.class), "class id param not found in query result");
				Classe classe = database.getClasse(classId);
				return CardImpl.buildCard(classe, map);
			}

			@Override
			public CMRelation toRelation() {
				String domainId = sqlDomainTableToDomainName(checkNotNull(convert(map.get(ATTR_IDDOMAIN), String.class), "domain id param not found in query result"));
				Domain domain = database.getDomain(domainId);
				return RelationImpl.builder()
						.withType(domain)
						.withAttributes(map)
						.build();
			}

		}
	}

	private static @Nullable
	Object attrValueToSql(Attribute attribute, @Nullable Object value) {
		return systemToSql(attribute.getType(), rawToSystem(attribute.getType(), value));
	}

	public static String buildDescAttrName(String attr) {
		return format("_%s_description", attr);
	}

	public static String buildCodeAttrName(String attr) {
		return format("_%s_code", attr);
	}

	private static String wrapExprWithBrackets(String expr) {
		if (!isSafelyWrappedWithBrakets(expr)) {
			expr = format("(%s)", expr);
		}
		return expr;
	}

	private static boolean isSafelyWrappedWithBrakets(String expr) { //TODO improve this, parse sql and check
		return expr.trim().matches("[(][^()]+[)]");
	}

	private static class SelectArg {

		private final Map<String, Object> meta;
		private final List<Object> params;
		private final String name;
		private final Object expr;

		public SelectArg(String name, Object expr) {
			this(name, expr, emptyMap(), emptyList());
		}

		public SelectArg(String name, Object expr, Map<String, Object> meta, List<Object> params) {
			this.meta = ImmutableMap.copyOf(meta);
			this.params = ImmutableList.copyOf(params);
			this.name = checkNotBlank(name);
			this.expr = checkNotNull(expr);
		}

		public Map<String, Object> getMeta() {
			return meta;
		}

		public List<Object> getParams() {
			return params;
		}

		public String getName() {
			return name;
		}

		public Object getExpr() {
			return expr;
		}

	}

	private static class WhereArg {

		private final Map<String, Object> meta;
		private final List<Object> params;
		private final String expr;
		private final Object operator;

		public WhereArg(String expr, Object operator, Map<String, Object> meta, List<Object> params) {
			this.meta = ImmutableMap.copyOf(meta);
			this.params = list(params).immutable();
			this.expr = checkNotBlank(expr);
			this.operator = operator;
		}

		public Map<String, Object> getMeta() {
			return meta;
		}

		public List<Object> getParams() {
			return params;
		}

		public String getExpr() {
			return expr;
		}

		public Object getOperator() {
			return operator;
		}

	}

	private enum SpecialOperators {
		EXPR
	}

	private static class SelectElement {

		private final Map<String, Object> meta;
		private final List<Object> params;
		private final String expr, alias, name;

		private SelectElement(SelectElementBuilder builder) {
			this.expr = checkNotBlank(builder.expr);
			this.alias = checkNotBlank(builder.alias);
			this.name = checkNotBlank(builder.name);
			this.meta = ImmutableMap.copyOf(builder.meta);
			this.params = ImmutableList.copyOf(builder.params);
		}

		public String getName() {
			return name;
		}

		public String getExpr() {
			return expr;
		}

		public String getAlias() {
			return alias;
		}

		public Map<String, Object> getMeta() {
			return meta;
		}

		public List<Object> getParams() {
			return params;
		}

		public static SelectElementBuilder builder() {
			return new SelectElementBuilder();
		}

		public static SelectElement build(String name, String expr, String alias) {
			return builder().withName(name).withExpr(expr).withAlias(alias).build();
		}

		public static SelectElementBuilder copyOf(SelectElement source) {
			return new SelectElementBuilder()
					.withMeta(source.getMeta())
					.withParams(source.getParams())
					.withExpr(source.getExpr())
					.withAlias(source.getAlias())
					.withName(source.getName());
		}

		public static class SelectElementBuilder implements Builder<SelectElement, SelectElementBuilder> {

			private Map<String, Object> meta = emptyMap();
			private List<Object> params = emptyList();
			private String expr;
			private String alias;
			private String name;

			public SelectElementBuilder withMeta(Map<String, Object> meta) {
				this.meta = meta;
				return this;
			}

			public SelectElementBuilder withParams(List<Object> params) {
				this.params = params;
				return this;
			}

			public SelectElementBuilder withExpr(String expr) {
				this.expr = expr;
				return this;
			}

			public SelectElementBuilder withAlias(String alias) {
				this.alias = alias;
				return this;
			}

			public SelectElementBuilder withName(String name) {
				this.name = name;
				return this;
			}

			@Override
			public SelectElement build() {
				return new SelectElement(this);
			}

		}
	}

	private static class WhereElement {

		private final Map<String, Object> meta;
		private final String expr;
		private final List<Object> params;

		public WhereElement(String expr, Map<String, Object> meta, Collection<Object> params) {
			this.meta = ImmutableMap.copyOf(meta);
			this.expr = trimAndCheckNotBlank(expr);
			this.params = list(params).immutable();
		}

		public WhereElement(String expr) {
			this(expr, emptyMap(), emptyList());
		}

		private WhereElement(String expr, Collection<Object> params) {
			this(expr, emptyMap(), params);
		}

		public String getExpr() {
			return expr;
		}

		public boolean forRowNumber() {
			return toBooleanOrDefault(getMeta().get(WHERE_ELEMENT_FOR_ROW_NUMBER), false);
		}

		public Map<String, Object> getMeta() {
			return meta;
		}

		public List<Object> getParams() {
			return params;
		}

		public WhereElement mapExpr(Function<String, String> exprFun) {
			return new WhereElement(exprFun.apply(expr), meta, params);
		}

		public WhereElement withMeta(Map<String, Object> meta) {
			return new WhereElement(expr, map(this.meta).with(meta), params);
		}

	}

}
