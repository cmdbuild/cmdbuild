/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.repository;

import com.google.common.base.Joiner;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import com.google.common.eventbus.Subscribe;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Optional;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.DaoException;
import static org.cmdbuild.dao.driver.postgres.Const.DOMAIN_PREFIX;
import org.cmdbuild.dao.driver.postgres.FunctionRepositoryImpl;
import static org.cmdbuild.dao.driver.postgres.utils.CommentUtils.domainCommentToMetadata;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.domainNameToSqlTable;
import org.cmdbuild.dao.driver.repository.ClasseRepository;
import org.cmdbuild.dao.driver.repository.DomainRepository;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwner;
import org.cmdbuild.dao.entrytype.DomainImpl;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.ClasseImpl;
import org.cmdbuild.dao.entrytype.DomainMetadata;
import org.cmdbuild.dao.event.AttributeGroupModifiedEvent;
import org.cmdbuild.dao.event.AttributeModifiedEvent;
import org.cmdbuild.dao.event.DaoEventServiceImpl;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.DomainDefinition;
import org.cmdbuild.dao.driver.repository.InnerAttributeRepository;
import org.cmdbuild.dao.entrytype.ClassPermissionMode;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.entryTypeToQuotedSqlIdentifier;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.quoteSqlIdentifier;

@Component
@Primary
public class DomainRepositoryImpl implements DomainRepository {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private final Holder<List<Domain>> allDomainsCache;
	private final CmdbCache<String, Optional<Domain>> domainsCacheByName;
	private final CmdbCache<String, List<Domain>> domainsForClasse;

	private final InnerAttributeRepository attributesRepository;
	private final ClasseRepository classRepository;
	private final JdbcTemplate jdbcTemplate;

	public DomainRepositoryImpl(CacheService cacheService, @Qualifier(SYSTEM_LEVEL_ONE) JdbcTemplate jdbcTemplate, InnerAttributeRepository attributesRepository, ClasseRepository classRepository, DaoEventServiceImpl eventService) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.attributesRepository = checkNotNull(attributesRepository);
		this.classRepository = checkNotNull(classRepository);

		allDomainsCache = cacheService.newHolder("dao_all_domains", CacheService.CacheConfig.SYSTEM_OBJECTS);
		domainsCacheByName = cacheService.newCache("dao_domains_by_name", CacheService.CacheConfig.SYSTEM_OBJECTS);
		domainsForClasse = cacheService.newCache("dao_domains_for_classe", CacheService.CacheConfig.SYSTEM_OBJECTS);

		eventService.getEventBus().register(new Object() {
			@Subscribe
			public void handleAttributeModifiedEvent(AttributeModifiedEvent event) {
				if (event.getOwner() instanceof Domain) {
					invalidateCache();
				}
			}

			@Subscribe
			public void handleAttributeGroupModifiedEvent(AttributeGroupModifiedEvent event) {
				invalidateCache();
			}
		});
	}

	private void invalidateCache() {
		allDomainsCache.invalidate();
		domainsCacheByName.invalidateAll();
		domainsForClasse.invalidateAll();
	}

	@Override
	@Nullable
	public Domain getDomainOrNull(Long id) {
		return getAllDomains().stream().filter((d) -> equal(d.getId(), id)).collect(toOptional()).orElse(null);
	}

	@Override
	public @Nullable
	Domain getDomainOrNull(@Nullable String domainId) {
		if (isBlank(domainId)) {
			return null;
		} else {
			return domainsCacheByName.get(domainId, () -> Optional.ofNullable(doFindDomain(domainId))).orElse(null);
		}
	}

	@Override
	public List<Domain> getAllDomains() {
		logger.trace("getting all domains");
		return allDomainsCache.get(this::doFindAllDomains);
	}

	@Override
	public DomainImpl createDomain(DomainDefinition definition) {
		logger.info("creating domain = {}", definition.getName());
		DomainImpl createdDomain = doCreateDomain(definition);
		invalidateCache();
		return createdDomain;
	}

	@Override
	public DomainImpl updateDomain(DomainDefinition definition) {
		logger.info("updating domain = {}", definition.getName());
		DomainImpl updatedDomain = doUpdateDomain(definition);
		invalidateCache();
		return updatedDomain;
	}

	@Override
	public void deleteDomain(Domain dbDomain) {
		logger.info("deleting domain = {}", dbDomain.getName());
		doDeleteDomain((DomainImpl) dbDomain);//TODO remove cast
		invalidateCache();
	}

	@Override
	public List<Domain> getDomainsForClasse(Classe classe) {
		checkNotNull(classe);
		return domainsForClasse.get(classe.getName(), () -> doGetDomainsForClasse(classe));
	}

	private List<Domain> doGetDomainsForClasse(Classe classe) {
		return getAllDomains().stream()
				.filter(Domain::isActive)
				.filter((d) -> d.isDomainForClasse(classe))
				.collect(toList());
	}

	private @Nullable
	Domain doFindDomain(String localname) {
		return getAllDomains().stream().filter((domain) -> equal(domain.getName(), localname)).findAny().orElse(null);
	}

	private List<Domain> doFindAllDomains() {
		return jdbcTemplate.query("SELECT d::int _id, _cm3_utils_regclass_to_name(d) _name, _cm3_class_comment_get_jsonb(d) _comment FROM _cm3_domain_list() d WHERE d <> '\"Map\"'::regclass ORDER BY d::varchar", (rs, i) -> {
			Long id = null;
			String name = "<unknown_name>";
			try {
				id = rs.getLong("_id");
				name = tableNameToDomainName(rs.getString("_name"));
				List<AttributeWithoutOwner> attributes = attributesRepository.getNonReservedEntryTypeAttributesForType(id);
				DomainMetadata meta = domainCommentToMetadata(rs.getString("_comment"));
				// FIXME we should handle this in another way
				Classe class1 = classRepository.getClasse(meta.get(DomainMetadata.CLASS_1));
				Classe class2 = classRepository.getClasse(meta.get(DomainMetadata.CLASS_2));
				DomainImpl domain = DomainImpl.builder() //
						.withName(name) //
						.withId(id) //
						.withMetadata(meta) //
						.withAllAttributes(attributes) //
						.withClass1(class1) //
						.withClass2(class2) //
						.build();
				return domain;
			} catch (Exception ex) {
				throw new DaoException(ex, "error processing domain = %s %s", firstNonNull(id, "<unknown oid>"), name);
			}
		});
	}

	private static String tableNameToDomainName(String tableName) {
		checkArgument(tableName.startsWith(DOMAIN_PREFIX), "invalid domain table name = %s", tableName);
		return tableName.substring(DOMAIN_PREFIX.length());
	}

	private DomainImpl doCreateDomain(DomainDefinition definition) {
		String domainComment = commentFrom(definition);
		long id = jdbcTemplate.queryForObject("SELECT _cm3_domain_create(?, ?::jsonb)", Long.class, definition.getName(), domainComment);
		return DomainImpl.builder()
				.withName(definition.getName())
				.withId(id)
				.withAllAttributes(attributesRepository.getNonReservedEntryTypeAttributesForType(id)) //
				// FIXME looks ugly!
				// .withAttribute(new DBAttribute(DBRelation._1, new
				// ReferenceAttributeType(id), null)) //
				// .withAttribute(new DBAttribute(DBRelation._2, new
				// ReferenceAttributeType(id), null)) //
				.withMetadata(domainCommentToMetadata(domainComment)) //
				.withClass1((ClasseImpl) definition.getSourceClass()) //
				.withClass2((ClasseImpl) definition.getTargetClass()) //
				.build();
	}

	private DomainImpl doUpdateDomain(DomainDefinition definition) {
		String domainComment = commentFrom(definition);
		jdbcTemplate.queryForObject("SELECT _cm3_domain_modify(?::regclass, ?::jsonb)", Object.class, quoteSqlIdentifier(domainNameToSqlTable(definition.getName())), domainComment);
		return DomainImpl.builder()
				.withName(definition.getName())
				.withId(definition.getOid())//TODO check id
				.withAllAttributes(attributesRepository.getNonReservedEntryTypeAttributesForType(definition.getOid())) //
				// FIXME looks ugly!
				// .withAttribute(new DBAttribute(DBRelation._1, new
				// ReferenceAttributeType(identifier), null)) //
				// .withAttribute(new DBAttribute(DBRelation._2, new
				// ReferenceAttributeType(identifier), null)) //
				.withMetadata(domainCommentToMetadata(domainComment)) //
				.withClass1((ClasseImpl) definition.getSourceClass()) //
				.withClass2((ClasseImpl) definition.getTargetClass()) //
				.build();
	}

	private void doDeleteDomain(Domain domain) {
		jdbcTemplate.queryForObject("SELECT _cm3_domain_delete(?::regclass)", Object.class, quoteSqlIdentifier(domainNameToSqlTable(domain.getName())));
	}

	private String commentFrom(DomainDefinition definition) {
		return toJson(map(
				"LABEL", definition.getMetadata().getDescription(),
				"DESCRDIR", defaultIfBlank(definition.getMetadata().getDirectDescription(), EMPTY),
				"DESCRINV", defaultIfBlank(definition.getMetadata().getInverseDescription(), EMPTY),
				"MODE", ClassPermissionMode.CPM_DEFAULT.name().toLowerCase(),
				"STATUS", definition.getMetadata().isActive() ? "active" : "noactive",
				"TYPE", "domain",
				"CLASS1", definition.getSourceClass().getName(),
				"CLASS2", definition.getTargetClass().getName(),
				"CARDIN", defaultIfBlank(definition.getMetadata().getCardinality(), "N:N"),
				"MASTERDETAIL", Boolean.toString(definition.getMetadata().isMasterDetail()),
				"MDLABEL", defaultIfBlank(definition.getMetadata().getMasterDetailDescription(), EMPTY),
				"MDFILTER", defaultIfBlank(definition.getMetadata().getMasterDetailFilter(), EMPTY),
				"DISABLED1", serializeParamList(definition.getMetadata().getDisabledSourceDescendants()),
				"DISABLED2", serializeParamList(definition.getMetadata().getDisabledTargetDescendants()),
				"INDEX1", Integer.toString(definition.getMetadata().getIndexForSource()),
				"INDEX2", Integer.toString(definition.getMetadata().getIndexForTarget())
		));
	}

	private static String serializeParamList(Iterable<String> values) {
		return Joiner.on(",").skipNulls().join(defaultIfNull(values, emptyList()));
	}

}
