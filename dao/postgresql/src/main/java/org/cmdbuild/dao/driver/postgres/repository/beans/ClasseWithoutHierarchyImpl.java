/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.repository.beans;

import java.util.Map;
import org.cmdbuild.dao.entrytype.ClassMetadata;
import org.cmdbuild.dao.entrytype.ClasseInfo;
import org.cmdbuild.dao.entrytype.ClasseWithoutHierarchy;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ComparisonChain;
import static com.google.common.collect.Maps.uniqueIndex;
import com.google.common.collect.Ordering;
import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwner;
import org.cmdbuild.dao.entrytype.AttributeImpl;
import org.cmdbuild.dao.entrytype.CMEntryTypeVisitor;
import org.cmdbuild.utils.lang.Builder;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.utils.lang.CmdbMapUtils;

public class ClasseWithoutHierarchyImpl implements ClasseWithoutHierarchy {

	private final ClassMetadata meta;
	private final ClasseInfo parentInfo;
	private final Map<String, Attribute> attributes;
	private final String name;
	private final Long id;

	private ClasseWithoutHierarchyImpl(ClasseWithoutHierarchyBuilderImpl builder) {
		this.meta = checkNotNull(builder.meta);
		this.parentInfo = builder.parentInfo;
		this.attributes = CmdbMapUtils.<String, Attribute>map().accept((m) -> {
			checkNotNull(builder.attributes).entrySet().stream()
					.sorted(new Ordering<AttributeWithoutOwner>() {
						@Override
						public int compare(AttributeWithoutOwner a, AttributeWithoutOwner b) {
							return ComparisonChain.start()
									.compare(a.getIndex(), b.getIndex())
									.compare(a.getName(), b.getName())
									.result();
						}

					}.onResultOf(Map.Entry::getValue))
					.forEach((entry) -> {
						m.put(entry.getKey(), AttributeImpl.copyOf(entry.getValue()).withOwner(ClasseWithoutHierarchyImpl.this).build());
					});
		}).immutable();
		this.name = checkNotNull(builder.name);
		this.id = checkNotNull(builder.id);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ClassMetadata getMetadata() {
		return meta;
	}

	@Override
	@Nullable
	public ClasseInfo getParentInfoOrNull() {
		return parentInfo;
	}

	@Override
	public Map<String, Attribute> getAllAttributesAsMap() {
		return attributes;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void accept(CMEntryTypeVisitor visitor) {
		throw new UnsupportedOperationException();
	}

	public static ClasseWithoutHierarchyBuilderImpl builder() {
		return new ClasseWithoutHierarchyBuilderImpl();
	}

	public static ClasseWithoutHierarchyBuilderImpl copyOf(ClasseWithoutHierarchyImpl source) {
		return new ClasseWithoutHierarchyBuilderImpl()
				.withMeta(source.getMetadata())
				.withParentInfo(source.getParentInfoOrNull())
				.withAttributes(source.getAllAttributesAsMap())
				.withName(source.getName())
				.withId(source.getId());
	}

	@Override
	public String toString() {
		return "ClasseWithoutHierarchyImpl{" + "name=" + name + ", id=" + id + '}';
	}

	public static class ClasseWithoutHierarchyBuilderImpl implements Builder<ClasseWithoutHierarchyImpl, ClasseWithoutHierarchyBuilderImpl> {

		private ClassMetadata meta;
		private ClasseInfo parentInfo;
		private Map<String, ? extends AttributeWithoutOwner> attributes;
		private String name;
		private Long id;

		public ClasseWithoutHierarchyBuilderImpl withMeta(ClassMetadata meta) {
			this.meta = meta;
			return this;
		}

		public ClasseWithoutHierarchyBuilderImpl withParentInfo(ClasseInfo parentInfo) {
			this.parentInfo = parentInfo;
			return this;
		}

		public ClasseWithoutHierarchyBuilderImpl withAttributes(Map<String, ? extends AttributeWithoutOwner> attributes) {
			this.attributes = attributes;
			return this;
		}

		public ClasseWithoutHierarchyBuilderImpl withAttributes(Iterable<? extends AttributeWithoutOwner> attributes) {
			this.attributes = uniqueIndex(attributes, AttributeWithoutOwner::getName);
			return this;
		}

		public ClasseWithoutHierarchyBuilderImpl withName(String name) {
			this.name = name;
			return this;
		}

		public ClasseWithoutHierarchyBuilderImpl withId(Long id) {
			this.id = id;
			return this;
		}

		@Override
		public ClasseWithoutHierarchyImpl build() {
			return new ClasseWithoutHierarchyImpl(this);
		}

	}
}
