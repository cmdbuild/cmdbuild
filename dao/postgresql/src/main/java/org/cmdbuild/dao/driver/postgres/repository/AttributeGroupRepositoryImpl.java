/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.driver.postgres.repository;

import org.cmdbuild.dao.driver.repository.AttributeGroupRepository;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.driver.postgres.repository.beans.AttributeGroupImpl;
import org.cmdbuild.dao.entrytype.AttributeGroup;
import org.cmdbuild.dao.event.AttributeGroupModifiedEvent;
import org.cmdbuild.dao.event.DaoEventServiceImpl;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class AttributeGroupRepositoryImpl implements AttributeGroupRepository {

	private final JdbcTemplate jdbcTemplate;
	private final DaoEventServiceImpl eventService;
	private final Holder<List<AttributeGroup>> allAttributeGroups;
	private final CmdbCache<String, Optional<AttributeGroup>> attributeGroupsById;

	public AttributeGroupRepositoryImpl(JdbcTemplate jdbcTemplate, CacheService cacheService, DaoEventServiceImpl eventService) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.eventService = checkNotNull(eventService);
		allAttributeGroups = cacheService.newHolder("all_attribute_groups", CacheService.CacheConfig.SYSTEM_OBJECTS);
		attributeGroupsById = cacheService.newCache("attribute_groups_by_id", CacheService.CacheConfig.SYSTEM_OBJECTS);
	}

	@Override
	public List<AttributeGroup> getAll() {
		return allAttributeGroups.get(this::doGetAll);
	}

	@Override
	@Nullable
	public AttributeGroup getOrNull(String groupId) {
		return attributeGroupsById.get(groupId, () -> {
			return getAll().stream().filter((a) -> equal(a.getName(), groupId)).collect(toOptional());
		}).orElse(null);
	}

	@Override
	public AttributeGroup create(AttributeGroup group) {
		jdbcTemplate.update("INSERT INTO \"_AttributeGroup\" (\"Code\",\"Description\") VALUES (?,?)", group.getName(), group.getDescription());
		invalidateCache();
		eventService.post(AttributeGroupModifiedEvent.INSTANCE);
		return get(group.getName());
	}

	@Override
	public AttributeGroup update(AttributeGroup group) {
		get(group.getName());
		jdbcTemplate.update("UPDATE \"_AttributeGroup\" SET \"Description\" = ? WHERE \"Code\" = ?", group.getDescription(), group.getName());
		invalidateCache();
		eventService.post(AttributeGroupModifiedEvent.INSTANCE);
		return get(group.getName());
	}

	private void invalidateCache() {
		allAttributeGroups.invalidate();
		attributeGroupsById.invalidateAll();
	}

	private List<AttributeGroup> doGetAll() {
		return jdbcTemplate.query("SELECT \"Code\",\"Description\" FROM \"_AttributeGroup\" WHERE \"Status\" = 'A'", (ResultSet rs, int rowNum) -> {
			return AttributeGroupImpl.builder()
					.withName(rs.getString("Code"))
					.withDescription(rs.getString("Description"))
					.build();
		});
	}

}
