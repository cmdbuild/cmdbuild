package org.cmdbuild.dao.driver.postgres;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.BeginDate;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.CurrentId;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.DomainId;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.DomainId1;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.DomainId2;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.DomainQuerySource;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.EndDate;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.Id;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.IdClass;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.RowNumber;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.RowsCount;
import static org.cmdbuild.dao.driver.postgres.Const.SystemAttributes.User;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.nameForSystemAttribute;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.Nullable;
import org.cmdbuild.dao.DaoException;

import org.cmdbuild.dao.driver.postgres.query.ColumnMapper;
import org.cmdbuild.dao.driver.postgres.query.ColumnMapper.EntryTypeAttribute;
import org.cmdbuild.dao.driver.postgres.query.QueryCreator;
import org.cmdbuild.dao.beans.CardDefinitionImpl;
import org.cmdbuild.dao.beans.DatabaseEntryImpl;
import org.cmdbuild.dao.beans.DBFunctionCallOutput;
import org.cmdbuild.dao.beans.LegacyRelationImpl;
import org.cmdbuild.dao.beans.IdAndDescriptionImpl;
import org.cmdbuild.dao.beans.LookupValueImpl;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.query.QueryResultImpl;
import org.cmdbuild.dao.query.DBQueryRow;
import org.cmdbuild.dao.query.QuerySpecs;
import org.cmdbuild.dao.query.clause.QueryRelation;
import org.cmdbuild.dao.query.clause.alias.Alias;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.cmdbuild.dao.query.QueryResult;
import org.cmdbuild.dao.driver.repository.ClasseRepository;
import org.cmdbuild.dao.driver.repository.DomainRepository;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.NullAttributeTypeVisitor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.EntryType;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import org.cmdbuild.dao.driver.PostgresService;
import static org.cmdbuild.dao.driver.postgres.utils.SqlTypeUtils.sqlToSystem;

@Component
public class EntryQueryExecutorService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ClasseRepository classeRepository;
	private final DomainRepository domainRepository;
	private final JdbcTemplate jdbcTemplate;
	private final ClassDescriptionService descriptionService;
	private final LookupDescriptionService lookupDescriptionService;

	public EntryQueryExecutorService(DomainRepository domainRepository, ClasseRepository classeRepository, JdbcTemplate jdbcTemplate, ClassDescriptionService descriptionService, LookupDescriptionService lookupDescriptionService) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.classeRepository = checkNotNull(classeRepository);
		this.domainRepository = checkNotNull(domainRepository);
		this.descriptionService = checkNotNull(descriptionService);
		this.lookupDescriptionService = checkNotNull(lookupDescriptionService);
	}

	public QueryResult executeQuery(QuerySpecs querySpecs, PostgresService dao) {
		logger.debug("executing query from '{}'", querySpecs);
		QueryCreator qc = new QueryCreator(querySpecs);
		String query = qc.getQuery();
		logger.debug("execute query = {}", query);
		Object[] params = qc.getParams();
		ResultFiller rch = new ResultFiller(qc.getColumnMapper(), querySpecs, dao);
		logger.debug("query: {}", query);
		logger.debug("params: {}", Arrays.asList(params));
		logger.trace("starting execution of query {}", query);
		jdbcTemplate.query(query, params, rch);
		logger.trace("execution of query {} finished", query);
		return rch.getResult();
	}

	private class ResultFiller implements RowCallbackHandler {

		private final Logger logger = LoggerFactory.getLogger(getClass());

		private final PostgresService dao;
		private final ColumnMapper columnMapper;
		private final QuerySpecs querySpecs;
		private final QueryResultImpl result;

		public ResultFiller(ColumnMapper columnMapper, QuerySpecs querySpecs, PostgresService dao) {
			this.dao = checkNotNull(dao);
			this.columnMapper = columnMapper;
			this.querySpecs = querySpecs;
			this.result = new QueryResultImpl();
		}

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			try {
				result.setTotalSize(rs.getInt(nameForSystemAttribute(querySpecs.getFromClause().getAlias(), RowsCount)));
			} catch (SQLException e) {
				logger.trace("warning", e);
				result.setTotalSize(0);
			}
			DBQueryRow row = new DBQueryRow();
			createRowNumber(rs, row);
			createBasicCards(rs, row);
			createBasicRelations(rs, row);
			createFunctionCallOutput(rs, row);
			result.add(row);
		}

		private void createRowNumber(ResultSet rs, DBQueryRow row) {
			try {
				row.setNumber(rs.getLong(nameForSystemAttribute(querySpecs.getFromClause().getAlias(), RowNumber)));
			} catch (SQLException e) {
				logger.trace("warning", e);
			}
		}

		private void createFunctionCallOutput(ResultSet rs, DBQueryRow row) throws SQLException {
			for (Alias a : columnMapper.getFunctionCallAliases()) {
				DBFunctionCallOutput out = new DBFunctionCallOutput();
				EntryType hackSinceAFunctionCanAppearOnlyInTheFromClause = querySpecs.getFromClause().getType();
				for (EntryTypeAttribute eta : columnMapper.getAttributes(a,
						hackSinceAFunctionCanAppearOnlyInTheFromClause)) {
					Object sqlValue = rs.getObject(eta.index);
					out.set(eta.name, sqlToSystem(eta.sqlType, sqlValue));
				}
				row.setFunctionCallOutput(a, out);
			}
		}

		private void createBasicCards(ResultSet rs, DBQueryRow row) throws SQLException {
		throw new UnsupportedOperationException("BROKEN - TODO");
//			logger.trace("creating cards");
//
//			for (Alias alias : columnMapper.getClassAliases()) {
//				if (columnMapper.getExternalReferenceAliases().contains(alias.toString())) {
//					continue;
//				}
//				logger.trace("creating card for alias '{}'", alias);
//				// Always extract a Long for the Id even if it's integer
//				Long id = rs.getLong(nameForSystemAttribute(alias, Id));
//				Long classId = rs.getLong(nameForSystemAttribute(alias, IdClass));
//				Classe realClass = classeRepository.getClasseOrNull(classId);
//				if (realClass == null) {
//					logger.warn("class not found for {} '{}', skipping creation", IdClass.getDBName(), classId);
//					continue;
//				}
//				logger.trace("real class for id '{}' is '{}'", classId, realClass.getIdentifier());
//				CardDefinitionImpl card = CardDefinitionImpl.newInstance(dao, realClass, id);
//
//				card.setUser(rs.getString(nameForSystemAttribute(alias, User)));
//				card.setBeginDate(getDateTime(rs, nameForSystemAttribute(alias, BeginDate)));
//				card.setEndDate(getDateTime(rs, nameForSystemAttribute(alias, EndDate)));
//				card.setCurrentId(getLong(rs, nameForSystemAttribute(alias, CurrentId)));
//
//				addUserAttributes(alias, card, rs);
//				row.setCard(alias, card);
//			}
		}

		private void createBasicRelations(ResultSet rs, DBQueryRow row) throws SQLException {
			for (Alias alias : columnMapper.getDomainAliases()) {
				Long id = rs.getLong(nameForSystemAttribute(alias, Id));
				Long domainId = rs.getLong(nameForSystemAttribute(alias, DomainId));
				String querySource = rs.getString(nameForSystemAttribute(alias, DomainQuerySource));
				Domain realDomain = domainRepository.getDomainOrNull(domainId);
				if (realDomain == null) {
					logger.trace("domain not found for id '{}', skipping creation", domainId);
					continue;
				}
				LegacyRelationImpl relation = LegacyRelationImpl.newInstance(dao, realDomain, id);

				relation.setUser(rs.getString(nameForSystemAttribute(alias, User)));
				relation.setBeginDate(getDateTime(rs, nameForSystemAttribute(alias, BeginDate)));
				relation.setEndDate(getDateTime(rs, nameForSystemAttribute(alias, EndDate)));
				Long idObject1 = rs.getLong(nameForSystemAttribute(alias, DomainId1));
				Long idObject2 = rs.getLong(nameForSystemAttribute(alias, DomainId2));
				relation.setCard1Id(idObject1);
				relation.setCard2Id(idObject2);

				addUserAttributes(alias, relation, rs);

				QueryRelation queryRelation = QueryRelation.newInstance(relation, querySource);
				row.setRelation(alias, queryRelation);
			}
		}

		private @Nullable
		DateTime getDateTime(ResultSet rs, String attributeAlias) {
			try {
				java.sql.Timestamp ts = rs.getTimestamp(attributeAlias);
				return (ts != null) ? new DateTime(ts.getTime()) : null;
			} catch (SQLException ex) {
				logger.trace("warning", ex);
				return null;
			}
		}

		private @Nullable
		Long getLong(ResultSet rs, String attributeAlias) {
			try {
				return rs.getLong(attributeAlias);
			} catch (SQLException ex) {
				logger.trace("warning", ex);
				return null;
			}
		}

		private void addUserAttributes(Alias typeAlias, DatabaseEntryImpl entry, ResultSet rs) throws SQLException {
		throw new UnsupportedOperationException("BROKEN - TODO");
//			logger.trace("adding user attributes for entry of type '{}' with alias '{}'", entry.getType().getIdentifier(), typeAlias);
//
//			Iterable<EntryTypeAttribute> attributes = columnMapper.getAttributes(typeAlias, entry.getType());
//			if (attributes != null) {
//				for (EntryTypeAttribute element : attributes) {
//					if (element.name != null) {
//						Attribute attribute = entry.getType().getAttribute(element.name);
//						CardAttributeType attributeType = attribute.getType();
//
//						AtomicReference value = new AtomicReference(rs.getObject(element.index));
//
//						attributeType.accept(new NullAttributeTypeVisitor() {
//
//							@Override
//							public void visit(LookupAttributeType attributeType) {
//								Long id = valueId(rs, element);
//								String description = id == null ? null : lookupDescriptionService.getDescription(id);
//								String type = ((LookupAttributeType) attributeType).getLookupTypeName();
//								value.set(new LookupValueImpl(id, description, type));
//							}
//
//							@Override
//							public void visit(ReferenceAttributeType attributeType) {
//								handleReference(() -> domainRepository.getDomain(attributeType.getDomainName()).getReferenceTarget());
//							}
//
//							@Override
//							public void visit(ForeignKeyAttributeType attributeType) {
//								handleReference(() -> classeRepository.getClasseOrNull(attributeType.getForeignKeyDestinationClassName()));
//							}
//
//							private void handleReference(Supplier<Classe> classeSupplier) {
//								Long id = valueId(rs, element);
//								String description;
//								if (id == null) {
//									description = null;
//								} else {
//									Classe referencedClass = checkNotNull(classeSupplier.get());
//									description = descriptionService.getDescription(referencedClass, id);
//								}
//								value.set(new IdAndDescriptionImpl(id, description));
//							}
//
//						});
//
//						entry.setOnly(element.name, sqlToSystem(element.sqlType, value.get()));
//					} else {
//						// skipping, not belonging to this entry type
//					}
//				}
//			}
		}

		/**
		 * @param rs
		 * @param attribute
		 * @return
		 * @throws SQLException
		 */
		private Long valueId(ResultSet rs, EntryTypeAttribute attribute) {
			try {
				Long externalReferenceId = rs.getLong(attribute.index) == 0 ? null : rs.getLong(attribute.index);
				return externalReferenceId;
			} catch (SQLException ex) {
				throw new DaoException(ex);
			}
		}

		private QueryResult getResult() {
			return result;
		}
	}

}
