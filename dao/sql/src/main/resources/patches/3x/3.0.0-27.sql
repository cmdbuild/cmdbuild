-- fix custom class permissions

DO $$ DECLARE
	_record RECORD;
	_value jsonb;
BEGIN

	ALTER TABLE "_Grant" DISABLE TRIGGER USER;
	
	FOR _record IN SELECT * FROM "_Grant" WHERE "_Grant"."OtherPrivileges" IS NOT NULL LOOP
		_value = _record."OtherPrivileges";
		IF _value->>'modify' IS NOT NULL THEN
			_value = ( _value || jsonb_build_object('update', _value->'modify') ) - 'modify';
		END IF;
		IF _value->>'remove' IS NOT NULL THEN
			_value = ( _value || jsonb_build_object('delete', _value->'remove') ) - 'remove';
		END IF;
		IF _value::varchar <> (_record."OtherPrivileges")::varchar THEN
			EXECUTE format('UPDATE "_Grant" SET "OtherPrivileges" = %L WHERE "Id" = %L', _value, _record."Id");
		END IF;
	END LOOP;

	ALTER TABLE "_Grant" ENABLE TRIGGER USER;

END $$ LANGUAGE PLPGSQL;
