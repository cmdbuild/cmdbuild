-- refactoring of lookup table

ALTER TABLE "LookUp" DISABLE TRIGGER USER;

UPDATE "LookUp" SET "Code" = replace(coalesce(nullif("TranslationUuid",''),uuid_in(md5(random()::text || now()::text)::cstring)::varchar),'-','') WHERE "Code" IS NULL OR "Code" ~ '^[[:blank:]]*$';
UPDATE "LookUp" SET "Type" = 'X' WHERE "Type" IS NULL OR "Type" ~ '^[[:blank:]]*$';

ALTER TABLE "LookUp" ALTER COLUMN "Code" SET NOT NULL;
ALTER TABLE "LookUp" ALTER COLUMN "Type" SET NOT NULL;

DO $$
DECLARE
	_code varchar;
	_type varchar;
BEGIN

	FOR _code, _type IN
		SELECT l."Code", l."Type" FROM (SELECT "Code","Type",count(*) c FROM "LookUp" WHERE "Status" = 'A' GROUP BY "Code","Type") l WHERE l.c > 1
	LOOP
		RAISE NOTICE 'found duplicated codes for lookup type = % code = %; replacing codes with unique values', _type, _code;
		UPDATE "LookUp" SET "Code" = "Code" || '_' || "Id" WHERE "Id" IN
			(SELECT "Id" FROM "LookUp" WHERE "Code" = _code AND "Type" = _type AND "Status" = 'A' ORDER BY "BeginDate" DESC OFFSET 1); --TODO check this
	END LOOP;

END $$ LANGUAGE PLPGSQL;

DROP TRIGGER IF EXISTS cm_set_translationuuid ON "LookUp";
DROP FUNCTION IF EXISTS cm_set_translationuuid();

UPDATE "_Translation" t SET "Code" = 'lookup.'||l."Type"||'.'||l."Code"||'.description' FROM "LookUp" l WHERE t."Code" = 'lookup.'||l."TranslationUuid"||'.description' AND l."Status" = 'A';

UPDATE "_Translation" SET "Status" = 'N' WHERE "Code" ~ 'lookup.[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}.description' AND "Status" = 'A';--TODO update these records above and keep them as status=n in translation table (requires conversion of trans table to standard class before this patch

ALTER TABLE "LookUp" DROP COLUMN "TranslationUuid";

CREATE UNIQUE INDEX "_Unique_LookUp_CodeType" ON "LookUp" ("Code","Type") WHERE "Status" = 'A';

SELECT _cm3_attribute_create('"LookUp"', 'IconType', 'varchar', 'DEFAULT: none|NOTNULL: true|MODE: write|DESCR: IconType');--TODO add constraint
SELECT _cm3_attribute_create('"LookUp"', 'IconImage', 'varchar', 'MODE: read|DESCR: Icon Image');
SELECT _cm3_attribute_create('"LookUp"', 'IconFont', 'varchar', 'MODE: read|DESCR: Icon Font'); --TODO add constraint
SELECT _cm3_attribute_create('"LookUp"', 'Color', 'varchar', 'MODE: read|DESCR: Color'); --TODO add constraint

CREATE UNIQUE INDEX "_Unique_Lookup_LookupType" ON "LookUp" ("Type") WHERE "Status" = 'A' AND "Code" = 'org.cmdbuild.LOOKUPTYPE';

ALTER TABLE "LookUp" ALTER COLUMN "IsDefault" SET DEFAULT false;

UPDATE "LookUp" SET "IsDefault" = false WHERE "IsDefault" IS NULL;

ALTER TABLE "LookUp" ALTER COLUMN "IsDefault" SET NOT NULL;

ALTER TABLE "LookUp" ENABLE TRIGGER USER;

DO $$ 
DECLARE
	_query text;
BEGIN

	CREATE TABLE lookup_aux AS (SELECT "Id","User","BeginDate","Code","Description","Status","Notes","Type","ParentType","ParentId","Number","IsDefault","IconType","IconImage","IconFont","Color" FROM "LookUp" l);

	SELECT _cm3_utils_prepare_dependant_views_create_query('"LookUp"'::regclass) INTO _query;
	PERFORM _cm3_utils_drop_dependant_views('"LookUp"'::regclass);

	DROP TABLE "LookUp";

	PERFORM _cm3_class_create('LookUp', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Lookup list|SUPERCLASS: false|STATUS: active');
	PERFORM _cm3_attribute_create('"LookUp"', 'Type', 'varchar', 'NOTNULL: true|MODE: read');
	PERFORM _cm3_attribute_create('"LookUp"', 'ParentType', 'varchar', 'MODE: read');
	PERFORM _cm3_attribute_create('"LookUp"', 'ParentId', 'integer', 'MODE: read');
	PERFORM _cm3_attribute_create('"LookUp"', 'Number', 'integer', 'NOTNULL: true|MODE: read');
	PERFORM _cm3_attribute_create('"LookUp"', 'IsDefault', 'boolean', 'DEFAULT: false|NOTNULL: true|MODE: read');
	PERFORM _cm3_attribute_create('"LookUp"', 'IconType', 'varchar', 'DEFAULT: none|NOTNULL: true|MODE: read|DESCR: IconType');--TODO add constraint
	PERFORM _cm3_attribute_create('"LookUp"', 'IconImage', 'varchar', 'MODE: read|DESCR: Icon Image');
	PERFORM _cm3_attribute_create('"LookUp"', 'IconFont', 'varchar', 'MODE: read|DESCR: Icon Font'); --TODO add constraint
	PERFORM _cm3_attribute_create('"LookUp"', 'Color', 'varchar', 'MODE: read|DESCR: Color'); --TODO add constraint

	ALTER TABLE "LookUp" ALTER COLUMN "Code" SET NOT NULL;
	CREATE UNIQUE INDEX "_Unique_LookUp_CodeType" ON "LookUp" ("Code","Type") WHERE "Status" = 'A';
	CREATE UNIQUE INDEX "_Unique_Lookup_LookupType" ON "LookUp" ("Type") WHERE "Status" = 'A' AND "Code" = 'org.cmdbuild.LOOKUPTYPE';

	ALTER TABLE "LookUp" DISABLE TRIGGER USER;

	INSERT INTO "LookUp" ("Id","CurrentId","IdClass","User","BeginDate","Code","Description","Status","Notes","Type","ParentType","ParentId","Number","IsDefault","IconType","IconImage","IconFont","Color")
		SELECT "Id","Id",'"LookUp"'::regclass,"User","BeginDate","Code","Description","Status","Notes","Type","ParentType","ParentId","Number","IsDefault","IconType","IconImage","IconFont","Color" FROM lookup_aux WHERE "Status" = 'A';

	ALTER TABLE "LookUp" ENABLE TRIGGER USER;

	DROP TABLE lookup_aux;

	RAISE NOTICE 'recreate dropped views';
	EXECUTE _query;

END $$ LANGUAGE PLPGSQL;

ALTER TABLE "LookUp" DROP COLUMN "Color";

SELECT _cm3_attribute_create('"LookUp"', 'Index', 'integer', 'MODE: read|DESCR: Index');
SELECT _cm3_attribute_create('"LookUp"', 'IconColor', 'varchar', 'MODE: read|DESCR: Icon Color'); --TODO add constraint
SELECT _cm3_attribute_create('"LookUp"', 'TextColor', 'varchar', 'MODE: read|DESCR: Text Color'); --TODO add constraint

ALTER TABLE "LookUp" ADD CONSTRAINT "LookUp_check_IconType_format" CHECK ( "IconType" ~ '^(font|image|none)$' );
ALTER TABLE "LookUp" ADD CONSTRAINT "LookUp_check_IconColor_format" CHECK ( "IconColor" ~* '^#?[A-F0-9]{6}$' );
ALTER TABLE "LookUp" ADD CONSTRAINT "LookUp_check_TextColor_format" CHECK ( "TextColor" ~* '^#?[A-F0-9]{6}$' );

ALTER TABLE "LookUp" DISABLE TRIGGER "_SanityCheck";

UPDATE "LookUp" SET "Index" = "Number";
UPDATE "LookUp" SET "Index" = -1 WHERE "Index" IS NULL;

ALTER TABLE "LookUp" ENABLE TRIGGER "_SanityCheck";

ALTER TABLE "LookUp" DROP COLUMN "Number";
ALTER TABLE "LookUp" ALTER COLUMN "Index" SET NOT NULL;

ALTER TABLE "LookUp" ADD CONSTRAINT "LookUp_check_Code_notblank" CHECK ( NOT "Code" ~ '^[[:blank:][:cntrl:]]*$' );

INSERT INTO "LookUp" ("Code","Type","ParentType","Index","IsDefault") SELECT DISTINCT 'org.cmdbuild.LOOKUPTYPE',"Type","ParentType",0,false FROM "LookUp" WHERE "Status" = 'A';
