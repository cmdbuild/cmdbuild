-- gis table fix

CREATE OR REPLACE FUNCTION _cm3_gis_class_list() RETURNS SETOF regclass AS $$
	SELECT oid::regclass FROM pg_class WHERE _cm3_class_is_simple_or_standard(oid) AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname='gis');
$$ LANGUAGE SQL STABLE;

DO $$ DECLARE
	_class regclass;
BEGIN

	FOR _class IN SELECT x FROM _cm3_gis_class_list() x LOOP 
		EXECUTE format('ALTER TABLE %s ALTER COLUMN "Id" TYPE bigint', _class);
		EXECUTE format('ALTER TABLE %s ALTER COLUMN "Master" TYPE bigint', _class);
		EXECUTE format('ALTER TABLE %s ALTER COLUMN "Id" SET DEFAULT _cm3_utils_new_card_id()', _class);
	END LOOP;
	
END $$ LANGUAGE PLPGSQL;

--TODO extend gis 'class'; gis history

