-- attribute modify function

CREATE OR REPLACE FUNCTION _cm3_attribute_modify(_class regclass,_attr varchar,_type varchar,_comment jsonb) RETURNS void AS $$ DECLARE
	_not_null boolean;
	_unique boolean;
	_default varchar;
	_current_comment jsonb;
BEGIN

	_not_null = coalesce(_comment->>'NOTNULL','false')::boolean;
	_unique = coalesce(_comment->>'UNIQUE','false')::boolean;
	_default = _comment->>'DEFAULT';

	_comment = _comment - 'NOTNULL' - 'UNIQUE' - 'DEFAULT';

--  TODO check
-- 	IF COALESCE(_cm_read_reference_domain_comment(OldComment), '') IS DISTINCT FROM COALESCE(_cm_read_reference_domain_comment(NewComment), '')
-- 		OR  _cm_read_reference_type_comment(OldComment) IS DISTINCT FROM _cm_read_reference_type_comment(NewComment)
-- 		OR  COALESCE(_cm_get_fk_target_comment(OldComment), '') IS DISTINCT FROM COALESCE(_cm_get_fk_target_comment(NewComment), '')
-- 	THEN
-- 		RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION';
-- 	END IF;

	PERFORM _cm3_attribute_validate_comment_and_type(_comment, _type);

	IF _cm3_attribute_sqltype_get(_class, _attr) <> trim(_type) THEN
		IF _cm3_attribute_is_inherited(_class, _attr) THEN
			RAISE 'CM: cannot alter type of attr %.%: attribute is inherited from parent class', _class, _attr;
		ELSE
			EXECUTE format('ALTER TABLE %s ALTER COLUMN %I TYPE %s', _class, _attr, _type);
		END IF;
	END IF;

    PERFORM _cm3_attribute_default_set(_class, _attr, _default, FALSE); 
	PERFORM _cm_attribute_set_notnull(_class, _attr, _not_null); --TODO migrate fun
	PERFORM _cm_attribute_set_uniqueness(_class, _attr, _unique); --TODO migrate fun
    PERFORM _cm_set_attribute_comment(_class, _attr, _cm3_comment_from_jsonb(_comment)); --TODO migrate fun
END $$ LANGUAGE PLPGSQL;

