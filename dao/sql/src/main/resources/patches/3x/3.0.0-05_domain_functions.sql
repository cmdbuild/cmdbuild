-- system functions for domain edit

CREATE OR REPLACE FUNCTION _cm3_domain_create(_name varchar, _comment jsonb) RETURNS int AS $$
DECLARE 
	_attr varchar;
	_domain regclass;
	_cardinality varchar;
BEGIN

	IF NOT _name LIKE 'Map_%' THEN 
		_name = format('Map_%s', _name);
	END IF;

-- 	EXECUTE format('CREATE TABLE "Map_%s" (CONSTRAINT "Map_%s_pkey" PRIMARY KEY ("IdDomain", "IdClass1", "IdObj1", "IdClass2", "IdObj2", "BeginDate")) INHERITS ("Map")', _name, _name);
	EXECUTE format('CREATE TABLE "%s" (CONSTRAINT "%s_pkey" PRIMARY KEY ("Id")) INHERITS ("Map")', _name, _name);

	_domain = _cm3_utils_name_to_regclass(_name);

	EXECUTE format('COMMENT ON TABLE %s IS %L', _domain, _cm3_comment_from_jsonb(_comment));
	
	PERFORM _cm3_attribute_copy_all_comments('"Map"'::regclass, _domain);

-- 	EXECUTE format('CREATE TABLE "Map_%s_history" (CONSTRAINT "Map_%s_history_pkey" PRIMARY KEY ("IdDomain","IdClass1", "IdObj1", "IdClass2", "IdObj2","EndDate")) ' TODO check this
	EXECUTE format('CREATE TABLE "%s_history" (CONSTRAINT "%s_history_pkey" PRIMARY KEY ("Id")) INHERITS (%s)', _name, _name, _domain);
	EXECUTE format('ALTER TABLE "%s_history" ALTER COLUMN "EndDate" SET DEFAULT now()', _name);

	_cardinality = _cm3_class_comment_get(_domain,'CARDIN');
	
	PERFORM _cm3_attribute_index_create(_domain, 'IdDomain');
	PERFORM _cm3_attribute_index_create(_domain, 'IdObj1');
	PERFORM _cm3_attribute_index_create(_domain, 'IdObj2');

	EXECUTE format('CREATE UNIQUE INDEX "idx_%s_ActiveRows" ON %s USING btree ("IdDomain","IdClass1", "IdObj1", "IdClass2", "IdObj2") WHERE "Status" = ''A''', _name, _domain);

	IF _cardinality LIKE '%:1' THEN
		EXECUTE format('CREATE UNIQUE INDEX "idx_%s_UniqueLeft" ON %s USING btree ("IdClass1", "IdObj1") WHERE "Status" = ''A''', _name, _domain);
	END IF;
	IF _cardinality LIKE '1:%' THEN
		EXECUTE format('CREATE UNIQUE INDEX "idx_%s_UniqueRight" ON %s USING btree ("IdClass2", "IdObj2") WHERE "Status" = ''A''', _name, _domain);
	END IF;

	EXECUTE format('CREATE TRIGGER "_SanityCheck" BEFORE INSERT OR UPDATE OR DELETE ON %s FOR EACH ROW EXECUTE PROCEDURE _cm_trigger_sanity_check()', _domain);
	EXECUTE format('CREATE TRIGGER "_CreateHistoryRow" AFTER DELETE OR UPDATE ON %s FOR EACH ROW EXECUTE PROCEDURE _cm_trigger_create_relation_history_row()', _domain);

	RETURN _domain::oid::int;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_domain_create(_name varchar, _comment varchar) RETURNS int AS $$ 
	SELECT _cm3_domain_create(_name, _cm3_comment_to_jsonb(_comment)) 
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_domain_modify(_domain regclass, _comment jsonb) RETURNS void AS $$
DECLARE
	_current_comment jsonb;
BEGIN
	_current_comment = _cm3_class_comment_get_jsonb(_domain);
	IF _current_comment->'CARDIN' <> _comment->'CARDIN' 
		OR _current_comment->'CLASS1' <> _comment->'CLASS1'
		OR _current_comment->'CLASS2' <> _comment->'CLASS2'
		OR _current_comment->'TYPE' <> _comment->'TYPE'
	THEN
		RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION: invalid comment change on domain = % current comment = "%" new comment = "%"', _domain, _cm3_comemnt_from_jsonb(_current_comment), _cm3_comemnt_from_jsonb(_comment);
	END IF;

	PERFORM _cm3_class_comment_set(_domain, _comment);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_domain_delete(_domain regclass) RETURNS void AS $$ BEGIN

	IF _cm3_class_has_records(_domain) THEN
		RAISE EXCEPTION 'CM_CONTAINS_DATA: cannot delete domain %s: table is not empty', _domain;
	END IF;

	PERFORM _cm3_attribute_delete_all(_domain);

	EXECUTE format('DROP TABLE %s CASCADE', _domain);
END $$ LANGUAGE PLPGSQL;


--- RELATION MODIFY ---

CREATE OR REPLACE FUNCTION _cm3_relation_update(_domain regclass, _source text, _source_class regclass, _source_card_id bigint, _target text, _target_class regclass, _target_card_id bigint) RETURNS void AS $$
DECLARE
	_source_class_attr varchar = _cm3_relation_utils_class_attr_from_card_attr(_source);
	_target_class_attr varchar = _cm3_relation_utils_class_attr_from_card_attr(_target);
BEGIN
	EXECUTE format('UPDATE %s SET %I = $1, %I = $2 WHERE %I = $3 AND %I = $4 AND "Status" = ''A''',_domain,_target,_target_class_attr,_source,_source_class_attr)
		USING _target_card_id,_target_class,_source_card_id, _source_class;
END $$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION _cm3_relation_insert(_domain regclass, _source text, _source_class regclass, _source_card_id bigint, _target text, _target_class regclass, _target_card_id bigint) RETURNS void AS $$
DECLARE
	_source_class_attr varchar = _cm3_relation_utils_class_attr_from_card_attr(_source);
	_target_class_attr varchar = _cm3_relation_utils_class_attr_from_card_attr(_target);
	_already_inserted boolean;
BEGIN

	RAISE DEBUG 'insert relation % % - % %', _source_class, _source_card_id, _target_class, _target_card_id;

	EXECUTE format('SELECT EXISTS (SELECT * FROM %s WHERE %I = $1 AND %I = $2 AND "Status" = ''A'')',_domain,_source,_target) USING _source_card_id, _target_card_id  INTO _already_inserted;
	IF _already_inserted THEN
		RAISE DEBUG 'relation already present, skipping';
	ELSE
		EXECUTE format('INSERT INTO %s ("IdDomain",%I,%I,%I,%I,"Status") VALUES ($1,$2,$3,$4,$5,''A'' )', _domain, _source, _target, _source_class_attr, _target_class_attr)
			USING _domain, _source_card_id, _target_card_id, _source_class, _target_class;
	END IF;
END $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION _cm3_relation_delete(_domain regclass, _source text, _source_class regclass, _source_card_id bigint) RETURNS void AS $$ 
BEGIN
	EXECUTE format('UPDATE %s SET "Status" = ''N'' WHERE "Status" = ''A'' AND %I = $1 AND %I = $2', _domain, _source, _cm3_relation_utils_class_attr_from_card_attr(_source)) USING _source_card_id, _source_class;
END $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION _cm_trigger_update_relation() RETURNS trigger AS $$
DECLARE
	_attribute_name text := TG_ARGV[0];
	_domain regclass := TG_ARGV[1]::regclass;
	_source text := TG_ARGV[2];
	_target text := TG_ARGV[3];

	_old_card_id bigint;
	_new_card_id bigint;
	_new_card_class regclass;

	_source_card_id bigint = NEW."Id";
	_source_card_class regclass = TG_RELID::regclass;
BEGIN

	RAISE DEBUG 'new relation source is % %', _source_card_class, _source_card_id;

	RAISE DEBUG 'Trigger % on %', TG_NAME, TG_TABLE_NAME;
	IF (TG_OP = 'UPDATE') THEN
		EXECUTE 'SELECT (' || quote_literal(OLD) || '::' || TG_RELID::regclass || ').' || quote_ident(_attribute_name) INTO _old_card_id;
	END IF;

	EXECUTE format('SELECT (%L::%s).%I',NEW,TG_RELID::regclass,_attribute_name) INTO _new_card_id;
	_new_card_class = _cm3_domain_utils_class_for_dir_and_card(_domain, _target, _new_card_id);

	RAISE DEBUG 'new relation target is % %', _new_card_class, _new_card_id;

	IF (_new_card_id IS NOT NULL) THEN
		IF (_old_card_id IS NOT NULL) THEN
			IF (_old_card_id <> _new_card_id) THEN
				PERFORM _cm3_relation_update(_domain, _source, _source_card_class, _source_card_id, _target, _new_card_class, _new_card_id);
			END IF;
		ELSE
			PERFORM _cm3_relation_insert(_domain, _source, _source_card_class, _source_card_id, _target, _new_card_class, _new_card_id);
		END IF;
	ELSE
		IF (_old_card_id IS NOT NULL) THEN
			PERFORM _cm3_relation_delete(_domain, _source, _source_card_class, _source_card_id);
		END IF;
	END IF;
	RETURN NEW;
END $$ LANGUAGE PLPGSQL;