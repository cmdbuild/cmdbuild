-- gis function fix

CREATE OR REPLACE FUNCTION _cm3_gis_find_values(attrs bigint[],area varchar) RETURNS TABLE(ownerclass regclass,attrname varchar,ownercard integer,geometry text) AS $$
BEGIN

	FOR ownerclass, attrname IN
		SELECT "Owner" ownerclass,"Code" attrname 
		FROM "_GisAttribute" 
		WHERE "Id" = ANY (attrs) 
	LOOP
		RETURN QUERY EXECUTE format('SELECT %L::regclass ownerclass,%L::varchar attrname, "Master" ownercard, gis.st_astext("Geometry") geometry FROM gis."Gis_%s_%s" AS g WHERE g."Geometry" operator(gis.&&) gis.st_makeenvelope(%s,900913)',
			ownerclass::regclass,attrname,_cm3_utils_regclass_to_name(ownerclass),attrname,area);
	END LOOP;
	RETURN;

END $$ LANGUAGE PLPGSQL;

