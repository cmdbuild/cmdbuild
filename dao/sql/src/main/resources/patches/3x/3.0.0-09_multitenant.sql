-- multitenant

CREATE OR REPLACE FUNCTION _cm3_multitenant_mode_set(_class regclass, _mode varchar) RETURNS VOID AS $$ BEGIN
	PERFORM _cm3_multitenant_mode_change(_class, _cm3_class_comment_get(_class, 'MTMODE'), _mode);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_multitenant_mode_change(_class regclass, _previous_mode varchar, _mode varchar) RETURNS VOID AS $$ DECLARE
	_previous_mode varchar;
	_create_policy boolean;
	_policy varchar;
	_subclass regclass;
BEGIN
	
	_mode = coalesce(_mode, 'never');
	_previous_mode = coalesce(_previous_mode, 'never');

	RAISE NOTICE 'set multitenant mode for class = %, cur mode = %, new mode = %', _class, _previous_mode, _mode;

	IF _mode <> _previous_mode THEN

		IF _cm3_class_is_superclass(_class) THEN

			FOR _subclass IN SELECT _cm3_class_list_descendant_classes_and_self_not_superclass(_class) LOOP
				PERFORM _cm3_multitenant_mode_set(_subclass, _mode);
			END LOOP;
		
		ELSE

			_policy = 'current_setting(''cmdbuild.ignore_tenant_policies'') = ''true'' OR "IdTenant" = ANY (current_setting(''cmdbuild.user_tenants'')::bigint[])';

			IF _mode = 'never' THEN
				_create_policy = false;
			ELSEIF _mode = 'always' THEN
				_create_policy = true;
			ELSEIF _mode = 'mixed' THEN
				_create_policy = true;
				_policy = '"IdTenant" IS NULL OR ' || _policy;
			ELSE
				RAISE EXCEPTION 'CM: error configuring multitenant for class = % : unsupported multitenant mode = %', _class, _mode;
			END IF;

			IF _previous_mode IN ('always','mixed') THEN
				EXECUTE format('DROP POLICY IF EXISTS "%s_policy" ON %s', _cm3_utils_regclass_to_name(_class), _class);
				EXECUTE format('ALTER TABLE %s DISABLE ROW LEVEL SECURITY', _class);
			END IF;

			IF _create_policy THEN
				IF NOT _cm3_attribute_exists(_class,'IdTenant') THEN
					PERFORM _cm3_attribute_create(_class, 'IdTenant', 'bigint', 'MODE: rescore|DESCR: Tenant Id|STATUS: active'); -- null idtenant are allowed in 'always' mode to allow class record migrations from never to always
					EXECUTE format('CREATE INDEX %s_idtenant ON %s ("IdTenant") WHERE "IdTenant" IS NOT NULL', _cm3_utils_regclass_to_name(_class), _class);
				END IF;
				EXECUTE format('ALTER TABLE %s ENABLE ROW LEVEL SECURITY, FORCE ROW LEVEL SECURITY', _class);
				EXECUTE format('CREATE POLICY "%s_policy" ON %s USING (%s)', _cm3_utils_regclass_to_name(_class),_class,_policy);
			END IF;

			--note: when setting mode to never, IdTenant column is not deleted to allow recover of card tenant info

			IF _mode = 'never' THEN
				PERFORM _cm3_class_comment_delete(_class, 'MTMODE');
			ELSE
				PERFORM _cm3_class_comment_set(_class, 'MTMODE', _mode);
			END IF;

		END IF;

	END IF;

END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_multitenant_get(_user_id bigint) RETURNS SETOF bigint AS $$ BEGIN
	IF _user_id = -1 THEN
-- 		RETURN QUERY SELECT "Tenant"."Id" FROM "Tenant" WHERE "Tenant"."Status" = 'A';
		RETURN; -- all tenants
	ELSE
-- 		RETURN QUERY SELECT "Tenant"."Id" FROM "Tenant" JOIN "Map_UserTenant" ON "Tenant"."Id" = "Map_UserTenant"."IdObj2" WHERE "Map_UserTenant"."IdObj1" = _user_id  AND "Tenant"."Status" = 'A' AND "Map_UserTenant"."Status" = 'A';
		RETURN; -- user tenants
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_multitenant_enable(_class regclass) RETURNS VOID AS $$ BEGIN
	
END $$ LANGUAGE PLPGSQL;