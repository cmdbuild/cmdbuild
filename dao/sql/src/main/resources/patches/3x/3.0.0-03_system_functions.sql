-- mixed system functions


-- SYSTEM COMMANDS --

CREATE OR REPLACE FUNCTION _cm3_system_message_send(_type varchar, _data jsonb) RETURNS VOID AS $$
	LISTEN cminfo;
	SELECT pg_notify('cmevents', jsonb_set(_data,ARRAY['type'],to_jsonb(_type))::varchar);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_system_command_send(_action varchar, _data jsonb) RETURNS VOID AS $$
	SELECT _cm3_system_message_send('command', jsonb_set(_data, ARRAY['action'], to_jsonb(_action)));
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_system_command_send(_action varchar) RETURNS VOID AS $$
	SELECT _cm3_system_command_send(_action, '{}'::jsonb);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_system_command_send(_action varchar, VARIADIC _args varchar[]) RETURNS VOID AS $$
	SELECT _cm3_system_command_send(_action, jsonb_set('{}'::jsonb, ARRAY['args'], to_jsonb(_args)));
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_system_reload() RETURNS VOID AS $$
	SELECT _cm3_system_command_send('reload');
$$ LANGUAGE SQL;


-- SYSTEM UTILS --

CREATE OR REPLACE FUNCTION _cm3_utils_build_sqltype_string(_sqltype oid, _typemod integer) RETURNS varchar AS $$
	SELECT pg_type.typname::text || COALESCE(
				CASE
					WHEN pg_type.typname IN ('varchar','bpchar') THEN '(' || _typemod - 4 || ')'
					WHEN pg_type.typname = 'numeric' THEN '(' || _typemod / 65536 || ',' || _typemod - _typemod / 65536 * 65536 - 4|| ')'
				END
			, '')
		FROM pg_type WHERE pg_type.oid = _sqltype;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_utils_operation_user_get() RETURNS varchar AS $$
BEGIN
	RETURN current_setting('cmdbuild.operation_user');
EXCEPTION WHEN undefined_object THEN
	RETURN 'postgres';
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_utils_new_card_id() RETURNS bigint AS $$
	SELECT nextval('class_seq')::bigint;
$$ LANGUAGE SQL VOLATILE;

CREATE OR REPLACE FUNCTION _cm3_utils_regclass_to_name(_class regclass) RETURNS varchar AS $$ BEGIN
	RETURN replace(_class::varchar,'"','');
END $$ LANGUAGE PLPGSQL IMMUTABLE;

CREATE OR REPLACE FUNCTION _cm3_utils_name_to_regclass(_class_name varchar) RETURNS regclass AS $$ 
DECLARE 
	_class_name_with_commas varchar;
BEGIN
	IF _class_name IS NULL THEN
		RETURN NULL;
	ELSE
		_class_name_with_commas = replace('"' || _class_name || '"','""','"');
		IF (SELECT pg_get_function_arguments(oid) FROM pg_proc WHERE proname = 'to_regclass') = 'cstring' THEN
			RETURN to_regclass(_class_name_with_commas::cstring);
		ELSE
			RETURN to_regclass(_class_name_with_commas::text);
		END IF;
	END IF;
END $$ LANGUAGE PLPGSQL IMMUTABLE;

-- COMMENT UTILS --

CREATE OR REPLACE FUNCTION _cm3_comment_to_jsonb(_comment varchar) RETURNS jsonb AS $$ 
DECLARE
	_map jsonb;
	_part varchar[];
BEGIN
	_map = '{}'::jsonb;
	FOR _part IN SELECT regexp_matches(unnest(string_to_array(_comment,'|')),'^ *([^:]+) *: *(.*)$') LOOP
		_map = jsonb_set(_map,ARRAY[(_part[1])],to_jsonb(_part[2]));
	END LOOP;
	RETURN _map;
END $$ LANGUAGE PLPGSQL IMMUTABLE; 

CREATE OR REPLACE FUNCTION _cm3_comment_get_part(_comment varchar,_key varchar) RETURNS varchar AS $$
	SELECT _cm3_comment_to_jsonb(_comment)->>_key;
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION _cm3_comment_from_jsonb(_map jsonb) RETURNS varchar AS $$ 
	SELECT string_agg(format('%s: %s',key,value),'|') from jsonb_each_text(_map);
$$ LANGUAGE SQL IMMUTABLE; 

CREATE OR REPLACE FUNCTION _cm3_comment_set_part(_comment varchar,_key varchar,_value varchar) RETURNS varchar AS $$
	SELECT _cm3_comment_from_jsonb(jsonb_set(_cm3_comment_to_jsonb(_comment),ARRAY[_key],to_jsonb(_value)));
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION _cm3_comment_delete_part(_comment varchar,_key varchar) RETURNS varchar AS $$
	SELECT _cm3_comment_from_jsonb(_cm3_comment_to_jsonb(_comment) - _key);
$$ LANGUAGE SQL IMMUTABLE;


--- TRIGGER UTILS ---

CREATE OR REPLACE FUNCTION _cm3_trigger_utils_fkey(_class regclass, _attr varchar) RETURNS VARCHAR AS $$
	SELECT format('%s_%s_fkey', _cm3_utils_regclass_to_name(_class), _attr);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_trigger_utils_constr(_class regclass, _attr varchar) RETURNS VARCHAR AS $$
	SELECT format('_Constr_%s_%s', _cm3_utils_regclass_to_name(_class), _attr);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_trigger_utils_updrel(_class regclass, _attr varchar) RETURNS VARCHAR AS $$
	SELECT format('_UpdRel_%s_%s', _cm3_utils_regclass_to_name(_class), _attr);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_trigger_utils_updref(_class regclass, _attr varchar) RETURNS VARCHAR AS $$
	SELECT format('_UpdRef_%s_%s', _cm3_utils_regclass_to_name(_class), _attr);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_trigger_utils_notnull(_attr varchar) RETURNS VARCHAR AS $$
	SELECT format('_NotNull_%s', _attr);
$$ LANGUAGE SQL;


-- CLASS COMMENT -- 

CREATE OR REPLACE FUNCTION _cm3_class_comment_get(_class regclass) RETURNS varchar AS $$
	SELECT description FROM pg_description WHERE objoid = _class AND objsubid = 0;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_comment_get(_class regclass,_key varchar) RETURNS varchar AS $$
	SELECT _cm3_comment_get_part(_cm3_class_comment_get(_class),_key);
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_comment_get_jsonb(_class regclass) RETURNS jsonb AS $$
	SELECT _cm3_comment_to_jsonb(_cm3_class_comment_get(_class));
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_class_comment_set(_class regclass, _comment jsonb) RETURNS void AS $$ BEGIN
	RAISE NOTICE 'update class comment % = %', _class, _comment;
	EXECUTE _cm3_class_comment_set(_class, _cm3_comment_from_jsonb(_comment));
END $$ LANGUAGE PLPGSQL VOLATILE; 


--- CLASS METADATA ---

CREATE OR REPLACE FUNCTION _cm3_class_metadata_get(_classe regclass) RETURNS jsonb AS $$ BEGIN
	RETURN COALESCE((SELECT "Metadata" FROM "_ClassMetadata" WHERE "Owner" = _classe AND "Status" = 'A'),'{}'::jsonb);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_metadata_get(_classe regclass,_key varchar) RETURNS varchar AS $$ BEGIN
	RETURN jsonb_extract_path_text(_cm3_class_metadata_get(_classe),_key::text);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_metadata_set(_classe regclass,_metadata jsonb) RETURNS VOID AS $$ BEGIN
	IF EXISTS (SELECT 1 FROM "_ClassMetadata" WHERE "Owner" = _classe AND "Status" = 'A') THEN
		UPDATE "_ClassMetadata" SET "Metadata" = _metadata WHERE "Owner" = _classe AND "Status" = 'A';
	ELSE
		INSERT INTO "_ClassMetadata" ("Owner","Metadata") VALUES (_classe,_metadata);
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_metadata_set(_classe regclass, _key varchar, _value varchar) RETURNS VOID AS $$
DECLARE
	_metadata jsonb;
BEGIN
	_metadata = _cm3_class_metadata_get(_classe);
	_metadata = jsonb_set(_metadata,ARRAY[_key::text],to_jsonb(_value));
	PERFORM _cm3_class_metadata_set(_classe,_metadata);
END $$ LANGUAGE PLPGSQL;


--- CLASS MISC ---

CREATE OR REPLACE FUNCTION _cm3_class_parent_get(_classe regclass) RETURNS regclass AS $$ BEGIN
	RETURN COALESCE((SELECT inhparent::regclass FROM pg_inherits WHERE inhrelid = _classe AND _cm3_class_comment_get(inhparent::regclass, 'TYPE') IS NOT NULL LIMIT 1), NULL);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_utils_class_for_card(_superclass regclass, _card_id bigint) RETURNS regclass AS $$
DECLARE
	_res regclass;
BEGIN
	EXECUTE format('SELECT tableoid FROM %s WHERE "Id"= $1 LIMIT 1', _superclass) USING _card_id INTO _res;
	RETURN _res;
END $$ LANGUAGE PLPGSQL STABLE RETURNS NULL ON NULL INPUT;


-- CLASS CHECK --

CREATE OR REPLACE FUNCTION _cm3_class_is_simple(_class regclass) RETURNS boolean AS $$
	SELECT _cm3_class_comment_get(_class,'TYPE') = 'simpleclass';
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_is_standard(_class regclass) RETURNS boolean AS $$
	SELECT _cm3_class_comment_get(_class,'TYPE') = 'class';
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_is_simple_or_standard(_class regclass) RETURNS boolean AS $$
	SELECT  _cm3_class_comment_get(_class,'TYPE') IN ('class','simpleclass');
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_is_superclass(_class regclass) RETURNS boolean AS $$
	SELECT _cm3_class_comment_get(_class,'SUPERCLASS') = 'true';
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_has_records(_class regclass) RETURNS boolean AS $$
DECLARE
	_has_records boolean;
BEGIN
	EXECUTE format('SELECT EXISTS (SELECT 1 FROM %s)', _class) INTO _has_records;
	RETURN _has_records;
END $$ LANGUAGE PLPGSQL;


-- CLASS LIST --

CREATE OR REPLACE FUNCTION _cm3_class_list() RETURNS SETOF regclass AS $$
	SELECT oid::regclass FROM pg_class WHERE _cm3_class_is_simple_or_standard(oid) AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname='public');
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_list_simple() RETURNS SETOF regclass AS $$
	SELECT oid::regclass FROM pg_class WHERE _cm3_class_is_simple(oid) AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname='public');
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_list_standard() RETURNS SETOF regclass AS $$
	SELECT oid::regclass FROM pg_class WHERE _cm3_class_is_standard(oid) AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname='public');
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_comment_set(_class regclass, _comment varchar) RETURNS void AS $$ BEGIN
	EXECUTE format('COMMENT ON TABLE %s IS %L', _class, _comment);
END $$ LANGUAGE PLPGSQL VOLATILE;

CREATE OR REPLACE FUNCTION _cm3_class_comment_set(_class regclass,_key varchar,_value varchar) RETURNS void AS $$
	SELECT _cm3_class_comment_set(_class, _cm3_comment_from_jsonb(jsonb_set(_cm3_comment_to_jsonb(_cm3_class_comment_get(_class)),ARRAY[_key],to_jsonb(_value))));
$$ LANGUAGE SQL VOLATILE;

CREATE OR REPLACE FUNCTION _cm3_class_list_descendants_and_self(_class regclass) RETURNS SETOF regclass AS $$
	SELECT _class
		UNION
	SELECT i.inhrelid::regclass FROM pg_catalog.pg_inherits i WHERE i.inhparent = _class
		UNION
	SELECT _cm3_class_list_descendants_and_self(i.inhrelid) FROM pg_catalog.pg_inherits i WHERE i.inhparent = _class;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_class_list_descendant_classes_and_self(_class regclass) RETURNS SETOF regclass AS $$
	SELECT c FROM _cm3_class_list_descendants_and_self(_class) c WHERE _cm3_class_is_standard(c);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_class_list_descendant_classes_and_self_not_superclass(_class regclass) RETURNS SETOF regclass AS $$
	SELECT c FROM _cm3_class_list_descendant_classes_and_self(_class) c WHERE NOT _cm3_class_is_superclass(c);
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_process_list() RETURNS SETOF regclass AS $$
	SELECT _cm3_class_list_descendants_and_self('"Activity"'::regclass);
$$ LANGUAGE SQL STABLE;


-- CREATE OR REPLACE FUNCTION _cm3_class_list_descendants_and_self_of(parentoid IN regclass, schemaname OUT name, tablename OUT name) RETURNS SETOF record AS $$--TODO replace this with a oid->table name function below
-- SELECT pg_namespace.nspname, pg_class.relname 
--         FROM _cm3_class_list_descendants_and_self($1) inh(inhrelid) 
--           INNER JOIN pg_catalog.pg_class ON (inh.inhrelid = pg_class.oid) 
--           INNER JOIN pg_catalog.pg_namespace ON (pg_class.relnamespace = pg_namespace.oid);
-- $$ LANGUAGE 'sql';


-- ATTRIBUTE COMMENT --

CREATE OR REPLACE FUNCTION _cm3_attribute_comment_get(_class regclass, _attr varchar) RETURNS varchar AS $$
	SELECT description FROM pg_description
		JOIN pg_attribute ON pg_description.objoid = pg_attribute.attrelid AND pg_description.objsubid = pg_attribute.attnum
		WHERE attrelid = _class and attname = _attr;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_attribute_comment_get_jsonb(_class regclass, _attr varchar) RETURNS jsonb AS $$
	SELECT _cm3_comment_to_jsonb(_cm3_attribute_comment_get(_class, _attr));
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_comment_set(_class regclass, _attr varchar, _comment varchar) RETURNS void AS $$ BEGIN
	RAISE NOTICE 'update class attribute comment %.% = %', _class, _attr, _comment;
	EXECUTE format('COMMENT ON COLUMN %s.%I IS %L',_class,_attr,_comment);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_comment_get(_class regclass, _attr varchar, _part varchar) RETURNS varchar AS $$
	SELECT _cm3_comment_get_part(_cm3_attribute_comment_get(_class,_attr),_part);
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_attribute_comment_set(_class regclass, _attr varchar, _key varchar, _value varchar) RETURNS void AS $$
	SELECT _cm3_attribute_comment_set(_class,_attr,_cm3_comment_set_part(_cm3_attribute_comment_get(_class,_attr),_key,_value));
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_comment_delete(_class regclass, _attr varchar, _key varchar) RETURNS void AS $$
	SELECT _cm3_attribute_comment_set(_class,_attr,_cm3_comment_delete_part(_cm3_attribute_comment_get(_class,_attr),_key));
$$ LANGUAGE SQL;


--- ATTRIBUTE METADATA ---

CREATE OR REPLACE FUNCTION _cm3_attribute_metadata_get(_classe regclass, _attr varchar) RETURNS jsonb AS $$ BEGIN
	RETURN COALESCE((SELECT "Metadata" FROM "_AttributeMetadata" WHERE "Owner" = _classe AND "Code" = _attr AND "Status" = 'A'),'{}'::jsonb);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_metadata_set(_class regclass, _attr varchar, _metadata jsonb) RETURNS void AS $$ BEGIN
	IF EXISTS (SELECT * FROM "_AttributeMetadata" WHERE "Owner" = _class AND "Code" = _attr AND "Status" = 'A') THEN
		UPDATE "_AttributeMetadata" SET "Metadata" = _metadata WHERE "Owner" = _class AND "Code" = _attr AND "Status" = 'A';
	ELSE
		INSERT INTO "_AttributeMetadata" ("Owner","Code","Status","Metadata") VALUES (_class,_attr,'A',_metadata);
	END IF;
END $$ LANGUAGE plpgsql;


-- ATTRIBUTE CHECK --
 
CREATE OR REPLACE FUNCTION _cm3_attribute_unique_get(_class regclass, _attr varchar) RETURNS boolean AS $$ BEGIN --TODO check simple classes
	RETURN EXISTS (SELECT * FROM pg_class JOIN pg_index ON pg_class.oid = pg_index.indexrelid 
		WHERE pg_index.indrelid = _class 
		AND relname = format('_Unique_%s_%s',_cm3_utils_regclass_to_name(_class), _attr));
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_notnull_get(_class regclass, _attr varchar) RETURNS boolean AS $$
	SELECT pg_attribute.attnotnull OR c.oid IS NOT NULL
	FROM pg_attribute
	LEFT JOIN pg_constraint AS c ON c.conrelid = pg_attribute.attrelid AND c.conname::text = format('_NotNull_%s',pg_attribute.attname)
	WHERE pg_attribute.attrelid = _class AND pg_attribute.attname = _attr;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_is_inherited(_class regclass, _attr varchar) RETURNS boolean AS $$
	SELECT pg_attribute.attinhcount <> 0 FROM pg_attribute WHERE pg_attribute.attrelid = _class AND pg_attribute.attname = _attr;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_has_data(_class regclass, _attr varchar) RETURNS boolean AS $$
DECLARE
	_has_data boolean;
BEGIN
	EXECUTE format('SELECT EXISTS (SELECT 1 FROM %s WHERE %I IS NOT NULL AND %I::text <> '''')', _class, _attr, _attr) INTO _has_data;
	RETURN _has_data;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_has_value(_class regclass, _attr varchar, _value bigint) RETURNS boolean AS $$
DECLARE
	_has_value boolean;
BEGIN
	IF _cm3_class_is_simple(_class) THEN
		EXECUTE format('SELECT EXISTS (SELECT 1 FROM %s WHERE %I = %L)', _class, _attr, _value) INTO _has_value;
	ELSE
		EXECUTE format('SELECT EXISTS (SELECT 1 FROM %s WHERE %I = %L AND "Status" = ''A'')', _class, _attr, _value) INTO _has_value;
	END IF;
	RETURN _has_value;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_sqltype_get(_class regclass, _attr varchar) RETURNS varchar AS $$
	SELECT _cm3_utils_build_sqltype_string(pg_attribute.atttypid, pg_attribute.atttypmod) FROM pg_attribute WHERE pg_attribute.attrelid = _class AND pg_attribute.attname = _attr;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_attribute_default_get(_class regclass, _attr varchar) RETURNS varchar AS $$
	SELECT pg_attrdef.adsrc FROM pg_attribute JOIN pg_attrdef ON pg_attrdef.adrelid = pg_attribute.attrelid AND pg_attrdef.adnum = pg_attribute.attnum WHERE pg_attribute.attrelid = _class AND pg_attribute.attname = _attr;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_attribute_is_reference(_class regclass, _attr varchar) RETURNS BOOLEAN AS $$ 
	SELECT _cm3_attribute_comment_get(_class, _attr, 'REFERENCEDOM') <> '' --TODO check this
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_reference_domain_get(_class regclass, _attr varchar) RETURNS regclass AS $$ 
	SELECT _cm3_utils_name_to_regclass('Map_' || _cm3_attribute_comment_get(_class, _attr, 'REFERENCEDOM'))
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_reference_domain_direct_is(_class regclass, _attr varchar) RETURNS boolean AS $$ 
	SELECT _cm3_attribute_comment_get(_class, _attr, 'REFERENCEDIRECT')::boolean;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_is_lookup(_class regclass, _attr varchar) RETURNS BOOLEAN AS $$ 
	SELECT _cm3_attribute_comment_get(_class, _attr, 'LOOKUP') <> '' --TODO check this
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_is_foreignkey(_class regclass, _attr varchar) RETURNS BOOLEAN AS $$ 
	SELECT _cm3_attribute_comment_get(_class, _attr, 'FKTARGETCLASS') <> '' --TODO check this
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_foreignkey_target_get(_class regclass, _attr varchar) RETURNS regclass AS $$ 
	SELECT _cm3_utils_name_to_regclass(_cm3_attribute_comment_get(_class, _attr, 'FKTARGETCLASS'))
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_reference_target_class_get(_class regclass, _attr varchar) RETURNS regclass AS $$ BEGIN
	RETURN _cm3_domain_target_class_get(_cm3_attribute_reference_domain_get(_class, _attr), _cm3_attribute_reference_domain_direct_is(_class, _attr));
END $$ LANGUAGE PLPGSQL STABLE;


--- ATTRIBUTE LIST ---

CREATE OR REPLACE FUNCTION _cm3_attribute_exists(_class regclass, _attr varchar) RETURNS boolean AS $$
	SELECT EXISTS (SELECT * FROM pg_attribute WHERE attrelid = _class AND attnum > 0 AND atttypid > 0 AND attname::varchar = _attr);
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_attribute_list(_class regclass) RETURNS SETOF varchar AS $$
	SELECT attname::varchar FROM pg_attribute WHERE attrelid = _class AND attnum > 0 AND atttypid > 0 ORDER BY attnum;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_attribute_list_detailed() RETURNS TABLE(
	owner regclass, 
	name varchar, 
	comment jsonb, 
	not_null_constraint boolean, 
	unique_constraint boolean,
	sql_type varchar,
	inherited boolean,
	default_value varchar,
	metadata jsonb
) AS $$ BEGIN
	RETURN QUERY SELECT
		_class::regclass AS owner,
		attr_name::varchar AS name,
		_cm3_attribute_comment_get_jsonb(_class, attr_name)::varchar AS comment,
		_cm3_attribute_notnull_get(_class, attr_name) AS not_null_constraint, --TODO migrate fun
		_cm3_attribute_unique_get(_class, attr_name) AS unique_constraint, 
		_cm3_attribute_sqltype_get(_class, attr_name)::varchar AS sql_type, --TODO migrate fun
		_cm3_attribute_is_inherited(_class, attr_name) AS inherited, --TODO migrate fun
		_cm3_attribute_default_get(_class, attr_name)::varchar AS default_value, --TODO migrate fun
		_cm3_attribute_metadata_get(_class, attr_name) AS metadata
	FROM 
		(SELECT c.x _class, _cm3_attribute_list(c.x) attr_name FROM (SELECT x FROM _cm3_class_list() x UNION SELECT x FROM _cm3_domain_list() x) c) a 
	ORDER BY 
		_cm3_attribute_comment_get(_class, attr_name, 'INDEX')::int;
END $$ LANGUAGE PLPGSQL;


--- DOMAIN CHECK

CREATE OR REPLACE FUNCTION _cm3_domain_target_class_get(_domain regclass, _direct boolean) RETURNS regclass AS $$ BEGIN
	IF _direct THEN
		RETURN _cm3_utils_name_to_regclass(_cm3_class_attribute_get_jsonb('CLASS2'));
	ELSE
		RETURN _cm3_utils_name_to_regclass(_cm3_class_attribute_get_jsonb('CLASS1'));
	END IF;
END $$ LANGUAGE PLPGSQL STABLE;


--- DOMAIN MISC

CREATE OR REPLACE FUNCTION _cm3_domain_utils_class_for_dir(_domain regclass, _dir varchar) RETURNS regclass AS $$
	SELECT _cm3_utils_name_to_regclass(_cm3_class_comment_get(_domain,CASE _dir WHEN 'IdObj1' THEN 'CLASS1' WHEN 'IdObj2' THEN 'CLASS2' ELSE NULL END))
$$ LANGUAGE SQL STABLE RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION _cm3_domain_utils_class_for_dir_and_card(_domain regclass, _dir varchar, _card_id bigint) RETURNS regclass AS $$
	SELECT _cm3_class_utils_class_for_card(_cm3_domain_utils_class_for_dir(_domain, _dir), _card_id)
$$ LANGUAGE SQL STABLE RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION _cm3_relation_utils_class_attr_from_card_attr(_card_attr varchar) returns varchar AS $$
	SELECT 'IdClass'||substring(_card_attr from '^IdObj(.)+');
$$ LANGUAGE SQL STABLE RETURNS NULL ON NULL INPUT;


--- DOMAIN LIST ---

CREATE OR REPLACE FUNCTION _cm3_domain_list() RETURNS SETOF regclass AS $$
	SELECT oid::regclass FROM pg_class WHERE _cm3_class_comment_get(oid,'TYPE') = 'domain' AND relnamespace = (SELECT oid FROM pg_namespace WHERE nspname='public');
$$ LANGUAGE SQL STABLE;


--- CARD MISC ---

CREATE OR REPLACE FUNCTION _cm3_card_description_get(_class regclass, _card_id bigint) RETURNS varchar AS $$ 
DECLARE
	_description VARCHAR;
	_ignore_tenant_policies VARCHAR;
BEGIN

	IF _card_id IS NULL THEN
		RETURN NULL;
	ELSE

		_ignore_tenant_policies = current_setting('cmdbuild.ignore_tenant_policies');
		SET SESSION cmdbuild.ignore_tenant_policies = 'true';

		IF _cm3_class_is_simple(_class) THEN
			EXECUTE format('SELECT "Description" FROM %s WHERE "Id" = %L', _class, _card_id) INTO _description;
		ELSE
			EXECUTE format('SELECT "Description" FROM %s WHERE "Id" = %L AND "Status" = ''A''', _class, _card_id) INTO _description;
		END IF;

		SET SESSION cmdbuild.ignore_tenant_policies = _ignore_tenant_policies;

		RETURN _description;

	END IF;

END $$ LANGUAGE PLPGSQL;


--- CARD METADATA ---

CREATE OR REPLACE FUNCTION _cm3_card_metadata_set(_classe regclass,_card bigint,_metadata jsonb) RETURNS VOID AS $$ BEGIN
	IF NOT EXISTS (SELECT * FROM "_CardMetadata" WHERE "OwnerClass" = _classe AND "OwnerCard" = _card AND "Status" = 'A') THEN
		INSERT INTO "_CardMetadata" ("OwnerClass","OwnerCard","Data") VALUES (_classe,_card,_metadata);
	ELSE
		UPDATE "_CardMetadata" SET "Data" = _metadata WHERE "OwnerClass" = _classe AND "OwnerCard" = _card AND "Status" = 'A';
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_card_metadata_get(_classe regclass,_card bigint) RETURNS jsonb AS $$ BEGIN
	RETURN COALESCE((SELECT "Data" FROM "_CardMetadata" WHERE "OwnerClass" = _classe AND "OwnerCard" = _card AND "Status" = 'A'),'{}'::jsonb);
END $$ LANGUAGE PLPGSQL;


--- FUNCTION COMMENT ---

CREATE OR REPLACE FUNCTION _cm3_function_comment_get(_function oid) RETURNS varchar AS $$
	SELECT description FROM pg_description WHERE objoid = _function;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_function_comment_get_jsonb(_function oid) RETURNS jsonb AS $$
	SELECT _cm3_comment_to_jsonb(_cm3_function_comment_get(_function));
$$ LANGUAGE SQL STABLE;


--- FUNCTION LIST ---

CREATE OR REPLACE FUNCTION _cm3_function_list(
		OUT function_name text,
		OUT function_id oid,
		OUT arg_io char[],
		OUT arg_names text[],
		OUT arg_types text[],
		OUT returns_set boolean,
		OUT comment text,
		OUT metadata jsonb
	) RETURNS SETOF record AS $$
DECLARE
	_record record;
	_index integer;
BEGIN
	FOR _record IN
		SELECT * FROM pg_proc WHERE _cm_comment_for_cmobject(oid) IS NOT NULL --TODO
	LOOP
		function_name := _record.proname::text;
		function_id := _record.oid;
		returns_set := _record.proretset;
		comment := _cm_comment_for_cmobject(_record.oid);
		metadata = _cm3_function_metadata_get(function_id);
		IF _record.proargmodes IS NULL
		THEN
			arg_io := '{}'::char[];
			arg_types := '{}'::text[];
			arg_names := '{}'::text[];
			-- add input columns
			FOR _index IN SELECT generate_series(1, array_upper(_record.proargtypes,1)) LOOP
				arg_io := arg_io || 'i'::char;
				arg_types := arg_types || _cm_get_sqltype_string(_record.proargtypes[_index], NULL);
				arg_names := arg_names || COALESCE(_record.proargnames[_index], '$'||_index);
			END LOOP;
			-- add single output column
			arg_io := arg_io || 'o'::char;
			arg_types := arg_types || _cm_get_sqltype_string(_record.prorettype, NULL);
			arg_names := arg_names || function_name;
		ELSE
			-- just normalize existing columns
			arg_io := _record.proargmodes;
			arg_types := '{}'::text[];
			arg_names := _record.proargnames;
			FOR _index IN SELECT generate_series(1, array_upper(arg_io,1)) LOOP
				-- normalize table output
				IF arg_io[_index] = 't' THEN
					arg_io[_index] := 'o';
				ELSIF arg_io[_index] = 'b' THEN
					arg_io[_index] := 'io';
				END IF;
				arg_types := arg_types || _cm_get_sqltype_string(_record.proallargtypes[_index], NULL);
				IF arg_names[_index] = '' THEN
					IF arg_io[_index] = 'i' THEN
						arg_names[_index] = '$'||_index;
					ELSE
						arg_names[_index] = 'column'||_index;
					END IF;
				END IF;
			END LOOP;
		END IF;
		RETURN NEXT;
	END LOOP;

	RETURN;
END $$ LANGUAGE PLPGSQL STABLE;

--- FUNCTION METADATA ---

CREATE OR REPLACE FUNCTION _cm3_function_metadata_get(_function oid) RETURNS jsonb AS $$ BEGIN
	RETURN COALESCE((SELECT "Data" FROM "_FunctionMetadata" WHERE "OwnerFunction" = _function AND "Status" = 'A'),'{}'::jsonb);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_function_metadata_get(_function oid,_key varchar) RETURNS varchar AS $$ BEGIN
	RETURN jsonb_extract_path_text(_cm3_class_metadata_get(_function),_key::text);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_function_metadata_set(_function oid,_metadata jsonb) RETURNS VOID AS $$ BEGIN
	IF EXISTS (SELECT 1 FROM "_FunctionMetadata" WHERE "Owner" = _function AND "Status" = 'A') THEN
		UPDATE "_FunctionMetadata" SET "Data" = _metadata WHERE "OwnerFunction" = _function AND "Status" = 'A';
	ELSE
		INSERT INTO "_FunctionMetadata" ("OwnerFunction","Data") VALUES (_function,_metadata);
	END IF;
END $$ LANGUAGE PLPGSQL;


--- USER CONFIG ---


CREATE OR REPLACE FUNCTION _cm3_user_config_get(_userid bigint) RETURNS jsonb AS $$
BEGIN
	RETURN COALESCE((SELECT "Data" FROM "_UserConfig" WHERE "Owner" = _userid AND "Status" = 'A'),'{}'::jsonb);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_user_config_set(_userid bigint,_data jsonb) RETURNS VOID AS $$
BEGIN
 	IF EXISTS (SELECT 1 FROM "_UserConfig" WHERE "Owner" = _userid AND "Status" = 'A') THEN	
		UPDATE "_UserConfig" SET "Data" = _data WHERE "Owner" = _userid AND "Status" = 'A';
	ELSE
		INSERT INTO "_UserConfig" ("Owner","Data") VALUES (_userid,_data);
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_user_config_get(_username varchar) RETURNS jsonb AS $$
BEGIN
	RETURN (SELECT _cm3_user_config_get((SELECT "Id" FROM "User" WHERE "Username" = _username AND "Status" = 'A')));
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_user_config_set(_username varchar,_data jsonb) RETURNS VOID AS $$
BEGIN
	PERFORM _cm3_user_config_set((SELECT "Id" FROM "User" WHERE "Username" = _username AND "Status" = 'A'),_data);
END $$ LANGUAGE PLPGSQL;