-- function detail fix

CREATE OR REPLACE FUNCTION _cm3_function_list_detailed( --note: fix already included in base patch 3.0.0-22
		OUT function_name text,
		OUT function_id oid,
		OUT arg_io char[],
		OUT arg_names text[],
		OUT arg_types text[],
		OUT returns_set boolean,
		OUT comment jsonb,
		OUT metadata jsonb
	) RETURNS SETOF record AS $$
DECLARE
	_record record;
	_index integer;
BEGIN
	FOR _record IN SELECT *,oid FROM pg_proc WHERE oid IN (SELECT _cm3_function_list()) LOOP
		function_name := _record.proname::text;
		function_id := _record.oid;
		returns_set := _record.proretset;
		comment := _cm3_function_comment_get_jsonb(_record.oid);
		metadata = _cm3_function_metadata_get(function_id);
		IF _record.proargmodes IS NULL
		THEN
			arg_io := '{}'::char[];
			arg_types := '{}'::text[];
			arg_names := '{}'::text[];
			-- add input columns
			FOR _index IN SELECT generate_series(1, array_upper(_record.proargtypes,1)) LOOP
				arg_io := arg_io || 'i'::char;
				arg_types := arg_types || _cm_get_sqltype_string(_record.proargtypes[_index], NULL);
				arg_names := arg_names || COALESCE(_record.proargnames[_index], '$'||_index);
			END LOOP;
			-- add single output column
			arg_io := arg_io || 'o'::char;
			arg_types := arg_types || _cm_get_sqltype_string(_record.prorettype, NULL);
			arg_names := arg_names || function_name;
		ELSE
			-- just normalize existing columns
			arg_io := _record.proargmodes;
			arg_types := '{}'::text[];
			arg_names := _record.proargnames;
			FOR _index IN SELECT generate_series(1, array_upper(arg_io,1)) LOOP
				-- normalize table output
				IF arg_io[_index] = 't' THEN
					arg_io[_index] := 'o';
				ELSIF arg_io[_index] = 'b' THEN
					arg_io[_index] := 'io';
				END IF;
				arg_types := arg_types || _cm_get_sqltype_string(_record.proallargtypes[_index], NULL);
				IF arg_names[_index] = '' THEN
					IF arg_io[_index] = 'i' THEN
						arg_names[_index] = '$'||_index;
					ELSE
						arg_names[_index] = 'column'||_index;
					END IF;
				END IF;
			END LOOP;
		END IF;
		RETURN NEXT;
	END LOOP;

	RETURN;
END $$ LANGUAGE PLPGSQL STABLE;

