-- Role table refactoring

SELECT _cm3_attribute_create('"Role"', 'Type', 'varchar', 'MODE: write|DESCR: Role Type|STATUS: active');
SELECT _cm3_attribute_create('"Role"', 'Permissions', 'jsonb', 'DEFAULT: ''{}''::jsonb|MODE: write|DESCR: Role Permissions|STATUS: active');

ALTER TABLE "Role" DISABLE TRIGGER USER;

UPDATE "Role" SET "Type" = 'admin' WHERE "Administrator" = true AND "Status" = 'A';
UPDATE "Role" SET "Type" = 'default' WHERE "Type" IS NULL AND "Status" = 'A';
UPDATE "Role" SET "Permissions" = '{}'::jsonb WHERE "Permissions" IS NULL;

ALTER TABLE "Role" DROP COLUMN "Administrator";

ALTER TABLE "Role" ENABLE TRIGGER USER;

SELECT _cm3_attribute_notnull_set('"Role"', 'Type', true);
SELECT _cm3_attribute_notnull_set('"Role"', 'Permissions', true);

DO $$ DECLARE
	_role record;
	_perms jsonb;
	_key varchar;
BEGIN
	ALTER TABLE "Role" DISABLE TRIGGER USER;

	FOR _role IN SELECT * FROM "Role" LOOP
		_perms = _role."Permissions";
		FOR _key IN SELECT jsonb_array_elements_text(_role."Config"->'disabledModules') LOOP
			_perms = jsonb_set(_perms, ARRAY[format('%s_access', lower(_key))], to_jsonb(false));
		END LOOP;
		FOR _key IN SELECT jsonb_array_elements_text(_role."Config"->'disabledCardTabs') LOOP
			_perms = jsonb_set(_perms, ARRAY[format('card_tab_%s_access', regexp_replace(lower(_key),'(^class)|(tab$)','','g'))], to_jsonb(false));
		END LOOP;
		FOR _key IN SELECT jsonb_array_elements_text(_role."Config"->'disabledProcessTabs') LOOP
			_perms = jsonb_set(_perms, ARRAY[format('flow_tab_%s_access', regexp_replace(lower(_key),'(^process)|(tab$)','','g'))], to_jsonb(false));
		END LOOP;

		UPDATE "Role" SET "Permissions" = _perms, "Config" = "Config" - 'disabledModules' - 'disabledCardTabs' - 'disabledProcessTabs' WHERE "Id" = _role."Id";
	END LOOP;
	
	ALTER TABLE "Role" ENABLE TRIGGER USER;
END $$ LANGUAGE PLPGSQL;
 

-- DisabledModules             | {bulkUpdate,changePassword,class,customPages,dashboard,dataView,exportCsv,importCsv,process,report}
-- DisabledCardTabs            | {classAttachmentTab,classDetailTab,classEmailTab,classHistoryTab,classNoteTab,classRelationTab}
-- DisabledProcessTabs         | {processAttachmentTab,processEmailTab,processHistoryTab,processNoteTab,processRelationTab}
-- -- 
-- UPDATE "Role" SET "DisabledProcessTabs" = '{processAttachmentTab,processEmailTab,processHistoryTab,processNoteTab,processRelationTab}' WHERE "Id" = 3893;
-- UPDATE "Role" SET "DisabledCardTabs" = '{classAttachmentTab,classDetailTab,classEmailTab,classHistoryTab,classNoteTab,classRelationTab}' WHERE "Id" = 3893;
-- UPDATE "Role" SET "DisabledModules" = '{bulkUpdate,changePassword,class,customPages,dashboard,dataView,exportCsv,importCsv,process,report}' WHERE "Id" = 3893;
-- 
