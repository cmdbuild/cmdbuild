-- report upgrade

SELECT _cm3_class_create('_Report', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Report|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Report"', 'Active', 'boolean', 'DEFAULT: true|NOTNULL: true|MODE: read|DESCR: Is Active');
SELECT _cm3_attribute_create('"_Report"', 'Query', 'text', 'MODE: read|DESCR: Query');
SELECT _cm3_attribute_create('"_Report"', 'SimpleReport', 'bytea', 'MODE: rescore');
SELECT _cm3_attribute_create('"_Report"', 'RichReport', 'bytea', 'MODE: rescore');
SELECT _cm3_attribute_create('"_Report"', 'Wizard', 'bytea', 'MODE: rescore');
SELECT _cm3_attribute_create('"_Report"', 'Images', 'bytea', 'MODE: rescore');
SELECT _cm3_attribute_create('"_Report"', 'ImagesLength', 'integer[]', 'MODE: rescore');
SELECT _cm3_attribute_create('"_Report"', 'ReportLength', 'integer[]', 'MODE: rescore');
SELECT _cm3_attribute_create('"_Report"', 'Groups', 'varchar[]', 'MODE: read');
SELECT _cm3_attribute_create('"_Report"', 'ImagesName', 'varchar[]', 'MODE: rescore');

CREATE UNIQUE INDEX "_Report_unique_code" ON "_Report" ("Code") WHERE "Status" = 'A';

ALTER TABLE "_Report" DISABLE TRIGGER USER;

INSERT INTO "_Report" (
	"Id",
	"CurrentId",
	"IdClass",
	"Code",
	"Description",
	"Status",
	"User",
	"BeginDate",
	"Query",
	"Active",
	"SimpleReport",
	"RichReport",
	"Wizard",
	"Images",
	"ImagesLength",
	"ReportLength",
	"Groups",
	"ImagesName"
) SELECT 
	"Id",
	"Id",
	'"_Report"'::regclass,
	"Code",
	"Description",
	'A',
	"User",
	"BeginDate",
	"Query",
	CASE WHEN "Status"='A' THEN true ELSE false END,
	"SimpleReport",
	"RichReport",
	"Wizard",
	"Images",
	"ImagesLength",
	"ReportLength",
	"Groups",
	"ImagesName"
FROM 
	"Report";

ALTER TABLE "_Report" ENABLE TRIGGER USER;

UPDATE "Menu" SET "IdElementClass" = '"_Report"'::regclass where "IdElementClass" = '"Report"'::regclass AND "Status" = 'A';

DROP TABLE "Report";

CREATE OR REPLACE FUNCTION _patch_30039_aux(sourcebytes IN bytea,length_array IN integer[]) RETURNS bytea[] AS $$
DECLARE
	len integer;
	res bytea[];
	start_index integer;
BEGIN

	res = array[]::bytea[];
	start_index = 1;

	FOREACH len IN ARRAY length_array LOOP
		res = array_append(res, substring(sourcebytes FROM start_index FOR len));
		start_index = start_index + len;
	END LOOP;

	RETURN res;
END;
$$ LANGUAGE PLPGSQL;

ALTER TABLE "_Report" RENAME COLUMN "Images" TO "ImagesOld";

SELECT _cm3_attribute_create('"_Report"', 'RichReports', 'bytea[]', 'MODE: reserved');
SELECT _cm3_attribute_create('"_Report"', 'Images', 'bytea[]', 'MODE: reserved');

ALTER TABLE "_Report" DISABLE TRIGGER USER;

UPDATE "_Report" SET "RichReports" = _patch_30039_aux("RichReport","ReportLength") WHERE "RichReport" IS NOT NULL AND "ReportLength" IS NOT NULL;
UPDATE "_Report" SET "Images" =  _patch_30039_aux("ImagesOld","ImagesLength") WHERE "ImagesOld" IS NOT NULL AND "ImagesLength" IS NOT NULL;

UPDATE "_Report" SET "RichReports" = '{}' WHERE "RichReports" IS NULL;
UPDATE "_Report" SET "Images" = '{}' WHERE "Images" IS NULL; 

ALTER TABLE "_Report" DROP COLUMN "RichReport";
ALTER TABLE "_Report" DROP COLUMN "ReportLength";
ALTER TABLE "_Report" DROP COLUMN "ImagesOld";
ALTER TABLE "_Report" DROP COLUMN "ImagesLength";

ALTER TABLE "_Report" ENABLE TRIGGER USER;

UPDATE "_Report" SET "Status" = 'N' WHERE ( "Images" IS NULL OR "RichReports" IS NULL OR "SimpleReport" IS NULL ) AND ("Status" = 'A'); --TODO check this (null check on array is weird)

DROP FUNCTION   _patch_30039_aux(sourcebytes IN bytea,length_array IN integer[]);

ALTER TABLE "_Report" DISABLE TRIGGER USER;
ALTER TABLE "_Report_history" DISABLE TRIGGER USER;

UPDATE "_Report" SET "Wizard" = NULL WHERE "Wizard" IS NOT NULL;

-- UPDATE "_Report" SET "SimpleReport" = "RichReports"[1], "RichReports" = "RichReports"[2:array_length("RichReports",1)] WHERE "Status" = 'A';
UPDATE "_Report" SET "SimpleReport" = "RichReports"[1], "RichReports" = "RichReports"[2:array_length("RichReports",1)];

INSERT INTO "_Grant" ("IdRole","Mode","Type","ObjectId") 
	SELECT (SELECT "Id" FROM "Role" WHERE "Code" = r.role_name AND "Status" = 'A'),'r','Report',r.report_id FROM --TODO handle case where role is not found for name
		(SELECT unnest("Groups") role_name, "Id" report_id FROM "_Report" WHERE "Status" = 'A') r;

UPDATE "_Report" SET "Groups" = NULL;

ALTER TABLE "_Report_history" ENABLE TRIGGER USER;
ALTER TABLE "_Report" ENABLE TRIGGER USER;

SELECT _cm3_attribute_delete('"_Report"', 'Wizard');
SELECT _cm3_attribute_delete('"_Report"', 'Groups');

SELECT _cm3_attribute_notnull_set('"_Report"', 'Active', true);
SELECT _cm3_attribute_notnull_set('"_Report"', 'Code', true);

