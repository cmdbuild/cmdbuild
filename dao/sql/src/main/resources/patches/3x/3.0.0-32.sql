-- dms table fix

ALTER TABLE "_Document" DROP COLUMN "DmsFileName";
ALTER TABLE "_DocumentData" ALTER COLUMN "DocumentId" TYPE bigint;

SELECT _cm3_attribute_create('"_Document"', 'CreationDate', 'timestamp', 'NOTNULL: true|MODE: read|DESCR: Creation Date');

CREATE UNIQUE INDEX "_Unique_Document_CardIdFileName" ON "_Document" ("CardId", "FileName") WHERE "Status" = 'A';
