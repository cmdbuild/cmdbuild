-- cm3 trigger functions


--- TRIGGER HELPERS ---

CREATE OR REPLACE FUNCTION _cm3_trigger_fail_if_reference_value_exists(_class regclass, _attr varchar, _value bigint) RETURNS VOID AS $$ BEGIN
	IF _cm3_attribute_has_value(_class, _attr, _value) THEN
		RAISE EXCEPTION 'CM_RESTRICT_VIOLATION: forbidden operation: found record in table % with attr % = %',_class,_attr,_value;
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_reference_update(_class regclass, _attr varchar, _card bigint, _value bigint) RETURNS void AS $$ BEGIN
	IF _value IS NULL THEN
		EXECUTE format('UPDATE %s SET %I = NULL WHERE "Status" = ''A'' AND "Id" = %L AND %I IS NOT NULL', _class, _attr, _card, _attr);
	ELSE
		EXECUTE format('UPDATE %s SET %I = %L WHERE "Status" = ''A'' AND "Id" = %L AND %I <> %L', _class, _attr, _value, _card, _attr, _value);
	END IF;
END $$ LANGUAGE PLPGSQL;


--- TRIGGER FUNCTIONS ---

CREATE OR REPLACE FUNCTION _cm_trigger_fk() RETURNS trigger AS $$
DECLARE
	_attribute_name text := TG_ARGV[0];
	_class regclass := TG_ARGV[1]::regclass;
	_fk_mode text := TG_ARGV[2];
	_reference_value bigint;
	_include_deleted boolean;
BEGIN
	RAISE DEBUG 'Trigger % on %', TG_NAME, TG_TABLE_NAME;
--	EXECUTE 'SELECT ($1).' || quote_ident(_attribute_name) INTO _reference_value USING NEW; -- pg84
-- 	RAISE NOTICE 'Trigger % on % ', TG_NAME, TG_TABLE_NAME;
-- 	RAISE '%', format('SELECT (%L::%s).%I',NEW,TG_RELID::regclass,_attribute_name);
	EXECUTE format('SELECT (%L::%s).%I',NEW,TG_RELID::regclass,_attribute_name) INTO _reference_value;

	IF (_fk_mode = 'simple') THEN
		_include_deleted := FALSE;
	ELSE
		_include_deleted := NEW."Status" = 'A';
	END IF;

	IF _reference_value IS NOT NULL AND NOT _cm3_card_exists_with_id(_class,_reference_value, _include_deleted) THEN
		RAISE 'CM: error while inserting new % record: card not found for class = % card_id = % (referenced from attr %.% )', TG_RELID::regclass, _class, _reference_value, _cm3_utils_regclass_to_name(TG_RELID::regclass), _attribute_name;
	END IF;

	RETURN NEW;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm_trigger_sanity_check() RETURNS trigger AS $$
BEGIN
	IF (TG_OP='UPDATE') THEN
		IF (NEW."Id" <> OLD."Id") THEN -- Id change
			RAISE EXCEPTION 'cm constraint violated: id update is not allowed';
		END IF;
		IF (NEW."Status"='N' AND OLD."Status"='N') THEN -- Deletion of a deleted card
			RAISE EXCEPTION 'cm constraint violated: cannot delete this card, it has already "Status" = "N"';
		END IF;
	ELSIF (TG_OP='INSERT') THEN
		IF (NEW."Status" IS NULL) THEN
			NEW."Status"='A';
		ELSIF (NEW."Status"='N') THEN -- Creation of a deleted card
			RAISE EXCEPTION 'cm constraint violated: cannot INSERT a card with status "N"';
		END IF;
		NEW."Id" = _cm3_utils_new_card_id();
		-- Class ID is needed because of the history tables
		BEGIN
			NEW."IdClass" = TG_RELID;
		EXCEPTION WHEN undefined_column THEN
			NEW."IdDomain" = TG_RELID;
		END;
	ELSE -- TG_OP='DELETE'
		RAISE EXCEPTION 'cm constraint violated: DELETE is not allowed for this table, you should UPDATE "Status" flag instead';
	END IF;

	-- 'U' is reserved for history tables only
	IF (position(NEW."Status" IN 'AND') = 0) THEN -- Invalid status
		RAISE EXCEPTION 'cm constraint violated: invalid card status = %', NEW."Status";
	END IF;
	NEW."BeginDate" = now();
	RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm_trigger_create_card_history_row() RETURNS trigger AS $$
DECLARE
	_attr_list_str varchar;
BEGIN
	IF (TG_OP='UPDATE') THEN

		OLD."Id" = _cm3_utils_new_card_id();
		OLD."Status" = 'U';
		OLD."EndDate" = now();

		SELECT INTO _attr_list_str string_agg(x.x,',') from (select quote_ident(x) x from _cm3_attribute_list(TG_RELID::regclass) x) x;

		EXECUTE format('INSERT INTO "%s_history" (%s) VALUES ( (%L::%s).* )', _cm3_utils_regclass_to_name(TG_RELID::regclass), _attr_list_str, OLD, TG_RELID::regclass);

	ELSEIF (TG_OP='DELETE') THEN
		RAISE EXCEPTION 'cm constraint violated: DELETE is not allowed for this table, use SET "Status"=''N'' instead';
	END IF;
	RETURN NEW;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm_trigger_restrict() RETURNS trigger AS $$ --TODO change name
DECLARE
	_class regclass = TG_ARGV[0]::regclass;
	_attr varchar = TG_ARGV[1];
BEGIN
	IF (TG_OP='UPDATE') THEN
		IF( NEW."Status"='N') THEN
			PERFORM _cm3_trigger_fail_if_reference_value_exists(_class, _attr, OLD."Id");
		END IF;
		RETURN NEW;
	ELSE -- TG_OP='DELETE'
		RETURN OLD;
	END IF;
END $$	LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm_trigger_update_reference() RETURNS trigger AS $$
DECLARE
	_attribute_name text := TG_ARGV[0];
	_class regclass := TG_ARGV[1]::regclass;
	_card_column text := TG_ARGV[2]; -- Domain column name for the card id
	_reference_column text := TG_ARGV[3];  -- Domain column name for the reference id

	_old_card_id bigint;
	_new_card_id bigint;
	_old_ref_value bigint;
	_new_ref_value bigint;
BEGIN
	RAISE DEBUG 'Trigger % on %', TG_NAME, TG_TABLE_NAME;
	IF (NEW."Status"='A') THEN
--		EXECUTE 'SELECT ($1).' || quote_ident(_reference_column) INTO _new_ref_value USING NEW; -- pg84
		EXECUTE 'SELECT (' || quote_literal(NEW) || '::' || TG_RELID::regclass || ').' || quote_ident(_reference_column) INTO _new_ref_value;
	ELSIF (NEW."Status"<>'N') THEN
		-- Ignore history rows
		RETURN NEW;
	END IF;

--	EXECUTE 'SELECT ($1).' || quote_ident(_card_column) INTO _new_card_id USING NEW; -- pg84
	EXECUTE 'SELECT (' || quote_literal(NEW) || '::' || TG_RELID::regclass || ').' || quote_ident(_card_column) INTO _new_card_id;

	IF (TG_OP='UPDATE') THEN
--		EXECUTE 'SELECT ($1).' || quote_ident(_card_column) INTO _old_card_id USING OLD; -- pg84
		EXECUTE 'SELECT (' || quote_literal(OLD) || '::' || TG_RELID::regclass || ').' || quote_ident(_card_column) INTO _old_card_id;
		IF (_old_card_id <> _new_card_id) THEN -- If the non-reference side changes...
			PERFORM _cm3_reference_update(_class, _attribute_name, _old_card_id, NULL);
			-- _old_ref_value is kept null because it is like a new relation
		ELSE
--			EXECUTE 'SELECT ($1).' || quote_ident(_card_column) INTO _old_ref_value USING OLD; -- pg84
			EXECUTE 'SELECT (' || quote_literal(OLD) || '::' || TG_RELID::regclass || ').' || quote_ident(_reference_column) INTO _old_ref_value;
		END IF;
	END IF;

	IF ((_new_ref_value IS NULL) OR (_old_ref_value IS NULL) OR (_old_ref_value <> _new_ref_value)) THEN
		PERFORM _cm3_reference_update( _class, _attribute_name, _new_card_id, _new_ref_value);
	END IF;

	RETURN NEW;
END $$	LANGUAGE plpgsql;
