-- fix class and attr permissions (within comments)


DO $$
DECLARE
	_class regclass;
	_attribute varchar;
	_fieldmode varchar;
	_mode varchar;
	_newmode varchar;
BEGIN

-- 	FOR _class IN SELECT * FROM (SELECT _cm3_domain_list() UNION SELECT _cm3_class_list()) c LOOP
	FOR _class IN  SELECT _cm3_domain_list() UNION SELECT _cm3_class_list() LOOP

		_mode = _cm3_class_comment_get(_class,'MODE');

		IF _mode IN ('baseclass', 'read') OR _mode LIKE 'sys%' THEN
			_newmode = 'protected';
		ELSEIF _mode = 'reserved' THEN
			_newmode = _mode;
		ELSE
			_newmode = 'default';
		END IF;

		IF _newmode <> _mode THEN
			RAISE NOTICE 'fix class mode for class %, from % to %', _class, _mode, _newmode;
			PERFORM _cm3_class_comment_set(_class, 'MODE', _newmode);
		END IF;

	END LOOP;

	FOR _class IN SELECT _cm3_class_list() LOOP
		FOR _attribute IN SELECT _cm3_attribute_list(_class) LOOP

			_fieldmode = NULLIF(_cm3_attribute_comment_get(_class,_attribute,'FIELDMODE'),'');
			_mode = NULLIF(_cm3_attribute_comment_get(_class,_attribute,'MODE'),'');			

			IF _fieldmode IS NOT NULL THEN
				_newmode = _fieldmode;
				PERFORM _cm3_attribute_comment_delete(_class,_attribute,'FIELDMODE');
			ELSE
				_newmode = _mode;
			END IF;

			IF _newmode LIKE 'sys%' THEN
				_newmode = 'sysread';
			END IF;

			IF _newmode NOT IN ('read','write','reserved','hidden','immutable','sysread') THEN
				_newmode = 'write';
				RAISE WARNING 'fix wf attr mode for attr = %.% change mode from % to %', _class, _attribute, _mode, _newmode;
			END IF;			

			IF _newmode <> _mode THEN
				PERFORM _cm3_attribute_comment_set(_class,_attribute,'MODE',_newmode);
			END IF;

		END LOOP;
	END LOOP;

	FOR _class IN SELECT _cm3_class_list_descendants_and_self('"Activity"'::regclass) LOOP
		FOR _attribute IN SELECT * FROM (SELECT _cm3_attribute_list(_class) x) a WHERE a.x IN ('UniqueProcessDefinition','PrevExecutors','ActivityInstanceId','NextExecutor','ProcessCode','ActivityDefinitionId','FlowData','FlowStatus') LOOP
-- 			IF _cm3_attribute_comment_get(_class,_attribute,'MODE') <> 'reserved' THEN 
-- 				RAISE NOTICE 'fix wf attribute permission for %.%, set MODE = ''reserved''', _class, _attribute;
			PERFORM _cm3_attribute_comment_set(_class,_attribute,'MODE','rescore');
-- 			END IF;
		END LOOP;
	END LOOP;

END $$ LANGUAGE PLPGSQL;
