-- menu table refactoring

ALTER TABLE "_Menu" DISABLE TRIGGER USER;

CREATE FUNCTION _cm_patch_aux_fix_menu_item(_menu jsonb) RETURNS jsonb AS $$ BEGIN

	_menu = _menu - 'index';

	IF _menu->>'type' LIKE 'report%' THEN
		_menu = jsonb_set(_menu, ARRAY['target'], to_jsonb((SELECT "Code" FROM "_Report" WHERE "Id" = (_menu->>'objectId')::bigint AND "Status" = 'A')));
	ELSEIF _menu->>'type' IN ('dashboard', 'view') THEN
		_menu = jsonb_set(_menu, ARRAY['target'], _menu->'objectId');
	ELSEIF _menu->>'type' IN ('processclass', 'class', 'custompage') THEN
		_menu = jsonb_set(_menu, ARRAY['target'], _menu->'objectType');
	END IF;

	_menu = _menu - 'objectId' - 'objectType';

	RETURN _cm_patch_aux_fix_menu_children(_menu);
END $$ LANGUAGE PLPGSQL;

CREATE FUNCTION _cm_patch_aux_fix_menu_children(_menu jsonb) RETURNS jsonb AS $$ DECLARE
	_item jsonb;
	_items jsonb[];
BEGIN
	_items = ARRAY[]::jsonb[];
	FOR _item IN SELECT * FROM jsonb_array_elements(_menu->'children') LOOP
		_item = _cm_patch_aux_fix_menu_item(_item);
		_items = array_append(_items, _item);
	END LOOP;
	RETURN jsonb_set(_menu, ARRAY['children'], to_jsonb(_items));
END $$ LANGUAGE PLPGSQL;

DO $$ DECLARE
	_record RECORD;
	_menu jsonb;
	_item jsonb;
BEGIN
	
	FOR _record IN SELECT * FROM "_Menu" LOOP
		_menu = jsonb_set(_record."Data", ARRAY['version'], to_jsonb(2));
		_menu = _cm_patch_aux_fix_menu_children(_menu);
		EXECUTE format('UPDATE "_Menu" SET "Data" = %L WHERE "Id" = %L', _menu, _record."Id");
	END LOOP;

END $$ LANGUAGE PLPGSQL;

DROP FUNCTION _cm_patch_aux_fix_menu_item(jsonb);
DROP FUNCTION _cm_patch_aux_fix_menu_children(jsonb);

ALTER TABLE "_Menu" ENABLE TRIGGER USER;

