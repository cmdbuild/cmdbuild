-- bim tables refactoring

CREATE TABLE _patch_aux_bimlayer (id bigint, usr varchar, begindate timestamp, classname varchar, root boolean , active boolean, rootreference varchar);
INSERT INTO _patch_aux_bimlayer (id,usr,begindate,classname,root,active,rootreference) SELECT "Id","User","BeginDate","ClassName","Root","Active","RootReference" FROM "_BimLayer";
DROP TABLE "_BimLayer";

SELECT _cm3_class_create('_BimLayer', '"Class"'::regclass, 'MODE: reserved|TYPE: class|DESCR: Bim Layers|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_BimLayer"'::regclass, 'OwnerClassId', 'regclass', 'NOTNULL: true|UNIQUE: true|MODE: read');
SELECT _cm3_attribute_create('"_BimLayer"'::regclass, 'Root', 'boolean', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_BimLayer"'::regclass, 'Active', 'boolean', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_BimLayer"'::regclass, 'RootReference', 'varchar','MODE: read|DESCR: attribute of owner class that references bim root class');

ALTER TABLE "_BimLayer" DISABLE TRIGGER USER;
INSERT INTO "_BimLayer" ("Id","CurrentId","IdClass","User","BeginDate","OwnerClassId","Root","Active","RootReference","Status") 
	SELECT id,id,'"_BimLayer"'::regclass,usr,begindate,_cm3_utils_name_to_regclass(classname),root,active,rootreference,'A' FROM _patch_aux_bimlayer;
ALTER TABLE "_BimLayer" ENABLE TRIGGER USER;
DROP TABLE _patch_aux_bimlayer;


SELECT _cm3_class_create('_BimObject', '"Class"'::regclass, 'MODE: reserved|TYPE: class|DESCR: Bim Objects|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_BimObject"'::regclass, 'OwnerClassId', 'regclass', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_BimObject"'::regclass, 'OwnerCardId', 'bigint', 'NOTNULL: true|MODE: read');
-- SELECT _cm3_attribute_create('"_BimObject"'::regclass, 'ProjectId', 'varchar', 'NOTNULL: TRUE|MODE: read');
SELECT _cm3_attribute_create('"_BimObject"'::regclass, 'ProjectId', 'varchar','MODE: read');--TODO fix import query below, mark projectid as not null
SELECT _cm3_attribute_create('"_BimObject"'::regclass, 'GlobalId', 'varchar','MODE: read');

DO $$
DECLARE 
	_owner_class regclass;
	_is_root boolean;
	_is_active boolean;
	_root_reference varchar;
	_domain regclass;
	_bim_class regclass;
BEGIN

	-- load root records
	FOR _owner_class, _is_active IN 
		SELECT "OwnerClassId", "Active" FROM "_BimLayer" WHERE "Root" = TRUE
	LOOP
		_domain = _cm3_utils_name_to_regclass(format('Map_%s_BimProject',_cm3_utils_regclass_to_name(_owner_class)));
		EXECUTE format('INSERT INTO "_BimObject" ("OwnerClassId","OwnerCardId","ProjectId","GlobalId") SELECT %L, m."IdObj1", p."ProjectId", NULL FROM "_BimProject" p JOIN %s m ON p."Id" = m."IdObj2" WHERE m."Status" = ''A''', _owner_class, _domain);
		EXECUTE format('DROP TABLE "%s_history"',_cm3_utils_regclass_to_name(_domain));
		EXECUTE format('DROP TABLE %s',_domain);
	END LOOP;

	-- load non-root records; load card mapping from bim schema
	FOR _owner_class IN 
		SELECT "OwnerClassId" FROM "_BimLayer" WHERE "Active" = TRUE
	LOOP
		
		_bim_class = format('bim.%s',_owner_class)::regclass;
		EXECUTE format('INSERT INTO "_BimObject" ("OwnerClassId","OwnerCardId","ProjectId","GlobalId") SELECT %L,"Master",NULL,"GlobalId" FROM %s',_owner_class,_bim_class);--TODO join with reference attr, retrieve project id
		EXECUTE format('DROP TABLE %s',_bim_class);
	
	END LOOP;

	DROP SCHEMA IF EXISTS bim;

END $$ LANGUAGE PLPGSQL;


CREATE TABLE _patch_aux_bimproject (id bigint, usr varchar, begindate timestamp,projectid varchar,active boolean,lastcheckin timestamp,importmapping text,code varchar, descr varchar);
INSERT INTO _patch_aux_bimproject(id,usr,begindate,projectid,active,lastcheckin,importmapping,code,descr) SELECT "Id","User","BeginDate","ProjectId","Active","LastCheckin","ImportMapping","Code","Description" FROM "_BimProject";
DROP TABLE "_BimProject";

SELECT _cm3_class_create('_BimProject', '"Class"'::regclass, 'MODE: reserved|TYPE: class|DESCR: Bim Projects|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_BimProject"'::regclass, 'ProjectId', 'varchar', 'NOTNULL: true|UNIQUE: true|MODE: read|DESCR: Project ID');
SELECT _cm3_attribute_create('"_BimProject"'::regclass, 'Active', 'boolean', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_BimProject"'::regclass, 'LastCheckin', 'timestamp','MODE: read|DESCR: Last Checkin');
SELECT _cm3_attribute_create('"_BimProject"'::regclass, 'ImportMapping', 'text','MODE: read');

ALTER TABLE "_BimProject" DISABLE TRIGGER USER;
INSERT INTO "_BimProject" ("Id","CurrentId","IdClass","User","BeginDate","ProjectId","Active","LastCheckin","ImportMapping","Status","Code","Description") 
	SELECT id,id,'"_BimLayer"'::regclass,usr,begindate,projectid,active,lastcheckin,importmapping,'A',code,descr FROM _patch_aux_bimproject;
ALTER TABLE "_BimProject" ENABLE TRIGGER USER;
DROP TABLE _patch_aux_bimproject;

