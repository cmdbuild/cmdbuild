-- fix _Grant table columns IdRole, ObjectId (change to bigint)

DO $$ DECLARE
	_query varchar;
BEGIN

	SELECT _cm3_utils_prepare_dependant_views_create_query('"_Grant"'::regclass) INTO _query;
	PERFORM _cm3_utils_drop_dependant_views('"_Grant"'::regclass);

	ALTER TABLE "_Grant" ALTER COLUMN "IdRole" TYPE bigint;
	ALTER TABLE "_Grant" ALTER COLUMN "ObjectId" TYPE bigint;

	EXECUTE _query;

END $$ LANGUAGE plpgsql;

