-- entity metadata upgrade


SELECT _cm3_class_create('_ClassMetadata', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Class metadata|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_ClassMetadata"', 'Owner', 'regclass', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_ClassMetadata"', 'Metadata', 'jsonb', 'DEFAULT: ''{}''::jsonb|NOTNULL: true|MODE: write|STATUS: active');

CREATE UNIQUE INDEX "_Unique__ClassMetadata_Owner" ON "_ClassMetadata" ("Owner") WHERE "Status" = 'A';

SELECT _cm3_class_create('_AttributeMetadata', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Class Attribute metadata|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_AttributeMetadata"', 'Owner', 'regclass', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_AttributeMetadata"', 'Metadata', 'jsonb', 'DEFAULT: ''{}''::jsonb|NOTNULL: true|MODE: write|STATUS: active');

CREATE UNIQUE INDEX "_Unique__AttributeMetadata_OwnerCode" ON "_AttributeMetadata" ("Owner","Code") WHERE "Status" = 'A';

SELECT _cm3_class_create('_CardMetadata', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Card metadata|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_CardMetadata"', 'OwnerClass', 'regclass', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_CardMetadata"', 'OwnerCard', 'bigint', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_CardMetadata"', 'Data', 'jsonb', 'DEFAULT: ''{}''::jsonb|NOTNULL: true|MODE: read');

SELECT _cm3_class_create('_FunctionMetadata', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Function metadata|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_FunctionMetadata"', 'OwnerFunction', 'integer', 'NOTNULL: true|UNIQUE: true|MODE: read');
SELECT _cm3_attribute_create('"_FunctionMetadata"', 'Data', 'jsonb', 'DEFAULT: ''{}''::jsonb|NOTNULL: true|MODE: read');

CREATE UNIQUE INDEX "_Unique_CardMetadata_Owner" ON "_CardMetadata" ("OwnerCard") WHERE "Status" = 'A';

INSERT INTO 
	"Metadata" 
	("IdClass","Code","Description","Notes","Status") 
SELECT 
	'"Metadata"',
	regexp_replace("Element",'[^.]+[.]',''),
	'cm_class_icon',
	'images/'||regexp_replace("Path",'^[\\/]+',''),
	'A'
FROM 
	"_Icon";

DROP TABLE "_Icon";


DO $$ 
DECLARE
	_function int;
	_attr varchar;
	_value varchar;
	_metadata jsonb;
BEGIN
	FOR _function,_attr,_value IN
		SELECT (SELECT oid FROM pg_proc WHERE proname=regexp_replace("Code",'(.*)[.].*','\1')),regexp_replace("Code",'.*[.](.*)','\1'),"Notes" FROM "Metadata" WHERE "Status" = 'A' AND "Description" = 'system.function.hideColumn'
	LOOP
		_metadata = _cm3_function_metadata_get(_function);
		_metadata = jsonb_set(_metadata,('{attribute.'||_attr||'.basedsp}')::text[],to_json(not _value::boolean)::jsonb);
		PERFORM _cm3_function_metadata_set(_function,_metadata);
	END LOOP;	

	TRUNCATE TABLE "_FunctionMetadata_history";
	UPDATE "Metadata" SET "Status" = 'N' WHERE "Description" = 'system.function.hideColumn' AND "Status" = 'A';

END $$ LANGUAGE PLPGSQL;

DO $$
DECLARE
	attr_name varchar;
	owner regclass;
	met jsonb;
	rec RECORD;
	cur RECORD;
BEGIN
	FOR rec IN SELECT * FROM "Metadata" WHERE "Status" = 'A' AND "Code" IS NOT NULL AND "Code" LIKE '%.%'
	LOOP
		BEGIN

			owner = _cm3_utils_name_to_regclass(split_part(rec."Code",'.',1));

			IF owner IS NULL THEN

				RAISE NOTICE 'error processing Metadata record %: table not found for name "%"', rec."Id", rec."Code";

			ELSE

				attr_name = split_part(rec."Code",'.',2);

				SELECT * INTO cur FROM "_AttributeMetadata" WHERE "Owner" = owner AND "Code" = attr_name AND "Status" = 'A';

				IF cur IS NULL THEN
					met='{}'::jsonb;
				ELSE
					met = cur."Metadata";
				END IF;

				met = jsonb_set(met,('{'||rec."Description"||'}')::text[],to_json(rec."Notes")::jsonb);

				IF cur IS NULL THEN
					INSERT INTO "_AttributeMetadata" ("Owner","Code","Metadata","Status") VALUES (owner,attr_name,met,'A');
				ELSE
					UPDATE "_AttributeMetadata" SET "Metadata" = met WHERE "Id" = cur."Id";
				END IF;

			END IF;

			UPDATE "Metadata" SET "Status" = 'N' WHERE "Id" = rec."Id";
			
		EXCEPTION WHEN others THEN
			RAISE EXCEPTION 'error processing Metadata record %: %', rec."Id", SQLERRM;
		END;
	END LOOP;
END;
$$ LANGUAGE PLPGSQL;


CREATE TABLE _patch_card_metadata_aux AS 
	SELECT "Description" AS "Code","Notes" AS "Value",cm."IdClass1" ownerclass,cm."IdObj1" ownercard 
	FROM "Metadata" m 
	JOIN "Map_ClassMetadata" cm ON m."Id" = cm."IdObj2" 
	WHERE m."Status" = 'A' and cm."Status" = 'A';

DO $$ 
DECLARE
	_record RECORD;
	_metadata jsonb;
BEGIN

	FOR _record IN
		SELECT * FROM _patch_card_metadata_aux
	LOOP
		_metadata = _cm3_card_metadata_get(_record.ownerclass,_record.ownercard);
		_metadata = jsonb_set(_metadata,ARRAY[_record."Code"],to_jsonb(_record."Value"));
		PERFORM _cm3_card_metadata_set(_record.ownerclass,_record.ownercard,_metadata);
	END LOOP;	

	TRUNCATE TABLE "_CardMetadata_history";

END $$ LANGUAGE PLPGSQL;

DO $$
DECLARE
	owner regclass;
	met jsonb;
	rec RECORD;
	cur RECORD;
BEGIN
	FOR rec IN SELECT * FROM "Metadata" WHERE "Status" = 'A' AND "Code" IS NOT NULL AND "Code" NOT LIKE '%.%'
	LOOP
		BEGIN
			owner = _cm3_utils_name_to_regclass(rec."Code");

			IF owner IS NULL THEN

				RAISE NOTICE 'error processing Metadata record %: table not found for name "%"', rec."Id", rec."Code";

			ELSE

				SELECT * INTO cur FROM "_ClassMetadata" WHERE "Owner" = owner AND "Status" = 'A';

				IF cur IS NULL THEN
					met='{}'::jsonb;
				ELSE
					met = cur."Metadata";
				END IF;

				met = jsonb_set(met,ARRAY[rec."Description"],to_jsonb(rec."Notes"));

				IF cur IS NULL THEN
					INSERT INTO "_ClassMetadata" ("Owner","Metadata","Status") VALUES (owner,met,'A');
				ELSE
					UPDATE "_ClassMetadata" SET "Metadata" = met WHERE "Id" = cur."Id";
				END IF;

			END IF;

			UPDATE "Metadata" SET "Status" = 'N' WHERE "Id" = rec."Id";
			
		EXCEPTION WHEN others THEN
			RAISE EXCEPTION 'error processing Metadata record %: %', rec."Id", SQLERRM;
		END;
	END LOOP;
END $$ LANGUAGE PLPGSQL;

-- SELECT _cm3_create_class_attribute('Metadata', 'Value', 'varchar', NULL, FALSE, FALSE, 'MODE: write|STATUS: active');
-- 
-- ALTER TABLE "Metadata" DISABLE TRIGGER "_SanityCheck";
-- 
-- UPDATE "Metadata" SET
-- 	"Code" = "Description",
-- 	"Description" = '',
-- 	"Value" = "Notes",
-- 	"Notes" = NULL
-- 	WHERE "Status" = 'A' OR "Code" IS NULL;
-- 
-- ALTER TABLE "Metadata" ENABLE TRIGGER "_SanityCheck";

DROP TABLE "Map_ClassMetadata" CASCADE;
DROP TABLE "Metadata" CASCADE;
DROP TABLE _patch_card_metadata_aux;

