-- system functions for class edit


-- class modify --

-- CREATE OR REPLACE FUNCTION _cm_get_class_metadata(_class oid) RETURNS jsonb AS $$

CREATE OR REPLACE FUNCTION _cm3_class_delete(_class regclass) RETURNS void AS $$
BEGIN
	IF _cm_class_has_domains(_class) THEN --TODO migrate check
		RAISE EXCEPTION 'CM_HAS_DOMAINS';
	ELSEIF _cm_class_has_children(_class) THEN --TODO migrate check
		RAISE EXCEPTION 'CM_HAS_CHILDREN';
	ELSEIF NOT _cm_table_is_empty(_class) THEN --TODO migrate check
		RAISE EXCEPTION 'CM_CONTAINS_DATA';
	END IF;

	PERFORM _cm_delete_local_attributes(_class); --TODO migrate fun

	UPDATE "_AttributeMetadata" SET "Status" = 'N' WHERE "Owner" = _class AND "Status" = 'A';
	UPDATE "_ClassMetadata" SET "Status" = 'N' WHERE "Owner" = _class AND "Status" = 'A';

	-- Cascade for the history table
-- 	EXECUTE 'DROP TABLE '|| _class::regclass ||' CASCADE';
	EXECUTE format('DROP TABLE "%s_history"', _cm3_utils_regclass_to_name(_class));
	EXECUTE format('DROP TABLE %s', _class);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_utils_copy_superclass_attribute_comments(_class regclass,_parent_class regclass) RETURNS void AS $$
DECLARE
	_attr varchar;
BEGIN
	FOR _attr IN SELECT * FROM _cm3_attribute_list(_parent_class) LOOP
		EXECUTE format('COMMENT ON COLUMN %s.%I IS %L', _class, _attr,_cm_comment_for_attribute(_parent_class, _attr));
	END LOOP;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_create_history(_class regclass) RETURNS void AS $$
DECLARE
	_class_name varchar;
	_history_table_name varchar;
BEGIN
	_class_name = _cm3_utils_regclass_to_name(_class); --TODO handle schema
-- 	_history_table_name = _class_name || '_history'; --TODO handle schema
	EXECUTE format('CREATE TABLE "%s_history" (
				CONSTRAINT "%s_history_pkey" PRIMARY KEY ("Id"),
				CONSTRAINT "%s_history_CurrentId_fkey" FOREIGN KEY ("CurrentId") REFERENCES %s ("Id") ON UPDATE RESTRICT ON DELETE SET NULL,
				CONSTRAINT "_Check_Status" CHECK ("Status" = ''U'') ) INHERITS (%s)', 
			_class_name,_class_name,_class_name,_class,_class);
	PERFORM _cm_create_index(_cm_history_id(_class_name), 'CurrentId'); --TODO
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_create(_class_name varchar, _class_parent regclass, _comment varchar) RETURNS regclass AS $$ BEGIN
	RETURN _cm3_class_create(_class_name, _class_parent, _cm3_comment_to_jsonb(_comment));
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_create(_class_name varchar, _class_parent regclass, _comment jsonb) RETURNS regclass AS $$
DECLARE
	_class_type varchar;
	_class_is_simple boolean;
	_class_is_standard boolean;
	_class regclass;
	_query varchar;
	_table_name varchar;
BEGIN

	_class_type = _comment->>'TYPE';

	IF _class_type !~ '^(simpleclass|class)$' THEN RAISE EXCEPTION 'CM: invalid class type = "%"', _class_type; END IF;

	_class_is_simple = _class_type = 'simpleclass';
	_class_is_standard = _class_type = 'class';

	IF _class_is_simple THEN
		IF _class_parent IS NOT NULL THEN RAISE EXCEPTION 'CM: cannot create simple class with a parent'; END IF;
	END IF;

	IF _class_is_standard THEN 
		IF _class_parent IS NULL THEN RAISE EXCEPTION 'CM: cannot create standard class with null parent'; END IF;
		IF NOT _cm3_class_is_superclass(_class_parent) THEN RAISE EXCEPTION 'CM: cannot extend class = % (it is not a superclass)', _class_parent; END IF;
	END IF;

	PERFORM _cm_create_schema_if_needed(_class_name);

	_table_name = _cm_table_dbname_unsafe(_class_name);
	_query = 'CREATE TABLE ' || _table_name || ' ( ';
		
	IF _class_is_simple THEN
		_query = _query || '"Id" bigint NOT NULL DEFAULT _cm3_utils_new_card_id(),';
	END IF;

	_query = _query || ' CONSTRAINT ' || quote_ident(_cm_classpk_name(_class_name)) ||' PRIMARY KEY ("Id")' || ')';
	
	IF _class_is_standard THEN 
		_query = _query || ' INHERITS ('|| _class_parent::varchar ||')';
	END IF;

	EXECUTE _query;
	EXECUTE format('COMMENT ON TABLE %s IS %L', _table_name, _cm3_comment_from_jsonb(_comment));
	EXECUTE format('COMMENT ON COLUMN %s."Id" IS ''MODE: reserved''', _table_name);

	_class = _cm3_utils_name_to_regclass(_class_name);

	PERFORM _cm3_class_utils_copy_superclass_attribute_comments(_class, _class_parent);

	PERFORM _cm_create_class_triggers(_class);

	IF _class_is_simple THEN
-- 		PERFORM _cm3_attribute_create(_class, 'IdClass', 'regclass', NULL, TRUE, FALSE, 'MODE: reserved');
-- 		PERFORM _cm3_attribute_create(_class, 'User', 'varchar(100)', NULL, FALSE, FALSE, 'MODE: reserved');
-- 		PERFORM _cm3_attribute_create(_class, 'BeginDate', 'timestamp', 'now()', TRUE, FALSE, 'MODE: write|FIELDMODE: read|BASEDSP: true');
		PERFORM _cm3_attribute_create(_class, 'IdClass', 'regclass', 'NOTNULL: true|MODE: reserved');
		PERFORM _cm3_attribute_create(_class, 'User', 'varchar(100)', 'MODE: reserved');
		PERFORM _cm3_attribute_create(_class, 'BeginDate', 'timestamp', 'DEFAULT: now()|NOTNULL: true|MODE: read|BASEDSP: true');
		PERFORM _cm_create_index(_class, 'BeginDate');
	END IF;

	IF _class_is_standard THEN 
	    PERFORM _cm_propagate_superclass_triggers(_class);
		PERFORM _cm_create_class_indexes(_class);
		IF NOT _cm3_class_is_superclass(_class) THEN
			PERFORM _cm3_class_create_history(_class);
		END IF;
	END IF;

	PERFORM _cm3_multitenant_mode_change(_class, NULL, _comment->>'MTMODE');

	RETURN _class;

END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_class_modify(_class regclass, _comment jsonb) RETURNS void AS $$
DECLARE
	_current_comment jsonb;
BEGIN
	_current_comment = _cm3_class_comment_get_jsonb(_class);
	IF _current_comment->>'SUPERCLASS' <> _comment->>'SUPERCLASS' --TODO run check in foreach, throw informative exception message
		OR _current_comment->>'TYPE' <> _comment->>'TYPE'
	THEN
		RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION';
	END IF;

	PERFORM _cm3_class_comment_set(_class, _comment);
	PERFORM _cm3_multitenant_mode_change(_class, _current_comment->>'MTMODE', _comment->>'MTMODE');
END $$ LANGUAGE PLPGSQL;

--- CLASS ATTRIBUTE MODIFY ---

CREATE OR REPLACE FUNCTION _cm3_attribute_unique_set(_class regclass, _attr varchar, _unique boolean) RETURNS VOID AS $$
DECLARE 
	c1 int;
	c2 int;
BEGIN
	IF _cm3_attribute_unique_get(_class, _attr) <> _unique THEN
		IF _unique THEN
			IF _cm3_class_is_superclass(_class) THEN
-- 			IF (_cm3_class_is_simple(_class) OR _cm3_class_is_superclass(_class)) THEN
-- 				RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION: error setting unique flag on attribute %.% User defined superclass or simple class attributes cannot be unique', _class, _attr;
				RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION: error setting unique flag on attribute %.%; superclass attributes cannot be unique', _class, _attr;
			END IF;
			
			EXECUTE format('SELECT COUNT(DISTINCT %I) FROM %s WHERE "Status" = ''A''', _attr, _class) INTO c1;
			EXECUTE format('SELECT COUNT(%I) FROM %s WHERE "Status" = ''A''', _attr, _class) INTO c2;

			IF c1 <> c2 THEN
				RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION: error setting unique flag on attribute %.%: there are active cards with non unique values for this attribute', _class, _attr;
			END IF;

			IF _cm3_class_is_simple(_class) THEN
				EXECUTE format('ALTER TABLE %s ADD UNIQUE (%I)', _class, _attr);
			ELSE
				EXECUTE format('CREATE UNIQUE INDEX ''_Unique_%s_%s'' ON %s (%I) WHERE "Status" = ''A''', _cm3_utils_regclass_to_name(_class), _class, _attr);
			END IF;

		ELSE
			
			IF _cm3_class_is_simple(_class) THEN
				EXECUTE format('ALTER TABLE %s DROP UNIQUE (%I)', _class, _attr);
			ELSE
				EXECUTE format('DROP INDEX ''_Unique_%s_%s''', _cm3_utils_regclass_to_name(_class), _attr);
			END IF;

		END IF;
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_notnull_set(_class regclass, _attr varchar, _notnull boolean) RETURNS VOID AS $$
DECLARE
	_hasnull boolean;
BEGIN
	IF _notnull <> _cm3_attribute_notnull_get(_class, _attr) THEN
		IF _notnull THEN
			IF _cm3_class_is_simple(_class) THEN
				EXECUTE format('SELECT EXISTS(SELECT * FROM %s WHERE %I IS NULL)', _class, _attr) INTO _hasnull;
			ELSE
				EXECUTE format('SELECT EXISTS(SELECT * FROM %s WHERE %I IS NULL AND "Status" = ''A'')', _class, _attr) INTO _hasnull;
			END IF;

			IF _hasnull THEN
				RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION: error setting notnull flag on attribute %.%: there are active cards with null values for this attribute', _class, _attr;
			END IF;

			IF _cm3_class_is_simple(_class) THEN
				EXECUTE format('ALTER TABLE %s ALTER COLUMN %I SET NOT NULL', _class, _attr);
			ELSE
				EXECUTE format('ALTER TABLE %s ADD CONSTRAINT "_NotNull_%s" CHECK ("Status"<>''A'' OR %I IS NOT NULL)', _class, _attr, _attr);
			END IF;
		ELSE
			IF _cm3_class_is_simple(_class) THEN
				EXECUTE format('ALTER TABLE %s ALTER COLUMN %I DROP NOT NULL', _class, _attr);
			ELSE
				EXECUTE format('ALTER TABLE %s DROP CONSTRAINT "_NotNull_%s"', _class, _attr);
			END IF;
		END IF;
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_default_set( _class regclass,	_attr varchar, _value varchar, _update_records boolean) RETURNS void AS $$ DECLARE
	_current_value varchar;
	_attr_sqltype varchar;
BEGIN

	_value = NULLIF(TRIM(_value), '');
	_attr_sqltype = LOWER(_cm_get_attribute_sqltype(_class, _attr));

	IF _value IS NOT NULL AND ( _attr_sqltype LIKE 'varchar%' OR _attr_sqltype = 'text' OR ( ( _attr_sqltype = 'date' OR _attr_sqltype = 'timestamp' ) AND TRIM(_value) <> 'now()' ) ) THEN
		_value = quote_literal(_value);
	END IF;

	_current_value = _cm3_attribute_default_get(_class, _attr);

    IF _value IS DISTINCT FROM _current_value THEN
    	IF _value IS NULL THEN
			EXECUTE format('ALTER TABLE %s ALTER COLUMN	%I DROP DEFAULT', _class, _attr);
	    ELSE
	        EXECUTE format('ALTER TABLE %s ALTER COLUMN %I SET DEFAULT %s', _class, _attr, _value);
			IF _update_records THEN
	        	EXECUTE format('UPDATE %s SET %I = %s', _class, _attr, _value);
	        END IF;
		END IF;
    END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_validate_comment_and_type(_comment jsonb, _type text) RETURNS VOID AS $$ DECLARE
	_count integer = 0; 
BEGIN
	IF _cm_read_reference_domain_comment(_cm3_comment_from_jsonb(_comment)) IS NOT NULL THEN --TODO migrate fun
		_count = _count +1;
	END IF;

	IF _cm_get_fk_target_comment(_cm3_comment_from_jsonb(_comment)) IS NOT NULL THEN --TODO migrate fun
		_count = _count +1;
	END IF;

	IF _cm_get_lookup_type_comment(_cm3_comment_from_jsonb(_comment)) IS NOT NULL THEN --TODO migrate fun
		_count = _count +1;
	END IF;

	IF (_count > 1) THEN
		RAISE EXCEPTION 'CM: attribute type error: inconsistent comment format (too many attribute type specified)';
	END IF;

	IF _count = 1 AND _type NOT IN ('int8','bigint') THEN
		RAISE EXCEPTION 'CM: attribute type error: this attribute should have sql type int8/bigint, has type % instead', _type;
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_index_create(_class regclass, _attr varchar) RETURNS void AS $$ BEGIN
	EXECUTE format ('CREATE INDEX "idx_%s_%s" ON %s USING btree(%I)',_cm3_utils_regclass_to_name(_class),_attr,_class,_attr);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_create(_class regclass,_attr varchar,_type varchar,_comment varchar) RETURNS void AS $$ BEGIN
	PERFORM _cm3_attribute_create(_class, _attr, _type, _cm3_comment_to_jsonb(_comment));
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_create(_class regclass,_attr varchar,_type varchar,_comment jsonb) RETURNS void AS $$ 
DECLARE
	_not_null boolean;
	_unique boolean;
	_default varchar;
	_fk_target regclass;
	_sub_class regclass;
	_domain regclass;
	_domain_direct boolean;
	_domain_source_attr varchar;
	_domain_target_attr varchar;
BEGIN

-- 	IF _cm3_class_is_simple(_class) AND _attr IN (SELECT _cm3_attribute_list('"Class"')) THEN
-- 		RAISE 'CM: cannot create attribute with name = %s: reserved name', _attr;
-- 	END IF;

	_not_null = coalesce(_comment->>'NOTNULL','false')::boolean;
	_unique = coalesce(_comment->>'UNIQUE','false')::boolean;
	_default = _comment->>'DEFAULT';
	
	_comment = _comment - 'NOTNULL' - 'UNIQUE' - 'DEFAULT';

	PERFORM _cm3_attribute_validate_comment_and_type(_comment, _type);

-- 	IF _cm_is_geometry_type(_type) THEN  -- TODO
-- 		PERFORM _cm_add_spherical_mercator();
-- 		PERFORM AddGeometryColumn(_cm_cmschema(_class), _cm_cmtable(_class), _attr, 900913, _type, 2);
-- 	ELSE
		EXECUTE format('ALTER TABLE %s ADD COLUMN %I %s', _class, _attr, _type);
-- 	END IF;

    PERFORM _cm3_attribute_default_set(_class, _attr, _default, TRUE);

	-- set the comment recursively (needs to be performed before unique and notnull, because they depend on the comment)
    PERFORM _cm_set_attribute_comment(_class, _attr, _cm3_comment_from_jsonb(_comment)); --TODO migrate fun

	PERFORM _cm_attribute_set_notnull(_class, _attr, _not_null); --TODO migrate fun
	PERFORM _cm_attribute_set_uniqueness(_class, _attr, _unique); --TODO migrate fun

	
	IF _cm3_attribute_is_foreignkey(_class, _attr) THEN
		_fk_target = _cm3_attribute_foreignkey_target_get(_class, _attr);
		FOR _sub_class IN SELECT _cm3_class_list_descendant_classes_and_self(_class) LOOP
			EXECUTE format('CREATE TRIGGER %I BEFORE INSERT OR UPDATE ON %s FOR EACH ROW EXECUTE PROCEDURE _cm_trigger_fk(%L,%L,%L)', _cm3_trigger_utils_fkey(_class, _attr), _sub_class, _attr, _fk_target, (CASE _cm3_class_is_simple(_sub_class) WHEN true THEN 'simple' ELSE '' END));
		END LOOP;
		FOR _sub_class IN SELECT _cm3_class_list_descendant_classes_and_self(_fk_target) LOOP
			EXECUTE format('CREATE TRIGGER %I BEFORE UPDATE OR DELETE ON %s FOR EACH ROW EXECUTE PROCEDURE _cm_trigger_restrict(%L,%L)', _cm3_trigger_utils_constr(_class, _attr), _sub_class, _class, _attr);
		END LOOP;
	END IF;

	IF _cm3_attribute_is_reference(_class, _attr) THEN
		_domain = _cm3_attribute_reference_domain_get(_class, _attr);
		_domain_direct = _cm3_attribute_reference_domain_direct_is(_class, _attr);
		--TODO update existing records, set reference value from domain
		-- 	FOR objid IN EXECUTE 'SELECT "Id" from '||TableId::regclass||' WHERE "Status"=''A'''
		-- 	LOOP
		-- 		FOR referencedid IN EXECUTE '
		-- 			SELECT '|| quote_ident(RefTargetIdAttribute) ||
		-- 			' FROM '|| ReferenceDomainId::regclass ||
		-- 			' WHERE '|| quote_ident(RefSourceClassIdAttribute) ||'='|| TableId ||
		-- 				' AND '|| quote_ident(RefSourceIdAttribute) ||'='|| objid ||
		-- 				' AND "Status"=''A'''
		-- 		LOOP
		-- 			EXECUTE 'SELECT count(*) FROM '||ReferenceTargetId::regclass||' where "Id"='||referencedid INTO ctrlint;
		-- 			IF(ctrlint<>0) THEN
		-- 				EXECUTE 'UPDATE '|| TableId::regclass ||
		-- 					' SET '|| quote_ident(AttributeName) ||'='|| referencedid ||
		-- 					' WHERE "Id"='|| objid;
		-- 			END IF;
		-- 		END LOOP;
		-- 	END LOOP;
		IF _domain_direct THEN
			_domain_source_attr = 'IdObj1';
			_domain_target_attr = 'IdObj2';
		ELSE
			_domain_source_attr = 'IdObj2';
			_domain_target_attr = 'IdObj1';
		END IF;
		
		FOR _sub_class IN SELECT _cm3_class_list_descendant_classes_and_self(_class) LOOP
			EXECUTE format('CREATE TRIGGER %I AFTER INSERT OR UPDATE ON %s FOR EACH ROW EXECUTE PROCEDURE _cm_trigger_update_relation(%L,%L,%L,%L)', _cm3_trigger_utils_updrel(_class, _attr), _class, _attr, _domain, _domain_source_attr, _domain_target_attr); --TODO check this
		END LOOP;
		EXECUTE format('CREATE TRIGGER %I AFTER INSERT OR UPDATE ON %s FOR EACH ROW EXECUTE PROCEDURE _cm_trigger_update_reference(%L,%L,%L,%L)',
			_cm3_trigger_utils_updref(_class, _attr), _domain, _attr, _class, _domain_source_attr, _domain_target_attr); --TODO check this			
	END IF;

END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_delete(_class regclass, _attr varchar) RETURNS VOID AS $$
DECLARE
-- 	GeoType text := _cm_get_geometry_type(_class, _attr); TODO
	_fk_target regclass;
	_sub_class regclass;
	_domain regclass;
BEGIN
	IF _cm3_attribute_is_inherited(_class, _attr) THEN
		RAISE EXCEPTION 'CM_FORBIDDEN_OPERATION: cannot remove attribute %.%: attribute is inherited', _class, _attr;
	END IF;

    IF _cm3_attribute_has_data(_class, _attr) THEN
		RAISE EXCEPTION 'CM_CONTAINS_DATA: cannot remove attribute %.%: attribute contains data', _class, _attr;
	END IF;

	IF _cm3_attribute_is_foreignkey(_class, _attr) THEN
		_fk_target = _cm3_attribute_foreignkey_target_get(_class, _attr);
		FOR _sub_class IN SELECT _cm3_class_list_descendant_classes_and_self(_class) LOOP
			EXECUTE format('DROP TRIGGER %I ON %s', _cm3_trigger_utils_fkey(_class, _attr), _sub_class);
		END LOOP;
		FOR _sub_class IN SELECT _cm3_class_list_descendant_classes_and_self(_fk_target) LOOP
			EXECUTE format('DROP TRIGGER %I ON %s', _cm3_trigger_utils_constr(_class, _attr), _sub_class);
		END LOOP;
	END IF;

	IF _cm3_attribute_is_reference(_class, _attr) THEN
		_domain = _cm3_attribute_reference_domain_get(_class, _attr);
		FOR _sub_class IN SELECT _cm3_class_list_descendant_classes_and_self(_class) LOOP
			EXECUTE format('DROP TRIGGER %I ON %s', _cm3_trigger_utils_updrel(_class, _attr), _sub_class);
		END LOOP;
		EXECUTE format('DROP TRIGGER %I ON %s', _cm3_trigger_utils_updref(_class, _attr), _domain);
	END IF;

-- 	IF GeoType IS NOT NULL THEN --TODO
-- 		PERFORM DropGeometryColumn(_cm_cmschema(_class), _cm_cmtable(_class), _attr);
-- 	ELSE
		EXECUTE format('ALTER TABLE %s DROP COLUMN %I CASCADE', _class, _attr);
-- 	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_delete_all(_class regclass) RETURNS void AS $$
DECLARE
	_attr varchar;
BEGIN
	FOR _attr IN SELECT a FROM _cm3_attribute_list(_class) a WHERE NOT _cm3_attribute_is_inherited(_class, a) LOOP
		PERFORM _cm3_attribute_delete(_class, _attr);
	END LOOP;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_attribute_copy_all_comments(_source regclass, _target regclass) RETURNS void AS $$
DECLARE
	_attr varchar;
BEGIN
	FOR _attr IN SELECT * FROM _cm3_attribute_list(_source) LOOP
		PERFORM _cm3_attribute_comment_set(_target, _attr, _cm3_attribute_comment_get(_source,_attr));
	END LOOP;
END $$ LANGUAGE PLPGSQL;


--- card check ---

CREATE OR REPLACE FUNCTION _cm3_card_exists_with_value(_class regclass,_attr varchar,_value bigint) RETURNS BOOLEAN AS $$ 
BEGIN
	RETURN _cm3_card_exists_with_value(_class,_attr,_value,FALSE);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_card_exists_with_value(_class regclass,_attr varchar,_value bigint,_include_deleted boolean) RETURNS BOOLEAN AS $$
DECLARE
	_query varchar;
	_res boolean;
BEGIN
	IF _value IS NULL THEN
		RETURN FALSE;
	ELSE
		_query = 'SELECT EXISTS (SELECT "Id" FROM ' || _class::varchar || ' WHERE ' || quote_ident(_attr) || ' = ' || _value;
		IF _cm3_class_is_standard(_class) AND NOT _include_deleted THEN
			_query = _query || ' AND "Status"=''A''';
		END IF;
		_query = _query || ')';
		EXECUTE _query INTO _res;
		RETURN _res; 
	END IF;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_card_exists_with_id(_class regclass,_card_id bigint,_include_deleted boolean) RETURNS BOOLEAN AS $$
	SELECT _cm3_card_exists_with_value(_class, 'Id', _card_id, _include_deleted);
$$ LANGUAGE SQL STABLE;


--- card modify ---

CREATE OR REPLACE FUNCTION _cm3_card_delete(_class regclass,_card_id bigint) RETURNS void AS $$ BEGIN
	IF _cm3_class_is_simple(_class) THEN
		RAISE DEBUG 'deleting card from simple class = % with id = %', _class, _card_id;
		EXECUTE format('DELETE FROM %s WHERE "Id" = %L', _class, _card_id);
	ELSE
		RAISE DEBUG 'deleting card from standard class = % with id = %', _class, _card_id;
		EXECUTE format('UPDATE %s SET "Status" = ''N'' WHERE "Id" = %L', _class, _card_id);
	END IF;
END $$ LANGUAGE PLPGSQL;


-- MISC --

CREATE OR REPLACE FUNCTION _cm_trigger_restrict() RETURNS trigger AS $$
DECLARE
	_class regclass;
	_attr varchar;
BEGIN
	_class = TG_ARGV[0]::regclass;
	_attr = TG_ARGV[1];
	RAISE DEBUG 'Trigger % on %', TG_NAME, TG_TABLE_NAME;
	IF (TG_OP='UPDATE' AND NEW."Status"='N') THEN
		IF _cm3_card_exists_with_value(_class,_attr,OLD."Id") THEN
			RAISE EXCEPTION 'CM: error, unable to delete card with id = %: this card is referenced from existing cards with attr = %.%',OLD."Id",_cm3_utils_regclass_to_name(_class),_attr;
		END IF;
	END IF;
	RETURN NEW;
END $$ LANGUAGE PLPGSQL;


