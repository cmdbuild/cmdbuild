-- comment refactoring

CREATE OR REPLACE FUNCTION _cm3_function_comment_get(_function oid, _key varchar) RETURNS varchar AS $$
	SELECT _cm3_function_comment_get_jsonb(_function)->>_key;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_class_comment_delete(_class regclass, _key varchar) RETURNS void AS $$
	SELECT _cm3_class_comment_set(_class,_cm3_comment_delete_part(_cm3_class_comment_get(_class),_key));
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_function_definition_get(_fun oid) RETURNS varchar AS $$
	SELECT format('%I(%s)', (SELECT proname FROM pg_proc WHERE oid = _fun), pg_get_function_identity_arguments(_fun));
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION _cm3_function_comment_set(_fun oid, _comment varchar) RETURNS void AS $$ BEGIN
	EXECUTE format('COMMENT ON FUNCTION %s IS %L', _cm3_function_definition_get(_fun), _comment);
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_function_comment_set(_fun oid, _comment jsonb) RETURNS void AS $$
	SELECT _cm3_function_comment_set(_fun, _cm3_comment_from_jsonb(_comment));
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_function_comment_set(_fun oid, _key varchar, _value varchar) RETURNS void AS $$
	SELECT _cm3_function_comment_set(_fun, _cm3_comment_set_part(_cm3_function_comment_get(_fun),_key,_value));
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION _cm3_function_comment_delete(_fun oid, _key varchar) RETURNS void AS $$
	SELECT _cm3_function_comment_set(_fun, _cm3_comment_delete_part(_cm3_function_comment_get(_fun),_key));
$$ LANGUAGE SQL STABLE;

DO $$ DECLARE
	_class regclass;
	_fun oid;
	_attr varchar;
	_value varchar;
BEGIN
	
	FOR _class IN SELECT * FROM _cm3_class_list() UNION SELECT * FROM _cm3_domain_list() LOOP
		_value = _cm3_class_comment_get(_class,'STATUS');
		IF _value = 'noactive' THEN
			PERFORM _cm3_class_comment_set(_class, 'ACTIVE', 'true');
		ELSEIF _value <> 'active' AND _value ~ '.+' THEN
			RAISE 'unsupported STATUS value found for class = % value = %', _class, _value;
		END IF;
		PERFORM _cm3_class_comment_delete(_class, 'STATUS');

		FOR _attr IN SELECT * FROM _cm3_attribute_list(_class) LOOP
			_value = _cm3_attribute_comment_get(_class, _attr, 'STATUS');
			IF _value = 'noactive' THEN
				PERFORM _cm3_attribute_comment_set(_class, _attr, 'ACTIVE', 'true');
			ELSEIF _value <> 'active' AND _value ~ '.+' THEN
				RAISE 'unsupported STATUS value found for attr = %.% value = %', _class, _attr, _value;
			END IF;
			PERFORM _cm3_attribute_comment_delete(_class, _attr, 'STATUS');
		END LOOP;
	END LOOP;

	FOR _fun IN SELECT * FROM _cm3_function_list() LOOP
		_value = _cm3_function_comment_get(_fun,'STATUS');
		IF _value = 'noactive' THEN
			PERFORM _cm3_function_comment_set(_fun, 'ACTIVE', 'true');
		ELSEIF _value <> 'active' AND _value ~ '.+' THEN
			RAISE 'unsupported STATUS value found for function = % value = %', _cm3_function_definition_get(_fun), _value;
		END IF;
		PERFORM _cm3_function_comment_delete(_fun, 'STATUS');
	END LOOP;

END $$ LANGUAGE PLPGSQL;

