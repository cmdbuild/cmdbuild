-- migrate primary key format from int ot bigint
 

CREATE OR REPLACE FUNCTION _cm3_utils_drop_dependant_views(_table regclass) RETURNS VOID AS $$
DECLARE
	_view regclass;
BEGIN

	FOR _view IN
		select distinct(r.ev_class::regclass) as views
			from pg_depend d join pg_rewrite r on r.oid = d.objid 
			where refclassid = 'pg_class'::regclass
				and refobjid = _table
				and classid = 'pg_rewrite'::regclass 
				and ev_class != _table
	LOOP
		PERFORM _cm3_utils_drop_dependant_views(_view);
		RAISE NOTICE 'temporarily drop view %', _view;
		EXECUTE format('DROP VIEW %s', _view);
	END LOOP;
	
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_utils_prepare_dependant_views_create_query(_table regclass) RETURNS varchar AS $$
DECLARE
	_view regclass;
	_query varchar;
BEGIN

	_query := '';

	FOR _view IN
		select distinct(r.ev_class::regclass) as views
			from pg_depend d join pg_rewrite r on r.oid = d.objid 
			where refclassid = 'pg_class'::regclass
				and refobjid = _table
				and classid = 'pg_rewrite'::regclass 
				and ev_class != _table
	LOOP
		RAISE NOTICE 'processing dependant views for table = %, processing view = %', _table, _view;
		_query = _query || 'CREATE VIEW ' || _view::varchar || ' AS ' || pg_get_viewdef(_view, true) || ';';
		_query = _query || _cm3_utils_prepare_dependant_views_create_query(_view);
	END LOOP;

	RETURN _query;

END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_utils_drop_class_triggers(_table regclass) RETURNS VOID AS $$ DECLARE
	_trigger varchar;
BEGIN
	FOR _trigger IN SELECT t.tgname FROM pg_trigger t WHERE t.tgrelid = _table AND NOT tgisinternal LOOP
		EXECUTE format('DROP TRIGGER %I ON %s',_trigger,_table);
	END LOOP;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_utils_prepare_class_triggers_query(_table regclass) RETURNS varchar AS $$
	SELECT coalesce((SELECT string_agg(pg_get_triggerdef(t.oid),';') FROM pg_trigger t WHERE t.tgrelid = _table AND NOT tgisinternal),'');
$$ LANGUAGE SQL;

DO $$ 
DECLARE
	_query text;
	_class regclass;
BEGIN

	SELECT _cm3_utils_prepare_dependant_views_create_query('"Class"'::regclass) INTO _query;
	PERFORM _cm3_utils_drop_dependant_views('"Class"'::regclass);
 
	ALTER TABLE "Class" ALTER COLUMN "Id" TYPE bigint; 

	FOR _class IN SELECT x FROM _cm3_class_list_standard() x WHERE NOT _cm3_class_is_superclass(x) LOOP 
		EXECUTE format('ALTER TABLE "%s_history" ALTER COLUMN "CurrentId" TYPE bigint', _cm3_utils_regclass_to_name(_class));
	END LOOP;

	EXECUTE _query;

	FOR _class IN SELECT _cm3_class_list_simple() LOOP
		SELECT _cm3_utils_prepare_dependant_views_create_query(_class) INTO _query;
		PERFORM _cm3_utils_drop_dependant_views(_class);
		EXECUTE format('ALTER TABLE %s ALTER COLUMN "Id" SET DEFAULT 0', _class); --TODO modify only if column uses sequence
		EXECUTE format('ALTER TABLE %s ALTER COLUMN "Id" TYPE bigint', _class);
		EXECUTE format('ALTER TABLE %s ALTER COLUMN "Id" SET DEFAULT _cm3_utils_new_card_id()', _class); --TODO modify only if column uses sequence
		IF _query <> '' THEN EXECUTE _query; END IF;
	END LOOP;

END $$ LANGUAGE PLPGSQL;

DO $$
DECLARE
	_class regclass;
BEGIN
	FOR _class IN SELECT x FROM _cm3_class_list() x UNION SELECT x FROM _cm3_domain_list() x LOOP
-- 	FOR _class IN SELECT _cm_class_list() UNION SELECT_cm_domain_list() LOOP
-- 		EXECUTE 'ALTER TABLE ' || _table::regclass::varchar || ' ALTER COLUMN "Id" SET DEFAULT 0'; --TODO modify only if column uses sequence
		EXECUTE format('ALTER TABLE %s ALTER COLUMN "Id" SET DEFAULT _cm3_utils_new_card_id()', _class); --TODO modify only if column uses sequence
	END LOOP;
END $$ LANGUAGE PLPGSQL;

-- DROP FUNCTION _cm_new_card_id();


-- DO $$
-- DECLARE
-- 	_table oid;
-- BEGIN
-- 	FOR _table IN SELECT x FROM _cm_class_list() x UNION SELECT x FROM _cm_domain_list() x LOOP
-- 		EXECUTE 'ALTER TABLE ' || _table::regclass::varchar || ' ALTER COLUMN "Id" SET DEFAULT _cm3_utils_new_card_id()'; --TODO modify only if column uses sequence
-- 	END LOOP;
-- END $$ LANGUAGE PLPGSQL;

-- DROP FUNCTION IF EXISTS _cm3_class_list_descendants_and_self(oid);
-- DROP FUNCTION IF EXISTS _cm3_class_list_descendants_and_self_of(parentoid IN regclass, schemaname OUT name, tablename OUT name);




-- system table fix BEGIN

-- ALTER TABLE "LookUp" ALTER COLUMN "ParentId" TYPE bigint; TODO check lookup table
-- TODO other tables

-- system table fix END






	
-- DROP FUNCTION cm_get_tenants_for_user(integer);
-- CREATE OR REPLACE FUNCTION cm_get_tenants_for_user(user_id bigint) RETURNS SETOF bigint AS 
-- $BODY$
-- BEGIN
-- 	IF user_id = -1 THEN
-- 		RETURN QUERY SELECT "Tenant"."Id" FROM "Tenant" WHERE "Tenant"."Status" = 'A';
-- 	ELSE
-- 		RETURN QUERY SELECT "Tenant"."Id" FROM "Tenant" JOIN "Map_UserTenant" ON "Tenant"."Id" = "Map_UserTenant"."IdObj2" WHERE "Map_UserTenant"."IdObj1" = user_id  AND "Tenant"."Status" = 'A' AND "Map_UserTenant"."Status" = 'A';
-- 	END IF;
-- END
-- $BODY$
-- LANGUAGE 'plpgsql';



--- baseclass modifications ---

ALTER TABLE "Class" ADD COLUMN "EndDate" timestamp without time zone;
COMMENT ON COLUMN "Class"."EndDate" IS 'MODE: reserved';

ALTER TABLE "Class" ADD COLUMN "CurrentId" bigint;
COMMENT ON COLUMN "Class"."CurrentId" IS 'MODE: reserved';

ALTER TABLE "Map" ADD COLUMN "CurrentId" bigint;
COMMENT ON COLUMN "Map"."CurrentId" IS 'MODE: reserved';
 
DO $$
DECLARE
	_class regclass;
BEGIN
	FOR _class IN SELECT _cm3_class_list_standard() LOOP		
		EXECUTE format('ALTER TABLE %s DISABLE TRIGGER USER', _class);
	END LOOP;

	UPDATE "Class" SET "CurrentId" = "Id" WHERE "CurrentId" IS NULL;

	FOR _class IN SELECT _cm3_class_list_standard() LOOP		
		EXECUTE format('ALTER TABLE %s ENABLE TRIGGER USER', _class);
	END LOOP;

	FOR _class IN SELECT _cm3_class_list_standard() LOOP		
		EXECUTE format('COMMENT ON COLUMN %s."CurrentId" IS %L',_class,'MODE: reserved');
		EXECUTE format('COMMENT ON COLUMN %s."EndDate" IS %L',_class,'MODE: reserved');
	END LOOP;
	
END $$ LANGUAGE PLPGSQL;

DO $$
DECLARE
	_domain regclass;
BEGIN
	FOR _domain IN SELECT _cm3_domain_list() LOOP		
		EXECUTE format('ALTER TABLE %s DISABLE TRIGGER USER', _domain);
	END LOOP;

-- 	UPDATE "Map" SET "CurrentId" = "Id" WHERE "CurrentId" IS NULL;

	UPDATE "Map" m SET "CurrentId" = (
			SELECT cur."Id" FROM "Map" cur 
			WHERE m."IdDomain"=cur."IdDomain" AND m."IdClass1"=cur."IdClass1" AND m."IdObj1"=cur."IdObj1" AND m."IdClass2"=cur."IdClass2" AND m."IdObj2"=cur."IdObj2"
			ORDER BY cur."BeginDate" DESC LIMIT 1) 
		WHERE "CurrentId" IS NULL;

	FOR _domain IN SELECT _cm3_domain_list() LOOP		
		EXECUTE format('ALTER TABLE %s ENABLE TRIGGER USER', _domain);
	END LOOP;

	FOR _domain IN SELECT _cm3_domain_list() LOOP		
		EXECUTE format('COMMENT ON COLUMN %s."CurrentId" IS %L',_domain,'MODE: reserved');
	END LOOP;

END $$ LANGUAGE PLPGSQL; 

ALTER TABLE "Class" ALTER COLUMN "CurrentId" SET NOT NULL;
ALTER TABLE "Map" ALTER COLUMN "CurrentId" SET NOT NULL;

DO $$ DECLARE
	_class regclass;
	_attr varchar;
	_query text;
BEGIN

	FOR _class, _attr IN SELECT a.classe, a.attr FROM (SELECT c.x classe, _cm3_attribute_list(c.x) attr FROM (SELECT x FROM _cm3_class_list() x UNION SELECT x FROM _cm3_domain_list() x) c) a
		WHERE ( _cm3_attribute_is_reference(a.classe, a.attr) OR _cm3_attribute_is_lookup(a.classe, a.attr) OR _cm3_attribute_is_foreignkey(a.classe, a.attr) ) AND NOT _cm3_attribute_is_inherited(a.classe, a.attr)
	LOOP
		RAISE NOTICE 'change type of attr %.% to "bigint"', _cm3_utils_regclass_to_name(_class), _attr;

		_query = _cm3_utils_prepare_class_triggers_query(_class);
		PERFORM _cm3_utils_drop_class_triggers(_class);

		EXECUTE format('ALTER TABLE %s ALTER COLUMN %I TYPE bigint', _class, _attr);

		EXECUTE _query;

	END LOOP;

END $$ LANGUAGE PLPGSQL;

DO $$ DECLARE
	_query text;
	class_table varchar;
	_class regclass;
BEGIN

	SELECT _cm3_utils_prepare_dependant_views_create_query('"Map"'::regclass) INTO _query;
	PERFORM _cm3_utils_drop_dependant_views('"Map"'::regclass);

	ALTER TABLE "Map" ALTER COLUMN "Id" TYPE bigint;
	ALTER TABLE "Map" ALTER COLUMN "CurrentId" TYPE bigint;
	ALTER TABLE "Map" ALTER COLUMN "IdObj1" TYPE bigint;
	ALTER TABLE "Map" ALTER COLUMN "IdObj2" TYPE bigint;

	EXECUTE _query;

END $$ LANGUAGE PLPGSQL;


--- TRIGGER FIX ---

CREATE OR REPLACE FUNCTION _cm_trigger_create_relation_history_row() RETURNS trigger AS $$ 
DECLARE
	_attr_list_str varchar;	
BEGIN
	IF (TG_OP='UPDATE') THEN

		OLD."Status" = 'U';
		OLD."EndDate" = now();
		OLD."Id" = _cm3_utils_new_card_id();

		SELECT INTO _attr_list_str string_agg(x.x,',') from (select quote_ident(x) x from _cm3_attribute_list(TG_RELID::regclass) x) x;

		EXECUTE format('INSERT INTO "%s_history" (%s) VALUES ( (%L::%s).* )', _cm3_utils_regclass_to_name(TG_RELID::regclass), _attr_list_str, OLD, TG_RELID::regclass);

	ELSEIF (TG_OP='DELETE') THEN
		RAISE EXCEPTION 'cm constraint violated: DELETE is not allowed for this table, use SET "Status"=''N'' instead';
	END IF;
	RETURN NEW;
END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm_trigger_sanity_check() RETURNS trigger AS $$ BEGIN
	IF (TG_OP='UPDATE') THEN
		IF (NEW."Id" <> OLD."Id") THEN -- Id change
			RAISE EXCEPTION 'cm constraint violated: id update is not allowed';
		END IF;
		IF (NEW."Status"='N' AND OLD."Status"='N') THEN -- Deletion of a deleted card
			RAISE EXCEPTION 'cm constraint violated: cannot delete this card, it has already "Status" = "N"';
		END IF;
	ELSEIF (TG_OP='INSERT') THEN
		IF (NEW."Status" IS NULL) THEN
			NEW."Status"='A';
		ELSIF (NEW."Status"='N') THEN -- Creation of a deleted card
			RAISE EXCEPTION 'cm constraint violated: cannot INSERT a card with status "N"';
		END IF;
		NEW."Id" = _cm3_utils_new_card_id();
		-- Class ID is needed because of the history tables
		BEGIN
			NEW."IdClass" = TG_RELID;
		EXCEPTION WHEN undefined_column THEN
			NEW."IdDomain" = TG_RELID;
		END;
	ELSE -- TG_OP='DELETE'
		RAISE EXCEPTION 'cm constraint violated: DELETE is not allowed for this table, you should UPDATE "Status" flag instead';
	END IF;

	-- 'U' is reserved for history tables only
	IF (position(NEW."Status" IN 'AND') = 0) THEN -- Invalid status
		RAISE EXCEPTION 'cm constraint violated: invalid card status = %', NEW."Status";
	END IF;
		
	NEW."CurrentId" = NEW."Id";
	NEW."BeginDate" = now();
	NEW."User" := _cm3_utils_operation_user_get();	

	RETURN NEW;
END $$ LANGUAGE PLPGSQL;







ALTER SEQUENCE class_seq RESTART WITH 100000000000; -- this is useful to test bigint ids right away; TODO: remove for production rollout