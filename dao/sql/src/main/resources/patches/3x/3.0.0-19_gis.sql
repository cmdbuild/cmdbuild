-- upgrade gis tables



ALTER TABLE "_Layer" ALTER COLUMN "MapStyle" TYPE jsonb USING "MapStyle"::jsonb;

ALTER TABLE "_Layer" ALTER COLUMN "Visibility" TYPE varchar[] USING string_to_array("Visibility",',');
ALTER TABLE "_Layer" ALTER COLUMN "CardsBinding" TYPE varchar[] USING string_to_array("CardsBinding",',');

SELECT _cm3_attribute_create('"_Layer"', 'Owner', 'varchar', 'MODE: read');

UPDATE "_Layer" SET "Owner" = regexp_replace("FullName",'gis.Detail_(.+)_.+','\1') WHERE NOT "FullName" ILIKE '%Detail__GeoServer%';  --TODO check this

SELECT _cm3_class_create('_GisAttribute', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Gis Class Attributes|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'Type', 'varchar', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'Owner', 'regclass', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'Index', 'integer', 'DEFAULT: 0|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'MinimumZoom', 'integer', 'DEFAULT: 1|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'DefaultZoom', 'integer', 'DEFAULT: 7|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'MaximumZoom', 'integer', 'DEFAULT: 9|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'Visibility', 'varchar[]', 'DEFAULT: ''{}''::varchar[]|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisAttribute"', 'MapStyle', 'jsonb', 'DEFAULT: ''{}''::jsonb|NOTNULL: true|MODE: write|STATUS: active');

SELECT _cm3_class_create('_GisLayer', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Gis Geoserver Layer|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'Type', 'varchar', 'UNIQUE: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'OwnerClass', 'regclass', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'OwnerCard', 'integer', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'GeoserverName', 'varchar', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'Index', 'integer', 'DEFAULT: 0|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'MinimumZoom', 'integer', 'DEFAULT: 1|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'DefaultZoom', 'integer', 'DEFAULT: 7|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'MaximumZoom', 'integer', 'DEFAULT: 9|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_GisLayer"', 'Visibility', 'varchar[]', 'DEFAULT: ''{}''::varchar[]|NOTNULL: true|MODE: write|STATUS: active');

CREATE UNIQUE INDEX "_GisLayer_unique_code" ON "_GisLayer" ("Code") WHERE "Status" = 'A';

ALTER TABLE "_GisAttribute" DISABLE TRIGGER USER;
ALTER TABLE "_GisLayer" DISABLE TRIGGER USER;

INSERT INTO "_GisAttribute" (
	"Id",
	"CurrentId",
	"IdClass",
	"Code",
	"Description",
	"Type",
	"Owner",
	"Index",
	"MinimumZoom",
	"DefaultZoom",
	"MaximumZoom",
	"MapStyle",
	"Visibility",
	"Notes",
	"Status"
) SELECT
	"Id",
	"Id",
	'"_GisAttribute"'::regclass,
	"Name",
	"Description",
	"Type",
	_cm3_utils_name_to_regclass("Owner"),
	"Index",
	"MinimumZoom",
	"MinimumZoom",
	"MaximumZoom",
	"MapStyle",
	"Visibility",
	format('migrated from table _Layer with patch 30038; old gis table name = ''%s''',"FullName"),
	'A'
FROM 
	"_Layer" 
WHERE 
	_cm3_utils_name_to_regclass("Owner") IS NOT NULL;

INSERT INTO "_GisLayer" (
	"Id",
	"CurrentId",
	"IdClass",
	"Code",
	"Description",
	"Type",
	"Index",
	"MinimumZoom",
	"DefaultZoom",
	"MaximumZoom",
	"GeoserverName",
	"OwnerClass",
	"OwnerCard",
	"Visibility",
	"Notes",
	"Status"
) SELECT
	"Id",
	"Id",
	'"_GisLayer"'::regclass,
	"Name",
	"Description",
	"Type",
	"Index",
	"MinimumZoom",
	"MinimumZoom",
	"MaximumZoom",
	"GeoServerName",
	regexp_replace("CardsBinding"[1],'(.+)_([0-9]+)$','"\1"')::regclass,
	regexp_replace("CardsBinding"[1],'(.+)_([0-9]+)$','\2')::integer,
	"Visibility",
	format('migrated from table _Layer with patch 30038; old gis CardsBinding = ''%s''',"CardsBinding"),
	'A'
FROM 
	"_Layer" 
WHERE 
	"Owner" IS NULL;

ALTER TABLE "_GisAttribute" ENABLE TRIGGER USER;
ALTER TABLE "_GisLayer" ENABLE TRIGGER USER;

DROP TABLE "_Layer";


DO $$
DECLARE
	cur_name varchar;
	new_name varchar;
	_fk_table varchar;
BEGIN

	FOR cur_name IN
		SELECT table_name cur_name FROM information_schema.tables WHERE table_schema = 'gis' and table_name LIKE 'Detail_%'
	LOOP
		new_name = regexp_replace(cur_name,'^Detail','Gis');
		_fk_table = regexp_replace(cur_name,'^Detail_(.*)_.*','\1');

		IF _cm3_utils_name_to_regclass(_fk_table) IS NULL THEN

			RAISE WARNING 'found orphan gis table = % (missing cm table = %s ); executing orphan table cleanup', cur_name, _fk_table;
			EXECUTE format('DROP TABLE gis.%I CASCADE', cur_name);

		ELSE

			RAISE NOTICE 'rename % -> %s', cur_name, new_name;

			EXECUTE format('ALTER TABLE gis.%I RENAME TO %I',cur_name,new_name);--TODO drop and create!

			EXECUTE format('ALTER INDEX gis."%s_pkey" RENAME TO "%s_pkey"',cur_name,new_name);
			EXECUTE format('ALTER TRIGGER "%s_Master_fkey" ON gis.%I RENAME TO "%s_Master_fkey"',cur_name,new_name,new_name);


			EXECUTE format('DROP TRIGGER "_Constr_%s_Master" ON %I',cur_name,_fk_table);
			EXECUTE format('CREATE TRIGGER "_Constr_%s_Master" BEFORE DELETE OR UPDATE ON %I FOR EACH ROW EXECUTE PROCEDURE _cm_trigger_restrict(''gis.%I'', ''Master'')',new_name,_fk_table,new_name);

		END IF;

	END LOOP;

END $$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION _cm3_gis_find_values(attrs bigint[],area varchar) RETURNS TABLE(ownerclass regclass,attrname varchar,ownercard integer,geometry text) AS $$
BEGIN

	FOR ownerclass, attrname IN
		SELECT "Owner" ownerclass,"Code" attrname 
		FROM "_GisAttribute" 
		WHERE "Id" = ANY (attrs) 
	LOOP
		RETURN QUERY EXECUTE format('SELECT %L::regclass ownerclass,%L::varchar attrname, "Master" ownercard, gis.st_astext("Geometry") geometry FROM gis."Gis_%s_%s" AS g WHERE g."Geometry" && gis.st_makeenvelope(%L,900913)',
			ownerclass::regclass,attrname,_cm3_regclass_to_name(ownerclass),attrname,area);
	END LOOP;
	RETURN;

END $$ LANGUAGE PLPGSQL;

