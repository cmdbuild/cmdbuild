-- email table patch

CREATE TABLE _patch_aux (card_id bigint, email_status varchar);

INSERT INTO _patch_aux (card_id, email_status) SELECT e."Id",lower(l."Code") FROM "Email" e LEFT JOIN "LookUp" l ON e."EmailStatus" = l."Id" WHERE l."Status" = 'A';

UPDATE _patch_aux SET email_status = 'draft' WHERE email_status IS NULL OR email_status = 'new';
UPDATE _patch_aux SET email_status = 'error' WHERE email_status NOT IN ('received','sent','outgoing','error','draft');

ALTER TABLE "Email" DISABLE TRIGGER USER;
ALTER TABLE "Email_history" DISABLE TRIGGER USER;

UPDATE "Email" SET "EmailStatus" = NULL;
SELECT _cm3_attribute_delete('"Email"', 'EmailStatus');
SELECT _cm3_attribute_create('"Email"', 'EmailStatus', 'varchar', 'NOTNULL: true|DEFAULT: draft|MODE: write|DESCR: Email Status|INDEX: 5|BASEDSP: true');
UPDATE "Email" SET "EmailStatus" = (SELECT email_status FROM _patch_aux WHERE card_id = "Id");

ALTER TABLE "Email" ADD CONSTRAINT "EmailStatus_value" CHECK ( "EmailStatus" IN ('received','sent','outgoing','error','draft') );

SELECT _cm3_attribute_create('"Email"', 'ErrorCount', 'integer', 'NOTNULL: true|DEFAULT: 0|MODE: write|DESCR: Error Count');

ALTER TABLE "Email" ENABLE TRIGGER USER;
ALTER TABLE "Email_history" ENABLE TRIGGER USER;

DROP TABLE _patch_aux;

