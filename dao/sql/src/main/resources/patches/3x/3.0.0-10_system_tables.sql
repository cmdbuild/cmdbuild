-- new system tables


SELECT _cm3_class_create('_Session', NULL, 'MODE: reserved|TYPE: simpleclass|DESCR: Session|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Session"'::regclass, 'SessionId', 'varchar(32)', 'UNIQUE: true|NOTNULL: true|MODE: read|DESCR: Session id');
SELECT _cm3_attribute_create('"_Session"'::regclass, 'Data', 'jsonb', 'DEFAULT: ''{}''::jsonb|NOTNULL: true|MODE: read|DESCR: Data');
SELECT _cm3_attribute_create('"_Session"'::regclass, 'LastActiveDate', 'timestamp', 'NOTNULL: true|MODE: read');


SELECT _cm3_class_create('_Request', NULL, 'MODE: reserved|TYPE: simpleclass|DESCR: Request|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Request"', 'Timestamp', 'timestamp', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_Request"', 'Path', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Request Path');
SELECT _cm3_attribute_create('"_Request"', 'Method', 'varchar(16)', 'NOTNULL: true|MODE: read|DESCR: Request Method');
SELECT _cm3_attribute_create('"_Request"', 'SessionId', 'varchar(50)', 'MODE: read|DESCR: Session id');
SELECT _cm3_attribute_create('"_Request"', 'SessionUser', 'varchar(40)', 'MODE: read|DESCR: Username');
SELECT _cm3_attribute_create('"_Request"', 'RequestId', 'varchar(50)', 'NOTNULL: true|MODE: read|DESCR: Request id');
SELECT _cm3_attribute_create('"_Request"', 'TrackingId', 'varchar(50)', 'NOTNULL: true|MODE: read|DESCR: Tracking id');
SELECT _cm3_attribute_create('"_Request"', 'ActionId', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Action Id');
SELECT _cm3_attribute_create('"_Request"', 'Client', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Client IP');
SELECT _cm3_attribute_create('"_Request"', 'UserAgent', 'varchar', 'NOTNULL: true|MODE: read|DESCR: User Agent');
SELECT _cm3_attribute_create('"_Request"', 'Query', 'varchar', 'MODE: read|DESCR: Request Query');
SELECT _cm3_attribute_create('"_Request"', 'Completed', 'boolean', 'NOTNULL: true|MODE: read|DESCR: Request is completed');
SELECT _cm3_attribute_create('"_Request"', 'Payload', 'text', 'MODE: read|DESCR: Request Payload');
SELECT _cm3_attribute_create('"_Request"', 'PayloadSize', 'integer', 'MODE: read|DESCR: Request Payload Size');
SELECT _cm3_attribute_create('"_Request"', 'Response', 'text', 'MODE: read|DESCR: Response Payload');
SELECT _cm3_attribute_create('"_Request"', 'ResponseSize', 'integer', 'MODE: read|DESCR: Response Payload Size');
SELECT _cm3_attribute_create('"_Request"', 'StatusCode', 'char(3)', 'MODE: read|DESCR: Response Status Code');
SELECT _cm3_attribute_create('"_Request"', 'ElapsedTime', 'integer', 'MODE: read');
SELECT _cm3_attribute_create('"_Request"', 'Errors', 'jsonb', 'DEFAULT: ''{}''::jsonb|MODE: read|DESCR: Request Processing Errors');


SELECT _cm3_class_create('_SystemConfig', '"Class"', 'MODE: reserved|TYPE: class|DESCR: System Configuration|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_SystemConfig"', 'Value', 'varchar', 'MODE: write|DESCR: Value|STATUS: active');


SELECT _cm3_class_create('_Lock', NULL, 'MODE: reserved|TYPE: simpleclass|DESCR: Lock|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Lock"', 'SessionId', 'varchar(50)', 'NOTNULL: true|MODE: read|DESCR: Session id');
SELECT _cm3_attribute_create('"_Lock"', 'ItemId', 'varchar(50)', 'NOTNULL: true|UNIQUE: true|MODE: read|DESCR: Item id');
SELECT _cm3_attribute_create('"_Lock"', 'LastActiveDate', 'timestamp', 'NOTNULL: true|MODE: read');


SELECT _cm3_class_create('_UserConfig', '"Class"', 'MODE: reserved|TYPE: class|DESCR: User Configuration|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_UserConfig"', 'Data', 'jsonb', 'DEFAULT: ''{}''::jsonb|NOTNULL: true|MODE: read');
SELECT _cm3_domain_create('UserConfigUser', 'MODE: reserved|TYPE: domain|CLASS1: _UserConfig|CLASS2: User|DESCRDIR: |DESCRINV: |CARDIN: N:1|STATUS: active');
SELECT _cm3_attribute_create('"_UserConfig"', 'Owner', 'bigint', 'NOTNULL: true|UNIQUE: true|MODE: sysread|DESCR: User|REFERENCEDOM: UserConfigUser|REFERENCEDIRECT: true|REFERENCETYPE: restrict|STATUS: active');


SELECT _cm3_class_create('_Document', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Documents|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Document"', 'FileName', 'varchar','NOTNULL: true|MODE: read|DESCR: File Name');
SELECT _cm3_attribute_create('"_Document"', 'MimeType', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Mime Type');
SELECT _cm3_attribute_create('"_Document"', 'DmsFileName', 'varchar', 'MODE: read|DESCR: Dms File Name');
SELECT _cm3_attribute_create('"_Document"', 'Version', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Version Number');
SELECT _cm3_attribute_create('"_Document"', 'Category', 'varchar', 'MODE: read|DESCR: Category');
SELECT _cm3_attribute_create('"_Document"', 'Hash', 'varchar', 'NOTNULL: true|MODE: read|DESCR: File Hash');
SELECT _cm3_attribute_create('"_Document"', 'Size', 'integer', 'NOTNULL: true|MODE: read|DESCR: File Size');
SELECT _cm3_attribute_create('"_Document"', 'CardId', 'integer', 'NOTNULL: true|MODE: read|DESCR: Attached to Card Id');
SELECT _cm3_attribute_create('"_Document"', 'Content', 'bytea', 'MODE: read|DESCR: File content');
--TODO: add unique constraint over cardId,code (or cardId,filename)
--TODO: add index for query, at least on on CardId


SELECT _cm3_class_create('_Plan', '"Class"', 'MODE: reserved|TYPE: class|DESCR: cmdbuild-river plan instances|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Plan"', 'ClassId', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Process Class Name');
SELECT _cm3_attribute_create('"_Plan"', 'Data', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Plan xpdl content');
COMMENT ON COLUMN "_Plan"."BeginDate" IS 'MODE: hidden';


SELECT _cm3_class_create('_SystemStatusLog', NULL, 'MODE: reserved|TYPE: simpleclass|DESCR: System Status Log|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'JavaMemoryUsed', 'integer', 'NOTNULL: true|MODE: read|DESCR: Memory Used by java (MB)');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'JavaMemoryAvailable', 'integer', 'NOTNULL: true|MODE: read|DESCR: Total Memory available for java (MB)');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'SystemMemoryUsed', 'integer', 'MODE: read|DESCR: Memory Used by host system (MB)');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'SystemMemoryAvailable', 'integer', 'MODE: read|DESCR: Total Memory available for host system (MB)');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'FilesystemMemoryUsed', 'integer', 'MODE: read|DESCR: Memory used on webapp filesystem (MB)');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'FilesystemMemoryAvailable', 'integer', 'MODE: read|DESCR: Total Memory available on webapp filesystem (MB)');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'LoadAvg', 'decimal', 'NOTNULL: true|MODE: read|DESCR: CPU Load Average');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'ActiveSessionCount', 'integer', 'NOTNULL: true|MODE: read|DESCR: Active User Sessions');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'Warnings', 'varchar', 'MODE: read|DESCR: Warning Messages');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'Pid', 'integer', 'NOTNULL: true|MODE: read|DESCR: Java Process ID');
SELECT _cm3_attribute_create('"_SystemStatusLog"', 'Hostname', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Hostname');
--TODO: I/O? other info? cache size?


SELECT _cm3_class_create('_Upload', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Uploaded files|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Upload"', 'Path', 'varchar[]', 'NOTNULL: true|UNIQUE: true|MODE: read|DESCR: Complete file path');
SELECT _cm3_attribute_create('"_Upload"', 'FileName', 'varchar', 'NOTNULL: true|MODE: read|DESCR: File Name');
SELECT _cm3_attribute_create('"_Upload"', 'MimeType', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Mime Type');
SELECT _cm3_attribute_create('"_Upload"', 'Hash', 'varchar', 'NOTNULL: true|MODE: read|DESCR: File Hash');
SELECT _cm3_attribute_create('"_Upload"', 'Size', 'integer', 'NOTNULL: true|MODE: read|DESCR: File Size');
SELECT _cm3_attribute_create('"_Upload"', 'Content', 'bytea', 'NOTNULL: true|MODE: read|DESCR: File content');


SELECT _cm3_class_create('_FormTrigger', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Class Form Triggers|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_FormTrigger"', 'Owner', 'regclass', 'NOTNULL: true|MODE: read|DESCR: Owner class');
SELECT _cm3_attribute_create('"_FormTrigger"', 'Active', 'boolean', 'DEFAULT: true|NOTNULL: true|MODE: read|DESCR: Is Active');
SELECT _cm3_attribute_create('"_FormTrigger"', 'Index', 'integer', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_FormTrigger"', 'Script', 'text', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_FormTrigger"', 'Bindings', 'varchar[]', 'NOTNULL: true|MODE: read');
-- TODO index on (Owner,Index) ?


SELECT _cm3_class_create('_ContextMenu', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Class Context Menu items|SUPERCLASS: false|STATUS: active'); -- sysread mode is useful to read table content via ws
SELECT _cm3_attribute_create('"_ContextMenu"', 'Owner', 'regclass', 'NOTNULL: true|MODE: read|DESCR: Owner class');
SELECT _cm3_attribute_create('"_ContextMenu"', 'Active', 'boolean', 'DEFAULT: true|NOTNULL: true|MODE: read|DESCR: Is Active');
SELECT _cm3_attribute_create('"_ContextMenu"', 'Index', 'integer', 'NOTNULL: true|MODE: read');
SELECT _cm3_attribute_create('"_ContextMenu"', 'Type', 'varchar', 'MODE: read');
SELECT _cm3_attribute_create('"_ContextMenu"', 'ComponentId', 'varchar', 'MODE: read');
SELECT _cm3_attribute_create('"_ContextMenu"', 'Script', 'text', 'MODE: read');
SELECT _cm3_attribute_create('"_ContextMenu"', 'Config', 'text', 'MODE: read');
SELECT _cm3_attribute_create('"_ContextMenu"', 'Visibility', 'varchar', 'MODE: read');


SELECT _cm3_class_create('_DocumentData', NULL, 'MODE: reserved|TYPE: simpleclass|DESCR: Document data|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_DocumentData"', 'DocumentId', 'integer', 'UNIQUE: true|NOTNULL: true|MODE: read|DESCR: Document id');
SELECT _cm3_attribute_create('"_DocumentData"', 'Data', 'bytea', 'NOTNULL: true|MODE: read|DESCR: Data');


--- MISC ---

ALTER TABLE "User" DROP COLUMN "Privileged";


SELECT _cm3_attribute_create('"Activity"', 'FlowData', 'jsonb', 'DEFAULT: ''{}''::jsonb|MODE: rescore');


CREATE TABLE _domain_tree_nav_aux AS SELECT * FROM "_DomainTreeNavigation";

DROP TABLE "_DomainTreeNavigation";

SELECT _cm3_class_create('_DomainTreeNavigation', '"Class"', 'MODE: reserved|STATUS: active|SUPERCLASS: false|TYPE: class');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'IdParent', 'integer', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'IdGroup', 'integer', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'Type', 'character varying', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'DomainName', 'character varying', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'Direct', 'boolean', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'BaseNode', 'boolean', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'TargetClassName', 'character varying', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'TargetClassDescription', 'character varying', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'TargetFilter', 'character varying', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_DomainTreeNavigation"', 'EnableRecursion', 'boolean', 'MODE: write|STATUS: active');

COMMENT ON COLUMN "_DomainTreeNavigation"."Description" IS 'MODE: write|STATUS: active';

ALTER TABLE "_DomainTreeNavigation" DISABLE TRIGGER USER;

INSERT INTO "_DomainTreeNavigation" ("Id","CurrentId","IdClass","User","BeginDate","IdParent","IdGroup","Type","DomainName","Direct","BaseNode","TargetClassName","TargetClassDescription","Description","TargetFilter","EnableRecursion","Status")
	SELECT "Id","Id",'"_DomainTreeNavigation"'::regclass,"User","BeginDate","IdParent","IdGroup","Type","DomainName","Direct","BaseNode","TargetClassName","TargetClassDescription","Description","TargetFilter","EnableRecursion",'A' 
	FROM _domain_tree_nav_aux;

ALTER TABLE "_DomainTreeNavigation" ENABLE TRIGGER USER;

DROP TABLE _domain_tree_nav_aux;


TRUNCATE TABLE "_CustomPage"; --TODO: migrate data
TRUNCATE TABLE "_CustomPage_history"; --TODO: migrate data
SELECT _cm3_attribute_create('"_CustomPage"', 'Data', 'bytea', 'NOTNULL: true|MODE: write|STATUS: active');


CREATE TABLE _patch_aux_filter AS SELECT "Id","User","BeginDate","Code","Description","UserId","Filter","ClassId","Shared" FROM "_Filter";

DROP TABLE "_Filter";

SELECT _cm3_class_create('_Filter', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Filter|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Filter"', 'UserId', 'int', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_Filter"', 'Filter', 'jsonb', 'MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_Filter"', 'ClassId', 'regclass', 'NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_create('"_Filter"', 'Shared', 'boolean', 'DEFAULT: false|NOTNULL: true|MODE: write|STATUS: active');
SELECT _cm3_attribute_notnull_set('"_Filter"', 'Code', true);
-- ALTER TABLE "_Filter" ALTER COLUMN "Code" SET NOT NULL;
CREATE UNIQUE INDEX "_Unique_Filter_CodeUserIdClassId" ON "_Filter" ("Code", "UserId", "ClassId") WHERE "Status" = 'A';

ALTER TABLE "_Filter" DISABLE TRIGGER USER;

INSERT INTO "_Filter" ("Id","CurrentId","IdClass","User","BeginDate","Code","Description","UserId","Filter","ClassId","Shared","Status")
	SELECT "Id","Id",'"_Filter"'::regclass,"User","BeginDate","Code","Description","UserId","Filter"::jsonb,"ClassId","Shared",'A' FROM _patch_aux_filter;

ALTER TABLE "_Filter" ENABLE TRIGGER USER;
-- SELECT cm3_create_domain('FilterRole', 'MODE: reserved|TYPE: domain|CLASS1: _Filter|CLASS2: Role|DESCRDIR: |DESCRINV: |CARDIN: N:N|STATUS: active');


SELECT _cm3_attribute_create('"Role"', 'Config', 'jsonb', 'MODE: hidden|STATUS: active');

ALTER TABLE "Role" DISABLE TRIGGER USER;

DO $$
DECLARE
	config jsonb;
	rec RECORD;
BEGIN
	FOR rec IN SELECT * FROM "Role" 
	LOOP
		BEGIN
			config = '{}'::jsonb;

			IF rec."startingClass" IS NOT NULL THEN
				config = jsonb_set(config,'{startingClass}'::text[], ('"'||replace(rec."startingClass"::text,'"','')||'"')::jsonb);
			END IF;

			IF rec."DisabledModules" IS NOT NULL AND rec."DisabledModules"[1] IS NOT NULL THEN
				config = jsonb_set(config,'{disabledModules}'::text[], to_jsonb(rec."DisabledModules"));
			END IF;

			IF rec."DisabledCardTabs" IS NOT NULL AND rec."DisabledCardTabs"[1] IS NOT NULL THEN
				config = jsonb_set(config,'{disabledCardTabs}'::text[], to_jsonb(rec."DisabledCardTabs"));
			END IF;

			IF rec."DisabledProcessTabs" IS NOT NULL AND rec."DisabledProcessTabs"[1] IS NOT NULL THEN
				config = jsonb_set(config,'{disabledProcessTabs}'::text[], to_jsonb(rec."DisabledProcessTabs"));
			END IF;

			IF rec."ProcessWidgetAlwaysEnabled" IS NOT NULL THEN
				config = jsonb_set(config,'{processWidgetAlwaysEnabled}'::text[], rec."ProcessWidgetAlwaysEnabled"::varchar::jsonb);
			END IF;

			UPDATE "Role" SET "Config" = config WHERE "Id" = rec."Id";
		EXCEPTION WHEN others THEN
			RAISE EXCEPTION 'error processing Role record %: %', rec."Id", SQLERRM;
		END;
	END LOOP;
END $$ LANGUAGE PLPGSQL;

UPDATE "Role" SET "Config" = '{}'::jsonb WHERE "Config" IS NULL;
UPDATE "Role" SET "Administrator" = FALSE WHERE "Administrator" IS NULL;

ALTER TABLE "Role" ALTER COLUMN "Config" SET DEFAULT '{}'::jsonb;
ALTER TABLE "Role" ALTER COLUMN "Config" SET NOT NULL;
ALTER TABLE "Role" ALTER COLUMN "Administrator" SET NOT NULL;

-- ALTER TABLE "Role" ALTER COLUMN "Config" DROP NOT NULL;
-- UPDATE "Role" SET 
-- 	"startingClass" = NULL,
-- 	"DisabledModules" = NULL,
-- 	"DisabledCardTabs" = NULL,
-- 	"DisabledProcessTabs" = NULL,
-- 	"ProcessWidgetAlwaysEnabled" = NULL,
-- 	"HideSidePanel" = NULL,
-- 	"FullScreenMode" = NULL,
-- 	"SimpleHistoryModeForCard" = NULL,
-- 	"SimpleHistoryModeForProcess" = NULL,
-- 	"CloudAdmin" = NULL;

ALTER TABLE "Role" ENABLE TRIGGER USER;

ALTER TABLE "Role" DROP COLUMN "startingClass";
ALTER TABLE "Role" DROP COLUMN "DisabledModules";
ALTER TABLE "Role" DROP COLUMN "DisabledCardTabs";
ALTER TABLE "Role" DROP COLUMN "DisabledProcessTabs";
ALTER TABLE "Role" DROP COLUMN "ProcessWidgetAlwaysEnabled";
ALTER TABLE "Role" DROP COLUMN "HideSidePanel";
ALTER TABLE "Role" DROP COLUMN "FullScreenMode";
ALTER TABLE "Role" DROP COLUMN "SimpleHistoryModeForCard";
ALTER TABLE "Role" DROP COLUMN "SimpleHistoryModeForProcess";
ALTER TABLE "Role" DROP COLUMN "CloudAdmin"; 

 