-- migrate widget table

CREATE TABLE _patch_aux ("Code" varchar,"Definition" varchar,"Description" varchar);
INSERT INTO _patch_aux ("Code","Definition","Description") SELECT "Code","Definition","Description" FROM "_Widget" WHERE "Status" = 'A';

DROP TABLE "_Widget_history";
DROP TABLE "_Widget";

SELECT _cm3_class_create('_Widget', '"Class"', 'MODE: reserved|TYPE: class|DESCR: Widget|SUPERCLASS: false|STATUS: active');
SELECT _cm3_attribute_create('"_Widget"', 'Owner', 'regclass', 'NOTNULL: true|MODE: read|DESCR: Owner class');
SELECT _cm3_attribute_create('"_Widget"', 'Active', 'boolean', 'DEFAULT: true|NOTNULL: true|MODE: read|DESCR: Is Active');
SELECT _cm3_attribute_create('"_Widget"', 'Type', 'varchar', 'NOTNULL: true|MODE: read|DESCR: Type');
SELECT _cm3_attribute_create('"_Widget"', 'Data', 'jsonb', 'NOTNULL: true|MODE: read|DESCR: Data');

INSERT INTO "_Widget" (
	"IdClass",
	"Owner",
	"Description",
	"Code",
	"Type",
	"Data",
	"User"
) SELECT 
	'"_Widget"'::regclass,
	_cm3_utils_name_to_regclass("Code"),
	"Definition"::json->>'label',
	"Definition"::json->>'id',
	lower(substring("Description" from 2)),
	"Definition"::json,
	'patch30' --NOTE: change this if patch version changes
FROM _patch_aux;

DROP TABLE _patch_aux;

ALTER TABLE "_Widget" DISABLE TRIGGER USER;

UPDATE "_Widget" SET "Code" = 'widget_'||"Id" WHERE "Code" IS NULL;

ALTER TABLE "_Widget" ENABLE TRIGGER USER;

ALTER TABLE "_Widget" ALTER COLUMN "Code" SET NOT NULL; --TODO use func
CREATE UNIQUE INDEX "_Unique_Widget_OwnerCode" ON "_Widget" ("Owner","Code") WHERE "Status" = 'A'; --TODO use func