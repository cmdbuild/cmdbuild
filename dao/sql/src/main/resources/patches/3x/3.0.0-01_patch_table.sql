-- create new patch table

CREATE TABLE _patch_aux (code varchar,description varchar);

DO $$ BEGIN
	INSERT INTO "_patch_aux" SELECT "Code","Description" FROM "Patch" WHERE "Status" = 'A'; --TODO copy BeginDate
	DROP TABLE "Patch_history";
	DROP TABLE "Patch";
EXCEPTION WHEN undefined_table THEN 
	RAISE NOTICE 'Patch table not found, skipping copy';
END $$ LANGUAGE PLPGSQL;

SELECT cm_create_class('_Patch', 'Class', 'DESCR: Applied patches|MODE: reserved|STATUS: active|SUPERCLASS: false|TYPE: class');
SELECT cm_create_class_attribute('_Patch', 'Category', 'varchar', null, false, false, 'STATUS: active|BASEDSP: false|CLASSORDER: 0|DESCR: Category|GROUP: |INDEX: 4|MODE: write');
SELECT cm_create_class_attribute('_Patch', 'Hash', 'varchar', null, false, false, 'STATUS: active|BASEDSP: true|CLASSORDER: 0|DESCR: Script Hash|GROUP: |INDEX: 5|MODE: write');
SELECT cm_create_class_attribute('_Patch', 'Content', 'text', null, false, false, 'STATUS: active|BASEDSP: false|CLASSORDER: 0|DESCR: Script Content|GROUP: |INDEX: 6|MODE: write');

INSERT INTO "_Patch" ("Code","Description") SELECT code,description FROM _patch_aux;

DROP TABLE _patch_aux;

