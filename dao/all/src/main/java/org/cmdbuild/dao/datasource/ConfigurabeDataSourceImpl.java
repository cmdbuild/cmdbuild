package org.cmdbuild.dao.datasource;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.p6spy.engine.common.P6Util;
import com.p6spy.engine.common.StatementInformation;
import com.p6spy.engine.event.CompoundJdbcEventListener;
import com.p6spy.engine.event.DefaultEventListener;
import com.p6spy.engine.event.JdbcEventListener;
import com.p6spy.engine.event.SimpleJdbcEventListener;
import com.p6spy.engine.spy.JdbcEventListenerFactory;
import com.p6spy.engine.spy.P6DataSource;
import static java.lang.String.format;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Pattern;
import javax.annotation.PreDestroy;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import static org.apache.commons.lang3.StringUtils.isBlank;

import org.apache.commons.lang3.Validate;
import org.apache.commons.dbcp2.BasicDataSource;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.common.java.sql.ForwardingDataSource;
import org.cmdbuild.dao.config.DatabaseConfiguration;
import org.cmdbuild.dao.config.SqlConfiguration;
import org.cmdbuild.dao.ConfigurableDataSource;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import static org.cmdbuild.utils.postgres.PgVersionUtils.getNormalizedVersionStringFromVersionNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.ConfigReloadEvent;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.RAW_DATA_SOURCE;

/**
 * note: if basic datasource lookup is null, or if lookup fails, a new
 * {@link BasicDataSource} instance will be used
 *
 * return bare datasource; with multitenant this cannot be used directly for
 * cmdbuild tables, and must usually be wrapped in a tenant-aware proxy
 *
 * act as a wrapper and configurator around a regular basic data source, which
 * will handle actual connection and pooling
 */
@Component(RAW_DATA_SOURCE)
@Qualifier(SYSTEM_LEVEL_ONE)
public class ConfigurabeDataSourceImpl extends ForwardingDataSource implements ConfigurableDataSource {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DatabaseConfiguration databaseConfiguration;
	private final SqlConfiguration sqlConfiguration;

	private BasicDataSource innnerDataSource;
	private P6DataSource loggerDataSource;

	private DataSource delegateDataSource;

	private boolean configured = false;

	public ConfigurabeDataSourceImpl(DatabaseConfiguration configuration, SqlConfiguration sqlConfiguration) {
		this.databaseConfiguration = checkNotNull(configuration);
		this.sqlConfiguration = checkNotNull(sqlConfiguration);

		checkPgDriverVersion();
		loadInnerDataSource();
		registerConfigurationEventListener();
		configureDatasource();
	}

	@PreDestroy
	public void cleanup() {
		logger.info("close inner data source");
		try {
			innnerDataSource.close();
		} catch (Exception ex) {
			logger.error("error closing inner data source", ex);
		}
		innnerDataSource = null;
		delegateDataSource = null;
	}

	private void checkPgDriverVersion() {
		String pgDriverVersion = org.postgresql.Driver.getVersion();
		logger.info("postgres driver version = {}", pgDriverVersion);
		String normalizedPgDriverVersion = pgDriverVersion.replaceFirst("PostgreSQL *", "");
		checkArgument(normalizedPgDriverVersion.matches("9.4.[0-9]+"), "unsupported jdbc driver version; required postgresql.jar version 9.4.x, but found %s", normalizedPgDriverVersion);
	}

	private boolean datasourceLoggingEnabled() {
		return loggerDataSource != null;
	}

	private void wrapDatasourceWithLogger() {
		logger.info("enable sql logger");
		checkArgument(!datasourceLoggingEnabled());
		loggerDataSource = new P6DataSource(getInner());
		loggerDataSource.setJdbcEventListenerFactory(new MyJdbcEventListenerFactory());
		delegateDataSource = loggerDataSource;
	}

	private void unwrapDatasourceLogger() {
		logger.info("disable datasource logger");
		checkArgument(datasourceLoggingEnabled());
		delegateDataSource = innnerDataSource;
		loggerDataSource = null;
	}

	private void loadInnerDataSource() {
		logger.debug("creating datasource");
		if (isBlank(databaseConfiguration.getBaseDatasourceLookup())) {
			innnerDataSource = new BasicDataSource();
		} else {
			try {
				logger.debug("lookup inner datasource = {}", databaseConfiguration.getBaseDatasourceLookup());
				innnerDataSource = (BasicDataSource) checkNotNull(new InitialContext().lookup(databaseConfiguration.getBaseDatasourceLookup()));
				logger.debug("using inner datasource = {}", innnerDataSource);
			} catch (Exception e) {
				logger.debug("datasource not found", e);
				innnerDataSource = new BasicDataSource();
				logger.warn("datasource not found for lookup = {}, using new datasource instance = {}", databaseConfiguration.getBaseDatasourceLookup(), innnerDataSource);
			}
		}
		delegateDataSource = innnerDataSource;
		logger.debug("datasource ready");
	}

	private void registerConfigurationEventListener() {
		databaseConfiguration.getEventBus().register(new Object() {

			@Subscribe
			public void handleConfigUpdateEvent(ConfigReloadEvent event) {
				configureDatasource();
			}
		});
		sqlConfiguration.getEventBus().register(new Object() {

			@Subscribe
			public void handleConfigUpdateEvent(ConfigReloadEvent event) {
				configureSqlLogger();
			}
		});
	}

	private void configureDatasource() {
		logger.info("configureDatasource");
		if (isBlank(databaseConfiguration.getDatabaseUrl()) || isBlank(databaseConfiguration.getDatabaseUser()) || isBlank(databaseConfiguration.getDatabasePassword())) {
			logger.warn("cannot configure data source: missing database configuration!");
			configured = false;
		} else {
			innnerDataSource.addConnectionProperty("autosave", "conservative");
			innnerDataSource.setDriverClassName(databaseConfiguration.getDriverClassName());
			innnerDataSource.setUrl(databaseConfiguration.getDatabaseUrl());
			innnerDataSource.setUsername(databaseConfiguration.getDatabaseUser());
			innnerDataSource.setPassword(databaseConfiguration.getDatabasePassword());

			if (databaseConfiguration.enableDatabaseConnectionEagerCheck()) {
				checkDatabaseConnectionAndStuff();
			}

			configureSqlLogger();

			configured = true;
			getEventBus().post(DatasourceConfiguredEvent.INSTANCE);
		}

	}

	private void configureSqlLogger() {
		logger.debug("configureSqlLogger");
		if (sqlConfiguration.enableSqlOrDdlLogging() && !datasourceLoggingEnabled()) {
			wrapDatasourceWithLogger();
		} else if (!sqlConfiguration.enableSqlOrDdlLogging() && datasourceLoggingEnabled()) {
			unwrapDatasourceLogger();
		}
	}

	private void checkDatabaseConnectionAndStuff() {
		try (Connection connection = innnerDataSource.getConnection()) {
			ResultSet resultSet = connection.prepareStatement("show server_version_num").executeQuery();
			checkArgument(resultSet.next(), "unable to get postgres server version");
			int pgServerVersion = resultSet.getInt(1);
			checkArgument(pgServerVersion > 0, "unable to get postgres server version");
			String versionString = getNormalizedVersionStringFromVersionNumber(pgServerVersion);
			logger.info("postgres server version = {}", versionString);
			if (pgServerVersion < databaseConfiguration.getMinSupportedPgVersion() || pgServerVersion > databaseConfiguration.getMaxSupportedPgVersion()) {
				logger.warn(marker(), "using unsupported postgres version = {} (recommended version is 9.5.x or 9.6.x)", pgServerVersion);
			}
		} catch (Exception ex) {
			logger.error(marker(), "error checking database connection", ex);
		}
	}

	@Override
	public BasicDataSource getInner() {
		return checkNotNull(innnerDataSource, "inner data source is null (not configured or already closed)");
	}

	@Override
	protected DataSource delegate() {
		return checkNotNull(delegateDataSource, "delegate data source is null (not configured or already closed)");
	}

	@Override
	public EventBus getEventBus() {
		return databaseConfiguration.getEventBus();
	}

	@Override
	public boolean isConfigured() {
		return configured;
	}

	private void checkDatasource() {
		checkArgument(configured, "the datasource is not configured!");
	}

	@Override
	public Connection getConnection() throws SQLException {
		checkDatasource();
		return super.getConnection();
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		checkDatasource();
		return super.getConnection(username, password);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T unwrap(final Class<T> iface) throws SQLException {
		Validate.notNull(iface, "Interface argument must not be null");
		if (!DataSource.class.equals(iface)) {
			final String message = format("data source of type '%s' can only be unwrapped as '%s', not as '%s'", //
					getClass().getName(), //
					DataSource.class.getName(), //
					iface.getName());
			throw new SQLException(message);
		}
		return (T) this;
	}

	@Override
	public boolean isWrapperFor(final Class<?> iface) throws SQLException {
		return DataSource.class.equals(iface);
	}

	private class MyJdbcEventListenerFactory extends SimpleJdbcEventListener implements JdbcEventListenerFactory {

		private final Logger sqlLogger = LoggerFactory.getLogger("org.cmdbuild.sql");
		private final Logger ddlLogger = LoggerFactory.getLogger("org.cmdbuild.sql_ddl");

		private final JdbcEventListener listener;

		public MyJdbcEventListenerFactory() {
			CompoundJdbcEventListener compoundEventListener = new CompoundJdbcEventListener();
			compoundEventListener.addListender(DefaultEventListener.INSTANCE);
			compoundEventListener.addListender(this);
			listener = compoundEventListener;
		}

		@Override
		public JdbcEventListener createJdbcEventListener() {
			return listener;
		}

		@Override
		public void onAfterAnyExecute(StatementInformation statementInformation, long timeElapsedNanos, SQLException e) {
			String sql = normalizeSql(statementInformation.getSqlWithValues());
			boolean logToSql = sqlConfiguration.enableSqlLogging() && (isBlank(sqlConfiguration.excludeSqlRegex()) || !Pattern.compile(sqlConfiguration.excludeSqlRegex()).matcher(sql).find());
			boolean logToDdl = sqlConfiguration.enableDdlLogging() && (isBlank(sqlConfiguration.includeDdlRegex()) || Pattern.compile(sqlConfiguration.includeDdlRegex()).matcher(sql).find());
			if (e == null) {
				if (logToSql) {
					sqlLogger.info(sql);
				} else {
					sqlLogger.trace(sql);
				}
				if (logToDdl) {
					ddlLogger.info(sql);
				}
			} else {
				sqlLogger.error("sql exception = {} on query = {}", e.toString(), sql);
			}
			if (sqlConfiguration.enableSqlLoggingTimeTracking()) {
				String message = format(" -- elapsed time = %sms", timeElapsedNanos / 1000000);
				if (logToSql) {
					sqlLogger.info(message);
				} else {
					sqlLogger.trace(message);
				}
			}
		}

		private String normalizeSql(String sql) {
			return format("%s;", P6Util.singleLine(sql));
		}

	}

}
