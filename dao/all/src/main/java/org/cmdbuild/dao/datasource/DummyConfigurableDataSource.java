/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.datasource;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.EventBus;
import java.util.function.Supplier;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.cmdbuild.common.java.sql.ForwardingDataSource;
import org.cmdbuild.dao.ConfigurableDataSource;

/**
 *
 */
public class DummyConfigurableDataSource extends ForwardingDataSource implements ConfigurableDataSource {

	private final Supplier<DataSource> delegate;

	public DummyConfigurableDataSource(Supplier<DataSource> delegate) {
		this.delegate = checkNotNull(delegate);
	}

	@Override
	protected DataSource delegate() {
		return checkNotNull(delegate.get());
	}

	@Override
	public EventBus getEventBus() {
		return new EventBus();//not used
	}

	@Override
	public boolean isConfigured() {
		return true;
	}

	@Override
	public BasicDataSource getInner() {
		throw new UnsupportedOperationException();
	}

}
