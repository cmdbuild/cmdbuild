/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.dao.config.inner;

import com.google.common.base.Objects;
import java.time.ZonedDateTime;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.versioning.ComparableVersion;

public interface Patch {

	String getVersion();

	String getDescription();

	String getCategory();

	boolean isApplied();

	@Nullable
	ZonedDateTime getApplyDate();

	@Nullable
	String getContent();

	@Nullable
	String getHash();

	/**
	 * return composite key, from category+version
	 *
	 * @return
	 */
	default String getKey() {
		return getCategory() + "-" + getVersion();
	}

	default ComparableVersion getComparableVersion() {
		return new ComparableVersion(getVersion());
	}

	default boolean hasContent() {
		return !StringUtils.isBlank(getContent());
	}

	default boolean hasHash() {
		return !StringUtils.isBlank(getHash());
	}

	default boolean isCore() {
		return Objects.equal(getCategory(), PatchManager.DEFAULT_CATEGORY);
	}

}
