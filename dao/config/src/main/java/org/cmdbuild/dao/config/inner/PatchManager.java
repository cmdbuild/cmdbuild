package org.cmdbuild.dao.config.inner;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.EventBus;
import java.time.ZonedDateTime;
import java.util.List;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;

public interface PatchManager {

	final static String DEFAULT_CATEGORY = "core";

	void reloadPatches();

	default void applyPendingPatches() {
		applyPendingCorePatches(PatchApplyStrategy.ONE_BY_ONE);
	}

	void applyPendingCorePatches(PatchApplyStrategy patchApplyStrategy);

	default void reloadAndApplyPendingPatches(PatchApplyStrategy patchApplyStrategy) {
		reloadPatches();
		applyPendingCorePatches(patchApplyStrategy);
	}

	default void reloadAndApplyPendingPatches() {
		reloadPatches();
		applyPendingPatches();
	}

	/**
	 * get all available patches (for all categories). Whithin each category,
	 * patches are ordered by version asc. Category ordering is by name, with
	 * core first.
	 *
	 * @return
	 */
	List<Patch> getAvailableCorePatches();

	PatchStatus getCorePatchStatus();

	default boolean isUpdated() {
		return equal(getCorePatchStatus(), PatchStatus.UP_TO_DATE);
	}

	default boolean hasPendingPatched() {
		return !isUpdated();
	}

	EventBus getEventBus();

	/**
	 * will return an empty list if there is an error loading from db
	 *
	 * @return
	 */
	List<Patch> getAllPatchesOnDb();

	List<Patch> getAllPatchesFromFiles();

	/**
	 * return all patch info (on db and on file)
	 *
	 * @return
	 */
	List<PatchInfo> getAllPatches();

	@Nullable
	String getLastPatchOnDbKeyOrNull();

	enum PatchApplyStrategy {
		ONE_BY_ONE, SINGLE_BATCH
	}

	enum PatchStatus {
		UNKNOWN,
		UP_TO_DATE,
		NEED_UPDATE
	}

	enum AllPatchAppliedAndDatabaseReadyEvent {
		INSTANCE
	}

	interface PatchInfo extends Patch {

		@Nullable
		Patch getPatchOnDb();

		@Nullable
		Patch getPatchOnFile();

		@Override
		default String getVersion() {
			return getPatch().getVersion();
		}

		@Override
		default String getDescription() {
			return getPatch().getDescription();
		}

		@Override
		default String getCategory() {
			return getPatch().getCategory();
		}

		@Override
		default boolean isApplied() {
			return hasPatchOnDb();
		}

		@Override
		@Nullable
		default ZonedDateTime getApplyDate() {
			return hasPatchOnDb() ? getPatchOnDb().getApplyDate() : null;
		}

		@Override
		@Nullable
		default String getContent() {
			return getPatch().getContent();
		}

		@Override
		@Nullable
		default String getHash() {
			return getPatch().getHash();
		}

		/**
		 * useful to access common data (version, description, etc)
		 *
		 * @return
		 */
		default Patch getPatch() {
			return firstNonNull(getPatchOnFile(), getPatchOnDb());
		}

		default boolean hashMismatch() {
			return hasPatchOnDb() && hasPatchOnFile() && !isBlank(getPatchOnDb().getHash()) && !equal(getPatchOnDb().getHash(), checkNotNull(getPatchOnFile().getHash()));
		}

		default boolean hasPatchOnDb() {
			return getPatchOnDb() != null;
		}

		default boolean hasPatchOnFile() {
			return getPatchOnFile() != null;
		}

		default boolean onlyOnDb() {
			return hasPatchOnDb() && !hasPatchOnFile();
		}

		default boolean onlyOnFile() {
			return hasPatchOnFile() && !hasPatchOnDb();
		}

	}

}
