/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.config.inner;

import org.cmdbuild.dao.ConfigurableDataSource;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javax.annotation.PostConstruct;
import org.cmdbuild.dao.ConfigurableDataSource.DatasourceConfiguredEvent;
import org.cmdbuild.dao.config.inner.PatchManager.AllPatchAppliedAndDatabaseReadyEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author davide
 */
@Component
public class DatabaseStatusServiceImpl implements DatabaseStatusService {

	private final EventBus eventBus = new EventBus();

	@Autowired
	private ConfigurableDataSource dataSource;
	@Autowired
	private PatchManager patchManager;

	private boolean isReady = false;

	@PostConstruct
	public void init() {
		dataSource.getEventBus().register(new Object() {
			@Subscribe
			public void handleDatasourceConfiguredEvent(DatasourceConfiguredEvent event) {
				checkStatus();
			}
		});
		patchManager.getEventBus().register(new Object() {
			@Subscribe
			public void handleAllPatchAppliedAndDatabaseReadyEvent(AllPatchAppliedAndDatabaseReadyEvent event) {
				checkStatus();
			}
		});
		checkStatus();
	}

	private void checkStatus() {
		if (!dataSource.isConfigured()) {
			isReady = false;
		} else if (!patchManager.isUpdated()) {
			isReady = false;
		} else {
			boolean wasNotReady = !isReady;
			isReady = true;
			if (wasNotReady) {
				eventBus.post(DatabaseBecomeReadyEvent.INSTANCE);
			}
		}
	}

	@Override
	public boolean isReady() {
		if (!isReady) {
			checkStatus();// TODO: not sure about this; events should be enough...
		}
		return isReady;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

}
