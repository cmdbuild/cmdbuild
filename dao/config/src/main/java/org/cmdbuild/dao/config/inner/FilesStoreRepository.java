package org.cmdbuild.dao.config.inner;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import java.util.Collection;
import static java.util.Collections.emptyList;
import javax.annotation.Nullable;
import org.apache.commons.io.FileUtils;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilesStoreRepository implements PatchFileRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	final File dir;
	final String category;

	public FilesStoreRepository(File dir, @Nullable String category) {
		this.dir = checkNotNull(dir);
		this.category = category;
	}

	@Override
	@Nullable
	public String getCategory() {
		return category;
	}

	@Override
	public Collection<File> getPatchFiles() {
		if (!(dir.exists() && dir.isDirectory() && dir.canRead())) {
			logger.warn(marker(), "unable to access patch dir = {}", dir.getAbsolutePath());
			return emptyList();
		} else {
			return FileUtils.listFiles(dir, new String[]{"sql"}, true);
		}
	}

}
