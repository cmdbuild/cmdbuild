package org.cmdbuild.dao.config.inner;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Ordering;
import java.io.File;
import java.io.IOException;
import static java.lang.String.format;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.List;

import javax.sql.DataSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.cmdbuild.dao.DaoException;

import org.cmdbuild.exception.ORMException.ORMExceptionType;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.postgres.PostgresUtils;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

public final class DatabaseCreator {

	public final static String EMPTY_DATABASE = "empty",
			EXISTING_DATABASE = "existing",
			DEMO_DATABASE = "demo",
			READY_2_USE_DATABASE = "ready2use.dump.xz";

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private static final String POSTGRES_SUPER_DATABASE = "postgres";
	private static final String SQL_STATE_FOR_ALREADY_PRESENT_ELEMENT = "42710";

	public static final String SHARK_PASSWORD = "shark";
	public static final String SHARK_USERNAME = "shark";
	private static final String SHARK_SCHEMA = "shark", QUARTZ_SCHEMA = "quartz", GIS_SCHEMA = "gis", PUBLIC_SCHEMA = "public";

	private static final String CREATE_LANGUAGE = "CREATE LANGUAGE plpgsql";
	private static final String CREATE_DATABASE = "CREATE DATABASE \"%s\" ENCODING = 'UTF8'";
	private static final String CREATE_ROLE = "CREATE ROLE \"%s\" NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN ENCRYPTED PASSWORD '%s'";
	private static final String CREATE_SCHEMA = "CREATE SCHEMA IF NOT EXISTS \"%s\"";
	private static final String ALTER_DATABASE_OWNER = "ALTER DATABASE \"%s\" OWNER TO \"%s\"";
	private static final String GRANT_SCHEMA_PRIVILEGES = "GRANT ALL ON SCHEMA \"%s\" TO \"%s\"";
	private static final String ALTER_SEARCH_PATH_FOR_ROLE = "ALTER ROLE \"%s\" SET search_path=%s";

	private static final String DROP_DATABASE = "DROP DATABASE IF EXISTS \"%s\"";

	private DatabaseCreatorConfig config = DatabaseCreatorConfigImpl.builder().build();

	public DatabaseCreator() {
	}

	public DatabaseCreator(DatabaseCreatorConfig config) {
		setConfig(config);
	}

	public void setConfig(DatabaseCreatorConfig config) {
		this.config = checkNotNull(config);
	}

	public DatabaseCreatorConfig getConfig() {
		return config;
	}

	private String getSqlPath(String sub) {
		checkArgument(!StringUtils.isBlank(config.getSqlPath()));
		return new File(config.getSqlPath(), sub).getAbsolutePath();
	}

	private String getBaseSqlPath() {
		return getSqlPath("base_schema");
	}

	private String getSampleSqlPath() {
		return getSqlPath("sample_schemas");
	}

	private String getSharkSqlPath() {
		return getSqlPath("shark_schema");
	}

	private File getDumpDir() {
		return new File(new File(config.getSqlPath()).getParentFile(), "dump");
	}

	public DataSource adminDataSource() {
		PGSimpleDataSource dataSource = new PGSimpleDataSource();
		dataSource.setServerName(config.getHost());
		dataSource.setPortNumber(config.getPort());
		dataSource.setUser(config.getAdminUser());
		dataSource.setPassword(config.getAdminPassword());
		dataSource.setDatabaseName(POSTGRES_SUPER_DATABASE);
		return dataSource;
	}

	public DataSource cmdbuildDataSource() {
		PGSimpleDataSource dataSource = new PGSimpleDataSource();
		dataSource.setServerName(config.getHost());
		dataSource.setPortNumber(config.getPort());
		dataSource.setUser(config.getCmdbuildUser());
		dataSource.setPassword(config.getCmdbuildPassword());
		dataSource.setDatabaseName(config.getDatabaseName());
		return dataSource;
	}

	public DataSource sharkDataSource() {
		PGSimpleDataSource dataSource = new PGSimpleDataSource();
		dataSource.setServerName(config.getHost());
		dataSource.setPortNumber(config.getPort());
		dataSource.setUser(SHARK_USERNAME);
		dataSource.setPassword(SHARK_PASSWORD);
		dataSource.setDatabaseName(config.getDatabaseName());
		return dataSource;
	}

	public boolean cmdbuildDatabaseExists() { //TODO check with a query ?
		logger.info("checking database");
		DataSource dataSource = cmdbuildDataSource();
		try {
			try (Connection connection = dataSource.getConnection()) {
				checkNotNull(connection);
				logger.info("database found");
				return true;
			}
		} catch (SQLException ex) {
			logger.info("database not found");
			return false;
		}
	}

	public void configureDatabase() {
		try {

			boolean useExistingDatabase = equal(EXISTING_DATABASE, config.getDatabaseType()),
					restoreFromDump = config.getDatabaseType().matches(".*[.](dump|backup|custom)([.]xz)?$");

			if (!useExistingDatabase) {
				logger.info("create database = {} from source = {}", config.getDatabaseUrl(), config.getDatabaseType());

				// create database
				checkArgument(!cmdbuildDatabaseExists(), "database %s already exists; if you really want to trash it, drop it manually before running this procedure", config.getDatabaseName());
				createDatabase(config.getDatabaseName());

				//create limited user role, if needed
				if (config.useLimitedUser() && !equal(config.getLimitedUser(), config.getAdminUser())) {
					createRole(config.getLimitedUser(), config.getLimitedPassword());
					alterDatabaseOwner(config.getDatabaseName(), config.getCmdbuildUser());

					adminJdbcTemplate().update(format("ALTER USER %s SUPERUSER", config.getLimitedUser()));//TODO escape sql
				}

				createPLSQLLanguage();

				//fill database
				if (restoreFromDump) {
					restoreDump();
				} else {

					if (equal(EMPTY_DATABASE, config.getDatabaseType())) {
						createCmdbuildStructure();
					} else if (equal(DEMO_DATABASE, config.getDatabaseType())) {
						restoreSampleDB();
					} else {
						checkArgument(false, "unsupported database type = %s", config.getDatabaseType());
					}
					if (config.useSharkSchema()) {
						createSharkRole();
						createSchema(SHARK_SCHEMA);
						grantSchemaPrivileges(SHARK_SCHEMA, SHARK_USERNAME);
						createSharkTables();
					}
				}
				{
					JdbcTemplate jdbcTemplate = new JdbcTemplate(cmdbuildDataSource());
					jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS \"_DatabaseImportLog\" (\"Source\" varchar not null,\"ImportTime\" timestamp not null default now())");
					jdbcTemplate.update("INSERT INTO \"_DatabaseImportLog\" (\"Source\") VALUES (?)", config.getDatabaseType());
				}

				if (config.useLimitedUser() && !equal(config.getLimitedUser(), config.getAdminUser())) {
					adminJdbcTemplate().update(format("ALTER USER %s NOSUPERUSER", config.getLimitedUser()));//TODO escape sql
				}
			}
		} catch (Exception e) {
			throw new DaoException(e, "Error while configuring the database");
		}
	}

	private void createDatabase(String name) {
		logger.info("Creating database {}", name);
		adminJdbcTemplate().execute(String.format(CREATE_DATABASE, escapeSchemaName(name)));
	}

	private void createPLSQLLanguage() {
		logger.info("Creating PL/SQL language");
		try {
			adminJdbcTemplate().execute(CREATE_LANGUAGE);
		} catch (DataAccessException e) {
			forwardIfNotAlreadyPresentElement(e);
		}
	}

	private void restoreSampleDB() {
		try {
			logger.info("Restoring demo structure");
			String sql = FileUtils.readFileToString(new File(getSampleSqlPath(), config.getDatabaseType() + "_schema.sql"));
			new JdbcTemplate(cmdbuildDataSource()).execute(sql);
		} catch (IOException ex) {
			throw new DaoException(ex);
		}
	}

	private void restoreDump() {
		try {
			String dumpToRestore = checkNotBlank(config.getDatabaseType());
			logger.info("restoring database from dump = {}", dumpToRestore);
			if (config.useSharkSchema()) {
				createSharkRole();
				createSchema(SHARK_SCHEMA);
				grantSchemaPrivileges(SHARK_SCHEMA, SHARK_USERNAME);
				PostgresUtils.newBuilder(config.getHost(), config.getPort(), SHARK_USERNAME, SHARK_PASSWORD).buildUtils()
						.restoreDumpFromFile(getDumpFile(dumpToRestore), config.getDatabaseName(), asList(SHARK_SCHEMA), false);
			}
			PostgresUtils.newBuilder(config.getHost(), config.getPort(), config.getCmdbuildUser(), config.getCmdbuildPassword()).buildUtils()
					.restoreDumpFromFile(getDumpFile(dumpToRestore), config.getDatabaseName(), list(PUBLIC_SCHEMA, QUARTZ_SCHEMA, GIS_SCHEMA, "bim"), true);
		} catch (Exception ex) {
			throw new DaoException(ex);
		}
	}

	private File getDumpFile(String dumpName) {
		checkNotBlank(dumpName);
		return list(new File(getDumpDir(), dumpName), new File(dumpName)).stream().filter(File::exists).findFirst().orElseThrow(() -> new IllegalArgumentException("dump file not found for name = " + dumpName));
	}

	private void createSharkTables() {
		try {
			logger.info("Creating shark tables");
			new JdbcTemplate(sharkDataSource()).execute(FileUtils.readFileToString(new File(getSharkSqlPath(), "02_shark_emptydb.sql")));
		} catch (IOException ex) {
			throw new DaoException(ex);
		}
	}

	private void createSchema(String schema) {
		logger.info("create schema = {}", schema);
		new JdbcTemplate(cmdbuildDataSource()).execute(String.format(CREATE_SCHEMA, escapeSchemaName(schema)));
	}

	private void createCmdbuildStructure() {
		try {
			logger.info("Creating CMDBuild structure, sql source dir = {}", getBaseSqlPath());
			List<File> sqlFiles = Ordering.usingToString().sortedCopy(Arrays.asList(new File(getBaseSqlPath()).listFiles((File dir, String name) -> {
				return name.toLowerCase().endsWith(".sql");
			})));
			for (File file : sqlFiles) {
				logger.info("applying '{}'", file);
				String content = FileUtils.readFileToString(file);
				new JdbcTemplate(cmdbuildDataSource()).execute(content);
			}
		} catch (IOException ex) {
			throw new DaoException(ex);
		}
	}

	private JdbcTemplate adminJdbcTemplate() {
		return new JdbcTemplate(adminDataSource());
	}

	private void alterDatabaseOwner(String database, String role) {
		logger.info("Changing database ownership");
		adminJdbcTemplate().execute(String.format(ALTER_DATABASE_OWNER, escapeSchemaName(database), escapeSchemaName(role)));
	}

	private void grantSchemaPrivileges(String schema, String role) {
		logger.info("Granting schema privileges");
		new JdbcTemplate(cmdbuildDataSource()).execute(String.format(GRANT_SCHEMA_PRIVILEGES, escapeSchemaName(schema), escapeSchemaName(role)));
	}

	private void createRole(String roleName, String rolePassword) {
		logger.info("Creating role = {}", roleName);
		try {
			adminJdbcTemplate().execute(String.format(CREATE_ROLE, escapeSchemaName(roleName), escapeValue(rolePassword)));
		} catch (DataAccessException e) {
			forwardIfNotAlreadyPresentElement(e);
		}
	}

	private void createSharkRole() {
		logger.info("Creating shark role");
		try {
			JdbcTemplate jdbcTemplate = adminJdbcTemplate();
			jdbcTemplate.execute(String.format(CREATE_ROLE, SHARK_USERNAME, SHARK_PASSWORD));
			jdbcTemplate.execute(String.format(ALTER_SEARCH_PATH_FOR_ROLE, SHARK_USERNAME, "pg_default,shark"));
		} catch (DataAccessException e) {
			forwardIfNotAlreadyPresentElement(e);
		}
	}

	/*
	 * We don't know what could go wrong if this is allowed
	 */
	private String escapeSchemaName(String name) {
		if (name.indexOf('"') >= 0) {
			throw ORMExceptionType.ORM_ILLEGAL_NAME_ERROR.createException(name); //TODO move this before, in param validation
		}
		return name;
	}

	private String escapeValue(String value) {
		return value.replaceAll("'", "''");
	}

	private void forwardIfNotAlreadyPresentElement(DataAccessException e) {
		Throwable cause = e.getCause();
		if (cause instanceof SQLException) {
			String sqlState = SQLException.class.cast(cause).getSQLState();
			if (!SQL_STATE_FOR_ALREADY_PRESENT_ELEMENT.equals(sqlState)) {
				throw e;
			} else {
				// TODO log
			}
		} else {
			throw e;
		}
	}

	public void dropDatabase() {
		logger.info("drop database = {}", config.getDatabaseUrl());
		String dbName = config.getDatabaseName();
		adminJdbcTemplate().execute(String.format(DROP_DATABASE, escapeSchemaName(dbName)));
	}

}
