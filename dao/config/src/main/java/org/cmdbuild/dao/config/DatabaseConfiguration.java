package org.cmdbuild.dao.config;

import com.google.common.eventbus.EventBus;

public interface DatabaseConfiguration {

	String getDatabaseUrl();

	default String getHost() {
		return getDatabaseUrl().replaceFirst(".*//([^:/]+)([:/].*)?", "$1"); //TODO test this
	}

	default int getPort() {
		return Integer.valueOf(getDatabaseUrl().replaceFirst(".*//([^:]+):([0-9]+).*", "$2"));//TODO test this
	}

	default String getDatabase() {
		return getDatabaseUrl().replaceFirst(".*//[^/]+/([^?]+).*", "$1"); //TODO test this
	}

	String getDatabaseUser();

	String getDatabasePassword();

	String getDatabaseAdminUsername();

	String getDatabaseAdminPassword();

	String getDriverClassName();

	EventBus getEventBus();

	boolean enableAutoPatch();

	default String getBaseDatasourceLookup() {
		return "java:/comp/env/jdbc/cmdbuild";
	}

	default int getMinSupportedPgVersion() {
		return 90500;
	}

	default int getMaxSupportedPgVersion() {
		return 90699;
	}

	boolean enableDatabaseConnectionEagerCheck();

}
