/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.config.inner;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;
import java.util.List;
import static org.cmdbuild.common.Constants.CODE_ATTRIBUTE;
import static org.cmdbuild.common.Constants.DESCRIPTION_ATTRIBUTE;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.date.DateUtils.toDateTime;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_BEGINDATE;

/**
 *
 * @author davide
 */
@Component
public class PatchCardRepositoryImpl implements PatchCardRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static final String CATEGORY_ATTR = "Category",
			HASH_ATTR = "Hash",
			CONTENT_ATTR = "Content";

	final JdbcTemplate jdbcTemplate;

	public PatchCardRepositoryImpl(@Qualifier(SYSTEM_LEVEL_ONE) JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
	}

	@Override
	public void store(Patch patch) {
		getStore().store(patch);
	}

	@Override
	public List<Patch> findAll() {
		return getStore().findAll();
	}

	private PatchCardRepository getStore() {
		boolean hasNewPatchTable = jdbcTemplate.queryForObject("SELECT to_regclass('public.\"_Patch\"') IS NOT NULL", Boolean.class);
		return hasNewPatchTable ? new NewCardStore() : new LegacyCardStore();
	}

	private class NewCardStore implements PatchCardRepository {

		@Override
		public void store(Patch patch) {
			jdbcTemplate.update("INSERT INTO \"_Patch\" (\"Code\",\"Description\",\"User\",\"Category\",\"Hash\",\"Content\",\"Status\") VALUES (?,?,'system',?,?,?,'A')",
					patch.getVersion(), patch.getDescription(), patch.getCategory(), patch.getHash(), patch.getContent());
		}

		@Override
		public List<Patch> findAll() {
			return jdbcTemplate.query("SELECT \"Code\",\"Description\",\"BeginDate\",\"Category\",\"Hash\",\"Content\" FROM \"_Patch\" ", (RowMapper<Patch>) (row, i) -> PatchImpl.builder()
					.withVersion(row.getString(CODE_ATTRIBUTE))
					.withDescription(row.getString(DESCRIPTION_ATTRIBUTE))
					.withCategory(row.getString(CATEGORY_ATTR))
					.appliedOn(toDateTime(row.getTimestamp(ATTR_BEGINDATE)))
					.withContent(row.getString(CONTENT_ATTR))
					.withHash(row.getString(HASH_ATTR))
					.build());
		}

	}

	private class LegacyCardStore implements PatchCardRepository {

		@Override
		public void store(Patch patch) {
			//TODO create table if not exists
			jdbcTemplate.update("INSERT INTO \"Patch\" (\"Code\",\"Description\",\"User\",\"Category\") VALUES (?,?,'system',?)",
					patch.getVersion(), patch.getDescription(), patch.getCategory());
		}

		@Override
		public List<Patch> findAll() {
			if (hasOldPatchTable()) {
				return jdbcTemplate.query("SELECT \"Code\",\"Description\",\"BeginDate\",\"Category\" FROM \"Patch\" ", (RowMapper<Patch>) (row, i) -> PatchImpl.builder()
						.withVersion(row.getString(CODE_ATTRIBUTE))
						.withDescription(row.getString(DESCRIPTION_ATTRIBUTE))
						.withCategory(row.getString(CATEGORY_ATTR))
						.appliedOn(toDateTime(row.getTimestamp(ATTR_BEGINDATE)))
						.build());
			} else {
				return emptyList();
			}
		}

		private boolean hasOldPatchTable() {
			return jdbcTemplate.queryForObject("SELECT to_regclass('public.\"Patch\"') IS NOT NULL", Boolean.class);
		}

	}

}
