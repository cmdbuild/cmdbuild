/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.config.inner;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.File;
import org.cmdbuild.dao.config.DatabaseConfiguration;
import org.cmdbuild.dao.driver.postgres.DumpService;
import org.cmdbuild.utils.postgres.PostgresUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DumpServiceImpl implements DumpService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DatabaseConfiguration databaseConfiguration;

	public DumpServiceImpl(DatabaseConfiguration databaseConfiguration) {
		this.databaseConfiguration = checkNotNull(databaseConfiguration);
	}

	@Override
	public void dumpDatabaseToFile(File file) {
		logger.info("dump cmdbuild database to file = {}", file.getAbsolutePath());
		PostgresUtils.newBuilder(
				databaseConfiguration.getHost(),
				databaseConfiguration.getPort(),
				databaseConfiguration.getDatabaseUser(),
				databaseConfiguration.getDatabasePassword())
				.buildUtils()
				.dumpDatabaseToFile(file, databaseConfiguration.getDatabase(), null);
	}

}
