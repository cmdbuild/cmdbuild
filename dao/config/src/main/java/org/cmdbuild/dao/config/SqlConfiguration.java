package org.cmdbuild.dao.config;

import com.google.common.eventbus.EventBus;

public interface SqlConfiguration {

	EventBus getEventBus();

	boolean enableSqlLogging();

	String excludeSqlRegex();

	boolean enableSqlLoggingTimeTracking();

	boolean enableDdlLogging();

	String includeDdlRegex();

	default boolean enableSqlOrDdlLogging() {
		return enableSqlLogging() || enableDdlLogging();
	}
}
