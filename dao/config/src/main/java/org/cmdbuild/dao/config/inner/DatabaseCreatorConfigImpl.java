package org.cmdbuild.dao.config.inner;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.builder.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.lang.Visitor;
import static org.cmdbuild.utils.lang.CmdbPreconditions.firstNotBlank;

public final class DatabaseCreatorConfigImpl implements DatabaseCreatorConfig {

	private final static String JDBC_URL_PATTERN = "^jdbc:postgresql://([^:]+):([0-9]+)/(.+)$";

	private final String adminUser, adminPassword, limitedUser, limitedPassword, sqlPath, dbHost, dbName;
	private final int dbPort;
	private final String cmdbuildDatabaseType;
	private final boolean useLimited, useSharkSchema;

	private DatabaseCreatorConfigImpl(DatabaseCreatorConfigBuilder builder) {
		this.adminUser = builder.adminUser;
		this.adminPassword = builder.adminPassword;
		this.limitedUser = builder.limitedUser;
		this.limitedPassword = builder.limitedPassword;
		this.sqlPath = builder.sqlPath;
		this.dbHost = checkNotBlank(builder.dbHost);
		this.dbName = checkNotBlank(builder.dbName);
		this.dbPort = builder.dbPort;
		this.cmdbuildDatabaseType = builder.cmdbuildDatabaseType;
		this.useLimited = builder.useLimited;
		this.useSharkSchema = builder.useSharkSchema;
	}

	@Override
	public Map<String, String> getConfig() {
		return ImmutableMap.<String, String>builder()
				.put("db.url", getDatabaseUrl())
				.put("db.username", getCmdbuildUser())
				.put("db.password", getCmdbuildPassword())
				.build();
	}

	@Override
	public boolean useLimitedUser() {
		return useLimited;
	}

	@Override
	public boolean useSharkSchema() {
		return useSharkSchema;
	}

	@Override
	public String getCmdbuildUser() {
		if (useLimitedUser()) {
			checkNotNull(limitedUser, "limited user enabled but limited username not set!");
			return limitedUser;
		} else {
			return getAdminUser();
		}
	}

	@Override
	public String getCmdbuildPassword() {
		if (useLimitedUser()) {
			checkNotNull(limitedPassword, "limited user enabled but limited password not set!");
			return limitedPassword;
		} else {
			return getAdminPassword();
		}
	}

	@Override
	public String getDatabaseUrl() {
		return String.format("jdbc:postgresql://%1$s:%2$s/%3$s", checkNotNull(dbHost), checkNotNull(dbPort), checkNotNull(dbName));
	}

	@Override
	public String getHost() {
		return dbHost;
	}

	@Override
	public int getPort() {
		return dbPort;
	}

	@Override
	public String getDatabaseName() {
		return dbName;
	}

	@Override
	public String getAdminUser() {
		return adminUser;
	}

	@Override
	public String getAdminPassword() {
		return adminPassword;
	}

	@Override
	public String getLimitedUser() {
		return limitedUser;
	}

	@Override
	public String getLimitedPassword() {
		return limitedPassword;
	}

	@Override
	public String getDatabaseType() {
		checkNotNull(emptyToNull(cmdbuildDatabaseType), "database type not set !");
		return cmdbuildDatabaseType;
	}

	@Override
	public String getSqlPath() {
		return sqlPath;
	}

	public static DatabaseCreatorConfigBuilder builder() {
		return new DatabaseCreatorConfigBuilder();
	}

	public static DatabaseCreatorConfigBuilder copyOf(DatabaseCreatorConfig config) {
		return new DatabaseCreatorConfigBuilder().
				withAdminUser(config.getAdminUser(), config.getAdminPassword()).
				withLimitedUser(config.getLimitedUser(), config.getLimitedPassword()).
				withSqlPath(config.getSqlPath()).
				withDatabaseUrl(config.getDatabaseUrl()).
				withDatabaseType(config.getDatabaseType()).
				withUseLimitedUser(config.useLimitedUser()).
				withUseSharkSchema(config.useSharkSchema());
	}

	public static class DatabaseCreatorConfigBuilder implements Builder<DatabaseCreatorConfig> {

		private String adminUser, adminPassword, limitedUser, limitedPassword, sqlPath, dbHost = "localhost", dbName = "cmdbuild";
		private Integer dbPort = 5432;
		private String cmdbuildDatabaseType;
		private boolean useLimited = false, useSharkSchema = false;

		private DatabaseCreatorConfigBuilder() {
		}

		public DatabaseCreatorConfigBuilder withConfig(Map<String, String> config) {
			String defaultUser = emptyToNull(config.get("db.username")),
					defaultPsw = emptyToNull(config.get("db.password"));
			String adminUserParam = firstNotBlank(config.get("db.admin.username"), defaultUser),
					adminPswParam = firstNotBlank(config.get("db.admin.password"), defaultPsw);
			String limitedUserParam = firstNotBlank(config.get("db.user.username"), defaultUser),
					limitedPswParam = firstNotBlank(config.get("db.user.password"), defaultPsw);

			withDatabaseUrl(config.get("db.url"))
					.withAdminUser(adminUserParam, adminPswParam)
					.withUseLimitedUser(true)
					.withLimitedUser(limitedUserParam, limitedPswParam);
			return this;
		}

		public DatabaseCreatorConfigBuilder withUseLimitedUser(boolean useLimited) {
			this.useLimited = useLimited;
			return this;
		}

		public DatabaseCreatorConfigBuilder withUseSharkSchema(boolean useSharkSchema) {
			this.useSharkSchema = useSharkSchema;
			return this;
		}

		public DatabaseCreatorConfigBuilder withAdminUser(String username, String password) {
			this.adminUser = username;
			this.adminPassword = password;
			return this;
		}

		public DatabaseCreatorConfigBuilder withLimitedUser(String username, String password) {
			this.limitedUser = username;
			this.limitedPassword = password;
			return this;
		}

		public DatabaseCreatorConfigBuilder withDatabaseUrl(String cmdbuildDatabaseUrl) {
			Matcher matcher = Pattern.compile(JDBC_URL_PATTERN).matcher(cmdbuildDatabaseUrl);
			checkArgument(matcher.find(), "database url syntax mismatch for url = '%s' with regexp = %s", cmdbuildDatabaseUrl, JDBC_URL_PATTERN);
			return this.withDatabaseUrl(matcher.group(1), Integer.valueOf(matcher.group(2)), emptyToNull(matcher.group(3)));
		}

		public DatabaseCreatorConfigBuilder withDatabaseName(String databaseName) {
			dbName = emptyToNull(databaseName);
			return this;
		}

		public DatabaseCreatorConfigBuilder withDatabaseUrl(String host, int port, String databaseName) {
			this.dbHost = host;
			this.dbPort = port;
			return this.withDatabaseName(databaseName);
		}

		public DatabaseCreatorConfigBuilder withDatabaseType(String databaseType) {
			this.cmdbuildDatabaseType = databaseType.trim();
			return this;
		}

		public DatabaseCreatorConfigBuilder withSqlPath(String sqlPath) {
			this.sqlPath = sqlPath;
			return this;
		}

		public DatabaseCreatorConfigBuilder withConfig(InputStream inputStream) {
			final Properties properties = new Properties();
			try {
				properties.load(inputStream);
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
			return this.withConfig((Map) properties);
		}

		public DatabaseCreatorConfigBuilder accept(Visitor<DatabaseCreatorConfigBuilder> visitor) {
			visitor.visit(this);
			return this;
		}

		@Override
		public DatabaseCreatorConfig build() {
			return new DatabaseCreatorConfigImpl(this);
		}

	}

}
