package org.cmdbuild.dao.config.inner;

import org.cmdbuild.dao.ConfigurableDataSource;
import com.github.difflib.DiffUtils;
import com.github.difflib.UnifiedDiffUtils;
import com.google.common.base.Joiner;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ComparisonChain;
import static com.google.common.collect.FluentIterable.concat;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Lists.transform;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import static com.google.common.collect.Sets.newHashSet;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import java.io.StringReader;
import static java.lang.String.format;
import java.util.Collection;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static java.util.Collections.sort;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import org.apache.commons.io.IOUtils;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.dao.ConfigurableDataSource.DatasourceConfiguredEvent;
import org.cmdbuild.dao.DaoException;
import static org.cmdbuild.dao.driver.postgres.PostgresDatabaseAdapterService.IGNORE_TENANT_POLICIES_POSTGRES_VARIABLE;
import static org.cmdbuild.dao.driver.postgres.PostgresDatabaseAdapterService.USER_TENANTS_POSTGRES_VARIABLE;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.SYSTEM_LEVEL_ONE;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.isNullOrEmpty;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.inner;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import static org.cmdbuild.utils.lang.CmdbStringUtils.addLineNumbers;

@Component
public class PatchManagerImpl implements PatchManager {

	private final EventBus eventBus = new EventBus();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final PatchCardRepository patchCardStore;
	private final List<PatchFileRepository> patchRepositories;
	private final ConfigurableDataSource dataSource;
	private final CacheService cacheService;

	private List<Patch> patchesOnDb, patchesFromFiles, availableCorePatches, allAvailablePatches;

	public PatchManagerImpl(PatchCardRepository patchCardStore, List<PatchFileRepository> patchRepositories, ConfigurableDataSource dataSource, @Qualifier(SYSTEM_LEVEL_ONE) CacheService cacheService) {
		this.patchCardStore = checkNotNull(patchCardStore);
		this.patchRepositories = copyOf(checkNotNull(patchRepositories));
		this.dataSource = checkNotNull(dataSource);
		this.cacheService = checkNotNull(cacheService);
		logger.info("init");
		dataSource.getEventBus().register(new Object() {
			@Subscribe
			public void handleDatasourceConfiguredEvent(DatasourceConfiguredEvent event) {
				reloadPatches();
			}
		});
	}

	@Override
	public synchronized void reloadPatches() {
		logger.debug("reloadPatches");
		List<Patch> newPatchesOnDb = null;
		if (dataSource.isConfigured()) {
			try {
				newPatchesOnDb = patchCardStore.findAll();
			} catch (Exception ex) {
				logger.error("unable to retrieve patches from db", ex);
			}
		}

		List<Patch> newPatchesFromFiles = newArrayList();
		for (PatchFileRepository repository : patchRepositories) {
			List<File> files = repository.getPatchFiles()
					.stream().sorted(Ordering.natural().onResultOf(File::getName)).collect(toList());
			String category = firstNonNull(repository.getCategory(), DEFAULT_CATEGORY);
			logger.debug("processing patch files from repository = {} category = {} ({} found)", repository, category, files.size());
			files.forEach((file) -> {
				newPatchesFromFiles.add(PatchImpl.builder()
						.withCategory(category)
						.withFile(file)
						.build());
			});
		}

		List<Patch> newAvailablePatches = null;
		if (newPatchesOnDb != null) {
			List list = newArrayList();
			Multimap<String, Patch> patchesOnDbByCategory = Multimaps.index(newPatchesOnDb, Patch::getCategory);
			Multimap<String, Patch> patchesFromFilesByCategory = Multimaps.index(newPatchesFromFiles, Patch::getCategory);
			for (String category : patchesFromFilesByCategory.keySet().stream().sorted((a, b) -> ComparisonChain.start().compareTrueFirst(equal(a, DEFAULT_CATEGORY), equal(b, DEFAULT_CATEGORY)).compare(a, b).result()).collect(toList())) {
				ComparableVersion lastAppliedPatchVersion = patchesOnDbByCategory.get(category).stream().sorted(Ordering.natural().onResultOf(Patch::getComparableVersion).reverse()).findFirst().map((p) -> p.getComparableVersion()).orElse(null);
				logger.debug("last patch for category = {} on db is = {}", category, lastAppliedPatchVersion);
				patchesFromFilesByCategory.get(category).stream().filter((p) -> lastAppliedPatchVersion == null || p.getComparableVersion().compareTo(lastAppliedPatchVersion) > 0)
						.sorted(Ordering.natural().onResultOf(Patch::getComparableVersion))
						.collect(toCollection(() -> list));
			}
			newAvailablePatches = list;
		}

		this.patchesOnDb = newPatchesOnDb == null ? null : copyOf(newPatchesOnDb);
		this.patchesFromFiles = copyOf(newPatchesFromFiles);
		this.allAvailablePatches = newAvailablePatches == null ? null : copyOf(newAvailablePatches);
		this.availableCorePatches = allAvailablePatches == null ? null : copyOf(allAvailablePatches.stream().filter(Patch::isCore).collect(toList()));
		logger.debug("loaded patches");
		checkPatchesHashAndPrintWarning();
		if (isUpdated()) {
			eventBus.post(AllPatchAppliedAndDatabaseReadyEvent.INSTANCE);
		}
	}

	private void checkPatchesHashAndPrintWarning() {
		patchesOnDb.stream().filter(Patch::hasHash).forEach((patchOnDb) -> {
			Patch patchOnFile = patchesFromFiles.stream().filter((patch) -> equal(patch.getKey(), patchOnDb.getKey())).findAny().orElse(null);
			if (patchOnFile == null) {
				logger.warn("patch = {} \"{}\" exist only on db, not found on file", patchOnDb.getKey(), patchOnDb.getDescription());
			} else if (patchOnDb.hasHash()) {
				if (!equal(patchOnDb.getHash(), patchOnFile.getHash())) {
					logger.warn("hash mismatch for patch = {} \"{}\"; db hash is = {}, patch file hash is = {}", patchOnDb.getKey(), patchOnDb.getDescription(), patchOnDb.getHash(), patchOnFile.getHash());
					if (patchOnDb.hasContent()) {
						try {
							List<String> onDb = IOUtils.readLines(new StringReader(patchOnDb.getContent())),
									onFile = IOUtils.readLines(new StringReader(patchOnFile.getContent()));
							com.github.difflib.patch.Patch<String> patch = DiffUtils.diff(onDb, onFile);
							List<String> udiff = UnifiedDiffUtils.generateUnifiedDiff(
									format("%s-%s_db.sql", patchOnDb.getCategory(), patchOnDb.getVersion()),
									format("%s-%s_patch.sql", patchOnFile.getCategory(), patchOnFile.getVersion()),
									onDb, patch, 4);
							logger.warn("patch file differences:\n\n{}\n", Joiner.on("\n").join(udiff));
						} catch (Exception ex) {
							logger.error("error building mismatching patch file diff message", ex);
						}

					}
				} else {
					logger.debug("unable to test hash of patch = {} \"{}\", patch hash not found on db", patchOnDb.getKey(), patchOnDb.getDescription());
				}
			}
		});
	}

	@Override
	public List<Patch> getAvailableCorePatches() {
		return checkNotNull(availableCorePatches, "unable to get available patches: cannot get patches from db");
	}

	@Override
	public List<Patch> getAllPatchesOnDb() {
		return firstNonNull(patchesOnDb, emptyList());
	}

	@Override
	@Nullable
	public String getLastPatchOnDbKeyOrNull() {
		if (isNullOrEmpty(patchesOnDb)) {
			return null;
		} else {
			return patchesOnDb.stream().sorted(Ordering.natural().onResultOf(Patch::getApplyDate).reversed()).findFirst().get().getKey();
		}
	}

	@Override
	public List<Patch> getAllPatchesFromFiles() {
		return patchesFromFiles;
	}

	@Override
	public List<PatchInfo> getAllPatches() {
		logger.debug("getAllPatches");
		Map<String, Patch> patchesOnDbByKey = patchesOnDb == null ? null : Maps.uniqueIndex(patchesOnDb, Patch::getKey),
				patchesFromFilesByKey = Maps.uniqueIndex(patchesFromFiles, Patch::getKey);
		List<PatchInfo> list = newArrayList();
		for (String key : newHashSet(concat(patchesFromFilesByKey.keySet(), patchesOnDbByKey == null ? emptySet() : patchesOnDbByKey.keySet()))) {
			Patch patchOnDb = patchesOnDbByKey == null ? null : patchesOnDbByKey.get(key);
			Patch patchOnFile = patchesFromFilesByKey.get(key);
			list.add(new PatchInfo() {
				@Override
				public Patch getPatchOnDb() {
					return patchOnDb;
				}

				@Override
				public Patch getPatchOnFile() {
					return patchOnFile;
				}
			});
		}
		sort(list, (a, b) -> ComparisonChain.start().compareTrueFirst(equal(a.getCategory(), DEFAULT_CATEGORY), equal(b.getCategory(), DEFAULT_CATEGORY)).compare(a.getCategory(), b.getCategory()).compare(a.getComparableVersion(), b.getComparableVersion()).result());
		return list;
	}

	@Override
	public void applyPendingCorePatches(PatchApplyStrategy patchApplyStrategy) {
		logger.debug("applyPendingPatches BEGIN");
		switch (patchApplyStrategy) {
			case ONE_BY_ONE: {
				List<Patch> avaiblePatches;
				while (!(avaiblePatches = getAvailableCorePatches()).isEmpty()) {
					Patch patch = avaiblePatches.iterator().next();
					applyPatchAndStore(preparePatchSqlCode(patch), format("%s (%s)", patch.getKey(), abbreviate(patch.getDescription())), singleton(patch));
				}
			}
			break;
			case SINGLE_BATCH: {
				List<Patch> avaiblePatches = getAvailableCorePatches();
				String sql = Joiner.on("\n\n").join(transform(avaiblePatches, this::preparePatchSqlCode));
				String info = format("single batch for [%s]", Joiner.on(",").join(transform(avaiblePatches, Patch::getKey)));
				applyPatchAndStore(sql, info, avaiblePatches);
				checkArgument(getAvailableCorePatches().isEmpty());
			}
			break;
			default:
				throw new IllegalArgumentException(format("unsupported patch strategy = %s", patchApplyStrategy));
		}
		eventBus.post(AllPatchAppliedAndDatabaseReadyEvent.INSTANCE);
		logger.debug("applyPendingPatches END");
	}

	private String preparePatchSqlCode(Patch patch) {
		return format("DO $$ BEGIN RAISE NOTICE 'apply cmdbuild patch %%', '%s'; END $$ LANGUAGE PLPGSQL;\n\n%s\n\nDO $$ BEGIN RAISE NOTICE 'applied cmdbuild patch %%', '%s'; END $$ LANGUAGE PLPGSQL;\n\n",
				patch.getKey(), checkNotBlank(patch.getContent()), patch.getKey());
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	private void applyPatchAndStore(String sql, String patchInfo, Collection<Patch> patchesToStore) {
		applyPatch(sql, patchInfo);
		cacheService.invalidateAll();//must clear typed object cache in case there are changes to patch table
		patchesToStore.forEach(patchCardStore::store);
		reloadPatches();
	}

	private void applyPatch(String sql, String patchInfo) {
		logger.info("applying patch {}", patchInfo);
		logger.debug("applying patch {} BEGIN", patchInfo);
		String sqlToRun = format("\nSET SESSION %s = 'true';\nSET SESSION %s = '{}';\n\n%s", IGNORE_TENANT_POLICIES_POSTGRES_VARIABLE, USER_TENANTS_POSTGRES_VARIABLE, sql);
		CompletableFuture<Void> future = new CompletableFuture<>();
		PlatformTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus status) {
				try {
					new JdbcTemplate(dataSource).execute(sqlToRun);
					future.complete(null);
				} catch (Exception e) {
					logger.debug("error processing patch = {}", patchInfo);
					logger.debug("error processing patch", e);
					status.setRollbackOnly();
					future.completeExceptionally(e);
				}
			}

		});
		try {
			future.get();
		} catch (Exception ex) {
			logger.error("error executing sql script for patch = {}\n{}", patchInfo, addLineNumbers(sqlToRun));
			throw new DaoException(inner(ex), "error processing patch = %s", patchInfo);
		}
		logger.debug("applying patch {} END", patchInfo);
	}

	@Override
	public PatchStatus getCorePatchStatus() {
		if (patchesOnDb == null) {
			return PatchStatus.UNKNOWN;
		} else {
			return getAvailableCorePatches().isEmpty() ? PatchStatus.UP_TO_DATE : PatchStatus.NEED_UPDATE;
		}
	}

}
