/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.config.inner;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.time.ZonedDateTime;
import java.util.regex.Matcher;
import static java.util.regex.Pattern.compile;
import javax.annotation.Nullable;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.apache.commons.lang3.builder.Builder;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;
import org.cmdbuild.exception.ORMException;
import static org.cmdbuild.dao.config.inner.PatchManager.DEFAULT_CATEGORY;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class PatchImpl implements Patch {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	final String version;

	private final String description, category, hash, content;
	final boolean isApplied;
	final ZonedDateTime applyDate;

	public PatchImpl(String version, String description, String category, String hash, String content, boolean isApplied, ZonedDateTime applyDate) {
		this.version = checkNotNull(version);
		this.description = checkNotNull(description);
		this.category = checkNotNull(category);
		this.hash = hash;
		this.content = content;
		this.isApplied = isApplied;
		this.applyDate = applyDate;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getCategory() {
		return category;
	}

	@Override
	public String getHash() {
		return hash;
	}

	@Override
	public String getContent() {
		return content;
	}

	@Override
	public boolean isApplied() {
		return isApplied;
	}

	@Override
	public ZonedDateTime getApplyDate() {
		return applyDate;
	}

	@Override
	public String toString() {
		return reflectionToString(this, SHORT_PREFIX_STYLE);
	}

	public static PatchBuilder builder() {
		return new PatchBuilder();
	}

	private static final String FILENAME_PATTERN = "(\\d\\.\\d\\.\\d-\\d{2}[a-z]?(_[a-z_0-9]+)?)\\.sql";
	private static final String FIRST_LINE_PATTERN = "--\\W*(.+)";

	public static class PatchBuilder implements Builder<Patch> {

		private final Logger logger = LoggerFactory.getLogger(getClass());

		private File file;
		private String category, version, description, content, hash;
		private boolean isApplied = false;
		private ZonedDateTime applyDate;

		/**
		 * Use factory method.
		 */
		private PatchBuilder() {
		}

		@Override
		public Patch build() {
			try {
				if (file != null) {
					if (isBlank(version)) {
						logger.trace("extracting version from file name '{}'", file);
						Matcher matcher = compile(FILENAME_PATTERN).matcher(file.getName());
						if (!matcher.lookingAt()) {
							logger.error("file name does not match expected pattern");
							throw ORMException.ORMExceptionType.ORM_MALFORMED_PATCH.createException();
						}
						version = matcher.group(1);
					}
					content = FileUtils.readFileToString(file);
					if (isBlank(description)) {
						logger.trace("extracting description from first line of file '{}'", file);
						String line = checkNotNull(new BufferedReader(new StringReader(content)).readLine());
						Matcher matcher = compile(FIRST_LINE_PATTERN).matcher(line);
						if (!matcher.lookingAt()) {
							logger.error("first line '{}' does not match expected pattern", line);
							throw ORMException.ORMExceptionType.ORM_MALFORMED_PATCH.createException();
						}
						description = matcher.group(1);
					}
					hash = DigestUtils.sha256Hex(content);
				}
				return new PatchImpl(version, description, firstNonNull(category, DEFAULT_CATEGORY), hash, content, isApplied, applyDate);
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		}

		public PatchBuilder appliedOn(ZonedDateTime dateTime) {
			this.isApplied = true;
			this.applyDate = checkNotNull(dateTime);
			return this;
		}

		public PatchBuilder withFile(File file) {
			this.file = checkNotNull(file);
			return this;
		}

		/**
		 * null category means default ('core')
		 *
		 * @param category
		 * @return
		 */
		public PatchBuilder withCategory(@Nullable String category) {
			this.category = category;
			return this;
		}

		public PatchBuilder withVersion(String version) {
			this.version = checkNotNull(version);
			return this;
		}

		public PatchBuilder withDescription(String description) {
			this.description = checkNotNull(description);
			return this;
		}

		public PatchBuilder withContent(@Nullable String content) {
			this.content = content;
			return this;
		}

		public PatchBuilder withHash(@Nullable String hash) {
			this.hash = hash;
			return this;
		}

	}

}
