/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.config.inner;

import java.util.Map;

/**
 *
 */
public interface DatabaseCreatorConfig {

	boolean useLimitedUser();

	boolean useSharkSchema();

	String getCmdbuildUser();

	String getCmdbuildPassword();

	String getLimitedUser();

	String getLimitedPassword();

	String getDatabaseUrl();

	String getHost();

	int getPort();

	String getDatabaseName();

	String getAdminUser();

	String getAdminPassword();

	String getDatabaseType();

	String getSqlPath();

	Map<String, String> getConfig();
}
