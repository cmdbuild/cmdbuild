/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.dao.orm.services;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.dao.orm.CardMapper;
import org.cmdbuild.dao.orm.CardMapperRepository;
import org.cmdbuild.dao.orm.CardMapperService;
import org.cmdbuild.utils.lang.Builder;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.beans.CardDefinition;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import org.cmdbuild.dao.view.DataView;

@Component
@DependsOn("cardMapperLoader")
public class CardMapperServiceImpl implements CardMapperService {

	private final CardMapperRepository mapperRepository;
	private final DataView dataView;

	public CardMapperServiceImpl(CardMapperRepository mapperRepository, DataView dataView) {
		this.mapperRepository = checkNotNull(mapperRepository);
		this.dataView = checkNotNull(dataView);
	}

	public CardDefinition createCard(Object object) {
		CardMapper mapper = mapperRepository.get(object.getClass());
		Classe thisClass = dataView.getClasse(mapper.getClassId());
		CardDefinition cardDefinition = dataView.createCardFor(thisClass);
		cardDefinition = objectToCard(cardDefinition, object);
		return cardDefinition;
	}

	public CardDefinition updateCard(Object object) {
		CardMapper mapper = mapperRepository.get(object.getClass());
		Classe thisClass = dataView.getClasse(mapper.getClassId());
		Long cardId = checkNotNull(mapper.getCardId(object), "missing card id for object = %s", object);
		Card card = dataView.getCard(thisClass, cardId);
		CardDefinition cardDefinition = dataView.update(card);
		cardDefinition = objectToCard(cardDefinition, object);
		return cardDefinition;
	}

	@Override
	@Deprecated
	public CardDefinition objectToCard(CardDefinition cardDefinition, Object object) {
		if (object instanceof Builder) {
			object = ((Builder) object).build();
		}
		CardMapper mapper = mapperRepository.get(object.getClass());
		return mapper.objectToCard(cardDefinition, object);
	}

	@Override
	public Card objectToCard(Object object) {
		if (object instanceof Builder) {
			object = ((Builder) object).build();
		}
		CardMapper mapper = mapperRepository.get(object.getClass());
		return mapper.objectToCard(CardImpl.builder(), object).withType(dataView.getClasse(mapper.getClassId())).build();
	}

	@Override
	public <T> T cardToObject(Card card) {
		CardMapper<T, ?> mapper = mapperRepository.get(card.getType());
		return mapper.cardToObject(card).build();
	}

	@Override
	public <T, B extends Builder<T, B>> CardMapper<T, B> getMapperForModelOrBuilder(Class model) {
		return mapperRepository.getByBuilderClassOrBeanClass(model);
	}

	@Override
	public Classe getClasseForModelOrBuilder(Class builderOrBeanClass) {
		CardMapper mapper = getMapperForModelOrBuilder(builderOrBeanClass);
		String classId = mapper.getClassId();
		return dataView.getClasse(classId);
	}

	@Override
	public CardMapper getMapperForClasse(Classe classe) {
		return mapperRepository.get(classe);
	}

}
