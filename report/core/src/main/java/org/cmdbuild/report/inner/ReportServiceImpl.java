package org.cmdbuild.report.inner;

import org.cmdbuild.report.custom.ReportProcessorForCmdbSchema;
import static com.google.common.base.Objects.equal;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.DataHandler;
import javax.sql.DataSource;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.MoreCollectors.onlyElement;
import java.io.File;
import static java.lang.String.format;
import java.util.List;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRSubreport;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JRDesignSubreport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.OnErrorTypeEnum;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.auth.grant.UserPrivileges;
import org.cmdbuild.auth.user.OperationUserStore;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.config.CoreConfiguration;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.report.ReportConst.ReportExtension;
import org.cmdbuild.report.ReportException;
import org.cmdbuild.report.ReportInfo;
import org.cmdbuild.report.ReportService;
import org.cmdbuild.report.ReportData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.report.dao.ReportRepository;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.codehaus.plexus.util.FileUtils;
import org.cmdbuild.dao.view.DataView;
import org.cmdbuild.config.api.DirectoryService;
import static org.cmdbuild.report.ReportConst.PARAM_IMAGE;
import static org.cmdbuild.report.ReportConst.PARAM_SUBREPORT;
import org.cmdbuild.report.dao.ReportDataImpl;
import org.cmdbuild.report.dao.ReportDataImpl.ReportDataImplBuilder;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNotBlank;
import org.cmdbuild.utils.lang.Visitor;
import org.cmdbuild.report.ReportProcessor;
import org.cmdbuild.report.utils.ReportUtils;
import static org.cmdbuild.report.utils.ReportUtils.doExecuteReportAndDownload;
import static org.cmdbuild.report.utils.ReportUtils.getImages;
import static org.cmdbuild.report.utils.ReportUtils.getReportParameters;
import static org.cmdbuild.report.utils.ReportUtils.getSubreports;
import static org.cmdbuild.report.utils.ReportUtils.toByteArray;
import static org.cmdbuild.report.utils.ReportUtils.toJasperDesign;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;

@Component
public class ReportServiceImpl implements ReportService {//TODO localization of report (description, other)

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ReportRepository reportStore;
	private final DataSource dataSource;
	private final CoreConfiguration configuration;
	private final DataView dataView;
	private final DirectoryService directoryService;
	private final OperationUserStore userStore;
	private final ReportProcessorService processor;

	public ReportServiceImpl(ReportRepository reportStore, DataSource dataSource, CoreConfiguration configuration, DataView dataView, DirectoryService directoryService, OperationUserStore userStore, ReportProcessorService processor) {
		this.reportStore = checkNotNull(reportStore);
		this.dataSource = checkNotNull(dataSource);
		this.configuration = checkNotNull(configuration);
		this.dataView = checkNotNull(dataView);
		this.directoryService = checkNotNull(directoryService);
		this.userStore = checkNotNull(userStore);
		this.processor = checkNotNull(processor);
	}

	@Override
	public List<ReportInfo> getAll() {
		UserPrivileges privileges = userStore.getUser().getPrivilegeContext();
		return reportStore.getAllActiveReports().stream().filter(privileges::hasReadAccess).map(ReportInfo.class::cast).collect(toList());
	}

	@Override
	public ReportInfo getById(long reportId) {
		return reportStore.getById(reportId);
	}

	@Override
	public ReportInfo getByCode(String code) {
		return reportStore.getReportByCode(code);
	}

	@Override
	public ReportData getReportData(long reportId) {
		return reportStore.getById(reportId);
	}

	@Override
	public ReportData updateReportTemplate(long reportId, Map<String, byte[]> reportFiles) {
		ReportData reportData = getReportData(reportId);
		return reportStore.updateReport(ReportDataImpl.copyOf(reportData)
				.accept(updateReportData(reportFiles))
				.build());
	}

	@Override
	public ReportData createReport(ReportInfo data, Map<String, byte[]> files) {
		ReportData reportData = ReportDataImpl.copyOf(data)
				.accept(updateReportData(files))
				.build();
		return reportStore.createReport(reportData);
	}

	@Override
	public ReportData updateReportInfo(ReportInfo info) {
		ReportData reportData = getReportData(info.getId());
		reportData = ReportDataImpl.copyOf(reportData).withInfo(info).build();
		return reportStore.updateReport(reportData);
	}

	@Override
	public ReportData updateReport(ReportInfo info, Map<String, byte[]> files) {
		ReportData reportData = getReportData(info.getId());
		reportData = ReportDataImpl.copyOf(reportData)
				.withInfo(info)
				.accept(updateReportData(files))
				.build();
		return reportStore.updateReport(reportData);
	}

	@Override
	public void deleteReport(long reportId) {
		reportStore.deleteReportById(reportId);
	}

	private Visitor<ReportDataImplBuilder> updateReportData(Map<String, byte[]> reportFilesParam) {
		return (builder) -> {
			Map<String, byte[]> reportFiles = map(reportFilesParam);
			reportFiles.forEach((k, v) -> checkArgument(isNotBlank(k) && v != null && v.length > 0, "invalid file = %s", k));

			List<Pair<String, JasperDesign>> reportSources = reportFiles.keySet().stream().filter((p) -> p.endsWith(".jrxml")).map((k) -> {
				byte[] data = checkNotNull(reportFiles.get(k));
				try {
					return Pair.of(k, toJasperDesign(data));
				} catch (Exception ex) {
					throw new ReportException(ex, "error processing report from file = {} ({})", k, org.apache.commons.io.FileUtils.byteCountToDisplaySize(data.length));
				}
			}).collect(toList());

			Entry<String, JasperDesign> masterReport;
			try {
				checkArgument(!reportSources.isEmpty());
				if (reportSources.size() == 1) {
					masterReport = getOnlyElement(reportSources);
				} else {
					masterReport = reportSources.stream().filter((r) -> !getSubreports(r.getValue()).isEmpty()).collect(onlyElement());
				}
			} catch (Exception ex) {
				throw new ReportException(ex, "unable to find master report; expected one and only one <file>.jrxml master report file");
			}

			logger.debug("processing master report from file = {}", masterReport.getKey());

			reportSources.stream().filter((p) -> !equal(p.getKey(), masterReport.getKey())).forEach((p) -> {
				String compiledReportName = format("%s.jasper", FilenameUtils.getBaseName(p.getKey()));
				logger.debug("compiling sub report source file = {} to file = {}", p.getKey(), compiledReportName);
				try {
					byte[] data = toByteArray(p.getValue());
					reportFiles.remove(p.getKey());
					reportFiles.put(compiledReportName, data);
				} catch (Exception ex) {
					throw new ReportException(ex, "error compiling sub report file = %s", p.getKey());
				}
			});

			JasperDesign jasperDesign = masterReport.getValue();
			List<String> expectedImages, expectedSubreports;

//		checkJasperDesignParameters(jd); TODO
			List<JRDesignImage> designImages = getImages(jasperDesign);
			expectedImages = designImages.stream().map(ReportUtils::getImageFileName).collect(toList());
			prepareDesignImagesForUpload(designImages);

			List<JRSubreport> subreports = getSubreports(jasperDesign);
			expectedSubreports = subreports.stream().map(ReportUtils::getSubreportName).collect(toList());
			prepareDesignSubreportsForUpload(subreports);

			List<String> missingFiles = list(expectedImages).with(expectedSubreports).stream().distinct().filter(not(reportFiles.keySet()::contains)).collect(toList());
			checkArgument(missingFiles.isEmpty(), "missing required files = %s", missingFiles.stream().collect((joining(","))));

			reportFiles.keySet().stream().filter(not(set(expectedImages).with(expectedSubreports).with(masterReport.getKey())::contains)).forEach((superflousFile) -> {
				logger.warn(marker(), "found unnecessary file = {} (this file will be ignored and discarded)", superflousFile);
			});

			List<byte[]> imageDataList = expectedImages.stream().map(reportFiles::get).collect(toList()),
					subreportDataList = expectedSubreports.stream().map(reportFiles::get).collect(toList());

			byte[] compiledMasterReport = toByteArray(jasperDesign);

			String query = jasperDesign.getQuery() == null ? null : jasperDesign.getQuery().getText().replaceAll("\"", "\\\"");

			builder
					.withImageNames(expectedImages)
					.withImages(imageDataList)
					.withRichReports(subreportDataList)
					.withSimpleReport(compiledMasterReport)
					.withQuery(query);
		};
	}

	@Override
	public List<Attribute> getParamsById(long id) {
		ReportData reportData = reportStore.getById(id);
		return getReportParameters(reportData).stream().map(ReportParameter::toCardAttribute).collect(toList());
	}

	@Override
	public DataHandler executeReportAndDownload(long reportId, ReportExtension reportExtension, Map<String, Object> parameters) {
		ReportData report = reportStore.getById(reportId);
		return processor.executeReport(report, reportExtension, parameters);
	}

	private File getReportDir() {
		return new File(directoryService.getWebappRootDirectory(), "WEB-INF/reports");
	}

	@Override
	public DataHandler executeSchemaReport(ReportExtension reportExtension) { //TODO generalize this report (avoid custom code)
		//TODO permission?
		ReportProcessorForCmdbSchema schemaReportFactory = new ReportProcessorForCmdbSchema(dataSource, reportExtension, configuration, dataView, getReportDir());
		return doExecuteReportAndDownload(schemaReportFactory);
	}

	@Override
	public DataHandler executeClassReport(String classId, ReportExtension reportExtension) { //TODO generalize this report (avoid custom code)
		//TODO permission?
		ReportProcessorForCmdbSchema schemaReportFactory = new ReportProcessorForCmdbSchema(dataSource, reportExtension, checkNotBlank(classId, "class param is null"), configuration, dataView, getReportDir());
		return doExecuteReportAndDownload(schemaReportFactory);
	}

	@Override
	public DataHandler executeReportFromFile(String fileName, ReportExtension reportExtension) {
		try {
			JasperDesign jasperDesign = JRXmlLoader.load(new File(getReportDir(), FileUtils.basename(fileName) + ".jrxml"));
			ReportProcessor reportFactory = new ReportProcessorFromFileImpl(dataSource, configuration, dataView, getReportDir()) {
				@Override
				public JasperDesign getJasperDesign() {
					return jasperDesign;
				}

				@Override
				public ReportExtension getReportExtension() {
					return reportExtension;
				}

				@Override
				public String getBaseFileName() {
					return FileUtils.basename(fileName);
				}
			};
			return doExecuteReportAndDownload(reportFactory);
		} catch (JRException ex) {
			throw new ReportException(ex);
		}
	}

	private static void prepareDesignSubreportsForUpload(List<JRSubreport> subreportsList) {
		for (int i = 0; i < subreportsList.size(); i++) {
			JRDesignSubreport jrSubreport = (JRDesignSubreport) subreportsList.get(i);
			JRDesignExpression newExpr = new JRDesignExpression();
			String newSubreportName = PARAM_SUBREPORT + (i + 1);
			newExpr.setText("$P{REPORT_PARAMETERS_MAP}.get(\"" + newSubreportName + "\")");
			jrSubreport.setExpression(newExpr);
		}
	}

	private static void prepareDesignImagesForUpload(List<JRDesignImage> designImagesList) {
		for (int i = 0; i < designImagesList.size(); i++) {
			JRDesignImage jrImage = designImagesList.get(i);

			// set expression
			JRDesignExpression newImageExpr = new JRDesignExpression();
			String newImageName = PARAM_IMAGE + i;
			newImageExpr.setText("$P{REPORT_PARAMETERS_MAP}.get(\"" + newImageName + "\")");
			jrImage.setExpression(newImageExpr);

			// set options
			jrImage.setUsingCache(true);
			jrImage.setOnErrorType(OnErrorTypeEnum.BLANK);
		}
	}

}
