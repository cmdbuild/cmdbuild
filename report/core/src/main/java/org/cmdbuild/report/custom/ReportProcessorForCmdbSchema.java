package org.cmdbuild.report.custom;

import java.io.File;
import javax.annotation.Nullable;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.trimToNull;

import org.cmdbuild.config.CoreConfiguration;
import org.cmdbuild.report.ReportConst.ReportExtension;
import org.cmdbuild.report.ReportException;
import org.cmdbuild.dao.view.DataView;
import org.cmdbuild.report.inner.ReportProcessorFromFileImpl;

public final class ReportProcessorForCmdbSchema extends ReportProcessorFromFileImpl {

	private final JasperDesign jasperDesign;
	private final ReportExtension reportExtension;
	private final String className;

	public ReportProcessorForCmdbSchema(DataSource dataSource, ReportExtension reportExtension, CoreConfiguration configuration, DataView dataView, File reportDir) {
		this(dataSource, reportExtension, null, configuration, dataView, reportDir);
	}

	public ReportProcessorForCmdbSchema(DataSource dataSource, ReportExtension reportExtension, @Nullable String className, CoreConfiguration configuration, DataView dataView, File reportDir) {
		super(dataSource, configuration, dataView, reportDir);
		try {
			this.reportExtension = reportExtension;
			jasperDesign = JRXmlLoader.load(new File(getReportDir(), "CMDBuild_dbschema.jrxml"));
			if ((this.className = trimToNull(className)) != null) {
				jasperDesign.setName(className);
				addFillParameter("class", className);
			}
			updateImagesPath();
			updateSubreportsPath();
		} catch (JRException ex) {
			throw new ReportException(ex);
		}
	}

	@Override
	public JasperDesign getJasperDesign() {
		return jasperDesign;
	}

	@Override
	public ReportExtension getReportExtension() {
		return reportExtension;
	}

	@Override
	public String getBaseFileName() {
		return firstNonNull(className, "CMDBuild schema");
	}
}
