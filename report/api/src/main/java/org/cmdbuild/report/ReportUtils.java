/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.report;

import org.cmdbuild.report.ReportConst.ReportExtension;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(ReportUtils.class);

	public static ReportExtension reportExtFromString(String ext) {
		return ReportConst.ReportExtension.valueOf(trimAndCheckNotBlank(ext).toUpperCase());
	}

}
