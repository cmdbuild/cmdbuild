package org.cmdbuild.report;

import java.util.Map;

import javax.activation.DataHandler;

import static java.util.Collections.emptyMap;
import java.util.List;
import static org.apache.commons.lang3.math.NumberUtils.isNumber;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.report.ReportConst.ReportExtension;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toLong;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;

public interface ReportService {

	List<ReportInfo> getAll();

	default ReportInfo getByIdOrCode(String reportId) {
		checkNotBlank(reportId);
		if (isNumber(reportId)) {
			return getById(toLong(reportId));
		} else {
			return getByCode(reportId);
		}
	}

	ReportInfo getByCode(String code);

	ReportData getReportData(long reportId);

	ReportInfo getById(long reportId);

	Iterable<Attribute> getParamsById(long id);

	DataHandler executeReportAndDownload(long reportId, ReportExtension extension, Map<String, Object> parameters);

	DataHandler executeSchemaReport(ReportExtension reportExtension); //TODO generalize this report (avoid custom method)

	DataHandler executeClassReport(String classId, ReportExtension reportExtension); //TODO generalize this report (avoid custom method)

	DataHandler executeReportFromFile(String fileName, ReportExtension reportExtension);

	ReportData createReport(ReportInfo data, Map<String, byte[]> files);

	ReportData updateReportInfo(ReportInfo data);

	ReportData updateReportTemplate(long reportId, Map<String, byte[]> reportFiles);

	ReportData updateReport(ReportInfo data, Map<String, byte[]> files);

	void deleteReport(long reportId);

	default DataHandler executeReportAndDownload(String reportIdOrCode, ReportConst.ReportExtension ext) {
		return executeReportAndDownload(reportIdOrCode, ext, emptyMap());
	}

	default DataHandler executeReportAndDownload(String reportIdOrCode, ReportConst.ReportExtension ext, Map<String, Object> parameters) {
		switch (checkNotBlank(reportIdOrCode)) {
			case "schema":
				return executeSchemaReport(ext);
			case "classschema":
				return executeClassReport(toStringOrNull(parameters.get("class")), ext);
			default:
				return executeReportAndDownload(getByIdOrCode(reportIdOrCode).getId(), ext, parameters);
		}
	}

}
