/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.report;

import javax.annotation.Nullable;

import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class ReportInfoImpl implements ReportInfo {

	private final Long id;
	private final String code, description;
	private final boolean isActive;

	private ReportInfoImpl(ReportInfoImplBuilder builder) {
		this.id = builder.id;
		this.code = checkNotBlank(builder.code);
		this.description = builder.description;
		this.isActive = builder.active;
	}

	@Override
	@Nullable
	public Long getId() {
		return id;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	@Override
	public String toString() {
		return "ReportInfoImpl{" + "id=" + id + ", code=" + code + '}';
	}

	public static ReportInfoImplBuilder builder() {
		return new ReportInfoImplBuilder();
	}

	public static ReportInfoImplBuilder copyOf(ReportInfo source) {
		return new ReportInfoImplBuilder()
				.withId(source.getId())
				.withCode(source.getCode())
				.withDescription(source.getDescription())
				.withActive(source.isActive());
	}

	public static class ReportInfoImplBuilder implements Builder<ReportInfoImpl, ReportInfoImplBuilder> {

		private Long id;
		private String code;
		private String description;
		private Boolean active = true;

		public ReportInfoImplBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public ReportInfoImplBuilder withCode(String code) {
			this.code = code;
			return this;
		}

		public ReportInfoImplBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public ReportInfoImplBuilder withActive(Boolean active) {
			this.active = active;
			return this;
		}

		@Override
		public ReportInfoImpl build() {
			return new ReportInfoImpl(this);
		}

	}
}
