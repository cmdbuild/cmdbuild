/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.postgres;

public class PgVersionUtils {

	public static String getNormalizedVersionStringFromVersionNumber(int versionNumber) {
		return Integer.toString(versionNumber).replaceFirst("^([0-9]{1,2})([0-9]{2})([0-9]{2})$", "$1.$2.$3").replaceAll("[.]0", ".");
	}
}
