/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.postgres;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Splitter;
import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.nullToEmpty;
import com.google.common.collect.ImmutableMap;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getLast;
import static com.google.common.collect.Iterables.transform;
import com.google.common.collect.Lists;
import static com.google.common.collect.Lists.newArrayList;
import com.google.common.collect.Sets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.String.format;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.Collections;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorOutputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.io.FileUtils;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempDir;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempFile;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.writeToFile;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;

/**
 *
 * @author davide
 */
public class PostgresUtils {

	public final static String POSTGRES_VERSION_10_4 = "10.4-1",
			POSTGRES_VERSION_9_6 = "9.6.9-1",
			POSTGRES_VERSION_9_5 = "9.5.13-1",
			POSTGRES_VERSION_9_4 = "9.4.18-1",
			POSTGRES_VERSION_9_3 = "9.3.23-1",
			POSTGRES_VERSION_AUTO = "auto",
			POSTGRES_VERSION_DEFAULT = POSTGRES_VERSION_AUTO;
	private final static Set<String> POSTGRES_VERSIONS = Collections.unmodifiableSet(Sets.newLinkedHashSet(asList(POSTGRES_VERSION_9_3, POSTGRES_VERSION_9_4, POSTGRES_VERSION_9_5, POSTGRES_VERSION_9_6, POSTGRES_VERSION_10_4)));
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final String host, username, password;
	private String postgresBinariesVersion;
	private final int port;
	private boolean verbose = false;

	private PostgresUtils(String host, int port, String username, String password, String postgresBinariesVersion) {
		checkNotNull(host);
		checkNotNull(username);
		checkNotNull(password);
		this.host = host;
		this.username = username;
		this.password = password;
		this.port = port;
		checkArgument(Sets.newHashSet(concat(POSTGRES_VERSIONS, asList(POSTGRES_VERSION_AUTO))).contains(postgresBinariesVersion), "unavailable postgres binaries for version = %s", postgresBinariesVersion);
		this.postgresBinariesVersion = postgresBinariesVersion;
	}

	public PostgresUtils withVerbose(boolean verbose) {
		this.verbose = verbose;
		return this;
	}

	public static void checkBinaries() {
		for (String version : POSTGRES_VERSIONS) {
			PostgresUtils instance = newBuilder().withPostgresVersion(version).buildUtils();
			instance.runCommand("pg_dump", asList("--version"));
			instance.runCommand("pg_restore", asList("--version"));
			instance.runCommand("psql", asList("--version"));
		}
	}

	public String getServerVersion() {
		String serverVersion = runCommand("psql", concat(userHostPortParams(), asList(
				"--dbname", "postgres",
				"--no-align",
				"--tuples-only",
				"--command", "show server_version_num"))).trim();
		checkArgument(serverVersion.matches("^[0-9]{5,6}$"), "server version syntax error for value = %s", serverVersion);
		return PgVersionUtils.getNormalizedVersionStringFromVersionNumber(Integer.valueOf(serverVersion));
	}

	/**
	 * restore into existing database.<br>
	 * you MUST create the target database beforehand. <i>pg_restore</i> does
	 * not support create and restore.
	 *
	 * @param file
	 * @param targetDatabase
	 * @throws java.lang.Exception
	 */
	public void restoreDumpFromFile(File file, String targetDatabase) throws Exception {
		restoreDumpFromFile(file, targetDatabase, null, false);
	}

	/**
	 * restore into existing database.<br>
	 * you MUST create the target database beforehand. <i>pg_restore</i> does
	 * not support create and restore.
	 * <br>
	 * <br>
	 * If you specify schemas to restore and createSchemas=true, psql will be
	 * used to manually create the schemas in the supplied database before
	 * running import (otherwise pg_restore would not work correctly).
	 *
	 * @param file
	 * @param targetDatabase
	 * @param schemasToRestore if null or empty it will be ignored
	 * @param createSchemas if true and if schemasToRestore not empty, will try
	 * to create schemas before import
	 * @throws java.lang.Exception
	 */
	public void restoreDumpFromFile(File file, String targetDatabase, @Nullable Collection<String> schemasToRestore, boolean createSchemas) throws Exception {
		checkNotNull(file);
		checkArgument(file.isFile());
		checkNotNull(emptyToNull(targetDatabase));
		logger.debug("restore from file = {} ({}) to database = {}", file.getAbsolutePath(), FileUtils.byteCountToDisplaySize(file.length()), targetDatabase);
		File sourceFile;
		boolean enableXzDecompression;
		if (file.getName().endsWith(".xz")) { //decompress
			logger.debug("the dump file is compressed with xz, decompressing before import");
			enableXzDecompression = true;
			sourceFile = tempFile("file_", ".dump");
			try (InputStream in = new FileInputStream(file); OutputStream out = new FileOutputStream(sourceFile);
					CompressorInputStream compressorInputStream = new CompressorStreamFactory().createCompressorInputStream(CompressorStreamFactory.XZ, in)) {
				IOUtils.copy(compressorInputStream, out);
			}
		} else {
			enableXzDecompression = false;
			sourceFile = file;
		}
		schemasToRestore = firstNonNull(schemasToRestore, emptyList());
		if (!schemasToRestore.isEmpty() && createSchemas) {
			// if we have specified schemas to restore, we must manually create target schemas, otherwise restore will fail :/
			for (String schema : schemasToRestore) {
				if (!equal(schema, "public")) { // we don't need to create public schema
					runCommand("psql", concat(userHostPortParams(), asList(
							"--echo-all",
							"--dbname", targetDatabase,
							"--command", format("CREATE SCHEMA IF NOT EXISTS \"%s\"", schema))));
				}
			}
		}
		List<String> schemasToRestoreCmd = newArrayList(transform(schemasToRestore, (String s) -> "--schema=" + s));
		try {
			runCommand("pg_restore", concat(userHostPortParams(), asList(
					"--dbname", targetDatabase,
					"--clean", "--if-exists",
					"--exit-on-error",
					"--no-owner", // any user name can be used for the initial connection, and this user will own all the created objects.
					"--no-tablespaces", // Do not output commands to select tablespaces. With this option, all objects will be created in whichever tablespace is the default during restore.
					"--no-privileges"), // Prevent restoration of access privileges (grant/revoke commands).
					schemasToRestoreCmd, singletonList(sourceFile.getAbsolutePath())));
		} finally {
			if (enableXzDecompression) {
				FileUtils.deleteQuietly(sourceFile);
			}
		}
		logger.debug("restored from file = {} ({}) to database = {}", file.getAbsolutePath(), FileUtils.byteCountToDisplaySize(file.length()), targetDatabase);
	}

	/**
	 *
	 * @param file
	 * @param sourceDatabase
	 * @param schemas
	 * @throws Exception
	 */
	public void dumpDatabaseToFile(File file, String sourceDatabase, @Nullable String schemas) {
		checkNotNull(file);
		checkNotNull(emptyToNull(sourceDatabase));
		schemas = firstNonNull(emptyToNull(Joiner.on("|").join(Splitter.on(",").trimResults().omitEmptyStrings().split(nullToEmpty(schemas)))), "*"); //TODO check *
		logger.debug("dump database {} (schema = {}) to file {}", sourceDatabase, schemas, file.getAbsolutePath());
		boolean enableXzCompression;
		File dumpFile;
		if (file.getName().endsWith(".xz")) {
			dumpFile = tempFile("file_", ".dump");
			enableXzCompression = true;
		} else {
			dumpFile = file;
			enableXzCompression = false;
		}
		try {
			runCommand("pg_dump", concat(userHostPortParams(), asList(
					"--format", "custom",
					"--no-owner", "--no-privileges",
					"--quote-all-identifiers",
					"--schema", schemas,
					"--file", dumpFile.getAbsolutePath(),
					sourceDatabase)), false);
			checkArgument(dumpFile.isFile() && dumpFile.length() > 0, "output file was not created");
			if (enableXzCompression) {
				logger.debug("the dump file will be compressed with xz, compressing after export");
				try (OutputStream out = new FileOutputStream(file);
						InputStream in = new FileInputStream(dumpFile);
						CompressorOutputStream compressorOutputStream = new CompressorStreamFactory().createCompressorOutputStream(CompressorStreamFactory.XZ, out)) {
					IOUtils.copy(in, compressorOutputStream);
				} catch (Exception ex) {
					throw runtime(ex);
				}
			}
		} finally {
			if (enableXzCompression) {
				FileUtils.deleteQuietly(dumpFile);
			}
		}
		logger.debug("dumped database {} (schema = {}) to file {} ({}) with psql version = {}", sourceDatabase, schemas, file.getAbsolutePath(), FileUtils.byteCountToDisplaySize(file.length()), postgresBinariesVersion);
	}

	public void executeScript(String database, String sqlScript) {
		checkNotBlank(sqlScript);
		checkNotBlank(database);
		File tempFile = tempFile();
		try {
			writeToFile(tempFile, sqlScript);
			runCommand("psql", concat(userHostPortParams(), asList("--dbname", database, "--file", tempFile.getAbsolutePath())));
		} finally {
			deleteQuietly(tempFile);
		}
	}

	private String runCommand(String command, Iterable<String> params) {
		return runCommand(command, params, true);
	}

	private String runCommand(String command, Iterable<String> params, boolean handleExitStatus) {
		if (equal(postgresBinariesVersion, POSTGRES_VERSION_AUTO)) {
			logger.debug("auto detect server version");
			List<String> workingVersions = Lists.newArrayList();
			String exactMatch = null;
			String versionFromServer = null;
			for (String version : POSTGRES_VERSIONS) {
				postgresBinariesVersion = version;
				try {
					versionFromServer = getServerVersion();
					workingVersions.add(version);
				} catch (Exception ex) {
					logger.trace("error", ex);
					logger.debug("psql version {} does not work with this server, trying next ({})", version, ex);
				}
				if (versionFromServer != null) {
					String serverMajorMinor = versionFromServer.replaceFirst("[.][0-9]+$", "");
					exactMatch = find(POSTGRES_VERSIONS, (String s) -> s.startsWith(serverMajorMinor + "."), null);
					if (exactMatch != null) {
						break;
					}
				}
			}
			if (exactMatch != null) {
				logger.debug("selected matching version = {}", exactMatch);
				postgresBinariesVersion = exactMatch;
			} else if (!workingVersions.isEmpty()) {
				postgresBinariesVersion = getLast(workingVersions);
				logger.debug("selected best effort working version = {}", postgresBinariesVersion);
			} else {
				throw new RuntimeException(format("unable to find working psql version for this postgres server version = %s !", firstNonNull(versionFromServer, "unknown")));
			}
		}
		try (PostgresOperation operation = new PostgresOperation()) {
			if (verbose) {
				params = concat(singletonList("--verbose"), params);
			}
			Pair<Integer, String> res = operation.prepare().runCommand(command, params);
			logger.debug("process output = \n\n{}", res.getRight());

			boolean hasError = res.getLeft() != 0 || Pattern.compile("ERROR", Pattern.CASE_INSENSITIVE & Pattern.DOTALL).matcher(res.getRight()).find();
			if (hasError) {
				if (handleExitStatus) {
					throw runtime("command %s return error code = %s : %s", command, res.getLeft(), abbreviate(res.getRight()));
				} else {
					logger.warn(MarkerFactory.getMarker("NOTIFY"), "command {} return error code = {} : {}", command, res.getLeft(), abbreviate(res.getRight()));//TODO duplicate marker factory code
				}
			}
			return res.getValue();

		} catch (Exception ex) {
			throw runtime(ex);
		}
	}

	private Iterable<String> userHostPortParams() {
		return asList("--username", username, "--host", host, "--port", String.valueOf(port));
	}

	private class PostgresOperation implements AutoCloseable {

		private File pgpass, logfile, pwd, bin;
		private Map<String, String> env;

		public PostgresOperation prepare() throws IOException, ZipException {
			pwd = tempDir("postgres_command_");
			logger.debug("create working dir = {} with psql version = {}", pwd.getAbsolutePath(), postgresBinariesVersion);
			checkArgument(pwd.isDirectory());
			pgpass = new File(pwd, ".pgpass");
			logfile = new File(pwd, "command.log");
			FileUtils.writeStringToFile(pgpass, "\n*:*:*:*:" + password.replaceAll("[:\\\\]", "\\\\$0"), Charsets.UTF_8); //TODO check password escape
			Files.setPosixFilePermissions(pgpass.toPath(), PosixFilePermissions.fromString("r--------"));
			env = ImmutableMap.of("PGPASSFILE", pgpass.getAbsolutePath());
			{
				File commands = new File(pwd, "commands.zip");
				FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("postgresql-" + postgresBinariesVersion + "-linux-x64-binaries_stripped.zip"), commands);
				ZipFile zipFile = new ZipFile(commands);
				zipFile.extractAll(pwd.getAbsolutePath());
				FileUtils.deleteQuietly(commands);
				for (File executableFile : (bin = new File(pwd, "pgsql/bin/")).listFiles()) {
					Files.setPosixFilePermissions(executableFile.toPath(), PosixFilePermissions.fromString("r-x------"));
				}
			}
			return this;
		}

		@Override
		public void close() throws Exception {
			logger.debug("clear working dir = {}", pwd.getAbsolutePath());
			FileUtils.deleteQuietly(pwd);
			pwd = pgpass = logfile = null;
			env = null;
		}

		public Pair<Integer, String> runCommand(String command, Iterable<String> commandParams) throws Exception {
			File executableFile = new File(bin, command);
			checkArgument(executableFile.isFile());
			List<String> params = newArrayList(concat(asList(executableFile.getAbsolutePath()), commandParams));
			logger.debug("running command {}", Joiner.on(" ").join(params));
			ProcessBuilder processBuilder = new ProcessBuilder(params)
					.redirectErrorStream(true)
					.redirectOutput(logfile)
					.directory(pwd);
			processBuilder.environment().putAll(env);
			int res = processBuilder.start().waitFor();
			String output = FileUtils.readFileToString(logfile, Charsets.UTF_8);
			return Pair.of(res, output);
		}
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public static Builder newBuilder(String host, int port, String username, String password) {
		return newBuilder()
				.withUsername(username)
				.withPassword(password)
				.withtHost(host)
				.withPort(port);
	}

	public static class Builder {

		private String host = "localhost", username = "postgres", password = "postgres", binariesVersion = POSTGRES_VERSION_DEFAULT;
		private Integer port = 5432;

		public Builder withtHost(String host) {
			this.host = emptyToNull(host);
			return this;
		}

		public Builder withUsername(String username) {
			this.username = emptyToNull(username);
			return this;
		}

		public Builder withPassword(String password) {
			this.password = emptyToNull(password);
			return this;
		}

		public Builder withPort(Integer port) {
			this.port = port;
			return this;
		}

		public Builder withPostgresVersion(String version) {
			this.binariesVersion = version;
			return this;
		}

		public PostgresUtils buildUtils() {
			return new PostgresUtils(host, port, username, password, binariesVersion);
		}

	}
}
