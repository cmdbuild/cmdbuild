/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.io;

import static com.google.common.base.MoreObjects.firstNonNull;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import javax.annotation.Nullable;
import org.apache.commons.io.FileUtils;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.cmdbuild.utils.date.DateUtils.dateTimeFileSuffix;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;

/**
 *
 * @author davide
 */
public final class CmdbuildFileUtils {

	public static void writeToFile(File file, String content) {
		try {
			FileUtils.write(file, content);
		} catch (IOException ex) {
			throw runtime(ex, "error writitng to file = %s", file);
		}
	}

	public static void writeToFile(File file, byte[] data) {
		try {
			FileUtils.writeByteArrayToFile(file, data);
		} catch (IOException ex) {
			throw runtime(ex, "error writitng to file = %s", file);
		}
	}

	public static String readToString(File file) {
		try {
			return FileUtils.readFileToString(file);
		} catch (IOException ex) {
			throw runtime(ex, "error reading file = %s", file);
		}
	}

	public static byte[] toByteArray(File file) {
		try {
			return FileUtils.readFileToByteArray(file);
		} catch (IOException ex) {
			throw runtime(ex, "error reading file = %s", file);
		}
	}

	/**
	 * return temp file; will set deleteOnExit().
	 *
	 * @return
	 */
	public static File tempFile() {
		return tempFile(null, null);
	}

	/**
	 * return temp file with given prefix and optional suffix; will set
	 * deleteOnExit().
	 *
	 * @param prefix optional file prefix, trailing [-_]+ will be stripped and
	 * replaced with single _
	 * @param suffix optional file suffix, leading [.]+ will be stripped and
	 * replaced with single .
	 * @return
	 */
	public static File tempFile(@Nullable String prefix, @Nullable String suffix) {
		File tempFile = new File(sysTmpDir(), (firstNonNull(prefix, "cmdbuild_temp_").replaceAll("[_-]+$", "") + "_" + tempId()) + "." + firstNonNull(suffix, "file").replaceAll("^[.]+", ""));
		checkArgument(tempFile.getParentFile().isDirectory(), "java temp dir error, tmpdir = (%s)", tempFile.getParent());
		tempFile.deleteOnExit();
		return tempFile;
	}

	/**
	 * return temp dir; will set deleteOnExit().
	 *
	 * @return
	 */
	public static File tempDir() {
		return tempDir(null);
	}

	/**
	 * return temp dir with optional prefix; will set deleteOnExit().
	 *
	 * @param prefix dir prefix, trailing [-_]+ will be stripped and replaced
	 * with single _
	 * @return
	 */
	public static File tempDir(@Nullable String prefix) {
		return tempDir(prefix, true);
	}

	public static File tempDir(@Nullable String prefix, boolean deleteOnExit) {
		File tempDir = new File(sysTmpDir(), (firstNonNull(prefix, "cmdbuild_temp_").replaceAll("[_-]+$", "") + "_" + tempId()));
		checkArgument(tempDir.mkdirs(), "unable to create temp dir %s", tempDir);
		tempDir.deleteOnExit();
		return tempDir;
	}

	public static File sysTmpDir() {
		String javaIoTmpdir = System.getProperty("java.io.tmpdir"), cmdbuildTmpDir = Optional.fromNullable(trimToNull(System.getProperty("cmdbuild.tmpdir"))).or(Optional.fromNullable(trimToNull(System.getenv("CMDBUILD_TMP")))).orNull();
		if (!isBlank(cmdbuildTmpDir) && new File(cmdbuildTmpDir).isDirectory()) {
			return new File(cmdbuildTmpDir);
		} else {
			return new File(javaIoTmpdir);
		}
	}

	/**
	 *
	 * @return a temp id build with date and random uuid, to be used as part of
	 * a temporary file name
	 */
	public static String tempId() {
		return dateTimeFileSuffix() + "_" + UUID.randomUUID().toString().substring(0, 6);
	}

}
