/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.io;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class CmdbuildIoUtils {

	public static void copy(InputStream in, OutputStream out) {
		try {
			IOUtils.copy(in, out);
		} catch (IOException ex) {
			throw runtime(ex);
		}
	}

	public static void copy(InputStream in, File out) {
		try {
			FileUtils.copyInputStreamToFile(in, out);
		} catch (IOException ex) {
			throw runtime(ex);
		}
	}

	public static void writeToFile(String data, File out) {
		try {
			FileUtils.write(out, data);
		} catch (IOException ex) {
			throw runtime(ex);
		}
	}

	public static void writeToFile(byte[] data, File out) {
		try {
			FileUtils.writeByteArrayToFile(out, data);
		} catch (IOException ex) {
			throw runtime(ex);
		}
	}

	public static void copy(DataHandler in, File out) {
		try {
			FileUtils.copyInputStreamToFile(in.getInputStream(), out);
		} catch (IOException ex) {
			throw runtime(ex);
		}
	}

	public static byte[] serializeObject(Object object) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(out)) {
			objectOutputStream.writeObject(object);
		} catch (IOException ex) {
			throw runtime(ex);
		}
		return out.toByteArray();
	}

	public static <T> T deserializeObject(byte[] data) {
		try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(data))) {
			return (T) objectInputStream.readObject();
		} catch (IOException | ClassNotFoundException ex) {
			throw runtime(ex);
		}
	}

	public static Properties loadProperties(byte[] data) {
		try {
			Properties properties = new Properties();
			properties.load(new ByteArrayInputStream(data));
			return properties;
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static byte[] toByteArray(DataHandler dataHandler) {
		try (InputStream in = dataHandler.getInputStream()) {
			return IOUtils.toByteArray(in);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static byte[] toByteArray(InputStream inputStream) {
		try {
			byte[] data = IOUtils.toByteArray(inputStream);
			return data;
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String readToString(DataHandler dataHandler) {
		try {
			return IOUtils.toString(dataHandler.getInputStream());
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String readToString(DataSource dataSource) {
		try {
			return IOUtils.toString(dataSource.getInputStream());
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String readToString(InputStream inputStream) {
		try {
			return IOUtils.toString(inputStream);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static DataHandler newDataHandler(byte[] data, String contentType, String fileName) {
		return new DataHandler(newDataSource(data, contentType, fileName));
	}

	public static DataSource toDataSource(DataHandler dataHandler) {
		return new DataSource() {
			@Override
			public String getContentType() {
				return dataHandler.getContentType();
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return dataHandler.getInputStream();
			}

			@Override
			public String getName() {
				return dataHandler.getName();
			}

			@Override
			public OutputStream getOutputStream() throws IOException {
				return dataHandler.getOutputStream();
			}
		};
	}

	public static DataSource newDataSource(byte[] data, String contentType, String fileName) {
		checkNotNull(data);
		checkNotBlank(contentType);
		checkNotBlank(fileName);
		return new DataSource() {
			@Override
			public InputStream getInputStream() throws IOException {
				return new ByteArrayInputStream(data);
			}

			@Override
			public OutputStream getOutputStream() throws IOException {
				throw new UnsupportedOperationException();
			}

			@Override
			public String getContentType() {
				return contentType;
			}

			@Override
			public String getName() {
				return fileName;
			}
		};
	}

}
