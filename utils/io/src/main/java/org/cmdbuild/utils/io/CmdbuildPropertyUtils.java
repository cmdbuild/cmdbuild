/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.io;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;

public class CmdbuildPropertyUtils {

	public static String serializeMapAsProperties(Map map) {
		Properties properties = new Properties();
		properties.putAll(Maps.transformValues(map, (x) -> firstNonNull(x, "")));
		StringWriter writer = new StringWriter();
		try {
			properties.store(writer, null);
		} catch (IOException ex) {
			throw runtime(ex);
		}
		return writer.toString();
	}

	public static Map<String, String> loadProperties(InputStream in) {
		try {
			Properties properties = new Properties();
			properties.load(checkNotNull(in, "input stream is null"));
			return (Map) properties;
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
