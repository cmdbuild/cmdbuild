/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.exec;

import com.google.common.base.Joiner;
import static com.google.common.base.Preconditions.checkArgument;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.io.IOUtils;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CmdbProcessUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(CmdbProcessUtils.class);

	private final static ExecutorService EXECUTOR = Executors.newCachedThreadPool();

	public static void executeProcess(String... params) {

		try {
			String cliStr = Joiner.on(" ").join(params);
			LOGGER.debug("exec command = '{}'", cliStr);
			ProcessBuilder processBuilder = new ProcessBuilder(params);
			processBuilder.redirectErrorStream(true);
			Process process = processBuilder.start();
			AtomicReference<String> output = new AtomicReference<>("<output unavailable>");
			EXECUTOR.submit(() -> {
				try {
					output.set(IOUtils.toString(process.getInputStream()));
				} catch (Exception ex) {
					LOGGER.warn("error processing stream output from command = '{}'", ex, cliStr);
				}
			});
			int res = process.waitFor();
			LOGGER.debug("command = '{}' returned with code = {} and output = \n\n{}\n", cliStr, res, output.get());
			checkArgument(res == 0, "error executing command = '%s' : %s", cliStr, abbreviate(output.get()));
		} catch (IOException | InterruptedException ex) {
			throw runtime(ex);
		}
	}
}
