#!/bin/bash

dir="$1"

shift

ssh share mkdir -vp "$dir" || exit 1
scp "$@" share:"$dir/" || exit 1
ssh share chown -R nobody:nogroup "$dir/" || exit 1


