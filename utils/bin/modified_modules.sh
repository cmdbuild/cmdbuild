#!/bin/bash

t=$(mktemp)
git status --porcelain | cut -c4- | while read path x; do
#	echo "test path $path"
	while ! [ -e "$path"/pom.xml ]; do
		path="$(dirname "$path")"
	done
#	echo "found dir $path"
	#TODO filter empty paths
	echo "$path" >> $t
done

t2=$(mktemp)
cat $t | sort | uniq | egrep -v '^[.]$|test' > $t2

modules="$(cat $t2 | tr '\n' ',' | sed 's#,$##' )"
rm $t $t2

echo "$modules"


