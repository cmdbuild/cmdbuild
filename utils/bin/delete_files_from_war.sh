#!/bin/bash

warfile="$1"
shift

echo "rm $@ from file $warfile"
echo -n "processing... "

t=$(mktemp -d)
cp "$warfile" "$t/file.zip" || exit 1

cd $t
unzip -q file.zip || exit 1
rm file.zip

rm -rf $@ || exit 1

zip -q -r file.zip *

cd - &>/dev/null
cp "$t/file.zip" "$warfile" || exit 1

rm -rf "$t"

echo "done"

