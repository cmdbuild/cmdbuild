#!/bin/bash

label="$1"
file="$2"

echo "set version label to $label for file $file"

[ -f "$file" ] || exit 1

echo -n "processing... "

t=$(mktemp -d)
cp "$file" "$t/file.zip" || exit 1

cd $t
unzip -q file.zip || exit 1
rm file.zip


sed -i -r 's#(.*"release" *: *")[^"]+(".*)#\1'"${label}"'\2#g' translations/*.json

zip -q -r file.zip *

cd - &>/dev/null
cp "$t/file.zip" "$file"

rm -rf "$t"

echo "done"

