/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import static java.util.Arrays.asList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class CmdbExecutorUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(CmdbExecutorUtils.class);

	public static void shutdownQuietly(ExecutorService... executors) {
		asList(executors).forEach((executor) -> executor.shutdown());
		asList(executors).forEach((executor) -> {
			try {
				executor.awaitTermination(5, TimeUnit.SECONDS);
			} catch (InterruptedException ex) {
			}
		});
		asList(executors).forEach((executor) -> {
			if (!executor.isTerminated()) {
				LOGGER.warn("executor service = {} failed to stop in time", executor);
			}
		});
	}
}
