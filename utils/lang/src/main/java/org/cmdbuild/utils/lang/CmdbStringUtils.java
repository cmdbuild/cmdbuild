/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import static com.google.common.base.Strings.nullToEmpty;
import com.google.common.collect.Ordering;
import static java.lang.Integer.max;
import static java.lang.String.format;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class CmdbStringUtils {

	private final static int DEFAULT_MAX = 500;

	public static @Nullable
	String toStringOrNull(@Nullable Object value) {
		if (value == null) {
			return null;
		} else {
			return value.toString();
		}
	}

	public static String toStringNotBlank(Object value) {
		return checkNotBlank(toStringOrNull(value));
	}

	public static String toStringOrEmpty(@Nullable Object value) {
		return nullToEmpty(toStringOrNull(value));
	}

	public static @Nullable
	String abbreviate(@Nullable Object value) {
		return abbreviate(toStringOrNull(value));
	}

	public static @Nullable
	String abbreviate(@Nullable String value) {
		return abbreviate(value, DEFAULT_MAX);
	}

	public static @Nullable
	String truncate(@Nullable String value, int len) {
		if (value == null || value.length() <= len) {
			return value;
		} else {
			return value.substring(0, len);
		}
	}

	public static @Nullable
	String addLineNumbers(@Nullable String value) {
		if (value == null) {
			return null;
		} else {
			List<String> list = list(Splitter.onPattern("\r?\n\r?").splitToList(value));
			for (int i = 0; i < list.size(); i++) {
				list.set(i, format("%4s: %s", i + 1, list.get(i)));
			}
			return Joiner.on("\n").join(list);
		}
	}

	public static @Nullable
	String normalize(@Nullable String value) {
		if (value == null) {
			return value;
		} else {
			return value.replaceAll("[\n\r]+", " ").replaceAll("[ \t]+", " ");
		}
	}

	public static @Nullable
	String abbreviate(@Nullable String value, int max) {
		if (value == null) {
			return value;
		} else {
			value = normalize(value);
			if (value.length() <= max) {
				return value;
			} else {
				String count = format(" (%s chars)", value.length());
				return StringUtils.abbreviate(value, max - count.length()) + count;
			}
		}
	}

	public static String mapToLoggableString(Properties properties) {
		return mapToLoggableString(map(properties));
	}

	public static String mapToLoggableString(Map<? extends String, ?> map) {
		if (map.isEmpty()) {
			return "<empty map>";
		} else {
			int len = max(20, map.keySet().stream().map(String::length).collect(Collectors.maxBy(Ordering.natural())).get() + 5);
			return Joiner.on("\n").join(map.entrySet().stream()
					.sorted(Ordering.natural().onResultOf(Map.Entry::getKey))
					.map((entry) -> format("\t\t%-" + len + "s %50s = %s", entry.getKey(), "(" + classNameOrVoid(entry.getValue()) + ")", abbreviate(entry.getValue())))
					.collect(toList()));
		}
	}

	public static Object mapToLoggableStringLazy(Map<? extends String, ?> map) {
		return new Object() {

			@Override
			public String toString() {
				return mapToLoggableString(map);
			}

		};
	}

	public static String classNameOrVoid(@Nullable Object value) {
		Class theClass = classOrVoid(value);
		return theClass.getName().startsWith("java.lang") ? theClass.getSimpleName() : theClass.getName();
	}

	public static Class classOrVoid(@Nullable Object value) {
		if (value == null) {
			return Void.class;
		} else {
			return value.getClass();
		}
	}

}
