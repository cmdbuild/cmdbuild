/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import com.google.gson.Gson;
import static java.lang.String.format;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.time.ZonedDateTime;
import java.util.Arrays;
import static java.util.Arrays.stream;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.utils.date.DateUtils;
import org.cmdbuild.utils.json.CmJsonUtils;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.getClassOfNullable;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNullOrBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CmdbConvertUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(CmdbConvertUtils.class);
//	private static final Map<String, Function> CUSTOM_CONVERTERS = map();
//
//	public static <A, B> void registerCustomConverter(Class<A> sourceClass, Class<B> targetClass, Function<A, B> converter) {
//		CUSTOM_CONVERTERS.put(format("%s -> %s", sourceClass.getName(), targetClass.getName()), checkNotNull(converter));
//	}
//
//	private static @Nullable
//	<A, B> Function<A, B> getCustomConverterOrNull(Class<A> sourceClass, Class<B> targetClass) {
//		return CUSTOM_CONVERTERS.get(format("%s -> %s", sourceClass.getName(), targetClass.getName()));
//	}

	@Nullable
	public static Boolean toBooleanOrNull(@Nullable Object value) {
		return convert(value, Boolean.class);
	}

	@Nullable
	public static BigDecimal toBigDecimalOrNull(@Nullable Object value) {
		return convert(value, BigDecimal.class);
	}

	@Nullable
	public static Integer toIntegerOrNull(@Nullable Object value) {
		return convert(value, Integer.class);
	}

	@Nullable
	public static Double toDoubleOrNull(@Nullable Object value) {
		return convert(value, Double.class);
	}

	@Nullable
	public static Long toLongOrNull(@Nullable Object value) {
		return convert(value, Long.class);
	}

	public static boolean isLong(Number number) {
		return Math.floor(number.doubleValue()) == number.doubleValue();
	}

	public static long toLong(Object value) {
		return checkNotNull(toLongOrNull(value));
	}

	public static boolean toBoolean(Object value) {
		return convert(value, Boolean.class);
	}

	public static boolean toBooleanOrDefault(@Nullable Object value, boolean defaultValue) {
		if (isBlank(value)) {
			return defaultValue;
		} else {
			return convert(value, Boolean.class);
		}
	}

	public static @Nullable
	Integer toIntegerOrDefault(@Nullable String value, @Nullable Integer defaultValue) {
		if (isBlank(value)) {
			return defaultValue;
		} else {
			return Integer.valueOf(value);
		}
	}

	public static @Nullable
	<T> T convert(@Nullable Object value, Class<T> targetClass) {
		try {
			if (value == null) {
				return null;
			} else if (targetClass.isInstance(value)) {
				return targetClass.cast(value);
			} else if (targetClass.equals(Iterable.class) || targetClass.equals(Collection.class) || targetClass.equals(List.class)) {
				if (value instanceof Iterable) {
					return targetClass.cast((T) list((Iterable) value));
				} else if (value.getClass().isArray()) {
					return targetClass.cast(arrayToList(value));
				} else if (value instanceof java.sql.Array) {
					try (ResultSet resultSet = ((java.sql.Array) value).getResultSet()) {
						List list = list();
						while (resultSet.next()) {
							list.add(resultSet.getObject(2)); // 1 is index, 2 is value
						}
						return (T) list;
					}
				} else {
					throw error(value, targetClass);
				}
			} else if (targetClass.equals(Set.class)) {
				return targetClass.cast((Set) set(convert(value, Iterable.class)));
			} else if (targetClass.isArray()) {
				List list = (List) convert(value, List.class).stream().map((v) -> convert(v, targetClass.getComponentType())).collect(toList());
				return targetClass.cast(list.toArray((Object[]) Array.newInstance(targetClass.getComponentType(), list.size())));
			} else if (isPrimitiveOrWrapper(targetClass) && ToPrimitive.class.isAssignableFrom(value.getClass())) {
				return convert(ToPrimitive.class.cast(value).toPrimitive(), targetClass);
			} else {
//				Function customConverter = getCustomConverterOrNull(getClassOfNullable(value), targetClass);//TODO use all hierarchy
//				if (customConverter != null) {
//					return targetClass.cast(customConverter.apply(value));
//				} else {

				T res = convertIfDateTimeOrReturnNull(value, targetClass);
				if (res != null) {
					return res;
				} else {
					return convert(value.toString(), targetClass);
				}
//				}
			}
		} catch (Exception ex) {
			throw new IllegalArgumentException(format("error converting value = %s of type = %s to type = %s", abbreviate(value), getClassOfNullable(value).getName(), targetClass), ex);
		}
	}

	public static boolean isPrimitiveOrWrapper(Object value) {
		return isPrimitiveOrWrapper(value.getClass());
	}

	public static boolean isPrimitiveOrWrapper(Class myClass) {
		return myClass.isPrimitive()
				|| myClass == Double.class
				|| myClass == Float.class
				|| myClass == Long.class
				|| myClass == Integer.class
				|| myClass == Short.class
				|| myClass == Character.class
				|| myClass == Byte.class
				|| myClass == Boolean.class
				|| myClass == String.class;
	}

	public static @Nullable
	<T> T convert(@Nullable String value, Class<T> targetClass) {
		try {
			if (targetClass.equals(String.class)) {
				return targetClass.cast(value);
			} else if (StringUtils.isBlank(value)) {
				return null;
			} else if (targetClass.equals(Integer.class) || targetClass.equals(int.class)) {
				return (T) (Integer) toLong(value).intValue();
			} else if (targetClass.equals(Long.class) || targetClass.equals(long.class)) {
				return (T) toLong(value);
			} else if (targetClass.equals(BigDecimal.class) || targetClass.equals(Number.class)) {
				return (T) new BigDecimal(value);
			} else if (targetClass.equals(Double.class) || targetClass.equals(double.class)) {
				return (T) Double.valueOf(value);
			} else if (targetClass.equals(Boolean.class) || targetClass.equals(boolean.class)) {
				return (T) Boolean.valueOf(value);
			} else if (targetClass.isEnum()) {
				return (T) parseEnum(value, (Class) targetClass);
			} else if (!isPrimitiveOrWrapper(targetClass) && hasJsonBeanAnnotation(targetClass)) {
				return (T) convertStringToBeanWithModel(value, targetClass);
			} else {
				T res = convertIfDateTimeOrReturnNull(value, targetClass);
				if (res != null) {
					return res;
				} else {
					throw new IllegalArgumentException("unsupported conversion");
				}
			}
		} catch (Exception ex) {
			throw new IllegalArgumentException(format("error converting value = %s to type = %s", abbreviate(value), targetClass), ex);
		}
	}

	public static @Nullable
	<T extends Enum<T>> T parseEnumOrNull(@Nullable String value, Class<T> enumClass) {
		return isBlank(value) ? null : parseEnum(value, enumClass);
	}

	public static <T extends Enum<T>> T parseEnumOrDefault(@Nullable String value, Class<T> enumClass, T defaultValue) {
		return isBlank(value) ? defaultValue : parseEnum(value, enumClass);
	}

	public static <T extends Enum<T>> T parseEnum(String value, Class<T> enumClass) {
		checkNotBlank(value);
		T enumValue = EnumUtils.getEnum(enumClass, value);
		if (enumValue == null) {
			enumValue = ((Map<String, T>) EnumUtils.getEnumMap(enumClass)).entrySet().stream().filter((e) -> e.getKey().equalsIgnoreCase(value)).collect(toOptional()).map(Entry::getValue).orElse(null);
		}
		if (enumValue == null) {
			String key = format("%s_%s", enumClass.getSimpleName().replaceAll("[^A-Z]", ""), value);
			enumValue = ((Map<String, T>) EnumUtils.getEnumMap(enumClass)).entrySet().stream().filter((e) -> e.getKey().equalsIgnoreCase(key)).collect(toOptional()).map(Entry::getValue).orElse(null);
		}
		return checkNotNull(enumValue, "enum value not found for name = %s, valid values = %s", value, EnumUtils.getEnumList(enumClass));
	}

	public static @Nullable
	<T> T convert(@Nullable Object value, Type type) {
		if (value == null) {
			return null;
		} else if (type instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) type;
			Class outer = (Class) parameterizedType.getRawType();
			if (List.class.isAssignableFrom(outer) || Collection.class.isAssignableFrom(outer) || Iterable.class.isAssignableFrom(outer)) {
				Class inner = (Class) parameterizedType.getActualTypeArguments()[0];
				return (T) convert(value, List.class).stream().map((e) -> convert(e, inner)).collect(toList());
			} else if (Map.class.isAssignableFrom(outer)) {
				return (T) convertToMap(value, type);
			} else {
				throw new IllegalArgumentException("unsupported conversion of parametrized type = " + type);
			}

		} else {
			return (T) convert(value, (Class) type);
		}
	}

	private static @Nullable
	Map convertToMap(@Nullable Object value, Type type) {
		try {
			checkArgument(Map.class.isAssignableFrom((Class<?>) (((ParameterizedType) type).getRawType())));
			if (value == null) {
				return null;
			} else if (value instanceof Map) {
				return (Map) value;//TODO convert keys
			} else if (value instanceof String && isJsonMap((String) value)) {
				return new Gson().fromJson((String) value, type);
			} else {
				throw new IllegalArgumentException("unsupported conversion");
			}
		} catch (Exception ex) {
			throw new IllegalArgumentException(format("error converting value = %s to type = %s", abbreviate(value), type), ex);
		}
	}

	private static boolean isJsonMap(String value) {
		return isNotBlank(value) && value.startsWith("{") && value.endsWith("}");
	}

	@Nullable
	private static <T> T convertIfDateTimeOrReturnNull(Object value, Class<T> targetClass) {
		if (Date.class.equals(targetClass)) {
			return targetClass.cast(DateUtils.toJavaDate(value));
		} else if (ZonedDateTime.class.equals(targetClass)) {
			return targetClass.cast(DateUtils.toDateTime(value));
		} else if (java.sql.Timestamp.class.equals(targetClass)) {
			return targetClass.cast(DateUtils.toSqlTimestamp(value));
		} else if (java.sql.Time.class.equals(targetClass)) {
			return targetClass.cast(DateUtils.toSqlTime(value));
		} else if (java.sql.Date.class.equals(targetClass)) {
			return targetClass.cast(DateUtils.toSqlDate(value));
		} else {
			return null;
		}
	}

	private static Long toLong(String value) {
		BigDecimal num = new BigDecimal(value);
		Long res;
		try {
			res = num.longValueExact();
		} catch (ArithmeticException ex) {
			try {
				res = num.longValue();
				LOGGER.warn("error converting value = {} to int/long, unable to execute exact conversion (had to truncate or something)", value);
				LOGGER.debug("error converting numeric value", ex);
			} catch (Exception exx) {
				throw ex;
			}
		}
		return res;
	}

	private static RuntimeException error(Object value, Class targetClass) {
		return new IllegalArgumentException(format("unsupported conversion of value %s to type = %s", abbreviate(value), targetClass));
	}

	private static <T> List<T> arrayToList(Object value) {
		List<T> list = list();
		for (int i = 0; i < Array.getLength(value); i++) {
			list.add((T) Array.get(value, i));
		}
		return list;
	}

	/**
	 * @Deprecated: use isNullOrBlank
	 */
	@Deprecated
	public static boolean isBlank(@Nullable Object value) {
		return isNullOrBlank(value);
	}

	public static <T> T defaultValue(Class<T> targetClass) {
		if (targetClass.equals(String.class)) {
			return targetClass.cast("");
		} else if (targetClass.equals(Integer.class) || targetClass.equals(int.class)) {
			return targetClass.cast(0);
		} else if (targetClass.equals(Long.class) || targetClass.equals(long.class)) {
			return targetClass.cast(0l);
		} else if (targetClass.equals(BigDecimal.class) || targetClass.equals(Number.class)) {
			return targetClass.cast(BigDecimal.ZERO);
		} else if (targetClass.equals(Double.class) || targetClass.equals(double.class)) {
			return targetClass.cast(0d);
		} else if (targetClass.equals(Float.class) || targetClass.equals(float.class)) {
			return targetClass.cast(0f);
		} else if (targetClass.equals(Boolean.class) || targetClass.equals(boolean.class)) {
			return targetClass.cast(false);
		} else {
//			return newInstance(targetClass);
			return null;
		}
	}

//	private static <T> T newInstance(Class<T> targetClass) {
//		try {
//			return targetClass.newInstance();
//		} catch (InstantiationException | IllegalAccessException ex) {
//			throw new RuntimeException("error creating new instance of type = " + targetClass.getName(), ex);
//		}
//	}
	public static <T> boolean hasJsonBeanAnnotation(Class<T> targetClass) {
		return targetClass.getAnnotation(JsonBean.class) != null
				|| stream(targetClass.getConstructors()).map(Constructor::getParameterAnnotations).flatMap(Arrays::stream).flatMap(Arrays::stream).anyMatch(JsonProperty.class::isInstance);
	}

	public static boolean hasJsonBeanAnnotation(Method method) {
		return method.getAnnotation(JsonBean.class) != null;
	}

	private static <T> T convertStringToBeanWithModel(String value, Class<T> targetInterfaceOrMode) {
		JsonBean annotation = targetInterfaceOrMode.getAnnotation(JsonBean.class);
		Class targetClass = annotation == null || annotation.value() == null ? targetInterfaceOrMode : annotation.value();
		checkArgument(targetInterfaceOrMode.isAssignableFrom(targetClass));
		return (T) targetInterfaceOrMode.cast(CmJsonUtils.fromJson(value, targetClass));
	}
}
