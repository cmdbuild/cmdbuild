/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import static com.google.common.collect.Iterables.concat;
import com.google.common.collect.Streams;
import static com.google.common.reflect.Reflection.newProxy;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.inner;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 */
public class CmdbReflectionUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(CmdbReflectionUtils.class);

	/**
	 * autowire all fields marked with {@Link Autowired} using supplied beans.
	 * Autowire is made by type, by scanning beans list and selecting the first
	 * valid entry.
	 *
	 * Will throw an exception if it fail to autowire all marked fields.
	 *
	 * @param <T>
	 * @param bean
	 * @param beans
	 * @return autowired bean (for chaining)
	 */
	@Deprecated
	public static <T> T autowireFields(T bean, Object... beans) {
		LOGGER.trace("processing bean = {}", bean);
		Streams.stream(getAllFields(bean.getClass())).filter((field) -> field.getAnnotation(Autowired.class) != null).forEach((field) -> {
			LOGGER.trace("processing field = {}", field.getName());
			boolean accessible = field.isAccessible();
			field.setAccessible(true);
			try {
				for (Object candidate : beans) {
					if (field.getType().isAssignableFrom(candidate.getClass())) {
						try {
							LOGGER.trace("autowire field = {} with value = {}", field.getName(), candidate);
							field.set(bean, candidate);
							return;
						} catch (IllegalArgumentException | IllegalAccessException ex) {
							throw new RuntimeException();
						}
					}
				}
				throw new RuntimeException("unable to autowire bean = " + bean + ", no valid candidate found for field = " + field.getName());
			} finally {
				field.setAccessible(accessible);
			}
		});
		return bean;
	}

	public static Iterable<Field> getAllFields(Class type) {
		if (type.equals(Object.class)) {
			return emptyList();
		} else {
			return concat(getAllFields(type.getSuperclass()), asList(type.getDeclaredFields()));
		}
	}

	public static <T> T wrapProxy(Class<T> iface, T inner, ProxyWrapper proxyWrapper) {
		return newProxy(iface, (Object proxy, Method method, Object[] args) -> {
			proxyWrapper.beforeMethodInvocation(method, args);
			try {
				Object response = method.invoke(inner, args);
				proxyWrapper.afterSuccessfullMethodInvocation(method, args, response);
				return response;
			} catch (Throwable error) {
				proxyWrapper.afterFailedMethodInvocation(method, args, error);
				throw error;
			} finally {
				proxyWrapper.afterMethodInvocation(method, args);
			}
		});
	}

	public static <T> T executeMethod(Method method) {
		try {
			return (T) method.invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			throw runtime(inner(ex));
		}
	}

	public static <T> T executeMethod(Object instance, Method method) {
		try {
			return (T) method.invoke(instance);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			throw runtime(inner(ex));
		}
	}

}
