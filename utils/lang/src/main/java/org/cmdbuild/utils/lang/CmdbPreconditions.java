/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;

public class CmdbPreconditions {

	public static String checkNotBlank(@Nullable String arg) {
		checkNotNull(trimToNull(arg));
		return arg;
	}

	public static long checkGtZero(@Nullable Long n) {
		checkNotNull(n);
		checkArgument(n > 0);
		return n;
	}

	public static <T extends Iterable> T checkNotEmpty(@Nullable T arg) {
		checkNotNull(arg);
		checkArgument(!isEmpty(arg));
		return arg;
	}

	public static <T extends Iterable> T checkNotEmpty(@Nullable T arg, @Nullable String message, @Nullable Object... params) {
		checkNotNull(arg, message, params);
		checkArgument(!isEmpty(arg), message, params);
		return arg;
	}

	public static String checkNotBlank(@Nullable String arg, @Nullable String message) {
		checkNotNull(trimToNull(arg), message);
		return arg;
	}

	public static String checkNotBlank(@Nullable String arg, @Nullable String message, @Nullable Object... params) {
		checkNotNull(trimToNull(arg), message, params);
		return arg;
	}

	public static String trimAndCheckNotBlank(@Nullable String arg) {
		return checkNotNull(trimToNull(arg));
	}

	public static String trimAndCheckNotBlank(@Nullable String arg, @Nullable String message) {
		return checkNotNull(trimToNull(arg), message);
	}

	public static String trimAndCheckNotBlank(@Nullable String arg, @Nullable String message, @Nullable Object... params) {
		return checkNotNull(trimToNull(arg), message, params);
	}

	public static String firstNotBlank(@Nullable String one, String two) {
		return checkNotBlank(firstNotBlankOrNull(one, two));
	}

	public static @Nullable
	String firstNotBlankOrNull(@Nullable String one, String two) {
		if (isBlank(one)) {
			return blankToNull(two);
		} else {
			return one;
		}
	}

	public static @Nullable
	String blankToNull(@Nullable String arg) {
		return isBlank(arg) ? null : arg;
	}

}
