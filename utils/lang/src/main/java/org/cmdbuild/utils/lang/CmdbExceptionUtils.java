/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import static java.lang.String.format;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutionException;
import javax.annotation.Nullable;

public class CmdbExceptionUtils {

	public static RuntimeException cause(Throwable ex) {
		return extractCause(ex);
	}

	public static RuntimeException extractCause(Throwable ex) {
		if (ex.getCause() != null) {
			return toRuntimeException(ex.getCause());
		} else {
			return toRuntimeException(ex);
		}
	}

	public static RuntimeException runtime(Throwable ex) {
		return toRuntimeException(ex);
	}

	public static RuntimeException runtime(Throwable ex, String message, Object... args) {
		return new RuntimeException(format(message, args), ex);
	}

	public static RuntimeException runtime(String message, Object... args) {
		return new RuntimeException(format(message, args));
	}

	public static Exception inner(Exception ex) {
		if ((ex instanceof ExecutionException || ex instanceof InvocationTargetException) && (ex.getCause() != null) && (ex.getCause() instanceof Exception)) {
			ex = (Exception) ex.getCause();
		}
		return ex;
	}

	public static RuntimeException toRuntimeException(Throwable ex) {
		if (ex instanceof RuntimeException) {
			return ((RuntimeException) ex);
		} else {
			return new RuntimeException(ex);
		}
	}

	public static UnsupportedOperationException unsupported(String message, Object... args) {
		return new UnsupportedOperationException(format(message, args));
	}

	public static @Nullable
	<T extends Throwable> T extractExceptionOrNull(Throwable ex, Class<T> type) {
		if (type.isInstance(ex)) {
			return type.cast(ex);
		} else {
			if (ex.getCause() == null) {
				return null;
			} else {
				return extractExceptionOrNull(ex.getCause(), type);
			}
		}
	}
}
