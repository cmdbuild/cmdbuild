/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Ordering;
import com.google.common.collect.Streams;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.Collections;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import java.util.stream.Stream;
import javax.annotation.Nullable;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.getClassOfNullable;

public class CmdbCollectionUtils {

	public static <T> Stream<T> stream(Object obj) {
		checkNotNull(obj);
		if (obj instanceof Iterable) {
			return Streams.stream((Iterable) obj);
		} else if (obj.getClass().isArray()) {
			return convert(obj, List.class).stream();//TODO improve performance
		} else {
			throw unsupported("unsupported conversion of value = %s (%s) to stream", obj, getClassOfNullable(obj));
		}
	}

	public static boolean isNullOrEmpty(@Nullable Collection collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isNullOrEmpty(@Nullable Map map) {
		return map == null || map.isEmpty();
	}

	public static <X, T extends Collection<X>> T nullToEmpty(T collection) {
		if (collection == null) {
			return (T) emptyList();//TODO: will break if T is, for example, Set
		} else {
			return collection;
		}
	}

	public static <X, T extends Set<X>> T nullToEmpty(T set) {
		if (set == null) {
			return (T) emptySet();
		} else {
			return set;
		}
	}

	/**
	 * cast to list; avoid making a copy if possible
	 */
	public static <T> List<T> toList(Iterable<T> iterable) {
		return (List) ((iterable instanceof List) ? ((List) iterable) : list(iterable));
	}

	/**
	 * cast to collection; avoid making a copy if possible
	 */
	public static <T> Collection<T> toCollection(Iterable<T> iterable) {
		return (iterable instanceof Collection) ? ((Collection) iterable) : (Collection) list(iterable);
	}

	/**
	 * always return a copy
	 */
	public static <T> FluentList<T> list(Iterable<T> iterable) {
		return new FluentListImpl<T>().with(iterable);
	}

	public static <T> FluentList<T> listOf(Class<T> classe) {
		return new FluentListImpl<>();
	}

	public static <T> FluentList<T> list(Iterator<T> iterator) {
		return new FluentListImpl<T>().with(iterator);
	}

	public static <T> FluentList<T> list(Stream<T> stream) {
		return new FluentListImpl<T>().accept((l) -> stream.forEach(l::add));
	}

	public static <T> FluentList<T> list(T... items) {
		return new FluentListImpl<T>().with(items);
	}

	public static <T> Queue<T> queue() {
		return new ConcurrentLinkedQueue<>();
	}

	public static <T> Queue<T> queue(Iterable<T> iterable) {
		Queue<T> queue = queue();
		Iterables.addAll(queue, iterable);
		return queue;
	}

	public static <T> Queue<T> queue(T... items) {
		return queue(asList(items));
	}

	public static <T> FluentList<T> list() {
		return new FluentListImpl<>();
	}

	public static <T> FluentList<T> list(Class<T> itemClass) {
		return new FluentListImpl<>();
	}

	public static <T> FluentSet<T> set(Iterable<T> iterable) {
		return new FluentSetImpl<T>().with(iterable);
	}

	public static <T> FluentSet<T> setFromNullable(@Nullable Iterable<T> iterable) {
		return new FluentSetImpl<T>().withNullable(iterable);
	}

	public static <T> FluentSet<T> set(T... items) {
		return new FluentSetImpl<T>().with(items);
	}

	public static <T> FluentSet<T> set() {
		return new FluentSetImpl<>();
	}

	public interface FluentList<T> extends List<T> {

		FluentList<T> with(T entry);

		FluentList<T> with(Iterable<T> entries);

		FluentList<T> with(Iterator<T> entries);

		default FluentList<T> with(Stream<T> stream) {
			stream.forEach(this::add);
			return this;
		}

		default FluentList<T> add(Iterable<T> entries) {
			return this.with(entries);
		}

		FluentList<T> with(T... items);

		FluentList<T> without(Predicate<T> predicate);

		default FluentList<T> add(T... items) {
			return this.with(items);
		}

		default FluentList<T> accept(Consumer<FluentList<T>> consumer) {
			consumer.accept(this);
			return this;
		}

		List<T> immutable();
	}

	public interface FluentSet<T> extends Set<T> {

		FluentSet<T> with(T entry);

		FluentSet<T> with(Iterable<T> entries);

		default FluentSet<T> withNullable(@Nullable Iterable<T> entries) {
			if (entries != null) {
				with(entries);
			}
			return this;
		}

		FluentSet<T> with(T... items);

		FluentSet<T> without(T entry);

		FluentSet<T> without(Iterable<T> entries);

		default FluentSet<T> withOnly(Collection<T> entries) {
			this.retainAll(entries);
			return this;
		}

		FluentSet<T> without(T... items);

		default FluentSet<T> sorted() {
			return this.sorted((Comparator) Ordering.natural());
		}

		FluentSet<T> sorted(Comparator<T> comparator);

		Set<T> immutable();

		default FluentSet<T> accept(Visitor<FluentSet<T>> visitor) {
			visitor.visit(this);
			return this;
		}
	}

	private static class FluentListImpl<T> extends ArrayList<T> implements FluentList<T> {

		public FluentListImpl() {
		}

		@Override
		public FluentList<T> with(Iterable<T> entries) {
			Iterables.addAll(this, entries);
			return this;
		}

		@Override
		public FluentList<T> with(Iterator<T> entries) {
			Iterators.addAll(this, entries);
			return this;
		}

		@Override
		public FluentList<T> with(T entry) {
			add(entry);
			return this;
		}

		@Override
		public FluentList<T> with(T... items) {
			return this.with(asList(items));
		}

		@Override
		public FluentList<T> without(Predicate<T> predicate) {
			removeIf(predicate);
			return this;
		}

		@Override
		public List<T> immutable() {
			return Collections.unmodifiableList(this);
		}

	}

	private static class FluentSetImpl<T> extends LinkedHashSet<T> implements FluentSet<T> {

		public FluentSetImpl() {
		}

		public FluentSetImpl(Collection<? extends T> c) {
			super(c);
		}

		@Override
		public FluentSet<T> with(T entry) {
			add(entry);
			return this;
		}

		@Override
		public FluentSet<T> with(Iterable<T> entries) {
			Iterables.addAll(this, entries);
			return this;
		}

		@Override
		public FluentSet<T> with(T... items) {
			return this.with(asList(items));
		}

		@Override
		public FluentSet<T> without(T entry) {
			remove(entry);
			return this;
		}

		@Override
		public FluentSet<T> without(Iterable<T> entries) {
			entries.forEach((e) -> remove(e));
			return this;
		}

		@Override
		public FluentSet<T> without(T... items) {
			return this.without(asList(items));
		}

		@Override
		public FluentSet<T> sorted(Comparator<T> comparator) {
			List<T> content = list(this);
			content.sort(comparator);
			clear();
			addAll(content);
			return this;
		}

		@Override
		public Set<T> immutable() {
			return Collections.unmodifiableSet(this);
		}

	}
}
