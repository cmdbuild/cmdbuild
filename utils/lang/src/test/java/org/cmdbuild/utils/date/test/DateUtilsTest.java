/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.date.test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org.cmdbuild.utils.date.DateUtils;
import static org.cmdbuild.utils.date.DateUtils.toDateTime;
import static org.cmdbuild.utils.date.DateUtils.toJavaDate;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class DateUtilsTest {

	@Test
	public void testLocalTimeToIsoTime1() {
		LocalTime localTime = LocalTime.of(12, 13, 14);

		String stringTime = DateUtils.toIsoTime(localTime);

		assertEquals("12:13:14", stringTime);
	}

	@Test
	public void testLocalTimeToIsoTime2() {
		LocalTime localTime = LocalTime.of(00, 13, 14);

		String stringTime = DateUtils.toIsoTime(localTime);

		assertEquals("00:13:14", stringTime);
	}

	@Test
	public void testLocalTimeToIsoTime3() {
		LocalTime localTime = LocalTime.of(23, 59, 59);

		String stringTime = DateUtils.toIsoTime(localTime);

		assertEquals("23:59:59", stringTime);
	}

	@Test
	public void testLocalTimeToIsoTime4() {
		LocalTime localTime = LocalTime.of(0, 0, 0);

		String stringTime = DateUtils.toIsoTime(localTime);

		assertEquals("00:00:00", stringTime);
	}

	@Test
	public void testLocalDateToIsoDate() {
		LocalDate localDate = LocalDate.of(2018, 9, 12);

		String stringDate = DateUtils.toIsoDate(localDate);

		assertEquals("2018-09-12", stringDate);
	}

	@Test
	public void testLocalDateConversion1() {
		LocalDate localDate = DateUtils.toDate("2018-09-12");

		String stringDate = DateUtils.toIsoDate(localDate);

		assertEquals("2018-09-12", stringDate);
	}

	@Test
	public void testLocalDateConversion2() {
		java.sql.Date sqlDate = new java.sql.Date(2018 - 1900, 8, 12);

		LocalDate localDate = DateUtils.toDate(sqlDate);
		assertEquals(2018, localDate.getYear());
		assertEquals(9, localDate.getMonthValue());
		assertEquals(12, localDate.getDayOfMonth());

		String stringDate = DateUtils.toIsoDate(localDate);

		assertEquals("2018-09-12", stringDate);
	}

	@Test
	public void testLocalDateParsinge() {
		LocalDate localDate = DateUtils.toDate("26/04/2018");

		String stringDate = DateUtils.toIsoDate(localDate);

		assertEquals("2018-04-26", stringDate);
	}

	@Test
	public void testLocalDateToJavaDate1() {
		LocalDate localDate = LocalDate.of(2018, 9, 12);

		Date date = DateUtils.toJavaDate(localDate);

		assertEquals(2018 - 1900, date.getYear());
		assertEquals(8, date.getMonth());
		assertEquals(12, date.getDate());
	}

	@Test
	public void testLocalDateToJavaDate2() {
		LocalDate localDate = LocalDate.of(2018, 9, 12);

		Date date = DateUtils.toJavaDate(localDate);

		localDate = DateUtils.toDate(date);

		String stringDate = DateUtils.toIsoDate(localDate);

		assertEquals("2018-09-12", stringDate);
	}

	@Test
	public void testStringToJavaDate1() {
		String in = "2018-04-26T11:23:57.589Z";

		Date javaDate = toJavaDate(in);

		assertEquals(1524741837589l, javaDate.getTime());
	}

	@Test
	public void testStringToJavaDate2() {
		String in = "2018-04-26T12:28:52.291Z";

		Date javaDate = toJavaDate(in);

		assertEquals(1524745732291l, javaDate.getTime());
	}

	@Test
	public void testStringToJavaDate3() {
		String in = "2018-04-26";

		Date javaDate = toJavaDate(in);

		assertEquals(1524700800000l, javaDate.getTime());
	}

	@Test
	public void testLocalTimeToJavaDate() {
		LocalTime localTime = LocalTime.of(12, 13, 14);

		Date javaDate = toJavaDate(localTime);

		assertEquals(43994000l, javaDate.getTime());

		localTime = DateUtils.toTime(javaDate);

		assertEquals(LocalTime.of(12, 13, 14), localTime);

		String string = DateUtils.toIsoTime(localTime);

		assertEquals("12:13:14", string);

		localTime = DateUtils.toTime(string);

		assertEquals(LocalTime.of(12, 13, 14), localTime);
	}

	@Test
	public void testGregorianCalendarToZonedDateTime() {
		Calendar source = GregorianCalendar.getInstance();
		source.setTimeZone(TimeZone.getTimeZone(ZoneOffset.UTC));
		source.set(Calendar.YEAR, 1980);
		source.set(Calendar.MONTH, 1);
		source.set(Calendar.DAY_OF_MONTH, 24);
		source.set(Calendar.HOUR_OF_DAY, 14);
		source.set(Calendar.MINUTE, 37);
		source.set(Calendar.SECOND, 44);

		ZonedDateTime dateTime = toDateTime(source).withZoneSameInstant(ZoneOffset.UTC);

		assertEquals(1980, dateTime.getYear());
		assertEquals(2, dateTime.getMonth().getValue());
		assertEquals(24, dateTime.getDayOfMonth());
		assertEquals(14, dateTime.getHour());
		assertEquals(37, dateTime.getMinute());
		assertEquals(44, dateTime.getSecond());
	}

	@Test
	public void testGregorianCalendarToZonedDateTimeWithZone() {
		Calendar source = GregorianCalendar.getInstance();
		source.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		source.set(Calendar.YEAR, 1980);
		source.set(Calendar.MONTH, 1);
		source.set(Calendar.DAY_OF_MONTH, 24);
		source.set(Calendar.HOUR_OF_DAY, 14);
		source.set(Calendar.MINUTE, 37);
		source.set(Calendar.SECOND, 44);

		ZonedDateTime dateTime = toDateTime(source).withZoneSameInstant(ZoneId.of("Europe/Rome"));

		assertEquals(1980, dateTime.getYear());
		assertEquals(2, dateTime.getMonth().getValue());
		assertEquals(24, dateTime.getDayOfMonth());
		assertEquals(14, dateTime.getHour());
		assertEquals(37, dateTime.getMinute());
		assertEquals(44, dateTime.getSecond());
	}
}
