/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.lang;

import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CmdbStringUtilsTest {

	@Test
	public void testCmdbStringUtilsAbbreviate1() {
		String orig = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
		String res = abbreviate(orig, 100);
		assertEquals(orig, res);
	}

	@Test
	public void testCmdbStringUtilsAbbreviate2() {
		String orig = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
		String res = abbreviate(orig, 99);
		assertEquals(99, res.length());
		assertEquals("123456789012345678901234567890123456789012345678901234567890123456789012345678901234... (100 chars)", res);
	}

	@Test
	public void testCmdbStringUtilsAbbreviate3() {
		String orig = "asd\n\rpppp\nasdasd";
		String res = abbreviate(orig, 99);
		assertEquals(orig.length() - 1, res.length());
		assertEquals("asd pppp asdasd", res);
	}

	@Test
	public void testCmdbStringUtilsAbbreviate4() {
		String orig = "123";
		String res = abbreviate(orig, 100);
		assertEquals(orig, res);
	}

	@Test
	public void testCmdbStringUtilsAbbreviate5() {
		String orig = null;
		String res = abbreviate(orig, 100);
		assertEquals(orig, res);
	}
}
