package org.cmdbuild.utils.crypto.test;

import org.cmdbuild.utils.crypto.CmdbuildCryptoUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CryptoUtilsTest {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testKeyId() throws Exception {
		logger.info("testKeyId BEGIN");
		assertEquals("e47f9eda18a855f9", CmdbuildCryptoUtils.defaultUtils().getKeyId());
		logger.info("testKeyId END");
	}

	@Test
	public void testEncryptionAndDecryption() throws Exception {
		logger.info("testEncryptionAndDecryption BEGIN");
		String value = "this is a param value to be encrypted";

		String encryptedValue = CmdbuildCryptoUtils.defaultUtils().encryptValue(value);
		assertNotEquals(encryptedValue, value);

		String decryptedValue = CmdbuildCryptoUtils.defaultUtils().decryptValue(encryptedValue);
		assertEquals(decryptedValue, value);
		logger.info("testEncryptionAndDecryption END");
	}

	@Test
	public void testEncryptionAndDecryption1() throws Exception {
		logger.info("testEncryptionAndDecryption1 BEGIN");
		String value = "p";

		String encryptedValue = CmdbuildCryptoUtils.defaultUtils().encryptValue(value);
		assertNotEquals(encryptedValue, value);

		String decryptedValue = CmdbuildCryptoUtils.defaultUtils().decryptValue(encryptedValue);
		assertEquals(decryptedValue, value);
		logger.info("testEncryptionAndDecryption1 END");
	}

	@Test
	public void testEncryptionAndDecryption3() throws Exception {
		logger.info("testEncryptionAndDecryption3 BEGIN");
		CmdbuildCryptoUtils cryptoUtils = CmdbuildCryptoUtils.defaultUtils();
		{
			String value = "passwordOne";
			String encryptedValue = cryptoUtils.encryptValue(value);
			assertNotEquals(encryptedValue, value);

			String decryptedValue = cryptoUtils.decryptValue(encryptedValue);
			assertEquals(decryptedValue, value);
		}
		{
			String value = "passwordTwo";
			String encryptedValue = cryptoUtils.encryptValue(value);
			assertNotEquals(encryptedValue, value);

			String decryptedValue = cryptoUtils.decryptValue(encryptedValue);
			assertEquals(decryptedValue, value);
		}
		logger.info("testEncryptionAndDecryption3 END");
	}

	@Test
	public void testEncryptionAndDecryption2() throws Exception {
		logger.info("testEncryptionAndDecryption2 BEGIN");
		String value = "this is a param value to be encrypted";

		String encryptedValue = CmdbuildCryptoUtils.defaultUtils().encryptValue(value);
		assertNotEquals(encryptedValue, value);

		String decryptedValue = CmdbuildCryptoUtils.defaultUtils().decryptValue(encryptedValue);
		assertEquals(decryptedValue, value);
		logger.info("testEncryptionAndDecryption2 END");
	}

	@Test
	public void testHandleEmptyValues1() throws Exception {
		logger.info("testHandleEmptyValues1 BEGIN");
		String value = "  ";

		String encryptedValue = CmdbuildCryptoUtils.defaultUtils().encryptValue(value);
		assertEquals(encryptedValue, value);

		String decryptedValue = CmdbuildCryptoUtils.defaultUtils().decryptValue(encryptedValue);
		assertEquals(decryptedValue, value);
		logger.info("testHandleEmptyValues1 END");
	}

	@Test
	public void testHandleEmptyValues2() throws Exception {
		logger.info("testHandleEmptyValues2 BEGIN");
		String value = "";

		String encryptedValue = CmdbuildCryptoUtils.defaultUtils().encryptValue(value);
		assertEquals(encryptedValue, value);

		String decryptedValue = CmdbuildCryptoUtils.defaultUtils().decryptValue(encryptedValue);
		assertEquals(decryptedValue, value);
		logger.info("testHandleEmptyValues2 END");
	}

	@Test
	public void testHandleEmptyValues3() throws Exception {
		logger.info("testHandleEmptyValues3 BEGIN");
		String value = null;

		String encryptedValue = CmdbuildCryptoUtils.defaultUtils().encryptValue(value);
		assertEquals(encryptedValue, value);

		String decryptedValue = CmdbuildCryptoUtils.defaultUtils().decryptValue(encryptedValue);
		assertEquals(decryptedValue, value);
		logger.info("testHandleEmptyValues3 END");
	}

}
