package org.cmdbuild.utils.crypto.test;

import static org.junit.Assert.assertEquals;

import org.cmdbuild.utils.crypto.legacy.digest.LegacyPasswordUtils;
import org.junit.Test;

public class LegacyPasswordUtilsTest {

	@Test
	public void test1() {
		String enc = LegacyPasswordUtils.encrypt("something");
		String dec = LegacyPasswordUtils.decrypt(enc);
		assertEquals("something", dec);
	}

	@Test
	public void test2() {
		String dec = LegacyPasswordUtils.decrypt("Sh7JgSrZ4JwCsVBZBZrAsQ==");
		assertEquals("chiara01", dec);
	}

}
