/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.utils.crypto;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.getLast;
import com.google.common.collect.Lists;
import static com.google.common.collect.Lists.newArrayList;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.SequenceInputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.annotation.Nullable;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.DecoderException;
import static org.apache.commons.codec.binary.Hex.decodeHex;
import static org.apache.commons.codec.binary.Hex.encodeHexString;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.DigestUtils.sha256;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import org.apache.commons.io.Charsets;
import static org.apache.commons.io.IOUtils.toByteArray;
import org.apache.commons.lang3.ArrayUtils;
import static org.apache.commons.lang3.StringUtils.abbreviate;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trim;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility class to encrypt data (for example password in config file). Note
 * that instances of this class are <b>not</b> thread safe, but can be reused
 * for many operations within the same thread.
 * <br><br>
 * common use cases will use {@link CmdbuildCryptoUtils defaultUtils} factory
 * method to get an instance of {@link CmdbuildCryptoUtils} with the default
 * configuration.
 *
 * @author davide
 */
public class CmdbuildCryptoUtils {

	private final static String MAGIC = encodeHexString(sha256("CMDBUILD_ENCRYPTED_VALUE_V1")).substring(0, 6);
	private final static List<Integer> MAGIC_POS = ImmutableList.of(7, 13, 19, 21, 24, 27);//random, hardcoded; index must be less than min string size (for aes 128, block size in 16 bytes, or 32 hex chars)	
	private final static int SALT_LEN = 4; // a short salt will strongly enhance password security, while maintaining the final string length quite short

	static {
		checkArgument(MAGIC.length() == MAGIC_POS.size());
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Key key;
	private final IvParameterSpec iv;
	private final Cipher cipher;
	private final String keyId;

	protected CmdbuildCryptoUtils(byte[] source) throws NoSuchAlgorithmException, NoSuchPaddingException {
		try {
			source = sha256(source);
			logger.debug("creating crypto utils from source = {}", abbreviate(encodeHexString(source), 10));
			byte[] keyBytes = Arrays.copyOfRange(source, 1, 17),
					ivBytes = Arrays.copyOfRange(source, 15, 31);
			keyId = sha256Hex(new SequenceInputStream(new ByteArrayInputStream(keyBytes), new ByteArrayInputStream(ivBytes))).substring(0, 16);
			logger.debug("crypto utils key id = {}", keyId);
			key = new SecretKeySpec(keyBytes, "AES");
			iv = new IvParameterSpec(ivBytes);
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			logger.debug("crypto utils ready");
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * encrypt supplied value; if value is empty or null, return original value;
	 * otherwise, the supplied value is salted, encrypted and marked with magic
	 * bytes. Returned string is hex encoded and can be stored as plaintext.
	 *
	 * @param value
	 * @return
	 */
	public @Nullable
	String encryptValue(@Nullable String value) {
		logger.trace("encrypting value = {}", value);
		String encryptedValue = isBlank(value) ? value : embedMagic(encodeHexString(encrypt(ArrayUtils.addAll(createSalt(), value.getBytes(Charsets.UTF_8)))));
		logger.trace("encrypted value = {} with result = {}", value, encryptedValue);
		return encryptedValue;
	}

	/**
	 * decrypt supplied value, if is encrypted (otherwise return plain value).
	 * <br>
	 * This method will use embedded magic bytes to identify if supplied string
	 * was encrypted with {@link encryptValue}.
	 *
	 * @param value
	 * @return decripted value, or original value if not encrypted
	 */
	public @Nullable
	String decryptValue(@Nullable String value) {
		logger.trace("decrypting value = {}", value);
		if (isBlank(value)) {
			logger.trace("value is blank, no decryption necessary");
			return value;
		} else if (!hasMagic(trim(value))) {
			logger.trace("value is cleartext, no decryption necessary");
			return value;
		} else {
			try {
				while (hasMagic(trim(value))) {// we handle recursive encryption (ie we handle values that have been encrypted multiple times)
					byte[] data = decrypt(decodeHex(stripMagic(trim(value)).toCharArray()));
					data = Arrays.copyOfRange(data, SALT_LEN, data.length);
					String decryptedValue = new String(data, Charsets.UTF_8);
					logger.trace("decrypted value = {} with result = {}", value, decryptedValue);
					value = decryptedValue;
				}
				return value;
			} catch (DecoderException ex) {
				throw new RuntimeException(ex);
			}
		}
	}

	/**
	 * encrypt supplied data (plain encrypt, no special features implemented).
	 *
	 * @param data
	 * @return
	 */
	public byte[] encrypt(byte[] data) {
		try {
			checkNotNull(data);
			cipher.init(Cipher.ENCRYPT_MODE, key, iv);
			return cipher.doFinal(data);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * decrypt supplied data (plain decrypt, no special features implemented).
	 *
	 * @param data
	 * @return
	 */
	public byte[] decrypt(byte[] data) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, key, iv);
			return cipher.doFinal(data);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
			throw new RuntimeException(ex);
		}
	}

	private byte[] createSalt() {
		return Arrays.copyOfRange(DigestUtils.sha256(UUID.randomUUID().toString()), 0, SALT_LEN);
	}

	/**
	 * return true if str contains an encrypted value (ie a value that has been
	 * generated with this class {@link encryptValue} method).
	 *
	 * @param str
	 * @return
	 */
	public boolean isEncrypted(@Nullable String str) {
		return hasMagic(str);
	}

	private boolean hasMagic(@Nullable String str) {
		if (isBlank(str) || str.length() <= getLast(MAGIC_POS)) {
			return false;
		} else {
			for (int i = 0; i < MAGIC.length(); i++) {
				char c = MAGIC.charAt(i);
				int pos = MAGIC_POS.get(i);
				if (pos > str.length() || c != str.charAt(pos)) {
					return false;
				}
			}
			return true;
		}
	}

	/**
	 * assume string is non-null and has embedded magic
	 *
	 * @param str
	 * @return str with stripped magic
	 */
	private String stripMagic(String str) {
		StringBuilder sb = new StringBuilder();
		int i, j = 0;
		Iterator<Integer> iterator = MAGIC_POS.iterator();
		while (iterator.hasNext()) {
			i = j;
			j = iterator.next();
			sb.append(str.substring(i, j));
			j++;
		}
		sb.append(str.substring(j));
		String res = sb.toString();
		checkArgument(res.length() == str.length() - MAGIC.length());
		return res;
	}

	/**
	 * embed magic in supplied string
	 *
	 * @param str
	 * @return
	 */
	private String embedMagic(String str) {
		checkArgument(str.length() > getLast(MAGIC_POS));
		StringBuilder sb = new StringBuilder();
		int i, j = 0, k = 0;
		Iterator<Integer> iterator = MAGIC_POS.iterator();
		Iterator<String> chars = Splitter.fixedLength(1).split(MAGIC).iterator();
		while (iterator.hasNext()) {
			i = j;
			j = iterator.next() - k;
			k++;
			sb.append(str.substring(i, j));
			sb.append(chars.next());
		}
		sb.append(str.substring(j));
		String res = sb.toString();
		checkArgument(res.length() == str.length() + MAGIC.length());
		checkArgument(hasMagic(res));
		return res;
	}

	/**
	 *
	 * @return id of the key wrapped in this crypto utils; compatible utils
	 * instances will have the same key id
	 */
	public String getKeyId() {
		return keyId;
	}

	/**
	 * return default cmdbuild crypto utils instance
	 *
	 * @return
	 */
	public static CmdbuildCryptoUtils defaultUtils() {
		try {
			return new CmdbuildCryptoUtils(toByteArray(firstNonNull(CmdbuildCryptoUtils.class.getResourceAsStream(new String(ArrayUtils.toPrimitive((Byte[]) Lists.reverse(newArrayList(concat(asList((byte) 0x74, (byte) 0x78), asList((byte) 0x74, (byte) 0x2e, (byte) 0x45, (byte) 0x4d, (byte) 0x44), asList((byte) 0x41, (byte) 0x45, (byte) 0x52), asList((byte) 0x2f, (byte) 0x6f, (byte) 0x74, (byte) 0x70, (byte) 0x79, (byte) 0x72, (byte) 0x63, (byte) 0x2f, (byte) 0x73, (byte) 0x6c, (byte) 0x69, (byte) 0x74, (byte) 0x75, (byte) 0x2f, (byte) 0x64, (byte) 0x6c, (byte) 0x69, (byte) 0x75, (byte) 0x62, (byte) 0x64, (byte) 0x6d, (byte) 0x63, (byte) 0x2f, (byte) 0x67, (byte) 0x72, (byte) 0x6f, (byte) 0x2f)))).toArray(new Byte[]{})), Charsets.UTF_8)), CmdbuildCryptoUtils.class.getClassLoader().getResourceAsStream(new String(ArrayUtils.toPrimitive((Byte[]) Lists.reverse(newArrayList(concat(asList((byte) 0x74, (byte) 0x78), asList((byte) 0x74, (byte) 0x2e, (byte) 0x45, (byte) 0x4d, (byte) 0x44), asList((byte) 0x41, (byte) 0x45, (byte) 0x52), asList((byte) 0x2f, (byte) 0x6f, (byte) 0x74, (byte) 0x70, (byte) 0x79, (byte) 0x72, (byte) 0x63, (byte) 0x2f, (byte) 0x73, (byte) 0x6c, (byte) 0x69, (byte) 0x74, (byte) 0x75, (byte) 0x2f, (byte) 0x64, (byte) 0x6c, (byte) 0x69, (byte) 0x75, (byte) 0x62, (byte) 0x64, (byte) 0x6d, (byte) 0x63, (byte) 0x2f, (byte) 0x67, (byte) 0x72, (byte) 0x6f, (byte) 0x2f)))).toArray(new Byte[]{})), Charsets.UTF_8)))));
		} catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
			throw new RuntimeException(ex);
		}
	}

}
