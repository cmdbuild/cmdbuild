package org.cmdbuild.gis;

import java.util.Set;

public interface GeoserverLayer extends GisLayer {

	String getDescription();

	String getType();

	String getGeoserverName();

	String getOwnerClassId();

	long getOwnerCardId();

}
