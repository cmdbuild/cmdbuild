package org.cmdbuild.gis;

import org.cmdbuild.navtree.NavTreeNode;
import org.cmdbuild.navtree.DomainTreeRepository;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.notNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.config.GisConfiguration;
import org.cmdbuild.dao.driver.postgres.relationquery.RelationInfo;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.navtree.DomainTreeCardNode;
import org.cmdbuild.navtree.DomainTreeNode;
import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.Subscribe;
import java.util.Collection;
import static java.util.stream.Collectors.toList;
import org.apache.commons.lang3.tuple.Pair;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.config.api.ConfigReloadEvent;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.driver.postgres.relationquery.DomainWithSource;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.startup.PostStartup;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.gis.geoserver.GeoserverService;
import org.cmdbuild.dao.entrytype.Domain;
import static org.cmdbuild.gis.utils.GisUtils.cmGeometryToPostgisSql;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_DISABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_ERROR;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;

@Component("gisService")
public class GisServiceImpl implements GisService, SystemService {

	private static final String DOMAIN_TREE_TYPE = "gisnavigation";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;
	private final GisAttributeRepository gisAttributeRepository;
	private final GisValueRepository geometriesRepository;
	private final DomainTreeRepository domainTreeStore;
	private final GisConfiguration configuration;
	private final GeoserverService geoServerService;
	private final GeoserverLayerRepository layerRepository;

	public GisServiceImpl(DomainTreeRepository domainTreeRepository, GeoserverLayerRepository layerRepository, DaoService dao, GisValueRepository geoFeatureStore, GisConfiguration configuration, GeoserverService geoServerService, GisAttributeRepository layerStore) {
		this.dao = checkNotNull(dao);
		this.gisAttributeRepository = checkNotNull(layerStore);
		this.domainTreeStore = checkNotNull(domainTreeRepository);
		this.geometriesRepository = checkNotNull(geoFeatureStore);
		this.configuration = checkNotNull(configuration);
		this.geoServerService = checkNotNull(geoServerService);
		this.layerRepository = checkNotNull(layerRepository);
		this.configuration.getEventBus().register(new Object() {

			@Subscribe
			public void handleConfigUpdateEvent(ConfigReloadEvent event) {
				checkGisSchemaSafe();
			}

		});
	}

	@Override
	public String getServiceName() {
		return "GIS Service";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (!isGisEnabled()) {
			return SS_DISABLED;
		} else {
			if (geometriesRepository.isGisSchemaOk()) {
				return SS_READY;
			} else {
				return SS_ERROR;
			}
		}
	}

	@Override
	public boolean canControlState() {
		return false;
	}

	@PostStartup
	public void init() {
		checkGisSchemaSafe();
	}

	@Override
	public boolean isGisEnabled() {
		return configuration.isEnabled();
	}

	private void checkGisSchemaSafe() {
		if (isGisEnabled()) {
			logger.debug("checkGisSchema");
			try {
				geometriesRepository.checkGisSchemaAndCreateIfMissing();
			} catch (Exception ex) {
				logger.error(marker(), "error checking gis schema", ex);
			}
		}
	}

	@Override
	@Transactional
	public GisAttribute createGeoAttribute(GisAttribute layer) {
		checkGisEnabled();
		geometriesRepository.createGeoTable(layer);
		return gisAttributeRepository.createLayer(layer);
	}

	@Override
	public GisAttribute updateGeoAttribute(GisAttribute layer) {
		checkGisEnabled();
		return gisAttributeRepository.updateLayer(layer);
	}

	@Override
	@Transactional
	public void deleteGeoAttribute(String masterTableName, String attributeName) {
		checkGisEnabled();
		Classe classe = dao.getClasse(masterTableName);
		checkNotBlank(attributeName);
		gisAttributeRepository.deleteLayer(classe.getName(), attributeName);
		geometriesRepository.deleteGeoTable(masterTableName, attributeName);
	}

	@Override
	public List<GisValue> getValues(String classId, Long cardId) {
		checkGisEnabled();
		Classe classe = dao.getClasse(classId);
		return gisAttributeRepository.getLayersByOwnerClass(classe.getName()).stream().map((l) -> geometriesRepository.getGeoFeatureOrNull(l, cardId)).filter(notNull()).collect(toList());//TODO avoid n*m query
	}

	@Override
	public List<GisValue> getGeoValues(Collection<Long> layers, String bbox) {
		checkGisEnabled();
		return geometriesRepository.getGeoValues(layers, bbox);
	}

	@Override
	public Pair<List<GisValue>, List<NavTreeNode>> getGeoValuesAndNavTree(Collection<Long> attrs, String bbox) {
		checkGisEnabled();
		DomainTreeNode navTreeDomains = getGisTreeNavigation();
		return geometriesRepository.getGeoValuesAndNavTree(attrs, bbox, navTreeDomains);
	}

	@Override
	@Deprecated
	public void updateValues(String classId, Long cardId, Map<String, Object> attributes) {

		checkGisEnabled();

		String geoAttributesJsonString = (String) attributes.get("geoAttributes");
		if (geoAttributesJsonString != null) {
			try {
				JSONObject geoAttributesObject = new JSONObject(geoAttributesJsonString);
				String[] geoAttributesName = JSONObject.getNames(geoAttributesObject);
				Classe masterTable = dao.getClasse(classId);

				if (geoAttributesName != null) {
					for (String name : geoAttributesName) {
						GisAttribute layerMetaData = gisAttributeRepository.getLayer(masterTable.getName(), name);
						String value = geoAttributesObject.getString(name);

						GisValue geoFeature = geometriesRepository.getGeoFeatureOrNull(layerMetaData, cardId);

						if (geoFeature == null) {
							// the feature does not exists
							// create it
							if (value != null && !value.trim().isEmpty()) {
								geometriesRepository.createGeoValue(layerMetaData, value, cardId);
							}
						} else {
							if (value != null && !value.trim().isEmpty()) {
								// there is a non empty value
								// update the geometry
								geometriesRepository.updateGeoValue(layerMetaData, value, cardId);
							} else {
								// the new value is blank, so delete the feature
								geometriesRepository.deleteGeoValue(layerMetaData, cardId);
							}
						}
					}
				}
			} catch (JSONException ex) {
				throw new GisException(ex);
			}
		}
	}

	@Override
	public GisValue setValue(GisValue value) {
		GisAttribute attribute = gisAttributeRepository.getLayer(value.getOwnerClassId(), value.getLayerName());
		String rawGeometryValue = cmGeometryToPostgisSql(value.getGeometry());
		boolean hasValue = getValueOrNull(value.getOwnerClassId(), value.getOwnerCardId(), value.getLayerName()) != null;
		if (hasValue) {
			geometriesRepository.updateGeoValue(attribute, rawGeometryValue, value.getOwnerCardId());
		} else {
			geometriesRepository.createGeoValue(attribute, rawGeometryValue, value.getOwnerCardId());
		}
		return getValue(value.getOwnerClassId(), value.getOwnerCardId(), value.getLayerName());
	}

	@Override
	public void deleteValue(String classId, long cardId, String attrId) {
		GisAttribute attribute = gisAttributeRepository.getLayer(classId, attrId);
		geometriesRepository.deleteGeoValue(attribute, cardId);
	}

	@Override
	public void createGeoServerLayer(GeoserverLayer layer, FileItem file) {
		try {
			checkGisEnabled();
			ensureGeoServerIsEnabled();

			String geoServerLayerName = geoServerService.createStoreAndLayer(layer, file.getInputStream());

//			String fullName = String.format(GEO_TABLE_NAME_FORMAT, GEOSERVER, layer.getLayerName());
//			layerRepository
			layer = GeoserverLayerImpl.copyOf(layer)
					//					.withTableName(fullName)
					.withGeoserverName(geoServerLayerName)
					.build();

			layerRepository.create(layer);
		} catch (IOException ex) {
			throw new GisException(ex);
		}
	}

//	@Override
//	@Transactional
//	public void modifyGeoServerLayer(Layer layer, FileItem file) {
////		String name, String description, int maximumZoom, int minimumZoom Set<String> cardBinding
//
//		ensureGisIsEnabled();
//		ensureGeoServerIsEnabled();
//
//		Layer layerMetadata = layerMetadataStore.modifyLayerMetadata(GEOSERVER, name, description, minimumZoom, maximumZoom, null, cardBinding);
//
//		if (file != null && file.getSize() > 0) {
//			try {
//				geoServerService.modifyStoreData(layerMetadata, file.getInputStream());
//			} catch (IOException ex) {
//				throw new GisException(ex);
//			}
//		}
//	}
	@Override
	@Transactional
	public void deleteGeoServerLayer(String name) {
		checkGisEnabled();
		ensureGeoServerIsEnabled();

//		String fullName = String.format(GEO_TABLE_NAME_FORMAT, GEOSERVER, name);
//		GisAttribute layer = gisAttributeRepository.getLayer(fullName);
		GeoserverLayer layer = layerRepository.get(name);
		geoServerService.deleteStoreAndLayers(layer);
		layerRepository.delete(name);
	}

	@Override
	@Transactional
	public List<GeoserverLayer> getGeoServerLayers() {
		checkGisEnabled();
		return layerRepository.getAll();
//		return gisAttributeRepository.getLayersByOwnerClass(GEOSERVER);
	}

	@Override
	public List<GeoserverLayer> getGeoServerLayersForCard(String classId, Long cardId) {
		checkGisEnabled();
		return layerRepository.getForCard(dao.getClasse(classId), cardId);
	}

	@Override
	public List<GeoserverLayer> getGeoLayersVisibleFromClass(String classId) {
		checkGisEnabled();
		return layerRepository.getVisibleFromClass(dao.getClasse(classId));
	}

	@Override
	public List<GeoserverLayer> getGeoLayersOwnedByClass(String classId) {
		checkGisEnabled();
		return layerRepository.getOwnedByClass(dao.getClasse(classId));
	}

//	@Override
//	public Map<String, ClassMapping> getGeoServerLayerMapping() {
//		List<GisAttribute> geoServerLayers = getGeoServerLayers();
//		Map<String, ClassMapping> mapping = Maps.newHashMap();
//
//		for (GisAttribute layer : geoServerLayers) {
//			for (String bindedCard : layer.getCardsBinding()) {
//
//				// A cardInfo is ClassName_CardId
//				String[] cardInfo = bindedCard.split("_");
//				String className = cardInfo[0];
//				String cardId = cardInfo[1];
//
//				ClassMapping classMapping;
//				if (mapping.containsKey(className)) {
//					classMapping = mapping.get(className);
//				} else {
//					classMapping = new DefaultClassMapping();
//					mapping.put(className, classMapping);
//				}
//
//				classMapping.addCardMapping(cardId, new DefaultCardMapping(layer.getLayerName(), layer.getDescription()));
//			}
//		}
//
//		return mapping;
//	}

	/* Common layers methods */
	@Override
	public List<GisAttribute> getAllLayers() {
		checkGisEnabled();
		return gisAttributeRepository.getAllLayers();
	}

	@Override
	public GisAttribute getLayerByClassAndName(String classId, String attributeId) {
		checkGisEnabled();
		Classe classe = dao.getClasse(classId);
		return gisAttributeRepository.getLayer(classe.getName(), attributeId);
	}

	@Override
	public List<GisAttribute> getGeoAttributeByOwnerClass(String classId) {
		checkGisEnabled();
		Classe classe = dao.getClasse(classId);
		return gisAttributeRepository.getLayersByOwnerClass(classe.getName());
	}

	@Override
	public List<GisAttribute> getVisibleGeoAttributesForClass(String classId) {
		checkGisEnabled();
		return gisAttributeRepository.getVisibleLayersForClass(classId);
	}

//	@Override
//	public void setLayerVisisbility(String layerFullName, String visibleTable, boolean visible) {
//
//		ensureGisIsEnabled();
//		layerMetadataStore.updateLayerVisibility(layerFullName, visibleTable, visible);
//	}
//	@Override
//	@Transactional
//	public void reorderLayers(int oldIndex, int newIndex) {
//		ensureGisIsEnabled();
//
//		OrderingUtils.alterPosition(getAllLayers(), oldIndex, newIndex, new PositionHandler<Layer>() {
//			@Override
//			public int getPosition(Layer l) {
//				return l.getIndex();
//			}
//
//			@Override
//			public void setPosition(Layer l, int p) {
//				layerMetadataStore.setLayerIndex(l, p);
//			}
//		});
//	}

	/* DomainTreeNavigation */
//	@Override
//	public void saveGisTreeNavigation(DomainTreeNode root) {
//		domainTreeStore.createOrReplaceTree(DOMAIN_TREE_TYPE, null, root);
//	}
	@Override
	public void removeGisTreeNavigation() {
		domainTreeStore.removeTree(DOMAIN_TREE_TYPE);
	}

	@Override
	public DomainTreeNode getGisTreeNavigation() {
		return domainTreeStore.getDomainTree(DOMAIN_TREE_TYPE);
	}

	@Override
	public DomainTreeCardNode expandDomainTree(DataAccessService dataAccesslogic) {
		Map<String, Long> domainIds = getDomainIds(dataAccesslogic);
		Map<Long, DomainTreeCardNode> nodes = new HashMap<>();

		DomainTreeNode root = this.getGisTreeNavigation();
		DomainTreeCardNode rootCardNode = new DomainTreeCardNode();

		if (root != null) {
			rootCardNode.setText(root.getTargetClassDescription());
			rootCardNode.setExpanded(true);
			rootCardNode.setLeaf(false);

			nodes.put(rootCardNode.getCardId(), rootCardNode);

			PagedElements<Card> domainTreeCards = dataAccesslogic.fetchCards(//
					root.getTargetClassName(), QueryOptionsImpl.builder().build() //
			);

			for (Card card : domainTreeCards) {
				DomainTreeCardNode node = new DomainTreeCardNode();
				node.setText(card.get("Description", String.class));
				node.setClassName(card.getClassName());
				node.setClassId(card.getType().getId());
				node.setCardId(card.getId());
				node.setLeaf(false);

				rootCardNode.addChild(node);
				nodes.put(node.getCardId(), node);
			}

			fetchRelationsByDomain(dataAccesslogic, domainIds, root, nodes);
			rootCardNode.sortByText();
			setDefaultCheck(nodes);
		}

		return rootCardNode;
	}

	/*
	 * the default check is that: identify the base nodes, AKA the nodes created
	 * expanding the base domain this nodes represents the base level, and we
	 * want that only the first child of a siblings group was checked. Also all
	 * the ancestors of this node must be checked
	 */
	private void setDefaultCheck(Map<Long, DomainTreeCardNode> nodes) {
		Map<Object, DomainTreeCardNode> visitedNodes = new HashMap<>();

		for (DomainTreeCardNode node : nodes.values()) {
			if (node.isBaseNode()) {
				DomainTreeCardNode parent = node.parent();
				if (parent != null && !visitedNodes.containsKey(parent.getCardId())) {

					parent.getChildren().get(0).setChecked(true, true, true);
					visitedNodes.put(parent.getCardId(), parent);
				}
			}
		}
	}

	private void fetchRelationsByDomain(DataAccessService dataAccesslogic, Map<String, Long> domainIds,
			DomainTreeNode root, Map<Long, DomainTreeCardNode> nodes) {

		Map<Object, Map<Object, List<RelationInfo>>> relationsByDomain = new HashMap<>();

		for (DomainTreeNode domainTreeNode : root.getChildNodes()) {
			Long domainId = domainIds.get(domainTreeNode.getDomainName());
			String querySource = domainTreeNode.getDirect() ? "_1" : "_2";
			DomainWithSource dom = DomainWithSource.create(domainId, querySource);
			Map<Object, List<RelationInfo>> relations = dataAccesslogic.relationsBySource(root.getTargetClassName(), dom);
			relationsByDomain.put(domainId, relations);
			boolean leaf = domainTreeNode.getChildNodes().isEmpty();
			boolean baseNode = domainTreeNode.getBaseNode();
			fillNodes(nodes, relationsByDomain, leaf, baseNode);
			fetchRelationsByDomain(dataAccesslogic, domainIds, domainTreeNode, nodes);
		}
	}

	private void fillNodes(Map<Long, DomainTreeCardNode> nodes, Map<Object, Map<Object, List<RelationInfo>>> relationsByDomain, boolean leaf, boolean baseNode) {

		for (Map<Object, List<RelationInfo>> relationsBySource : relationsByDomain.values()) {
			for (Object sourceCardId : relationsBySource.keySet()) {
				DomainTreeCardNode parent = nodes.get(sourceCardId);

				if (parent == null) {
					continue;
				}

				for (RelationInfo ri : relationsBySource.get(sourceCardId)) {
					Long childId = ri.getTargetId();
					DomainTreeCardNode child;
					if (nodes.containsKey(childId)) {
						child = nodes.get(childId);
					} else {
						child = new DomainTreeCardNode();
						nodes.put(childId, child);
					}
					String text = ri.getTargetDescription();
					if (text == null || text.equals("")) {
						text = ri.getTargetCode();
					}

					child.setText(text);
					child.setCardId(childId);
					child.setClassId(ri.getTargetType().getId());
					child.setClassName(ri.getTargetType().getName());
					child.setLeaf(leaf);
					child.setBaseNode(baseNode);

					parent.addChild(child);
				}
			}
		}
	}

//	private Layer modifyLayerMetadata(String targetTableName, String name, String description, int minimumZoom, int maximumZoom, String style, Set<String> cardBinding) {
//
//		String fullName = fullName(targetTableName, name);
//		Layer changes = new Layer();
//		changes.setDescription(description);
//		changes.setMinimumZoom(minimumZoom);
//		changes.setMaximumzoom(maximumZoom);
//		changes.setMapStyle(style);
//		changes.setCardBinding(cardBinding);
//
//		return layerMetadataStore.updateLayer(fullName, changes);
//	}
	private void checkGisEnabled() {
		if (!isGisEnabled()) {
			throw new GisException("GIS Module is non enabled");
		}
	}

	private void ensureGeoServerIsEnabled() {
		if (!configuration.isGeoServerEnabled()) {
			throw new GisException("GEOServer is non enabled");
		}
	}

	private Map<String, Long> getDomainIds(DataAccessService dataAccessLogic) {
		Map<String, Long> domainIds = new HashMap<>();

		for (Domain d : dataAccessLogic.findActiveDomains()) {
			domainIds.put(d.getName(), d.getId());
		}

		return domainIds;
	}

//	private static class DefaultCardMapping implements CardMapping {
//
//		private final String name;
//		private final String description;
//
//		public DefaultCardMapping(String name, String description) {
//			this.name = name;
//			this.description = description;
//		}
//
//		@Override
//		public String getName() {
//			return name;
//		}
//
//		@Override
//		public String getDesription() {
//			return description;
//		}
//
//	}
//	private static class DefaultClassMapping implements ClassMapping {
//
//		private final Map<String, CardMapping> map = Maps.newHashMap();
//
//		@Override
//		public void addCardMapping(String cardId, CardMapping mapping) {
//			map.put(cardId, mapping);
//		}
//
//		@Override
//		public Set<String> cards() {
//			return map.keySet();
//		}
//
//		@Override
//		public CardMapping get(String cardId) {
//			return map.get(cardId);
//		}
//
//	}
}
