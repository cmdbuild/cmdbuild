package org.cmdbuild.gis;

public interface GisValue<T extends Geometry> {

	String getLayerName();

	String getOwnerClassId();

	long getOwnerCardId();

	default GisValueType getType() {
		return getGeometry().getType();
	}

	T getGeometry();

	default <E> E getGeometry(Class<E> type) {
		return (E)getGeometry();
	}

}
