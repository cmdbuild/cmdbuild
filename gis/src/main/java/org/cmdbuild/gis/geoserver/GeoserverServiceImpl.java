package org.cmdbuild.gis.geoserver;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

import org.cmdbuild.config.GisConfiguration;
import org.cmdbuild.exception.NotFoundException;
import org.cmdbuild.gis.GeoserverLayer;
import org.cmdbuild.gis.geoserver.GeoServerStore.StoreDataType;
import org.cmdbuild.gis.geoserver.commands.CreateModifyDataStore;
import org.cmdbuild.gis.geoserver.commands.DeleteFeatureTypeOrCoverage;
import org.cmdbuild.gis.geoserver.commands.DeleteLayer;
import org.cmdbuild.gis.geoserver.commands.DeleteStore;
import org.cmdbuild.gis.geoserver.commands.ListLayers;
import org.cmdbuild.gis.geoserver.commands.ListStores;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_DISABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GeoserverServiceImpl implements GeoserverService, SystemService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final GisConfiguration configuration;

	public GeoserverServiceImpl(GisConfiguration configuration) {
		this.configuration = checkNotNull(configuration);
	}

	@Override
	public String getServiceName() {
		return "GIS GeoServer client";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (!configuration.isGeoServerEnabled()) {
			return SS_DISABLED;
		} else {
			return SS_READY; //TODO check status
		}
	}

	@Override
	public boolean canControlState() {
		return false;
	}

	@Override
	public List<GeoServerStore> getStores() {
		return ListStores.exec(configuration);
	}

	@Override
	public List<GeoserverLayerInfo> getLayers() {
		return ListLayers.exec(configuration);
	}

	@Override
	public String createStoreAndLayer(GeoserverLayer layerMetadata, InputStream data) {
		if (nameIsNotValid(layerMetadata.getLayerName())) {
			throw new IllegalArgumentException(String.format("Layer name must match regex \"%s\"",
					namePattern.toString()));
		}

		GeoServerStore s = new GeoServerStore(layerMetadata.getLayerName(), StoreDataType.valueOf(layerMetadata.getType()));
		return checkNotNull(CreateModifyDataStore.exec(configuration, s, data), "Geoserver has not created the layer");
	}

	@Override
	public void modifyStoreData(GeoserverLayer layerMetadata, InputStream data) {
		GeoServerStore s = new GeoServerStore(layerMetadata.getLayerName(), StoreDataType.valueOf(layerMetadata.getType()));
		CreateModifyDataStore.exec(configuration, s, data);
	}

	@Override
	public void deleteStoreAndLayers(GeoserverLayer layer) {
		StoreDataType storeDatatype = StoreDataType.valueOf(layer.getType());
		GeoServerStore store = new GeoServerStore(layer.getLayerName(), storeDatatype);

		try {
			// Delete the layer first because the store
			// must be empty to be deleted
			List<GeoserverLayerInfo> storeLayers = ListLayers.exec(configuration, store.getName());
			for (GeoserverLayerInfo geoServerLayer : storeLayers) {
				DeleteLayer.exec(configuration, geoServerLayer);
				DeleteFeatureTypeOrCoverage.exec(configuration, geoServerLayer, store);
			}
		} catch (NotFoundException e) {
			logger.warn(String.format("GeoServer layer for store %s not found", layer.getLayerName()));
		}

		DeleteStore.exec(configuration, store);
	}

	private static final Pattern namePattern = java.util.regex.Pattern.compile("^\\S+$");

	private boolean nameIsNotValid(String name) {
		return !namePattern.matcher(name).matches();
	}
}
