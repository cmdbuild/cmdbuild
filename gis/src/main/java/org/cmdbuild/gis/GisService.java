package org.cmdbuild.gis;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import org.cmdbuild.navtree.NavTreeNode;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.navtree.DomainTreeCardNode;
import org.cmdbuild.navtree.DomainTreeNode;
import org.cmdbuild.data2.api.DataAccessService;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface GisService {

	boolean isGisEnabled();

	GisAttribute createGeoAttribute(GisAttribute layerMetaData);

	void deleteGeoAttribute(String masterTableName, String attributeName);

	Pair<List<GisValue>, List<NavTreeNode>> getGeoValuesAndNavTree(Collection<Long> attrs, String bbox);

	void createGeoServerLayer(GeoserverLayer layerMetaData, FileItem file);

	void deleteGeoServerLayer(String name);

	List<GeoserverLayer> getGeoServerLayers();

	List<GeoserverLayer> getGeoServerLayersForCard(String classId, Long cardId);

	List<GisAttribute> getAllLayers();

//	void saveGisTreeNavigation(DomainTreeNode root);
	void removeGisTreeNavigation();

	DomainTreeNode getGisTreeNavigation();

	DomainTreeCardNode expandDomainTree(DataAccessService dataAccesslogic);

	GisAttribute getLayerByClassAndName(String classId, String attributeId);

	GisAttribute updateGeoAttribute(GisAttribute layer);

	List<GisAttribute> getGeoAttributeByOwnerClass(String classId);

	List<GisAttribute> getVisibleGeoAttributesForClass(String classId);

	List<GeoserverLayer> getGeoLayersVisibleFromClass(String classId);

	List<GeoserverLayer> getGeoLayersOwnedByClass(String classId);

	List<GisValue> getValues(String classId, Long cardId);

	@Nullable
	default GisValue getValueOrNull(String classId, Long cardId, String attrId) {
		checkNotBlank(attrId);
		return getValues(classId, cardId).stream().filter((v) -> equal(v.getLayerName(), attrId)).collect(toOptional()).orElse(null);
	}

	default GisValue getValue(String classId, Long cardId, String attrId) {
		return checkNotNull(getValueOrNull(classId, cardId, attrId), "geo value not found for classId = %s cardId = %s attrId = %s", classId, cardId, attrId);
	}

	List<GisValue> getGeoValues(Collection<Long> attrs, String bbox);

	@Deprecated
	void updateValues(String classId, Long cardId, Map<String, Object> attributes);

	GisValue setValue(GisValue value);

	void deleteValue(String classId, long cardId, String attrId);
}
