package org.cmdbuild.gis;

import org.cmdbuild.navtree.NavTreeNode;
import org.cmdbuild.navtree.NavTreeNodeImpl;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;
import com.google.common.collect.Lists;
import static com.google.common.collect.Maps.uniqueIndex;
import static com.google.common.collect.MoreCollectors.toOptional;
import static java.lang.String.format;
import java.sql.ResultSet;
import java.sql.SQLException;
import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import static org.cmdbuild.cache.CacheUtils.key;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.driver.postgres.q3.AliasBuilder;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Domain;

import org.cmdbuild.gis.utils.GisUtils;
import static org.cmdbuild.gis.utils.GisUtils.buildGisTableNameForQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.navtree.DomainTreeNode;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.isNullOrEmpty;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.gis.utils.GisUtils.postgisSqlToCmGeometryOrNull;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.entryTypeToQuotedSqlIdentifier;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.dao.driver.postgres.utils.DaoPgUtils.quoteSqlIdentifier;

@Component("geoFeatureStore")
public class GisValueRepositoryImpl implements GisValueRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;
	private final JdbcTemplate jdbcTemplate;
	private final GisDatabaseService databaseService;
	private final GisAttributeRepository attributeRepository;

	public GisValueRepositoryImpl(DaoService dao, GisDatabaseService databaseService, GisAttributeRepository attributeRepository) {
		this.dao = checkNotNull(dao);
		this.jdbcTemplate = dao.getJdbcTemplate();
		this.databaseService = checkNotNull(databaseService);
		this.attributeRepository = checkNotNull(attributeRepository);
	}

	@Override
	public void checkGisSchemaAndCreateIfMissing() {
		databaseService.checkGisSchemaAndCreateIfMissing();
	}

	@Override
	public boolean isGisSchemaOk() {
		return databaseService.isGisSchemaOk();
	}

	private String getTableName(GisAttribute gisAttribute) {
		Classe classe = dao.getClasse(gisAttribute.getOwnerClassId());
		return buildGisTableNameForQuery(classe.getName(), gisAttribute.getLayerName());
	}

	@Override
	public void createGeoTable(GisAttribute gisAttribute) {//TODO replace this stuff with _cm3_gis_xxx functions

		String geoAttributeTableName = getTableName(gisAttribute);
		String createGeoTableQuery = String.format("SELECT _cm3_class_create('%s', NULL, '%s'::jsonb)", geoAttributeTableName, toJson(map(
				"DESCR", gisAttribute.getDescription(),
				"MODE", "write",
				"STATUS", "active",
				"SUPERCLASS", "false",
				"TYPE", "simpleclass"
		)));
		logger.debug("Create GIS Table: {}", createGeoTableQuery);

		jdbcTemplate.execute(createGeoTableQuery);

		jdbcTemplate.execute(format("SELECT _cm3_attribute_create('%s','Master','bigint','FKTARGETCLASS: %s|STATUS: active|BASEDSP: false|CLASSORDER: 0|DESCR: Master|INDEX: -1|MODE: write')", geoAttributeTableName, gisAttribute.getOwnerClassId()));
		jdbcTemplate.execute(format("SELECT _cm3_attribute_create('%s','Geometry','%s','STATUS: active|BASEDSP: false|CLASSORDER: 0|DESCR: Geometry|INDEX: -1|MODE: write')", geoAttributeTableName, gisAttribute.getType()));

	}

	@Override
	public void deleteGeoTable(String targetClassName, String geoAttributeName) {
		String geoAttributeTableName = GisUtils.buildGisTableName(targetClassName, geoAttributeName);
		logger.debug("Delete GIS table = {}", geoAttributeTableName);
		jdbcTemplate.execute(format("SELECT _cm3_class_delete('%s'::regclass)", quoteSqlIdentifier(geoAttributeTableName)));
	}

	@Override
	public void createGeoValue(GisAttribute gisAttribute, String value, long ownerCardId) {
		jdbcTemplate.execute(format("INSERT INTO %s (\"Master\", \"Geometry\") VALUES (%s, gis.ST_GeomFromText('%s',900913))", getTableName(gisAttribute), ownerCardId, value));
	}

	@Override
	public void updateGeoValue(GisAttribute gisAttribute, String value, long ownerCardId) {
		jdbcTemplate.execute(format("UPDATE %s SET \"Geometry\" = gis.ST_GeomFromText('%s',900913) WHERE \"Master\" = %s", getTableName(gisAttribute), value, ownerCardId));
	}

	@Override
	public void deleteGeoValue(GisAttribute gisAttribute, long ownerCardId) {
		jdbcTemplate.update(format("DELETE FROM %s WHERE \"Master\" = %s", getTableName(gisAttribute), ownerCardId));
	}

	@Override
	public @Nullable
	GisValue getGeoFeatureOrNull(GisAttribute gisAttribute, Long ownerCardId) {
		Classe classe = dao.getClasse(gisAttribute.getOwnerClassId());
		return jdbcTemplate.query(format("SELECT gis.st_astext(\"Geometry\") AS geometry FROM %s WHERE \"Master\" = %s", getTableName(gisAttribute), checkNotNull(ownerCardId)), (rs, i) -> {
			String geometryAsString = rs.getString("geometry");
			return postgisSqlToCmGeometryOrNull(gisAttribute.getLayerName(), classe.getName(), ownerCardId, geometryAsString);
		}).stream().collect(toOptional()).orElse(null);
	}

	@Override
	public List<GisValue> getGeoValues(Iterable<Long> layers, String bbox) {
		return jdbcTemplate.query(buildGeoValueBaseQuery(layers, null, bbox), (rs, i) -> gisValueFromResultSet(rs));
	}

	private static GisValue gisValueFromResultSet(ResultSet rs) throws SQLException {
		String geometryAsString = rs.getString("_geometry"),
				ownerClass = rs.getString("_ownerclass"),
				attrName = rs.getString("_attrname");
		Long ownerCard = rs.getLong("_ownercard");
		return postgisSqlToCmGeometryOrNull(attrName, ownerClass, ownerCard, checkNotBlank(geometryAsString));
	}

	private String buildGeoValueBaseQuery(Iterable<Long> layers, List<String> select, String bbox) {
		checkArgument(!isEmpty(layers), "geo attribute (layer) list param cannot be null");
		checkNotBlank(bbox, "area param cannot be null");
		String otherSelect;
		if (isNullOrEmpty(select)) {
			otherSelect = "";
		} else {
			otherSelect = "," + Joiner.on(",").join(select);
		}
		return format("SELECT replace((_geometry.ownerclass)::varchar,'\"','') _ownerclass,_geometry.attrname _attrname,_geometry.ownercard _ownercard,_geometry.geometry _geometry%s FROM _cm3_gis_find_values('{%s}'::bigint[],'%s') _geometry", otherSelect, Joiner.on(",").join(layers), getEnvelopeParamsFromBboxArea(bbox));
	}

	@Override
	public Pair<List<GisValue>, List<NavTreeNode>> getGeoValuesAndNavTree(Iterable<Long> layers, String bbox, DomainTreeNode navTreeDomains) {
		List<GisValue> gisValues = list();
		List<NavTreeNode> navTreeNodes = list();
		DomainTreeNodeAux domainTreeNodes = new DomainTreeNodeAux(navTreeDomains);
		for (Long attrId : layers) {
			Pair<List<GisValue>, List<NavTreeNode>> geoValuesAndNavTree = new GeoValuesAndNavTreeAux(attrId, domainTreeNodes).getGeoValuesAndNavTree(bbox);
			gisValues.addAll(geoValuesAndNavTree.getLeft());
			navTreeNodes.addAll(geoValuesAndNavTree.getRight());
		}

		return Pair.of(gisValues, navTreeNodes);
	}

	private class DomainTreeNodeAux {

		private final List<DomainTreeNode> domainTreeNodes;
		private final Map<String, DomainTreeNode> domainTreeNodesByTargetClassName = map();
		private final Map<Long, DomainTreeNode> domainTreeNodesById;

		public DomainTreeNodeAux(DomainTreeNode navTreeDomains) {
			this.domainTreeNodes = navTreeDomains.getThisNodeAndAllDescendants();
			domainTreeNodes.forEach((n) -> domainTreeNodesByTargetClassName.put(n.getTargetClassName(), n));// allow duplicates, keep a random element
			domainTreeNodesById = uniqueIndex(domainTreeNodes, DomainTreeNode::getId);
		}

		public @Nullable
		DomainTreeNode getNodeByTargetClassOrNull(String classId) {
			Classe classe = dao.getClasse(classId);
			return getNodeByTargetClassOrNull(classe);
		}

		private @Nullable
		DomainTreeNode getNodeByTargetClassOrNull(Classe classe) {
			DomainTreeNode node = domainTreeNodesByTargetClassName.get(classe.getName());
			if (node == null && classe.hasParent()) {
				node = getNodeByTargetClassOrNull(classe.getParent());
			}
			return node;
		}

		public DomainTreeNode getNodeById(long id) {
			return checkNotNull(domainTreeNodesById.get(id), "node not found for id = %s", id);
		}
	}

	private class GeoValuesAndNavTreeAux {

		private final long attrId;
		private final DomainTreeNodeAux domainTreeNodes;

		private final List<GisValue> gisValues = list();
		private final Set<String> addedNodeKeys = set();
		private final List<NavTreeNode> navTreeNodes = list();
		private final AliasBuilder aliasBuilder = new AliasBuilder();
		private final List<Triple<String, String, Classe>> aliasesForCardIdAndDescription = list();
		private final List<String> select = list();
		private final List<String> queryJoins = list();

		public GeoValuesAndNavTreeAux(long layer, DomainTreeNodeAux domainTreeNodes) {
			this.attrId = layer;
			this.domainTreeNodes = checkNotNull(domainTreeNodes);
		}

		public Pair<List<GisValue>, List<NavTreeNode>> getGeoValuesAndNavTree(String bbox) {
			GisAttribute attr = attributeRepository.getLayer(attrId);
			List<DomainTreeNode> navTreeBranch = buildReverseNavTreeBranchForAttr(attr);

			{
				Classe targetClass = dao.getClasse(attr.getOwnerClassId());
				String targetClassAlias = aliasBuilder.buildAlias(targetClass.getName());

				queryJoins.add(format(" JOIN %s %s ON %s.\"IdClass\" = _geometry.ownerclass AND %s.\"Id\" = _geometry.ownercard AND %s.\"Status\" = 'A'",
						entryTypeToQuotedSqlIdentifier(targetClass), targetClassAlias, targetClassAlias, targetClassAlias, targetClassAlias));

				selectCardAttrs(targetClass, targetClassAlias);
			}

			String sourceClassIdExpr = "_geometry.ownerclass";
			String sourceCardIdExpr = "_geometry.ownercard";

			for (DomainTreeNode node : navTreeBranch) {

				String domainId = node.getDomainName();
				Domain domain = dao.getDomain(domainId);
				Classe targetClass;
				String id1, id2;
				if (node.getDirect()) {
					id1 = "2";
					id2 = "1";
					targetClass = domain.getSourceClass();
				} else {
					id1 = "1";
					id2 = "2";
					targetClass = domain.getTargetClass();
				}
				String domainAlias = aliasBuilder.buildAlias(domainId);

				queryJoins.add(format(" LEFT JOIN %s %s ON %s.\"IdObj%s\" = %s AND %s.\"IdClass%s\" = %s AND %s.\"Status\" = 'A' ",
						entryTypeToQuotedSqlIdentifier(domain), domainAlias, domainAlias, id1, sourceCardIdExpr, domainAlias, id1, sourceClassIdExpr, domainAlias));

				String targetClassAlias = aliasBuilder.buildAlias(targetClass.getName());

				queryJoins.add(format(" LEFT JOIN %s %s ON %s.\"IdClass\" = %s.\"IdClass%s\" AND %s.\"Id\" = %s.\"IdObj%s\" AND %s.\"Status\" = 'A'",
						entryTypeToQuotedSqlIdentifier(targetClass), targetClassAlias, targetClassAlias, domainAlias, id2, targetClassAlias, domainAlias, id2, targetClassAlias));

				sourceClassIdExpr = format("%s.\"IdClass\"", targetClassAlias);
				sourceCardIdExpr = format("%s.\"Id\"", targetClassAlias);

				selectCardAttrs(targetClass, targetClassAlias);
			}

			String query = buildGeoValueBaseQuery(singleton(attrId), select, bbox) + " " + Joiner.on(" ").join(queryJoins);

			jdbcTemplate.query(query, (ResultSet rs) -> {
				gisValues.add(gisValueFromResultSet(rs));
				NavTreeNode parent = null;
				for (Triple<String, String, Classe> joinParams : Lists.reverse(aliasesForCardIdAndDescription)) {
					String cardIdAlias = joinParams.getLeft();
					String cardDescAlias = joinParams.getMiddle();

					long cardId = rs.getLong(cardIdAlias);
					if (cardId == 0) {
						break;
					}

					String cardDesc = rs.getString(cardDescAlias);

					NavTreeNode thisNode = NavTreeNodeImpl.builder()
							.withCardId(cardId)
							.withClassId(joinParams.getRight().getName())
							.withDescription(cardDesc)
							.withParentClassId(parent == null ? null : parent.getClassId())
							.withParentCardId(parent == null ? null : parent.getCardId())
							.build();
					addNode(thisNode);
					parent = thisNode;
				}
			});

			return Pair.of(gisValues, navTreeNodes);
		}

		private void addNode(NavTreeNode node) {
			if (addedNodeKeys.add(key(node.getClassId(), node.getCardId()))) {
				navTreeNodes.add(node);
			}
		}

		private void selectCardAttrs(Classe targetClass, String targetClassAlias) {
			String cardIdAlias = aliasBuilder.buildAlias(targetClass.getName() + "cardid");
			String cardDescAlias = aliasBuilder.buildAlias(targetClass.getName() + "carddesc");
			select.add(format("%s.\"Id\" %s", targetClassAlias, cardIdAlias));
			select.add(format("%s.\"Description\" %s", targetClassAlias, cardDescAlias));
			aliasesForCardIdAndDescription.add(Triple.of(cardIdAlias, cardDescAlias, targetClass));
		}

		private List<DomainTreeNode> buildReverseNavTreeBranchForAttr(GisAttribute attr) {
			Classe classe = dao.getClasse(attr.getOwnerClassId());
			return buildReverseNavTreeBranchForClassId(classe.getName());
		}

		private List<DomainTreeNode> buildReverseNavTreeBranchForClassId(String classId) {
			logger.debug("buildReverseNavTreeBranchForClassId = {}", classId);
			DomainTreeNode node = domainTreeNodes.getNodeByTargetClassOrNull(classId);
			if (node == null || isBlank(node.getDomainName())) {
				logger.warn("nav tree domain not found for class = {} (node = {})", classId, node);
				return emptyList();
			} else {
				logger.debug("found node = {}", node);
				List<DomainTreeNode> list = list(node);
				while (node.getIdParent() != null && isNotBlank(node.getDomainName())) {
					DomainTreeNode curNode = node;
					node = domainTreeNodes.getNodeById(curNode.getIdParent());
					logger.debug("found node = {}", node);
					if (isNotBlank(node.getDomainName())) {
						list.add(node);
					}
				}
				return list;
			}
		}
	}

	@Override
	public List<GisValue> readGeoFeatures(GisAttribute gisAttribute, @Nullable String bbox) {

		String tableName = getTableName(gisAttribute);
		Classe ownerClass = dao.getClasse(gisAttribute.getOwnerClassId());

		String sql = String.format(
				"SELECT \"FeatureTable\".\"Master\" AS owner, gis.asText(\"FeatureTable\".\"Geometry\") AS \"Geometry\", \"OwnerCardTable\".\"IdClass\"::oid AS \"MasterClassId\", _cm_cmtable(\"OwnerCardTable\".\"IdClass\"::oid) AS \"MasterClassName\" "
				+ "FROM %s AS \"FeatureTable\" " + "JOIN \"%s\" AS \"OwnerCardTable\" " + "ON \"FeatureTable\".\"Master\" = \"OwnerCardTable\".\"Id\" ",
				tableName,
				ownerClass.getName());

		if (isNotBlank(bbox)) {
			sql += format(" WHERE (\"FeatureTable\".\"Geometry\" && gis.st_makeenvelope(%s,900913))", getEnvelopeParamsFromBboxArea(bbox));
		}

		return jdbcTemplate.query(sql, (rs, i) -> {
			String geometryAsString = rs.getString("Geometry");
			Long ownerCardId = rs.getLong("owner");
			return checkNotNull(postgisSqlToCmGeometryOrNull(gisAttribute.getLayerName(), ownerClass.getName(), ownerCardId, geometryAsString));
		});
	}

	private String getEnvelopeParamsFromBboxArea(String bbox) {
		String[] coordinates = bbox.split(",");
		Preconditions.checkArgument(coordinates.length == 4);
		return Joiner.on(",").join(coordinates);
	}

}
