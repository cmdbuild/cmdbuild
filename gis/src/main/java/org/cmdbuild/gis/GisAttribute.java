package org.cmdbuild.gis;

public interface GisAttribute extends GisLayer {

	String getOwnerClassId();

	String getDescription();

	String getMapStyle();

	String getType();

}
