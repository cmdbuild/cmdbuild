package org.cmdbuild.gis;

import org.cmdbuild.navtree.NavTreeNode;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.navtree.DomainTreeNode;

public interface GisValueRepository {

	void createGeoTable(GisAttribute layerMetadata);

	void deleteGeoTable(String targetClassName, String geoAttributeName);

	void createGeoValue(GisAttribute layerMetaData, String value, long ownerCardId);

	void updateGeoValue(GisAttribute layerMetaData, String value, long ownerCardId);

	GisValue getGeoFeatureOrNull(GisAttribute layerMetaData, Long ownerCardId);

	List<GisValue> readGeoFeatures(GisAttribute layerMetaData, String bbox);

	void deleteGeoValue(GisAttribute layerMetaData, long ownerCardId);

	void checkGisSchemaAndCreateIfMissing();

	List<GisValue> getGeoValues(Iterable<Long> layers, String bbox);

	Pair<List<GisValue>, List<NavTreeNode>> getGeoValuesAndNavTree(Iterable<Long> layers, String bbox, DomainTreeNode navTreeDomains);

	boolean isGisSchemaOk();
}
