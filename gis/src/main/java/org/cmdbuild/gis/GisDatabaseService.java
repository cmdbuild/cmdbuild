package org.cmdbuild.gis;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import static com.google.common.collect.Iterables.getOnlyElement;
import java.io.File;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import java.util.Collection;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.cmdbuild.dao.config.DatabaseConfiguration;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.readToString;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.postgres.PostgresUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class GisDatabaseService {//TODO merge this in gis service/gis repo

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final JdbcTemplate jdbcTemplate;
	private final DatabaseConfiguration databaseConfiguration;

	private final Supplier<String> postgisVersion = Suppliers.memoize(this::doGetPostgisVersion);

	public GisDatabaseService(DaoService dao, DatabaseConfiguration databaseConfiguration) {
		this.jdbcTemplate = dao.getJdbcTemplate();
		this.databaseConfiguration = checkNotNull(databaseConfiguration);
	}

	public String getPostgisVersion() {
		return postgisVersion.get();
	}

	public void checkGisSchemaAndCreateIfMissing() {
		boolean gisSchemaExists = jdbcTemplate.queryForObject("SELECT EXISTS(SELECT schema_name FROM information_schema.schemata WHERE schema_name = 'gis')", Boolean.class);
//				gisSchemaHasTables = jdbcTemplate.queryForObject("SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'gis')", Boolean.class);
		if (!gisSchemaExists) {
			logger.info("gis schema not found; preparing gis schema");
			prepareGisSchema();
		}
		String version = getPostgisVersion();
		logger.info("postgis ready with version = {}", version);
	}

	public boolean isGisSchemaOk() {
		try {
			doGetPostgisVersion();
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	private void prepareGisSchema() {
		logger.info("searching for postgis directory");
		try {
			// es: /usr/share/postgresql/9.5/contrib/postgis-2.2
			Collection<File> files = FileUtils.listFiles(new File("/usr/share/postgresql/"), FileFilterUtils.nameFileFilter("postgis.sql"), FileFilterUtils.directoryFileFilter()); //TODO search for cur pg db version
			checkArgument(!files.isEmpty(), "unable to find postgis extension script, you will have to install postgis or manually prepare the 'gis' schema");
			checkArgument(files.size() <= 1, "found more than one posgis extension script, unable to select the correct one. You'll have to manually prepare the 'gis' schema");
			File dir = getOnlyElement(files).getParentFile();
			logger.info("loading postgis extension script from {}", dir.getAbsolutePath());
			StringBuilder sqlScript = new StringBuilder()
					.append(format("\nCREATE SCHEMA gis AUTHORIZATION %s;\n", databaseConfiguration.getDatabaseUser()))//TODO escape user name
					.append("\nSET SEARCH_PATH TO gis, public;\n");
			asList("postgis.sql", "spatial_ref_sys.sql", "legacy.sql").forEach((script) -> {
				sqlScript.append("\n").append(readToString(new File(dir, script))).append("\n");
			});
			sqlScript.append("\nALTER DATABASE :DBNAME SET search_path=public, gis;\n");
			PostgresUtils.newBuilder(databaseConfiguration.getHost(), databaseConfiguration.getPort(), databaseConfiguration.getDatabaseAdminUsername(), databaseConfiguration.getDatabaseAdminPassword()).buildUtils()
					.executeScript(databaseConfiguration.getDatabase(), sqlScript.toString());
		} catch (Exception ex) {
			throw new GisException(ex, "unable to prepare the 'gis' schema");
		}
	}

	private String doGetPostgisVersion() {
		try {
			return checkNotBlank(jdbcTemplate.queryForObject("SELECT gis.postgis_lib_version()", String.class));
		} catch (Exception ex) {
			throw new GisException(ex, "error processing gis schema: invalid gis schema content");
		}
	}

}
