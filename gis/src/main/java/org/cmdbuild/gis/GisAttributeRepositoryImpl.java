package org.cmdbuild.gis;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.onlyElement;

import java.util.List;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.Holder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;

@Component
public class GisAttributeRepositoryImpl implements GisAttributeRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;
	private final Holder<List<GisAttribute>> allAttributes;

	public GisAttributeRepositoryImpl(DaoService dao, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		allAttributes = cacheService.newHolder("all_gis_attributes", CacheService.CacheConfig.SYSTEM_OBJECTS);
	}

	private void invalidateCache() {
		allAttributes.invalidate();
	}

	@Override
	public GisAttribute createLayer(GisAttribute attr) {
		logger.info("create layer = {}", attr);
		attr = dao.create(GisAttributeImpl.copyOf(attr).build());
		//TODO auto index
		invalidateCache();
		return attr;
	}

	@Override
	public GisAttribute getLayer(String classId, String name) {
		checkNotNull(classId);
		checkNotBlank(name);
		try {
			return getAllLayers().stream().filter((a) -> equal(a.getOwnerClassId(), classId) && equal(a.getLayerName(), name)).collect(onlyElement());
		} catch (Exception ex) {
			throw runtime(ex, "gis attr not found for classe = %s and layer name = %s", classId, name);
		}
	}

	@Override
	public GisAttribute getLayer(long attrId) {
		try {
			return getAllLayers().stream().filter((l) -> l.getId() == attrId).collect(onlyElement());
		} catch (Exception ex) {
			throw runtime(ex, "gis attr not found for id = %s", attrId);
		}
	}

	@Override
	public List<GisAttribute> getLayersByOwnerClass(String classId) {
		checkNotNull(classId);
		return getAllLayers().stream().filter((a) -> equal(a.getOwnerClassId(), classId)).collect(toList());
	}

	@Override
	public List<GisAttribute> getVisibleLayersForClass(String classId) {
		checkNotNull(classId);
		return getAllLayers().stream().filter((a) -> a.getVisibility().contains(classId)).collect(toList());
	}

	@Override
	public List<GisAttribute> getLayersByOwnerClassAndLayerName(String classId, @Nullable Iterable<String> layerNames) {
		checkNotNull(classId);
		Stream<GisAttribute> stream = getAllLayers().stream().filter((a) -> equal(a.getOwnerClassId(), classId));
		if (layerNames != null) {
			Set<String> set = set(checkNotEmpty(layerNames));
			stream = stream.filter((a) -> set.contains(a.getLayerName()));
		}
		return stream.collect(toList());
	}

	@Override
	public GisAttribute updateLayer(GisAttribute attr) {
		logger.info("update layer = {}", attr);
		attr = dao.update(GisAttributeImpl.copyOf(attr).build());
		invalidateCache();
		return attr;
	}

	@Override
	public void deleteLayer(String classId, String name) {
		logger.info("deleteLayer layer = {} {}", classId, name);
		GisAttribute attr = getLayer(classId, name);
		dao.delete(attr);
		invalidateCache();
	}

	@Override
	public List<GisAttribute> getAllLayers() {
		return allAttributes.get(this::doGetAllLayers);
	}

	private List<GisAttribute> doGetAllLayers() {
		return dao.selectAll().from(GisAttributeImpl.class).asList();
	}

}
