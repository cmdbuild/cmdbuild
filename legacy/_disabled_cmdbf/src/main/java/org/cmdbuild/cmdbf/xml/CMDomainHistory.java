package org.cmdbuild.cmdbf.xml;

import org.cmdbuild.dao.entrytype.ForwardingDomain;
import org.cmdbuild.dao.entrytype.Domain;

public class CMDomainHistory extends ForwardingDomain {

	private final Domain delegate;

	public CMDomainHistory(final Domain delegate) {
		this.delegate = delegate;
	}

	@Override
	protected Domain delegate() {
		return delegate;
	}

	public Domain getBaseType() {
		return delegate;
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		return delegate.equals(obj);
	}

}
