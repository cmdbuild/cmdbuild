package org.cmdbuild.cmdbf.xml;

import org.cmdbuild.dao.entrytype.ForwardingClass;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import java.util.Collection;
import org.cmdbuild.dao.entrytype.Classe;

public class CMClassHistory extends ForwardingClass {

	private final Classe delegate;

	public CMClassHistory(final Classe delegate) {
		this.delegate = delegate;
	}

	@Override
	protected Classe delegate() {
		return delegate;
	}

	public Classe getBaseType() {
		return delegate;
	}

	@Override
	public Classe getParentOrNull() {
		return super.getParentOrNull() != null ? new CMClassHistory(super.getParentOrNull()) : null;
	}

	@Override
	public Collection<Classe> getLeaves() {
		return Collections2.transform(super.getLeaves(), TO_HISTORIC);
	}

	@Override
	public Collection<Classe> getDescendants() {
		return Collections2.transform(super.getDescendants(), TO_HISTORIC);
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		return delegate.equals(obj);
	}

	private static final Function<Classe, Classe> TO_HISTORIC = new Function<Classe, Classe>() {

		@Override
		public Classe apply(final Classe input) {
			return new CMClassHistory(input);
		}

	};
}
