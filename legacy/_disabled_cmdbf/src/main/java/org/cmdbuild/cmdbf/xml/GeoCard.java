package org.cmdbuild.cmdbf.xml;

import java.util.HashMap;
import java.util.Map;
import org.cmdbuild.gis.GisValue;


public class GeoCard {
	private final GeoClass type;
	private final Map<String, GisValue> geometries;

	public GeoCard(final GeoClass type) {
		this.type = type;
		geometries = new HashMap<String, GisValue>();
	}

	public GeoClass getType() {
		return type;
	}

	public GisValue get(final String name) {
		return geometries.get(name);
	}

	public void set(final String name, final GisValue geometry) {
		geometries.put(name, geometry);
	}

	public boolean isEmpty() {
		return geometries.isEmpty();
	}
}
