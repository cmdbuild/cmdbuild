package org.cmdbuild.cmdbf.xml;

import org.cmdbuild.cmdbf.cmdbmdr.CmdbRelation;
import org.cmdbuild.dao.beans.CMRelation;
import org.cmdbuild.dao.entrytype.Domain;

public class CMRelationHistory extends CmdbRelation {

	private final CMDomainHistory type;

	public CMRelationHistory(final CMRelation delegate, final String card1ClassName, final String card2ClassName) {
		super(delegate, card1ClassName, card2ClassName);
		this.type = new CMDomainHistory(delegate.getType());
	}

	@Override
	public Domain getType() {
		return type;
	}
}
