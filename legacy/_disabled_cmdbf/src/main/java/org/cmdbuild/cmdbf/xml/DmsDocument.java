package org.cmdbuild.cmdbf.xml;

import java.io.InputStream;
import org.cmdbuild.dms.inner.SimpleStoredDocumentInfo;

import org.cmdbuild.dms.StoredDocumentInfo;

public class DmsDocument extends SimpleStoredDocumentInfo {

	private InputStream inputStream;

	public DmsDocument() {
	}

	public DmsDocument(StoredDocumentInfo document, InputStream inputStream) {
		setAuthor(document.getAuthor());
		setDescription(document.getDescription());
		setName(document.getId());
		setPath(document.getPath());
		setDmsFileName(document.getDmsFileName());
		setVersion(document.getVersion());
		setCategory(document.getCategory());
		setCreated(document.getCreated_old());
		setModified(document.getModified_old());
		setMetadata(document.getMetadata());
		setInputStream(inputStream);
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
}
