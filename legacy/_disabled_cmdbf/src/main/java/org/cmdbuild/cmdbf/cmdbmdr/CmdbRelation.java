package org.cmdbuild.cmdbf.cmdbmdr;

import org.cmdbuild.dao.beans.CMRelation;
import org.cmdbuild.dao.beans.ForwardingEntry;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.beans.DatabaseEntry;

public class CmdbRelation extends ForwardingEntry implements CMRelation {

	private final CMRelation delegate;
	private final String card1ClassName;
	private final String card2ClassName;

	public CmdbRelation(final CMRelation delegate, final String card1ClassName, final String card2ClassName) {
		this.delegate = delegate;
		this.card1ClassName = card1ClassName;
		this.card2ClassName = card2ClassName;
	}

	@Override
	protected DatabaseEntry delegate() {
		return delegate;
	}

	@Override
	public Domain getType() {
		return delegate.getType();
	}

	@Override
	public Long getSourceId() {
		return delegate.getSourceId();
	}

	@Override
	public Long getTargetId() {
		return delegate.getTargetId();
	}

	public String getCard1ClassName() {
		return card1ClassName;
	}

	public String getCard2ClassName() {
		return card2ClassName;
	}
}
