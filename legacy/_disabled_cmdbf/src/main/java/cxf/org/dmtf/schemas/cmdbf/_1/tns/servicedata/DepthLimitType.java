
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DepthLimitType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DepthLimitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="maxIntermediateItems" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="intermediateItemTemplate" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepthLimitType")
public class DepthLimitType {

    @XmlAttribute(name = "maxIntermediateItems")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxIntermediateItems;
    @XmlAttribute(name = "intermediateItemTemplate")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object intermediateItemTemplate;

    /**
     * Gets the value of the maxIntermediateItems property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxIntermediateItems() {
        return maxIntermediateItems;
    }

    /**
     * Sets the value of the maxIntermediateItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxIntermediateItems(BigInteger value) {
        this.maxIntermediateItems = value;
    }

    /**
     * Gets the value of the intermediateItemTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIntermediateItemTemplate() {
        return intermediateItemTemplate;
    }

    /**
     * Sets the value of the intermediateItemTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIntermediateItemTemplate(Object value) {
        this.intermediateItemTemplate = value;
    }

}
