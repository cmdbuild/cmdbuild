
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MdrScopedIdType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MdrScopedIdType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mdrId" type="{http://www.w3.org/2001/XMLSchema}anyURI"/&gt;
 *         &lt;element name="localId" type="{http://www.w3.org/2001/XMLSchema}anyURI"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MdrScopedIdType", propOrder = {
    "mdrId",
    "localId"
})
public class MdrScopedIdType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String mdrId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String localId;

    /**
     * Gets the value of the mdrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdrId() {
        return mdrId;
    }

    /**
     * Sets the value of the mdrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdrId(String value) {
        this.mdrId = value;
    }

    /**
     * Gets the value of the localId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalId() {
        return localId;
    }

    /**
     * Sets the value of the localId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalId(String value) {
        this.localId = value;
    }

}
