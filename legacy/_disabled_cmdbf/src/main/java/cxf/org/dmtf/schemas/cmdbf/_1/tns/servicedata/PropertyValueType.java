
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3c.dom.Element;


/**
 * <p>Java class for PropertyValueType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PropertyValueType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="equal" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}EqualOperatorType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="less" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}ComparisonOperatorType" minOccurs="0"/&gt;
 *         &lt;element name="lessOrEqual" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}ComparisonOperatorType" minOccurs="0"/&gt;
 *         &lt;element name="greater" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}ComparisonOperatorType" minOccurs="0"/&gt;
 *         &lt;element name="greaterOrEqual" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}ComparisonOperatorType" minOccurs="0"/&gt;
 *         &lt;element name="contains" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}StringOperatorType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="like" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}StringOperatorType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="isNull" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}NullOperatorType" minOccurs="0"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="namespace" use="required" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&gt;
 *       &lt;attribute name="localName" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" /&gt;
 *       &lt;attribute name="recordMetadata" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="matchAny" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PropertyValueType", propOrder = {
    "equal",
    "less",
    "lessOrEqual",
    "greater",
    "greaterOrEqual",
    "contains",
    "like",
    "isNull",
    "any"
})
public class PropertyValueType {

    protected List<EqualOperatorType> equal;
    protected ComparisonOperatorType less;
    protected ComparisonOperatorType lessOrEqual;
    protected ComparisonOperatorType greater;
    protected ComparisonOperatorType greaterOrEqual;
    protected List<StringOperatorType> contains;
    protected List<StringOperatorType> like;
    protected NullOperatorType isNull;
    @XmlAnyElement(lax = true)
    protected List<Object> any;
    @XmlAttribute(name = "namespace", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String namespace;
    @XmlAttribute(name = "localName", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String localName;
    @XmlAttribute(name = "recordMetadata")
    protected Boolean recordMetadata;
    @XmlAttribute(name = "matchAny")
    protected Boolean matchAny;

    /**
     * Gets the value of the equal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEqual().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EqualOperatorType }
     * 
     * 
     */
    public List<EqualOperatorType> getEqual() {
        if (equal == null) {
            equal = new ArrayList<EqualOperatorType>();
        }
        return this.equal;
    }

    /**
     * Gets the value of the less property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public ComparisonOperatorType getLess() {
        return less;
    }

    /**
     * Sets the value of the less property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public void setLess(ComparisonOperatorType value) {
        this.less = value;
    }

    /**
     * Gets the value of the lessOrEqual property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public ComparisonOperatorType getLessOrEqual() {
        return lessOrEqual;
    }

    /**
     * Sets the value of the lessOrEqual property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public void setLessOrEqual(ComparisonOperatorType value) {
        this.lessOrEqual = value;
    }

    /**
     * Gets the value of the greater property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public ComparisonOperatorType getGreater() {
        return greater;
    }

    /**
     * Sets the value of the greater property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public void setGreater(ComparisonOperatorType value) {
        this.greater = value;
    }

    /**
     * Gets the value of the greaterOrEqual property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public ComparisonOperatorType getGreaterOrEqual() {
        return greaterOrEqual;
    }

    /**
     * Sets the value of the greaterOrEqual property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonOperatorType }
     *     
     */
    public void setGreaterOrEqual(ComparisonOperatorType value) {
        this.greaterOrEqual = value;
    }

    /**
     * Gets the value of the contains property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contains property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContains().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StringOperatorType }
     * 
     * 
     */
    public List<StringOperatorType> getContains() {
        if (contains == null) {
            contains = new ArrayList<StringOperatorType>();
        }
        return this.contains;
    }

    /**
     * Gets the value of the like property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the like property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLike().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StringOperatorType }
     * 
     * 
     */
    public List<StringOperatorType> getLike() {
        if (like == null) {
            like = new ArrayList<StringOperatorType>();
        }
        return this.like;
    }

    /**
     * Gets the value of the isNull property.
     * 
     * @return
     *     possible object is
     *     {@link NullOperatorType }
     *     
     */
    public NullOperatorType getIsNull() {
        return isNull;
    }

    /**
     * Sets the value of the isNull property.
     * 
     * @param value
     *     allowed object is
     *     {@link NullOperatorType }
     *     
     */
    public void setIsNull(NullOperatorType value) {
        this.isNull = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

    /**
     * Gets the value of the namespace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets the value of the namespace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNamespace(String value) {
        this.namespace = value;
    }

    /**
     * Gets the value of the localName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Sets the value of the localName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalName(String value) {
        this.localName = value;
    }

    /**
     * Gets the value of the recordMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRecordMetadata() {
        if (recordMetadata == null) {
            return false;
        } else {
            return recordMetadata;
        }
    }

    /**
     * Sets the value of the recordMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRecordMetadata(Boolean value) {
        this.recordMetadata = value;
    }

    /**
     * Gets the value of the matchAny property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMatchAny() {
        if (matchAny == null) {
            return false;
        } else {
            return matchAny;
        }
    }

    /**
     * Sets the value of the matchAny property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchAny(Boolean value) {
        this.matchAny = value;
    }

}
