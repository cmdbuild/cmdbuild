
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="itemTemplate" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}ItemTemplateType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="relationshipTemplate" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}RelationshipTemplateType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryType", propOrder = {
    "itemTemplate",
    "relationshipTemplate"
})
public class QueryType {

    protected List<ItemTemplateType> itemTemplate;
    protected List<RelationshipTemplateType> relationshipTemplate;

    /**
     * Gets the value of the itemTemplate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemTemplate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemTemplate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemTemplateType }
     * 
     * 
     */
    public List<ItemTemplateType> getItemTemplate() {
        if (itemTemplate == null) {
            itemTemplate = new ArrayList<ItemTemplateType>();
        }
        return this.itemTemplate;
    }

    /**
     * Gets the value of the relationshipTemplate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relationshipTemplate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelationshipTemplate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelationshipTemplateType }
     * 
     * 
     */
    public List<RelationshipTemplateType> getRelationshipTemplate() {
        if (relationshipTemplate == null) {
            relationshipTemplate = new ArrayList<RelationshipTemplateType>();
        }
        return this.relationshipTemplate;
    }

}
