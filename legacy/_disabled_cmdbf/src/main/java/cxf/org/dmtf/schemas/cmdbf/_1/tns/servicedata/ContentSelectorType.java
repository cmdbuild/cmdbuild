
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for ContentSelectorType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContentSelectorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="selectedRecordType" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}SelectedRecordTypeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="xpathSelector" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}XPathExpressionType" minOccurs="0"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContentSelectorType", propOrder = {
    "selectedRecordType",
    "xpathSelector",
    "any"
})
public class ContentSelectorType {

    protected List<SelectedRecordTypeType> selectedRecordType;
    protected XPathExpressionType xpathSelector;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the selectedRecordType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectedRecordType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectedRecordType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelectedRecordTypeType }
     * 
     * 
     */
    public List<SelectedRecordTypeType> getSelectedRecordType() {
        if (selectedRecordType == null) {
            selectedRecordType = new ArrayList<SelectedRecordTypeType>();
        }
        return this.selectedRecordType;
    }

    /**
     * Gets the value of the xpathSelector property.
     * 
     * @return
     *     possible object is
     *     {@link XPathExpressionType }
     *     
     */
    public XPathExpressionType getXpathSelector() {
        return xpathSelector;
    }

    /**
     * Sets the value of the xpathSelector property.
     * 
     * @param value
     *     allowed object is
     *     {@link XPathExpressionType }
     *     
     */
    public void setXpathSelector(XPathExpressionType value) {
        this.xpathSelector = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
