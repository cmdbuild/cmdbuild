
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeregisterResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeregisterResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DeregisterInstanceResponse" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}DeregisterInstanceResponseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeregisterResponseType", propOrder = {
    "deregisterInstanceResponse"
})
public class DeregisterResponseType {

    @XmlElement(name = "DeregisterInstanceResponse")
    protected List<DeregisterInstanceResponseType> deregisterInstanceResponse;

    /**
     * Gets the value of the deregisterInstanceResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deregisterInstanceResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeregisterInstanceResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeregisterInstanceResponseType }
     * 
     * 
     */
    public List<DeregisterInstanceResponseType> getDeregisterInstanceResponse() {
        if (deregisterInstanceResponse == null) {
            deregisterInstanceResponse = new ArrayList<DeregisterInstanceResponseType>();
        }
        return this.deregisterInstanceResponse;
    }

}
