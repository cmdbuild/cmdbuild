
package org.dmtf.schemas.cmdbf._1.tns.query;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.1.10
 * 2018-05-16T09:19:51.218+02:00
 * Generated source version: 3.1.10
 */

@WebFault(name = "UnsupportedConstraintFault", targetNamespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData")
public class UnsupportedConstraintFault extends Exception {
    
    private org.dmtf.schemas.cmdbf._1.tns.servicedata.UnsupportedConstraintFault unsupportedConstraintFault;

    public UnsupportedConstraintFault() {
        super();
    }
    
    public UnsupportedConstraintFault(String message) {
        super(message);
    }
    
    public UnsupportedConstraintFault(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedConstraintFault(String message, org.dmtf.schemas.cmdbf._1.tns.servicedata.UnsupportedConstraintFault unsupportedConstraintFault) {
        super(message);
        this.unsupportedConstraintFault = unsupportedConstraintFault;
    }

    public UnsupportedConstraintFault(String message, org.dmtf.schemas.cmdbf._1.tns.servicedata.UnsupportedConstraintFault unsupportedConstraintFault, Throwable cause) {
        super(message, cause);
        this.unsupportedConstraintFault = unsupportedConstraintFault;
    }

    public org.dmtf.schemas.cmdbf._1.tns.servicedata.UnsupportedConstraintFault getFaultInfo() {
        return this.unsupportedConstraintFault;
    }
}
