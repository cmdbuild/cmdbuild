
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeregisterInstanceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeregisterInstanceResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="instanceId" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}MdrScopedIdType"/&gt;
 *         &lt;element name="accepted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="declined" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}DeclinedType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeregisterInstanceResponseType", propOrder = {
    "instanceId",
    "accepted",
    "declined"
})
public class DeregisterInstanceResponseType {

    @XmlElement(required = true)
    protected MdrScopedIdType instanceId;
    protected String accepted;
    protected DeclinedType declined;

    /**
     * Gets the value of the instanceId property.
     * 
     * @return
     *     possible object is
     *     {@link MdrScopedIdType }
     *     
     */
    public MdrScopedIdType getInstanceId() {
        return instanceId;
    }

    /**
     * Sets the value of the instanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdrScopedIdType }
     *     
     */
    public void setInstanceId(MdrScopedIdType value) {
        this.instanceId = value;
    }

    /**
     * Gets the value of the accepted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccepted() {
        return accepted;
    }

    /**
     * Sets the value of the accepted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccepted(String value) {
        this.accepted = value;
    }

    /**
     * Gets the value of the declined property.
     * 
     * @return
     *     possible object is
     *     {@link DeclinedType }
     *     
     */
    public DeclinedType getDeclined() {
        return declined;
    }

    /**
     * Sets the value of the declined property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeclinedType }
     *     
     */
    public void setDeclined(DeclinedType value) {
        this.declined = value;
    }

}
