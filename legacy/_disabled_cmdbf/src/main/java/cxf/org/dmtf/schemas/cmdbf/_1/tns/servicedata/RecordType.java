
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3c.dom.Element;


/**
 * <p>Java class for RecordType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecordType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;any processContents='skip' namespace='##other' minOccurs="0"/&gt;
 *         &lt;element ref="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}propertySet" minOccurs="0"/&gt;
 *         &lt;element name="recordMetadata" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="recordId" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *                   &lt;element name="lastModified" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *                   &lt;element name="baselineId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="snapshotId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecordType", propOrder = {
    "any",
    "propertySet",
    "recordMetadata"
})
public class RecordType {

    @XmlAnyElement
    protected Element any;
    protected PropertySetType propertySet;
    protected RecordType.RecordMetadata recordMetadata;

    /**
     * Gets the value of the any property.
     * 
     * @return
     *     possible object is
     *     {@link Element }
     *     
     */
    public Element getAny() {
        return any;
    }

    /**
     * Sets the value of the any property.
     * 
     * @param value
     *     allowed object is
     *     {@link Element }
     *     
     */
    public void setAny(Element value) {
        this.any = value;
    }

    /**
     * Gets the value of the propertySet property.
     * 
     * @return
     *     possible object is
     *     {@link PropertySetType }
     *     
     */
    public PropertySetType getPropertySet() {
        return propertySet;
    }

    /**
     * Sets the value of the propertySet property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropertySetType }
     *     
     */
    public void setPropertySet(PropertySetType value) {
        this.propertySet = value;
    }

    /**
     * Gets the value of the recordMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link RecordType.RecordMetadata }
     *     
     */
    public RecordType.RecordMetadata getRecordMetadata() {
        return recordMetadata;
    }

    /**
     * Sets the value of the recordMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordType.RecordMetadata }
     *     
     */
    public void setRecordMetadata(RecordType.RecordMetadata value) {
        this.recordMetadata = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="recordId" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
     *         &lt;element name="lastModified" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
     *         &lt;element name="baselineId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="snapshotId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recordId",
        "lastModified",
        "baselineId",
        "snapshotId",
        "any"
    })
    public static class RecordMetadata {

        @XmlSchemaType(name = "anyURI")
        protected String recordId;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastModified;
        protected String baselineId;
        protected String snapshotId;
        @XmlAnyElement(lax = true)
        protected List<Object> any;

        /**
         * Gets the value of the recordId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecordId() {
            return recordId;
        }

        /**
         * Sets the value of the recordId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecordId(String value) {
            this.recordId = value;
        }

        /**
         * Gets the value of the lastModified property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastModified() {
            return lastModified;
        }

        /**
         * Sets the value of the lastModified property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastModified(XMLGregorianCalendar value) {
            this.lastModified = value;
        }

        /**
         * Gets the value of the baselineId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBaselineId() {
            return baselineId;
        }

        /**
         * Sets the value of the baselineId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBaselineId(String value) {
            this.baselineId = value;
        }

        /**
         * Gets the value of the snapshotId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSnapshotId() {
            return snapshotId;
        }

        /**
         * Sets the value of the snapshotId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSnapshotId(String value) {
            this.snapshotId = value;
        }

        /**
         * Gets the value of the any property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the any property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAny().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * {@link Element }
         * 
         * 
         */
        public List<Object> getAny() {
            if (any == null) {
                any = new ArrayList<Object>();
            }
            return this.any;
        }

    }

}
