
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for RecordConstraintType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecordConstraintType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="recordType" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}QNameType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="propertyValue" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}PropertyValueType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="xpathConstraint" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}XPathExpressionType" minOccurs="0"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecordConstraintType", propOrder = {
    "recordType",
    "propertyValue",
    "xpathConstraint",
    "any"
})
public class RecordConstraintType {

    protected List<QNameType> recordType;
    protected List<PropertyValueType> propertyValue;
    protected XPathExpressionType xpathConstraint;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the recordType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recordType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecordType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QNameType }
     * 
     * 
     */
    public List<QNameType> getRecordType() {
        if (recordType == null) {
            recordType = new ArrayList<QNameType>();
        }
        return this.recordType;
    }

    /**
     * Gets the value of the propertyValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the propertyValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPropertyValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyValueType }
     * 
     * 
     */
    public List<PropertyValueType> getPropertyValue() {
        if (propertyValue == null) {
            propertyValue = new ArrayList<PropertyValueType>();
        }
        return this.propertyValue;
    }

    /**
     * Gets the value of the xpathConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link XPathExpressionType }
     *     
     */
    public XPathExpressionType getXpathConstraint() {
        return xpathConstraint;
    }

    /**
     * Sets the value of the xpathConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link XPathExpressionType }
     *     
     */
    public void setXpathConstraint(XPathExpressionType value) {
        this.xpathConstraint = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
