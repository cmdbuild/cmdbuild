
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RelationshipType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationshipType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="source" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}MdrScopedIdType"/&gt;
 *         &lt;element name="target" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}MdrScopedIdType"/&gt;
 *         &lt;element ref="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}record" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}instanceId" maxOccurs="unbounded"/&gt;
 *         &lt;element name="additionalRecordType" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}QNameType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationshipType", propOrder = {
    "source",
    "target",
    "record",
    "instanceId",
    "additionalRecordType"
})
public class RelationshipType {

    @XmlElement(required = true)
    protected MdrScopedIdType source;
    @XmlElement(required = true)
    protected MdrScopedIdType target;
    protected List<RecordType> record;
    @XmlElement(required = true)
    protected List<MdrScopedIdType> instanceId;
    protected List<QNameType> additionalRecordType;

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link MdrScopedIdType }
     *     
     */
    public MdrScopedIdType getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdrScopedIdType }
     *     
     */
    public void setSource(MdrScopedIdType value) {
        this.source = value;
    }

    /**
     * Gets the value of the target property.
     * 
     * @return
     *     possible object is
     *     {@link MdrScopedIdType }
     *     
     */
    public MdrScopedIdType getTarget() {
        return target;
    }

    /**
     * Sets the value of the target property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdrScopedIdType }
     *     
     */
    public void setTarget(MdrScopedIdType value) {
        this.target = value;
    }

    /**
     * Gets the value of the record property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the record property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RecordType }
     * 
     * 
     */
    public List<RecordType> getRecord() {
        if (record == null) {
            record = new ArrayList<RecordType>();
        }
        return this.record;
    }

    /**
     * Gets the value of the instanceId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instanceId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstanceId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdrScopedIdType }
     * 
     * 
     */
    public List<MdrScopedIdType> getInstanceId() {
        if (instanceId == null) {
            instanceId = new ArrayList<MdrScopedIdType>();
        }
        return this.instanceId;
    }

    /**
     * Gets the value of the additionalRecordType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalRecordType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalRecordType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QNameType }
     * 
     * 
     */
    public List<QNameType> getAdditionalRecordType() {
        if (additionalRecordType == null) {
            additionalRecordType = new ArrayList<QNameType>();
        }
        return this.additionalRecordType;
    }

}
