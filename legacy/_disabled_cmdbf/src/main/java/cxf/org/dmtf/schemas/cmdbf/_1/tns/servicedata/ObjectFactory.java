
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.dmtf.schemas.cmdbf._1.tns.servicedata package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Query_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "query");
    private final static QName _QueryResult_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "queryResult");
    private final static QName _RegisterRequest_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "registerRequest");
    private final static QName _RegisterResponse_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "registerResponse");
    private final static QName _DeregisterRequest_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "deregisterRequest");
    private final static QName _DeregisterResponse_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "deregisterResponse");
    private final static QName _Item_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "item");
    private final static QName _Relationship_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "relationship");
    private final static QName _Record_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "record");
    private final static QName _InstanceId_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "instanceId");
    private final static QName _PropertySet_QNAME = new QName("http://schemas.dmtf.org/cmdbf/1/tns/serviceData", "propertySet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.dmtf.schemas.cmdbf._1.tns.servicedata
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InvalidPropertyTypeFault }
     * 
     */
    public InvalidPropertyTypeFault createInvalidPropertyTypeFault() {
        return new InvalidPropertyTypeFault();
    }

    /**
     * Create an instance of {@link UnsupportedConstraintFault }
     * 
     */
    public UnsupportedConstraintFault createUnsupportedConstraintFault() {
        return new UnsupportedConstraintFault();
    }

    /**
     * Create an instance of {@link UnsupportedSelectorFault }
     * 
     */
    public UnsupportedSelectorFault createUnsupportedSelectorFault() {
        return new UnsupportedSelectorFault();
    }

    /**
     * Create an instance of {@link UnsupportedRecordTypeFault }
     * 
     */
    public UnsupportedRecordTypeFault createUnsupportedRecordTypeFault() {
        return new UnsupportedRecordTypeFault();
    }

    /**
     * Create an instance of {@link org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType }
     * 
     */
    public org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType createRecordType() {
        return new org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType();
    }

    /**
     * Create an instance of {@link QueryType }
     * 
     */
    public QueryType createQueryType() {
        return new QueryType();
    }

    /**
     * Create an instance of {@link QueryResultType }
     * 
     */
    public QueryResultType createQueryResultType() {
        return new QueryResultType();
    }

    /**
     * Create an instance of {@link RegisterRequestType }
     * 
     */
    public RegisterRequestType createRegisterRequestType() {
        return new RegisterRequestType();
    }

    /**
     * Create an instance of {@link RegisterResponseType }
     * 
     */
    public RegisterResponseType createRegisterResponseType() {
        return new RegisterResponseType();
    }

    /**
     * Create an instance of {@link DeregisterRequestType }
     * 
     */
    public DeregisterRequestType createDeregisterRequestType() {
        return new DeregisterRequestType();
    }

    /**
     * Create an instance of {@link DeregisterResponseType }
     * 
     */
    public DeregisterResponseType createDeregisterResponseType() {
        return new DeregisterResponseType();
    }

    /**
     * Create an instance of {@link UnknownTemplateIDFault }
     * 
     */
    public UnknownTemplateIDFault createUnknownTemplateIDFault() {
        return new UnknownTemplateIDFault();
    }

    /**
     * Create an instance of {@link InvalidPropertyTypeFault.PropertyName }
     * 
     */
    public InvalidPropertyTypeFault.PropertyName createInvalidPropertyTypeFaultPropertyName() {
        return new InvalidPropertyTypeFault.PropertyName();
    }

    /**
     * Create an instance of {@link XPathErrorFault }
     * 
     */
    public XPathErrorFault createXPathErrorFault() {
        return new XPathErrorFault();
    }

    /**
     * Create an instance of {@link UnsupportedConstraintFault.Constraint }
     * 
     */
    public UnsupportedConstraintFault.Constraint createUnsupportedConstraintFaultConstraint() {
        return new UnsupportedConstraintFault.Constraint();
    }

    /**
     * Create an instance of {@link UnsupportedSelectorFault.Selector }
     * 
     */
    public UnsupportedSelectorFault.Selector createUnsupportedSelectorFaultSelector() {
        return new UnsupportedSelectorFault.Selector();
    }

    /**
     * Create an instance of {@link QueryErrorFault }
     * 
     */
    public QueryErrorFault createQueryErrorFault() {
        return new QueryErrorFault();
    }

    /**
     * Create an instance of {@link ExpensiveQueryErrorFault }
     * 
     */
    public ExpensiveQueryErrorFault createExpensiveQueryErrorFault() {
        return new ExpensiveQueryErrorFault();
    }

    /**
     * Create an instance of {@link InvalidRecordFault }
     * 
     */
    public InvalidRecordFault createInvalidRecordFault() {
        return new InvalidRecordFault();
    }

    /**
     * Create an instance of {@link UnsupportedRecordTypeFault.RecordType }
     * 
     */
    public UnsupportedRecordTypeFault.RecordType createUnsupportedRecordTypeFaultRecordType() {
        return new UnsupportedRecordTypeFault.RecordType();
    }

    /**
     * Create an instance of {@link InvalidMDRFault }
     * 
     */
    public InvalidMDRFault createInvalidMDRFault() {
        return new InvalidMDRFault();
    }

    /**
     * Create an instance of {@link RegistrationErrorFault }
     * 
     */
    public RegistrationErrorFault createRegistrationErrorFault() {
        return new RegistrationErrorFault();
    }

    /**
     * Create an instance of {@link DeregistrationErrorFault }
     * 
     */
    public DeregistrationErrorFault createDeregistrationErrorFault() {
        return new DeregistrationErrorFault();
    }

    /**
     * Create an instance of {@link ItemType }
     * 
     */
    public ItemType createItemType() {
        return new ItemType();
    }

    /**
     * Create an instance of {@link RelationshipType }
     * 
     */
    public RelationshipType createRelationshipType() {
        return new RelationshipType();
    }

    /**
     * Create an instance of {@link MdrScopedIdType }
     * 
     */
    public MdrScopedIdType createMdrScopedIdType() {
        return new MdrScopedIdType();
    }

    /**
     * Create an instance of {@link PropertySetType }
     * 
     */
    public PropertySetType createPropertySetType() {
        return new PropertySetType();
    }

    /**
     * Create an instance of {@link ItemTemplateType }
     * 
     */
    public ItemTemplateType createItemTemplateType() {
        return new ItemTemplateType();
    }

    /**
     * Create an instance of {@link RelationshipTemplateType }
     * 
     */
    public RelationshipTemplateType createRelationshipTemplateType() {
        return new RelationshipTemplateType();
    }

    /**
     * Create an instance of {@link RelationshipRefType }
     * 
     */
    public RelationshipRefType createRelationshipRefType() {
        return new RelationshipRefType();
    }

    /**
     * Create an instance of {@link DepthLimitType }
     * 
     */
    public DepthLimitType createDepthLimitType() {
        return new DepthLimitType();
    }

    /**
     * Create an instance of {@link ContentSelectorType }
     * 
     */
    public ContentSelectorType createContentSelectorType() {
        return new ContentSelectorType();
    }

    /**
     * Create an instance of {@link SelectedRecordTypeType }
     * 
     */
    public SelectedRecordTypeType createSelectedRecordTypeType() {
        return new SelectedRecordTypeType();
    }

    /**
     * Create an instance of {@link InstanceIdConstraintType }
     * 
     */
    public InstanceIdConstraintType createInstanceIdConstraintType() {
        return new InstanceIdConstraintType();
    }

    /**
     * Create an instance of {@link RecordConstraintType }
     * 
     */
    public RecordConstraintType createRecordConstraintType() {
        return new RecordConstraintType();
    }

    /**
     * Create an instance of {@link PropertyValueType }
     * 
     */
    public PropertyValueType createPropertyValueType() {
        return new PropertyValueType();
    }

    /**
     * Create an instance of {@link ComparisonOperatorType }
     * 
     */
    public ComparisonOperatorType createComparisonOperatorType() {
        return new ComparisonOperatorType();
    }

    /**
     * Create an instance of {@link StringOperatorType }
     * 
     */
    public StringOperatorType createStringOperatorType() {
        return new StringOperatorType();
    }

    /**
     * Create an instance of {@link EqualOperatorType }
     * 
     */
    public EqualOperatorType createEqualOperatorType() {
        return new EqualOperatorType();
    }

    /**
     * Create an instance of {@link NullOperatorType }
     * 
     */
    public NullOperatorType createNullOperatorType() {
        return new NullOperatorType();
    }

    /**
     * Create an instance of {@link XPathExpressionType }
     * 
     */
    public XPathExpressionType createXPathExpressionType() {
        return new XPathExpressionType();
    }

    /**
     * Create an instance of {@link PrefixMappingType }
     * 
     */
    public PrefixMappingType createPrefixMappingType() {
        return new PrefixMappingType();
    }

    /**
     * Create an instance of {@link NodesType }
     * 
     */
    public NodesType createNodesType() {
        return new NodesType();
    }

    /**
     * Create an instance of {@link EdgesType }
     * 
     */
    public EdgesType createEdgesType() {
        return new EdgesType();
    }

    /**
     * Create an instance of {@link ItemListType }
     * 
     */
    public ItemListType createItemListType() {
        return new ItemListType();
    }

    /**
     * Create an instance of {@link RelationshipListType }
     * 
     */
    public RelationshipListType createRelationshipListType() {
        return new RelationshipListType();
    }

    /**
     * Create an instance of {@link MdrScopedIdListType }
     * 
     */
    public MdrScopedIdListType createMdrScopedIdListType() {
        return new MdrScopedIdListType();
    }

    /**
     * Create an instance of {@link RegisterInstanceResponseType }
     * 
     */
    public RegisterInstanceResponseType createRegisterInstanceResponseType() {
        return new RegisterInstanceResponseType();
    }

    /**
     * Create an instance of {@link DeregisterInstanceResponseType }
     * 
     */
    public DeregisterInstanceResponseType createDeregisterInstanceResponseType() {
        return new DeregisterInstanceResponseType();
    }

    /**
     * Create an instance of {@link AcceptedType }
     * 
     */
    public AcceptedType createAcceptedType() {
        return new AcceptedType();
    }

    /**
     * Create an instance of {@link DeclinedType }
     * 
     */
    public DeclinedType createDeclinedType() {
        return new DeclinedType();
    }

    /**
     * Create an instance of {@link QNameType }
     * 
     */
    public QNameType createQNameType() {
        return new QNameType();
    }

    /**
     * Create an instance of {@link org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType.RecordMetadata }
     * 
     */
    public org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType.RecordMetadata createRecordTypeRecordMetadata() {
        return new org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType.RecordMetadata();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "query")
    public JAXBElement<QueryType> createQuery(QueryType value) {
        return new JAXBElement<QueryType>(_Query_QNAME, QueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryResultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "queryResult")
    public JAXBElement<QueryResultType> createQueryResult(QueryResultType value) {
        return new JAXBElement<QueryResultType>(_QueryResult_QNAME, QueryResultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "registerRequest")
    public JAXBElement<RegisterRequestType> createRegisterRequest(RegisterRequestType value) {
        return new JAXBElement<RegisterRequestType>(_RegisterRequest_QNAME, RegisterRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "registerResponse")
    public JAXBElement<RegisterResponseType> createRegisterResponse(RegisterResponseType value) {
        return new JAXBElement<RegisterResponseType>(_RegisterResponse_QNAME, RegisterResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeregisterRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "deregisterRequest")
    public JAXBElement<DeregisterRequestType> createDeregisterRequest(DeregisterRequestType value) {
        return new JAXBElement<DeregisterRequestType>(_DeregisterRequest_QNAME, DeregisterRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeregisterResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "deregisterResponse")
    public JAXBElement<DeregisterResponseType> createDeregisterResponse(DeregisterResponseType value) {
        return new JAXBElement<DeregisterResponseType>(_DeregisterResponse_QNAME, DeregisterResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "item")
    public JAXBElement<ItemType> createItem(ItemType value) {
        return new JAXBElement<ItemType>(_Item_QNAME, ItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelationshipType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "relationship")
    public JAXBElement<RelationshipType> createRelationship(RelationshipType value) {
        return new JAXBElement<RelationshipType>(_Relationship_QNAME, RelationshipType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "record")
    public JAXBElement<org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType> createRecord(org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType value) {
        return new JAXBElement<org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType>(_Record_QNAME, org.dmtf.schemas.cmdbf._1.tns.servicedata.RecordType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MdrScopedIdType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "instanceId")
    public JAXBElement<MdrScopedIdType> createInstanceId(MdrScopedIdType value) {
        return new JAXBElement<MdrScopedIdType>(_InstanceId_QNAME, MdrScopedIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertySetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.dmtf.org/cmdbf/1/tns/serviceData", name = "propertySet")
    public JAXBElement<PropertySetType> createPropertySet(PropertySetType value) {
        return new JAXBElement<PropertySetType>(_PropertySet_QNAME, PropertySetType.class, null, value);
    }

}
