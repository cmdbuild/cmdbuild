
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegisterRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegisterRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mdrId" type="{http://www.w3.org/2001/XMLSchema}anyURI"/&gt;
 *         &lt;element name="itemList" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}ItemListType" minOccurs="0"/&gt;
 *         &lt;element name="relationshipList" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}RelationshipListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegisterRequestType", propOrder = {
    "mdrId",
    "itemList",
    "relationshipList"
})
public class RegisterRequestType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String mdrId;
    protected ItemListType itemList;
    protected RelationshipListType relationshipList;

    /**
     * Gets the value of the mdrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdrId() {
        return mdrId;
    }

    /**
     * Sets the value of the mdrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdrId(String value) {
        this.mdrId = value;
    }

    /**
     * Gets the value of the itemList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemListType }
     *     
     */
    public ItemListType getItemList() {
        return itemList;
    }

    /**
     * Sets the value of the itemList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemListType }
     *     
     */
    public void setItemList(ItemListType value) {
        this.itemList = value;
    }

    /**
     * Gets the value of the relationshipList property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipListType }
     *     
     */
    public RelationshipListType getRelationshipList() {
        return relationshipList;
    }

    /**
     * Sets the value of the relationshipList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipListType }
     *     
     */
    public void setRelationshipList(RelationshipListType value) {
        this.relationshipList = value;
    }

}
