
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3c.dom.Element;


/**
 * <p>Java class for RelationshipTemplateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationshipTemplateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}Constraints"/&gt;
 *         &lt;element name="sourceTemplate" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}RelationshipRefType" minOccurs="0"/&gt;
 *         &lt;element name="targetTemplate" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}RelationshipRefType" minOccurs="0"/&gt;
 *         &lt;element name="depthLimit" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}DepthLimitType" minOccurs="0"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *       &lt;attribute name="suppressFromResult" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationshipTemplateType", propOrder = {
    "contentSelector",
    "instanceIdConstraint",
    "recordConstraint",
    "sourceTemplate",
    "targetTemplate",
    "depthLimit",
    "any"
})
public class RelationshipTemplateType {

    protected ContentSelectorType contentSelector;
    protected InstanceIdConstraintType instanceIdConstraint;
    protected List<RecordConstraintType> recordConstraint;
    protected RelationshipRefType sourceTemplate;
    protected RelationshipRefType targetTemplate;
    protected DepthLimitType depthLimit;
    @XmlAnyElement(lax = true)
    protected List<Object> any;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "suppressFromResult")
    protected Boolean suppressFromResult;

    /**
     * Gets the value of the contentSelector property.
     * 
     * @return
     *     possible object is
     *     {@link ContentSelectorType }
     *     
     */
    public ContentSelectorType getContentSelector() {
        return contentSelector;
    }

    /**
     * Sets the value of the contentSelector property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContentSelectorType }
     *     
     */
    public void setContentSelector(ContentSelectorType value) {
        this.contentSelector = value;
    }

    /**
     * Gets the value of the instanceIdConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdConstraintType }
     *     
     */
    public InstanceIdConstraintType getInstanceIdConstraint() {
        return instanceIdConstraint;
    }

    /**
     * Sets the value of the instanceIdConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdConstraintType }
     *     
     */
    public void setInstanceIdConstraint(InstanceIdConstraintType value) {
        this.instanceIdConstraint = value;
    }

    /**
     * Gets the value of the recordConstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recordConstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecordConstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RecordConstraintType }
     * 
     * 
     */
    public List<RecordConstraintType> getRecordConstraint() {
        if (recordConstraint == null) {
            recordConstraint = new ArrayList<RecordConstraintType>();
        }
        return this.recordConstraint;
    }

    /**
     * Gets the value of the sourceTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipRefType }
     *     
     */
    public RelationshipRefType getSourceTemplate() {
        return sourceTemplate;
    }

    /**
     * Sets the value of the sourceTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipRefType }
     *     
     */
    public void setSourceTemplate(RelationshipRefType value) {
        this.sourceTemplate = value;
    }

    /**
     * Gets the value of the targetTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipRefType }
     *     
     */
    public RelationshipRefType getTargetTemplate() {
        return targetTemplate;
    }

    /**
     * Sets the value of the targetTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipRefType }
     *     
     */
    public void setTargetTemplate(RelationshipRefType value) {
        this.targetTemplate = value;
    }

    /**
     * Gets the value of the depthLimit property.
     * 
     * @return
     *     possible object is
     *     {@link DepthLimitType }
     *     
     */
    public DepthLimitType getDepthLimit() {
        return depthLimit;
    }

    /**
     * Sets the value of the depthLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link DepthLimitType }
     *     
     */
    public void setDepthLimit(DepthLimitType value) {
        this.depthLimit = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the suppressFromResult property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSuppressFromResult() {
        if (suppressFromResult == null) {
            return false;
        } else {
            return suppressFromResult;
        }
    }

    /**
     * Sets the value of the suppressFromResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuppressFromResult(Boolean value) {
        this.suppressFromResult = value;
    }

}
