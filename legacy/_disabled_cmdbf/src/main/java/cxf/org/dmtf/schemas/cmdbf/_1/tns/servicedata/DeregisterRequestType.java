
package org.dmtf.schemas.cmdbf._1.tns.servicedata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeregisterRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeregisterRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mdrId" type="{http://www.w3.org/2001/XMLSchema}anyURI"/&gt;
 *         &lt;element name="itemIdList" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}MdrScopedIdListType" minOccurs="0"/&gt;
 *         &lt;element name="relationshipIdList" type="{http://schemas.dmtf.org/cmdbf/1/tns/serviceData}MdrScopedIdListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeregisterRequestType", propOrder = {
    "mdrId",
    "itemIdList",
    "relationshipIdList"
})
public class DeregisterRequestType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String mdrId;
    protected MdrScopedIdListType itemIdList;
    protected MdrScopedIdListType relationshipIdList;

    /**
     * Gets the value of the mdrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdrId() {
        return mdrId;
    }

    /**
     * Sets the value of the mdrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdrId(String value) {
        this.mdrId = value;
    }

    /**
     * Gets the value of the itemIdList property.
     * 
     * @return
     *     possible object is
     *     {@link MdrScopedIdListType }
     *     
     */
    public MdrScopedIdListType getItemIdList() {
        return itemIdList;
    }

    /**
     * Sets the value of the itemIdList property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdrScopedIdListType }
     *     
     */
    public void setItemIdList(MdrScopedIdListType value) {
        this.itemIdList = value;
    }

    /**
     * Gets the value of the relationshipIdList property.
     * 
     * @return
     *     possible object is
     *     {@link MdrScopedIdListType }
     *     
     */
    public MdrScopedIdListType getRelationshipIdList() {
        return relationshipIdList;
    }

    /**
     * Sets the value of the relationshipIdList property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdrScopedIdListType }
     *     
     */
    public void setRelationshipIdList(MdrScopedIdListType value) {
        this.relationshipIdList = value;
    }

}
