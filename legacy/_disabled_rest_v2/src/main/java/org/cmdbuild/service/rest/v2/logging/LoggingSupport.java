package org.cmdbuild.service.rest.v2.logging;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public interface LoggingSupport {

//	protected final Logger logger = LoggerFactory.getLogger(getClass());
	final Logger logger = LoggerFactory.getLogger(LoggingSupport.class.getPackage().getName());

}
