package org.cmdbuild.service.rest.v2.cxf;

import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Maps.transformEntries;
import static org.cmdbuild.service.rest.v2.cxf.util.Json.safeJsonArray;
import static org.cmdbuild.service.rest.v2.cxf.util.Json.safeJsonObject;
import static org.cmdbuild.service.rest.v2.model.Models.newCard;
import static org.cmdbuild.service.rest.v2.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseMultiple;

import java.util.Map;
import java.util.Map.Entry;

import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.service.rest.v2.Cql;
import org.cmdbuild.service.rest.v2.model.Card;
import org.cmdbuild.service.rest.v2.model.ResponseMultiple;

import com.google.common.collect.Maps.EntryTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.service.rest.v2.cxf.serialization.Converter.toClient;

@Component("v2_cql")
public class CxfCql implements Cql {

	@Autowired
	private DataAccessService dataAccessLogic;

	@Override
	public ResponseMultiple<Card> read(final String filter, final String sort, final Integer limit, final Integer offset) {
		final QueryOptionsImpl queryOptions = QueryOptionsImpl.builder() //
				.filter(safeJsonObject(filter)) //
				.orderBy(safeJsonArray(sort)) //
				.limit(limit) //
				.offset(offset) //
				.build();
		final PagedElements<org.cmdbuild.dao.beans.Card> response = dataAccessLogic.fetchCards(null, queryOptions);
		final Iterable<Card> elements = from(response.elements()) //
				.transform((final org.cmdbuild.dao.beans.Card input) -> newCard() //
				.withType(input.getType().getName()) //
				.withId(input.getId()) //
				.withValues(adaptOutputValues(input.getType(), input)) //
				.build());
		return newResponseMultiple(Card.class) //
				.withElements(elements) //
				.withMetadata(newMetadata() //
						.withTotal(Long.valueOf(response.totalSize())) //
						.build()) //
				.build();
	}

	private Iterable<? extends Entry<String, Object>> adaptOutputValues(final Classe targetClass,
			final org.cmdbuild.dao.beans.Card card) {
		return adaptOutputValues(targetClass, card.getAllValuesAsMap());
	}

	private Iterable<? extends Entry<String, Object>> adaptOutputValues(final Classe targetClass,
			final Map<String, Object> values) {
		return transformEntries(values, new EntryTransformer<String, Object, Object>() {

			@Override
			public Object transformEntry(final String key, final Object value) {
				final Attribute attribute = targetClass.getAttributeOrNull(key);
				final Object _value;
				if (attribute == null) {
					_value = value;
				} else {
					final CardAttributeType<?> attributeType = attribute.getType();
					_value = toClient().convert(attributeType, value);
				}
				return _value;
			}

		}).entrySet();
	}

}
