package org.cmdbuild.service.rest.v2.cxf.configuration;

import org.cmdbuild.service.rest.v2.cxf.ErrorHandler;
import org.cmdbuild.service.rest.v2.cxf.HeaderResponseHandler;
import org.cmdbuild.service.rest.v2.cxf.WebApplicationExceptionErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

@Configuration
public class ServicesV2 {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ApplicationContextHelperV2 helper;

	@Bean
	protected ErrorHandler v2_errorHandler() {
		return new WebApplicationExceptionErrorHandler();
	}

	@Bean
	public HeaderResponseHandler v2_headerResponseHandler() {
		return new HeaderResponseHandler();
	}

}
