package org.cmdbuild.service.rest.v2;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.cmdbuild.service.rest.v2.constants.Serialization.ID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.cmdbuild.service.rest.v2.model.ResponseSingle;
import org.cmdbuild.service.rest.v2.model.Session;

/**
 * handles user sessions (login and stuff)
 */
@Path("sessions/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public interface Sessions {

	/**
	 * create a new session (ie: login)
	 *
	 * @param session require only {@link Session#username} and
	 * {@link Session#password} params to be set
	 * @return
	 */
	@POST
	@Path(EMPTY)
	public ResponseSingle<Session> create(Session session);

	/**
	 *
	 * @param id session id
	 * @return
	 */
	@GET
	@Path("{" + ID + "}/")
	public ResponseSingle<Session> read(@PathParam(ID) String id);

	/**
	 *
	 * @param id session id
	 * @param session require only {@link Session#role} and optionally
	 * {@link Session#tenant} and/or {@link Session#activeTenants}
	 * @return
	 */
	@PUT
	@Path("{" + ID + "}/")
	public ResponseSingle<Session> update(@PathParam(ID) String id, Session session);

	/**
	 * delete session (logout)
	 *
	 * @param id
	 */
	@DELETE
	@Path("{" + ID + "}/")
	public void delete(@PathParam(ID) String id);

}
