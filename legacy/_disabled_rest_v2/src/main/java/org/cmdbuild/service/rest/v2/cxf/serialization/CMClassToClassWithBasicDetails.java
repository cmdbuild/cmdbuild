package org.cmdbuild.service.rest.v2.cxf.serialization;

import org.cmdbuild.service.rest.v2.model.ClassWithBasicDetails;

import com.google.common.base.Function;
import static org.cmdbuild.service.rest.v2.model.Models.newClassWithBasicDetails;
import org.cmdbuild.dao.entrytype.Classe;

public class CMClassToClassWithBasicDetails implements Function<Classe, ClassWithBasicDetails> {

	private static final CMClassToClassWithBasicDetails TO_SIMPLE_CLASS_DETAIL = new CMClassToClassWithBasicDetails();

	public static CMClassToClassWithBasicDetails toBasicDetail() {
		return TO_SIMPLE_CLASS_DETAIL;
	}

	private static final String MISSING_PARENT = null;

	@Override
	public ClassWithBasicDetails apply(final Classe input) {
		final Classe parent = input.getParentOrNull();
		return newClassWithBasicDetails() //
				.withId(input.getName()) //
				.withName(input.getName()) //
				.withDescription(input.getDescription()) //
				.withParent((parent == null) ? MISSING_PARENT : parent.getName()) //
				.thatIsPrototype(input.isSuperclass()) //
				.build();
	}

}
