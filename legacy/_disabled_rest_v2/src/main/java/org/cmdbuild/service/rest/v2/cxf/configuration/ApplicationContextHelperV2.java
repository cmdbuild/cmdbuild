package org.cmdbuild.service.rest.v2.cxf.configuration;

import org.cmdbuild.config.GraphConfiguration;
import org.cmdbuild.lookup.LookupService;
import org.cmdbuild.logic.email.EmailLogic;
import org.cmdbuild.email.EmailTemplateLogic;
import org.cmdbuild.cardfilter.CardFilterServiceImpl;
import org.cmdbuild.workflow.core.LookupHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.cmdbuild.auth.OperationUserSupplier;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.auth.session.SessionService;
import org.cmdbuild.config.CoreConfiguration;
import org.cmdbuild.logic.auth.AuthenticationServiceTwo;
import org.cmdbuild.report.ReportService;
import org.cmdbuild.cardfilter.CardFilterService;
import org.cmdbuild.authorization.AuthorizationService;
import org.cmdbuild.menu.MenuService;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.dao.view.DataView;

@Component
public class ApplicationContextHelperV2 {

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private AutowireCapableBeanFactory autowireCapableBeanFactory;

	public AuthenticationServiceTwo authenticationLogic() {
		return applicationContext.getBean(SessionService.class);
	}

	public CoreConfiguration cmdbuildConfiguration() {
		return applicationContext.getBean(CoreConfiguration.class);
	}

	public DmsService dmsLogic() {
		return applicationContext.getBean(DmsService.class);
	}

	public EmailLogic emailLogic() {
		return applicationContext.getBean(EmailLogic.class);
	}

	public EmailTemplateLogic emailTemplateLogic() {
		return applicationContext.getBean(EmailTemplateLogic.class);
	}

//	public FileLogic fileLogic() {
//		return applicationContext.getBean(FileLogic.class);
//	}
//	public FilesStore filesStore() {
//		return applicationContext.getBean(FilesStore.class);
//	}

	public CardFilterService filterLogic() {
		return applicationContext.getBean(CardFilterServiceImpl.class);
	}

//	public GisService gisLogic() {
//		return applicationContext.getBean(GISLogic.class);
//	}
	public GraphConfiguration graphConfiguration() {
		return applicationContext.getBean(GraphConfiguration.class);
	}

	public LookupHelper lookupHelper() {
		return applicationContext.getBean(LookupHelper.class);
	}

	public LookupService lookupLogic() {
		return applicationContext.getBean(LookupService.class);
	}

	public MenuService menuLogic() {
		return applicationContext.getBean(MenuService.class);
	}

//	public MetadataStoreFactory metadataStoreFactory() {
//		return applicationContext.getBean(MetadataStoreFactory.class);
//	}
//	public NavigationTreeLogic navigationTreeLogic() {
//		return applicationContext.getBean(NavigationTreeLogic.class);
//	}
	public ReportService reportLogic() {
		return applicationContext.getBean(ReportService.class);
	}

	public AuthorizationService securityLogic() {
		return applicationContext.getBean(AuthorizationService.class);
	}

	public SessionService sessionLogic() {
		return applicationContext.getBean(SessionService.class);
	}

	public DataAccessService systemDataAccessLogic() {
//		return applicationContext.getBean(SystemDataAccessLogic.class);
		return applicationContext.getBean(DataAccessService.class);
	}

	public DataView systemDataView() {
		return applicationContext.getBean("systemDataView", DataView.class);
	}
//
//	public CardFilterService temporaryfilterLogic() {
//		return applicationContext.getBean(SessionUserCardFilterRepository.class);
//	}

	public DataAccessService userDataAccessLogic() {
		return applicationContext.getBean(DataAccessService.class);
	}

	public DataView userDataView() {
		return applicationContext.getBean("UserDataView", DataView.class);
	}

	public OperationUserSupplier userStore() {
		return applicationContext.getBean(OperationUserSupplier.class);
	}

	public WorkflowService userWorkflowLogic() {
		return applicationContext.getBean(WorkflowService.class);
	}

	public <T> T autowireBean(T bean) {
		autowireCapableBeanFactory.autowireBean(bean);
		return bean;
	}

}
