/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v2.cxf.serialization;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.cmdbuild.service.rest.v2.constants.Serialization.UNDERSCORED_ID;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.cmdbuild.custompage.CustomPageData;

/**
 *
 */
public class CustomPageJsonBean {

	private String id, name, description;

	@JsonProperty(UNDERSCORED_ID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static CustomPageJsonBean fromCustomPage(CustomPageData customPage) {
		checkNotNull(customPage);
		return new CustomPageJsonBean() {
			{
				setId(customPage.getId().toString());
				setName(customPage.getName());
				setDescription(customPage.getDescription());
			}
		};
	}

}
