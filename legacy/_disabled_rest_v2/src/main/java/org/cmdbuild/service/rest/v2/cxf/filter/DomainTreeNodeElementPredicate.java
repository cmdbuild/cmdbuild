package org.cmdbuild.service.rest.v2.cxf.filter;

import org.cmdbuild.logic.data.access.filter.model.Attr;
import org.cmdbuild.logic.data.access.filter.model.Element;
import org.cmdbuild.navtree.DomainTreeNode;

import com.google.common.base.Predicate;

public class DomainTreeNodeElementPredicate extends ElementPredicate<DomainTreeNode> {

	public DomainTreeNodeElementPredicate(final Element element) {
		super(element);
	}

	@Override
	protected Predicate<DomainTreeNode> predicateOf(final Attr element) {
		return new DomainTreeNodeAttributePredicate(element);
	}

	@Override
	protected Predicate<DomainTreeNode> predicateOf(final Element element) {
		return new DomainTreeNodeElementPredicate(element);
	}

}