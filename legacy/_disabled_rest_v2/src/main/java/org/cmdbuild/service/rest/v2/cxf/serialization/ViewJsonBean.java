/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v2.cxf.serialization;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.view.View;
import static org.cmdbuild.service.rest.v2.constants.Serialization.UNDERSCORED_ID;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public class ViewJsonBean {

	private String description, filter, id, name, sourceClassName, sourceFunction, type;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	@JsonProperty(UNDERSCORED_ID)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSourceClassName() {
		return sourceClassName;
	}

	public void setSourceClassName(String sourceClassName) {
		this.sourceClassName = sourceClassName;
	}

	public String getSourceFunction() {
		return sourceFunction;
	}

	public void setSourceFunction(String sourceFunction) {
		this.sourceFunction = sourceFunction;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static ViewJsonBean fromView(View view) {
		checkNotNull(view);
		return new ViewJsonBean() {
			{
				setId(view.getId().toString());
				setDescription(view.getDescription());
				setFilter(view.getFilter());
				setName(view.getName());
//				setSourceClassName(view.getSourceClass()); TODO fix this (should set class name for backward compatibility
				setSourceFunction(view.getSourceFunction());
				setType(view.getType().toString());
			}
		};

	}

}
