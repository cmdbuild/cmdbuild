package org.cmdbuild.service.rest.v2.cxf.serialization;

import static com.google.common.collect.Lists.newArrayList;
import static org.cmdbuild.service.rest.v2.cxf.serialization.ToAttribute.toAttribute;
import static org.cmdbuild.service.rest.v2.model.Models.newProcessActivityWithFullDetails;

import java.util.Collection;
import org.cmdbuild.service.rest.v2.model.ProcessActivityWithFullDetails;
import org.cmdbuild.service.rest.v2.model.ProcessActivityWithFullDetails.AttributeStatus;
import org.cmdbuild.service.rest.v2.model.Widget;
import org.cmdbuild.workflow.model.WorkflowException;
import org.cmdbuild.workflow.model.TaskVariable;

import com.google.common.base.Function;
import static java.util.Collections.emptyList;
import org.cmdbuild.workflow.model.TaskDefinition;

public class ToProcessActivityDefinition implements Function<TaskDefinition, ProcessActivityWithFullDetails> {

	public static class Builder implements org.apache.commons.lang3.builder.Builder<ToProcessActivityDefinition> {

		private boolean writable;

		private Builder() {
			// use static method
		}

		@Override
		public ToProcessActivityDefinition build() {
			validate();
			return new ToProcessActivityDefinition(this);
		}

		private void validate() {
			// TODO Auto-generated method stub
		}

		public Builder withWritableStatus(final boolean writable) {
			this.writable = writable;
			return this;
		}

	}

	public static Builder newInstance() {
		return new Builder();
	}

	private final boolean writable;

	private ToProcessActivityDefinition(final Builder builder) {
		this.writable = builder.writable;
	}

	@Override
	public ProcessActivityWithFullDetails apply(final TaskDefinition input) {
		return newProcessActivityWithFullDetails() //
				.withId(input.getId()) //
				.withWritableStatus(writable) //
				.withDescription(input.getDescription()) //
				.withInstructions(input.getInstructions()) //
				.withAttributes(safeAttributesOf(input)) //
				.withWidgets(safeWidgetsOf(input)) //
				.build();
	}

	private Collection<AttributeStatus> safeAttributesOf(final TaskDefinition input) {
		try {
			final Collection<AttributeStatus> attributes = newArrayList();
			for (final TaskVariable element : input.getVariables()) {
				attributes.add(toAttribute(Long.valueOf(attributes.size())).apply(element));
			}
//			for (final org.cmdbuild.widget.Widget widget : from(input.getWidgets()) // TODO
//					.filter(org.cmdbuild.widget.Widget.class)) {
//				((WidgetVisitable) widget).accept(new ForwardingWidgetVisitor() {
//
//					private final WidgetVisitor delegate = NullWidgetVisitor.getInstance();
//
//					@Override
//					protected WidgetVisitor delegate() {
//						return delegate;
//					}
//
//					@Override
//					public void visit(final OpenNote widget) {
//						attributes.add(newAttributeStatus() //
//								.withId(Notes.getDBName()) //
//								.withWritable(true) //
//								.withMandatory(false) //
//								.withIndex(Long.valueOf(attributes.size())) //
//								.build());
//					}
//
//				});
//			}
			return attributes;
		} catch (final WorkflowException e) {
			throw new RuntimeException(e);
		}
	}

	private Iterable<Widget> safeWidgetsOf(final TaskDefinition input) {
		try {
			return emptyList();//TODO
//			return from(input.getWidgets()) //
//					.filter(org.cmdbuild.widget.Widget.class) //
//					.transform(new Function<org.cmdbuild.widget.Widget, Widget>() {
//
//						@Override
//						public Widget apply(final org.cmdbuild.widget.Widget input) {
//							/*
//							 * TODO do in a better way
//							 */
//							final ObjectMapper objectMapper = new ObjectMapper();
//							@SuppressWarnings("unchecked")
//							final Map<String, Object> objectAsMap = objectMapper.convertValue(input, Map.class);
//							return newWidget() //
//									.withId(input.getIdentifier()) //
//									.withType(input.getType()) //
//									.withActive(input.isActive()) //
//									.withRequired(new ForwardingWidgetVisitor() {
//
//										private final WidgetVisitor delegate = NullWidgetVisitor.getInstance();
//										private final org.cmdbuild.widget.Widget widget = input;
//										private boolean required;
//
//										@Override
//										protected WidgetVisitor delegate() {
//											return delegate;
//										}
//
//										public boolean isRequired() {
//											required = false;
//											((WidgetVisitable) widget).accept(this);
//											return required;
//										}
//
//										@Override
//										public void visit(final CustomForm widget) {
//											required = widget.isRequired();
//										}
//
//										@Override
//										public void visit(final Grid widget) {
//											required = widget.isRequired();
//										}
//
//										@Override
//										public void visit(final LinkCards widget) {
//											required = widget.isRequired();
//										}
//
//										@Override
//										public void visit(final ManageRelation widget) {
//											required = widget.isRequired();
//										}
//
//									}.isRequired()) //
//									.withLabel(input.getLabel()) //
//									.withData(newValues() //
//											.withValues(objectAsMap) //
//											.build()) //
//									.build();
//						}
//
//					});
		} catch (final WorkflowException e) {
			throw new RuntimeException(e);
		}
	}
}
