package org.cmdbuild.service.rest.v2.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.FluentIterable.from;
import static java.util.Arrays.asList;
import static org.cmdbuild.service.rest.v2.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseSingle;

import java.util.List;

import org.cmdbuild.service.rest.v2.ProcessInstanceActivities;
import org.cmdbuild.service.rest.v2.cxf.serialization.ToProcessActivityDefinition;
import org.cmdbuild.service.rest.v2.model.ProcessActivityWithBasicDetails;
import org.cmdbuild.service.rest.v2.model.ProcessActivityWithFullDetails;
import org.cmdbuild.service.rest.v2.model.ResponseMultiple;
import org.cmdbuild.service.rest.v2.model.ResponseSingle;
import org.cmdbuild.workflow.model.WorkflowException;

import com.google.common.base.Predicates;
import static java.util.stream.Collectors.toList;
import static org.cmdbuild.service.rest.v2.model.Models.newProcessActivityWithBasicDetails;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.workflow.model.Flow;

@Component("v2_processInstanceActivities")
public class CxfProcessInstanceActivities implements ProcessInstanceActivities {

	private final ErrorHandler errorHandler;
	private final WorkflowService workflowService;

	public CxfProcessInstanceActivities(@Qualifier("v2_errorHandler") ErrorHandler errorHandler, WorkflowService workflowService) {
		this.errorHandler = checkNotNull(errorHandler);
		this.workflowService = checkNotNull(workflowService);
	}

	@Override
	public ResponseMultiple<ProcessActivityWithBasicDetails> read(String classeName, Long flowCardId) {
//		FlowCard flowCard = workflowService.getFlowCard(classeName, flowCardId);
//
//		String planId = flowCard.getPlanId();
//		String flowId = flowCard.getFlowId();

		List<Task> taskList = workflowService.getTaskListForCurrentUserByClassIdAndCardId(classeName, flowCardId);

		return newResponseMultiple(ProcessActivityWithBasicDetails.class)
				.withElements(taskList.stream().map((task) -> {
					return newProcessActivityWithBasicDetails()
							.withId(task.getId())
							.withWritableStatus(task.isWritable())
							.withDescription(task.getDefinition() == null ? "" : task.getDefinition().getDescription())
							.build();
				}).collect(toList()))
				.withMetadata(newMetadata().withTotal(taskList.size()).build())
				.build();
	}

	@Override
	public ResponseSingle<ProcessActivityWithFullDetails> read(String processId, Long processInstanceId, String processActivityId) {
//		PlanClasse foundType = workflowService.getProcessClasse(processId);
//		if (foundType == null) {
//			errorHandler.processNotFound(processId);
//		}
		Flow foundInstance = workflowService.getFlowCard(processId, processInstanceId);
		if (foundInstance == null) {
			errorHandler.processInstanceNotFound(processInstanceId);
		}
		Task activityInstance = workflowService.getUserTask(foundInstance, processActivityId);
		if (activityInstance == null) {
			errorHandler.processActivityNotFound(processActivityId);
		}
		try {
//			TaskDefinition delegate = activityInstance.getDefinition();
//			TaskDefinition foundActivity = new ForwardingActivity() {
//
//				@Override
//				protected TaskDefinition delegate() {
//					return delegate;
//				}
//
//				@Override
//				public List<Widget> getWidgets() throws WorkflowException {
//					return activityInstance.getWidgets();
//				}
//
//			};
			ToProcessActivityDefinition TO_PROCESS_ACTIVITY = ToProcessActivityDefinition.newInstance() //
					.withWritableStatus(activityInstance.isWritable()) //
					.build();
			return newResponseSingle(ProcessActivityWithFullDetails.class) //
					.withElement(from(asList(activityInstance.getDefinition())) //
							.filter(Predicates.notNull()) //
							.transform(TO_PROCESS_ACTIVITY) //TODO get widgets from instance
							.first() //
							.get()) //
					.build();
		} catch (WorkflowException e) {
			errorHandler.propagate(e);
			return null;
		}
	}
}
