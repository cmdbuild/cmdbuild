package org.cmdbuild.service.rest.v2;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.cmdbuild.service.rest.v2.constants.Serialization.CARD_ID;
import static org.cmdbuild.service.rest.v2.constants.Serialization.CLASS_ID;
import static org.cmdbuild.service.rest.v2.constants.Serialization.FILTER;
import static org.cmdbuild.service.rest.v2.constants.Serialization.LIMIT;
import static org.cmdbuild.service.rest.v2.constants.Serialization.POSITION_OF;
import static org.cmdbuild.service.rest.v2.constants.Serialization.SORT;
import static org.cmdbuild.service.rest.v2.constants.Serialization.START;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.cmdbuild.service.rest.v2.model.Card;
import org.cmdbuild.service.rest.v2.model.ResponseMultiple;
import org.cmdbuild.service.rest.v2.model.ResponseSingle;

@Path("classes/{" + CLASS_ID + "}/cards/")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public interface Cards {

	/**
	 *
	 * @param classId
	 * @param card
	 * @return
	 */
	@POST
	@Path(EMPTY)
	public ResponseSingle<Long> create(@PathParam(CLASS_ID) String classId, Card card);

	/**
	 *
	 * return single card. Example output (without ext=true):
	 *
	 * <pre><code>
	 *{
	 * "data": {
	 * "_id": 542,
	 * "_type": "PC",
	 * "AcceptanceDate": null,
	 * "AcceptanceNotes": null,
	 * "Assignee": 130,
	 * "Brand": 136,
	 * "CPUNumber": 4,
	 * "CPUSpeed": null,
	 * "Code": "PC0004",
	 * "Description": "Sony Vajo F",
	 * "FinalCost": null,
	 * "HDSize": 2,
	 * "IPAddress": null,
	 * "IdTenant": 1519,
	 * "Model": "Vajo F",
	 * "Notes": null,
	 * "PurchaseDate": null,
	 * "RAM": 8,
	 * "Room": 272,
	 * "SerialNumber": "TY747687",
	 * "SoundCard": null,
	 * "Supplier": null,
	 * "TechnicalReference": 116,
	 * "VideoCard": null,
	 * "Workplace": null
	 * },
	 * "meta": {
	 * "total": null,
	 * "positions": {},
	 * "references": {
	 * "272": {
	 * "description": "Office Building B - Floor 2 - Room 003"
	 * },
	 * "130": {
	 * "description": "Wilson Barbara"
	 * },
	 * "116": {
	 * "description": "Smith James"
	 * }
	 * }
	 * }
	 * }
	 *
	 * </code></pre>
	 *
	 * when ext param is set to true, will try to attach description to each
	 * attribute of type reference. Description attribute key will be
	 * '_'+attributeName+'_description'
	 *
	 * Description fields may be null or absent altogether, client code should
	 * not expect them to be present and should check the values and handle
	 * correctly null, undefined and blank values
	 *
	 * example response with ext=true :
	 *
	 * <pre><code>
	 * {
	 * "data": {
	 * "_id": 542,
	 * "_type": "PC",
	 * "AcceptanceDate": null,
	 * "AcceptanceNotes": null,
	 * "Assignee": 130,
	 * "_Assignee_description": "Wilson Barbara",
	 * "Brand": 136,
	 * "CPUNumber": 4,
	 * "CPUSpeed": null,
	 * "Code": "PC0004",
	 * "Description": "Sony Vajo F",
	 * "FinalCost": null,
	 * "HDSize": 2,
	 * "IPAddress": null,
	 * "IdTenant": 1519,
	 * "Model": "Vajo F",
	 * "Notes": null,
	 * "PurchaseDate": null,
	 * "RAM": 8,
	 * "Room": 272,
	 * "_Room_description": "Office Building B - Floor 2 - Room 003",
	 * "SerialNumber": "TY747687",
	 * "SoundCard": null,
	 * "Supplier": null,
	 * "TechnicalReference": 116,
	 * "_TechnicalReference_description": "Smith James",
	 * "VideoCard": null,
	 * "Workplace": null
	 * },
	 * "meta": {
	 * "total": null,
	 * "positions": {},
	 * "references": {
	 * "272": {
	 * "description": "Office Building B - Floor 2 - Room 003"
	 * },
	 * "130": {
	 * "description": "Wilson Barbara"
	 * },
	 * "116": {
	 * "description": "Smith James"
	 * }
	 * }
	 * }
	 * }
	 *
	 * </code></pre>
	 *
	 * @param classId
	 * @param id
	 * @return
	 */
	@GET
	@Path("{" + CARD_ID + "}/")
	public ResponseSingle<Card> read(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long id);

	/**
	 *
	 * see description of
	 * {@link Cards#read(java.lang.String, java.lang.Long, java.lang.Boolean)}
	 *
	 * @param classId
	 * @param filter
	 * @param sort
	 * @param limit
	 * @param offset
	 * @param cardIds
	 * @return
	 */
	@GET
	@Path(EMPTY)
	public ResponseMultiple<Card> read(
			@PathParam(CLASS_ID) String classId,
			@QueryParam(FILTER) String filter,
			@QueryParam(SORT) String sort,
			@QueryParam(LIMIT) Integer limit,
			@QueryParam(START) Integer offset,
			@QueryParam(POSITION_OF) Set<Long> cardIds);

	/**
	 *
	 * @param classId
	 * @param id
	 * @param card
	 */
	@PUT
	@Path("{" + CARD_ID + "}/")
	public void update(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long id, Card card);

	/**
	 *
	 * @param classId
	 * @param id
	 */
	@DELETE
	@Path("{" + CARD_ID + "}/")
	public void delete(@PathParam(CLASS_ID) String classId, @PathParam(CARD_ID) Long id);

}
