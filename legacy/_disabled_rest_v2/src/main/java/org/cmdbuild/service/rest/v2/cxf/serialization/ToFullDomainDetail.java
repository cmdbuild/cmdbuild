package org.cmdbuild.service.rest.v2.cxf.serialization;

import static org.cmdbuild.service.rest.v2.model.Models.newDomainWithFullDetails;

import org.apache.commons.lang3.Validate;
import org.cmdbuild.service.rest.v2.model.DomainWithFullDetails;

import com.google.common.base.Function;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.dao.entrytype.Domain;

public class ToFullDomainDetail implements Function<Domain, DomainWithFullDetails> {

	public static class Builder implements org.apache.commons.lang3.builder.Builder<ToFullDomainDetail> {

		private DataAccessService dataAccessLogic;

		private Builder() {
			// use static method
		}

		@Override
		public ToFullDomainDetail build() {
			validate();
			return new ToFullDomainDetail(this);
		}

		private void validate() {
			Validate.notNull(dataAccessLogic, "missing '%s'", DataAccessService.class);
		}

		public Builder withDataAccessLogic(final DataAccessService dataAccessLogic) {
			this.dataAccessLogic = dataAccessLogic;
			return this;
		}

	}

	public static Builder newInstance() {
		return new Builder();
	}

	private final DataAccessService dataAccessLogic;

	private ToFullDomainDetail(final Builder builder) {
		this.dataAccessLogic = builder.dataAccessLogic;
	}

	@Override
	public DomainWithFullDetails apply(final Domain input) {
		return newDomainWithFullDetails() //
				.withId(input.getName()) //
				.withName(input.getName()) //
				.withDescription(input.getDescription()) //
				.withSource(input.getSourceClass().getName()) //
				.withSourceProcess(dataAccessLogic.isProcess(input.getSourceClass())) //
				.withDestination(input.getTargetClass().getName()) //
				.withDestinationProcess(dataAccessLogic.isProcess(input.getTargetClass())) //
				.withCardinality(input.getCardinality()) //
				.withDescriptionDirect(input.getDirectDescription()) //
				.withDescriptionInverse(input.getInverseDescription()) //
				.withDescriptionMasterDetail(input.getMasterDetailDescription()) //
				.withActive(input.isActive()) //
				.build();
	}

}
