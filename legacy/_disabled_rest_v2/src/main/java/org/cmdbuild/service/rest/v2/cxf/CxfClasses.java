package org.cmdbuild.service.rest.v2.cxf;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Iterables.size;
import static org.cmdbuild.service.rest.v2.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseSingle;

import org.cmdbuild.service.rest.v2.Classes;
import org.cmdbuild.service.rest.v2.model.ClassWithBasicDetails;
import org.cmdbuild.service.rest.v2.model.ClassWithFullDetails;
import org.cmdbuild.service.rest.v2.model.ResponseMultiple;
import org.cmdbuild.service.rest.v2.model.ResponseSingle;

import com.google.common.collect.Ordering;
import static com.google.common.collect.Streams.stream;
import static java.lang.Math.abs;
import java.util.List;
import static java.util.stream.Collectors.toList;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Component;
import org.cmdbuild.data2.api.DataAccessService;
import static org.cmdbuild.service.rest.v2.cxf.serialization.CMClassToClassWithBasicDetails.toBasicDetail;
import org.cmdbuild.dao.entrytype.Classe;
import static org.cmdbuild.dao.entrytype.Predicates.classOrder;
import static org.cmdbuild.service.rest.v2.constants.Serialization.ASCENDING;
import static org.cmdbuild.service.rest.v2.constants.Serialization.DESCENDING;
import static org.cmdbuild.service.rest.v2.model.Models.newAttributeOrder;
import static org.cmdbuild.service.rest.v2.model.Models.newClassWithFullDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.view.DataView;

@Component("v2_classes")
public class CxfClasses implements Classes {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DataAccessService dataAccessLogic;
	private final DataView dataView;

	public CxfClasses(DataAccessService dataAccessLogic, DataView dataView) {
		this.dataAccessLogic = checkNotNull(dataAccessLogic);
		this.dataView = checkNotNull(dataView);
	}

	@Override
	public ResponseMultiple<ClassWithBasicDetails> readAll(boolean activeOnly, Integer limit, Integer offset) {
		//TODO paginate with query
		Iterable<? extends Classe> allClasses = dataAccessLogic.findClasses(activeOnly);
		Iterable<? extends Classe> ordered = Ordering.natural().onResultOf(Classe::getName).sortedCopy(allClasses);
		Iterable<ClassWithBasicDetails> elements = from(ordered)
				.skip((offset == null) ? 0 : offset)
				.limit((limit == null) ? Integer.MAX_VALUE : limit)
				.transform(toBasicDetail());

		return newResponseMultiple(ClassWithBasicDetails.class)
				.withElements(elements)
				.withMetadata(newMetadata().withTotal(Long.valueOf(size(ordered))).build())
				.build();
	}

	@Override
	public ResponseSingle<ClassWithFullDetails> read(String classId) {
		Classe classe = dataView.getClasse(classId);
		return buildResponse(classe);
	}

	private ResponseSingle<ClassWithFullDetails> buildResponse(Classe classe) {
		ClassWithFullDetails element = buildFullDetailResponse(classe);
		return newResponseSingle(ClassWithFullDetails.class).withElement(element).build();
	}

	public ClassWithFullDetails buildFullDetailResponse(Classe input) {
		return newClassWithFullDetails()
				.withId(input.getName())
				.withName(input.getName())
				.withDescription(input.getDescription())
				.thatIsPrototype(input.isSuperclass())
				.withDescriptionAttributeName(input.getDescriptionAttributeName())
				.withDefaultOrder(buildAttributeOrder(input))
				.withParent(input.getParentNameOrNull())
				.build();
	}

	private List<ClassWithFullDetails.AttributeOrder> buildAttributeOrder(Classe classe) {
		return stream(classe.getActiveAttributes())
				.filter(classOrder(not(equalTo(0))))
				.sorted((Attribute o1, Attribute o2) -> ObjectUtils.compare(abs(o1.getClassOrder()), abs(o2.getClassOrder())))
				.map((Attribute attr) -> {
					return newAttributeOrder()
							.withAttribute(attr.getName())
							.withDirection(attr.getClassOrder() > 0 ? ASCENDING : DESCENDING)
							.build();
				})
				.collect(toList());

	}
}
