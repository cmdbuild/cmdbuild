package org.cmdbuild.service.rest.v2.cxf.filter;

import org.cmdbuild.logic.data.access.filter.model.Attr;
import org.cmdbuild.logic.data.access.filter.model.Element;

import com.google.common.base.Predicate;
import org.cmdbuild.dao.function.StoredFunction;

public class FunctionElementPredicate extends ElementPredicate<StoredFunction> {

	public FunctionElementPredicate(final Element element) {
		super(element);
	}

	@Override
	protected Predicate<StoredFunction> predicateOf(final Attr element) {
		return new FunctionAttributePredicate(element);
	}

	@Override
	protected Predicate<StoredFunction> predicateOf(final Element element) {
		return new FunctionElementPredicate(element);
	}

}