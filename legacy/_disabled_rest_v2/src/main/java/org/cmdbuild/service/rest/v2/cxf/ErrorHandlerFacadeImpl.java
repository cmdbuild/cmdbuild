package org.cmdbuild.service.rest.v2.cxf;

import org.cmdbuild.dao.view.DataView;

public class ErrorHandlerFacadeImpl extends ForwardingErrorHandler implements ErrorHandlerFacade {

	private final ErrorHandler delegate;
	private final DataView dataView;

	public ErrorHandlerFacadeImpl(final ErrorHandler delegate, final DataView dataView) {
		this.delegate = delegate;
		this.dataView = dataView;
	}

	@Override
	protected ErrorHandler delegate() {
		return delegate;
	}

	@Override
	public void checkClass(final String value) {
		if (dataView.findClasse(value) == null) {
			classNotFound(value);
		}
	}

}
