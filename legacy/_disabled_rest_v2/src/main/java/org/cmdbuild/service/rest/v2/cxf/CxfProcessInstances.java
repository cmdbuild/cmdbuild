package org.cmdbuild.service.rest.v2.cxf;

import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Maps.transformEntries;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.Maps.uniqueIndex;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.cmdbuild.dao.entrytype.Functions.attributeName;
import static org.cmdbuild.dao.entrytype.Predicates.attributeTypeInstanceOf;
import static org.cmdbuild.service.rest.v2.constants.Serialization.UNDERSCORED_STATUS;
import static org.cmdbuild.service.rest.v2.cxf.util.Json.safeJsonArray;
import static org.cmdbuild.service.rest.v2.cxf.util.Json.safeJsonObject;
import static org.cmdbuild.service.rest.v2.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v2.model.Models.newReference;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseSingle;

import java.util.Map;
import java.util.Set;

import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.dao.beans.IdAndDescriptionImpl;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.logic.mapping.json.FilterElementGetters;
import org.cmdbuild.logic.mapping.json.JsonFilterHelper;
import org.cmdbuild.service.rest.v2.ProcessInstances;
import org.cmdbuild.service.rest.v2.cxf.serialization.ToProcessInstance;
import org.cmdbuild.service.rest.v2.model.DetailResponseMetadata.Reference;
import org.cmdbuild.service.rest.v2.model.ProcessInstance;
import org.cmdbuild.service.rest.v2.model.ProcessInstanceAdvanceable;
import org.cmdbuild.service.rest.v2.model.ResponseMultiple;
import org.cmdbuild.service.rest.v2.model.ResponseSingle;
import org.cmdbuild.service.rest.v2.model.Widget;
import org.cmdbuild.workflow.core.LookupHelper;
import org.cmdbuild.workflow.inner.UserFlowWithPosition;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Function;
import org.cmdbuild.workflow.WorkflowService;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.model.Task;
import static org.cmdbuild.workflow.utils.WorkflowClassAttributes.ATTR_FLOW_STATUS;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;
import static org.cmdbuild.service.rest.v2.cxf.serialization.Converter.fromClient;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.Process;

@Component("v2_processInstances")
public class CxfProcessInstances implements ProcessInstances {

	private static final Iterable<Long> NO_INSTANCE_IDS = emptyList();
	private static final Map<Long, Long> NO_POSITIONS = emptyMap();

	private static final Function<Widget, String> WIDGET_ID = (Widget input) -> input.getId();

//	private static final Function<Widget, Object> WIDGET_OUTPUT = (Widget input) -> {
//		return map().with(WIDGET_SUBMIT_KEY, input.getOutput());
//	};
	@Autowired
	@Qualifier("v2_errorHandler")
	private ErrorHandler errorHandler;
	@Autowired
	private WorkflowService workflowService;
	@Autowired
	private LookupHelper lookupHelper;
//	@Autowired
//	private FilterLoader filterLoader;

	@Override
	public ResponseSingle<Long> create(String processId, ProcessInstanceAdvanceable processInstance) {
		Process processClass = workflowService.getPlanClasse(processId);
//		if (processClass == null) {
//			errorHandler.processNotFound(processId);
//		}
		try {
			Flow instance = workflowService.startProcess(
					processId,
					adaptInputValues(processClass, processInstance),
					processInstance.isAdvance()).getFlowCard();
			return newResponseSingle(Long.class).withElement(instance.getId()).build();
		} catch (Throwable e) {
			errorHandler.propagate(e);
		}
		return null;
	}

	@Override
	public ResponseSingle<ProcessInstance> read(String processId, Long instanceId) {
		Process plan = workflowService.getPlanClasse(processId);
//		if (plan == null) {
//			errorHandler.processNotFound(processId);
//		}
		QueryOptionsImpl queryOptions = QueryOptionsImpl.builder() //
				.limit(1) //
				.offset(0) //
				.filter(filterFor(instanceId)) //
				.build();
		PagedElements<Flow> elements = workflowService.getFlowCardsByClasseIdAndQueryOptions(plan.getName(), queryOptions);
		if (elements.totalSize() == 0) {
			errorHandler.processInstanceNotFound(instanceId);
		}
		Flow walk = from(elements).first().get();
		Map<Long, Reference> references = newHashMap();
		for (Attribute attr : from(walk.getType().getAllAttributes()).filter(attributeTypeInstanceOf(ReferenceAttributeType.class))) {
			Object value = walk.get(attr.getName());
			if (value instanceof IdAndDescriptionImpl) {
				IdAndDescriptionImpl idAndDescription = IdAndDescriptionImpl.class.cast(value);
				if (idAndDescription.getId() != null) {
					references.put(idAndDescription.getId(), newReference() //
							.withDescription(idAndDescription.getDescription()) //
							.build());
				}
			}
		}
		Function<Flow, ProcessInstance> toProcessInstance = ToProcessInstance.newInstance()
				.withType(plan)
				.withLookupHelper(lookupHelper)
				.build();
		return newResponseSingle(ProcessInstance.class)
				.withElement(toProcessInstance.apply(walk))
				.withMetadata(newMetadata()
						.withReferences(references)
						.build())
				.build();
	}

	private JSONObject filterFor(Long id) {
		try {
			JSONObject emptyFilter = new JSONObject();
			return new JsonFilterHelper(emptyFilter).merge(FilterElementGetters.id(id));
		} catch (JSONException e) {
			errorHandler.propagate(e);
			return new JSONObject();
		}
	}

	@Override
	public ResponseMultiple<ProcessInstance> read(String processId, String filter, String sort, Integer limit, Integer offset, Set<Long> instanceIds) {
		Process found = workflowService.getPlanClasse(processId);
//		if (found == null) {
//			errorHandler.processNotFound(processId);
//		}
		// TODO do it better
		// <<<<<
		String regex = "\"attribute\"[\\s]*:[\\s]*\"" + UNDERSCORED_STATUS + "\"";
		String replacement = "\"attribute\":\"" + ATTR_FLOW_STATUS + "\"";
//		String _filter = defaultString(filterLoader.load(filter)).replaceAll(regex, replacement);
		String _filter = "";//TODO
		// <<<<<
		Iterable<String> attributes = activeAttributes(found);
		Iterable<String> _attributes = concat(attributes, asList(ATTR_FLOW_STATUS));
		QueryOptionsImpl queryOptions = QueryOptionsImpl.builder() //
				.onlyAttributes(_attributes) //
				.filter(safeJsonObject(_filter)) //
				.orderBy(safeJsonArray(sort)) //
				.limit(limit) //
				.offset(offset) //
				.build();
		PagedElements<? extends Flow> elements;
		Map<Long, Long> positions;
		if (isEmpty(defaultIfNull(instanceIds, NO_INSTANCE_IDS))) {
			elements = workflowService.getFlowCardsByClasseIdAndQueryOptions(found.getName(), queryOptions);
			positions = NO_POSITIONS;
		} else {
			PagedElements<UserFlowWithPosition> response0 = workflowService.queryWithPosition(
					found.getName(), queryOptions, instanceIds);
			elements = response0;
			positions = transformValues(//
					uniqueIndex(response0, (UserFlowWithPosition input) -> input.getId()), //
					(UserFlowWithPosition input) -> input.getPosition());
		}
		Map<Long, Reference> references = newHashMap();
		for (Flow element : elements) {
			for (Attribute _element : from(element.getType().getAllAttributes()) //
					.filter(attributeTypeInstanceOf(ReferenceAttributeType.class))) {
				Object value = element.get(_element.getName());
				if (value instanceof IdAndDescriptionImpl) {
					IdAndDescriptionImpl _value = IdAndDescriptionImpl.class.cast(value);
					if (_value.getId() != null) {
						references.put(_value.getId(), newReference() //
								.withDescription(_value.getDescription()) //
								.build());
					}
				}
			}
		}
		Function<Flow, ProcessInstance> toProcessInstance = ToProcessInstance.newInstance() //
				.withType(found) //
				.withLookupHelper(lookupHelper) //
				.withAttributes(attributes) //
				.build();
		return newResponseMultiple(ProcessInstance.class) //
				.withElements(from(elements) //
						.transform(toProcessInstance)) //
				.withMetadata(newMetadata() //
						.withTotal(Long.valueOf(elements.totalSize())) //
						.withPositions(positions) //
						.withReferences(references) //
						.build()) //
				.build();
	}

	private Iterable<String> activeAttributes(Process target) {
		return from(target.getActiveAttributes()) //
				.transform(attributeName());
	}

	@Override
	public void update(String planClassId, Long flowCardId, ProcessInstanceAdvanceable processInstance) {
		Process plan = workflowService.getPlanClasse(planClassId);
		try {
			Flow flowCard = workflowService.getFlowCard(planClassId, flowCardId);
			Task task = workflowService.getTask(flowCard, checkNotBlank(processInstance.getActivity(), "must set 'activity' param"));
//			if (task == null) {
//				errorHandler.processActivityNotFound(processInstance.getActivity());
//			}
			workflowService.updateProcess( //
					planClassId, //
					flowCardId, //
					task.getId(), //
					adaptInputValues(plan, processInstance), //
					//					adaptWidgets(processInstance.getWidgets()), //
					processInstance.isAdvance()).getFlowCard();
		} catch (Throwable e) {
			errorHandler.propagate(e);
		}
	}

	@Override
	public void delete(String processId, Long instanceId) {
		Process found = workflowService.getPlanClasse(processId);
//		if (found == null) {
//			errorHandler.processNotFound(processId);
//		}
		try {
			workflowService.abortProcess(processId, instanceId);
		} catch (Throwable e) {
			errorHandler.propagate(e);
		}
	}

	private Map<String, Object> adaptInputValues(Process userProcessClass, ProcessInstanceAdvanceable processInstanceAdvanceable) {
		return transformEntries(processInstanceAdvanceable.getValues(), (String key, Object value) -> {
			Attribute attribute = userProcessClass.getAttributeOrNull(key);
			Object _value;
			if (attribute == null) {
				_value = value;
			} else {
				CardAttributeType<?> attributeType = attribute.getType();
				_value = fromClient().convert(attributeType, value);
			}
			return _value;
		});
	}

//	private Map<String, Object> adaptWidgets(Collection<Widget> elements) {
//		return transformValues(uniqueIndex(elements, WIDGET_ID), WIDGET_OUTPUT);
//	}
}
