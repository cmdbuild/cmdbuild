package org.cmdbuild.service.rest.v2.cxf;

import static com.google.common.base.Predicates.alwaysTrue;
import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Iterables.size;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.cmdbuild.service.rest.v2.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseSingle;

import org.cmdbuild.logic.data.access.filter.json.JsonParser;
import org.cmdbuild.logic.data.access.filter.model.Element;
import org.cmdbuild.logic.data.access.filter.model.Filter;
import org.cmdbuild.logic.data.access.filter.model.Parser;
import org.cmdbuild.service.rest.v2.Domains;
import org.cmdbuild.service.rest.v2.cxf.filter.DomainElementPredicate;
import org.cmdbuild.service.rest.v2.cxf.serialization.ToSimpleDomainDetail;
import org.cmdbuild.service.rest.v2.model.DomainWithBasicDetails;
import org.cmdbuild.service.rest.v2.model.DomainWithFullDetails;
import org.cmdbuild.service.rest.v2.model.ResponseMultiple;
import org.cmdbuild.service.rest.v2.model.ResponseSingle;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.service.rest.v2.cxf.serialization.ToFullDomainDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.entrytype.Domain;

@Component("v2_domains")
public class CxfDomains implements Domains {

	private static final ToSimpleDomainDetail TO_SIMPLE_DOMAIN_DETAIL = ToSimpleDomainDetail.newInstance().build();

	@Autowired
	@Qualifier("v2_errorHandler")
	private ErrorHandler errorHandler;
	@Autowired
	private DataAccessService dataAccessLogic;

	@Override
	public ResponseMultiple<DomainWithBasicDetails> readAll(final String filter, final Integer limit,
			final Integer offset) {
		final Predicate<Domain> predicate;
		if (isNotBlank(filter)) {
			final Parser parser = new JsonParser(filter);
			final Filter filterModel = parser.parse();
			final Optional<Element> element = filterModel.attribute();
			if (element.isPresent()) {
				predicate = new DomainElementPredicate(element.get());
			} else {
				predicate = alwaysTrue();
			}
		} else {
			predicate = alwaysTrue();
		}
		final Iterable<? extends Domain> allElements = dataAccessLogic.getAllDomains();
		final Iterable<DomainWithBasicDetails> elements = from(allElements) //
				.filter(predicate) //
				.skip((offset == null) ? 0 : offset) //
				.limit((limit == null) ? Integer.MAX_VALUE : limit) //
				.transform(TO_SIMPLE_DOMAIN_DETAIL);
		return newResponseMultiple(DomainWithBasicDetails.class) //
				.withElements(elements) //
				.withMetadata(newMetadata() //
						.withTotal(Long.valueOf(size(allElements))) //
						.build()) //
				.build();
	}

	@Override
	public ResponseSingle<DomainWithFullDetails> read(final String domainId) {
		final Domain found = dataAccessLogic.findDomain(domainId);
		if (found == null) {
			errorHandler.domainNotFound(domainId);
		}
		return newResponseSingle(DomainWithFullDetails.class) //
				.withElement(ToFullDomainDetail.newInstance().withDataAccessLogic(dataAccessLogic).build().apply(found)) //
				.build();
	}
}
