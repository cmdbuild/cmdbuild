/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v2.cxf.serialization;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newLinkedHashMap;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.emptyList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import org.apache.commons.lang3.builder.Builder;
import org.cmdbuild.dashboard.ChartDefinition;
import org.cmdbuild.dashboard.DashboardDefinition;
import org.cmdbuild.dashboard.DefaultDashboardDefinition.DashboardColumn;
import static org.cmdbuild.service.rest.v2.constants.Serialization.UNDERSCORED_ID;
import org.cmdbuild.service.rest.v2.cxf.serialization.DashboardJsonBean.ChartDefinitionJsonBean.ChartInputJsonBean;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public class DashboardJsonBean {

	private String name, id;
	private String description;
	private Map<String, ChartDefinitionJsonBean> charts = new LinkedHashMap<>();
	private List<DashboardColumnJsonBean> columns = new ArrayList<>();
	private List<String> groups = new ArrayList<>();

	public DashboardJsonBean() {
	}

	@JsonProperty(UNDERSCORED_ID)
	public String getId() {
		return id;
	}

	public void setId(String _id) {
		this.id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, ChartDefinitionJsonBean> getCharts() {
		return charts;
	}

	public void setCharts(Map<String, ChartDefinitionJsonBean> charts) {
		this.charts = charts;
	}

	public List<DashboardColumnJsonBean> getColumns() {
		return columns;
	}

	public void setColumns(List<DashboardColumnJsonBean> columns) {
		this.columns = columns;
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}


	/*
	 * A representation of a column of the dashboard to manage the references to
	 * the charts
	 */
	public static class DashboardColumnJsonBean {

		private float width = 0;
		private List<String> charts = new ArrayList<>();

		public DashboardColumnJsonBean() {
		}

		public DashboardColumnJsonBean(float width, List<String> charts) {
			this.width = width;
			this.charts = checkNotNull(charts);
		}

		public float getWidth() {
			return width;
		}

		public void setWidth(float width) {
			this.width = width;
		}

		public List<String> getCharts() {
			return charts;
		}

		public void setCharts(List<String> charts) {
			this.charts = charts;
		}

		public void addChart(String chartId) {
			this.charts.add(chartId);
		}

		public void removeChart(String chartId) {
			this.charts.remove(chartId);
		}
	}

	/*
 * A representation of the definition of a chart
	 */
	static public class ChartDefinitionJsonBean {

		private String name, description, dataSourceName, type, singleSeriesField, labelField, categoryAxisField,
				categoryAxisLabel, valueAxisLabel, fgcolor, bgcolor, chartOrientation;
		private boolean active, autoLoad, legend;
		private int height, maximum, minimum, steps;
		private List<ChartInputJsonBean> dataSourceParameters = new ArrayList<>();
		private List<String> valueAxisFields = new ArrayList<>();

		public ChartDefinitionJsonBean() {
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getDataSourceName() {
			return dataSourceName;
		}

		public void setDataSourceName(String dataSourceName) {
			this.dataSourceName = dataSourceName;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getSingleSeriesField() {
			return singleSeriesField;
		}

		public void setSingleSeriesField(String singleSeriesField) {
			this.singleSeriesField = singleSeriesField;
		}

		public String getLabelField() {
			return labelField;
		}

		public void setLabelField(String labelField) {
			this.labelField = labelField;
		}

		public String getCategoryAxisField() {
			return categoryAxisField;
		}

		public void setCategoryAxisField(String categoryAxisField) {
			this.categoryAxisField = categoryAxisField;
		}

		public String getCategoryAxisLabel() {
			return categoryAxisLabel;
		}

		public void setCategoryAxisLabel(String categoryAxisLabel) {
			this.categoryAxisLabel = categoryAxisLabel;
		}

		public String getValueAxisLabel() {
			return valueAxisLabel;
		}

		public void setValueAxisLabel(String valueAxisLabel) {
			this.valueAxisLabel = valueAxisLabel;
		}

		public String getFgcolor() {
			return fgcolor;
		}

		public void setFgcolor(String fgcolor) {
			this.fgcolor = fgcolor;
		}

		public String getBgcolor() {
			return bgcolor;
		}

		public void setBgcolor(String bgcolor) {
			this.bgcolor = bgcolor;
		}

		public String getChartOrientation() {
			return chartOrientation;
		}

		public void setChartOrientation(String chartOrientation) {
			this.chartOrientation = chartOrientation;
		}

		public boolean getActive() {
			return active;
		}

		public void setActive(boolean active) {
			this.active = active;
		}

		public boolean getAutoLoad() {
			return autoLoad;
		}

		public void setAutoLoad(boolean autoLoad) {
			this.autoLoad = autoLoad;
		}

		public boolean getLegend() {
			return legend;
		}

		public void setLegend(boolean legend) {
			this.legend = legend;
		}

		public int getHeight() {
			return height;
		}

		public void setHeight(int height) {
			this.height = height;
		}

		public int getMaximum() {
			return maximum;
		}

		public void setMaximum(int maximum) {
			this.maximum = maximum;
		}

		public int getMinimum() {
			return minimum;
		}

		public void setMinimum(int minimum) {
			this.minimum = minimum;
		}

		public int getSteps() {
			return steps;
		}

		public void setSteps(int steps) {
			this.steps = steps;
		}

		public List<ChartInputJsonBean> getDataSourceParameters() {
			return dataSourceParameters;
		}

		public void setDataSourceParameters(List<ChartInputJsonBean> dataSourceParamenters) {
			this.dataSourceParameters = dataSourceParamenters;
		}

		public List<String> getValueAxisFields() {
			return valueAxisFields;
		}

		public void setValueAxisFields(List<String> valueAxisFields) {
			this.valueAxisFields = valueAxisFields;
		}

		public static class FilterJsonBean {

			private String expression;
			private Map<String, String> context;

			public FilterJsonBean() {
			}

			//TODO probably not all params can be null, fix this
			public FilterJsonBean(@Nullable String expression, @Nullable Map<String, String> context) {
				this.expression = expression;
				this.context = context;
			}

			public String getExpression() {
				return expression;
			}

			public void setExpression(String expression) {
				this.expression = expression;
			}

			public Map<String, String> getContext() {
				return context;
			}

			public void setContext(Map<String, String> context) {
				this.context = context;
			}

		}

		public static class ChartInputJsonBean {

			private String name;
			private String type;
			private String fieldType;
			private String defaultValue;
			private String lookupType;
			private String className;
			private String classToUseForReferenceWidget;
			private FilterJsonBean filter;

			private boolean required;

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public String getType() {
				return type;
			}

			public void setType(String type) {
				this.type = type;
			}

			public String getFieldType() {
				return fieldType;
			}

			public void setFieldType(String fieldType) {
				this.fieldType = fieldType;
			}

			public String getDefaultValue() {
				return defaultValue;
			}

			public void setDefaultValue(String defaultValue) {
				this.defaultValue = defaultValue;
			}

			public String getLookupType() {
				return lookupType;
			}

			public void setLookupType(String lookupType) {
				this.lookupType = lookupType;
			}

			public String getClassName() {
				return className;
			}

			public void setClassName(String className) {
				this.className = className;
			}

			public String getClassToUseForReferenceWidget() {
				return classToUseForReferenceWidget;
			}

			public void setClassToUseForReferenceWidget(String classToUseForReferenceWidget) {
				this.classToUseForReferenceWidget = classToUseForReferenceWidget;
			}

			public boolean getRequired() {
				return required;
			}

			public void setRequired(boolean required) {
				this.required = required;
			}

			public FilterJsonBean getFilter() {
				return filter;
			}

			public void setFilter(FilterJsonBean filter) {
				this.filter = filter;
			}

		}
	}

	public static DashboardJsonBeanBuilder builder() {
		return new DashboardJsonBeanBuilder();
	}

	public static class DashboardJsonBeanBuilder implements Builder<DashboardJsonBean> {

		private final DashboardJsonBean bean = new DashboardJsonBean();

		public DashboardJsonBeanBuilder withDashboardId(Integer dashboardId) {
			bean.setId(checkNotNull(dashboardId).toString());
			return this;
		}

		public DashboardJsonBeanBuilder withDashboardDefinition(DashboardDefinition dashboardDefinition) {
			checkNotNull(dashboardDefinition);
			bean.setName(dashboardDefinition.getName());
			bean.setDescription(dashboardDefinition.getDescription());
			bean.setGroups(newArrayList(firstNonNull(dashboardDefinition.getGroups(), emptyList())));
			bean.setColumns(firstNonNull(dashboardDefinition.getColumns(), Collections.<DashboardColumn>emptyList()).stream()
					.map((DashboardColumn column) -> new DashboardColumnJsonBean(column.getWidth(), column.getCharts())).collect(toList()));
			bean.setCharts(firstNonNull(dashboardDefinition.getCharts(), Collections.<String, ChartDefinition>emptyMap()).entrySet().stream()
					.collect(toMap(Entry::getKey, (entry) -> new ChartDefinitionJsonBean() {
				{

					ChartDefinition chart = entry.getValue();
					setName(chart.getName());
					setDescription(chart.getDescription());
					setDataSourceName(chart.getDataSourceName());
					setType(chart.getType());
					setSingleSeriesField(chart.getSingleSeriesField());
					setLabelField(chart.getLabelField());
					setCategoryAxisField(chart.getCategoryAxisField());
					setCategoryAxisLabel(chart.getCategoryAxisLabel());
					setValueAxisLabel(chart.getValueAxisLabel());
					setFgcolor(chart.getFgcolor());
					setBgcolor(chart.getBgcolor());
					setChartOrientation(chart.getChartOrientation());
					setActive(chart.isActive());
					setAutoLoad(chart.isAutoLoad());
					setLegend(chart.isLegend());
					setHeight(chart.getHeight());
					setMaximum(chart.getMaximum());
					setMinimum(chart.getMinimum());
					setSteps(chart.getSteps());
					setDataSourceParameters(firstNonNull(chart.getDataSourceParameters(), Collections.<ChartDefinition.ChartInput>emptyList()).stream()
							.map((ChartDefinition.ChartInput param) -> new ChartInputJsonBean() {
						{
							setName(param.getName());
							setType(param.getType());
							setFieldType(param.getFieldType());
							setDefaultValue(param.getDefaultValue());
							setLookupType(param.getLookupType());
							setClassName(param.getClassName());
							setClassToUseForReferenceWidget(param.getClassToUseForReferenceWidget());
							setFilter(param.getFilter() == null ? null : new ChartDefinitionJsonBean.FilterJsonBean(param.getFilter().getExpression(), param.getFilter().getContext() == null ? null : newLinkedHashMap(param.getFilter().getContext())));
							setRequired(param.isRequired());
						}
					}
							).collect(toList()));
					setValueAxisFields(newArrayList(firstNonNull(chart.getValueAxisFields(), emptyList())));
				}
			}
					)));

			return this;
		}

		@Override
		public DashboardJsonBean build() {
			checkArgument(!isNull(bean.getId()));
			//TODO other validation
			return bean;
		}

	}

}
