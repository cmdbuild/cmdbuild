package org.cmdbuild.service.rest.v2.model.adapter;

import static com.google.common.base.Predicates.containsPattern;
import static com.google.common.base.Predicates.not;
import com.google.common.collect.ComparisonChain;
import static org.cmdbuild.service.rest.v2.constants.Serialization.UNDERSCORED_ID;
import static org.cmdbuild.service.rest.v2.constants.Serialization.UNDERSCORED_TYPE;
import static org.cmdbuild.service.rest.v2.model.Models.newCard;
import static org.cmdbuild.service.rest.v2.model.Models.newValues;

import org.cmdbuild.service.rest.v2.model.Card;
import org.cmdbuild.service.rest.v2.model.Values;

import static com.google.common.collect.Maps.filterKeys;
import com.google.common.collect.Ordering;
import static org.cmdbuild.service.rest.v2.model.Models.ATTRIBUTE_DESCRIPTION_SUFFIX;
import static org.cmdbuild.service.rest.v2.model.Models.ATTRIBUTE_SPECIAL_PREFIX;

public class CardAdapter extends ModelToValuesAdapter<Card> {

	private final static String DESCRIPTION_REGEX_1 = "^" + ATTRIBUTE_SPECIAL_PREFIX + ".*" + ATTRIBUTE_DESCRIPTION_SUFFIX + "$",
			DESCRIPTION_REGEX_2 = "^" + ATTRIBUTE_SPECIAL_PREFIX + "(.*" + ATTRIBUTE_DESCRIPTION_SUFFIX + ")$";

	@Override
	protected Values modelToValues(final Card input) {
		/*
		 * predefined attributes must always be added last so they are not
		 * overwritten
		 */
		return newValues()
				.withValues(input.getValues())
				.withValue(UNDERSCORED_TYPE, input.getType())
				.withValue(UNDERSCORED_ID, input.getId())
				.orderValuesByKey(Ordering.<String>from((a, b) -> ComparisonChain.start()
				.compareTrueFirst(a.startsWith(ATTRIBUTE_SPECIAL_PREFIX), b.startsWith(ATTRIBUTE_SPECIAL_PREFIX)).compare(a, b).result()).onResultOf((k) -> k.replaceFirst(DESCRIPTION_REGEX_2, "$1")))
				.build();
	}

	@Override
	protected Card valuesToModel(final Values input) {
		return newCard()
				.withType(getAndRemove(input, UNDERSCORED_TYPE, String.class))
				.withId(getAndRemove(input, UNDERSCORED_ID, Long.class))
				.withValues(filterKeys(input, not(containsPattern(DESCRIPTION_REGEX_1))))
				.build();
	}

}
