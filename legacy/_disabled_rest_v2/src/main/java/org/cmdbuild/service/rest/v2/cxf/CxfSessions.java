package org.cmdbuild.service.rest.v2.cxf;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.service.rest.v2.constants.Serialization.GROUP;
import static org.cmdbuild.service.rest.v2.model.Models.newResponseSingle;
import static org.cmdbuild.service.rest.v2.model.Models.newSession;

import org.cmdbuild.auth.acl.CMGroup;
import org.cmdbuild.auth.multitenant.api.MultitenantService;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.logic.auth.SimpleLoginData;
import org.cmdbuild.service.rest.v2.Sessions;
import org.cmdbuild.service.rest.v2.model.ResponseSingle;
import org.cmdbuild.service.rest.v2.model.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.cmdbuild.auth.session.SessionService;
import org.springframework.stereotype.Component;

@Component("v2_sessions")
public class CxfSessions implements Sessions {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	@Qualifier("v2_errorHandler")
	protected ErrorHandler errorHandler;
	@Autowired
	protected SessionService sessionLogic;
	@Autowired
	protected MultitenantService multitenantService;

	@Override
	public ResponseSingle<Session> create(Session session) {
		if (isBlank(session.getUsername())) {
			errorHandler.missingUsername();
		}
		if (isBlank(session.getPassword())) {
			errorHandler.missingPassword();
		}

		String sessionId = sessionLogic.create(SimpleLoginData.builder()
				.withLoginString(session.getUsername())
				.withPassword(session.getPassword())
				.withGroupName(session.getRole())
				.withServiceUsersAllowed(true)
				.build());
		OperationUser user = sessionLogic.getUser(sessionId);

		return newResponseSingle(Session.class)
				.withElement(newSession()
						.withId(sessionId)
						.withUsername(user.getAuthenticatedUser().getUsername())
						.withRole(user.getDefaultGroupNameOrNull())
						.withAvailableRoles(user.getAuthenticatedUser().getGroupNames())
						.build())
				.build();
	}

	@Override
	public ResponseSingle<Session> read(String sessionId) {
		if (!sessionLogic.exists(sessionId)) {
			errorHandler.sessionNotFound(sessionId);
		}

		OperationUser user = sessionLogic.getUser(sessionId);

		return newResponseSingle(Session.class)
				.withElement(newSession()
						.withId(sessionId)
						.withUsername(user.getAuthenticatedUser().getUsername())
						.withRole(user.getDefaultGroupNameOrNull())
						.withAvailableRoles(user.getAuthenticatedUser().getGroupNames())
						.build())
				.build();
	}

	@Override
	public ResponseSingle<Session> update(String sessionId, Session sessionBean) {
		if (!sessionLogic.exists(sessionId)) {
			errorHandler.sessionNotFound(sessionId);
		}

		if (isBlank(sessionBean.getRole())) {
			errorHandler.missingParam(GROUP);
		}

		OperationUser currentOperationUser = sessionLogic.getUser(sessionId);
		sessionLogic.update(sessionId,
				SimpleLoginData.builder()
						.withLoginString(currentOperationUser.getAuthenticatedUser().getUsername())
						.withGroupName(sessionBean.getRole())
						.withDefaultTenant(null)
						.withActiveTenants(null)
						.withServiceUsersAllowed(true)
						.build());

		OperationUser newOperationUser = sessionLogic.getUser(sessionId);

		return newResponseSingle(Session.class)
				.withElement(newSession()
						.withId(sessionId)
						.withUsername(newOperationUser.getAuthenticatedUser().getUsername())
						.withRole(newOperationUser.getDefaultGroupNameOrNull())
						.withAvailableRoles(newOperationUser.getAuthenticatedUser().getGroupNames())
						.build())
				.build();
	}

	@Override
	public void delete(final String id) {
		if (!sessionLogic.exists(id)) {
			errorHandler.sessionNotFound(id);
		}
		sessionLogic.delete(id);
	}

}
