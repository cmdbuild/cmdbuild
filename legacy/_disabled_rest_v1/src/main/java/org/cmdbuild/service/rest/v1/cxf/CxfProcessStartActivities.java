package org.cmdbuild.service.rest.v1.cxf;

import org.cmdbuild.auth.OperationUserSupplier;
import static org.cmdbuild.service.rest.v1.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseSingle;

import org.cmdbuild.common.utils.UnsupportedProxyFactory;
import org.cmdbuild.service.rest.v1.ProcessStartActivities;
import org.cmdbuild.service.rest.v1.cxf.serialization.ToProcessActivityDefinition;
import org.cmdbuild.service.rest.v1.cxf.serialization.ToProcessActivityWithBasicDetailsFromCMActivity;
import org.cmdbuild.service.rest.v1.model.ProcessActivityWithBasicDetails;
import org.cmdbuild.service.rest.v1.model.ProcessActivityWithFullDetails;
import org.cmdbuild.service.rest.v1.model.ResponseMultiple;
import org.cmdbuild.service.rest.v1.model.ResponseSingle;
import org.cmdbuild.workflow.model.WorkflowException;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.workflow.model.TaskDefinition;
import org.cmdbuild.workflow.core.utils.WorkflowUtils;
import org.cmdbuild.workflow.model.Process;

public class CxfProcessStartActivities implements ProcessStartActivities {

	private static final TaskDefinition UNSUPPORTED_ACTIVITY = UnsupportedProxyFactory.of(TaskDefinition.class).create();

	private static final ToProcessActivityWithBasicDetailsFromCMActivity TO_ACTIVITY_BASIC = ToProcessActivityWithBasicDetailsFromCMActivity
			.newInstance().build();
	private static final ToProcessActivityDefinition TO_ACTIVITY_FULL = ToProcessActivityDefinition.newInstance()
			.withWritableStatus(true) //
			.build();

	private final ErrorHandler errorHandler;
	private final WorkflowService workflowLogic;
	private final OperationUserSupplier userSupplier;

	public CxfProcessStartActivities(OperationUserSupplier userSupplier, final ErrorHandler errorHandler, final WorkflowService workflowLogic) {
		this.errorHandler = errorHandler;
		this.workflowLogic = workflowLogic;
		this.userSupplier = userSupplier;
	}

	@Override
	public ResponseMultiple<ProcessActivityWithBasicDetails> read(final String processId) {
		final Process found = workflowLogic.getPlanClasse(processId);
//		if (found == null) {
//			errorHandler.processNotFound(processId);
//		}
		final TaskDefinition activity = startActivityFor(found);
		final ProcessActivityWithBasicDetails element = TO_ACTIVITY_BASIC.apply(activity);
		return newResponseMultiple(ProcessActivityWithBasicDetails.class) //
				.withElement(element) //
				.withMetadata(newMetadata() //
						.withTotal(1L) //
						.build()) //
				.build();
	}

	@Override
	public ResponseSingle<ProcessActivityWithFullDetails> read(final String processId, final String activityId) {
		final Process found = workflowLogic.getPlanClasse(processId);
//		if (found == null) {
//			errorHandler.processNotFound(processId);
//		}
		final TaskDefinition activity = startActivityFor(found);
		if (!activity.getId().equals(activityId)) {
			errorHandler.processActivityNotFound(activityId);
		}
		final ProcessActivityWithFullDetails element = TO_ACTIVITY_FULL.apply(activity);
		return newResponseSingle(ProcessActivityWithFullDetails.class) //
				.withElement(element) //
				.build();
	}

	private TaskDefinition startActivityFor(Process planClasse) {
		try {
			return WorkflowUtils.getEntryTaskForCurrentUser(planClasse, userSupplier.getUser());
		} catch (final WorkflowException e) {
			errorHandler.propagate(e);
			return UNSUPPORTED_ACTIVITY;
		}
	}

}
