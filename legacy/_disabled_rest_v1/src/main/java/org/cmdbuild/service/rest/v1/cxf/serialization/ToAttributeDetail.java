package org.cmdbuild.service.rest.v1.cxf.serialization;

import static org.cmdbuild.dao.entrytype.AttributeMode.HIDDEN;
import static org.cmdbuild.dao.entrytype.AttributeMode.WRITE;
import static org.cmdbuild.service.rest.v1.model.Models.newAttribute;
import static org.cmdbuild.service.rest.v1.model.Models.newFilter;


import org.apache.commons.lang3.Validate;
import org.cmdbuild.dao.entrytype.attributetype.CMAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.DecimalAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForwardingAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.NullAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.StringAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.TextAttributeType;
import org.cmdbuild.lookup.LookupService;
import org.cmdbuild.service.rest.v1.cxf.ErrorHandler;
import org.cmdbuild.service.rest.v1.model.WsAttribute;
import org.cmdbuild.service.rest.v1.model.Models.AttributeBuilder;
//import org.cmdbuild.services.meta.MetadataStoreFactory;

import com.google.common.base.Function;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.view.DataView;

public class ToAttributeDetail implements Function<Attribute, WsAttribute> {

	public static class Builder implements org.apache.commons.lang3.builder.Builder<ToAttributeDetail> {

		private ErrorHandler errorHandler;
		private AttributeTypeResolver attributeTypeResolver;
		private DataView dataView;
//		private MetadataStoreFactory metadataStoreFactory;
		private LookupService lookupLogic;

		private Builder() {
			// use static method
		}

		@Override
		public ToAttributeDetail build() {
			validate();
			return new ToAttributeDetail(this);
		}

		private void validate() {
			Validate.notNull(errorHandler, "missing '%s'", ErrorHandler.class);
			Validate.notNull(attributeTypeResolver, "missing '%s'", AttributeTypeResolver.class);
			Validate.notNull(dataView, "missing '%s'", DataView.class);
//			Validate.notNull(metadataStoreFactory, "missing '%s'", MetadataStoreFactory.class);
			Validate.notNull(lookupLogic, "missing '%s'", LookupService.class);
		}

		public Builder withAttributeTypeResolver(final AttributeTypeResolver attributeTypeResolver) {
			this.attributeTypeResolver = attributeTypeResolver;
			return this;
		}

		public Builder withDataView(final DataView dataView) {
			this.dataView = dataView;
			return this;
		}

		public Builder withErrorHandler(final ErrorHandler errorHandler) {
			this.errorHandler = errorHandler;
			return this;
		}

//		public Builder withMetadataStoreFactory(final MetadataStoreFactory metadataStoreFactory) {
//			this.metadataStoreFactory = metadataStoreFactory;
//			return this;
//		}
		public Builder withLookupLogic(final LookupService lookupLogic) {
			this.lookupLogic = lookupLogic;
			return this;
		}

	}

	public static Builder newInstance() {
		return new Builder();
	}

	private final ErrorHandler errorHandler;
	private final AttributeTypeResolver attributeTypeResolver;
	private final DataView dataView;
	private final LookupService lookupLogic;

	private ToAttributeDetail(final Builder builder) {
		this.attributeTypeResolver = builder.attributeTypeResolver;
		this.dataView = builder.dataView;
		this.errorHandler = builder.errorHandler;
		this.lookupLogic = builder.lookupLogic;
	}

	@Override
	public WsAttribute apply(final Attribute input) {
		final AttributeBuilder builder = newAttribute() //
				.withId(input.getName()) //
				.withType(attributeTypeResolver.resolve(input).asString()) //
				.withName(input.getName()) //
				.withDescription(input.getDescription()) //
				.thatIsDisplayableInList(input.isDisplayableInList()) //
				.thatIsUnique(input.isUnique()) //
				.thatIsMandatory(input.isMandatory()) //
				.thatIsInherited(input.isInherited()) //
				.thatIsActive(input.isActive()) //
				.withIndex(Long.valueOf(input.getIndex())) //
				.withDefaultValue(input.getDefaultValue()) //
				.withGroup(input.getGroupNameOrNull()) //
				.thatIsWritable(WRITE.equals(input.getMode())) //
				.thatIsHidden(HIDDEN.equals(input.getMode()));
		new ForwardingAttributeTypeVisitor() {

			private final CMAttributeTypeVisitor DELEGATE = NullAttributeTypeVisitor.getInstance();

			private Attribute attribute;
			private AttributeBuilder builder;

			@Override
			protected CMAttributeTypeVisitor delegate() {
				return DELEGATE;
			}

			public void fill(final Attribute attribute, final AttributeBuilder builder) {
				this.attribute = attribute;
				this.builder = builder;
				attribute.getType().accept(this);
			}

			@Override
			public void visit(final DecimalAttributeType attributeType) {
				builder.withPrecision(Long.valueOf(attributeType.precision)) //
						.withScale(Long.valueOf(attributeType.scale));
			}

			@Override
			public void visit(final ForeignKeyAttributeType attributeType) {
				final String className = attributeType.getForeignKeyDestinationClassName();
				final Classe found = dataView.findClasse(className);
				if (found == null) {
					errorHandler.classNotFound(className);
				}

				builder.withTargetClass(found.getName());
			}

			@Override
			public void visit(final LookupAttributeType attributeType) {
				final String name = attributeType.getLookupTypeName();
//				final LookupType found = lookupLogic.typeFor(name);
//				if (found == null) {
//					errorHandler.lookupTypeNotFound(name);
//				}
				builder.withLookupType(lookupLogic.getLookupType(name).getName());
			}

			@Override
			public void visit(final ReferenceAttributeType attributeType) {
				final String domainName = attributeType.getDomainName();
				final Domain domain = dataView.findDomain(domainName);
				if (domain == null) {
					errorHandler.domainNotFound(domainName);
				}

				final String domainCardinality = domain.getCardinality();
				Classe target = null;
				if ("N:1".equals(domainCardinality)) {
					target = domain.getTargetClass();
				} else if ("1:N".equals(domainCardinality)) {
					target = domain.getSourceClass();
				}

				builder.withTargetClass(target.getName()) //
						.withDomainName(domain.getName()) //
						.withFilter(newFilter() //
								.withText(attribute.getFilter()) //
								//								.withParams(toMap(metadataStoreFactory.getAllMetadataForAttribute(attribute))) //
								.build());
			}

			;

//			private Map<String, String> toMap(final Collection<Metadata> elements) {
//				final Map<String, String> map = Maps.newHashMap();
//				for (final Metadata element : elements) {
//					map.put(element.getName(), element.getValue());
//				}
//				return map;
//			}

			@Override
			public void visit(final StringAttributeType attributeType) {
				builder.withLength(Long.valueOf(attributeType.length));
			}

			@Override
			public void visit(final TextAttributeType attributeType) {
				builder.withEditorType(attribute.getEditorType());
			}

		}.fill(input, builder);
		return builder.build();
	}

}
