package org.cmdbuild.service.rest.v1.cxf.serialization;

import static com.google.common.collect.Maps.newHashMap;
import static org.cmdbuild.service.rest.v1.model.Models.newAttachment;
import static org.cmdbuild.service.rest.v1.model.Models.newValues;

import java.util.Date;
import java.util.Map;

import org.cmdbuild.dao.entrytype.attributetype.DateAttributeType;
import org.cmdbuild.service.rest.v1.model.Attachment;
import org.cmdbuild.service.rest.v1.model.Values;

import com.google.common.base.Function;
import org.cmdbuild.dms.inner.DocumentMetadata;
import org.cmdbuild.dms.inner.DocumentInfoAndDetail;
import static org.cmdbuild.service.rest.v1.cxf.serialization.Converter.toClient;

public class ToAttachment implements Function<DocumentInfoAndDetail, Attachment> {

	public static class Builder implements org.apache.commons.lang3.builder.Builder<ToAttachment> {

		private boolean metadata;

		private Builder() {
			// use factory method
		}

		@Override
		public ToAttachment build() {
			validate();
			return new ToAttachment(this);
		}

		private void validate() {
			// TODO Auto-generated method stub
		}

		public Builder withMetadata(final boolean metadata) {
			this.metadata = metadata;
			return this;
		}
	}

	public static Builder newInstance() {
		return new Builder();
	}

	private static final DateAttributeType DATE_ATTRIBUTE_TYPE = new DateAttributeType();

//	private static final Iterable<MetadataGroup> NO_METADATA_GROUPS = emptyList();
	private final boolean metadata;

	private ToAttachment(final Builder builder) {
		this.metadata = builder.metadata;
	}

	@Override
	public Attachment apply(final DocumentInfoAndDetail input) {
		return newAttachment() //
				.withId(input.getId()) //
				.withName(input.getId()) //
				.withCategory(input.getCategory()) //
				.withDescription(input.getDescription()) //
				.withVersion(input.getVersion()) //
				.withAuthor(input.getAuthor()) //
				.withCreated(dateAsString(input.getCreated_old())) //
				.withModified(dateAsString(input.getModified_old())) //
				.withMetadata(metadata(input.getMetadata())) //
				.build();
	}

	private String dateAsString(final Date input) {
		return toClient().convert(DATE_ATTRIBUTE_TYPE, input).toString();

	}

	private Values metadata(DocumentMetadata metadataGroups) {
		Map<String, Object> values = newHashMap();
		if (this.metadata) {
			metadataGroups.getAllMetadataByCategory().forEach((category, map) -> {
				map.forEach((key, value) -> {
					values.put(key, value);
				});
			});
		}
		return newValues().withValues(values).build();
	}

}
