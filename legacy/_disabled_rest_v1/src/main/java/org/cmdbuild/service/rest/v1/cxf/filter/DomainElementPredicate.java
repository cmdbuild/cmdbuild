package org.cmdbuild.service.rest.v1.cxf.filter;

import org.cmdbuild.logic.data.access.filter.model.Attr;
import org.cmdbuild.logic.data.access.filter.model.Element;

import com.google.common.base.Predicate;
import org.cmdbuild.dao.entrytype.Domain;

public class DomainElementPredicate extends ElementPredicate<Domain> {

	public DomainElementPredicate(final Element element) {
		super(element);
	}

	@Override
	protected Predicate<Domain> predicateOf(final Attr element) {
		return new DomainAttributePredicate(element);
	}

	@Override
	protected Predicate<Domain> predicateOf(final Element element) {
		return new DomainElementPredicate(element);
	}

}