package org.cmdbuild.service.rest.v1.cxf.configuration;

import org.cmdbuild.lookup.LookupService;
import org.cmdbuild.workflow.core.LookupHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.cmdbuild.auth.OperationUserSupplier;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.auth.session.SessionService;
import org.cmdbuild.config.CoreConfiguration;
import org.cmdbuild.logic.auth.AuthenticationServiceTwo;
import org.cmdbuild.authorization.AuthorizationService;
import org.cmdbuild.menu.MenuService;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.dao.view.DataView;

@Component
public class ApplicationContextHelperV1 {

	@Autowired
	private ApplicationContext applicationContext;

	public AuthenticationServiceTwo authenticationLogic() {
		return applicationContext.getBean(SessionService.class);
	}

	public CoreConfiguration cmdbuildConfiguration() {
		return applicationContext.getBean(CoreConfiguration.class);
	}

	public DmsService dmsLogic() {
		return applicationContext.getBean(DmsService.class);
	}

	public LookupHelper lookupHelper() {
		return applicationContext.getBean(LookupHelper.class);
	}

	public LookupService lookupLogic() {
		return applicationContext.getBean(LookupService.class);
	}

	public MenuService menuLogic() {
		return applicationContext.getBean(MenuService.class);
	}

	public AuthorizationService securityLogic() {
		return applicationContext.getBean(AuthorizationService.class);
	}

	public SessionService sessionLogic() {
		return applicationContext.getBean(SessionService.class);
	}

	public DataAccessService systemDataAccessLogic() {
		return applicationContext.getBean(DataAccessService.class);
//		return applicationContext.getBean(SystemDataAccessLogic.class);
	}

	public DataView systemDataView() {
		return applicationContext.getBean("systemDataView", DataView.class);
	}

	public DataAccessService userDataAccessLogic() {
//		return applicationContext.getBean("webServiceDataAccessLogic", DataAccessLogic.class);
		return applicationContext.getBean(DataAccessService.class);
	}

	public DataView userDataView() {
		return applicationContext.getBean("UserDataView", DataView.class);
	}

	public OperationUserSupplier userStore() {
		return applicationContext.getBean(OperationUserSupplier.class);
	}

	public WorkflowService userWorkflowLogic() {
		return applicationContext.getBean(WorkflowService.class);
	}

}
