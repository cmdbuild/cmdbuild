package org.cmdbuild.service.rest.v1.cxf.serialization;

import static org.cmdbuild.service.rest.v1.model.Models.newProcessStatus;
import static org.cmdbuild.service.rest.v1.model.ProcessStatus.ABORTED;
import static org.cmdbuild.service.rest.v1.model.ProcessStatus.COMPLETED;
import static org.cmdbuild.service.rest.v1.model.ProcessStatus.OPEN;
import static org.cmdbuild.service.rest.v1.model.ProcessStatus.SUSPENDED;

import java.util.Map;

import org.cmdbuild.lookup.Lookup;
import org.cmdbuild.service.rest.v1.model.ProcessStatus;

import com.google.common.base.Function;
import org.cmdbuild.utils.lang.CmdbMapUtils;

public class ToProcessStatus implements Function<Lookup, ProcessStatus> {

	public static final String STATE_CLOSED_ABORTED = "closed.aborted";
	public static final String STATE_CLOSED_COMPLETED = "closed.completed";
	public static final String STATE_OPEN_NOT_RUNNING_SUSPENDED = "open.not_running.suspended";
	public static final String STATE_OPEN_RUNNING = "open.running";
	public static final String STATE_CLOSED_TERMINATED = "closed.terminated";

	private static final Map<String, String> PROCESS_STATUS_CODE_MAP = CmdbMapUtils.<String, String>map(
			STATE_OPEN_RUNNING, OPEN,
			STATE_OPEN_NOT_RUNNING_SUSPENDED, SUSPENDED,
			STATE_CLOSED_COMPLETED, COMPLETED,
			STATE_CLOSED_ABORTED, ABORTED
	).immutable();

	@Override
	public ProcessStatus apply(final Lookup input) {
		return newProcessStatus() //
				.withId(input.getId()) //
				// FIXME do it in a better way
				.withValue(PROCESS_STATUS_CODE_MAP.get(input.getCode())) //
				.withDescription(input.getDescription()) //
				.build();
	}

}
