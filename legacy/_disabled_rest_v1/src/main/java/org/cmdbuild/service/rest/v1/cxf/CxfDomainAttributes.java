package org.cmdbuild.service.rest.v1.cxf;

import static com.google.common.collect.FluentIterable.from;
import static org.cmdbuild.service.rest.v1.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseMultiple;

import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.lookup.LookupService;
import org.cmdbuild.service.rest.v1.DomainAttributes;
import org.cmdbuild.service.rest.v1.cxf.serialization.AttributeTypeResolver;
import org.cmdbuild.service.rest.v1.cxf.serialization.ToAttributeDetail;
import org.cmdbuild.service.rest.v1.model.WsAttribute;
import org.cmdbuild.service.rest.v1.model.ResponseMultiple;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.data2.api.DataAccessService.AttrQueryParams;
import org.cmdbuild.dao.entrytype.Attribute;
import static org.cmdbuild.spring.SpringIntegrationUtils.applicationContext;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.view.DataView;

public class CxfDomainAttributes implements DomainAttributes {

	private static final AttributeTypeResolver ATTRIBUTE_TYPE_RESOLVER = new AttributeTypeResolver();

	private final ErrorHandler errorHandler;
	private final DataAccessService userDataAccessLogic;
	private final DataView systemDataView;
	private final LookupService lookupLogic;

	public CxfDomainAttributes(final ErrorHandler errorHandler, final DataAccessService userDataAccessLogic,
			final DataView systemDataView,
			final LookupService lookupLogic) {
		this.errorHandler = errorHandler;
		this.userDataAccessLogic = userDataAccessLogic;
		this.systemDataView = systemDataView;
		this.lookupLogic = lookupLogic;
	}

	@Override
	public ResponseMultiple<WsAttribute> readAll(final String domainId, final boolean activeOnly, final Integer limit,
			final Integer offset) {
		final Domain target = userDataAccessLogic.findDomain(domainId);
		if (target == null) {
			errorHandler.domainNotFound(domainId);
		}
		final PagedElements<Attribute> filteredAttributes = userDataAccessLogic.getDomainAttributes(//
				target.getName(), //
				activeOnly, //
				new AttrQueryParams() {

					@Override
					public Integer limit() {
						return limit;
					}

					@Override
					public Integer offset() {
						return offset;
					}

				});

		final ToAttributeDetail toAttributeDetails = ToAttributeDetail.newInstance() //
				.withAttributeTypeResolver(ATTRIBUTE_TYPE_RESOLVER) //
				.withDataView(systemDataView) //
				.withErrorHandler(errorHandler) //
//				.withMetadataStoreFactory(metadataStoreFactory) //
				.withLookupLogic(lookupLogic) //
				.build();
		final Iterable<WsAttribute> elements = from(filteredAttributes) //
				.transform(toAttributeDetails);
		return newResponseMultiple(WsAttribute.class) //
				.withElements(elements) //
				.withMetadata(newMetadata() //
						.withTotal(Long.valueOf(filteredAttributes.totalSize())) //
						.build()) //
				.build();
	}

}
