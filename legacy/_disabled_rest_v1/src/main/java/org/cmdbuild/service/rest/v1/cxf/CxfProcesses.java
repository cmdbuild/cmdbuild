package org.cmdbuild.service.rest.v1.cxf;

import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Iterables.size;
import static org.cmdbuild.service.rest.v1.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseSingle;

import java.util.Comparator;

import org.cmdbuild.service.rest.v1.Processes;
import org.cmdbuild.service.rest.v1.cxf.serialization.ToFullProcessDetail;
import org.cmdbuild.service.rest.v1.cxf.serialization.ToSimpleProcessDetail;
import org.cmdbuild.service.rest.v1.model.ProcessWithBasicDetails;
import org.cmdbuild.service.rest.v1.model.ProcessWithFullDetails;
import org.cmdbuild.service.rest.v1.model.ResponseMultiple;
import org.cmdbuild.service.rest.v1.model.ResponseSingle;

import com.google.common.collect.Ordering;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.workflow.model.Process;

public class CxfProcesses implements Processes {

	private static final ToSimpleProcessDetail TO_SIMPLE_DETAIL = ToSimpleProcessDetail.newInstance().build();

	private static final Comparator<Classe> NAME_ASC = new Comparator<Classe>() {

		@Override
		public int compare(final Classe o1, final Classe o2) {
			return o1.getName().compareTo(o2.getName());
		}

	};

	private final ErrorHandler errorHandler;
	private final WorkflowService workflowLogic;
	private final ToFullProcessDetail toFullDetail;

	public CxfProcesses(final ErrorHandler errorHandler, final WorkflowService workflowLogic,
			final ProcessStatusHelper processStatusHelper) {
		this.errorHandler = errorHandler;
		this.workflowLogic = workflowLogic;
		this.toFullDetail = ToFullProcessDetail.newInstance() //
				.withLookupHelper(processStatusHelper) //
				.build();
	}

	@Override
	public ResponseMultiple<ProcessWithBasicDetails> readAll(final boolean activeOnly, final Integer limit,
			final Integer offset) {
		final Iterable<? extends Process> all = activeOnly ? workflowLogic.getActiveProcessClasses() : workflowLogic.getAllProcessClasses();
		final Iterable<? extends Process> ordered = Ordering.from(NAME_ASC) //
				.sortedCopy(all);
		final Iterable<ProcessWithBasicDetails> elements = from(ordered) //
				.skip((offset == null) ? 0 : offset) //
				.limit((limit == null) ? Integer.MAX_VALUE : limit) //
				.transform(TO_SIMPLE_DETAIL);
		return newResponseMultiple(ProcessWithBasicDetails.class) //
				.withElements(elements) //
				.withMetadata(newMetadata() //
						.withTotal(Long.valueOf(size(ordered))) //
						.build()) //
				.build();
	}

	@Override
	public ResponseSingle<ProcessWithFullDetails> read(final String processId) {
		final Classe found = workflowLogic.getPlanClasse(processId);
//		if (found == null) {
//			errorHandler.processNotFound(processId);
//		}
		final ProcessWithFullDetails element = toFullDetail.apply(found);
		return newResponseSingle(ProcessWithFullDetails.class) //
				.withElement(element) //
				.build();
	}

}
