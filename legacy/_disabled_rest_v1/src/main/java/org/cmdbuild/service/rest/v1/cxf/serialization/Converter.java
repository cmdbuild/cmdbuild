package org.cmdbuild.service.rest.v1.cxf.serialization;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.common.Constants.REST_ALL_DATES_PATTERN;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import org.cmdbuild.dao.beans.IdAndDescription;
import static org.cmdbuild.utils.date.DateUtils.toJavaDate;

import org.cmdbuild.dao.entrytype.attributetype.CMAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.DateAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.DateTimeAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForeignKeyAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.ForwardingAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.NullAttributeTypeVisitor;
import org.cmdbuild.dao.entrytype.attributetype.ReferenceAttributeType;
import org.cmdbuild.dao.entrytype.attributetype.TimeAttributeType;
import static org.cmdbuild.dao.utils.AttributeConversionUtils.rawToSystem;
import org.cmdbuild.dao.entrytype.attributetype.CardAttributeType;

public class Converter {

	private static final DateFormat dateFormat = new SimpleDateFormat(REST_ALL_DATES_PATTERN);

	public static interface ValueConverter {

		Object convert(CardAttributeType<?> attributeType, Object value);

	}

	private static class ToClientImpl extends ForwardingAttributeTypeVisitor implements ValueConverter {

		private static final CMAttributeTypeVisitor DELEGATE = NullAttributeTypeVisitor.getInstance();

		private Object input;
		private Object output;

		@Override
		protected CMAttributeTypeVisitor delegate() {
			return DELEGATE;
		}

		@Override
		public Object convert(CardAttributeType<?> attributeType, final Object value) {
			output = input = value;
			attributeType.accept(this);
			return output;
		}

		@Override
		public void visit(DateAttributeType attributeType) {
			output = convert(rawToSystem(attributeType, input));
		}

		@Override
		public void visit(DateTimeAttributeType attributeType) {
			output = convert(rawToSystem(attributeType, input));
		}

		@Override
		public void visit(ForeignKeyAttributeType attributeType) {
			output = convert(rawToSystem(attributeType, input));
		}

		@Override
		public void visit(LookupAttributeType attributeType) {
			output = convert(rawToSystem(attributeType, input));
		}

		@Override
		public void visit(ReferenceAttributeType attributeType) {
			output = convert(rawToSystem(attributeType, input));
		}

		@Override
		public void visit(TimeAttributeType attributeType) {
			output = convert(rawToSystem(attributeType, input));
		}

		private static Object convert(ZonedDateTime value) {
			return (value == null) ? null : dateFormat.format(toJavaDate(value));
		}

		private static Object convert(LocalDate value) {
			return (value == null) ? null : dateFormat.format(toJavaDate(value));
		}

		private static Object convert(LocalTime value) {
			return (value == null) ? null : dateFormat.format(toJavaDate(value));
		}

		private static Object convert(IdAndDescription value) {
			return (value == null) ? null : value.getId();
		}

	}

	private static class FromClientImpl extends ForwardingAttributeTypeVisitor implements ValueConverter {

		private static final CMAttributeTypeVisitor DELEGATE = NullAttributeTypeVisitor.getInstance();

		private Object input;
		private Object output;

		@Override
		protected CMAttributeTypeVisitor delegate() {
			return DELEGATE;
		}

		@Override
		public Object convert(final CardAttributeType<?> attributeType, final Object value) {
			output = input = value;
			attributeType.accept(this);
			return output;
		}

		@Override
		public void visit(final DateAttributeType attributeType) {
			output = convertDate(input);
		}

		@Override
		public void visit(final DateTimeAttributeType attributeType) {
			output = convertDate(input);
		}

		@Override
		public void visit(final TimeAttributeType attributeType) {
			output = convertDate(input);
		}

		private static Object convertDate(final Object input) {
			try {
				final String s = String.class.cast(input);
				return isBlank(s) ? null : dateFormat.parse(s);
			} catch (final ParseException e) {
				throw new RuntimeException(e);
			}
		}

	}

	public static ValueConverter toClient() {
		return new ToClientImpl();
	}

	public static ValueConverter fromClient() {
		return new FromClientImpl();
	}

}
