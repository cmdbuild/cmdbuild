package org.cmdbuild.service.rest.v1.cxf;

import static com.google.common.collect.FluentIterable.from;
import static org.cmdbuild.service.rest.v1.model.Models.newMenu;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseSingle;

import org.cmdbuild.service.rest.v1.Menu;
import org.cmdbuild.service.rest.v1.model.MenuDetail;
import org.cmdbuild.service.rest.v1.model.ResponseSingle;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Ordering;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.menu.MenuService;
import org.cmdbuild.dao.view.DataView;
import org.cmdbuild.menu.inner.MenuJsonNode;

public class CxfMenu implements Menu {

//	private static final class MenuItemToMenuDetail implements Function<MenuJsonNode, MenuDetail> {
//
//		private final DataView dataView;
//
//		public MenuItemToMenuDetail(final DataView dataView) {
//			this.dataView = dataView;
//		}
//
//		@Override
//		public MenuDetail apply(final MenuJsonNode input) {
//			final Classe referencedClass = dataView.findClasse(input.getReferencedClassName());
//			return newMenu() //
//					.withMenuType(input.getType().getValue()) // TODO translate
//					.withIndex(Long.valueOf(input.getIndex())) //
//					.withObjectType((referencedClass == null) ? null : referencedClass.getName()) //
//					.withObjectId(input.getReferencedElementId().longValue()) //
//					.withObjectDescription(input.getDescription()) //
//					.withChildren(from(Ordering.natural().onResultOf(MenuJsonNode::getIndex) //
//											.sortedCopy(input.getChildren()) //
//							) //
//									.transform(this) //
//									.toList()) //
//					.build();
//		}
//
//	};

	private final Supplier<String> currentGroupSupplier;
	private final MenuService menuLogic;
	private final DataView dataView;

	public CxfMenu(final Supplier<String> currentGroupSupplier, final MenuService menuLogic, final DataView dataView) {
		this.currentGroupSupplier = currentGroupSupplier;
		this.menuLogic = menuLogic;
		this.dataView = dataView;
	}

	@Override
	public ResponseSingle<MenuDetail> read() {
		throw new UnsupportedOperationException();
//		String group = currentGroupSupplier.get();
//		MenuJsonNode menuItem = menuLogic.getMenuForCurrentUser();
//		MenuDetail element = new MenuItemToMenuDetail(dataView).apply(menuItem);
//		return newResponseSingle(MenuDetail.class) //
//				.withElement(element) //
//				.build();
	}

}
