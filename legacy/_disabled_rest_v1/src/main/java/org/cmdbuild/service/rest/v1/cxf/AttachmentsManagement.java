package org.cmdbuild.service.rest.v1.cxf;

import static com.google.common.collect.FluentIterable.from;

import javax.activation.DataHandler;

import org.cmdbuild.service.rest.v1.cxf.serialization.ToAttachment;
import org.cmdbuild.service.rest.v1.model.Attachment;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import org.cmdbuild.auth.OperationUserSupplier;
import org.cmdbuild.dms.inner.DocumentInfoAndDetail;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.dms.DocumentDataImpl;

public class AttachmentsManagement implements AttachmentsHelper {

	private static final Function<DocumentInfoAndDetail, Attachment> TO_ATTACHMENT_WITH_NO_METADATA
			= ToAttachment.newInstance() //
					.build();

	private static final Function<DocumentInfoAndDetail, Attachment> TO_ATTACHMENT_WITH_METADATA = ToAttachment.newInstance() //
			.withMetadata(true) //
			.build();

	private final DmsService dmsLogic;
	private final OperationUserSupplier userStore;

	public AttachmentsManagement(final DmsService dmsLogic, final OperationUserSupplier userStore) {
		this.dmsLogic = dmsLogic;
		this.userStore = userStore;
	}

	@Override
	public String create(final String classId, final Long cardId, final String attachmentName,
			final Attachment attachment, final DataHandler dataHandler) throws Exception {
		dmsLogic.create(classId, cardId, DocumentDataImpl.builder()
				.withAuthor(userStore.getUser().getAuthenticatedUser().getUsername())
				.withData(dataHandler.getInputStream())
				.withFilename(attachmentName)
				.withCategory(attachment.getCategory())
				.withDescription(attachment.getDescription())
				.withMajorVersion(true)
				.build());
		return dataHandler.getName();
	}

	@Override
	public void update(final String classId, final Long cardId, final String attachmentId, final Attachment attachment,
			final DataHandler dataHandler) throws Exception {
		dmsLogic.update(  classId,  cardId, attachmentId,DocumentDataImpl.builder()
				.withAuthor(userStore.getUser().getAuthenticatedUser().getUsername())
				.withData(dataHandler)
				.withCategory(attachment.getCategory())
				.withDescription(attachment.getDescription())
				.withMajorVersion(false)
				.build());
	}

	@Override
	public Iterable<Attachment> search(final String classId, final Long cardId) {
		final Iterable<DocumentInfoAndDetail> documents = dmsLogic.getCardAttachments(classId, cardId);
		final Iterable<Attachment> elements = from(documents) //
				.transform(TO_ATTACHMENT_WITH_NO_METADATA);
		return elements;
	}

	@Override
	public Optional<Attachment> search(final String classId, final Long cardId, final String attachmentId) {
		final Optional<DocumentInfoAndDetail> document = dmsLogic.getCardAttachment(classId, cardId, attachmentId);
		if (!document.isPresent()) {
			return Optional.absent();
		}
		final Attachment element = TO_ATTACHMENT_WITH_METADATA.apply(document.get());
		return Optional.of(element);
	}

	@Override
	public DataHandler download(final String classId, final Long cardId, final String attachmentId) {
		return dmsLogic.download(classId, cardId, attachmentId, null);
	}

	@Override
	public void delete(final String classId, final Long cardId, final String attachmentId) {
		dmsLogic.delete(classId, cardId, attachmentId);
	}

}
