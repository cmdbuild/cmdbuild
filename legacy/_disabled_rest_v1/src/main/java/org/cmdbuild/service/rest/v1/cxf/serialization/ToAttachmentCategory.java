package org.cmdbuild.service.rest.v1.cxf.serialization;

import static org.cmdbuild.service.rest.v1.model.Models.newAttachmentCategory;

import org.cmdbuild.dms.inner.DocumentTypeDefinition;
import org.cmdbuild.service.rest.v1.model.AttachmentCategory;

import com.google.common.base.Function;

public class ToAttachmentCategory implements Function<DocumentTypeDefinition, AttachmentCategory> {

	@Override
	public AttachmentCategory apply(final DocumentTypeDefinition input) {
		return newAttachmentCategory() //
				.withId(input.getCategory()) //
				.withDescription(input.getCategory()) //
				.build();
	}

}