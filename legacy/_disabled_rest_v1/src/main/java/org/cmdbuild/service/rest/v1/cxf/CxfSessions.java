package org.cmdbuild.service.rest.v1.cxf;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.cmdbuild.service.rest.v1.constants.Serialization.GROUP;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseSingle;
import static org.cmdbuild.service.rest.v1.model.Models.newSession;

import org.cmdbuild.auth.acl.CMGroup;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.logic.auth.SimpleLoginData;
import org.cmdbuild.service.rest.v1.Sessions;
import org.cmdbuild.service.rest.v1.logging.LoggingSupport;
import org.cmdbuild.service.rest.v1.model.ResponseSingle;
import org.cmdbuild.service.rest.v1.model.Session;
import org.cmdbuild.auth.session.SessionService;

public class CxfSessions implements Sessions, LoggingSupport {

	private final ErrorHandler errorHandler;
	private final SessionService sessionLogic;

	public CxfSessions(final ErrorHandler errorHandler, final SessionService sessionLogic) {
		this.errorHandler = errorHandler;
		this.sessionLogic = sessionLogic;
	}

	@Override
	public ResponseSingle<Session> create(final Session session) {
		if (isBlank(session.getUsername())) {
			errorHandler.missingUsername();
		}
		if (isBlank(session.getPassword())) {
			errorHandler.missingPassword();
		}

		final String sessionId = sessionLogic.create(SimpleLoginData.builder() //
				.withLoginString(session.getUsername()) //
				.withPassword(session.getPassword()) //
				.withGroupName(session.getRole()) //
				.withServiceUsersAllowed(true) //
				.build());
		final OperationUser user = sessionLogic.getUser(sessionId);

		return newResponseSingle(Session.class) //
				.withElement(newSession() //
						.withId(sessionId) //
						.withUsername(user.getAuthenticatedUser().getUsername()) //
						.withRole(user.getDefaultGroupNameOrNull()) //
						.withAvailableRoles(user.getAuthenticatedUser().getGroupNames()) //
						.build()) //
				.build();
	}

	@Override
	public ResponseSingle<Session> read(final String id) {
		if (!sessionLogic.exists(id)) {
			errorHandler.sessionNotFound(id);
		}

		OperationUser user = sessionLogic.getUser(id);

		return newResponseSingle(Session.class) //
				.withElement(newSession() //
						.withId(id) //
						.withUsername(user.getAuthenticatedUser().getUsername()) //
						.withRole(user.getDefaultGroupNameOrNull())
						.withAvailableRoles(user.getAuthenticatedUser().getGroupNames()) //
						.build()) //
				.build();
	}

	@Override
	public ResponseSingle<Session> update(final String id, final Session session) {
		if (!sessionLogic.exists(id)) {
			errorHandler.sessionNotFound(id);
		}

		if (isBlank(session.getRole())) {
			errorHandler.missingParam(GROUP);
		}

		final OperationUser user = sessionLogic.getUser(id);
		sessionLogic.update(id,
				SimpleLoginData.builder() //
						.withLoginString(user.getAuthenticatedUser().getUsername()) //
						.withGroupName(session.getRole()) //
						.withServiceUsersAllowed(true) //
						.build());

		final OperationUser updatedUser = sessionLogic.getUser(id);
//		final CMGroup group = updatedUser.getDefaultGroupOrNull();

		return newResponseSingle(Session.class) //
				.withElement(newSession() //
						.withId(id) //
						.withUsername(updatedUser.getAuthenticatedUser().getUsername()) //
						.withRole(updatedUser.getDefaultGroupNameOrNull()) //
						.withAvailableRoles(updatedUser.getAuthenticatedUser().getGroupNames()) //
						.build()) //
				.build();
	}

	@Override
	public void delete(final String id) {
		if (!sessionLogic.exists(id)) {
			errorHandler.sessionNotFound(id);
		}

		sessionLogic.delete(id);
	}

}
