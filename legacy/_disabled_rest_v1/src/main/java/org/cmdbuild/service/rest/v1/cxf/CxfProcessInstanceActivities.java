package org.cmdbuild.service.rest.v1.cxf;

import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.size;
import static java.util.Arrays.asList;
import static org.cmdbuild.service.rest.v1.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseSingle;


import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.logic.mapping.json.FilterElementGetters;
import org.cmdbuild.logic.mapping.json.JsonFilterHelper;
import org.cmdbuild.service.rest.v1.ProcessInstanceActivities;
import org.cmdbuild.service.rest.v1.cxf.serialization.ToProcessActivityDefinition;
import org.cmdbuild.service.rest.v1.cxf.serialization.ToProcessActivityWithBasicDetailsFromUserActivityInstance;
import org.cmdbuild.service.rest.v1.model.ProcessActivityWithBasicDetails;
import org.cmdbuild.service.rest.v1.model.ProcessActivityWithFullDetails;
import org.cmdbuild.service.rest.v1.model.ResponseMultiple;
import org.cmdbuild.service.rest.v1.model.ResponseSingle;
import org.cmdbuild.workflow.model.WorkflowException;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Predicates;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.Process;

public class CxfProcessInstanceActivities implements ProcessInstanceActivities {

	private static final ToProcessActivityWithBasicDetailsFromUserActivityInstance TO_OUTPUT = ToProcessActivityWithBasicDetailsFromUserActivityInstance
			.newInstance() //
			.build();

	private final ErrorHandler errorHandler;
	private final WorkflowService workflowLogic;

	public CxfProcessInstanceActivities(final ErrorHandler errorHandler, final WorkflowService workflowLogic) {
		this.errorHandler = errorHandler;
		this.workflowLogic = workflowLogic;
	}

	@Override
	public ResponseMultiple<ProcessActivityWithBasicDetails> read(final String processId, final Long processInstanceId) {
		final Process found = workflowLogic.getPlanClasse(processId);
//		if (found == null) {
//			errorHandler.processNotFound(processId);
//		}
		final QueryOptionsImpl queryOptions = QueryOptionsImpl.builder() //
				.limit(1) //
				.offset(0) //
				.filter(filterFor(processInstanceId)) //
				.build();
		final PagedElements<Flow> elements = workflowLogic.getFlowCardsByClasseAndQueryOptions(found, queryOptions);
		if (elements.totalSize() == 0) {
			errorHandler.processInstanceNotFound(processInstanceId);
		}
		final Iterable<Task> activities = workflowLogic.getTaskList(getOnlyElement(elements));
		return newResponseMultiple(ProcessActivityWithBasicDetails.class) //
				.withElements(from(activities) //
						.transform(TO_OUTPUT) //
				) //
				.withMetadata(newMetadata() //
						.withTotal(Long.valueOf(size(activities))) //
						.build() //
				).build();
	}

	private JSONObject filterFor(final Long id) {
		try {
			final JSONObject emptyFilter = new JSONObject();
			return new JsonFilterHelper(emptyFilter) //
					.merge(FilterElementGetters.id(id));
		} catch (final JSONException e) {
			errorHandler.propagate(e);
			return new JSONObject();
		}
	}

	@Override
	public ResponseSingle<ProcessActivityWithFullDetails> read(final String processId, final Long processInstanceId,
			final String processActivityId) {
//		final PlanClasse foundType = workflowLogic.getProcessClasse(processId);
//		if (foundType == null) {
//			errorHandler.processNotFound(processId);
//		}
		final Flow foundInstance = workflowLogic.getFlowCard(processId, processInstanceId);
		if (foundInstance == null) {
			errorHandler.processInstanceNotFound(processInstanceId);
		}
		final Task activityInstance = workflowLogic.getUserTask(foundInstance, processActivityId);
		if (activityInstance == null) {
			errorHandler.processActivityNotFound(processActivityId);
		}
		try {
//			final TaskDefinition delegate = activityInstance.getDefinition();
//			final TaskDefinition foundActivity = new ForwardingActivity() {
//
//				@Override
//				protected TaskDefinition delegate() {
//					return delegate;
//				}
//
//				@Override
//				public List<Widget> getWidgets() {
//					return activityInstance.getWidgets();
//				}
//
//			};
			final ToProcessActivityDefinition TO_PROCESS_ACTIVITY = ToProcessActivityDefinition.newInstance() //
					.withWritableStatus(activityInstance.isWritable()) //
					.build();
			return newResponseSingle(ProcessActivityWithFullDetails.class) //
					.withElement(from(asList(activityInstance.getDefinition())) //
							.filter(Predicates.notNull()) //
							.transform(TO_PROCESS_ACTIVITY) //TODO get widgets from task
							.first() //
							.get()) //
					.build();
		} catch (final WorkflowException e) {
			errorHandler.propagate(e);
			return null;
		}
	}

}
