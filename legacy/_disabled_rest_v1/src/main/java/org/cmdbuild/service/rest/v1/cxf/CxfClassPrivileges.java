package org.cmdbuild.service.rest.v1.cxf;

import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.Iterables.size;
import static org.cmdbuild.service.rest.v1.model.Models.newClassPrivilege;
import static org.cmdbuild.service.rest.v1.model.Models.newMetadata;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseMultiple;
import static org.cmdbuild.service.rest.v1.model.Models.newResponseSingle;

import org.cmdbuild.auth.acl.CMGroup;
import org.cmdbuild.auth.acl.SerializablePrivilege;
import org.cmdbuild.auth.privileges.constants.GrantMode;
import org.cmdbuild.authorization.GrantInfo;
import org.cmdbuild.service.rest.v1.ClassPrivileges;
import org.cmdbuild.service.rest.v1.model.ClassPrivilege;
import org.cmdbuild.service.rest.v1.model.ResponseMultiple;
import org.cmdbuild.service.rest.v1.model.ResponseSingle;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import org.cmdbuild.data2.api.DataAccessService;
import org.cmdbuild.logic.auth.AuthenticationServiceTwo;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.authorization.AuthorizationService;

public class CxfClassPrivileges implements ClassPrivileges {

	private final ErrorHandler errorHandler;
	private final AuthenticationServiceTwo authenticationLogic;
	private final AuthorizationService securityLogic;
	private final DataAccessService dataAccessLogic;

	public CxfClassPrivileges(final ErrorHandler errorHandler, final AuthenticationServiceTwo authenticationLogic,
			final AuthorizationService securityLogic, final DataAccessService dataAccessLogic) {
		this.errorHandler = errorHandler;
		this.authenticationLogic = authenticationLogic;
		this.securityLogic = securityLogic;
		this.dataAccessLogic = dataAccessLogic;
	}

	@Override
	public ResponseMultiple<ClassPrivilege> read(final String roleId) {
		final CMGroup role = authenticationLogic.getGroupWithName(roleId);
//		if (role instanceof NullGroup) {
//			errorHandler.roleNotFound(roleId);
//		}
		final Iterable<ClassPrivilege> elements = from(classPrivileges(role)).toList();
		return newResponseMultiple(ClassPrivilege.class) //
				.withElements(elements) //
				.withMetadata(newMetadata() //
						.withTotal(Long.valueOf(size(elements))) //
						.build()) //
				.build();
	}

	@Override
	public ResponseSingle<ClassPrivilege> read(final String roleId, final String classId) {
		final CMGroup role = authenticationLogic.getGroupWithName(roleId);
//		if (role instanceof NullGroup) {
//			errorHandler.roleNotFound(roleId);
//		}
		final Optional<ClassPrivilege> element = from(classPrivileges(role)) //
				.filter(new Predicate<ClassPrivilege>() {

					@Override
					public boolean apply(final ClassPrivilege input) {
						return input.getId().equals(classId);
					}

				}) //
				.first();
		if (!element.isPresent()) {
			errorHandler.classNotFound(classId);
		}
		return newResponseSingle(ClassPrivilege.class) //
				.withElement(element.get()) //
				.build();
	}

	private Iterable<ClassPrivilege> classPrivileges(final CMGroup role) {
		final Iterable<ClassPrivilege> elements;
		if (role.isAdmin()) {
			elements = from(dataAccessLogic.findClasses(true)) //
					.transform(classPrivilegeWithWriteMode());
		} else {
			elements = from(securityLogic.fetchClassPrivilegesForGroup(role.getId())) //
					.filter(privileged()) //
					.transform(classPrivilege());
		}
		return elements;
	}

	private Predicate<GrantInfo> privileged() {
		return new Predicate<GrantInfo>() {

			@Override
			public boolean apply(final GrantInfo input) {
				final SerializablePrivilege privileged = input.getPrivilegedObject();
				final Classe privilegedClass = Classe.class.cast(privileged);
				return dataAccessLogic.hasClass(privilegedClass.getId());
			}

		};
	}

	private Function<GrantInfo, ClassPrivilege> classPrivilege() {
		return new Function<GrantInfo, ClassPrivilege>() {

			@Override
			public ClassPrivilege apply(final GrantInfo input) {
				final SerializablePrivilege privilege = input.getPrivilegedObject();
				return newClassPrivilege() //
						.withId(privilege.getName()) //
						.withName(privilege.getName()) //
						.withDescription(privilege.getDescription()) //
						.withMode(input.getMode().getValue()) //
						.build();
			}

		};
	}

	private Function<Classe, ClassPrivilege> classPrivilegeWithWriteMode() {
		return new Function<Classe, ClassPrivilege>() {

			@Override
			public ClassPrivilege apply(final Classe input) {
				return newClassPrivilege() //
						.withId(input.getName()) //
						.withName(input.getName()) //
						.withDescription(input.getDescription()) //
						.withMode(GrantMode.WRITE.getValue()) //
						.build();
			}

		};
	}

}
