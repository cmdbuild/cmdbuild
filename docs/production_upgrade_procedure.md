# Production release procedure

This is the recommended procedure for upgrading a production instance of cmdbuild.

1. enable wfy mode (block user access)

`cmdbuild restws setconfig org.cmdbuild.core.wfy.enabled true`

2. export configuration (required for db reset)

```
cd $tomcat
cmdbuild restws -wfy exportconfigs | tee conf/cmdbuild/old.conf
```

3. shut down tomcat

```
cd $tomcat
bin/shutdown.sh -force
```

4. back up webapp, replace webapp files

```
cd $tomcat/webapps
tar -czvf cmdbuild_2018_12_21.tar.gz cmdbuild
rm -r cmdbuild
mkdir cmdbuild
unzip $file_war -d ./cmdbuild/
```

5. recreate db (if db reset is required)

`cmdbuild dbconfig recreate ready2use.dump.xz`

6. start tomcat (will auto load saved config from old.conf)

```
cd $tomcat
bin/startup.sh
```

7. check that system started ok; if necessary, apply patches
```
cmdbuild -wfy restws status
cmdbuild -wfy restws applyPatches
```
8. check that system is ok (use wfy token - set cookie `CMDbuild-WFYpasstoken: cmdbuildwfy` in browser)
9. disable wfy mode

`cmdbuild restws -wfy setconfig org.cmdbuild.core.wfy.enabled false`



