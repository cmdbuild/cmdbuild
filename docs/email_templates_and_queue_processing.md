## Email 

This document describe email processing.

### Email Template processing

Email records may or may not have an email template associated. Also email template processing is controlled by there email attributes:

* `KeepSynchronization`
* `PromptSynchronization`

When a template is selected, it triggers in response of the following events:

* when saving the email or the card, if `KeepSynchronization` is set to `true`;

when triggered server side, template processing will fill email fields with data from template (interpolated with card data);

The template is also triggered in response to certain ui events; in these cases, the ui must call the /sync ws endpoint, attaching current ui card data:

* when a template is first selected in email edit;
* when the user switch from card view to email view (if `KeepSynchronization` is set to `true`);

### Email Queue processing

Email queue processing uses these email attributes:

 * EmailStatus;
 * Delay;
 * ErrorCount;
 * Account;

When `EmailStatus` is set to "outgoing", an email record is eligible for processing by the queue service. If `Delay` param is set, then the mail will be processed only after `Delay` milliseconds (using `BeginDate` as last modify date).

When email is selected by queue service for processing:

 * first a valid Account must be selected (based on `Account` param, or default); if no valid Account is selected , email status is set to "error";
 * then email delivery is attempted;
 * if email delivery is succesfull, email status is set to "sent";
 * if email delivery fail, then `ErrorCount` is incremented; an outgoing email with nonzero error count will be retryed after a certain time (a retry delay is calculated with an appropriate algorithm); after a certain amount of tries, email status is set to "error" (note: an email with an high value of error count will still be processed by the queue service: only in case of processing failure the email status will eventualy be set to "error");

After a mail status is set to "sent" or "error", the queue service will make no further attempt to process it. 