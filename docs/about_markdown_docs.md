
# info about markdown

Files that ends with `.md` are markdown files. A nice offline editor for these files is ReText (or you can use any text editor you like).

Markdown files will be also rendered by gitlab/github, so make sure that the md syntax you're using is handled correctly (there are many markdown flavors out there).


