
# processes ws

process model is quite similar to class model

```
{
 "success": true,
 "data": {
  "_id": "RequestForChange",
  "name": "RequestForChange",
  "description": "Request for change",
  "_description_translation": "Request for change",
  "prototype": false,
  "parent": "Activity",
  "active": true,
  "type": "standard",
  "system": false,
  "defaultFilter": null,
  "description_attribute_name": "Description",
  "metadata": {},
  "_icon": null,
  "attachmentTypeLookup": null,
  "attachmentDescriptionMode": "mandatory",
  "noteInline": false,
  "noteInlineClosed": false,
  "validationRule": null,
  "defaultOrder": [],
  "flowStatusAttr": null,                    \\ attribute that contains flow-specific status
  "messageAttr": null,                       \\ attribute that contains a flowmessage (shown in user task)
  "enableSaveButton": true,
  "stoppableByUser": false,                  \\ true if the process may be stopped by user
  "engine": "river"                          \\ flow engine (one of river/shark). Should be non-null
 }
}
```