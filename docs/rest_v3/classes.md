
# classes ws

class ws model (read) is like this:

```
{
 "success": true,
 "data": {
  "_id": "Building",                          // class id
  "name": "Building",                         // class name (usually the same as _id)
  "description": "Building",                  // description
  "_description_translation": "Building",
  "prototype": false,                         // true if class is superclass, false otherwise
  "parent": "Class",                          // class parent (only for standard classes)
  "active": true,                             // active true/false
  "type": "standard",                         // type: standard/simple
  "system": false,                            // true if system (reserved) class
  "defaultFilter": null,
  "description_attribute_name": "Description",
  "metadata": {},
  "_icon": null,
  "attachmentTypeLookup": null,
  "attachmentDescriptionMode": "mandatory",
  "noteInline": false,
  "noteInlineClosed": false,
  "validationRule": null,
  "hasBimLayer": true,                        // true if bim is enabled, and this class has bim layer.
                                              // false or undefined otherwise
  "defaultOrder": [],
  "widgets": [],
  "formTriggers": [],
  "contextMenuItems": []
 }
}
```

class model (write) is slightly different
