### INTRO

A *permission* is a property of an object, such as a file. It says which agents are permitted to use the object, and what they are permitted to do (read it, modify it, etc.).

A *privilege* is a property of an agent, such as a user. It lets the agent do things that are not ordinarily allowed. For example, there are privileges which allow an agent to access an object that it does not have permission to access, and privileges which allow an agent to perform maintenance functions such as restart the computer.

So a card require *WRITE permission* to be modified, and an user may have *WRITE privilege* over a card.

### MULTITENANT

All STANDARD classes have an IdTenant attribute (column), that may be null.
All users, once logged in, have either:

* an *ignore_tenat* flag (super admin);
* a list of zero or more active tenant ids (the user may activate or deactivate access to its tenant, if he want to see only a subset of them);

Tenant access is configured PER CARD (ie per table row); different rows may have different tenant access.

If a card has a NULL IdTenant, it is publicly accessible.  
If a card has a IdTenant value set, it will be visibile only by users with active tenant access on that tenantId, or by superUser with ignoreTenant flag set.

Exception: if a user can see a card, it will be able to see the description (only the description, and of course Id) of all cards referenced by this card, no matter the tenant they belong to.




### CLASS PERMISSION

Each class (or entry type in general) attribute can have different access level in different context:

* CORE: dao/service level;
* SERVICE: workflow executors, soap ws, rest ws;
* UI: user interface;

In each context, these permissions can be considered:

 * WRITE: full access to attr value is granted (imply CREATE, UPDATE, READ and LIST);
 * CREATE: attr value can be inserted at creation, but not modified (imply LIST);
 * UPDATE: attr value can be updated (imply LIST);
 * CLONE: object can be duplicated (imply LIST);
 * READ: read only access to attr value is granted (imply LIST);
 * NONE: no access (not used as a keyword, instead empty permission list = NONE);
 * MODIFY: this is meninguful only when altering data model: a MODIFY attribute can be seen and modified by admin users (imply LIST);
 * LIST: as above, but only read access is permitted;

Different normalized forms are possible:

* mixed: any permission keyword can be used (this is the form user input usually comes in);
* normalized: only atomic keywords are used (so all excluded WRITE);
* expanded: all keywords are expanded;

Permissions should always be normalized for storage. Permissions are usually expanded for processing and transmission.

Moving from CORE to SERVICE to UI permissions can only be reduced, never extended; this means than an attribute that is READ on CORE, cannot become WRITE for UI (but can become NONE). Normalization should also take care of this.  
Note that NONE is a dummy mode, no permissions means NONE. An attribute may have more than one permission (for example READ and MODIFY).

Object permissions can be set with a coarse-grained **MODE** parameter, and with more fine-grained **permission** parameters. Standard MODEs are predefined sets of permissions, each item has exactly one MODE that sets its default permissions.

Standard class modes are (check with *DaoPermissionUtils.java*):

|MODE      |CORE |SERVICE|  UI |
|----------|-----|-------|-----|
|ALL       | WM  |  A    |   A |
|DEFAULT   | WM  |  A    |   A |
|WRITE     | WM  |  A    |   A |
|PROTECTED | WL  |  A    |   A |
|RESERVED  | WL  |       |     |
|NONE      |     |       |     |


### ATTRIBUTE PERMISSION

Standard attribute modes are (check with *DaoPermissionUtils.java*):

|MODE      |CORE |SERVICE|  UI |
|----------|-----|-------|-----|
|ALL       |  A  |  A    |   A |
|WRITE     | WM  |  A    |   A |
|READ      | WM  |  A    |  RM |
|HIDDEN    | WM  |  A    |   M |
|IMMUTABLE | CRM |  A    |   A |
|SYSREAD   | WL  |  RL   |   A |
|RESERVED  | RL  |       |     |
|RESCORE   | WL  |       |     |
|CUSTOM    |     |       |     |
|NONE      |     |       |     |


Exception: disabled attributes (STATUS: noactive) receive a specific permission mask, which is (normalized):

 * CORE: -WCUR, SERVICE: -WCUR, UI: -WCUR (service/ui mask propagates from core)

Inherited attributes can have any permissions, but cannot be deleted (only deactivated).

When interrogated, services return standard attribute MODE and only ui-related permissions (currently).

Fine-granined custom permissions can be set as extended attributes metadata, for example:

* cm_permissions: 'WM|WM|N';

When processing attribute metadata, first default MODE is applied (and normalized), then single permissions are applied as overlay (and eventually normalized, expecially when propagating from inner to outer layer).

### DOMAIN AND RELATIONS

User should see card relations (read access) if it can see at least one subclass of domain source and one subclass of domain target.


### GRANT PRIVILEGES 

Merge of grant privileges follows 2 different conventions:

* class,report,view,... privileges = MIN( databasePermissions , rolePrivileges DEFAULT 'N' ) );
* attribute privileges = MIN ( databasePermissions , rolePrivileges DEFAULT 'W' ) );

so attribute privileges are W by default, and can be decreased, where privileges of all other objects are N by default, and can be increased. Either way granted privilege may never exeed database (actually, 'service' level) permissions.

NOTE: grant attribute privileges apply at the SERVICE level (this is different from most attr permission modes, where access is restricted at UI level).


