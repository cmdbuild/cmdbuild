/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.client.rest;

import org.cmdbuild.client.rest.core.InnerRestClient;
import java.io.Closeable;
import static java.lang.Math.round;
import static java.lang.String.format;
import org.apache.commons.io.FileUtils;
import org.cmdbuild.client.rest.api.AttachmentApi;
import org.cmdbuild.client.rest.api.AuditApi;
import org.cmdbuild.client.rest.api.CardApi;
import org.cmdbuild.client.rest.api.LoginApi;
import org.cmdbuild.client.rest.api.LookupApi;
import org.cmdbuild.client.rest.api.SessionApi;
import org.cmdbuild.client.rest.api.SystemApi;
import org.cmdbuild.client.rest.api.WokflowApi;
import org.cmdbuild.client.rest.api.ClassApi;
import org.cmdbuild.client.rest.api.CustomPageApi;
import org.cmdbuild.client.rest.api.MenuApi;
import org.cmdbuild.client.rest.api.ReportApi;
import org.cmdbuild.client.rest.api.UploadApi;
import static org.cmdbuild.utils.date.DateUtils.toUserDuration;
import static org.cmdbuild.utils.date.DateUtils.toUserDuration;

public interface RestClient extends Closeable {

	LoginApi login();

	WokflowApi workflow();

	SessionApi session();

	CardApi card();

	ClassApi classe();

	AttachmentApi attachment();

	SystemApi system();

	AuditApi audit();

	InnerRestClient inner();

	MenuApi menu();

	LookupApi lookup();

	UploadApi uploads();

	ReportApi report();

	CustomPageApi customPage();

	default RestClient doLogin(String username, String password) {
		return login().doLogin(username, password);
	}

	default RestClient doLoginWithAnyGroup(String username, String password) {
		return login().doLoginWithAnyGroup(username, password);
	}

	default RestClient withSessionToken(String sessionToken) {
		inner().setSessionToken(sessionToken);
		return this;
	}

	default RestClient withActionId(String actionId) {
		inner().setActionId(actionId);
		return this;
	}

	default RestClient withHeader(String key, String value) {
		inner().setHeader(key, value);
		return this;
	}

	default RestClient withUploadProgressListener(UploadProgressListener listener) {
		inner().addUploadProgressListener(listener);
		return this;
	}

	/**
	 *
	 * @return session token (never null)
	 * @throws RuntimeException if session token is not available
	 */
	default String getSessionToken() {
		return inner().getSessionToken();
	}

	interface UploadProgressListener {

		void handleUploadProgressEvent(UploadProgressEvent event);
	}

	interface UploadProgressEvent {

		String getUploadId();

		String getUploadDescription();

		/**
		 * @return progress, between 0.0d and 1.0d
		 */
		double getProgress();

		int getCount();

		int getTotal();

		long getElapsedTime();

		default String getProgressDescription() {
			return format("%s%%", round(getProgress() * 1000) / 10d);
		}

		default String getProgressDescriptionLong() {
			long eta = getElapsedTime() * getTotal() / getCount();
			return format("%s  %s / %s (eta %s)", getProgressDescription(), FileUtils.byteCountToDisplaySize(getCount()), FileUtils.byteCountToDisplaySize(getTotal()), toUserDuration(eta));
		}
	}

}
