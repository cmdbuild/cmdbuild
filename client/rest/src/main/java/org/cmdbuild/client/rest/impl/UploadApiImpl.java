/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.client.rest.impl;

import org.cmdbuild.client.rest.core.RestWsClient;
import org.cmdbuild.client.rest.core.AbstractServiceClientImpl;
import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.base.Splitter;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import static java.lang.String.format;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.cmdbuild.client.rest.api.UploadApi;

public class UploadApiImpl extends AbstractServiceClientImpl implements UploadApi {

	public UploadApiImpl(RestWsClient restClient) {
		super(restClient);
	}

	@Override
	public UploadApi upload(String path, byte[] data) {
		List<String> paths = Splitter.on("/").splitToList(path);
		checkArgument(paths.size() == 3);
		HttpEntity multipart = MultipartEntityBuilder.create()
				.addBinaryBody("file", listenUpload("file upload", new ByteArrayInputStream(data)), ContentType.APPLICATION_OCTET_STREAM, paths.get(2))
				.build();
		JsonElement response = post(format("filestores/%s/folders/%s/files/", encodeUrlPath(paths.get(0)), encodeUrlPath(paths.get(1))), multipart).asJson();
		return this;
	}

	@Override
	public UploadData download(String path) {
		List<String> paths = Splitter.on("/").splitToList(path);
		checkArgument(paths.size() == 3);
		byte[] data = getBytes(format("filestores/%s/folders/%s/files/%s/download",
				encodeUrlPath(paths.get(0)),
				encodeUrlPath(paths.get(1)),
				encodeUrlPath(paths.get(2))));
		return () -> data;
	}

}
