/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.client.rest.impl;

import com.fasterxml.jackson.databind.JsonNode;
import static com.google.common.base.Preconditions.checkArgument;
import org.cmdbuild.client.rest.core.RestWsClient;
import org.cmdbuild.client.rest.core.AbstractServiceClientImpl;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.client.rest.RestClient;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.client.rest.api.LoginApi;
import org.cmdbuild.client.rest.model.Session;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.isNullOrEmpty;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class LoginApiImpl extends AbstractServiceClientImpl implements LoginApi {

	public LoginApiImpl(RestWsClient restClient) {
		super(restClient);
	}

	@Override
	public RestClient doLogin(String username, String password) {
		return doLogin(username, password, false);
	}

	@Override
	public RestClient doLoginWithAnyGroup(String username, String password) {
		return doLogin(username, password, true);
	}

	private RestClient doLogin(String username, String password, boolean ensureGroup) {
		logger.debug("doLogin");
		JsonNode response = post("sessions?scope=service", map()
				.with("username", checkNotNull(username, "username cannot be null"))
				.with("password", checkNotNull(password, "password cannot be null"))).asJackson().get("data");
		Session session = fromJson(response, Session.class);
		String sessionToken = checkNotNull(session.getSessionId(), "cannot find session token in response");
		RestClient restClient = restClient().withSessionToken(sessionToken);
		if (ensureGroup && isBlank(session.getRole())) {
			checkArgument(!isNullOrEmpty(session.getAvailableRoles()), "no roles available for user %s", session.getUsername());
			String candidateRole;
			if (session.getAvailableRoles().contains("SuperUser")) {
				candidateRole = "SuperUser";
			} else {
				candidateRole = session.getAvailableRoles().iterator().next();
			}
			session = Session.copyOf(session).withRole(candidateRole).build();
			session = restClient.session().updateSession(session).getSession();
			checkNotBlank(session.getRole());
		}
		return restClient;
	}

	@Override
	protected boolean isSessionTokenRequired() {
		return false;
	}

}
