/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.client.rest.impl;

import org.cmdbuild.client.rest.core.RestWsClient;
import org.cmdbuild.client.rest.core.AbstractServiceClientImpl;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.gson.JsonElement;
import java.util.Map;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.client.rest.api.SessionApi;
import org.cmdbuild.client.rest.model.Session;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;

public class SessionApiImpl extends AbstractServiceClientImpl implements SessionApi {

	public SessionApiImpl(RestWsClient restClient) {
		super(restClient);
	}

	@Override
	public SessionInfo getSessionInfo() {
		JsonElement response = get("sessions/current").asJson();
		String sessionToken = checkNotNull(trimToNull(response.getAsJsonObject().getAsJsonObject("data").getAsJsonPrimitive("_id").getAsString()));
		return new SimpleSessionInfo(sessionToken);
	}

	@Override
	public Map<String, String> getPreferences() {
		logger.debug("getPreferences");
		Map<String, String> map = map();
		get("sessions/current/preferences").asJson().getAsJsonObject().getAsJsonObject("data").entrySet().forEach((entry) -> {
			map.put(entry.getKey(), toString(entry.getValue()));
		});
		return map;
	}

	@Override
	public Map<String, String> getSystemConfig() {
		logger.debug("getSystemConfig");
		Map<String, String> map = map();
		get("configuration").asJson().getAsJsonObject().getAsJsonObject("data").entrySet().forEach((entry) -> {
			map.put(entry.getKey(), toString(entry.getValue()));
		});
		return map;
	}

	@Override
	public SessionApiWithSession updateSession(Session session) {
		Session response = fromJson(put("sessions/current", session).asJackson().get("data"), Session.class);
		return new SessionApiWithSession() {
			@Override
			public SessionApi then() {
				return SessionApiImpl.this;
			}

			@Override
			public Session getSession() {
				return response;
			}
		};
	}

	private static class SimpleSessionInfo implements SessionInfo {

		private final String sessionToken;

		public SimpleSessionInfo(String sessionToken) {
			this.sessionToken = checkNotNull(sessionToken);
		}

		@Override
		public String getSessionToken() {
			return sessionToken;
		}

		@Override
		public String toString() {
			return "SimpleSessionInfo{" + "sessionToken=" + sessionToken + '}';
		}

	}

}
