/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.client.rest.api;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import org.cmdbuild.client.rest.core.RestServiceClient;
import org.cmdbuild.config.api.ConfigDefinition;
import org.cmdbuild.startup.SystemStatus;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;
import org.cmdbuild.debuginfo.BugReportInfo;

public interface SystemApi extends RestServiceClient {

	SystemStatus getStatus();

	List<LoggerInfo> getLoggers();

	List<PatchInfo> getPatches();

	void upgradeWebapp(InputStream warFileData);

	void applyPatches();

	void importFromDms();

	void setLogger(LoggerInfo logger);

	default void setLogger(String category, String level) {
		setLogger(new SimpleLoggerInfo(category, level));
	}

	void deleteLogger(String category);

	void reloadConfig();

	void dropAllCaches();

	void dropCache(String cacheId);

	byte[] dumpDatabase();

	byte[] downloadDebugInfo();

	default BugReportInfo sendBugReport() {
		return sendBugReport(null);
	}

	BugReportInfo sendBugReport(@Nullable String message);

	interface LoggerInfo {

		String getCategory();

		String getLevel();
	}

	void eval(String script);//TODO return data

	interface PatchInfo {

		String getCategory();

		String getName();

		String getDescription();
	}

	Map<String, String> getConfig();

	Map<String, ConfigDefinition> getConfigDefinitions();

	String getConfig(String key);

	SystemApi setConfig(String key, String value);

	SystemApi setConfigs(Map<String, String> data);

	default SystemApi setConfigs(Object... items) {
		return setConfigs(map(items));
	}

	SystemApi deleteConfig(String key);

	static class SimpleLoggerInfo implements LoggerInfo {

		private final String category, level;

		public SimpleLoggerInfo(String category, String level) {
			this.category = trimAndCheckNotBlank(category);
			this.level = trimAndCheckNotBlank(level);
		}

		@Override
		public String getCategory() {
			return category;
		}

		@Override
		public String getLevel() {
			return level;
		}

		@Override
		public String toString() {
			return "SimpleLogger{" + "category=" + category + ", level=" + level + '}';
		}

	}

	static class SimplePatchInfo implements PatchInfo {

		private final String name, description, category;

		public SimplePatchInfo(String name, String description, String category) {
			this.name = trimAndCheckNotBlank(name);
			this.description = checkNotNull(description);
			this.category = trimAndCheckNotBlank(category);
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public String getCategory() {
			return category;
		}

		@Override
		public String toString() {
			return "SimplePatchInfo{" + "name=" + name + ", description=" + description + ", category=" + category + '}';
		}

	}
}
