/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.client.rest.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.client.rest.core.RestWsClient;
import org.cmdbuild.client.rest.core.AbstractServiceClientImpl;
import static com.google.common.collect.Streams.stream;
import com.google.common.net.UrlEscapers;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.InputStream;
import static java.lang.String.format;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import static org.apache.http.entity.ContentType.APPLICATION_FORM_URLENCODED;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.cmdbuild.config.api.ConfigDefinition;
import org.cmdbuild.config.api.ConfigDefinitionImpl;
import org.cmdbuild.startup.SystemStatus;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;
import org.cmdbuild.client.rest.api.SystemApi;
import org.cmdbuild.debuginfo.DebugInfoImpl;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.debuginfo.BugReportInfo;

public class SystemApiImpl extends AbstractServiceClientImpl implements SystemApi {

	public SystemApiImpl(RestWsClient restClient) {
		super(restClient);
	}

	private Boolean requireSession;

	@Override
	protected boolean isSessionTokenRequired() {
		return firstNonNull(requireSession, true);
	}

	@Override
	public SystemStatus getStatus() {
		try {
			requireSession = false; //this is not great. TODO: refactor this with aspectj or something
			logger.debug("getStatus");
			JsonElement response = get("boot/status").asJson();
			String systemStatusValue = trimAndCheckNotBlank(response.getAsJsonObject().get("status").getAsString(), "cannot find status code in response");
			return SystemStatus.valueOf(systemStatusValue);
		} finally {
			requireSession = null;
		}
	}

	@Override
	public List<LoggerInfo> getLoggers() {
		logger.debug("getLoggers");
		return stream(get("system/loggers").asJson().getAsJsonObject().getAsJsonArray("data"))
				.map(JsonElement::getAsJsonObject)
				.map((record) -> new SimpleLoggerInfo(record.getAsJsonPrimitive("category").getAsString(), record.getAsJsonPrimitive("level").getAsString()))
				.collect(toList());
	}

	@Override
	public void setLogger(LoggerInfo loggerToUpdate) {
		logger.debug("updateLogger = {}", loggerToUpdate);
		post("system/loggers/" + trimAndCheckNotBlank(loggerToUpdate.getCategory()), trimAndCheckNotBlank(loggerToUpdate.getLevel()));
	}

	@Override
	public void deleteLogger(String category) {
		logger.debug("deleteLogger = {}", category);
		delete("system/loggers/" + trimAndCheckNotBlank(category));
	}

	@Override
	public Map<String, String> getConfig() {
		logger.debug("getConfig");
		Map<String, String> map = map();
		get("system/config").asJson().getAsJsonObject().getAsJsonObject("data").entrySet().forEach((entry) -> {
			map.put(entry.getKey(), toString(entry.getValue()));
		});
		return map;
	}

	@Override
	public Map<String, ConfigDefinition> getConfigDefinitions() {
		logger.debug("getConfigDefinitions");
		Map<String, ConfigDefinition> map = map();
		get("system/config?detailed=true").asJson().getAsJsonObject().getAsJsonObject("data").entrySet().forEach((entry) -> {
			JsonObject definition = entry.getValue().getAsJsonObject();
			if (definition.getAsJsonPrimitive("hasDefinition").getAsBoolean()) {
				map.put(entry.getKey(), ConfigDefinitionImpl.builder()
						.withKey(entry.getKey())
						.withDescription(toString(definition.get("description")))
						.withDefaultValue(toString(definition.get("default")))
						.build());
			}
		});
		return map;
	}

	@Override
	public void upgradeWebapp(InputStream warFileData) {
		checkNotNull(warFileData, "data param cannot be null");
		HttpEntity multipart = MultipartEntityBuilder.create().addBinaryBody("file", listenUpload("war file upload", warFileData), ContentType.APPLICATION_OCTET_STREAM, "file.war").build();
		post("system/upgrade", multipart);
	}

	@Override
	public String getConfig(String key) {
		logger.debug("getConfig for key = {}", key);
		return toString(get("system/config/" + trimAndCheckNotBlank(key)).asJson().getAsJsonObject().getAsJsonPrimitive("data"));
	}

	@Override
	public void eval(String script) {
		post("system/eval", new StringEntity(format("script=%s", UrlEscapers.urlFormParameterEscaper().escape(script)), APPLICATION_FORM_URLENCODED));
		//TODO parse response, return something
	}

	@Override
	public SystemApi setConfig(String key, String value) {
		logger.debug("setConfig for key = {} to value = {}", key, value);
		put("system/config/" + trimAndCheckNotBlank(key), value);
		return this;
	}

	@Override
	public SystemApi setConfigs(Map<String, String> data) {
		logger.debug("setConfig with data = {}", data);
		put("system/config/_MANY", data);
		return this;
	}

	@Override
	public SystemApi deleteConfig(String key) {
		logger.debug("deleteConfig for key = {}", key);
		delete("system/config/" + trimAndCheckNotBlank(key));
		return this;
	}

	@Override
	public List<PatchInfo> getPatches() {
		try {
			requireSession = false; //this is not great. TODO: refactor this with aspectj or something
			logger.debug("get patches");
			return stream(get("boot/patches").asJson().getAsJsonObject().getAsJsonArray("data")).map(JsonElement::getAsJsonObject).map((patch) -> {
				return new SimplePatchInfo(toString(patch.get("name")), toString(patch.get("description")), toString(patch.get("category")));
			}).collect(toList());
		} finally {
			requireSession = null;
		}
	}

	@Override
	public void applyPatches() {
		try {
			requireSession = false; //this is not great. TODO: refactor this with aspectj or something
			logger.debug("apply patches");
			post("boot/patches/apply", "");
		} finally {
			requireSession = null;
		}
	}

	@Override
	public void importFromDms() {
		logger.debug("importFromDms");
		post("system/dms/import", "");
	}

	@Override
	public void reloadConfig() {
		logger.debug("reloadConfig");
		post("system/config/reload", "");
	}

	@Override
	public void dropAllCaches() {
		logger.debug("drop all caches");
		post("system/cache/drop", "");
	}

	@Override
	public void dropCache(String cacheId) {
		checkNotBlank(cacheId);
		logger.debug("drop cache = %s", cacheId);
		post(format("system/cache/%s/drop", encodeUrlPath(cacheId)), "");
	}

	@Override
	public byte[] dumpDatabase() {
		return getBytes("system/database/dump");
	}

	@Override
	public byte[] downloadDebugInfo() {
		return getBytes("system/debuginfo/download");
	}

	@Override
	public BugReportInfo sendBugReport(@Nullable String message) {
		String url = "system/debuginfo/send";
		if (isNotBlank(message)) {
			url += "?message=" + encodeUrlQuery(message);
		}
		JsonObject data = post(url, "").asJson().getAsJsonObject().getAsJsonObject("data");
		return new DebugInfoImpl(toString(data.get("fileName")));
	}

}
