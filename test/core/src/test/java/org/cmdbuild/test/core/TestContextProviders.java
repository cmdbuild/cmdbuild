/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.core;

import org.cmdbuild.test.core.utils.CoreTestContextInitializer;
import org.cmdbuild.test.dao.utils.ContextProvider;
import org.cmdbuild.test.dao.utils.TestContext;

@ContextProvider
public class TestContextProviders {

	public static TestContext coreTestContext() {
		CoreTestContextInitializer databaseInitializer = new CoreTestContextInitializer();
		databaseInitializer.init();
		return new TestContext(databaseInitializer, (c) -> databaseInitializer.cleanup());
	}

//	public static TestContext r2u() {
//		DaoTestContextInitializer databaseInitializer = new DaoTestContextInitializer(DaoTestContextConfigurationR2u.class);
//		databaseInitializer.init();
//		return new TestContext(databaseInitializer, (c) -> databaseInitializer.cleanup());
//	}
}
