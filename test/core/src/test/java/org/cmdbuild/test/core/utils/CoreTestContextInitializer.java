/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.core.utils;

import com.google.common.eventbus.Subscribe;
import java.io.File;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.cmdbuild.common.logging.LogbackConfigurationStore;
import org.cmdbuild.config.api.DirectoryService;
import static org.cmdbuild.config.api.DirectoryService.DeploymentMode.DUMMY;
import static org.cmdbuild.config.leveltwo.SqlConfigurationImpl.SQL_CONFIGURATION;
import org.cmdbuild.dao.config.DatabaseConfiguration;
import org.cmdbuild.dao.config.SqlConfiguration;
import org.cmdbuild.dao.config.inner.PatchFileRepository;
import org.cmdbuild.spring.utils.SpringContextInitializer;
import org.cmdbuild.startup.SystemEvents;
import org.cmdbuild.system.SystemEventService;
import org.cmdbuild.test.dao.utils.DaoTestContextConfiguration;
import org.cmdbuild.test.dao.utils.DaoTestContextInitializer;
import org.cmdbuild.utils.io.CmdbuildIoUtils;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

/**
 *
 */
public class CoreTestContextInitializer extends SpringContextInitializer {

	@Override
	protected void doInit() throws Exception {
		addConfig(DummyConfigDirectoryService.class);
		addConfig(DummyLogbackConfigurationStore.class);
		addConfig(CoreTestContextConfiguration.class);
		addConfig(ContextWaiterService.class);
		logger.info("start core spring context");
		super.doInit();
		logger.info("wait for system ready");
		getBean(ContextWaiterService.class).waitForSystemReady();
		logger.info("system is ready!");
	}

	@Override
	protected void doCleanup() {
		CoreTestContextConfiguration config = getBean(CoreTestContextConfiguration.class);
		super.doCleanup();
		config.cleanupDatabase();
	}

	@ImportResource({"classpath:test-core-application-context.xml"})
	public static class CoreTestContextConfiguration {

		private final DaoTestContextInitializer testDatabaseInitializer;

		public CoreTestContextConfiguration() {
			this.testDatabaseInitializer = new DaoTestContextInitializer(DaoTestContextConfiguration.class);
			try {
				testDatabaseInitializer.init();
			} catch (Exception ex) {
				throw runtime(ex);
			}
		}

		public void cleanupDatabase() {
			testDatabaseInitializer.cleanup();
		}

		@Bean
		public List<PatchFileRepository> getRepositories() {
			return emptyList();
		}

		@Bean("databaseConfiguration")
		public DatabaseConfiguration getDatabaseConfiguration() {
			return testDatabaseInitializer.getBean(DatabaseConfiguration.class);
		}

		@Bean(SQL_CONFIGURATION)
		public SqlConfiguration getSqlConfiguration() {
			return testDatabaseInitializer.getBean(SqlConfiguration.class);
		}

	}

	public static class ContextWaiterService {

		private final AtomicReference<SystemEvents.SystemReadyEvent> eventHolder = new AtomicReference<>();

		public ContextWaiterService(SystemEventService systemEventService) {
			systemEventService.getEventBus().register(new Object() {
				@Subscribe
				public void handleEvent(SystemEvents.SystemReadyEvent event) {
					synchronized (eventHolder) {
						eventHolder.set(event);
						eventHolder.notifyAll();
					}
				}
			});
		}

		public void waitForSystemReady() throws InterruptedException {
			synchronized (eventHolder) {
				while (eventHolder.get() == null) {
					eventHolder.wait();
				}
			}
		}

	}

	public static class DummyConfigDirectoryService implements DirectoryService {

		public DummyConfigDirectoryService() {
		}

		@Override
		public File getConfigDirectory() {
			throw new UnsupportedOperationException();
		}

		@Override
		public File getWebappRootDirectory() {
			throw new UnsupportedOperationException();
		}

		@Override
		public File getCatalinaBaseDirectory() {
			throw new UnsupportedOperationException();
		}

		@Override
		public DirectoryService.DeploymentMode getDeploymentMode() {
			return DUMMY;
		}

	}

	public static class DummyLogbackConfigurationStore implements LogbackConfigurationStore {

		@Override
		public String getLogbackXmlConfiguration() {
			return CmdbuildIoUtils.readToString(getClass().getResourceAsStream("/logback.xml"));
		}

		@Override
		public void setLogbackXmlConfiguration(String xmlConfiguration) {
			throw new UnsupportedOperationException();
		}

	}
}
