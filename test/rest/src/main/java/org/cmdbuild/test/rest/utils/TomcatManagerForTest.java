/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.rest.utils;

import com.google.common.base.Joiner;
import static com.google.common.base.MoreObjects.firstNonNull;
import com.google.common.collect.ImmutableMap;
import java.io.File;
import java.io.IOException;
import javax.sql.DataSource;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import java.util.List;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.IOUtils;
import static org.apache.commons.lang.StringUtils.trimToNull;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import static org.cmdbuild.dao.config.inner.DatabaseCreator.SHARK_PASSWORD;
import static org.cmdbuild.dao.config.inner.DatabaseCreator.SHARK_USERNAME;
import org.cmdbuild.dao.config.inner.DatabaseCreatorConfig;
import org.cmdbuild.dao.config.inner.DatabaseCreatorConfigImpl;
import org.cmdbuild.test.dao.utils.DaoTestContextConfiguration;
import org.cmdbuild.test.dao.utils.DaoTestContextInitializer;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.readToString;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import org.cmdbuild.utils.tomcatmanager.LogFollow;
import org.cmdbuild.utils.tomcatmanager.TomcatConfig;
import org.cmdbuild.utils.tomcatmanager.TomcatManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 */
public class TomcatManagerForTest {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private TomcatManager tomcatManager;
	private DaoTestContextInitializer testDatabaseInitializer;
	private LogFollow sharkLog;

	public void initTomcatAndDb(String dbType) throws Exception {
		initTomcatAndDb(dbType, false);
	}

	public void initTomcatAndDb(String dbType, boolean includeShark) {
		try {
			doInitTomcatAndDb(dbType, includeShark);
		} catch (Exception ex) {
			throw runtime(ex);
		}
	}

	private void doInitTomcatAndDb(String dbType, boolean includeShark) throws Exception {
		logger.info("\n\n\ninitTomcatAndDb BEGIN\n\n\n");
		testDatabaseInitializer = new DaoTestContextInitializer(asList(RestTestContextConfiguration.class, new RestTestDatabaseTypeConfiguration() {
			@Override
			public String getDatabaseType() {
				return dbType;
			}

			@Override
			public boolean includeSharkDb() {
				return includeShark;
			}
		}));
		testDatabaseInitializer.init();

		DatabaseCreatorConfig dbConfig = testDatabaseInitializer.getBean(DatabaseCreator.class).getConfig();

		String projectVersion = firstNonNull(trimToNull(System.getProperty("project.version")), "3.0-SNAPSHOT");//TODO workaround for null property, fix this

		List<String> wars = list(format("org.cmdbuild:cmdbuild:%s:war", projectVersion));
		if (includeShark) {
			wars.add(format("org.cmdbuild:cmdbuild-shark-server:%s:war AS shark", projectVersion));
		}

		TomcatConfig config = TomcatConfig.builder()
				.withProperties(getClass().getResourceAsStream("/tomcat-manager-cmdbuild-config.properties"))
				.withProperty("tomcat_deploy_artifacts", Joiner.on(",").join(wars))
				.withOverlay("database", ImmutableMap.of(
						"db.url", dbConfig.getDatabaseUrl(),
						"db.username", dbConfig.getCmdbuildUser(),
						"db.password", dbConfig.getCmdbuildPassword()))
				.withOverlay("logback", IOUtils.toString(getClass().getResourceAsStream("/tomcat_logback.xml")))
				.withOverlay("core", ImmutableMap.of("legacy_ui.enabled", Boolean.TRUE.toString()))
				.build();

		if (includeShark) {
			config = TomcatConfig.copyOf(config)
					.withOverlay("sharkdb", buildSharkContextXml(dbConfig.getDatabaseUrl(), SHARK_USERNAME, SHARK_PASSWORD))
					.withOverlay("sharkconf", buildSharkConf(format("http://%s:%s/cmdbuild/", getHostname(), config.getHttpPort())))
					.build();
		}

		tomcatManager = new TomcatManager(config);
		if (includeShark) {
			sharkLog = new LogFollow(new File(tomcatManager.getConfig().getInstallDir(), "logs/shark.log"));
			sharkLog.startFollowingLog();
		}
		tomcatManager.buildAndStart();
		logger.info("\n\n\ninitTomcatAndDb END\n\n\n");
	}

	public void cleanupTomcatAndDb() {
		logger.info("\n\n\ncleanupClass BEGIN\n\n\n");
		tomcatManager.stopAndCleanup();
		tomcatManager = null;
		testDatabaseInitializer.cleanup();
		testDatabaseInitializer = null;
		if (sharkLog != null) {
			sharkLog.stopFollowingLog();
			sharkLog.flushLogs();
			sharkLog = null;
		}
		logger.info("\n\n\ncleanupClass END\n\n\n");
	}

	public TomcatManager getTomcatManager() {
		return tomcatManager;
	}

	public String getHostname() {
		return firstNonNull(trimToNull(System.getProperty("cmdbuild.test.my.hostname")), "localhost");
	}

	public int getPort() {
		return getTomcatManager().getConfig().getHttpPort();
	}

	public String getBaseUrl() {
		return format("http://%s:%s/cmdbuild/", getHostname(), getPort());
	}

	public String getSharkUrl() {
		return format("http://%s:%s/shark", getHostname(), getPort());
	}

	private String buildSharkContextXml(String url, String user, String psw) {
		return readToString(getClass().getResourceAsStream("/shark_context_template.xml"))
				.replaceFirst("DBURL", url)
				.replaceFirst("DBUSERNAME", user)
				.replaceFirst("DBPASSWORD", psw);
	}

	private String buildSharkConf(String cmdbuildUrl) {
		return readToString(getClass().getResourceAsStream("/Shark_conf_template.conf"))
				.replaceFirst("CMDBUILDURL", cmdbuildUrl);

	}

	@ImportResource({"classpath:test-dao-application-context.xml"})
	public static class RestTestContextConfiguration extends DaoTestContextConfiguration {

		@Autowired
		private RestTestDatabaseTypeConfiguration configuration;

		@Override
		public void init() throws IOException, ZipException {
			getDatabaseConfigurator().setConfig(DatabaseCreatorConfigImpl.copyOf(getDatabaseConfigurator().getConfig())
					.withDatabaseType(configuration.getDatabaseType())
					.withUseSharkSchema(configuration.includeSharkDb())
					.build());
			super.init();
		}

	}

	public interface RestTestDatabaseTypeConfiguration {

		String getDatabaseType();

		boolean includeSharkDb();
	}

	public DataSource getDataSource() {
//		if (testDatabaseInitializer == null)
//			externalTomcatTestDBInitializer();
		return testDatabaseInitializer.getBean(DataSource.class);
	}

//	private void externalTomcatTestDBInitializer() throws Exception  {
//		testDatabaseInitializer = new TestDatabaseInitializer(asList(RestTestContextConfiguration.class, new RestTestDatabaseTypeConfiguration() {
//			@Override
//			public String getDatabaseType() {
//				return DatabaseCreator.EXISTING_DATABASE;
//			}
//		}));
//		testDatabaseInitializer.init();
//	}
	private JdbcTemplate defaultJdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		if (defaultJdbcTemplate == null) {
			defaultJdbcTemplate = new JdbcTemplate(getDataSource());
		}
		return defaultJdbcTemplate;
	}

}
