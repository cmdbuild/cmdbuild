package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.util.HashMap;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class MenuRestIT extends AbstractWsIT {

	@Test
	public void getMenues() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("menu/")).then().statusCode(200);
	}

	@Test
	public void getMenuById() {
		String token = getSessionToken();

		long firstId = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("menu")).then().extract()
				.jsonPath().getLong("data[0]._id");

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("menu/" + firstId)).then()
				.body("data.menuType", equalTo("root")).body("data.objectDescription", equalTo("ROOT")).statusCode(200);
	}

	@Test
	public void postDeleteMenu() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createMenu();

		long menuId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
				.body(jsonAsMap).when().post(buildRestV3Url("menu/")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("menu/" + menuId)).then()
				.body("data.group", equalTo("newgroup3")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("menu/" + menuId)).then()
				.statusCode(200);
	}

	@Test
	public void postPutDeleteMenu() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createMenu();

		long menuId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
				.body(jsonAsMap).when().post(buildRestV3Url("menu/")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("menu/" + menuId)).then()
				.body("data.group", equalTo("newgroup3")).statusCode(200);

		jsonAsMap.put("group", "group modificato");

		given().header(CMDBUILD_AUTH_HEADER, token).when().contentType("application/json").body(jsonAsMap)
				.put(buildRestV3Url("menu/" + menuId)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("menu/" + menuId)).then()
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("menu/" + menuId)).then()
				.statusCode(200);
		
	}

	public Map<String, Object> createMenu() {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("group", "newgroup3");

		return jsonAsMap;
	}

}
