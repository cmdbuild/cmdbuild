package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasValue;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class RelationRestIT extends AbstractWsIT {

	@Test
	public void getFromDomains() {
		String domainId = "BuildingFloor";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("domains/" + domainId + "/relations")).then()
				.body("data._type", everyItem(equalTo(domainId))).statusCode(200);
	}

	@Test
	public void getFromCards() {
		String classId = "Employee";
		int cardId = 132;

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/relations")).then()
				.body("data[0]._type", equalTo("AssetAssignee")).body("data[0]._destinationCode", equalTo("MON0004"))
				.statusCode(200);
	}

	@Test
	public void testLimit() {
		String domainId = "BuildingFloor";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).queryParam("limit", 1)
				.get(buildRestV3Url("domains/" + domainId + "/relations")).then().body("data.size()", equalTo(1))
				.statusCode(200);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		String domainId = "BuildingFloor";
		int startValue = 4;

		int totalValue = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("domains/" + domainId + "/relations")).then().extract().jsonPath()
				.getInt("meta.total");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", startValue)
				.get(buildRestV3Url("domains/" + domainId + "/relations")).then()
				.body("data.size()", equalTo(totalValue - startValue)).statusCode(200);
	}

	@Test
	public void getFromDomainsWithRelationId() {
		String domainId = "BuildingFloor";
		int relationId = 89;

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("domains/" + domainId + "/relations/" + relationId)).then()
				.body("data._id", equalTo(relationId)).statusCode(200);
	}

	@Test
	public void testPostDeleteRelation() {
		String token = getSessionToken();
		String domainId = "FloorRoom";
		long floorId = createFloor(token);
		long roomId = createRoom(token);
		Map<String, Object> jsonAsMap = createRelation(floorId, roomId);

		logger.info(jsonAsMap.toString());

		long relationId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap)
				.when().post(buildRestV3Url("domains/" + domainId + "/relations")).then().statusCode(200).extract()
				.path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.get(buildRestV3Url("domains/" + domainId + "/relations/" + relationId)).then()
				.body("data._type", equalTo("FloorRoom")).body("data._sourceId", equalTo(floorId))
				.body("data._destinationId", equalTo(roomId)).body("data._sourceType", equalTo("Floor"))
				.body("data._destinationType", equalTo("Room")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.delete(buildRestV3Url("domains/" + domainId + "/relations/" + relationId)).then().statusCode(200);

		deleteFloor(token, floorId);
		deleteRoom(token, roomId);
	}

	public Map<String, Object> createRelation(long sourceId, long destinationId) {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("_type", "FloorRoom");
		jsonAsMap.put("_sourceId", sourceId);
		jsonAsMap.put("_destinationId", destinationId);
		jsonAsMap.put("_sourceType", "Floor");
		jsonAsMap.put("_destinationType", "Room");

		return jsonAsMap;
	}

	public long createFloor(String token) {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Code", "AB83");
		jsonAsMap.put("Description", "Office Building B - Floor 3");
		jsonAsMap.put("Building", 76);

		long userId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/Floor/cards")).then().statusCode(200).extract().path("data._id");

		return userId;

	}

	public long createRoom(String token) {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Code", "214525");
		jsonAsMap.put("Description", "Office Building B - Floor 3 - Room 01");

		long userId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/Room/cards")).then().statusCode(200).extract().path("data._id");

		return userId;
	}

	public void deleteFloor(String token, long id) {

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/Floor/cards/" + id)).then()
				.statusCode(200);
	}

	public void deleteRoom(String token, long id) {

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/Room/cards/" + id)).then()
				.statusCode(200);
	}

	public JSONObject createJson(String attribute, String operator, Object value) {
		JSONObject json = new JSONObject();
		json.put("attribute", attribute);
		json.put("operator", operator);
		json.put("value", value);

		JSONObject json2 = new JSONObject();
		json2.put("simple", json);

		JSONObject json3 = new JSONObject();
		json3.put("attribute", json2);
		return json3;
	}

}
