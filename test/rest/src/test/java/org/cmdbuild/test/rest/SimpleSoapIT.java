package org.cmdbuild.test.rest;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;
import static org.cmdbuild.services.soap.client.CmdbuildSoapClient.token;
import static org.cmdbuild.services.soap.client.CmdbuildSoapClient.usernameAndPassword;
import static org.cmdbuild.services.soap.client.CmdbuildSoapClient.PasswordType.DIGEST;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cmdbuild.api.fluent.Card;
import org.cmdbuild.api.fluent.CardDescriptor;
import org.cmdbuild.api.fluent.ExecutorBasedFluentApi;
import org.cmdbuild.api.fluent.ExistingCard;
import org.cmdbuild.api.fluent.FluentApi;
import org.cmdbuild.api.fluent.ws.WsFluentApiExecutor;
import org.cmdbuild.services.soap.client.CmdbuildSoapClient;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.common.base.Supplier;

import org.cmdbuild.services.soap.Private;


@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class SimpleSoapIT extends AbstractWsIT {

//	@Parameters
//	public static Collection data() {
//		return Arrays.asList(WsAuthMode.LOGIN, WsAuthMode.TOKEN);//TODO parametrized
//	}
	public enum WsAuthMode {
		TOKEN, LOGIN
	}

//	@Parameter
	public WsAuthMode wsAuthMode = WsAuthMode.LOGIN;//TODO parametrized

	private Private proxy;
	private FluentApi api;

	@Before
	public void createProxyAndApi() throws Exception {
		logger.info("init soap client with auth mode = {}", wsAuthMode);
		String usernameAndGroup = "admin", password = "admin"; //TODO
		switch (wsAuthMode) {
			case LOGIN:
				proxy = CmdbuildSoapClient.<Private>aSoapClient().forClass(Private.class)
						.withUrl(buildSoapWsUrl())
						.withAuthentication(usernameAndPassword(DIGEST, usernameAndGroup, password))
						.build().getProxy();

				break;
			case TOKEN:
				Supplier<String> tokenSupplier = () -> CmdbuildSoapClient.<Private>aSoapClient().forClass(Private.class)
						.withUrl(buildSoapWsUrl())
						.withAuthentication(usernameAndPassword(DIGEST, usernameAndGroup, password))
						.build().getProxy().createSession();
				proxy = CmdbuildSoapClient.<Private>aSoapClient().forClass(Private.class)
						.withUrl(buildSoapWsUrl())
						.withAuthentication(token(tokenSupplier))
						.build().getProxy();
				break;
		}
		checkNotNull(proxy);
		api = new ExecutorBasedFluentApi(new WsFluentApiExecutor(proxy));
	}

	@After
	public void cleanupProxyAndApi() {
		api = null;
		proxy = null;
	}

	protected Private proxy() {
		return proxy;
	}

	protected FluentApi api() {
		return api;
	}

	@Test
	@Ignore
	public void findCards() throws Exception {
		logger.info("findCards BEGIN");
		try {
			String className = "PC";

			List<Card> list = api().queryClass(className).fetch();
			logger.info("res = {}", list);
			assertNotNull(list);
			assertTrue(!list.isEmpty());
		} catch (Exception ex) {
			logger.error("error", ex);
			throw ex;
		} finally {
			flushLogs();
		}
		logger.info("findCards END");
	}

	@Test
	@Ignore
	public void createEmptyCard() throws Exception {
		logger.info("createEmptyCard BEGIN");
		try {
			String className = "PC";

			CardDescriptor cardDescriptor = api().newCard(className).create();
			logger.info("new card created = {}", cardDescriptor);
			assertNotNull(cardDescriptor);
			assertEquals(className, cardDescriptor.getClassName());

			ExistingCard existingCard = api().existingCard(cardDescriptor.getClassName(), cardDescriptor.getId());
			assertNotNull(existingCard);
			assertEquals(className, existingCard.getClassName());
			assertEquals(cardDescriptor.getId(), existingCard.getId());
		} catch (Exception ex) {
			logger.error("error", ex);
			throw ex;
		} finally {
			flushLogs();
		}
		logger.info("createEmptyCard END");
	}

}
