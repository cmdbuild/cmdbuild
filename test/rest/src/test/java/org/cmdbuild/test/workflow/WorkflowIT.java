package org.cmdbuild.test.workflow;

import static com.google.common.base.Objects.equal;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.MoreCollectors.onlyElement;
import java.util.List;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.client.rest.RestClient;
import org.cmdbuild.client.rest.model.SimpleFlowData;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.testutils.CmdbTestUtils.waitFor;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.cmdbuild.client.rest.api.WokflowApi;
import org.cmdbuild.client.rest.api.WokflowApi.PlanVersionInfo;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.dao.utils.TestContext;
import static org.cmdbuild.test.rest.TestContextProviders.initTomcatAndDb;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import static org.junit.Assert.assertFalse;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
public class WorkflowIT extends AbstractWsIT {

	private final String ASSET_MGT = "AssetMgt",
			INCIDENT_MGT = "IncidentMgt";

	private RestClient restClient;

	public static TestContext r2uWithShark() throws Exception {
		return initTomcatAndDb(DatabaseCreator.DEMO_DATABASE, true);
	}

	@Before
	public void init() {
		restClient = getRestClient();
		restClient.system().setConfigs("org.cmdbuild.workflow.river.enabled", "true",
				"org.cmdbuild.workflow.shark.enabled", "true",
				"org.cmdbuild.workflow.provider", "river",
				"org.cmdbuild.workflow.enabled", "true");
		if (initializedByTomcatManager()) {
			restClient.system().setConfig("org.cmdbuild.workflow.endpoint", getTomcatManagerForTest().getSharkUrl());
		}
		//restClient.workflow().uploadPlanVersionAndMigrateProcess(ASSET_MGT, WorkflowIT.class.getResourceAsStream("/org/cmdbuild/test/workflow/AssetMgt.xpdl"), "river");
	}                                                                                                                                          

	@Test
	public void testWorkflowConfigEnabled() {
		assertEquals("true", restClient.session().getSystemConfig().get("cm_system_workflow_enabled"));
	}

	@Test
	public void testWorkflowConfigDisabled1() {
		restClient.system().setConfig("org.cmdbuild.workflow.enabled", "false");
		assertEquals("false", restClient.session().getSystemConfig().get("cm_system_workflow_enabled"));
	}

	@Test
	public void testWorkflowConfigDisabled2() {
		restClient.system().setConfigs("org.cmdbuild.workflow.river.enabled", "false",
				"org.cmdbuild.workflow.shark.enabled", "false");
		assertEquals("false", restClient.session().getSystemConfig().get("cm_system_workflow_enabled"));
	}

	@Test
	public void testPlanVersionUpdate() {
		PlanVersionInfo versionInfo = restClient.workflow().getPlanVersions(ASSET_MGT).stream().filter(PlanVersionInfo::isDefault).collect(onlyElement());
		logger.info(""+versionInfo);
	//	assertEquals("jufoo9soeyrli4hyto1hn560", versionInfo.getPlanId());
/*
		restClient.workflow().uploadPlanVersion(ASSET_MGT, WorkflowIT.class.getResourceAsStream("/org/cmdbuild/test/workflow/AssetMgt_var.xpdl"));

		versionInfo = restClient.workflow().getPlanVersions(ASSET_MGT).stream().filter(PlanVersionInfo::isDefault).collect(onlyElement());
		assertEquals("10bse5af75owizxw3urkpg4d", versionInfo.getPlanId());

		restClient.workflow().uploadPlanVersion(ASSET_MGT, WorkflowIT.class.getResourceAsStream("/org/cmdbuild/test/workflow/AssetMgt.xpdl"));

		versionInfo = restClient.workflow().getPlanVersions(ASSET_MGT).stream().filter(PlanVersionInfo::isDefault).collect(onlyElement());
		assertEquals("jufoo9soeyrli4hyto1hn560", versionInfo.getPlanId());*/
	}

	@Test
	public void testSharkPlanUpload() {
		restClient.system().setConfigs("org.cmdbuild.workflow.shark.enabled", "true",
				"org.cmdbuild.workflow.provider", "shark");
		restClient.workflow().uploadPlanVersion(ASSET_MGT, WorkflowIT.class.getResourceAsStream("/org/cmdbuild/test/workflow/AssetMgt.xpdl"));
	}

	@Test
	public void testGetPlanVersions() {
		List<WokflowApi.PlanVersionInfo> planVersions = restClient.workflow().getPlanVersions(ASSET_MGT);
		assertFalse(planVersions.isEmpty());
		assertEquals(1, planVersions.stream().filter(PlanVersionInfo::isDefault).count());
		//TODO check all vars
	}

	@Test
	public void testGetPlan() {
		WokflowApi.PlanInfo plan = restClient.workflow().getPlan(ASSET_MGT);
		assertEquals(ASSET_MGT, plan.getId());
		//TODO check all vars
	}

	@Test
	public void testGetTemplate() {
		String xpdlTemplate = restClient.workflow().getXpdlTemplate(ASSET_MGT);
		assertTrue(xpdlTemplate.contains("ExtendedAttribute Name=\"cmdbuildBindToClass\" Value=\"AssetMgt\""));//TODO
	}

	@Test
	public void testProcess() {
		logger.info("\n\n\ntestProcess BEGIN\n\n");

		WokflowApi.FlowDataAndStatus flowInfo = restClient.workflow().start(ASSET_MGT, SimpleFlowData.builder().withAttributes(map("Type", "2676")).build()).getFlowData();
		assertNotNull(flowInfo);
		String flowCardId = flowInfo.getFlowCardId();
		assertTrue(!isBlank(flowCardId));
		logger.info("started process = {}", flowInfo.getFlowCardId());

		List<WokflowApi.TaskInfo> taskList = restClient.workflow().getTaskList(ASSET_MGT, flowCardId);
		assertEquals(1, taskList.size());
		String taskId1 = getOnlyElement(taskList).getId();
		assertNotNull(trimToNull(taskId1));

		restClient.workflow().advance(ASSET_MGT, flowCardId, taskId1, SimpleFlowData.builder().withAttributes(map("GRHeaderAction", "2677", "Supplier", "6034", "Order", "8088")).build());

		waitFor(() -> restClient.workflow().get(ASSET_MGT, flowCardId), (flowData) -> equal("Completed", flowData.getStatus()));

		logger.info("\n\n\ntestProcess END\n\n");
	}

	@Test
	public void testGroovyProcess() {
		logger.info("\n\n\ntestProcess BEGIN\n\n");

		restClient.workflow().uploadPlanVersion(ASSET_MGT, WorkflowIT.class.getResourceAsStream("/org/cmdbuild/test/workflow/AssetMgt_groovy.xpdl"));

		WokflowApi.FlowDataAndStatus flowInfo = restClient.workflow().start(ASSET_MGT, SimpleFlowData.builder().withAttributes(map("Type", "2676")).build()).getFlowData();
		assertNotNull(flowInfo);
		String flowCardId = flowInfo.getFlowCardId();
		assertTrue(!isBlank(flowCardId));
		logger.info("started process = {}", flowInfo.getFlowCardId());

		List<WokflowApi.TaskInfo> taskList = restClient.workflow().getTaskList(ASSET_MGT, flowCardId);
		assertEquals(1, taskList.size());
		String taskId1 = getOnlyElement(taskList).getId();
		assertNotNull(trimToNull(taskId1));

		restClient.workflow().advance(ASSET_MGT, flowCardId, taskId1, SimpleFlowData.builder().withAttributes(map("GRHeaderAction", "2677", "Supplier", "6034", "Order", "8088")).build());

		waitFor(() -> restClient.workflow().get(ASSET_MGT, flowCardId), (flowData) -> equal("Completed", flowData.getStatus()));

		logger.info("\n\n\ntestProcess END\n\n");
	}

	@Test
	public void testIncidentMgtProcess() {
		logger.info("\n\n\ntestIncidentMgtProcess BEGIN\n\n");

		restClient.workflow().uploadPlanVersion(INCIDENT_MGT, WorkflowIT.class.getResourceAsStream("/org/cmdbuild/test/workflow/IncidentMgt.xpdl"));

		WokflowApi.FlowDataAndStatus flowInfo = restClient.workflow().start(INCIDENT_MGT, SimpleFlowData.builder().withAttributes(map(
				"Requester", "6083",
				"ShortDescr", "test_" + randomId(6),
				"OpeningChannel", "3869"
		)).build()).getFlowData();
		assertNotNull(flowInfo);
		String flowCardId = flowInfo.getFlowCardId();
		assertTrue(!isBlank(flowCardId));
		logger.info("started process = {}", flowInfo.getFlowCardId());

		{
			List<WokflowApi.TaskInfo> taskList = waitFor(() -> restClient.workflow().getTaskList(INCIDENT_MGT, flowCardId), (tl) -> !tl.isEmpty());
			assertEquals(1, taskList.size());
			String taskId1 = getOnlyElement(taskList).getId();
			assertNotNull(trimToNull(taskId1));

			restClient.workflow().advance(INCIDENT_MGT, flowCardId, taskId1, SimpleFlowData.builder().withAttributes(map(
					"Category", "6494",
					"Subcategory", "6506",
					"Urgency", "3871",
					"Impact", "3875",
					"SPClassificAction", "3960",
					"SPNextRole", "3897",
					"CurrentUser", "18"
			)).build());
		}

		{
			List<WokflowApi.TaskInfo> taskList = waitFor(() -> restClient.workflow().getTaskList(INCIDENT_MGT, flowCardId), (tl) -> !tl.isEmpty());
			assertEquals(1, taskList.size());
			String taskId1 = getOnlyElement(taskList).getId();
			assertNotNull(trimToNull(taskId1));

			restClient.workflow().advance(INCIDENT_MGT, flowCardId, taskId1, SimpleFlowData.builder().withAttributes(map(
					"Outcome", "3880",
					"Answer", "test_ok",
					"SPClosureAction", "3975",
					"CurrentUser", "18"
			)).build());
		}

		waitFor(() -> restClient.workflow().get(INCIDENT_MGT, flowCardId), (flowData) -> equal("Completed", flowData.getStatus()));

		logger.info("\n\n\ntestIncidentMgtProcess END\n\n");
	}

	@Test
	public void testIncidentMgtProcess2() {
		logger.info("\n\n\ntestIncidentMgtProcess2 BEGIN\n\n");

		restClient.workflow().uploadPlanVersion(INCIDENT_MGT, WorkflowIT.class.getResourceAsStream("/org/cmdbuild/test/workflow/IncidentMgt.xpdl"));

		WokflowApi.FlowDataAndStatus flowInfo = restClient.workflow().start(INCIDENT_MGT, SimpleFlowData.builder().withAttributes(map(
				"Requester", "6083",
				"ShortDescr", "test_" + randomId(6),
				"OpeningChannel", "3869",
				"TTSPClassification", null
		)).build()).getFlowData();
		assertNotNull(flowInfo);
		String flowCardId = flowInfo.getFlowCardId();
		assertTrue(!isBlank(flowCardId));
		logger.info("started process = {}", flowInfo.getFlowCardId());

		{
			List<WokflowApi.TaskInfo> taskList = waitFor(() -> restClient.workflow().getTaskList(INCIDENT_MGT, flowCardId), (tl) -> !tl.isEmpty());
			assertEquals(1, taskList.size());
			String taskId1 = getOnlyElement(taskList).getId();
			assertNotNull(trimToNull(taskId1));

			restClient.workflow().advance(INCIDENT_MGT, flowCardId, taskId1, SimpleFlowData.builder().withAttributes(map(
					"Category", "6494",
					"Subcategory", "6506",
					"Urgency", "3871",
					"Impact", "3874",
					"SPClassificAction", "3960",
					"SPNextRole", "3897",
					"CurrentUser", "18",
					"TTSPClassification", null
			)).build());
		}

		{
			List<WokflowApi.TaskInfo> taskList = waitFor(() -> restClient.workflow().getTaskList(INCIDENT_MGT, flowCardId), (tl) -> !tl.isEmpty());
			assertEquals(1, taskList.size());
			String taskId1 = getOnlyElement(taskList).getId();
			assertNotNull(trimToNull(taskId1));

			restClient.workflow().advance(INCIDENT_MGT, flowCardId, taskId1, SimpleFlowData.builder().withAttributes(map(
					"Outcome", "3880",
					"Answer", "test_ok",
					"SPClosureAction", "3975",
					"CurrentUser", "18",
					"TTSPClassification", null
			)).build());
		}

		waitFor(() -> restClient.workflow().get(INCIDENT_MGT, flowCardId), (flowData) -> equal("Completed", flowData.getStatus()));

		logger.info("\n\n\ntestIncidentMgtProcess2 END\n\n");
	}

}
