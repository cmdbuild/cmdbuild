package org.cmdbuild.test.rest;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.trimToNull;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.apache.http.entity.ContentType.TEXT_PLAIN;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gson.JsonParser;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class SimpleRestIT extends AbstractWsIT {

	@Test
	public void testAppJsServiceUrlRewrite() throws IOException {
		logger.info("testAppJsServiceUrlRewrite BEGIN");
		try {
			int port = URI.create(getBaseUrl()).toURL().getPort();
			HttpGet request = new HttpGet("http://localhost:" + port + "/cmdbuild/ui/cmdbuild/app.js");
			HttpResponse response = getHttpClient().execute(request);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertTrue(responseContent.contains("\"baseUrl\":\"http://localhost:" + port + "/cmdbuild/services/rest/v3\",\"processedByUiFilter\":true"));
		} finally {
			flushLogs();
		}
		logger.info("testAppJsServiceUrlRewrite END");
	}
	
	@Test
	public void testSystemStatusWs() throws IOException {
		logger.info("testSystemStatusWs BEGIN");
		try {
			String sessionId = getSessionToken();
			HttpGet request = new HttpGet(buildRestV3Url("system/status"));
			request.setHeader("CMDBuild-Authorization", sessionId);
			HttpResponse response = getHttpClient().execute(request);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
		} finally {
			flushLogs();
		}
		logger.info("testSystemStatusWs END");
	}

	@Test
	public void testBootStatus() throws IOException {
		logger.info("testBootStatus BEGIN");
		try {
			HttpGet request = new HttpGet(buildRestV3Url("boot/status"));
			HttpResponse response = getHttpClient().execute(request);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertEquals("{\"success\":true,\"status\":\"READY\"}", responseContent);
		} finally {
			flushLogs();
		}
		logger.info("testBootStatus END");
	}

	@Test
	public void testLanguages1() throws IOException {
		try {
			logger.info("testLanguages1 BEGIN");
			HttpGet get = new HttpGet(buildRestV3Url("configuration/languages"));
			HttpResponse response = getHttpClient().execute(get);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertThat(responseContent, containsString("\"code\":\"en\",\"description\":\"English\""));
		} finally {
			flushLogs();
		}
		logger.info("testLanguages1 END");
	}

	@Test
	public void testLanguages2() throws IOException {
		logger.info("testLanguages2 BEGIN");
		try {
			HttpGet get = new HttpGet(buildRestV3Url("configuration/languages/"));
			HttpResponse response = getHttpClient().execute(get);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertThat(responseContent, containsString("\"code\":\"en\",\"description\":\"English\""));
		} finally {
			flushLogs();
		}
		logger.info("testLanguages2 END");
	}

	@Test
	public void testUiHome1() throws IOException {
		logger.info("testUiHome1 BEGIN");
		try {
			HttpGet get = new HttpGet(getBaseUrl() + "/ui");
			HttpResponse response = getHttpClient().execute(get);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertThat(responseContent, containsString("<html"));
			assertThat(responseContent, containsString("var Ext = Ext"));
		} finally {
			flushLogs();
		}
		logger.info("testUiHome1 END");
	}

	@Test
	public void testUiHome2() throws IOException {
		logger.info("testUiHome2 BEGIN");
		try {
			HttpGet get = new HttpGet(getBaseUrl() + "/ui/");
			HttpResponse response = getHttpClient().execute(get);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertThat(responseContent, containsString("<html"));
			assertThat(responseContent, containsString("var Ext = Ext"));
		} finally {
			flushLogs();
		}
		logger.info("testUiHome2 END");
	}

	@Test
	public void testUserPreferences() throws IOException {
		logger.info("testUserPreferences BEGIN");
		try {
			String sessionId = getSessionToken();

			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/current/preferences"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = getHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				assertThat(responseContent, containsString("cm_ui_startingClass"));
				EntityUtils.consume(response.getEntity());
			}
			{
				HttpPut request = new HttpPut(buildRestV3Url("sessions/current/preferences/my_config"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				request.setEntity(new StringEntity("my-value", TEXT_PLAIN));
				HttpResponse response = getHttpClient().execute(request);
				flushLogs();
//				assertNull(response.getEntity());
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				EntityUtils.consume(response.getEntity());
			}
			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/current/preferences"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = getHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				assertThat(responseContent, containsString("\"my_config\":\"my-value\""));
				EntityUtils.consume(response.getEntity());
			}
			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/current/preferences/my_config"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = getHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				assertThat(responseContent, equalTo("my-value"));
				EntityUtils.consume(response.getEntity());
			}
			{
				HttpDelete request = new HttpDelete(buildRestV3Url("sessions/current/preferences/my_config"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = getHttpClient().execute(request);
				flushLogs();
				logger.info("delete OK");
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				EntityUtils.consume(response.getEntity());
			}
			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/current/preferences/my_config"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = getHttpClient().execute(request);
				flushLogs();
				assertThat(response.getStatusLine().getStatusCode(), equalTo(204));
				EntityUtils.consume(response.getEntity());
			}
			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/current/preferences"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = getHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				assertThat(responseContent, not(containsString("\"my_config\":\"my-value\"")));
				EntityUtils.consume(response.getEntity());
			}
		} finally {
			flushLogs();
		}
		logger.info("testUserPreferences END");
	}

	@Test
	public void testSimpleSecurity() throws IOException {
		logger.info("testSimpleSecurity BEGIN");
		try {
			HttpGet get = new HttpGet(buildRestV3Url("classes/Email"));
			HttpResponse response = getHttpClient().execute(get);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), either(equalTo(401)).or(equalTo(403))); //TODO restrict to only one return code (??)
		} finally {
			flushLogs();
		}
		logger.info("testSimpleSecurity END");
	}

	@Test
	public void doTestLogin1() throws IOException {
		logger.info("doTestLogin1 BEGIN");
		try {

			HttpPost post = new HttpPost(buildRestV3Url("sessions?scope=ui"));
			post.setEntity(new StringEntity("{\"username\" : \"admin\", \"password\" : \"admin\"}", APPLICATION_JSON));
			HttpResponse response = getHttpClient().execute(post);

			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));

		} finally {
			flushLogs();
		}
		logger.info("doTestLogin1 END");
	}

	@Test
	public void doTestLogin2() throws IOException {
		logger.info("doTestLogin2 BEGIN");
		try {

			HttpPost post = new HttpPost(buildRestV3Url("sessions/?scope=ui"));
			post.setEntity(new StringEntity("{\"username\" : \"admin\", \"password\" : \"admin\"}", APPLICATION_JSON));
			HttpResponse response = getHttpClient().execute(post);

			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));

		} finally {
			flushLogs();
		}
		logger.info("doTestLogin2 END");
	}

	//TODO add session update test
	@Test
	@Ignore //TODO fix this
	public void doTestImpersonate() throws IOException {
		logger.info("doTestImpersonate BEGIN");
		try {

			String adminUser = "admin";
			String sessionId;

			{
				HttpPost post = new HttpPost(buildRestV3Url("sessions"));
				post.setEntity(new StringEntity("{\"username\" : \"" + adminUser + "\", \"password\" : \"admin\"}", APPLICATION_JSON));
				HttpResponse response = getHttpClient().execute(post);
				flushLogs();

				String responseContent = IOUtils.toString(response.getEntity().getContent());

				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));

				sessionId = checkNotNull(trimToNull(new JsonParser()
						.parse(responseContent).getAsJsonObject().get("data").getAsJsonObject().get("_id").getAsString()));
			}

			logger.info("sessionId = {}", sessionId);

			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/" + sessionId));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = newHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				assertThat(responseContent, containsString(sessionId));
				assertThat(responseContent, containsString(adminUser));
			}

			String impersonateUser = "guest";
			logger.info("impersonate user = {}", impersonateUser);

			{
				HttpPut request = new HttpPut(buildRestV3Url("sessions/" + sessionId + "/impersonate/" + impersonateUser));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = newHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(204));
			}

			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/" + sessionId));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = newHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				assertThat(responseContent, containsString(sessionId));
				assertThat(responseContent, containsString(impersonateUser));
				assertThat(responseContent, not(containsString(adminUser)));
			}
			logger.info("de-impersonate user = {}", impersonateUser);

			{
				HttpDelete request = new HttpDelete(buildRestV3Url("sessions/" + sessionId + "/impersonate"));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = newHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(204));
			}

			{
				HttpGet request = new HttpGet(buildRestV3Url("sessions/" + sessionId));
				request.setHeader("CMDBuild-Authorization", sessionId);
				HttpResponse response = newHttpClient().execute(request);
				flushLogs();
				String responseContent = IOUtils.toString(response.getEntity().getContent());
				logger.info("response = {}", responseContent);
				assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
				assertThat(responseContent, containsString(sessionId));
				assertThat(responseContent, containsString(adminUser));
				assertThat(responseContent, not(containsString(impersonateUser)));
			}

		} finally {
			flushLogs();
		}
		logger.info("doTestImpersonate END");
	}

	@Test
	public void testListDashboards() throws IOException {
		logger.info("testListDashboards BEGIN");
		try {
			String sessionId = getSessionToken();

			HttpGet request = new HttpGet(buildRestV3Url("dashboards/"));
			request.setHeader("CMDBuild-Authorization", sessionId);
			HttpResponse response = newHttpClient().execute(request);
			flushLogs();
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertThat(responseContent, containsString("success"));
//			assertThat(responseContent, containsString("\"_id\""));
//			assertThat(responseContent, not(containsString("\"id\"")));
		} finally {
			flushLogs();
		}
		logger.info("testListDashboards END");
	}

	@Test
	public void testListCustomPages() throws IOException {
		logger.info("testListCustomPages BEGIN");
		try {
			String sessionId = getSessionToken();

			HttpGet request = new HttpGet(buildRestV3Url("custompages/"));
			request.setHeader("CMDBuild-Authorization", sessionId);
			HttpResponse response = newHttpClient().execute(request);
			flushLogs();
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			//TODO add data, then validate output
		} finally {
			flushLogs();
		}
		logger.info("testListCustomPages END");
	}

	@Test
	public void testCommentsInWadl1() throws IOException {
		logger.info("testCommentsInWadl1 BEGIN");
		try {
			HttpGet request = new HttpGet(buildRestV3Url("?_wadl"));
			request.setHeader("CMDBuild-Authorization", getSessionToken());
			HttpResponse response = getHttpClient().execute(request);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertThat(responseContent, containsString("<doc>"));
		} finally {
			flushLogs();
		}
		logger.info("testCommentsInWadl1 END");
	}

	@Test
	public void testCommentsInWadl2() throws IOException {
		logger.info("testCommentsInWadl2 BEGIN");
		try {
			HttpGet request = new HttpGet(buildRestV3Url("classes?_wadl"));
			request.setHeader("CMDBuild-Authorization", getSessionToken());
			HttpResponse response = getHttpClient().execute(request);
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			assertThat(responseContent, containsString("<doc>"));
		} finally {
			flushLogs();
		}
		logger.info("testCommentsInWadl2 END");
	}
}
