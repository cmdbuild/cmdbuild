package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;

import org.cmdbuild.client.rest.RestClient;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ProcessStartActivityRestIT extends AbstractWsIT {
	
	private RestClient restClient;

	@Before
	public void init() {
		restClient = getRestClient();
		restClient.system().setConfigs("org.cmdbuild.workflow.river.enabled", "true", "org.cmdbuild.workflow.provider",
				"river", "org.cmdbuild.workflow.shark.enabled", "false");
	}

	@Test
	public void getStartActivities() {
		String token = getSessionToken();
		String processId = "RequestForChange";

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("processes/" + processId + "/start_activities"))
				.then().statusCode(200);
	}
}
