package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.util.HashMap;
import java.util.Map;

import org.cmdbuild.client.rest.RestClient;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ProcessConfigurationRestIT extends AbstractWsIT {
	
	private RestClient restClient;

	@Before
	public void init() {
		restClient = getRestClient();
		restClient.system().setConfigs("org.cmdbuild.workflow.river.enabled", "true", "org.cmdbuild.workflow.provider",
				"river", "org.cmdbuild.workflow.shark.enabled", "false");
	}

	@Test
	public void getProcessesConfigurationsStatuses() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("configuration/processes/statuses"))
				.then().statusCode(200);
	}
}
