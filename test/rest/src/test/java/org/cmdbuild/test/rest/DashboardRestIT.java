package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.hasKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class DashboardRestIT extends AbstractWsIT {

	@Test
	public void getDashboards() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("dashboards/")).then().assertThat()
				.statusCode(200);
	}
}
