package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ViewCardRestIT extends AbstractWsIT {

	@Test
	public void getCardsByViewId() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("views/" + viewId + "/cards")).then()
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void getCardById() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("views/" + viewId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object random = allCardsIds.get(new Random().nextInt(allCardsIds.size()));

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("views/" + viewId + "/cards/" + random)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void testLimit() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();
		int limit = 3;

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("limit", limit).when()
				.get(buildRestV3Url("views/" + viewId + "/cards/")).then().log().body()
				.body("data.size()", equalTo(limit)).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();
		int start = 2;

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		int total = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("views/" + viewId + "/cards")).then()
				.extract().jsonPath().getInt("data.size()");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", start).when()
				.get(buildRestV3Url("views/" + viewId + "/cards")).then().body("data.size()", equalTo(total - start))
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void testSort() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		JSONArray jArray = new JSONArray();
		JSONObject json = new JSONObject();
		json.put("property", "_id");
		json.put("direction", "ASC");
		jArray.put(json);

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		List<Object> unSortedList = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("views/" + viewId + "/cards")).then().extract().jsonPath().getList("data._id");

		List<Object> sortedListASC = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("sort", jArray)
				.get(buildRestV3Url("views/" + viewId + "/cards")).then().extract().jsonPath().getList("data._id");

		json.put("direction", "DESC");
		jArray.put(json);

		List<Object> sortedListDESC = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("sort", jArray)
				.get(buildRestV3Url("views/" + viewId + "/cards")).then().extract().jsonPath().getList("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);

		logger.info("Lista disordinata: {}", unSortedList);
		logger.info("Lista ordinata (crescente): {}", sortedListASC);
		logger.info("Lista ordinata (decrescente): {}", sortedListDESC);
	}

	@Test
	public void testFilter() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		String attribute = "Surname";
		String operator = "equal";
		String[] codes = { "Taylor" };

		JSONObject json = createJson(attribute, operator, codes);

		logger.info("Query: {}", json);

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("views/" + viewId + "/cards")).then()
				.body("data.Surname", everyItem(equalTo("Taylor"))).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void testPositionOf() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("views/" + viewId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object random = allCardsIds.get(new Random().nextInt(allCardsIds.size()));
		
		logger.info(""+random);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", 1).queryParam("limit", 3)
				.queryParam("positionOf", random).when().get(buildRestV3Url("views/" + viewId + "/cards")).then()
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void testPostDeleteCard() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();
		Map<String, Object> jsonAsMap2 = createCard();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("views/")).then().statusCode(200).extract().path("data._id");

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap2)
				.when().post(buildRestV3Url("views/" + viewId + "/cards")).then().statusCode(200).extract()
				.path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("views/" + viewId + "/cards/" + cardId))
				.then().body("data.Name", equalTo("Walter")).body("data.Surname", equalTo("White"))
				.body("data.Code", equalTo("65")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.delete(buildRestV3Url("views/" + viewId + "/cards/" + cardId)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void testPostPutDeleteCard() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();
		Map<String, Object> jsonAsMap2 = createCard();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
				.body(jsonAsMap).when().post(buildRestV3Url("views/")).then().statusCode(200).extract()
				.path("data._id");

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap2)
				.when().post(buildRestV3Url("views/" + viewId + "/cards")).then().statusCode(200).extract()
				.path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("views/" + viewId + "/cards/" + cardId))
				.then().body("data.Name", equalTo("Walter")).body("data.Surname", equalTo("White"))
				.body("data.Code", equalTo("65")).statusCode(200);

		jsonAsMap2.put("Surname", "Rossi");
		jsonAsMap2.put("Name", "Mario");

		given().header(CMDBUILD_AUTH_HEADER, token).when().contentType("application/json").body(jsonAsMap2)
				.put(buildRestV3Url("views/" + viewId + "/cards/" + cardId)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("views/" + viewId + "/cards/" + cardId)).then()
				.body("data.Surname", equalTo("Rossi")).body("data.Name", equalTo("Mario")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.delete(buildRestV3Url("views/" + viewId + "/cards/" + cardId)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	public Map<String, Object> createView() {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("type", "filter");
		jsonAsMap.put("name", "Employee");
		jsonAsMap.put("sourceClassName", "Employee");
		jsonAsMap.put("description", "employee");
		jsonAsMap.put("filter", false);

		return jsonAsMap;
	}

	public Map<String, Object> createCard() {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Code", "65");
		jsonAsMap.put("Description", "Walter White");
		jsonAsMap.put("Surname", "White");
		jsonAsMap.put("Name", "Walter");
		jsonAsMap.put("Type", 21);
		jsonAsMap.put("Qualification", 22);
		jsonAsMap.put("Level", 146);
		jsonAsMap.put("Email", "white.walter@example.com");
		jsonAsMap.put("Office", 108);
		jsonAsMap.put("Phone", "4534635");
		jsonAsMap.put("Mobile", "565479");

		return jsonAsMap;
	}

	public JSONObject createJson(String attribute, String operator, Object value) {
		JSONObject json = new JSONObject();
		json.put("attribute", attribute);
		json.put("operator", operator);
		json.put("value", value);

		JSONObject json2 = new JSONObject();
		json2.put("simple", json);

		JSONObject json3 = new JSONObject();
		json3.put("attribute", json2);
		return json3;
	}

}
