package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;

import java.util.HashMap;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ClassFilterRestIT extends AbstractWsIT {

	@Test
	public void getClassFilters() {
		String classId = "Room";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("classes/" + classId + "/filters"))
				.then().statusCode(200);
	}

	@Test
	public void postDeleteClassFilters() {
		String classId = "Monitor";
		String token = getSessionToken();

		String attribute = "Code";
		String operator = "equal";
		String[] codes = { "87" };

		JSONObject json = createJson(attribute, operator, codes);
		long filterId = createFilter(token, json, classId);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.delete(buildRestV3Url("classes/" + classId + "/filters/" + filterId)).then().statusCode(200);
	}

	public JSONObject createJson(String attribute, String operator, Object value) {
		JSONObject json = new JSONObject();
		json.put("attribute", attribute);
		json.put("operator", operator);
		json.put("value", value);

		JSONObject json2 = new JSONObject();
		json2.put("simple", json);

		JSONObject json3 = new JSONObject();
		json3.put("attribute", json2);
		return json3;
	}

	public long createFilter(String token, JSONObject json, String classId) {
		Map<String, Object> jsonAsMap = new HashMap<>();
		jsonAsMap.put("name", "Monitor");
		jsonAsMap.put("target", "Monitor");
		jsonAsMap.put("description", "monitor");
		jsonAsMap.put("configuration", json.toString());
		jsonAsMap.put("shared", true);

		long filterId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
				.body(jsonAsMap).when().post(buildRestV3Url("classes/" + classId + "/filters/")).then().statusCode(200)
				.extract().path("data._id");
		return filterId;
	}
}
