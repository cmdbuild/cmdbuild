package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.allOf;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class CustomPageRestIT extends AbstractWsIT {

	@Test
	public void getCustomPages() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("custompages/")).then()
				.statusCode(200);
	}

	@Test
	public void getCustomPageById() {
		long customPageId = createCustomPage();

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("custompages/" + customPageId))
				.then().statusCode(200);

		deleteCustomPage(customPageId);
	}

	@Test
	public void postDeleteNewCustomPage() {
		long customPageId = createCustomPage();

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("custompages/" + customPageId))
				.then().body("data.description", equalTo("classescp")).statusCode(200);

		deleteCustomPage(customPageId);
	}

	@Test
	public void postPutDeleteNewCustomPage() {
		Map<String, Object> jsonAsMap = new HashMap<>();
		jsonAsMap.put("description", "classescp_modificata");

		long customPageId = createCustomPage();

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("custompages/" + customPageId))
				.then().body("data.description", equalTo("classescp")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).when().contentType("application/json").body(jsonAsMap)
				.put(buildRestV3Url("custompages/" + customPageId)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("custompages/" + customPageId))
				.then().body("data.description", equalTo("classescp_modificata")).statusCode(200);

		deleteCustomPage(customPageId);
	}
	

	public long createCustomPage() {
		long customPageId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.multiPart(new File("src/test/resources/org/cmdbuild/test/rest/classescp.zip")).when()
				.post(buildRestV3Url("custompages/")).then().statusCode(200).extract().path("data._id");

		return customPageId;
	}

	public void deleteCustomPage(long customPageId) {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).when()
				.delete(buildRestV3Url("custompages/" + customPageId)).then().statusCode(200);

	}

}