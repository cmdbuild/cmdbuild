package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasKey;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasValue;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.http.ContentType;

import org.apache.commons.lang3.RandomStringUtils;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ReportRestIT extends AbstractWsIT {
	private final static Logger LOGGER = LoggerFactory.getLogger(SimpleRestIT.class);

	@Test
	public void getReports() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("reports")).then().log().body()
				.statusCode(200);
	}

	@Test
	public void testLimit() {
		String token = getSessionToken();

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("limit", 1).get(buildRestV3Url("reports")).then()
				.body("data.size()", equalTo(1)).statusCode(200);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		int startValue = 1;

		int totalValue = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("reports")).then().extract()
				.jsonPath().getInt("meta.total");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", startValue).get(buildRestV3Url("reports"))
				.then().body("data.size()", equalTo(totalValue - startValue)).statusCode(200);
	}

	@Test
	public void testDetailed() {
		String token = getSessionToken();

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("detailed", true).get(buildRestV3Url("reports")).then()
				.body("data[0]", hasKey("query")).statusCode(200);

	}

	@Test
	public void getReportById() {
		String token = getSessionToken();
		int id = 597;

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("reports/" + id)).then()
				.statusCode(200);
	}

	@Test
	public void getReportTemplate() {
		String token = getSessionToken();
		int id = 597;

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("reports/" + id + "/template/file.zip")).then().statusCode(200);
	}

	@Test
	public void getReportAttributes() {
		String token = getSessionToken();
		int id = 597;

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("reports/" + id + "/attributes")).then()
				.statusCode(200);
	}

}
