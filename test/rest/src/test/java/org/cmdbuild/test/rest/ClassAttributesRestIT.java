package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.allOf;

import java.util.HashMap;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ClassAttributesRestIT extends AbstractWsIT {

	@Test
	public void getClassAttributes() {
		String classId = "Printer";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/" + classId + "/attributes/")).then().statusCode(200);
	}

	@Test
	public void getProcessAttributes() {
		String processId = "Building";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("processes/" + processId + "/attributes/")).then().statusCode(200);
	}

	@Test
	public void getClassAttributesById() {
		String classId = "Printer";
		String attributeId = "FinalCost";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/" + classId + "/attributes/" + attributeId)).then().statusCode(200);
	}

	@Test
	public void getProcessAttributesById() {
		String processId = "PC";
		String attributeId = "IPAddress";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("processes/" + processId + "/attributes/" + attributeId)).then().statusCode(200);
	}

	@Test
	public void testLimit() {
		String token = getSessionToken();
		String classId = "User";
		int limitValue = 2;

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("limit", limitValue)
				.get(buildRestV3Url("classes/" + classId + "/attributes/")).then()
				.body("data.size()", equalTo(limitValue)).statusCode(200);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		int startValue = 2;
		String classId = "NetworkDevice";

		int totalValue = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/attributes/")).then().extract().jsonPath()
				.getInt("meta.total");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", startValue)
				.get(buildRestV3Url("classes/" + classId + "/attributes/")).then()
				.body("data.size()", equalTo(totalValue - startValue)).statusCode(200);
	}
	
	@Test
	public void postDeleteDomainAttribute() {
		String token = getSessionToken();
		String classId = "NetworkDevice";
		Map<String, Object> jsonAsMap = new HashMap<>();

		String attributeId = createAttribute(token, jsonAsMap, classId);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.get(buildRestV3Url("classes/" + classId + "/attributes/" + attributeId)).then()
				.body("data.type", equalTo("boolean")).body("data.name", equalTo("Attribute Test"))
				.body("data.description", equalTo("Attribute Test")).body("data.active", equalTo(true))
				.body("data.mode", equalTo("write")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.delete(buildRestV3Url("classes/" + classId + "/attributes/" + attributeId)).then().statusCode(200);
	}

	@Test
	public void postPutDeleteDomainAttribute() {
		String classId = "Office";
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		String attributeId = createAttribute(token, jsonAsMap, classId);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.get(buildRestV3Url("classes/" + classId + "/attributes/" + attributeId)).then()
				.body("data.type", equalTo("boolean")).body("data.name", equalTo("Attribute Test"))
				.body("data.description", equalTo("Attribute Test")).body("data.active", equalTo(true))
				.body("data.mode", equalTo("write")).statusCode(200);

		jsonAsMap.put("description", "AttributeMoficato");

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.put(buildRestV3Url("classes/" + classId + "/attributes/" + attributeId)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.get(buildRestV3Url("classes/" + classId + "/attributes/" + attributeId)).then()
				.body("data.description", equalTo("AttributeMoficato")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.delete(buildRestV3Url("classes/" + classId + "/attributes/" +attributeId)).then().statusCode(200);
	}

	
	public String createAttribute(String token, Map<String, Object> jsonAsMap, String classId) {
		jsonAsMap.put("type", "boolean");
		jsonAsMap.put("name", "Attribute Test");
		jsonAsMap.put("description", "Attribute Test");
		jsonAsMap.put("active", true);
		jsonAsMap.put("index", 1);
		jsonAsMap.put("mode", "write");
		jsonAsMap.put("showInGrid", true);
		jsonAsMap.put("unique", false);
		jsonAsMap.put("mandatory", false);
		jsonAsMap.put("inherited", false);
		jsonAsMap.put("hidden", false);

		String attributeId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap)
				.when().post(buildRestV3Url("classes/" + classId + "/attributes/")).then().statusCode(200).extract()
				.path("data._id");

		return attributeId;
	}

}
