package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.jayway.restassured.http.ContentType;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class GrantRestIT extends AbstractWsIT {

	@Test
	public void getGrants() {
		String token = getSessionToken();

		List<Object> allRolesIds = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("roles/")).then()
				.statusCode(200).extract().jsonPath().getList("data._id");

		Object random = allRolesIds.get(new Random().nextInt(allRolesIds.size()));

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("roles/" + random + "/grants")).then()
				.statusCode(200);
	}

	@Test
	public void testLimit() {
		String token = getSessionToken();
		int limit = 3;
		int roleId = 942;

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("limit", limit).when()
				.get(buildRestV3Url("roles/" + roleId + "/grants/")).then()
				.body("data.size()", equalTo(limit)).statusCode(200);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		int start = 2;
		int roleId = 942;

		int total = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("roles/" + roleId + "/grants"))
				.then().extract().jsonPath().getInt("data.size()");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", start).when()
				.get(buildRestV3Url("roles/" + roleId + "/grants")).then().body("data.size()", equalTo(total - start))
				.statusCode(200);
	}

}