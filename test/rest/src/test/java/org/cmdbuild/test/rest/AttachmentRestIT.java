package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class AttachmentRestIT extends AbstractWsIT {

	@Test
	public void getAttachments() {
		String classId = "Employee";
		String token = getSessionToken();

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object random = allCardsIds.get(new Random().nextInt(allCardsIds.size()));

		given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards/" + random + "/attachments")).then().statusCode(200);
	}

	@Test
	public void postDeleteNewAttachment() {
		String classId = "Printer";
		String token = getSessionToken();

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object cardId = allCardsIds.get(new Random().nextInt(allCardsIds.size()));

		Object attachmentId = createAttachment(classId, cardId, token);

		given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/attachments/" + attachmentId)).then()
				.body("data._author", equalTo("admin")).body("data._name", equalTo("pdfProva.pdf")).statusCode(200);

		deleteAttachment(classId, cardId, attachmentId, token);
	}
	
	@Test
	public void getAttachmentPreview() {
		String classId = "Printer";
		String token = getSessionToken();

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object cardId = allCardsIds.get(new Random().nextInt(allCardsIds.size()));

		Object attachmentId = createAttachment(classId, cardId, token);

		given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/attachments/" + attachmentId+"/preview")).then()
				.body("data", hasKey("hasPreview")).statusCode(200);

		deleteAttachment(classId, cardId, attachmentId, token);
	}
	
	@Test
	public void getAttachmentHistory() {
		String classId = "Printer";
		String token = getSessionToken();

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object cardId = allCardsIds.get(new Random().nextInt(allCardsIds.size()));

		Object attachmentId = createAttachment(classId, cardId, token);

		given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/attachments/" + attachmentId+"/history")).then()
				.statusCode(200);

		deleteAttachment(classId, cardId, attachmentId, token);
	}
	
	@Test
	public void getAttachmentFile() {
		String classId = "Printer";
		String token = getSessionToken();

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object cardId = allCardsIds.get(new Random().nextInt(allCardsIds.size()));

		Object attachmentId = createAttachment(classId, cardId, token);

		given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/attachments/" + attachmentId+"/history/1.0/file")).then()
				.statusCode(200);

		deleteAttachment(classId, cardId, attachmentId, token);
	}
	
	
	
	public Object createAttachment(String classId, Object cardId, String token) {

		Object attachmentId = given().header(CMDBUILD_AUTH_HEADER, token)
				.multiPart(new File("src/test/resources/org/cmdbuild/test/rest/pdfProva.pdf"))
				.post(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/attachments/")).then()
				.statusCode(200).extract().path("data._id");

		return attachmentId;
	}

	public void deleteAttachment(String classId, Object cardId, Object attachmentId, String token) {

		given().header(CMDBUILD_AUTH_HEADER, token)
				.delete(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/attachments/" + attachmentId))
				.then().statusCode(200);
	}

}
