package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.hasKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ClassRestIT extends AbstractWsIT {
	@Test
	public void testClasses() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("classes/")).then().assertThat()
				.statusCode(200);
	}

	@Test
	public void testClassDetail() {
		List<String> allClasses = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/")).then().extract().jsonPath().getList("data._id");

		for (String string : allClasses) {
			given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("classes/" + string)).then()
					.assertThat().statusCode(200);
			logger.info(string);
		}
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		int startValue = 22;
		int totalValue = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("classes")).then().extract()
				.jsonPath().getInt("meta.total");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", startValue).get(buildRestV3Url("classes"))
				.then().body("data.size()", equalTo(totalValue - startValue)).statusCode(200);
	}

	@Test
	public void testLimit() {
		String token = getSessionToken();

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("limit", 1).get(buildRestV3Url("classes")).then()
				.body("data.size()", equalTo(1)).statusCode(200);
	}

	@Test
	public void testDetailed() {
		String token = getSessionToken();

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("detailed", true).get(buildRestV3Url("classes")).then()
				.body("data", everyItem((hasKey("widgets"))))
				.body("data", everyItem((hasKey("defaultOrder")))).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("detailed", false).get(buildRestV3Url("classes")).then()
				.body("data", everyItem(not(hasKey("widgets")))).body("data", everyItem(not(hasKey("defaultOrder"))))
				.statusCode(200);
	}

	@Test
	public void postDeleteClass() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createClass();

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes")).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + jsonAsMap.get("name")))
				.then().body("data.parent", equalTo("Class")).body("data.name", equalTo("TestClass"))
				.body("data.description", equalTo("Test")).body("data.active", equalTo(true))
				.body("data.prototype", equalTo(false)).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + jsonAsMap.get("name")))
				.then().statusCode(200);
	}

	@Test
	public void testPostPutDeleteClass() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createClass();

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/")).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + jsonAsMap.get("name")))
				.then().body("data.name", equalTo("TestClass")).body("data.parent", equalTo("Class"))
				.body("data.description", equalTo("Test")).body("data.type", equalTo("standard")).statusCode(200);

		jsonAsMap.put("description", "This is a test");

		given().header(CMDBUILD_AUTH_HEADER, token).when().contentType("application/json").body(jsonAsMap)
				.put(buildRestV3Url("classes/" + jsonAsMap.get("name"))).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + jsonAsMap.get("name")))
				.then().body("data.description", equalTo("This is a test")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + jsonAsMap.get("name")))
				.then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + jsonAsMap.get("name")))
				.then().statusCode(404);
	}

	public Map<String, Object> createClass() {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("parent", "Class");
		jsonAsMap.put("name", "TestClass");
		jsonAsMap.put("description", "Test");
		jsonAsMap.put("active", true);
		jsonAsMap.put("_can_read", true);
		jsonAsMap.put("_can_create", true);
		jsonAsMap.put("_can_update", true);
		jsonAsMap.put("_can_clone", true);
		jsonAsMap.put("_can_delete", true);
		jsonAsMap.put("_can_modify", true);
		jsonAsMap.put("prototype", false);
		jsonAsMap.put("type", "standard");

		return jsonAsMap;
	}
}