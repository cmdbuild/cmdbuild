/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.rest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.test.dao.utils.ContextProvider;
import org.cmdbuild.test.dao.utils.TestContext;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.cmdbuild.test.rest.utils.TomcatManagerForTest;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.LoggerFactory;

@ContextProvider
public class TestContextProviders {

	public static final String R2U = "r2u", DEMO = "demo";

	private static final String BASE_URL_KEY = "cmdbuild.test.base.url";

	@ContextProvider(R2U)
	public static TestContext r2u() {
		return initTomcatAndDb(DatabaseCreator.READY_2_USE_DATABASE, false);
	}

	@ContextProvider(DEMO)
	public static TestContext demo() {
		return initTomcatAndDb(DatabaseCreator.DEMO_DATABASE, false);
	}

	public static TestContext initTomcatAndDb(String dbType, boolean includeShark) {
		String baseUrl = buildBaseUrl();
		TomcatManagerForTest tomcatManagerForTest;
		if (baseUrl == null) {
			tomcatManagerForTest = new TomcatManagerForTest();
			tomcatManagerForTest.initTomcatAndDb(dbType, includeShark);
			baseUrl = tomcatManagerForTest.getBaseUrl();
		} else {
			tomcatManagerForTest = null;
		}
		System.out.println("\n\n\n");
		return new TestContext(map("baseUrl", baseUrl, "tomcatManagerForTest", tomcatManagerForTest), (c) -> {
			if (tomcatManagerForTest != null) {
				tomcatManagerForTest.cleanupTomcatAndDb();
			}
		});
	}

	private static String buildBaseUrl() {
		String baseUrl = trimToNull(System.getProperty(BASE_URL_KEY));
		if (baseUrl == null) {
			java.util.Properties p = new Properties();
			try {
				p.load(new FileInputStream("target/classes/test.properties"));
			} catch (IOException e) {
				LoggerFactory.getLogger(AbstractWsIT.class).warn("unable to load test config from file", e);
			}
			baseUrl = trimToNull(p.getProperty(BASE_URL_KEY));
		}
		return baseUrl;
	}
}
