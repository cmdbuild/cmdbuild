package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.notNullValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.jayway.restassured.http.ContentType;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class AuditRestIT extends AbstractWsIT {

	@Test
	public void getMark() {
		String mark = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("system/audit/mark"))
				.then().extract().jsonPath().getString("data.mark");

		logger.info(mark);
	}

	@Test
	public void getRequests() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("system/audit/requests")).then()
				.statusCode(200);
	}

	@Test
	public void getErrors() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("system/audit/errors")).then()
				.statusCode(200);
	}

	@Test
	public void getRequestById() {
		List<String> allRequestsIds = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("system/audit/requests")).then().extract().jsonPath().getList("data.requestId");

		String randomId = allRequestsIds.get(new Random().nextInt(allRequestsIds.size()));

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("system/audit/requests/" + randomId))
				.then().statusCode(200);
	}

}
