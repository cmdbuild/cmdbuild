package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;

import java.util.HashMap;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ViewRestIT extends AbstractWsIT {

	@Test
	public void viewTest() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("views/")).then().statusCode(200);
	}

	@Test
	public void getViewById() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
				.body(jsonAsMap).when().post(buildRestV3Url("views/")).then().statusCode(200).extract()
				.path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("views/" + viewId)).then()
				.body("data.type", equalTo("FILTER")).body("data.name", equalTo("Room"))
				.body("data.description", equalTo("room")).body("data.sourceClassName", equalTo("Room"))
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void postDeleteView() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
				.body(jsonAsMap).when().post(buildRestV3Url("views/")).then().statusCode(200).extract()
				.path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("views/" + viewId)).then()
				.body("data.type", equalTo("FILTER")).body("data.name", equalTo("Room"))
				.body("data.description", equalTo("room")).body("data.sourceClassName", equalTo("Room"))
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	@Test
	public void testPostPutDeleteView() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = createView();

		long viewId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
				.body(jsonAsMap).when().post(buildRestV3Url("views/")).then().statusCode(200).extract()
				.path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("views/" + viewId)).then()
				.body("data.type", equalTo("FILTER")).body("data.name", equalTo("Room"))
				.body("data.description", equalTo("room")).body("data.sourceClassName", equalTo("Room"))
				.statusCode(200);

		jsonAsMap.put("description", "room updated");

		given().header(CMDBUILD_AUTH_HEADER, token).when().contentType("application/json").body(jsonAsMap)
				.put(buildRestV3Url("views/" + viewId)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("views/" + viewId)).then()
				.body("data.description", equalTo("room updated")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("views/" + viewId)).then()
				.statusCode(200);
	}

	public Map<String, Object> createView() {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("type", "filter");
		jsonAsMap.put("name", "Room");
		jsonAsMap.put("sourceClassName", "Room");
		jsonAsMap.put("description", "room");
		jsonAsMap.put("filter", false);

		return jsonAsMap;
	}
}
