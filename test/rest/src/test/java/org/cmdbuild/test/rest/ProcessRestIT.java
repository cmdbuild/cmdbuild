package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.hasItems;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import static org.cmdbuild.test.rest.TestContextProviders.R2U;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasValue;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmdbuild.client.rest.RestClient;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.http.ContentType;

import org.apache.commons.lang3.RandomStringUtils;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ProcessRestIT extends AbstractWsIT {

	private RestClient restClient;

	@Before
	public void init() {
		restClient = getRestClient();
		restClient.system().setConfigs("org.cmdbuild.workflow.river.enabled", "true", "org.cmdbuild.workflow.provider",
				"river", "org.cmdbuild.workflow.shark.enabled", "false");
	}

	@Test
	public void getProcesses() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("processes/")).then()
				.statusCode(200);
	}

	@Test
	public void testLimit() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).queryParam("limit", 1).get(buildRestV3Url("processes"))
				.then().body("data.size()", equalTo(1)).statusCode(200);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		int startValue = 2;

		int totalValue = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("processes")).then().extract()
				.jsonPath().getInt("meta.total");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", startValue).get(buildRestV3Url("processes"))
				.then().body("data.size()", equalTo(totalValue - startValue)).statusCode(200);
	}

	@Test
	public void getProcessesById() {
		String id = "Activity";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("processes/" + id)).then()
				.body("data.description", equalTo("Attività")).body("data.parent", equalTo("Class")).statusCode(200);
	}

	@Test
	public void getGeneratedId() {
		String id = "RequestForChange";

		long generatedId = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("processes/" + id + "/generate_id")).then().body("success", equalTo(true))
				.statusCode(200).extract().path("data");

		logger.info("Generated id: {}", generatedId);
	}

	@Test
	public void getVersions() {
		String id = "RequestForChange";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("processes/" + id + "/versions"))
				.then().body("success", equalTo(true)).statusCode(200);
	}

	@Test
	public void getTemplate() {
		String id = "Activity";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("processes/" + id + "/template"))
				.then().body(startsWith("<?xml version=")).statusCode(200);
	}

	@Test
	public void postFileNewVersion() {
		String id = "RequestForChange";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.multiPart(new File("src/test/resources/org/cmdbuild/test/workflow/RequestForChange_1.xpdl")).when()
				.post(buildRestV3Url("processes/" + id + "/versions")).then().statusCode(200);
	}

	@Test
	public void getFile() {
		String token = getSessionToken();
		String id = "RequestForChange";

		String planId = given().header(CMDBUILD_AUTH_HEADER, token)
				.multiPart(new File("src/test/resources/org/cmdbuild/test/workflow/RequestForChange_1.xpdl")).when()
				.post(buildRestV3Url("processes/" + id + "/versions")).then().statusCode(200).extract()
				.path("data.planId");

		given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("processes/" + id + "/versions/" + planId + "/file")).then()
				.body(startsWith("<?xml version=")).statusCode(200);
	}

	@Test
	public void postFileMigration() {
		String id = "RequestForChange";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.multiPart(new File("src/test/resources/org/cmdbuild/test/workflow/RequestForChange_1.xpdl"))
				.queryParam("provider", "river").when().post(buildRestV3Url("processes/" + id + "/migration")).then()
				.statusCode(200);
	}

}
