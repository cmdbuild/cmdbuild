package org.cmdbuild.test.rest;

import java.io.IOException;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.cmdbuild.client.rest.api.LookupApi;
import org.cmdbuild.client.rest.model.ClassData;
import org.cmdbuild.client.rest.model.MenuEntry;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.R2U;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(R2U)
public class SimpleRestR2UIT extends AbstractWsIT {

	@Test
	public void testCurrentSessionWs() throws IOException {
		logger.info("testCurrentSessionWs BEGIN");
		String responseContent = loginAndGet("sessions/current");
		assertThat(responseContent, containsString("\"username\":\"admin\""));
		assertThat(responseContent, containsString("\"success\":true"));
		logger.info("testCurrentSessionWs END");
	}

	@Test
	public void testBootStatusWs() throws IOException {
		logger.info("testBootStatusWs BEGIN");
		String responseContent = loginAndGet("boot/status");
		assertEquals("{\"success\":true,\"status\":\"READY\"}", responseContent);
		logger.info("testBootStatusWs END");
	}

	@Test
	public void testSystemConfigWs() throws IOException {
		logger.info("testSystemConfigWs BEGIN");
		getRestClient().system().setConfig("my.config", "something");
		String responseContent = loginAndGet("system/config");
		assertThat(responseContent, containsString("\"my.config\":\"something\""));
		logger.info("testSystemConfigWs END");
	}

	@Test
	public void testClassesWs() throws IOException {
		logger.info("testClassesWs BEGIN");
		String responseContent = loginAndGet("classes");
		assertThat(responseContent, containsString("\"success\":true"));
		assertThat(responseContent, containsString("\"name\":\"Building\""));
		assertThat(responseContent, containsString("\"_id\":\"Building\""));
		logger.info("testClassesWs END");
	}

	@Test
	public void testClassDetailWs() throws IOException {
		logger.info("testClassDetailWs BEGIN");
		ClassData classe = getRestClient().classe().getById("Employee");
		assertEquals("Employee", classe.getName());
		assertEquals("Employee", classe.getId());
		assertEquals("standard", classe.getType());//TODO test all data
		logger.info("testClassDetailWs END");
	}

	@Test
	public void testLookupWs() throws IOException {
		logger.info("testLookupWs BEGIN");
		List<LookupApi.LookupValue> values = getRestClient().lookup().getValues("RFC Category");
		assertEquals(7, values.size());
		LookupApi.LookupValue value = values.get(0);
		assertEquals("FPC", value.getCode());
		assertEquals("Formatting PC", value.getDescription());
		logger.info("testLookupWs END");
	}

	@Test
	public void testCardsWs() throws IOException {
		logger.info("testCardsWs BEGIN");
		String responseContent = loginAndGet("classes/Building/cards");
		assertThat(responseContent, containsString("\"success\":true"));
		assertThat(responseContent, containsString("\"_type\":\"Building\""));
		assertThat(responseContent, containsString("\"Description\":\"Data Center\""));
		logger.info("testCardsWs END");
	}

	@Test
	public void testDomainsWs() throws IOException {
		logger.info("testDomainsWs BEGIN");
		String responseContent = loginAndGet("domains");
		assertThat(responseContent, containsString("\"success\":true"));
		logger.info("testDomainsWs END");
	}

	@Test
	public void testMenuWs() throws IOException {
		logger.info("testMenuWs BEGIN");
		String responseContent = loginAndGet("menu");
		assertThat(responseContent, containsString("\"success\":true"));
		logger.info("testMenuWs END");
	}

	@Test
	public void testMenuWs2() throws IOException {
		logger.info("testMenuWs2 BEGIN");
		MenuEntry menu = getRestClient().menu().getMenu();
		assertEquals("root", menu.getMenuType());
		assertFalse(menu.getChildren().isEmpty());
		logger.info("testMenuWs2 END");
	}

	private String loginAndGet(String path) throws IOException {
		try {
			String sessionId = getSessionToken();
			HttpGet request = new HttpGet(buildRestV3Url(path));
			request.setHeader("CMDBuild-Authorization", sessionId);
			HttpResponse response = newHttpClient().execute(request);
			flushLogs();
			String responseContent = IOUtils.toString(response.getEntity().getContent());
			logger.info("response = {}", responseContent);
			assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
			return responseContent;
		} finally {
			flushLogs();
		}
	}
}
