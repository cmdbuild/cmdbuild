package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.hasItems;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.http.ContentType;

import org.apache.commons.lang3.RandomStringUtils;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class UserRestIT extends AbstractWsIT {

	private final static Logger LOGGER = LoggerFactory.getLogger(SimpleRestIT.class);

	@Test
	public void getUsers() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("users/")).then()
				.body("data.username", hasItem("admin")).statusCode(200);
	}

	@Test
	public void getUserById() {
		String token = getSessionToken();
		int id = (int) given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("users")).then().extract()
				.jsonPath().getList("data.findAll{ it.username == 'admin'}._id ").get(0);

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("users/" + id)).then()
				.body("data.description", equalTo("Administrator")).body("data.active", equalTo(true)).statusCode(200);
	}

	@Test
	public void testLimit() {
		String token = getSessionToken();

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("limit", 1).get(buildRestV3Url("users")).then()
				.body("data.size()", equalTo(1)).statusCode(200);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		int startValue = 3;

		int totalValue = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("users")).then().extract()
				.jsonPath().getInt("meta.total");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", startValue).get(buildRestV3Url("users")).then()
				.body("data.size()", equalTo(totalValue - startValue)).statusCode(200);
	}

	@Test
	public void testSort() {
		String token = getSessionToken();

		JSONArray jArray = new JSONArray();
		JSONObject json = new JSONObject();
		json.put("property", "Id");
		json.put("direction", "ASC");
		jArray.put(json);

		List<Integer> unSortedList = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("users")).then()
				.extract().jsonPath().getList("data._id");

		logger.info("" + unSortedList);

		List<Integer> sortedListASC = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("sort", jArray)
				.get(buildRestV3Url("users")).then().extract().jsonPath().getList("data._id");

		logger.info("" + sortedListASC);

		json.put("direction", "DESC");
		jArray.put(json);

		List<Integer> sortedListDESC = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("sort", jArray)
				.get(buildRestV3Url("users")).then().extract().jsonPath().getList("data._id");

		logger.info("" + sortedListDESC);

		logger.info("Lista disordinata: {}", unSortedList);
		logger.info("Lista ordinata (crescente): {}", sortedListASC);
		logger.info("Lista ordinata (decrescente): {}", sortedListDESC);
	}

	@Test
	public void testFilterEqual() {
		String token = getSessionToken();

		String attribute = "Username";
		String operator = "equal";
		String[] codes = {"admin"};

		JSONObject json = createJson(attribute, operator, codes);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.username", hasItem("admin")).statusCode(200);
	}

	@Test
	public void testFilterNotEqual() {
		String token = getSessionToken();

		String attribute = "Email";
		String operator = "notequal";
		String[] codes = {"michael.davis@example.com"};

		JSONObject json = createJson(attribute, operator, codes);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.email", not(hasItem("michael.davis@example.com"))).statusCode(200);

	}

	@Test
	public void testFilterNull() {
		String token = getSessionToken();

		String attribute = "Email";
		String operator = "isnull";
		String[] emptyArr = {};

		JSONObject json = createJson(attribute, operator, emptyArr);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.email", everyItem(nullValue())).statusCode(200);
	}

	@Test
	public void testFilterNotNull() {
		String token = getSessionToken();

		String attribute = "Email";
		String operator = "isnotnull";
		String[] emptyArr = {};

		JSONObject json = createJson(attribute, operator, emptyArr);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.email", everyItem(not(nullValue()))).statusCode(200);
	}

	@Test
	public void testFilterGreater() {
		String token = getSessionToken();
		String attribute = "Id";
		String operator = "greater";
		long[] id = {100000000000L};

		JSONObject json = createJson(attribute, operator, id);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data._id", everyItem(greaterThan(id[0]))).statusCode(200);

	}

	@Test
	public void testFilterLess() {
		String token = getSessionToken();

		String attribute = "Id";
		String operator = "less";
		int[] id = {678};

		JSONObject json = createJson(attribute, operator, id);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data._id", everyItem(lessThan(id[0]))).statusCode(200);
	}

	@Test
	public void testFilterIn() {
		String token = getSessionToken();

		String attribute = "Email";
		String operator = "in";
		String[] emails = {"michael.davis@example.com", "patricia.jones@example.com"};

		JSONObject json = createJson(attribute, operator, emails);

		logger.info("Query 1: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.email", hasItems(emails[0], emails[1])).statusCode(200);
	}

	@Test
	public void testFilterBetween() {
		String token = getSessionToken();
	
		String attribute = "Id";
		String operator = "between";
		int[] offices = {600, 700};

		JSONObject json = createJson(attribute, operator, offices);

		logger.info("Query 1: {}", json);

		List<String> requestedOffices = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("users")).then().statusCode(200).and().extract().jsonPath().getList("data._id");
		logger.info("Utenti richiesti: {}", requestedOffices);
	}


	@Test
	public void testFilterLike() {
		String token = getSessionToken();

		String attribute = "Description";
		String operator = "like";
		String[] desc = {"Administrator"};

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query : {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.description", everyItem(containsString(desc[0]))).statusCode(200);
	}

	@Test
	public void testFilterContain() {
		String token = getSessionToken();

		String attribute = "Description";
		String operator = "contain";
		String[] desc = {"work"};

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query : {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.description", everyItem(containsString(desc[0]))).statusCode(200);
	}

	@Test
	public void testFilterNotContain() {
		String token = getSessionToken();

		String attribute = "Description";
		String operator = "notcontain";
		String[] desc = {"workflow"};

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query : {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when().get(buildRestV3Url("users"))
				.then().body("data.description", everyItem(not(containsString(desc[0])))).statusCode(200);
	}

	@Test
	public void testPostPut() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		long id = createUser(token, jsonAsMap, true, false);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("users/" + id)).then()
				.body("data.username", equalTo(jsonAsMap.get("username")))
				.body("data.description", equalTo("White Walter")).body("data.email", equalTo(jsonAsMap.get("email")))
				.body("data.active", equalTo(true)).body("data.service", equalTo(false)).statusCode(200);

		jsonAsMap.put("description", "White Walter modificato");

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.put(buildRestV3Url("users/" + id)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("users/" + id)).then()
				.body("data.description", equalTo("White Walter modificato")).statusCode(200);
	}

	@Test
	public void testLoginAdminUser() {
		given().contentType(ContentType.JSON).body("{\"username\" : \"admin\", \"password\" : \"admin\"}")
				.post(buildRestV3Url("sessions?scope=ui")).then().statusCode(200);
	}

	@Test
	public void testLoginNewUser() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();
		createUser(token, jsonAsMap, true, false);

		JSONObject json = new JSONObject();
		json.put("username", jsonAsMap.get("username"));
		json.put("password", jsonAsMap.get("password"));

		given().contentType(ContentType.JSON).body(json).post(buildRestV3Url("sessions?scope=ui")).then().statusCode(200);
	}

	@Test
	public void testLoginNewUserServiceTrue() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		createUser(token, jsonAsMap, true, true);

		JSONObject json = new JSONObject();
		json.put("username", jsonAsMap.get("username"));
		json.put("password", jsonAsMap.get("password"));

		given().contentType(ContentType.JSON).body(json).post(buildRestV3Url("sessions?scope=ui"))
				.then().statusCode(not(200));
	}

	@Test
	public void testLoginNewUserServiceTrueAsService() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		createUser(token, jsonAsMap, true, true);

		JSONObject json = new JSONObject();
		json.put("username", jsonAsMap.get("username"));
		json.put("password", jsonAsMap.get("password"));

		given().contentType(ContentType.JSON).body(json).post(buildRestV3Url("sessions?scope=service"))
				.then().statusCode(200);
	}

	@Test
	public void testLoginNewUserActiveFalse() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		createUser(token, jsonAsMap, false, false);

		JSONObject json = new JSONObject();
		json.put("username", jsonAsMap.get("username"));
		json.put("password", jsonAsMap.get("password"));

		given().contentType(ContentType.JSON).body(json).post(buildRestV3Url("sessions?scope=ui")).then().statusCode(not(200));
	}

	public JSONObject createJson(String attribute, String operator, Object value) {
		JSONObject json = new JSONObject();
		json.put("attribute", attribute);
		json.put("operator", operator);
		json.put("value", value);

		JSONObject json2 = new JSONObject();
		json2.put("simple", json);

		JSONObject json3 = new JSONObject();
		json3.put("attribute", json2);
		return json3;
	}

	public long createUser(String token, Map<String, Object> jsonAsMap, boolean active, boolean service) {
		JSONObject[] jsonArray = {};
		jsonAsMap.put("username", "wwhite" + randomizedString());
		jsonAsMap.put("description", "White Walter");
		jsonAsMap.put("email", "walter.white" + randomizedString() + "@example.com");
		jsonAsMap.put("active", active);
		jsonAsMap.put("service", service);
		jsonAsMap.put("password", "wwhite");
		jsonAsMap.put("userGroups", jsonArray);

		long userId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("users")).then().statusCode(200).extract().path("data._id");

		return userId;
	}

	public String randomizedString() {
		String str = RandomStringUtils.randomNumeric(9);
		return str;
	}

}
