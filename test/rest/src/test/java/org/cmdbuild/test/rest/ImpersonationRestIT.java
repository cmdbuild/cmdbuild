package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.jayway.restassured.http.ContentType;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class ImpersonationRestIT extends AbstractWsIT {

	@Test
	public void testImpersonateUser() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();
		String userToImpersonate = "mdavis";

		long userId = createUser(token, jsonAsMap, true, false);

		String sessionId = given().contentType(ContentType.JSON)
				.body("{\"username\" : \"" + jsonAsMap.get("username") + "\", \"password\" : \""
						+ jsonAsMap.get("password") + "\"}")
				.post(buildRestV3Url("sessions?scope=ui")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token)
				.put(buildRestV3Url("sessions/" + sessionId + "/impersonate/" + userToImpersonate)).then()
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when()
				.delete(buildRestV3Url("sessions/" + sessionId + "/impersonate/")).then().statusCode(200);
	}

	public long createUser(String token, Map<String, Object> jsonAsMap, boolean active, boolean service) {

		JSONObject json1 = new JSONObject();
		json1.put("_id", 677);
		json1.put("username", "Helpdesk");
		json1.put("description", "Helpdesk");

		JSONObject json2 = new JSONObject();
		json2.put("_id", 940);
		json2.put("username", "ChangeManager");
		json2.put("description", "ChangeManager");

		JSONObject[] jsonArray = { json1, json2 };

		jsonAsMap.put("username", "wwhite" + randomizedString());
		jsonAsMap.put("description", "White Walter");
		jsonAsMap.put("email", "walter.white" + randomizedString() + "@example.com");
		jsonAsMap.put("active", active);
		jsonAsMap.put("service", service);
		jsonAsMap.put("password", "wwhite");
		jsonAsMap.put("userGroups", jsonArray);

		long userId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("users")).then().statusCode(200).extract().path("data._id");

		return userId;
	}

	public String randomizedString() {
		String str = RandomStringUtils.randomNumeric(6);
		return str;
	}
}