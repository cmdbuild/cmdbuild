package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.util.HashMap;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.jayway.restassured.http.ContentType;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class SystemConfigRestIT extends AbstractWsIT {

	@Test
	public void getSystemConfig() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("system/config")).then()
				.statusCode(200);
	}

	@Test
	public void getSystemConfigDetailed() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).queryParam("detailed", true)
				.get(buildRestV3Url("system/config")).then().log().body().statusCode(200);
	}

	@Test
	public void getSystemConfigBykey() {
		String key = "org.cmdbuild.workflow.provider";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("system/config/" + key)).then()
				.statusCode(200);
	}

	@Test
	public void putTextPlainBykey() {
		String key = "org.cmdbuild.workflow.provider";

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType(ContentType.TEXT).body("something")
				.put(buildRestV3Url("system/config/" + key)).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).queryParam("detailed", true)
				.get(buildRestV3Url("system/config/" + key)).then().body("data", equalTo("something")).statusCode(200);
	}

	@Test
	public void putMany() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType(ContentType.TEXT).body("something")
				.put(buildRestV3Url("system/config/_MANY")).then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).queryParam("detailed", true)
				.get(buildRestV3Url("system/config")).then().body("data._MANY.value", equalTo("something"))
				.statusCode(200);
	}

}
