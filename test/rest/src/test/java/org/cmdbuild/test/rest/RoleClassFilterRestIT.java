package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.anyOf;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.json.Json;

import org.apache.commons.lang3.RandomStringUtils;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class RoleClassFilterRestIT extends AbstractWsIT {

	@Test
	public void getRoleFilters() {
		String token = getSessionToken();

		List<Object> allRolesIds = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("roles")).then()
				.statusCode(200).extract().jsonPath().getList("data._id");

		Object roleId = allRolesIds.get(new Random().nextInt(allRolesIds.size()));

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("roles/" + roleId + "/filters")).then()
				.statusCode(200);
	}

	@Test
	public void postNewRoleClassFilter() {
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		ArrayList<Object> lista = new ArrayList<Object>();
		
		JSONObject json1 = new JSONObject();
		json1.put("_id", 942);
		
		JSONObject json2 = new JSONObject();
		json2.put("_id", 941);
		
		JSONObject json3 = new JSONObject();
		json3.put("_id", 100000005327L);
		
		lista.add(json1);
		lista.add(json2);
		lista.add(json3);

		long roleId = createRole(token, jsonAsMap);

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(lista)
				.post(buildRestV3Url("roles/" + roleId + "/filters")).then().statusCode(200);
	}
	
	public long createRole(String token, Map<String, Object> jsonAsMap) {
		jsonAsMap.put("name", "Specialist" + randomizedString());
		jsonAsMap.put("type", "default");
		jsonAsMap.put("description", "Specialist " + randomizedString());
		jsonAsMap.put("active", true);

		long roleId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("roles")).then().statusCode(200).extract().path("data._id");

		return roleId;
	}

	public String randomizedString() {
		String str = RandomStringUtils.randomNumeric(3);
		return str;
	}
}
