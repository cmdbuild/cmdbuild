package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class BootRestIT extends AbstractWsIT {

	@Test
	public void getBootStatus() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("boot/status")).then()
				.body("status", equalTo("READY")).statusCode(200);
	}

	@Test
	@Ignore("TODO: fix this test (auth issue with sprign security)")
	public void testConnection() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).formParam("host", "localhost").formParam("port", 5432)
				.formParam("user", "postgres").formParam("password", "postgres")
				.post(buildRestV3Url("boot/database/test")).then().statusCode(200);
	}

	@Test
	@Ignore("TODO: fix this test (auth issue with sprign security)")
	public void getBootPatches() {
		given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("boot/patches")).then()
				.statusCode(200);
	}
}
