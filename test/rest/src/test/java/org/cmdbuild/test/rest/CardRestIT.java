package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import static org.cmdbuild.test.rest.TestContextProviders.R2U;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class CardRestIT extends AbstractWsIT {

	@Test
	public void testCards() {
		List<String> allClasses = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/")).then().extract().jsonPath().getList("data._id");

		for (String string : allClasses) {
			given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("classes/" + string + "/cards"))
					.then().statusCode(200);
			logger.info(string);
		}
	}

	@Test
	public void testCardDetail() {
		String _id = "Building";
		int cardId = 73;

		given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/" + _id + "/cards/" + cardId)).then()
				.body("data.Address", equalTo("Liverpool Street 18")).statusCode(200);
	}

	@Test
	public void testPostDeleteEmployee() {
		String _id = "Employee";
		String token = getSessionToken();
		long cardId = createEmployee(token);
		
		logger.info("{}", cardId);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().body("data.Description", equalTo("Blue Robert")).body("data.Level", equalTo(23))
				.body("data.Email", equalTo("robert.blue@example.com")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(404);
	}

	@Test
	public void testCardPostPutDelete() {
		String token = getSessionToken();
		String _id = "Employee";
		long cardId = createEmployee(token);

		logger.info("{}", cardId);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().body("data.Code", equalTo("87")).body("data.Email", equalTo("robert.blue@example.com"))
				.body("data.Name", equalTo("Robert")).body("data.Phone", equalTo("65432"))
				.body("data.Surname", equalTo("Blue")).body("data.Qualification", equalTo(22)).statusCode(200);

		updateEmployee(token, cardId);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().body("data.Code", equalTo("92")).body("data.Email", equalTo("roberto.grigi@example.com"))
				.body("data.Name", equalTo("Roberto")).body("data.Phone", equalTo("6765756"))
				.body("data.Surname", equalTo("Grigi")).body("data.Qualification", equalTo(147)).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);
	}

	@Test
	public void testWrongGet() {
		int cardId = 125;
		List<String> allClasses = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/")).then().extract().jsonPath().getList("data._id");

		for (String string : allClasses) {
			given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
					.get(buildRestV3Url("classes/" + string + "/cards/" + cardId)).then()
					.body("success", equalTo(false)).statusCode(404);
		}
	}

	@Test
	public void testPostDeleteBuilding() {
		String _id = "Building";
		String token = getSessionToken();
		long cardId = createBuilding(token);
		logger.info("{}", cardId);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().body("data.Address", equalTo("Main Streen 77")).body("data.City", equalTo("London"))
				.body("data.Code", equalTo("AB")).body("data.Country", equalTo(25))
				.body("data.Description", equalTo("Office")).body("data.ZIP", equalTo("58213")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(404);
	}

	@Test
	public void testPrototype() {
		List<String> allClasses = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/")).then().extract().jsonPath()
				.getList("data.findAll{ it.prototype == true}._id ");

		for (String string : allClasses) {
			given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).get(buildRestV3Url("classes/" + string + "/cards"))
					.then().assertThat().statusCode(200);
			logger.info(string);
		}
	}

	@Test
	public void testDataNetworkDevice() {
		String _id = "NetworkDevice";
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("AcceptanceDate", "5566"); // errore voluto nella data
		jsonAsMap.put("Assignee", 130);
		jsonAsMap.put("Brand", 137);
		jsonAsMap.put("Code", "ND0677");

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().log().body().statusCode(500);

		jsonAsMap.put("AcceptanceDate", "12-09-2013"); // data formattata al contrario

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().log().body().statusCode(500);

		jsonAsMap.put("AcceptanceDate", "2013/09/12"); // formattazione diversa della data

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().log().body().statusCode(500);

		jsonAsMap.put("AcceptanceDate", "20130912"); // formattazione diversa della data

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().log().body().statusCode(500);

		jsonAsMap.put("AcceptanceDate", "2013-09-12"); // data corretta

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().statusCode(200).extract().path("data._id");

		jsonAsMap.put("AcceptanceDate", "2000-13-89"); // numeri fuori range vengono modularizzati

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.put(buildRestV3Url("classes/" + _id + "/cards/" + cardId)).then().statusCode(200);

		String data = (String) given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + _id + "/cards")).then().extract().jsonPath()
				.getList("data.findAll{ it.Code == 'ND0677'}.AcceptanceDate ").get(0);

		logger.info("Data sbagliata: " + jsonAsMap.get("AcceptanceDate").toString());
		logger.info("Data modularizzata: " + data);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);

	}

	@Test
	public void testNumberFieldsNetworkDevice() {
		String _id = "NetworkDevice";
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Assignee", "aaa"); // stringa di caratteri non accettata
		jsonAsMap.put("Brand", "aaa"); // stringa di caratteri non accettata

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().statusCode(500);

		jsonAsMap.put("Assignee", 130);
		jsonAsMap.put("Brand", 137);
		jsonAsMap.put("Code", "ND0677");

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);
	}

	@Test
	public void testPostDeleteFloor() {
		String _id = "Floor";
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Building", 64);
		jsonAsMap.put("Code", "AB67");

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().body("data.Building", equalTo(64)).body("data.Code", equalTo("AB67")).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(404);

	}

	@Test
	public void testPostDeleteInvoice() {
		String _id = "Invoice";
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Code", "ARARM66");
		jsonAsMap.put("Description", "fffff");

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().body("data.Description", equalTo("fffff")).body("data.Code", equalTo("ARARM66"))
				.statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).when().get(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(404);
	}

	@Test
	public void testIPFieldPC() {
		String _id = "PC";
		String token = getSessionToken();
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Assignee", 120);
		jsonAsMap.put("Brand", 138);
		jsonAsMap.put("CPUNumber", 2);
		jsonAsMap.put("Code", 677);
		jsonAsMap.put("IPAddress", "19216821"); // indirizzo IP non valido
		jsonAsMap.put("RAM", 8);

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().statusCode(500);

		jsonAsMap.put("IPAddress", "192.168.2.1"); // indirizzo IP valido

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/" + _id + "/cards")).then().statusCode(200).extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/" + _id + "/cards/" + cardId))
				.then().statusCode(200);
	}

	@Test
	public void testTotal() {
		String _id = "Supplier";
		String token = getSessionToken();
		int totalValue = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("classes/" + _id + "/cards"))
				.then().extract().jsonPath().getInt("meta.total");

		logger.info("Numero di oggetti: {}", totalValue);

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("classes/" + _id + "/cards")).then()
				.body("data.size()", equalTo(totalValue)).statusCode(200);
	}

	@Test
	public void testTypeClass() {
		String token = getSessionToken();
		String _id = "Printer";

		List<Integer> allCards = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + _id + "/cards")).then().extract().jsonPath().getList("data._id");

		logger.info("Ci sono " + allCards.size() + " elementi");

		for (Integer a : allCards) {
			given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("classes/" + _id + "/cards/" + a)).then()
					.body("data._type", equalTo(_id)).statusCode(200);
		}

	}

	@Test
	public void testStartLimitSort() {
		String token = getSessionToken();
		String name = "Employee";
		int start = 0;
		int limit = 7;

		JSONArray jArray = new JSONArray();
		JSONObject json = new JSONObject();
		json.put("property", "Code");
		json.put("direction", "ASC");
		jArray.put(json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", start).queryParam("limit", limit)
				.queryParam("sort", jArray).when().get(buildRestV3Url("classes/" + name + "/cards")).then()
				.statusCode(200);
	}

	@Test
	public void testCardsSize() {
		List<String> allClasses = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
				.get(buildRestV3Url("classes/")).then().extract().jsonPath().getList("data._id");

		for (String string : allClasses) {
			int v = given().header(CMDBUILD_AUTH_HEADER, getSessionToken())
					.get(buildRestV3Url("classes/" + string + "/cards")).then().extract().jsonPath()
					.getInt("data.size()");
			logger.info(string + ": " + v);
		}
	}

	@Test
	public void testSort() {
		String token = getSessionToken();
		String name = "Room";

		JSONArray jArray = new JSONArray();
		JSONObject json = new JSONObject();
		json.put("property", "Code");
		json.put("direction", "ASC");
		jArray.put(json);

		List<Integer> unSortedList = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + name + "/cards")).then().extract().jsonPath().getList("data.Code");

		List<Integer> sortedListASC = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("sort", jArray)
				.get(buildRestV3Url("classes/" + name + "/cards")).then().extract().jsonPath().getList("data.Code");

		json.put("direction", "DESC");
		jArray.put(json);

		List<Integer> sortedListDESC = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("sort", jArray)
				.get(buildRestV3Url("classes/" + name + "/cards")).then().extract().jsonPath().getList("data.Code");

		logger.info("Lista disordinata: {}", unSortedList);
		logger.info("Lista ordinata (crescente): {}", sortedListASC);
		logger.info("Lista ordinata (decrescente): {}", sortedListDESC);
	}

	@Test
	public void testStart() {
		String token = getSessionToken();
		String name = "Monitor";
		int start = 2;

		int total = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("classes/" + name + "/cards")).then()
				.extract().jsonPath().getInt("data.size()");

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("start", start).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().body("data.size()", equalTo(total - start))
				.statusCode(200);

	}

	@Test
	public void testLimit() {
		String token = getSessionToken();
		String name = "Class";
		int limit = 3;

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("limit", limit).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().body("data.size()", equalTo(limit))
				.statusCode(200);
	}

	@Test
	public void testFilter() {
		String token = getSessionToken();
		String name = "Room";

		String attribute = "Code";
		String operator = "equal";
		String[] codes = { "B103001" };

		JSONObject json = createJson(attribute, operator, codes);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().body("data.Code", hasItem("B103001"))
				.statusCode(200);
	}

	@Test
	public void testFilterAnd() {
		String token = getSessionToken();

		String name = "Employee";
		int[] codes = { 108 };
		String[] surnames = { "Johnson" };
		JSONObject json = createFilterAndJson(codes, surnames);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().body("data.Surname", hasItem("Johnson"))
				.body("data.Name", hasItem("Mary")).body("data.Office", hasItem(108)).statusCode(200);

	}

	@Test
	public void testFilterAndOr() {
		String token = getSessionToken();
		String name = "Monitor";

		String[] codes = { "MON0003" };
		String[] serialNumbers = { "PRT576" };
		int[] finalCosts = { 178 };
		int[] greaterThan = { 600 };
		JSONObject json = createFilterAndOrJson(codes, serialNumbers, finalCosts, greaterThan);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.statusCode(200);
	}

	@Test
	public void testFilterEqual() {
		String token = getSessionToken();
		String name = "NetworkDevice";

		long _id = createNetworkDevice(token);

		LocalDate data = LocalDate.of(2012, Month.OCTOBER, 12); // valutazione equal con tipo data

		String attribute = "AcceptanceDate";
		String operator = "equal";
		String[] date = { data.toString() };

		JSONObject json1 = createJson(attribute, operator, date);

		logger.info("Query 1: {}", json1);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json1).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().body("data.AcceptanceDate", hasItem(date[0]))
				.statusCode(200);

		String attribute2 = "Supplier";
		String operator2 = "equal";
		int[] supplier = { 723 }; // valutazione equal con tipo num

		JSONObject json2 = createJson(attribute2, operator2, supplier);

		logger.info("Query 2: {}", json2);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json2).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().body("data.Supplier", hasItem(supplier[0]))
				.statusCode(200);

		String attribute3 = "SerialNumber";
		String operator3 = "equal";
		String[] serialNumber = { "YRTU87" }; // valutazione equal con tipo string

		JSONObject json3 = createJson(attribute3, operator3, serialNumber);

		logger.info("Query 3: {}", json3);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json3).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.SerialNumber", hasItem(serialNumber[0])).statusCode(200);

		deleteNetworkDevice(token, _id);

	}

	@Test
	public void testFilterNotEqual() {
		String token = getSessionToken();
		String name = "NetworkDevice";

		LocalDate data = LocalDate.of(2011, Month.SEPTEMBER, 13);

		String attribute = "AcceptanceDate";
		String operator = "notequal";
		String[] date = { data.toString() };

		JSONObject json = createJson(attribute, operator, date);

		logger.info("Query 1: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.AcceptanceDate", not(hasItem(date[0]))).statusCode(200);

		String attribute2 = "Assignee";
		String operator2 = "notequal";
		int[] assignee = { 118 };

		JSONObject json2 = createJson(attribute2, operator2, assignee);

		logger.info("Query 2: {}", json2);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json2).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Assignee", not(hasItem(assignee[0]))).statusCode(200);

		String attribute3 = "SerialNumber";
		String operator3 = "notequal";
		String[] serialNumber = { "YFGE87" };

		JSONObject json3 = createJson(attribute3, operator3, serialNumber);

		logger.info("Query 3: {}", json3);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json3).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.SerialNumber", not(hasItem(serialNumber[0]))).statusCode(200);
	}

	@Test
	public void testFilterNull() {
		String token = getSessionToken();
		String name = "Floor";

		String attribute = "Notes";
		String operator = "isnull";
		String[] emptyArr = {};

		JSONObject json = createJson(attribute, operator, emptyArr);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().body("data.Notes", everyItem(nullValue()))
				.statusCode(200);
	}

	@Test
	public void testFilterNotNull() {
		String token = getSessionToken();
		String name = "Floor";

		String attribute = "Notes";
		String operator = "isnotnull";
		String[] emptyArr = {};

		JSONObject json = createJson(attribute, operator, emptyArr);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Notes", everyItem(not(nullValue()))).statusCode(200);
	}

	@Test
	public void testFilterGreater() {
		String token = getSessionToken();
		String name = "Room";

		String attribute = "Code";
		String operator = "greater";
		String[] code = { "5000" }; // test stringa

		JSONObject json = createJson(attribute, operator, code);

		logger.info("Query 1: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Code", everyItem(greaterThan(code[0]))).statusCode(200);

		String attribute2 = "Surface";
		String operator2 = "greater";
		float[] surface = { 30 }; // test num

		JSONObject json2 = createJson(attribute2, operator2, surface);

		logger.info("Query 2: {}", json2);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json2).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Surface", everyItem(greaterThan(surface[0]))).statusCode(200);

		name = "NetworkDevice";
		LocalDate data = LocalDate.of(2011, Month.SEPTEMBER, 13); // test data

		String attribute3 = "AcceptanceDate";
		String operator3 = "greater";
		String[] date = { data.toString() };

		JSONObject json3 = createJson(attribute3, operator3, date);

		logger.info("Query 3: {}", json3);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json3).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.AcceptanceDate", everyItem(greaterThan(date[0]))).statusCode(200);
	}

	@Test
	public void testFilterLess() {
		String token = getSessionToken();
		String name = "Room";

		String attribute = "Code";
		String operator = "less";
		String[] code = { "5000" }; // test string less

		JSONObject json = createJson(attribute, operator, code);

		logger.info("Query 1: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Code", everyItem(lessThan(code[0]))).statusCode(200);

		String attribute2 = "Surface";
		String operator2 = "less";
		float[] surface = { 30 }; // test num less

		JSONObject json2 = createJson(attribute2, operator2, surface);

		logger.info("Query 2: {}", json2);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json2).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Surface", everyItem(lessThan(surface[0]))).statusCode(200);
		name = "NetworkDevice";
		LocalDate data = LocalDate.of(2011, Month.SEPTEMBER, 13); // test data less

		String attribute3 = "AcceptanceDate";
		String operator3 = "less";
		String[] date = { data.toString() };

		JSONObject json3 = createJson(attribute3, operator3, date);

		logger.info("Query 3: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json3).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.AcceptanceDate", everyItem(lessThan(date[0]))).statusCode(200);
	}

	@Test
	public void testFilterIn() {
		String token = getSessionToken();
		String name = "Employee";

		String attribute = "Email";
		String operator = "in";
		String[] emails = { "michael.davis@example.com", "mary.johnson@example.com" };

		JSONObject json = createJson(attribute, operator, emails);

		logger.info("Query 1: {}", json);

		for (int i = 0; i < emails.length; i++) {
			given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
					.get(buildRestV3Url("classes/" + name + "/cards")).then().log().body()
					.body("data.Email", hasItem(emails[i])).statusCode(200);
		}

		String attribute2 = "Office";
		String operator2 = "in";
		int[] office = { 112, 110 }; // test num in

		JSONObject json2 = createJson(attribute2, operator2, office);

		logger.info("Query 2: {}", json2);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json2).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Office", hasItems(office[0], office[1])).statusCode(200);
	}

	@Test
	public void testFilterBetween() {
		String token = getSessionToken();
		String name = "Employee";
		String attribute = "Office";
		String operator = "between";
		int[] offices = { 110, 112 }; // test num between

		JSONObject json = createJson(attribute, operator, offices);

		logger.info("Query 1: {}", json);

		List<String> requestedOffices = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().statusCode(200).and().extract().jsonPath()
				.getList("data.Office");
		logger.info("Uffici richiesti: " + requestedOffices.toString());

		name = "NetworkDevice";
		LocalDate data1 = LocalDate.of(2010, Month.NOVEMBER, 30); // test data between
		LocalDate data2 = LocalDate.of(2012, Month.FEBRUARY, 15);

		String attribute2 = "AcceptanceDate";
		String operator2 = "between";
		String[] date = { data1.toString(), data2.toString() };

		JSONObject json2 = createJson(attribute2, operator2, date);

		logger.info("Query 2: {}", json2);

		List<String> requestedDates = given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json2).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then().statusCode(200).and().extract().jsonPath()
				.getList("data.AcceptanceDate");
		logger.info("Date richieste: " + requestedDates.toString());
	}

	@Test
	public void testFilterLike() {
		String token = getSessionToken();
		String name = "Room";

		String attribute = "Description";
		String operator = "like";
		String[] desc = { "Data Center" };

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query : {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Description", everyItem(containsString(desc[0]))).statusCode(200);
	}

	@Test
	public void testFilterContain() {
		String token = getSessionToken();
		String name = "Room";

		String attribute = "Description";
		String operator = "contain";
		String[] desc = { "Data Center" };

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query : {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Description", everyItem(containsString(desc[0]))).statusCode(200);
	}

	@Test
	public void testFilterNotContain() {
		String token = getSessionToken();
		String name = "Room";

		String attribute = "Description";
		String operator = "notcontain";
		String[] desc = { "dddd" };

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query : {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Description", everyItem(not(containsString(desc[0])))).statusCode(200);
	}

	@Test
	public void testFilterBegin() {
		String token = getSessionToken();
		String name = "Room";

		String attribute = "Description";
		String operator = "begin";
		String[] desc = { "Off" };

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Description", everyItem(containsString(desc[0]))).statusCode(200);
	}

	@Test
	public void testFilterNotBegin() {
		String token = getSessionToken();
		String name = "Employee";

		String attribute = "Surname";
		String operator = "notbegin";
		String[] desc = { "De" };

		JSONObject json = createJson(attribute, operator, desc);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Surname", everyItem(not(startsWith(desc[0])))).statusCode(200);
	}

	@Test
	public void testFilterEnd() {
		String token = getSessionToken();
		String name = "Employee";

		String attribute = "Email";
		String operator = "end";
		String[] email = { ".com" };

		JSONObject json = createJson(attribute, operator, email);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Email", everyItem(endsWith(email[0]))).statusCode(200);
	}

	@Test
	public void testFilterNotEnd() {
		String token = getSessionToken();
		String name = "Employee";

		String attribute = "Phone";
		String operator = "notend";
		String[] phone = { "564" };

		JSONObject json = createJson(attribute, operator, phone);

		logger.info("Query: {}", json);

		given().header(CMDBUILD_AUTH_HEADER, token).queryParam("filter", json).when()
				.get(buildRestV3Url("classes/" + name + "/cards")).then()
				.body("data.Phone", everyItem(not(endsWith(phone[0])))).statusCode(200);
	}

	public JSONObject createJson(String attribute, String operator, Object value) {
		JSONObject json = new JSONObject();
		json.put("attribute", attribute);
		json.put("operator", operator);
		json.put("value", value);

		JSONObject json2 = new JSONObject();
		json2.put("simple", json);

		JSONObject json3 = new JSONObject();
		json3.put("attribute", json2);
		return json3;
	}

	public JSONObject createFilterAndJson(Object codes, Object surnames) {
		JSONObject json = new JSONObject();
		json.put("attribute", "Office");
		json.put("operator", "equal");
		json.put("value", codes);

		JSONObject json2 = new JSONObject();
		json2.put("attribute", "Surname");
		json2.put("operator", "equal");
		json2.put("value", surnames);

		JSONObject jsonsimple1 = new JSONObject();
		jsonsimple1.put("simple", json);

		JSONObject jsonsimple2 = new JSONObject();
		jsonsimple2.put("simple", json2);

		JSONArray and = new JSONArray();
		and.put(jsonsimple1);
		and.put(jsonsimple2);

		JSONObject json3 = new JSONObject();
		json3.put("and", and);

		JSONObject jsontotal = new JSONObject();
		jsontotal.put("attribute", json3);
		return jsontotal;
	}

	public JSONObject createFilterAndOrJson(String[] codes, String[] serialNumbers, int[] finalCosts,
			int[] greaterThan) {
		JSONObject json = new JSONObject();
		json.put("attribute", "Code");
		json.put("operator", "equal");
		json.put("value", codes);

		JSONObject json2 = new JSONObject();
		json2.put("attribute", "SerialNumber");
		json2.put("operator", "equal");
		json2.put("value", serialNumbers);

		JSONObject json3 = new JSONObject();
		json3.put("attribute", "FinalCost");
		json3.put("operator", "equal");
		json3.put("value", finalCosts);

		JSONObject json4 = new JSONObject();
		json4.put("attribute", "FinalCost");
		json4.put("operator", "greater");
		json4.put("value", greaterThan);

		JSONObject jsonsimple1 = new JSONObject();
		jsonsimple1.put("simple", json);

		JSONObject jsonsimple2 = new JSONObject();
		jsonsimple2.put("simple", json2);

		JSONObject jsonsimple3 = new JSONObject();
		jsonsimple3.put("simple", json3);

		JSONObject jsonsimple4 = new JSONObject();
		jsonsimple4.put("simple", json4);

		JSONArray or1 = new JSONArray();
		or1.put(jsonsimple1);
		or1.put(jsonsimple2);

		JSONObject jsonOr1 = new JSONObject();
		jsonOr1.put("or", or1);

		JSONArray or2 = new JSONArray();
		or2.put(jsonsimple3);
		or2.put(jsonsimple4);

		JSONObject jsonOr2 = new JSONObject();
		jsonOr2.put("or", or2);

		JSONArray and = new JSONArray();
		and.put(jsonOr1);
		and.put(jsonOr2);

		JSONObject jsonAnd = new JSONObject();
		jsonAnd.put("and", and);

		JSONObject jsonTotal = new JSONObject();
		jsonTotal.put("attribute", jsonAnd);
		return jsonTotal;
	}

	public long createEmployee(String token) {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Code", "87");
		jsonAsMap.put("Email", "robert.blue@example.com");
		jsonAsMap.put("Level", 23);
		jsonAsMap.put("Mobile", "6886794");
		jsonAsMap.put("Office", 110);
		jsonAsMap.put("Name", "Robert");
		jsonAsMap.put("Phone", "65432");
		jsonAsMap.put("Qualification", 22);
		jsonAsMap.put("State", 156);
		jsonAsMap.put("Surname", "Blue");
		jsonAsMap.put("Type", 149);
		
		 

		long id = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/Employee/cards")).then().statusCode(200).extract().path("data._id");

		return id;
	}

	public long createBuilding(String token) {
		Map<String, Object> jsonAsMap = new HashMap<>();
		jsonAsMap.put("Address", "Main Streen 77");
		jsonAsMap.put("City", "London");
		jsonAsMap.put("Code", "AB");
		jsonAsMap.put("Country", 25);
		jsonAsMap.put("Description", "Office");
		jsonAsMap.put("ZIP", "58213");

		long cardId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/Building/cards")).then().statusCode(200).extract().path("data._id");

		return cardId;
	}

	public void updateEmployee(String token, long cardId) {
		Map<String, Object> updatedFields = new HashMap<>();

		updatedFields.put("Code", "92");
		updatedFields.put("Email", "roberto.grigi@example.com");
		updatedFields.put("Name", "Roberto");
		updatedFields.put("Phone", "6765756");
		updatedFields.put("Surname", "Grigi");
		updatedFields.put("Qualification", 147);

		given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(updatedFields).when()
				.put(buildRestV3Url("classes/Employee/cards/" + cardId)).then().statusCode(200);

	}

	public long createNetworkDevice(String token) {
		Map<String, Object> jsonAsMap = new HashMap<>();

		jsonAsMap.put("Code", "ND0532");
		jsonAsMap.put("Description", "Switch Panel CISCO Catalyst 3750 S.N. YRTU87");
		jsonAsMap.put("SerialNumeber", "YRTU87");
		jsonAsMap.put("PurchaseDate", "2012-09-22");
		jsonAsMap.put("AcceptanceDate", "2012-10-12");
		jsonAsMap.put("Model", "Catalyst 3750");
		jsonAsMap.put("Assignee", 130);

		long id = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json").body(jsonAsMap).when()
				.post(buildRestV3Url("classes/NetworkDevice/cards")).then().statusCode(200).extract().path("data._id");

		return id;
	}

	public void deleteNetworkDevice(String token, long id) {

		given().header(CMDBUILD_AUTH_HEADER, token).when().delete(buildRestV3Url("classes/NetworkDevice/cards/" + id))
				.then().statusCode(200);
	}

}
