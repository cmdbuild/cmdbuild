package org.cmdbuild.test.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.isOneOf;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import static org.cmdbuild.test.rest.TestContextProviders.DEMO;
import org.cmdbuild.test.rest.utils.AbstractWsIT;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context(DEMO)
public class LockRestIT extends AbstractWsIT {

	@Test
	public void getLocks() {
		String token = getSessionToken();

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("locks")).then().statusCode(200);
	}

	@Test
	public void postDeleteNewLock() {
		String classId = "Room";
		String token = getSessionToken();

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		Object cardId = allCardsIds.get(new Random().nextInt(allCardsIds.size()));

		String lockId = given().header(CMDBUILD_AUTH_HEADER, token).contentType("application/json")
				.post(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/lock")).then().statusCode(200)
				.extract().path("data._id");

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("locks/" + lockId)).then()
				.body("data.sessionId", equalTo(token)).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).delete(buildRestV3Url("locks/" + lockId)).then().statusCode(200);
	}

	@Test
	public void deleteAllLocks() {
		String classId = "PC";
		String token = getSessionToken();

		List<Object> allCardsIds = given().header(CMDBUILD_AUTH_HEADER, token)
				.get(buildRestV3Url("classes/" + classId + "/cards")).then().statusCode(200).extract().jsonPath()
				.getList("data._id");

		int locksTotali = given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("locks")).then()
				.statusCode(200).extract().path("meta.total");

		for (Object cardId : allCardsIds) {
			given().header(CMDBUILD_AUTH_HEADER, getSessionToken()).contentType("application/json")
					.post(buildRestV3Url("classes/" + classId + "/cards/" + cardId + "/lock")).then().statusCode(200);
		}

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("locks")).then()
				.body("meta.total", equalTo(locksTotali + allCardsIds.size())).statusCode(200);

		given().header(CMDBUILD_AUTH_HEADER, token).delete(buildRestV3Url("locks/_ALL")).then().statusCode(isOneOf(200,204));

		given().header(CMDBUILD_AUTH_HEADER, token).get(buildRestV3Url("locks")).then().body("meta.total", equalTo(0))
				.statusCode(200);

	}
}
