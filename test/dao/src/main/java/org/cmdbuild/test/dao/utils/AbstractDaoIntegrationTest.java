package org.cmdbuild.test.dao.utils;

import org.cmdbuild.spring.utils.SpringContextInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Before;
import org.cmdbuild.dao.driver.PostgresService;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.view.DataView;

/**
 * Class containing methods for initializing the integration tests database
 */
public abstract class AbstractDaoIntegrationTest {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private static int i = 0;

	@Before
	public void setupTest() throws Exception {
		i++;
	}

	/**
	 * return test unique id (to prefix names and stuff)
	 *
	 * @return
	 */
	protected String tuid() {
		return "_" + Integer.toString(i, 32);
	}

	/**
	 * append test unique id
	 *
	 * @param id
	 * @return
	 */
	protected String tuid(String id) {
		return id + tuid();
	}

//	protected SpringContextInitializer getSpringContextInitializer() {
//		return TestDatabaseBuilderRunListener.getSpringContextInitializer();
//	}

//	protected PostgresService getDriver() {
//		return getSpringContextInitializer().getBean(PostgresService.class);
//	}
//
//	protected DataView getDataView() {
//		return getSpringContextInitializer().getBean(DataView.class);
//	}
//
//	protected DaoService getDao() {
//		return getSpringContextInitializer().getBean(DaoService.class);
//	}
//
//	protected <T> T getBean(Class<T> beanClass) {
//		return getSpringContextInitializer().getBean(beanClass);
//	}

}
