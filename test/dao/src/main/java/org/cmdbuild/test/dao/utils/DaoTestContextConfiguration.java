package org.cmdbuild.test.dao.utils;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.eventbus.EventBus;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.trimToNull;
import org.cmdbuild.auth.multitenant.config.MultitenantConfiguration;
import static org.cmdbuild.auth.multitenant.config.SimpleMultitenantConfiguration.multitenantDisabled;
import org.cmdbuild.config.api.ConfigReloadEventImpl;
import static org.cmdbuild.config.levelone.DatabaseConfigurationImpl.DEFAULT_DB_DRIVER_CLASS_NAME;
import static org.cmdbuild.config.leveltwo.SqlConfigurationImpl.DEFAULT_EXCLUDE_REGEXP;
import static org.cmdbuild.config.leveltwo.SqlConfigurationImpl.SQL_CONFIGURATION;
import org.cmdbuild.dao.config.DatabaseConfiguration;
import org.cmdbuild.dao.config.SqlConfiguration;

import org.cmdbuild.dao.config.inner.FilesStoreRepository;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.dao.config.inner.DatabaseCreatorConfigImpl;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempDir;
import static org.cmdbuild.utils.io.CmdbuildZipUtils.unzipToDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.cmdbuild.dao.config.inner.PatchFileRepository;
import org.cmdbuild.dao.config.inner.PatchManager;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;

@ImportResource({"classpath:test-dao-application-context.xml"})
public class DaoTestContextConfiguration implements ApplicationContextAware {

	public static final String MULTITENANT_CONFIGURATION = "multitenantConfiguration";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DatabaseCreator databaseConfigurator;
	private final File tempDir;
	private final EventBus sqlConfigEventBus = new EventBus();

	private ApplicationContext applicationContext;
	private boolean enableSqlLogging = false;

	public DaoTestContextConfiguration() {
		tempDir = tempDir("sql_sources_for_test_");
		databaseConfigurator = new DatabaseCreator(DatabaseCreatorConfigImpl.builder()
				.withDatabaseType(getDatabaseType())
				.withConfig(getClass().getResourceAsStream("/database.properties"))
				.accept((dc) -> {

					String databaseUrlFromSystem = Optional.fromNullable(trimToNull(System.getProperty("cmdbuild.test.database"))).or(Optional.fromNullable(trimToNull(System.getenv("CMDBUILD_TEST_DATABASE")))).orNull();
					if (!isBlank(databaseUrlFromSystem)) {
						dc.withDatabaseUrl(databaseUrlFromSystem);
					}
				})
				.withDatabaseName("cmdbuild_test_" + UUID.randomUUID().toString().substring(0, 6))
				.withSqlPath(new File(tempDir, "sql").getAbsolutePath()).build());
	}

	public String getDatabaseType() {
		return DatabaseCreator.EMPTY_DATABASE;
	}

//	public void setDatabaseType(String dbType) {
//		databaseConfigurator.setConfig(copyOf(databaseConfigurator.getConfig()).withDatabaseType(dbType).build());
//	}
	public void createDb() throws IOException, ZipException {
		logger.info("create database BEGIN");
		unzipToDir(getClass().getResourceAsStream("/cmdbuild-dao-sql.zip"), tempDir);
		checkArgument(new File(tempDir, "sql/sample_schemas/demo_schema.sql").isFile());
		databaseConfigurator.configureDatabase();
	}

	public void init() throws IOException, ZipException {
		createDb();
		logger.info("apply patches");
		applicationContext.getBean(PatchManager.class).reloadAndApplyPendingPatches(PatchManager.PatchApplyStrategy.SINGLE_BATCH);
		enableSqlLogging = true;
		sqlConfigEventBus.post(new ConfigReloadEventImpl());
		logger.info("create database END");
	}

	public void cleanup() {
		if (applicationContext != null) {
			((ConfigurableApplicationContext) applicationContext).close();
		}
		FileUtils.deleteQuietly(tempDir);
		logger.info("dropping database");
		if (databaseConfigurator.cmdbuildDatabaseExists()) {
			databaseConfigurator.dropDatabase();
			logger.info("database dropped");
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Bean
	public PatchFileRepository getPatchRepository() {
		return new FilesStoreRepository(new File(tempDir, "patches/"), null);
	}

	@Bean
	public DatabaseCreator getDatabaseConfigurator() {
		return databaseConfigurator;
	}

	@Bean(MULTITENANT_CONFIGURATION)
	public MultitenantConfiguration getMultitenantConfiguration() {
		return multitenantDisabled();
	}

	@Bean("databaseConfiguration")
	public DatabaseConfiguration getDatabaseConfiguration() {
		return new DatabaseConfiguration() {

			private final EventBus eventBus = new EventBus();

			@Override
			public String getDatabaseUrl() {
				return databaseConfigurator.getConfig().getDatabaseUrl();
			}

			@Override
			public String getDatabaseUser() {
				return databaseConfigurator.getConfig().getCmdbuildUser();
			}

			@Override
			public String getDatabasePassword() {
				return databaseConfigurator.getConfig().getCmdbuildPassword();
			}

			@Override
			public String getDatabaseAdminUsername() {
				return null;
			}

			@Override
			public String getDatabaseAdminPassword() {
				return null;
			}

			@Override
			public String getDriverClassName() {
				return DEFAULT_DB_DRIVER_CLASS_NAME;
			}

			@Override
			public EventBus getEventBus() {
				return eventBus;
			}

			@Override
			public boolean enableAutoPatch() {
				return false;
			}

			@Override
			public boolean enableDatabaseConnectionEagerCheck() {
				return true;
			}

		};

//				new DatabaseConfigurationImpl(databaseConfigurator.getConfig().getDatabaseUrl(), databaseConfigurator.getConfig().getCmdbuildUser(), databaseConfigurator.getConfig().getCmdbuildPassword(), false);
	}

	@Bean(SQL_CONFIGURATION)
	public SqlConfiguration getSqlConfiguration() {
		return new SqlConfiguration() {
			@Override
			public EventBus getEventBus() {
				return sqlConfigEventBus;
			}

			@Override
			public boolean enableSqlLogging() {
				return enableSqlLogging;
			}

			@Override
			public boolean enableSqlLoggingTimeTracking() {
				return false;
			}

			@Override
			public String excludeSqlRegex() {
				return DEFAULT_EXCLUDE_REGEXP;
			}

			@Override
			public boolean enableDdlLogging() {
				return false;
			}

			@Override
			public String includeDdlRegex() {
				return null;
			}
		};
	}

}
