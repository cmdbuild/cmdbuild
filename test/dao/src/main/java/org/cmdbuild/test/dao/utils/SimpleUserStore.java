package org.cmdbuild.test.dao.utils;

import javax.annotation.Nullable;
import org.cmdbuild.auth.user.OperationUser;
import static org.cmdbuild.auth.user.OperationUserImpl.anonymousOperationUser;
import org.cmdbuild.auth.user.OperationUserStore;

public class SimpleUserStore implements OperationUserStore {

	private OperationUser operationUser;

	public SimpleUserStore() {
		this(anonymousOperationUser());
	}

	public SimpleUserStore(@Nullable OperationUser preset) {
		this.operationUser = preset;
	}

	@Override
	public OperationUser getUser() {
		return operationUser;
	}

	@Override
	public void setUser(@Nullable OperationUser value) {
		operationUser = value;
	}

}
