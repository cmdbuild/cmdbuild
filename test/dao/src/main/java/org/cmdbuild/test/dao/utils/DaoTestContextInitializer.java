/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.dao.utils;

import org.cmdbuild.spring.utils.SpringContextInitializer;

/**
 *
 */
public class DaoTestContextInitializer extends SpringContextInitializer {

	public DaoTestContextInitializer(Object config) {
		super(config);
	}

	@Override
	protected void doInit() throws Exception {
		super.doInit();
		logger.info("configure, load db");
		getBean(DaoTestContextConfiguration.class).init();
	}

	@Override
	protected void doCleanup() {
		getBean(DaoTestContextConfiguration.class).cleanup();
		super.doCleanup();
	}

}
