/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.dao.utils;

import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.springframework.context.annotation.ImportResource;

@ImportResource({"classpath:test-dao-application-context.xml"})
public class DaoTestContextConfigurationR2u extends DaoTestContextConfiguration {

	@Override
	public String getDatabaseType() {
		return DatabaseCreator.READY_2_USE_DATABASE;
	}

}
