package org.cmdbuild.test.dao;

import com.google.common.base.Joiner;
import java.sql.ResultSet;
import static java.util.Arrays.asList;
import org.cmdbuild.auth.user.OperationUserStore;
import org.cmdbuild.auth.multitenant.UserTenantContextImpl;
import org.cmdbuild.auth.multitenant.config.MultitenantConfiguration;
import org.cmdbuild.auth.multitenant.config.SimpleMultitenantConfiguration;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.auth.user.OperationUserImpl;
import org.cmdbuild.dao.driver.DatabaseAccessConfig;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import org.springframework.jdbc.core.JdbcTemplate;
import org.cmdbuild.spring.utils.SpringContextInitializer;
import org.cmdbuild.test.dao.utils.DaoTestContextConfiguration;
import static org.cmdbuild.test.dao.utils.DaoTestContextConfiguration.MULTITENANT_CONFIGURATION;
import org.cmdbuild.test.dao.utils.DaoTestContextInitializer;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.cmdbuild.dao.driver.PostgresService;

public class SqlConnectionIT {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private SpringContextInitializer springContextInitializer;

	@Before
	public void init() throws Exception {
		springContextInitializer = new DaoTestContextInitializer(asList(DaoTestContextConfiguration.class, CustomConfig.class));
		springContextInitializer.init();
	}

	@Test
	public void testSqlConnection() throws Exception {
		logger.info("testConnection BEGIN");

		assertTrue(springContextInitializer.getBean(MultitenantConfiguration.class).isMultitenantEnabled());
		assertTrue(springContextInitializer.getBean(DatabaseAccessConfig.class).getMultitenantConfiguration().isMultitenantEnabled());

		UserTenantContextImpl userTenantContext = new UserTenantContextImpl(false, asList(10l, 20l, 30l));
		OperationUser operationUser = OperationUserImpl.builder().withUserTenantContext(userTenantContext).build();
		springContextInitializer.getBean(OperationUserStore.class).setUser(operationUser);

		logger.info("set operation user with tenants ids = {}", springContextInitializer.getBean(DatabaseAccessConfig.class).getUserContext().getTenantIds());

		assertFalse(springContextInitializer.getBean(DatabaseAccessConfig.class).getUserContext().ignoreTenantPolicies());
		assertFalse(springContextInitializer.getBean(DatabaseAccessConfig.class).getUserContext().getTenantIds().isEmpty());

		PostgresService dbDriver = springContextInitializer.getBean(PostgresService.class);
		JdbcTemplate jdbcTemplate = dbDriver.getJdbcTemplate();
		logger.info("JdbcTemplate query BEGIN");
		String res = jdbcTemplate.query("SHOW cmdbuild.user_tenants", (ResultSet rs) -> {
			logger.info("extracting query result");
			assertTrue(rs.next());
			return rs.getString(1);
		});
		logger.info("res = {}", res);
		assertFalse(userTenantContext.getActiveTenantIds().isEmpty());
		assertEquals("{" + Joiner.on(",").join(userTenantContext.getActiveTenantIds()) + "}", res);
		logger.info("JdbcTemplate query END");

		logger.info("testConnection END");
	}

	@Configuration
	public static class CustomConfig {

		@Bean(MULTITENANT_CONFIGURATION)
		public MultitenantConfiguration getMultitenantConfiguration() {
			return new SimpleMultitenantConfiguration(MultitenantConfiguration.MultitenantMode.DB_FUNCTION);
		}

	}

	@After
	public void cleanup() {
		springContextInitializer.cleanup();
	}

}
