/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.dao;

import static java.util.Collections.emptyMap;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.cmdbuild.test.dao.utils.TestContext;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
@Context("dummy")
public class DummyIT {

	public static TestContext dummy() {
		return new TestContext(emptyMap());//TODO add params, test
	}

	@Test
	public void testDummy() {
	}

}
