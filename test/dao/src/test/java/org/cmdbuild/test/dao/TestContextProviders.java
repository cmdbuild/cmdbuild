/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cmdbuild.test.dao;

import org.cmdbuild.test.dao.utils.ContextProvider;
import org.cmdbuild.test.dao.utils.DaoTestContextConfiguration;
import org.cmdbuild.test.dao.utils.DaoTestContextConfigurationR2u;
import org.cmdbuild.test.dao.utils.DaoTestContextInitializer;
import org.cmdbuild.test.dao.utils.TestContext;

@ContextProvider
public class TestContextProviders {

	@ContextProvider("emptyDb")
	public static TestContext getEmptyDb() {
		DaoTestContextInitializer databaseInitializer = new DaoTestContextInitializer(DaoTestContextConfiguration.class);
		databaseInitializer.init();
		return new TestContext(databaseInitializer, (c) -> databaseInitializer.cleanup());
	}

	public static TestContext r2u() {
		DaoTestContextInitializer databaseInitializer = new DaoTestContextInitializer(DaoTestContextConfigurationR2u.class);
		databaseInitializer.init();
		return new TestContext(databaseInitializer, (c) -> databaseInitializer.cleanup());
	}
}
