/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.dao;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.core.q3.ResultRow;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
public class DaoIT {

	private final DaoService dao;

	public DaoIT(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@Test
	@Context("r2u")
	public void testDao() {
		List<ResultRow> rows = dao.selectAll().from("User").run();
		checkArgument(!rows.isEmpty());
	}

	@Test
	@Context("r2u")
	public void testDao2() {
		List<ResultRow> rows = dao.selectAll().from("Role").run();
		checkArgument(!rows.isEmpty());
	}

	@Test
	@Context("emptyDb")
	public void testDao3() {
		List<ResultRow> rows = dao.selectAll().from("User").run();
		checkArgument(rows.isEmpty());
	}

	@Test
	public void testDao4() {
		List<Classe> list = dao.getAllClasses();
		checkArgument(!list.isEmpty());
	}

	@Test
	@Context({"emptyDb", "r2u"})
	public void testDao5() {
		List<Classe> list = dao.getAllClasses();
		checkArgument(!list.isEmpty());
	}

}
