/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.dao;

import static java.util.Collections.singletonList;
import org.cmdbuild.test.dao.utils.DaoTestContextInitializer;
import org.cmdbuild.test.dao.utils.DaoTestContextConfiguration;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
public class DaoSpringContextIT {

	private DaoTestContextInitializer databaseInitializer;

	@Test
	public void testSpringContext() throws Exception {
		databaseInitializer = new DaoTestContextInitializer(singletonList(DaoTestContextConfiguration.class));
		databaseInitializer.init();
		//do something
		databaseInitializer.cleanup();
	}

	@After
	public void cleanup() {
		databaseInitializer.cleanup();
	}
}
