/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.dao;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.core.q3.ResultRow;
import org.cmdbuild.test.dao.utils.Context;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
public class DaoQueryIT {

	private final DaoService dao;

	public DaoQueryIT(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@Test
	@Context("r2u")
	public void testDao() {
		List<ResultRow> rows = dao.selectAll().from("User").run();
		checkArgument(!rows.isEmpty());
	}

}
