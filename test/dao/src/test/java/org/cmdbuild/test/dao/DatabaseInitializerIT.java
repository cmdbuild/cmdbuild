/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.dao;

import static com.google.common.base.Preconditions.checkArgument;
import java.util.UUID;
import org.cmdbuild.dao.config.inner.DatabaseCreator;
import static org.cmdbuild.dao.config.inner.DatabaseCreatorConfigImpl.copyOf;
import org.junit.After;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.test.dao.utils.DaoTestContextConfiguration;
import org.cmdbuild.test.dao.utils.MyRunnerForDaoTest;
import org.junit.runner.RunWith;

@RunWith(MyRunnerForDaoTest.class)
public class DatabaseInitializerIT {//TODO test also patches

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private DaoTestContextConfiguration dbInitializer;

	@Test
	public void testEmptyDatabase() throws Exception {
		dbInitializer = new DaoTestContextConfiguration();
		dbInitializer.getDatabaseConfigurator().setConfig(copyOf(dbInitializer.getDatabaseConfigurator().getConfig())
				.withDatabaseType(DatabaseCreator.EMPTY_DATABASE)
				.withDatabaseName("cmdbuild_test_" + UUID.randomUUID().toString().substring(0, 6))
				.build());
		dbInitializer.createDb();
		checkArgument(dbInitializer.getDatabaseConfigurator().cmdbuildDatabaseExists());
	}

	@Test
	public void testDemoDatabase() throws Exception {
		dbInitializer = new DaoTestContextConfiguration();
		dbInitializer.getDatabaseConfigurator().setConfig(copyOf(dbInitializer.getDatabaseConfigurator().getConfig())
				.withDatabaseType(DatabaseCreator.DEMO_DATABASE)
				.withDatabaseName("cmdbuild_test_" + UUID.randomUUID().toString().substring(0, 6))
				.build());
		dbInitializer.createDb();
		checkArgument(dbInitializer.getDatabaseConfigurator().cmdbuildDatabaseExists());
	}

	@Test
	public void testDemoDumpDatabase() throws Exception {
		dbInitializer = new DaoTestContextConfiguration();
		dbInitializer.getDatabaseConfigurator().setConfig(copyOf(dbInitializer.getDatabaseConfigurator().getConfig())
				.withDatabaseType("demo.dump.xz")
				.withDatabaseName("cmdbuild_test_" + UUID.randomUUID().toString().substring(0, 6))
				.build());
		dbInitializer.createDb();
		checkArgument(dbInitializer.getDatabaseConfigurator().cmdbuildDatabaseExists());
	}

	@After
	public void cleanup() {
		if (dbInitializer != null) {
			try {
				dbInitializer.cleanup();
			} catch (Exception ex) {
				logger.error("error dropping database", ex);
			}
			dbInitializer = null;
		}
	}
}
