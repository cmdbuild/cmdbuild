package org.cmdbuild.test.web;

import static org.cmdbuild.test.web.utils.ExtjsUtils.sendKeys2ExtjsInput;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cmdbuild.test.web.utils.BaseWebIT;
import org.cmdbuild.test.web.utils.ExtjsUtils;
import org.cmdbuild.test.web.utils.WebUILoginCredentials;
import static org.cmdbuild.utils.tomcatmanager.TomcatManagerUtils.sleepSafe;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Contains All tests about login
 *
 */
public class LoginIT extends BaseWebIT {

	@Override
	protected boolean passesDefaultClientLogsChecks() {
		return fetchClientLog()
				.supress("404").supress("401")
				.supress("Use of the Application Cache is deprecated on insecure origins")
				.supress("This page includes a password or credit card input in a non-secure context")
				.printLogs(logger, null)
				.success();
	}

	@Test
	public void loginTest() {

		testStart();
		getUrl(getBaseUrlUI());
		WebUILoginCredentials adminCredentials = WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN);

		fillCredentials(adminCredentials);
		selectFirstlanguage();
		submitLogin();

		if (secondLoginStepTriggered()) {
			performStandardSecondStep();
		}

		assertTrue(loggedIn());
		checkClient();
		testEnd();

	}

	@Test
	public void loginTest2() { //just check a different user (normal login over user known to have role/tenant choices

		testStart();
		getUrl(getBaseUrlUI());
		WebUILoginCredentials adminCredentials = defaultDemoUserCredentialsInstance();

		fillCredentials(adminCredentials);
		selectFirstlanguage();
		submitLogin();

		if (secondLoginStepTriggered()) {
			performStandardSecondStep();
		}

		assertTrue(loggedIn());
		checkClient();
		testEnd();

	}

	@Test
	public void logoutTest() {

		testStart();
		login(defaultAdminCredentialsInstance());

		WebElement logoutDiv = findElementByXPath("//div[@data-testid='header-logout']");
		logoutDiv.click();
		waitForLoad();
		String signedOutUrl = getDriver().getCurrentUrl();
		logger.info("Sign out landed @ {}", signedOutUrl);
		Assert.assertTrue(signedOutUrl.contains("#login"));
		Assert.assertFalse(signedOutUrl.contains("/cmdbuild/ui/#classes"));
		checkClient();
		testEnd();

	}

	@Test
	public void wrongPasswordTest() {

		testStart();
		getUrl(getBaseUrlUI());

		WebUILoginCredentials credentials = defaultAdminCredentialsInstance();
		credentials.withPassword(credentials.getPassword() + "WrongPassword");

		fillCredentials(credentials);
		selectFirstlanguage();
		submitLogin();

		if (secondLoginStepTriggered()) {
			performStandardSecondStep();
		}

		assertTrue(loginFailed());
		checkClient();
		testEnd();
	}

	@Test
	public void userNotExistingLoginMustFailTest() {

		testStart();
		getUrl(getBaseUrlUI());

		WebUILoginCredentials credentials = defaultAdminCredentialsInstance();
		credentials.withUserName(credentials.getUserName() + "DoesNotExists");

		fillCredentials(credentials);
		selectFirstlanguage();
		submitLogin();

		if (secondLoginStepTriggered()) {
			performStandardSecondStep();
		}

		assertTrue(loginFailed());
		checkClient();
		testEnd();
	}

	@Test
	public void existingUserCaseMispelledLoginMustFailIfCaseSensitiveTest() {

		logger.info("Forcing case sensitive auth ");
		String previousValue = setCmdbuildSystemConfig("org.cmdbuild.auth.case.insensitive", "false");
		logger.info("Case insensitive auth is set to {}", getCmdbuildSystemConfig("org.cmdbuild.auth.case.insensitive"));

		testStart();
		getUrl(getBaseUrlUI());

		WebUILoginCredentials credentials = defaultAdminCredentialsInstance();
		assertFalse(credentials.getUserName().equals(credentials.getUserName().toUpperCase()));
		credentials.withUserName(credentials.getUserName().toUpperCase());

		fillCredentials(credentials);
		selectFirstlanguage();
		submitLogin();

		if (secondLoginStepTriggered()) {
			performStandardSecondStep();
		}

		assertTrue(loginFailed());
		checkClient();
		testEnd();
		setCmdbuildSystemConfig("org.cmdbuild.auth.case.insensitive", previousValue);
	}

	@Test
	public void existingUserCaseMispelledLoginMustSucceedIfCaseInsensitiveTest() {

		logger.info("Forcing case insensitive auth ");
		setCmdbuildSystemConfig("org.cmdbuild.auth.case.insensitive", "true");
		logger.info("Case insensitive auth is set to {}", getCmdbuildSystemConfig("org.cmdbuild.auth.case.insensitive"));

		testStart();
		getUrl(getBaseUrlUI());

		WebUILoginCredentials credentials = defaultAdminCredentialsInstance();
		assertFalse(credentials.getUserName().equals(credentials.getUserName().toUpperCase()));
		credentials.withUserName(credentials.getUserName().toUpperCase());

		fillCredentials(credentials);
		selectFirstlanguage();
		submitLogin();

		if (secondLoginStepTriggered()) {
			performStandardSecondStep();
		}

		assertTrue(loggedIn());
		checkClient();
		testEnd();
	}

	/**
	 * Test simple login case with user known to be presented Role or Tenant choice (2 steps)
	 */
	@Test
	public void login2steps() {

		testStart();
		getUrl(getBaseUrlUI());
		WebUILoginCredentials adminCredentials = defaultDemoUserCredentialsInstance();

		fillCredentials(adminCredentials);
		selectFirstlanguage();
		submitLogin();

		waitForLoad();

		assertTrue(secondLoginStepTriggered());
		assertFalse(loggedIn());

		selectFirstRoleIfAny();
		selectFirstTenantIfAny();
		submitLogin();

		assertTrue(loggedIn());
		checkClient();
		testEnd();
	}

	/**
	 * Test simple login case with user known NOT to be presented Role or Tenant choice (single step)
	 * TODO: pay attention: this test must be skipped if no user without tenant/role choice is present
	 */
	@Test
	public void login1step() {

		testStart();
		getUrl(getBaseUrlUI());
		WebUILoginCredentials adminCredentials = defaultAdminCredentialsInstance();

		fillCredentials(adminCredentials);
		selectFirstlanguage();
		submitLogin();

		waitForLoad();

		assertFalse(secondLoginStepTriggered());
		assertTrue(loggedIn());

		assertTrue(loggedIn());
		checkClient();
		testEnd();
	}

	protected void fillCredentials(WebUILoginCredentials credentials) {

		WebElement username = findElementByTestId("login-inputusername");
		sendKeys2ExtjsInput(username, credentials.getUserName());
		WebElement password = findElementByTestId("login-inputpassword");
		sendKeys2ExtjsInput(password, credentials.getPassword());

	}

	protected void submitLogin() {

		WebElement loginButton = findElementByTestId("login-btnlogin");
		loginButton.click();
		waitForLoad();
	}

	protected boolean loggedIn() {

		waitForLoad();
		String landingUrl = getDriver().getCurrentUrl();
		logger.info("Login landed @ {}", landingUrl);
		boolean success =  !landingUrl.contains("#login") && landingUrl.matches(".*/cmdbuild/ui/#(classes|management).*");
		if(!success && isInteractiveMode()){//TODO always wait after a test fail in interactive mode
			logger.warn("login FAILED!");
			sleepSafe(10000);
		}
		return success;
	}

	//TODO we also need to be sure we are not in second step of login
	protected boolean loginFailed() {

		waitForLoad();
		String landingUrl = getDriver().getCurrentUrl();
		logger.info("Login landed @ {}", landingUrl);

		return (landingUrl.contains("#login")) && (!(landingUrl.contains("/cmdbuild/ui/#classes") || landingUrl.contains("/cmdbuild/ui/#management")));
	}

	protected boolean secondLoginStepTriggered() {
		//several implementations are possible and possibly better of this one
		waitForLoad();
		String landingUrlFirstStep = getDriver().getCurrentUrl();
		List<WebElement> userNameStillPresent = getDriver().findElements(By.xpath("//div[@data-testid='login-inputusername']"));
		if (!userNameStillPresent.isEmpty() && landingUrlFirstStep.contains("#login")
				&& (ExtjsUtils.isDropDownActive(tenantDropDownLocator, getDriver()) || ExtjsUtils.isDropDownActive(roleDropDownLocator, getDriver()))) {
			return true;
		}
		return false;

	}

	protected void performStandardSecondStep() {
		selectFirstRoleIfAny();
		selectFirstTenantIfAny();
		submitLogin();
	}

	protected void selectFirstlanguage() {

		By languageDivCriteria = By.xpath("//div[@data-testid='login-inputlanguage']");
		ExtjsUtils.clickOnDropdownOption(languageDivCriteria, null, getDriver(), true);
	}

	private void selectFirstRoleIfAny() {
		if (ExtjsUtils.isDropDownActive(roleDropDownLocator, getDriver())) {
			ExtjsUtils.clickOnDropdownOption(roleDropDownLocator, null, getDriver(), true);
		}
	}

	private void selectFirstTenantIfAny() {
		if (ExtjsUtils.isDropDownActive(tenantDropDownLocator, getDriver())) {
			ExtjsUtils.clickOnDropdownOption(tenantDropDownLocator, null, getDriver(), true);
		}
	}

}
