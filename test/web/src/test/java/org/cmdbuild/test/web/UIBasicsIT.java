package org.cmdbuild.test.web;

import static org.cmdbuild.test.web.utils.ExtjsUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
//import org.cmdbuild.test.web.utils.AbstractWebIT;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

//import org.cmdbuild.dao.config.services.DatabaseCreator;
import org.cmdbuild.test.web.utils.ExtjsCardGrid;
import org.cmdbuild.test.web.utils.ExtjsCardGrid.Cell;
import org.cmdbuild.test.web.utils.WebUILoginCredentials;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntry;
import com.google.common.base.Strings;
import org.cmdbuild.test.web.utils.BaseWebIT;

/**
 * These tests have to be run against the "ready2use" database
 *
 */
public class UIBasicsIT extends BaseWebIT {

	//TODO check. Promote to qualified test or delete
	//@Test //@Ignore
	public void experimentalTest() throws Exception {

		logger.info("WebUITest: Experimental START");
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());

		WebElement navigationContainer = findElementByTestId("management-navigation-container");
		List<WebElement> navItems = navigationContainer.findElements(By.tagName("li"));
		for (WebElement navItem : navItems) {
			WebElement textWrapper = navItem.findElement(By.className("x-treelist-item-text"));
			if (textWrapper != null && !Strings.isNullOrEmpty(textWrapper.getText())) {
				logger.info("Navigation Item found: " + textWrapper.getText());
			}
			try {
				sleep(300);
				WebElement expander = navItem.findElement(By.className("x-treelist-item-expander"));
				if (expander != null && expander.isEnabled()) {
					expander.click();
				}
			} catch (Exception e) {
				logger.error("problem with nav item...");
			}

		}
		logger.info("WebUITest: Experimental STOP");

		close();

	}

	@Test
	public void uiNavigatesToProperCardGridOnSidebarNavigationInteractionTest() throws Exception {

		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		waitForLoad();

		//sleep(1000);//TODO replace with proper waiting strategy, this is mostly here for human check
		assertTrue(clickOnSideNavLeaf(getDriver(), "Locations", "Floors"));
		assertTrue(getCurrentUrl().contains("#classes/Floor/cards"));
		waitForLoad();
		WebElement content = findElementById("CMDBuildManagementContent");
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards floor"));
		waitForLoad();
		assertTrue(clickOnSideNavLeaf(getDriver(), "Software", "Databases"));
		waitForLoad();
		assertTrue(getCurrentUrl().contains("#classes/Database/cards"));
		content = findElementById("CMDBuildManagementContent");
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards database"));

		//close();
	}

	@Test
	public void checkSelectedCardsAreShownTest() throws Exception {

		testStart();
//		logger.info("WebUITest: checkSomeCardsAreShown START");

		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		sleepDeprecated();//TODO replace with proper waiting strategy, this is mostly here for human check
		assertTrue(clickOnSideNavLeaf(getDriver(), "Locations", "Floors"));
		sleepDeprecated(1000);
		assertTrue(getCurrentUrl().contains("#classes/Floor/cards"));
		WebElement content = findElementById("CMDBuildManagementContent");
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards floor"));
		//sleep();

		ExtjsCardGrid grid = getCardGrid(getDriver());

		assertEquals(3, (grid.getRowsContainingAnyCells(new Cell("Building", "Aon Center"))).size());
		assertEquals(2, (grid.getRowsContainingAnyCells(new Cell("Code", "F01"))).size());
		assertEquals(2, (grid.getRowsContainingAnyCells(new Cell("Level", "00"))).size());
		assertEquals(1, (grid.getRowsContainingAnyCells(new Cell("Description", "AC Aon Center - 01"))).size());
		assertEquals((grid.getRowsContainingAllCells(new Cell("Description", "AC Aon Center - 01"))).size(), (grid.getRowsContainingAnyCells(new Cell("Description", "AC Aon Center - 01"))).size());
		assertNotEquals(
				(grid.getRowsContainingAllCells(new Cell("Building", "Aon Center"), new Cell("Code", "F00"))).size(),
				(grid.getRowsContainingAnyCells(new Cell("Building", "Aon Center"), new Cell("Code", "F00"))).size()
		);

		sleep();//TODO replace with proper waiting strategy, this is mostly here for human check
		assertTrue(safelylickOnSideNavLeaf(getDriver(), "Locations", "Buildings"));
		waitForLoad();
		sleepDeprecated(1000);
		assertTrue(getCurrentUrl().contains("#classes/Building/cards"));
		sleep();
		content = getDriver().findElement(By.id("CMDBuildManagementContent"));
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards building"));
		sleep();

		grid = getCardGrid(getDriver());

		assertEquals(0, (grid.getRowsContainingAllCells(new Cell("Building", "Aon Center"))).size());
		assertEquals(1, (grid.getRowsContainingAllCells(
				new Cell("Code", "AC"),
				new Cell("Description", "Aon Center"),
				new Cell("City", "Chicago")
		)).size());
		assertEquals(0, (grid.getRowsContainingAllCells(
				new Cell("Code", "LMT"),
				new Cell("Description", "Aon Center"),
				new Cell("City", "Chicago")
		)).size());
		assertEquals(2, (grid.getIndexOfRowsCointainingAllCells(
				new Cell("Country", "United States of America")
		)).size());
		assertEquals((grid.getIndexOfRowsCointainingAllCells( //test for ExtjsCardGrid
				new Cell("City", "Baltimora")
		)).size(),
				(grid.getRowsContainingAllCells(
						new Cell("City", "Baltimora")
				)).size());

		//printAllLogEntries(getDriver());
		//printLogEntries(getDriver(), Level.WARNING);
		//printLogEntries(getDriver(), Level.WARNING , "404" , "not found" , "warning");
		List<LogEntry> logs = fetchBrowserLogEntries(getDriver());

		//demo about logs
		printLogs(filterLogEntries(logs, Level.WARNING));
		printLogs(filterLogEntries(logs, Level.WARNING, "404"));
		printLogs(filterLogEntries(logs, Level.WARNING, "404"), Level.WARNING, "404");

		testEnd();
		//logger.info("WebUITest: checkSomeCardsAreShown STOP");
		//close();

	}

	@Test //TODO: when possible (now limited by a bug, only few classes are searcheable) add more cases // refactoring...
	//FIXME: free this test from sleep(x) dependency
	//		@2018-03-19 bug with many classes (perhaps those on right side of 1..N relation) so test is written for "root" class)
	public void cardsSearchTest() {

		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());

		assertTrue(clickOnSideNavLeaf(getDriver(), "Suppliers", "Suppliers"));
		waitForLoad();
		sleep(2000);
		assertTrue(getCurrentUrl().contains("#classes/Supplier/cards"));
		WebElement content = getDriver().findElement(By.id("CMDBuildManagementContent"));
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards supplier"));

		//Search tests from here
		ExtjsCardGrid grid = getCardGrid(getDriver());
		int numberOfCardsShownNoFilter = grid.getRows().size();
		assertEquals(6, numberOfCardsShownNoFilter);
		setQuickSearch4CardsText(getDriver(), "lee", null, null);
		waitForLoad();
		sleep(1000);
		grid = getCardGrid(getDriver());
		assertEquals(1, grid.getRows().size());
		//Changing searchtext content w/o pressing enter should not change displayed cards
		setQuickSearch4CardsText(getDriver(), "Catch nothing filter... ", false, false);
		sleep(1000); //FIXME
		assertEquals(1, getCardGrid(getDriver()).getRows().size());
		//no rows with catch nothing filter
		setQuickSearch4CardsText(getDriver(), "Catch nothing filter... ", true, true);
		sleep(1000); //FIXME
		assertEquals(0, getCardGrid(getDriver()).getRows().size());

		setQuickSearch4CardsText(getDriver(), "ia", true, true);
		waitForLoad();
		sleep(1000);
		assertEquals(3, getCardGrid(getDriver()).getRows().size());
		//check case insensitive and other fields
		setQuickSearch4CardsText(getDriver(), "[KaYbA]", null, null);
		waitForLoad();
		sleep(1000);
		assertEquals(1, getCardGrid(getDriver()).getRows().size());
		//clear should display the initial number of cards
		clearQuickSearch4CardsText(getDriver());
		sleep(1000);
		assertEquals(numberOfCardsShownNoFilter, getCardGrid(getDriver()).getRows().size());
		testEnd();
	}

	//TODO move off topic test section (exception on button not found) to a new class of "meta" tests (tests against test suite)
	//TODO use test-id to locate buttons when it will become possible
	//TODO check console and filters as well
	@Test
	public void cardUpdateTest() {

		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());

		sleep(2000);
		clickOnSideNavLeaf(getDriver(), "Locations", "Rooms"); //beware: fails if with checks version used
		waitForLoad();
		sleep(3000);

		ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver());
		Optional<Cell> cellBeforeEdit = grid.getRows().get(grid.rows() - 1).stream().filter(r -> "Code".equals(r.getFieldName())).findFirst();
		WebElement gridRow = grid.expandGridRow(grid.rows() - 1);
		sleep(1000);
		grid.editCard(gridRow);
		sleep(3000);
		WebElement detailsForm = getManagementDetailsWindow(getDriver());
		logger.info("Field Code * contains: {}", getFormTextFieldContent(detailsForm, "Code *"));
		logger.info("Field Code * contains: {}", getFormTextFieldContent(detailsForm, "Code"));
//		logger.info("Field Code contains: {}" ,getFormTextFieldContent(detailsForm, "Code"));
//		logger.info("Field Use contains: {}" ,getFormTextFieldContent(detailsForm, "Use"));
//		logger.info("Field Floor contains: {}" ,getFormTextFieldContent(detailsForm, "Floor"));

		fillFormTextField(detailsForm, "Code", "T");
		fillFormTextField(detailsForm, "Code *", "T");
		clickDetailFormButton(detailsForm, "Save"); //TODO: collapses row!!
//		clickDetailFormButton(detailsForm, "Cancel"); //TODO: collapses row!!
		sleep(3000);
		Optional<Cell> cellAfterEdit = grid.refresh().getRows().get(grid.rows() - 1).stream().filter(r -> "Code".equals(r.getFieldName())).findFirst();
		assertNotEquals(cellAfterEdit.get().getContent(), cellBeforeEdit.get().getContent());

		gridRow = grid.refresh().expandGridRow(grid.rows() - 1);
		grid.editCard(gridRow); //FIXME: must return detailsFormElement
		detailsForm = getManagementDetailsWindow(getDriver());

		//
//		Optional<WebElement> combo = ExtjsUtils.fetchCombo(detailsForm, "Floor");
		/*
		 * FIXME in forms COMBO (SELECT) OPTIONS ARE INJECT ONLY AFTER ARROW DOWN BUTTON IS TRIGGERED (below is dinamically injected DIV with options
		 * <div id="combo-1239-picker-listWrap" data-ref="listWrap" class="x-boundlist-list-ct x-unselectable x-scroller" style="overflow: auto; height: auto;"><ul id="combo-1239-picker-listEl" data-ref="listEl" class="x-list-plain" role="listbox" aria-hidden="true" aria-disabled="false"><li role="option" unselectable="on" class="x-boundlist-item" tabindex="-1" data-recordindex="0" data-recordid="526" data-boundview="combo-1239-picker">AC Aon Center - 00</li><li role="option" unselectable="on" class="x-boundlist-item" tabindex="-1" data-recordindex="1" data-recordid="527" data-boundview="combo-1239-picker">AC Aon Center - 01</li><li role="option" unselectable="on" class="x-boundlist-item" tabindex="-1" data-recordindex="2" data-recordid="528" data-boundview="combo-1239-picker">AC Aon Center - 02</li><li role="option" unselectable="on" class="x-boundlist-item" tabindex="-1" data-recordindex="3" data-recordid="529" data-boundview="combo-1239-picker">LMT Legg Mason Tower - 00</li><li role="option" unselectable="on" class="x-boundlist-item" tabindex="-1" data-recordindex="4" data-recordid="530" data-boundview="combo-1239-picker">LMT Legg Mason Tower - 01</li><li role="option" unselectable="on" class="x-boundlist-item x-boundlist-item-over x-boundlist-selected" tabindex="-1" data-recordindex="5" data-recordid="531" data-boundview="combo-1239-picker" id="ext-element-1303">LMT Legg Mason Tower - -1</li></ul></div>
		 */
//		selectFirstOptionInExtjsCombo(getDriver(), combo.get());
//		sleep(3000);
		// 
		try {
			sleep();
			clickDetailFormButton(detailsForm, "No button with this caption, this should fail");
			Assert.fail();
		} catch (WebUICMDBuidTestException e) {
			Assert.assertTrue(e.getMessage().contains(WebUICMDBuidTestException.MESSAGE_NO_NAMED_BUTTON_FOUND));
		}
		testEnd();
		sleep(3000);
	}

	//@Test
	public void demoGridContent() throws Exception {

		logger.info("WebUITest: DEVxxx START");

		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		sleep();//TODO replace with proper waiting strategy, this is mostly here for human check
		assertTrue(clickOnSideNavLeaf(getDriver(), "Locations", "Floor"));
		assertTrue(getCurrentUrl().contains("#classes/Floor/cards"));
		sleep();
		WebElement content = getDriver().findElement(By.id("CMDBuildManagementContent"));
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards floor"));
		sleep();

		ExtjsCardGrid grid = getCardGrid(getDriver());

		List<String> gridFields = grid.getFields();
		getClass();
		gridFields.stream().forEach(f -> logger.info("Grid Field: {}", f));

		int r = 0;
		for (List<Cell> row : grid.getRows()) {
			logger.info("\nGrid row {}", ++r);
			row.stream().forEach(cell -> logger.info(" {}", cell));
		}
		sleep(1000);

		logger.info("WebUITest: DECxxx STOP");

		close();

	}

}
