/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.web;

import org.cmdbuild.test.web.utils.AbstractWebIT;
import org.junit.Test;

/**
 *
 */
public class LegacyUiIT extends AbstractWebIT {

	@Test
	public void simpleTest() {
		logger.info("simpleTest BEGIN");

		getUrl(getBaseUrl());

		find("input[name=username]").sendKeys("admin");
		find("input[name=password]").sendKeys("admin");
		find(".x-btn-button").click();
		waitForLoad();

		waitFor(".x-tree-node-text", "Employees");
		waitFor("a[href=\"administration.jsp\"]", "Administration module").click();
		waitForLoad();

		waitFor("a[href=\"management.jsp\"]");
		waitFor(".x-tree-node-text", "Standard");
		waitFor("a[href=\"management.jsp\"]", "Data management module").click();
		waitForLoad();

		waitFor(".x-tree-node-text", "Employees");
		close();

		logger.info("simpleTest END");
	}

}
