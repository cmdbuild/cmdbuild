/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.web.utils;

import static com.google.common.base.Strings.nullToEmpty;
import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;
import java.io.File;
import java.net.URL;
import java.util.Optional;
import org.apache.commons.io.FileUtils;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.cmdbuild.utils.io.CmdbuildFileUtils.tempDir;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class WebDriverManager {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String webDriverParam = trimToNull(System.getProperty("cmdbuild.test.webdriver"));

	private File chromedriver;
	private RemoteWebDriver driver;

	public void init() throws Exception {
		switch (getMode()) {
			case CHROME: {
				chromedriver = new File(tempDir(), "chromedriver");
				FileUtils.copyInputStreamToFile(Thread.currentThread().getContextClassLoader().getResourceAsStream("chromedriver_linux64"), chromedriver);
				chromedriver.setExecutable(true);
				System.setProperty("webdriver.chrome.driver", chromedriver.getAbsolutePath());
				ChromeOptions options = new ChromeOptions();
				driver = new ChromeDriver(options);
			}
			break;
			case REMOTE: {
				logger.info("using remote webdriver = {}", webDriverParam);
				driver = new RemoteWebDriver(new URL(webDriverParam), DesiredCapabilities.chrome());
			}
		}
	}

	public void cleanup() {
		driver.quit();
		driver = null;
		switch (getMode()) {
			case CHROME:
				FileUtils.deleteQuietly(chromedriver.getParentFile());
				chromedriver = null;
				break;
		}
	}

	private Mode getMode() {
		if (nullToEmpty(webDriverParam).startsWith("http://")) {
			return Mode.REMOTE;
		} else {
			return Optional.ofNullable(webDriverParam).map((value) -> Mode.valueOf(value.toUpperCase())).orElse(Mode.CHROME);
		}
	}

	private enum Mode {
		CHROME, REMOTE
	}

	public RemoteWebDriver getDriver() {
		return checkNotNull(driver);
	}

}
