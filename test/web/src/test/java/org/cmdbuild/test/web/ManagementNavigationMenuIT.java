package org.cmdbuild.test.web;
import static org.cmdbuild.test.web.utils.ExtjsUtils.getCardGrid;
import static org.cmdbuild.test.web.utils.ExtjsUtils.safelyClickOnSideNavLeafWithChecksAgainst;
import static org.cmdbuild.test.web.utils.ExtjsUtils.checkCorrectClassIsDisplayed;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.cmdbuild.client.rest.RestClient;
import org.cmdbuild.client.rest.RestClientImpl;
import org.cmdbuild.test.web.utils.BaseWebIT;
import org.cmdbuild.test.web.utils.ExtjsCardGrid;
import org.cmdbuild.test.web.utils.ExtjsCardGrid.Cell;
import org.cmdbuild.test.web.utils.ExtjsNavigationMenu;
import org.cmdbuild.test.web.utils.WebServiceUtilities;
import org.cmdbuild.test.web.utils.WebUILoginCredentials;
import org.cmdbuild.test.web.utils.ExtjsNavigationMenu.MenuItem;
import org.cmdbuild.test.web.utils.ExtjsNavigationMenu.MenuType;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.cmdbuild.client.rest.model.MenuEntry;


public class ManagementNavigationMenuIT extends BaseWebIT {
	
	@Override
	protected boolean passesDefaultClientLogsChecks() {
		return fetchClientLog().supress("404").supress("401").success();
	}
	
	private RestClient restClient;
	private WebUILoginCredentials clientCredentials = WebUILoginCredentials.getDefaultAdminCredentialsInstance();
	
	@Test //TODO: refine, needs more cases tested.
	public void subClassSelectionTest() {
		
		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		
		safelyClickOnSideNavLeafWithChecksAgainst(getDriver(),"Computer", " computer", "Workplaces" , "All computers");
		ExtjsCardGrid grid = getCardGrid(getDriver());
		assertEquals(9 , grid.rows());
		assertEquals( 1 ,(grid.getRowsContainingAnyCells(new Cell("Hostname", "laptop-external-01"))).size());
		
		safelyClickOnSideNavLeafWithChecksAgainst(getDriver(),"Notebook", " notebook", "Workplaces", "Notebooks");
		grid = getCardGrid(getDriver());
		assertEquals(5 , grid.rows());
		assertEquals( 1 ,(grid.getRowsContainingAnyCells(new Cell("Hostname", "laptop-external-01"))).size());
		
		checkClient();
		testEnd();
		
	}
	
	@Test @Ignore //DEV...
	//TODO replace static data with actual menu structure provided by menu ws when possible.
	//pay attention: don't expect first item is a class
	public void firstItemOfNavigationMenuIsSelectedAfterLoginTest() {
		
		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		
		sleepDeprecated(4500); //TODO remove
		
//		assertTrue(getDriver().getCurrentUrl().contains("#classes/Building/cards")); //real result
		assertTrue(getDriver().getCurrentUrl().contains("#classes/Employee/cards"));
		WebElement content = getDriver().findElement(By.id("CMDBuildManagementContent"));
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards employee"));
//		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards building"));
		
		checkClient();
		testEnd();
		
	}
	
	
	/**
	 * It checks side menu full tree compliance with the description of menu delivered by web service call .
	 * Checks also that menu types are correct inferring them from rendered icons
	 */
	@Test 
	//TODO check if "All Elements" resumee depends on configuration [YES BUT IGNORE FOR NOW]
	//TODO write also a case for a user with different tree (which has some hidden classes)
	public void navigationMenuIsShownAccordingCorrectOrderTest() {
		
		testStart();
		login(WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN));
//		login(WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_DEMOUSER));
		
		MenuEntry menuFromWS = getRestClient().menu().getMenu();
		ExtjsNavigationMenu menuFromBrowser = new ExtjsNavigationMenu(getDriver());
		List<MenuItem> firstLevelFromBrowser = menuFromBrowser.fetchFirstLevelNodes();
		List<MenuEntry> firstlevelFromWs = menuFromWS.getChildren();
		
		for (int i = 0; i <  firstlevelFromWs.size(); i++) {
			
			logger.info("Check root menu item #{}" , i);
			MenuItem menuItemFromBrowser = firstLevelFromBrowser.get(i);
			MenuEntry menuItemFromWs = firstlevelFromWs.get(i);
			checkNodeIsCorrectlyShown(menuItemFromBrowser, menuItemFromWs);
		}
		
		checkClient();
		testEnd();
		
	}
	
	
	/**
	 * It is supposed that items in web service node are served according to order defined by index and there is no invisible item in the tree
	 * @param menuBrowser MenuItem of checked node
	 * @param menuWS json node of checked node (from web service)
	 */
	private void checkNodeIsCorrectlyShown(MenuItem menuBrowser, MenuEntry menuWS) {
		
		String wsMenuText = menuWS.getObjectDescriptionOrNull().trim(); //may fail if not trimmed
		String brMenuText = menuBrowser.getText().trim();
		logger.info("Comparing menu item: {} WS: {} - BR: {}" , menuBrowser.getPath() , wsMenuText, brMenuText ); //Loger.debug 
		if (! brMenuText.equalsIgnoreCase(wsMenuText)) {
			logger.warn("Browser menu and Web service menu do not match for {} , WS is [{}], BR is[{}", menuBrowser.getPath(), wsMenuText, brMenuText);
		}
		assertTrue(brMenuText.equalsIgnoreCase(wsMenuText));

		Optional<MenuType> itemMenuType = menuBrowser.getMenuType();
		String wsItemMenuType = menuWS.getMenuType();
		logger.info("Comparing type of menu item: {} WS: {} - BR: {}" , menuBrowser.getPath(), wsItemMenuType, itemMenuType ); //Loger.debug 
		assertTrue(itemMenuType.get().matchesJson(wsItemMenuType));
		logger.info("Menu state: root: {} , leaf: {} , displayed {}" , menuBrowser.isFirstLevel(), menuBrowser.isLeaf(), menuBrowser.isDisplayed());
		
		//Subtree check
		if (! menuBrowser.isLeaf() ) {
			menuBrowser.expandByClick();
			sleepDeprecated(800);
			List<MenuItem> childrenBrowser = menuBrowser.getVisibleChildren();
			List<MenuEntry> childrenWS = menuWS.getChildren();
			logger.info("Menu: {} has {} children. Checking children...", menuBrowser.getPath(), childrenBrowser.size());
			assertEquals(childrenBrowser.size() , childrenWS.size());
			//we suppose that items in ws node are served according to order define in index
			for (int n = 0; n < childrenBrowser.size(); n++) {
				checkNodeIsCorrectlyShown(childrenBrowser.get(n) , childrenWS.get(n) );
			}
		}
	}
	
	
	/**
	 * Selected item after landing from login is configurable, so we cannot assume it's the first<br/>
	 * In first implementation we relax test contraints by supposing that starting item is the first one shown and it is a class, so some cards are shown
	 */
	@Test  //@Ignore //this test is known to fail(feature not yet implemneted in Client)
	public void menuIsVisibleAndFirstItemSelectedTest() {
		
		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		
		sleepDeprecated(1000);
		//MOVE OUT (test configuration)
		int expectedMenuItemsShownAtStartup = 14;
		int minDimensionForWebElementToBeConsideredVisible = 4;
		String expectedSelectedClassName = "Computer";
		String[] expectedSelectedClassSelectionPath = {"Workplaces" , "All computers"};
		Integer expectedSelectedClassCards = 9;
		//known working config as of 2018-03-30
//		String expectedSelectedClassName = "Building";
//		String[] expectedSelectedClassSelectionPath = {"Locations" , "Buildings"};
//		Integer expectedSelectedClassCards = 2;

		
		//1: TODO check menu is visible
		ExtjsNavigationMenu menu = new ExtjsNavigationMenu(getDriver());
		List<MenuItem> l1 = menu.fetchFirstLevelNodes();
		//TODO rethink all constraints
		assertTrue(l1.size() == expectedMenuItemsShownAtStartup);
		assertTrue(l1.stream().allMatch(m -> m.isDisplayed() &&  (!Strings.isNullOrEmpty(m.getText())) 
				&& m.getSize().height > minDimensionForWebElementToBeConsideredVisible && m.getSize().width > minDimensionForWebElementToBeConsideredVisible));
		assertTrue(l1.stream().allMatch(m -> m.isFirstLevel()));
		
		//2: Check that right class is selected

		ExtjsCardGrid gridStartup = getCardGrid(getDriver());
		assertEquals(expectedSelectedClassCards, new Integer(gridStartup.rows()) );
		assertTrue(checkCorrectClassIsDisplayed(getDriver(),"Building"));
		
		safelyClickOnSideNavLeafWithChecksAgainst(getDriver(), expectedSelectedClassName, expectedSelectedClassName.toLowerCase(), expectedSelectedClassSelectionPath);
		sleepDeprecated(2000);
		ExtjsCardGrid gridSelectedClass = getCardGrid(getDriver());
		assertEquals( gridSelectedClass.rows(), gridStartup.rows());
		for (int r = 0; r  < gridStartup.getRows().size(); r++) {
			List<Cell> rowStartup = gridStartup.getRows().get(r);
			List<Cell> rowSelected = gridSelectedClass.getRows().get(r);
			assertEquals(rowStartup.size() , rowSelected.size());
			for (int c = 0; c < rowSelected.size() ; c++ ) {
				assertTrue(rowStartup.get(c).matches(rowSelected.get(c)));
				//logger.debug(rowStartup.get(c).toString());
			}
		}
		
		checkClient();
		testEnd();
		
	}
	

}
