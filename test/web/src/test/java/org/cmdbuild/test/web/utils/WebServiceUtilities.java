package org.cmdbuild.test.web.utils;

import static org.apache.commons.lang3.StringUtils.trimToNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.MoreObjects.firstNonNull;
//import javax.annotation.PostConstruct;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.cmdbuild.client.rest.RestClient;
import org.cmdbuild.client.rest.RestClientImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Class for calling Cmdbuild exposed rest web services, with some convenience method (e.g.:
 * setting / retrieving configuration properties on the fly)
 * 
 *
 */
//@Component
public class WebServiceUtilities {


//	private static RestClient adminClient; //always use getter
//	private static WebUILoginCredentials adminCredentials = WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN);
//	private static LocalDateTime adminClientExpirationTms = LocalDateTime.now();
//	private static long clientTimeoutMinutes = 20;

	public static Logger logger = LoggerFactory.getLogger(WebServiceUtilities.class);

//	public static String getPropertyValue(String property) {
//		return getAdminClient().system().getConfig(property);
//	}
//
////	/**
////	 * @param property
////	 * @param value
////	 * @return previous value if found
////	 */
//	public static String setPropertyValue(String property, String value) {
//
//		String previous = getAdminClient().system().getConfig(property);
//		getAdminClient().system().setConfig(property, value);
//		return previous;
//	}
//
//	public static RestClient getClientInstance(@Nonnull WebUILoginCredentials credentials) {
//
//		serverURL = firstNonNull(serverURL, firstNonNull(System.getProperty("cmdbuild.test.base.url")));
//		RestClient client = RestClientImpl.builder().withServerUrl(serverURL).build();
//		client = client.doLogin(credentials.getUserName(), credentials.getPassword());
//		return client;
//	}
//
//	/**
//	 * 
//	 * @return a client instance logged in with default admin user
//	 * 
//	 * Prefer using specific credentials for each test
//	 */
//	public static RestClient getDefaultAdminClientInstance() {
//
//		RestClient defaultAdminRestClient = RestClientImpl.builder().withServerUrl(
//				firstNonNull(System.getProperty("cmdbuild.test.base.url"), fallbackServerUrl)).build();
//		WebUILoginCredentials credentials = WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN);
//		defaultAdminRestClient = defaultAdminRestClient.doLogin(credentials.getUserName(), credentials.getPassword());
//		return defaultAdminRestClient;
//	}
//
//	private static RestClient getAdminClient() {
//
//		if (LocalDateTime.now().isAfter(adminClientExpirationTms) || adminClient == null) {
//			adminClientExpirationTms = LocalDateTime.now().plusMinutes(clientTimeoutMinutes);
//			adminClient = RestClientImpl.builder().withServerUrl(
//					firstNonNull(System.getProperty("cmdbuild.test.base.url"), fallbackServerUrl)).build();
//			WebUILoginCredentials credentials = WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN);
//			adminClient = adminClient.doLogin(credentials.getUserName(), credentials.getPassword());
//
//		}
//		return adminClient;
//	}

	public static List<JsonObject> toList(JsonArray jsonArray) {

		ArrayList<JsonObject> list = new ArrayList<>(jsonArray.size());
		for (JsonElement jsonElement : jsonArray) {
			list.add(jsonElement.getAsJsonObject());
		}
		return list;
	}

}
