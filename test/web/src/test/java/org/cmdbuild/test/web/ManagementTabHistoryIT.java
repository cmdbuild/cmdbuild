package org.cmdbuild.test.web;

import static org.cmdbuild.test.web.utils.ExtjsUtils.clearFormTextField;
import static org.cmdbuild.test.web.utils.ExtjsUtils.getFormTextFieldContent;
import static org.cmdbuild.test.web.utils.UILocators.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import org.cmdbuild.test.web.utils.BaseWebIT;
import org.cmdbuild.test.web.utils.ExtjsCardGrid;
import org.cmdbuild.test.web.utils.ExtjsCardGrid.Cell;
import org.cmdbuild.test.web.utils.ExtjsUtils;
import org.cmdbuild.test.web.utils.UITestContext;
import org.cmdbuild.test.web.utils.UITestDefaults;
import org.cmdbuild.test.web.utils.UITestRule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

public class ManagementTabHistoryIT extends BaseWebIT {
	
	////TEST PARAMETERS
	protected long tmsToleranceTresholdMillis = 3000L;
	
	//TAB HISTORY test parameters
	protected long testTHDurationMillis =  45 * SECONDS;
	protected List<String> testTHIgnoreClientErrors = ImmutableList.of("401", "404");
	
	////TEST VARIABLES
	//TAB HISTORY test variables
	protected String testTHMainMenu = "Locations";
	protected String testTHSubMenu = "Buildings";
	protected String testTHTargetClass = "Building";
	protected String testTHTargetCardSelectionCriteria1Column = "Code";
	protected String testTHTargetCardSelectionCriteria1Content= "LMT";
	protected String testTHTargetCardSelectionCriteria2Column = "Description";
	protected String testTHTargetCardSelectionCriteria2Content= "Legg Mason Tower";
	protected String testTHTargetCardFieldToChange= "Postcode";
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * All in one test for simplicity's sake! This test:
	 * <ul>
	 * <li> opens card history of a selected element to check how many history versions the card has
	 * <li> modifies the card (thus adding 1 item to history)
	 * <li> checks again the card history counting items
	 * <li> checks date field is correct (TODO specs needed)
	 */
	@SuppressWarnings({  "deprecation" }) //uses date parse because accepts more formats
	@Test
	public void tabHistory() {
		
		UITestContext testContext = testStart();
		testContext.withRule(UITestRule.define("Duration", "Test execution took longer than allowed to complete"
				, c -> c.getMeasuredTimeSpent() < testTHDurationMillis));
		testContext.withRule(UITestRule.defineClientLogCheckRule(null, testTHIgnoreClientErrors, null, null));
		
		login(defaultAdminCredentialsInstance()); //also tested with defaultDemoUserCredentialsInstance
		
		//STEP 1: count current history items
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testTHMainMenu , testTHSubMenu );		
		ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver(), testContext);
		List<Integer> idxs = grid.getIndexOfRowsCointainingAllCells(new Cell(testTHTargetCardSelectionCriteria1Column,testTHTargetCardSelectionCriteria1Content),
				new Cell(testTHTargetCardSelectionCriteria2Column,testTHTargetCardSelectionCriteria2Content));
		assertEquals(1, idxs.size());
		int idx = idxs.get(0);
		WebElement targetRow = grid.expandGridRow(idx);
		WebElement cmdbuildManagementDetailsWindow = grid.openCard(targetRow);
		// open tab view
		WebElement leftTab = cmdbuildManagementDetailsWindow.findElement(By.className("x-tab-bar-default-docked-left"));
		WebElement spanHistory = leftTab.findElement(By.className("fa-history"));
		spanHistory.click();
		WebElement historyGrid = ExtjsUtils.waitForVisibilityOfNestedElement(getDriver(), cardDetailHistoryGrid(), cmdbuildManagementDetailsWindowLocator());
		List<WebElement> historyRows = historyGrid.findElements(By.tagName("table"));
		int cardHistoryItemsBefore = historyRows.size();
		logger.info("Card History Items (before): {}" ,cardHistoryItemsBefore); 
		grid.closeCardDetails();
		waitForLoad();
		//sleepDeprecated(2000);
		//STEP 2: modify the card to increment history items count
		grid.editCard(targetRow);
		
		WebElement detailsForm = ExtjsUtils.getManagementDetailsWindow(getDriver());
		String fieldContent = getFormTextFieldContent(detailsForm, testTHTargetCardFieldToChange);
		logger.info("Field {} contains: {}" ,testTHTargetCardFieldToChange ,fieldContent); 
		clearFormTextField(detailsForm, testTHTargetCardFieldToChange); 
		//sleepDeprecated(800);
		ExtjsUtils.fillFormTextField(detailsForm, testTHTargetCardFieldToChange, "" + (new Random().nextInt(98765)));
		ExtjsUtils.clickDetailFormButton(detailsForm, UITestDefaults.cardEditSaveButtonCaptionDefault);
		
		LocalDateTime cardEditedTimestamp = LocalDateTime.now();
		
		//STEP 3: check history count incremented by 1
		grid.refresh();
		targetRow = grid.expandGridRow(idx);
		cmdbuildManagementDetailsWindow = grid.openCard(targetRow);
		leftTab = cmdbuildManagementDetailsWindow.findElement(By.className("x-tab-bar-default-docked-left"));
		spanHistory = leftTab.findElement(By.className("fa-history"));
		spanHistory.click();
		WebElement historyGridAfter = ExtjsUtils.waitForVisibilityOfNestedElement(getDriver(), cardDetailHistoryGrid(), cmdbuildManagementDetailsWindowLocator());
		List<WebElement> historyRowsAfter = historyGridAfter.findElements(By.tagName("table"));
		int cardHistoryItemsAfter = historyRowsAfter.size();
		logger.info("Card History Items (after): {}" ,cardHistoryItemsAfter); 
		assertEquals(cardHistoryItemsBefore +1, cardHistoryItemsAfter);
		
		
		//STEP 4: check timestamp of last edit is correct
		
		//fetch first and second item
		String lastBeginDateS = historyRowsAfter.get(0).findElements(By.tagName("td")).get(UITestDefaults.historyDetailsColumnBeginDateDefault).getText();
		String lastEndDateS = historyRowsAfter.get(0).findElements(By.tagName("td")).get(UITestDefaults.historyDetailsColumnEndDateDateDefault).getText();
		String previousEndDateS = historyRowsAfter.get(1).findElements(By.tagName("td")).get(UITestDefaults.historyDetailsColumnEndDateDateDefault).getText();
		
		LocalDateTime lastBeginTms = (new java.util.Date(java.util.Date.parse(lastBeginDateS))).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		LocalDateTime previousEndTms = (new java.util.Date(java.util.Date.parse(previousEndDateS))).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				
		logger.info("Card last version begin date: {}" , lastBeginDateS);
		logger.info("Card last version end date: {}" , lastEndDateS);
		logger.info("Card  previous version end date: {}" , lastEndDateS);
		
		logger.debug("DEBUG: check conversion... {}" , lastBeginTms);
		
		assertTrue(Strings.isNullOrEmpty(Strings.nullToEmpty(lastEndDateS).trim()));
		assertInstantsAreEqual(lastBeginTms, previousEndTms, tmsToleranceTresholdMillis);
		assertInstantsAreEqual(cardEditedTimestamp, previousEndTms, tmsToleranceTresholdMillis);
		
		
		sleepDeprecated(2000); //TODO remove
		testEnd(testContext);
	}

	//TODO promote to library??
	private void assertInstantsAreEqual(@Nonnull LocalDateTime instant1, @Nonnull LocalDateTime instant2,
			long tmsToleranceTresholdMillis) {
			
		assertNotNull(instant1);
		assertNotNull(instant2);
		assertTrue( Math.abs(ChronoUnit.MILLIS.between(instant1, instant2)) <= tmsToleranceTresholdMillis);
		
	}
	
	

}
