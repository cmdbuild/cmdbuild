package org.cmdbuild.test.web;

import static org.cmdbuild.test.web.utils.ExtjsUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.cmdbuild.test.web.utils.BaseWebIT;
import org.cmdbuild.test.web.utils.ExtjsCardGrid;
import org.cmdbuild.test.web.utils.ExtjsUtils;
import org.cmdbuild.test.web.utils.FormField;
import org.cmdbuild.test.web.utils.UITestContext;
import org.cmdbuild.test.web.utils.UITestDefaults;
import org.cmdbuild.test.web.utils.UITestRule;
import org.cmdbuild.test.web.utils.ExtjsCardGrid.Cell;
import org.cmdbuild.test.web.utils.WebUILoginCredentials;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.springframework.dao.DataAccessException;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import net.bytebuddy.implementation.bind.annotation.Super;

public class ManagementClassListIT extends BaseWebIT {
	
	//TEST PARAMETERS
	UITestRule defaultCLientLogRule = UITestRule.defineClientLogCheckRule(null, ImmutableList.of("404","401"), null, null);
	//TODO: since some tests (e.g.: saveButtonDisabledWhenLeavingARequiredFieldBlankTest andsaveButtonDisabledWhenLeavingARequiredFieldBlankTestOnNewCard)
	// are known to generate http 400 / 500 errors, following relaxed rule is temporary adopted: 
	UITestRule relaxedCLientLogRule = UITestRule.defineClientLogCheckRule(null, ImmutableList.of("404","401","400","500"), null, null);
	
	//Commons
	String cardEditSaveButtonCaption = UITestDefaults.cardEditSaveButtonCaptionDefault;
	String cardNewSaveButtonCaption = UITestDefaults.cardNewSaveButtonCaptionDefault;
	
	//TEST variables
	// saveButtonDisabledWhenLeavingARequiredFieldBlankTest
	String testSBDMenuClassMain = "Workplaces";
	String testSBDMenuClassSub = "All computers";
	String testSBDCardFieldToModify = "Code";
	// saveButtonDisabledWhenLeavingARequiredFieldBlankTest
	String testSBDNewCardMenuClassMain = "Locations";
	String testSBDNewCardMenuClassSub = "Buildings";
	String testSBDNewCardCardRequiredFieldToLeaveBlank = "Description";
	String testSBDNewCardCardField1ToFill = "City";
	String testSBDNewCardCardField1ToFillContent = "Udine";
	String testSBDNewCardCardField2ToFill = "Code";
	String testSBDNewCardCardField2ToFillContent = "TEST";
	//abortCardCreationTest
	String testAbortCCMenuClassMain = "Workplaces";
	String testAbortCCMenuClassSub = "All computers";
	//createCardTest
	//Integer testAbortCCMenu = "All computers";
	
	String testCreateCardTouchedClass = "VLAN";
	String testCreateCardMenuClassMain = "Networks";
	String testCreateCardMenuClassSub = "VLANs";
	String testCreateCardMenuClassOtherMain = "Workplaces";
	String testCreateCardMenuClassOtherSub = "All computers";
	Integer testCreateCardRandomSuffix = (int)Math.round(Math.random() * 999999);
	//referenceDropDownTest
	String testReferenceDropDownCommonMenuClassMain = "Locations";
	String testReferenceDropDownReferencedClass1MenuSub = "Buildings";
	String testReferenceDropDownReferencedClass2MenuSub = "Floors";
	String testReferenceDropDownReferencerClassMenuSub = "Rooms";
	String testReferenceDropDownDescriptionFieldName = "Description";
	String testReferenceDropDownReference1FieldName = "Building";
	String testReferenceDropDownReference2FieldName = "Floor";
	
	
	List<FormField> testCreateCardFormFields;
	{
		testCreateCardFormFields = FormField.listOf(
				ImmutableList.of("Code", "Name", "Subnet" , "Number" ) //intentionally left Netmask empty
				, ImmutableList.of("TVLAN-" +testCreateCardRandomSuffix, "Test VLAN" , "10.0.0.0" , "" + testCreateCardRandomSuffix ));
	}
	 
	
	//TEST rules -related
	long testSBDMaxDurationMillis = 25 * SECONDS;
	long testSBDNewCardMaxDurationMillis = 25 * SECONDS;
	long defaultTestDurationMillis = 30 * SECONDS; //TODO move out, to be used when UI performance is not of concern or test is quick
	
	//
	Random random;
	
	
//	@Override //TODO make configurable
//	protected boolean passesDefaultClientLogsChecks() { 
//		//TODO saveButtonDisabledWhenLeavingARequiredFieldBlankTest is known to generate a 400 client error, please fix
//		return fetchClientLog().printLogs(logger, null).supress("404").supress("401").supress("400").success();
//	}
	
	@After
	public void cleanup() {
		cleanupTouchedClasses();
	}
	
	@Test   
	public void saveButtonDisabledWhenLeavingARequiredFieldBlankTest() {
		
		UITestContext testContext = testStart();
		testContext.withRule(UITestRule.define("Duration", "Test execution took longer than allowed to complete"
				, c -> c.getMeasuredTimeSpent() < testSBDMaxDurationMillis));
		testContext.withRule(UITestRule.defineClientLogCheckRule(null, ImmutableList.of("404","401","400"), null, null));
		
		
		login(defaultAdminCredentialsInstance()); //also tested with defaultDemoUserCredentialsInstance
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testSBDMenuClassMain , testSBDMenuClassSub);
		
		ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver(), testContext); //checks for grid stability and accounts for time taken to assure grid is stable but not imputable to client
//		Optional<Cell> cellBeforeEdit = grid.getRows().get(grid.rows()-1).stream().filter(r -> testSBDCardFieldToModify.equals(r.getFieldName())).findFirst();
		WebElement gridRow = grid.expandGridRow(grid.rows() -1);
		waitForLoad();
		
		grid.editCard(gridRow);
		waitForLoad();
		
		//TODO? wait for something else, managementdetailwindows is not the best choice
		WebElement detailsForm = getManagementDetailsWindow(getDriver());
		String fieldContent = getFormTextFieldContent(detailsForm, testSBDCardFieldToModify);
		logger.info("Field {} contains: {}" ,testSBDCardFieldToModify ,fieldContent); 
		
		assertFalse(Strings.isNullOrEmpty(fieldContent));
		assertFalse(isDetailFormButtonDisabled(detailsForm, cardEditSaveButtonCaption));
		
		clearFormTextField(detailsForm, testSBDCardFieldToModify); 
		assertTrue(Strings.isNullOrEmpty( getFormTextFieldContent(detailsForm, testSBDCardFieldToModify)));
		assertTrue(isDetailFormButtonDisabled(detailsForm, cardEditSaveButtonCaption));
		
		//checkClient();
		testEnd(testContext);
		
	}
	
	@Test  //This test is known to fail due to a bug (issue #282)
	public void saveButtonDisabledWhenLeavingARequiredFieldBlankTestOnNewCard() {
		
		UITestContext testContext = testStart();
		testContext.withRule(UITestRule.define("Duration", "Test execution took longer than allowed to complete"
				, c -> c.getMeasuredTimeSpent() < testSBDNewCardMaxDurationMillis));
		testContext.withRule(relaxedCLientLogRule);
		
		
		login(defaultAdminCredentialsInstance()); 
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testSBDNewCardMenuClassMain , testSBDNewCardMenuClassSub);
		
		ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver(), testContext); WebElement gridRow = grid.expandGridRow(grid.rows() -1);
		waitForLoad();
		
		WebElement detailsForm =  grid.newCard();
		//TODO maybe we need to wait for field edit / validation to complete before assertion checks...
		//Add only if test starts to fail 
		assertTrue(isDetailFormButtonDisabled(detailsForm, cardNewSaveButtonCaption));
		fillFormTextField(detailsForm, testSBDNewCardCardField1ToFill, testSBDNewCardCardField1ToFillContent);
		assertTrue(isDetailFormButtonDisabled(detailsForm, cardNewSaveButtonCaption));
		fillFormTextField(detailsForm, testSBDNewCardCardField2ToFill, testSBDNewCardCardField2ToFillContent);
		assertTrue(isDetailFormButtonDisabled(detailsForm, cardNewSaveButtonCaption));
		fillFormTextField(detailsForm, testSBDNewCardCardRequiredFieldToLeaveBlank, "This is a test");
		assertFalse(isDetailFormButtonDisabled(detailsForm, cardNewSaveButtonCaption));
		clearFormTextField(detailsForm, testSBDNewCardCardRequiredFieldToLeaveBlank);
		assertTrue(isDetailFormButtonDisabled(detailsForm, cardNewSaveButtonCaption));
		
		testEnd(testContext);
		
	}
	
	
	
	@Test  //KNOWN TO FAIL: cancel button is not working, so developed also a case using keyboard to quit edit modal; the latter is successfull
	public void abortCardCreationTest() {
		
		UITestContext testContext = testStart();
		testContext.withRule(UITestRule.define("Duration", "Test execution took longer than allowed to complete"
				, c -> c.getMeasuredTimeSpent() < testSBDNewCardMaxDurationMillis));
		testContext.withRule(relaxedCLientLogRule);
		
		
		login(defaultAdminCredentialsInstance()); 
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testAbortCCMenuClassMain , testAbortCCMenuClassSub);
		
		//TODO identify
		ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver(), testContext); WebElement gridRow = grid.expandGridRow(0);
		waitForLoad();
		
		artificialSleep(500, testContext);
		assertTrue(grid.isGridRowExpanded(0));
		
		//case 1: quit edit modal pressing esc
		WebElement detailsForm =  grid.newCard();
		detailsForm.sendKeys(Keys.ESCAPE);
		artificialSleep(500, testContext);
		assertTrue(grid.isGridRowExpanded(0));
		
		//case 2: quit edit modal clicking Cancel button
		//TODO known not working ( #286 )
		detailsForm =  grid.newCard();
		Optional<WebElement> cancelButton = getDetailFormButton(detailsForm, "Cancel");
		cancelButton.get().click(); 
		artificialSleep(500, testContext);
		assertTrue(grid.isGridRowExpanded(0));
		
		testEnd(testContext);
	}
	
	//TODO add a case with dropdowns 
	@Test 
	public void createCardTest() {
		
		
		UITestContext testContext = new UITestContext().withWebDriver(getDriver());
		testContext.withRule(UITestRule.define("Duration", "Test execution took longer than allowed to complete"
				, c -> c.getMeasuredTimeSpent() < defaultTestDurationMillis));
		testContext.withRule(relaxedCLientLogRule);
		testContext.withTouchedClass(testCreateCardTouchedClass);
		
		testStart(testContext);
		
		login(defaultAdminCredentialsInstance()); 
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testCreateCardMenuClassMain , testCreateCardMenuClassSub);
		waitForLoad();
		
		ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver(), testContext); 
		waitForLoad();
		int gridItemsBeforeNewCard = grid.rows();
		
		artificialSleep(500, testContext);
		WebElement detailsForm =  grid.newCard();
		grid.fillForm(testCreateCardFormFields);
		
		Optional<WebElement> saveButton = getDetailFormButton(detailsForm, cardNewSaveButtonCaption);
		saveButton.get().click(); 
		//close details window
		detailsForm = getManagementDetailsWindow(getDriver());
		detailsForm.sendKeys(Keys.ESCAPE);
		
		artificialSleep(2500, testContext);
		
		// CHECKS
		/*
		grid = ExtjsCardGrid.extract(getDriver(), testContext);
		//grid.refresh();
		int gridItemsAfterNewCard = grid.rows();
		assertEquals(gridItemsBeforeNewCard +1, gridItemsAfterNewCard);
		assertTrue(grid.hasRowContainingAllFields(testCreateCardFormFields));
		*/
		
		//TODO
		// check db also
		
		// go somewhere else and come back, assure the new card is present
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testCreateCardMenuClassOtherMain , testCreateCardMenuClassOtherSub);
		waitForLoad();
		grid = ExtjsCardGrid.extract(getDriver(), testContext); //make sure grid is refreshed
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testCreateCardMenuClassMain , testCreateCardMenuClassSub);
		waitForLoad();
		grid = ExtjsCardGrid.extract(getDriver(), testContext); //make sure grid is refreshed
		assertEquals(gridItemsBeforeNewCard +1, grid.rows());
		grid.hasRowContainingAllFields(testCreateCardFormFields);
		waitForLoad();
		
		testEnd(testContext);
	}
	
	@Test //UNDER DEVELOPEMENT
	public void referenceDropdownTest() {
		
		UITestContext context = getDefaultTestContextInstance();
		context.withRule(UITestRule.define("Duration", "Test execution took longer than allowed to complete"
				, c -> c.getMeasuredTimeSpent() < defaultTestDurationMillis));
		context.withRule(relaxedCLientLogRule);
		testStart(context);
		
		login(defaultDemoUserCredentialsInstance()); 
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testReferenceDropDownCommonMenuClassMain , testReferenceDropDownReferencedClass1MenuSub);
		waitForLoad();
		
		ExtjsCardGrid grid1 = ExtjsCardGrid.extract(getDriver(), context); 
		List<String> reference1Values = new ArrayList<>();
		grid1.getRows().forEach(cl -> reference1Values.add(ExtjsCardGrid.getContent(cl, testReferenceDropDownDescriptionFieldName)));
		logger.info("Relation 1 values: {}", reference1Values.toString());
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testReferenceDropDownCommonMenuClassMain , testReferenceDropDownReferencedClass2MenuSub);
		waitForLoad();
		List<String> reference2Values = new ArrayList<>();
		ExtjsCardGrid grid2 = ExtjsCardGrid.extract(getDriver(), context);
		grid2.getRows().forEach(cl -> reference2Values.add(ExtjsCardGrid.getContent(cl, testReferenceDropDownDescriptionFieldName)));
		logger.info("Relation 2 values: {}", reference2Values.toString());
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), testReferenceDropDownCommonMenuClassMain , testReferenceDropDownReferencerClassMenuSub);
		waitForLoad();
		ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver(), context);
		WebElement gridRow = grid.expandGridRow(0);
		grid.editCard(gridRow);
		WebElement detailsForm =  getManagementDetailsWindow(getDriver());
		
		List<String> optionsField1 = ExtjsUtils.getDetailFormDropDownFieldOptions(getDriver(), detailsForm, testReferenceDropDownReference1FieldName);
		List<String> optionsField2 = ExtjsUtils.getDetailFormDropDownFieldOptions(getDriver(), detailsForm, testReferenceDropDownReference2FieldName);
		logger.info("Field {} dropdown options: {}" ,testReferenceDropDownReference1FieldName , optionsField1.toString());
		logger.info("Field {} dropdown options: {}" ,testReferenceDropDownReference2FieldName , optionsField2.toString());
		
		assertEquals(reference1Values.size(), optionsField1.size());
		assertEquals(reference2Values.size(), optionsField2.size());
		assertTrue(reference1Values.stream().allMatch(o -> optionsField1.contains(o)));
		assertTrue(reference2Values.stream().allMatch(o -> optionsField2.contains(o)));
		
		//artificialSleep(5000, context);
		testEnd(context);
	}
	
	@Test @Ignore //TODO postponed
	public void disableCLassFromNavigationMenu() {
		
	}
	
	
	@Test @Ignore //TODO feature not yet implemented
	public void createCardFromSuperclassTest () {
		
		
		UITestContext testContext = new UITestContext().withWebDriver(getDriver());
		testContext.withRule(UITestRule.define("Duration", "Test execution took longer than allowed to complete"
				, c -> c.getMeasuredTimeSpent() < defaultTestDurationMillis));
		testContext.withRule(relaxedCLientLogRule);
		//testContext.withTouchedClass(testCreateCardTouchedClass);
		
		testStart(testContext);
	}

}
