package org.cmdbuild.test.web.utils;

import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;

//FIXME promote, move out of here
public class WebUILoginCredentials {
	
	public static final String DEFAULT_USER_ADMIN = "admin";
	public static final String DEFAULT_USER_DEMOUSER = "demouser";
	
	//Maybe sometime we'll want to fill credentials from some type of external configuration...
	public static  Map<String, String[]> defaultUserDefs = ImmutableMap.<String,String[]>builder()
			.put(DEFAULT_USER_ADMIN, new String[] {"admin", "admin", "English" , null, null})
			.put(DEFAULT_USER_DEMOUSER, new String[] {"demouser", "demouser", "English" , null , null})
			.build();
	
	private @Nonnull String userName;
	private @Nonnull String password;		
	private String language;
	private String tenant;
	private String role;
	
	protected WebUILoginCredentials(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}
	
	/**
	 * 
	 * @param language if null first option shown is selected
	 */
	public WebUILoginCredentials(String userName, String password,  @Nullable String language) {
		this (userName, password);
		this.language = language;
	}
	

	public WebUILoginCredentials(String userName, String password, @Nullable String language,  @Nullable String role, @Nullable String tenant) {
		this (userName, password, language);
		this.role = role;
		this.tenant = tenant; 
	}
	
	@Deprecated //in favour of defaultCredentialsInstance
	public static WebUILoginCredentials getDefaultAdminCredentialsInstance() {
		return new WebUILoginCredentials("admin", "admin");
	}
	
	public static WebUILoginCredentials defaultCredentialsInstance(String defaultUser)  {
		String[] def = defaultUserDefs.get(defaultUser);
		if (defaultUser==null) throw new NullPointerException("DEFAULT USER NOT DEFINED IN WebUILoginCredentials");
		return new WebUILoginCredentials(def[0], def[1],def[2], def[3], def[4]);
	}
	
	public static WebUILoginCredentials fromUsernameAndPassword(String username, String password) {
		return new WebUILoginCredentials(username, password);
	}
	
	/**
	 * In place modification
	 * 
	 * @param user new username
	 * @return modified instance 
	 */
	public WebUILoginCredentials withUserName(@Nonnull String user) {this.userName = user; return this;}
	/**
	 * In place modification
	 * 
	 * @param password new password
	 * @return modified instance 
	 */
	public WebUILoginCredentials withPassword(@Nonnull String password) {this.password = password; return this;}
	public WebUILoginCredentials withLanguage(@Nonnull String language) {this.language = language; return this;}
	public WebUILoginCredentials withTenant(@Nonnull String tenant) {this.tenant = tenant; return this;}
	public WebUILoginCredentials withRole(@Nonnull String role) {this.role= role; return this;}
	
	public @Nonnull String getUserName() {return userName;}
	public @Nonnull String getPassword() {return password;}
	public @Nullable String getLanguage() {return language;}
	public @Nullable String getTenant() {return tenant;}
	public @Nullable String getRole() {return role;}
}