package org.cmdbuild.test.web.utils;

import org.openqa.selenium.By;

public class UILocators {
	
	
	public static By cmdbuildManagementDetailsWindowLocator() {return cmdbuildManagementDetailsWindowLocator;}
	public static By cardDetailHistoryGrid() {return cardDetailHistoryGrid;}
	public static By cardDetailsCloseButton() {return cardDetailsCloseButton;}

	//Could depend on browser, perhaps
	private static By cmdbuildManagementDetailsWindowLocator = By.id("CMDBuildManagementDetailsWindow");
	private static By cardDetailHistoryGrid = By.className("x-panel-default");
	private static By cardDetailsCloseButton = By.xpath("//div[@data-qtip='Close dialog']");
	

}
