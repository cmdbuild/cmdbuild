/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.test.web.utils;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;
import java.net.URI;
import java.util.Optional;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;

import org.cmdbuild.dao.config.inner.DatabaseCreator;
import org.cmdbuild.test.rest.utils.TomcatManagerForTest;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.runtime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.postgresql.ds.PGPoolingDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

/**

 */
public abstract class AbstractWebIT {

	private final static long DEFAULT_DELAY = 100;

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private static TomcatManagerForTest tomcatManagerForTest;

	private WebDriverManager webDriverManager;

	private static String baseUrl;
	private static long defaultDelay;
	private static boolean interactiveMode;

	@BeforeClass
	public static void setupClass() throws Exception {

		defaultDelay = Optional.ofNullable(trimToNull(System.getProperty("cmdbuild.test.delay"))).map(Long::valueOf).orElse(DEFAULT_DELAY);
		baseUrl = trimToNull(System.getProperty("cmdbuild.test.base.url"));

		if (isBlank(baseUrl)) { //if baseurl not set, run with tomcat manager
			tomcatManagerForTest = new TomcatManagerForTest();
			tomcatManagerForTest.initTomcatAndDb(DatabaseCreator.READY_2_USE_DATABASE);
			sleepLong();
			baseUrl = tomcatManagerForTest.getBaseUrl();
		}

		interactiveMode = !isBlank(System.getProperty("cmdbuild.test.interactive")) && !Boolean.FALSE.toString().equalsIgnoreCase(System.getProperty("cmdbuild.test.interactive"));
	}

	public static boolean isInteractiveMode() {
		return interactiveMode;
	}

	@AfterClass
	public static void tearDownClass() {
		if (tomcatManagerForTest != null) {
			tomcatManagerForTest.cleanupTomcatAndDb();
			tomcatManagerForTest = null;
		}
		if (dataSource != null) {
			try {
				dataSource.close();
			} catch (Exception ex) {
			}
			dataSource = null;
		}
	}

	@Before
	public void setUp() throws Exception {
		logger.info("setup test");
		logger.info("cmdbuild base url = {}", baseUrl);

		webDriverManager = new WebDriverManager();
		webDriverManager.init();
	}

	@After
	public void tearDown() {
		logger.info("teardown test");
		webDriverManager.cleanup();
		webDriverManager = null;
	}

	public RemoteWebDriver getDriver() {
		return webDriverManager.getDriver();
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void waitForLoad() {
		waitFor((WebDriver thisDriver) -> ((JavascriptExecutor) thisDriver).executeScript("return document.readyState").equals("complete"));
	}

	public void waitFor(ExpectedCondition<Boolean> expectedCondition) {
		sleep();
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(expectedCondition);
	}

	public WebElement waitFor(String css) {
		waitFor((driver) -> !driver.findElements(By.cssSelector(css)).isEmpty());
		return find(css);
	}

	public WebElement waitFor(String css, String content) {
		waitFor(css);
		waitFor((driver) -> driver.findElement(By.cssSelector(css)).getText().equals(content));
		return find(css);
	}

	public WebElement find(String css) {
		return getDriver().findElement(By.cssSelector(css));
	}

	public void getUrl(String url) {
		getDriver().get(url);
		waitForLoad();
	}

	public void close() {
		sleep();
		getDriver().close();
	}

	public static void sleep() {
		sleep(defaultDelay);
	}

	public static void sleepLong() {
		sleep(defaultDelay * 5);
	}

	public static void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}

	private static PGPoolingDataSource dataSource;

	public JdbcTemplate getJdbcTemplate() {
		if (tomcatManagerForTest != null) {
			return tomcatManagerForTest.getJdbcTemplate();
		} else {
			if (dataSource == null) {
				try {
					String jdbcUrl = trimToNull(System.getProperty("cmdbuild.test.database.url")),
							username = firstNonNull(trimToNull(System.getProperty("cmdbuild.test.database.username")), "postgres"),
							password = firstNonNull(trimToNull(System.getProperty("cmdbuild.test.database.password")), "postgres");
					checkNotNull(jdbcUrl, "unable to get jdbc connection, tomcat manager is null and cmdbuild.test.database.url is null");

					logger.info("open connection to postgres db {} {}:{}", jdbcUrl, username, password);
					URI uri = new URI(jdbcUrl);
					dataSource = new PGPoolingDataSource();
					dataSource.setServerName(uri.getHost());
					dataSource.setPortNumber(uri.getPort());
					dataSource.setDatabaseName(uri.getPath().replaceAll("/", ""));
					dataSource.setUser(username);
					dataSource.setPassword(password);
//					dataSource.setMaxConnections(2);
					dataSource.initialize();

				} catch (Exception ex) {
					throw runtime(ex);
				}
			}
			return new JdbcTemplate(dataSource);
		}
	}

}
