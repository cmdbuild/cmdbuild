package org.cmdbuild.test.web;

import static org.cmdbuild.test.web.utils.ExtjsUtils.getCardGrid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.cmdbuild.test.web.utils.BaseWebIT;
import org.cmdbuild.test.web.utils.ExtjsCardGrid;
import org.cmdbuild.test.web.utils.ExtjsUtils;
import org.cmdbuild.test.web.utils.WebServiceUtilities;
import org.cmdbuild.test.web.utils.WebUILoginCredentials;
import org.cmdbuild.test.web.utils.ExtjsCardGrid.Cell;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.gson.JsonObject;
import org.cmdbuild.client.rest.model.AttributeData;

public class ManagementGridIT extends BaseWebIT {
	
	@Override //TODO make configurable
	protected boolean passesDefaultClientLogsChecks() {
		return fetchClientLog().supress("404").supress("401").success();
	}
	
	
	/**
	 * Several checks against UI facilities for card sorting.
	 * 
	 * TODO: check whole tree, not only first level nodes
	 * TODO: add checks for ordering via column header dropdown buttons (when found a general viable solution for dropdowns)
	 * TODO: add checks for menu type
	 */
	@Test 
	public void interactiveCardSortingTest() {
		
		//TODO configuration, externalize
		List<String> columns2OrderBy = Arrays.asList(new String[]{"Hostname" , "Code" , "Assignee" /*contains nulls*/});
		//works with nulls
		
		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		sleep(2500);
		
		//fails FIXME server side error lookup not found ?
		//	safelyClickOnSideNavLeafWithChecksAgainst(getDriver(),"Computer", " computer", "Workplaces" , "All computers");
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), "Workplaces" , "All computers");
		sleep(3500);
		
		for (String column2OrderBy : columns2OrderBy) {
			
			logger.info("Ordering by column {}" , column2OrderBy);
			ExtjsCardGrid gridAscending = getCardGrid(getDriver());
			assertTrue(gridAscending.rows() > 1); //can't check sorting otherwise

			gridAscending.clickOnGridHeader(column2OrderBy);
			waitForLoad(); //Todo should be encapsulated in clickOnGridheader
			gridAscending.refresh();
			List<List<Cell>> ascendingOrderRows = gridAscending.getRows();
		
			gridAscending.clickOnGridHeader(column2OrderBy);
			waitForLoad();
			ExtjsCardGrid gridDescending = getCardGrid(getDriver());
			//grid1.refresh();
			List<List<Cell>> descendingOrderRows = gridDescending.getRows();
			//checks against ordering
			//PAY ATTENTION: if you order by non unique field you can only check against that field
			int idxOfColumn = gridAscending.getColumnIndexOfColumn(column2OrderBy);
			assertEquals(ascendingOrderRows.size(), descendingOrderRows.size());
			assertTrue(descendingOrderRows.size() > 1);
			assertTrue(ascendingOrderRows.size() > 1);
			//check asc and desc ordered grids are reversed and each grid order is respected
			int size = ascendingOrderRows.size();
			for (int i = 0; i < size; i++) {
				logger.info("Comparing row {}, ASC: {} , DESC {}", i, ascendingOrderRows.get(i).get(idxOfColumn),
						descendingOrderRows.get(size - i - 1).get(idxOfColumn));
				assertTrue(ascendingOrderRows.get(i).get(idxOfColumn)
						.matches(descendingOrderRows.get(size - i - 1).get(idxOfColumn)));
				if (i > 0) {
					logger.info("ASC Comparing # {} with {} : {} - {}", i , i-1, ascendingOrderRows.get(i).get(idxOfColumn).getContent(), ascendingOrderRows.get(i-1).get(idxOfColumn).getContent() );;
					assertTrue(ascendingOrderRows.get(i).get(idxOfColumn).getContent().
							compareToIgnoreCase(ascendingOrderRows.get(i-1).get(idxOfColumn).getContent()) >= 0);
					logger.info("DESC Comparing # {} with {} : {} - {}", i , i-1, descendingOrderRows.get(i).get(idxOfColumn).getContent(), descendingOrderRows.get(i-1).get(idxOfColumn).getContent() );;
					assertTrue(descendingOrderRows.get(i).get(idxOfColumn).getContent().
							compareToIgnoreCase(descendingOrderRows.get(i-1).get(idxOfColumn).getContent()) <= 0);
				}
			} 
			
		}
		checkClient();
		testEnd();
		
	}
	
	
	/**
	 * HP: Defined criteria are those returned by ws [class] "attributes" for the user
	 * TODO: (if applicable) check column list is different for users with different class visualization configuration
	 */
	@Test  //DEV
	public void gridColumnsOrderedAccordingToDefinedCriteriaTest() {
		
		testStart();
		login(WebUILoginCredentials.getDefaultAdminCredentialsInstance());
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), "Workplaces" , "All computers");
		sleep(3000);
		
		List<String> displayedColumnsList = getRestClient().classe().getAttributes("Computer").stream()
				.filter(AttributeData::getShowInGrid)
				.map(AttributeData::getDescription)
				.map((s) -> s.replace('"', ' ').trim())
				.collect(Collectors.toList());
		
		 ExtjsCardGrid grid = ExtjsCardGrid.extract(getDriver());
		 List<String> gridColumns = grid.getFields();
		 assertTrue(gridColumns.size() == displayedColumnsList.size());
		 for (int i = 0; i < gridColumns.size(); i++) {
			 logger.info("Comparing #{} column: ws: {} -  ui; {}", i , displayedColumnsList.get(i), gridColumns.get(i)); //demote to debug level when done
			assertEquals(displayedColumnsList.get(i) , gridColumns.get(i));
		}
		
		checkClient();
		testEnd();
		
	}
	
	
	/**
	 * 
	 * Delegates cleanup to @After
	 * 
	 * This test is completely dependent on r2u.
	 * It assumes AttributesPrivileges are null for the (Class,Role) tested, and is aware of the attributes of the class being used
	 * 
	 * 
	 * @throws Exception 
	 */
	//TODO parametrize Class, Role, and User
	@Test 
	public void visibilityOfCardColumnsAsDefinedForRoleTest() throws Exception{
		
		testStart();
		JdbcTemplate template = getJdbcTemplate();

//		String sqlFetchCurrentPrivilegesFromClassBuildingForGroupHWHelpdesk = "select unnest( \"AttributesPrivileges\" ) from \"Grant\" " +
//				" where \"Status\"='A' and \"IdRole\" = (select \"Id\" from \"Role\" where \"Status\"='A' and \"Code\"='HWHelpdesk') and \"IdGrantedClass\"='\"Building\"'::regclass;";
		String sqlSetLimitedPrivilegesOnClassBuildingForGroupHWHelpDesk = "update \"Grant\" set \"AttributesPrivileges\" = array['Address:none','City:none','Country:none','Postcode:none','Region:none'] " 
				+	" where \"Status\"='A' and \"IdRole\" = (select \"Id\" from \"Role\" where \"Status\"='A' and \"Code\"='HWHelpdesk') and \"IdGrantedClass\"='\"Building\"'::regclass;";
		String sqlSetDefaultPrivilegesOnClassBuildingForGroupHWHelpdesk = "update  \"Grant\" set \"AttributesPrivileges\" = null  " +
				" where \"Status\"='A' and \"IdRole\" = (select \"Id\" from \"Role\" where \"Status\"='A' and \"Code\"='HWHelpdesk') and \"IdGrantedClass\"='\"Building\"'::regclass;";
		;
		//List<String> currentprivileges = template.queryForList(sqlFetchCurrentPrivilegesFromClassBuildingForGroupHWHelpdesk, String.class);
		assertEquals(1,template.update(sqlSetLimitedPrivilegesOnClassBuildingForGroupHWHelpDesk) );
		
		sleep(1000); //give backend time to persit change?
		
		WebUILoginCredentials credentials = WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_DEMOUSER)
				.withRole("Hardware helpdesk");
		login(credentials);
		waitForLoad();
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), "Locations" , "Buildings");
		ExtjsCardGrid gridWithLessPrivileges = ExtjsCardGrid.extract(getDriver());
		List<String> fieldsLessPrivileges = gridWithLessPrivileges.getFields();
		
		logger.info("Fields of less privileges grid: {}" , fieldsLessPrivileges.toString() );
		logout();
		
		assertEquals(1, template.update(sqlSetDefaultPrivilegesOnClassBuildingForGroupHWHelpdesk));
		waitForLoad();
		sleep(1000);
		login(credentials);
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), "Locations" , "Buildings");
		ExtjsCardGrid gridWithStandardPrivileges = ExtjsCardGrid.extract(getDriver());
		List<String> fieldsStandardPrivileges = gridWithStandardPrivileges.getFields();
		logger.info("Fields of standard privileges grid: {}" , fieldsStandardPrivileges.toString() );
		
		assertNotEquals(fieldsStandardPrivileges.size(), fieldsLessPrivileges.size());
		
		checkClient();
		testEnd();
		
	}
	
	/**
	 * Maybe desired behaviour will be overturned. If so, simply negate final conditions
	 */
	@Test
	public void columnOrderRestoreAfterShufflingTest() {
		
		testStart();
		login(WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_DEMOUSER));
		
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), "Locations" , "Buildings");
		ExtjsCardGrid gridDefault = ExtjsCardGrid.extract(getDriver());
		List<String> fieldsDefault = gridDefault.getFields();
		WebElement source =  gridDefault.getHeaderWebElement(fieldsDefault.get(0));
		WebElement target =  gridDefault.getHeaderWebElement(fieldsDefault.get(2));
		//It moves first column by 1 to the right!!
		//With extjs , if you drag and drop first column left to the center of the second one, no swap is performed. You have to drop left 
		// of center of second column... Therefore we are using third field as target of d&d!
		
		LocalDateTime start = LocalDateTime.now();
		new Actions(getDriver()).dragAndDrop(source, target).perform();
		logger.warn("Drag and drop action took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		waitForLoad();
		
		ExtjsCardGrid gridSwapped = ExtjsCardGrid.extract(getDriver());
		List<String> fieldsSwapped = gridSwapped.getFields();
		assertTrue(fieldsSwapped.get(1).equals(fieldsDefault.get(0)) );
		assertTrue(fieldsSwapped.get(0).equals(fieldsDefault.get(1)) );
		
		//"refresh" to default order by going to another card and coming back...
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), "Locations" , "Floors");
		ExtjsUtils.safelylickOnSideNavLeaf(getDriver(), "Locations" , "Buildings");
		ExtjsCardGrid gridDefaultRestored = ExtjsCardGrid.extract(getDriver());
		List<String> fieldsDefaultRestored = gridDefaultRestored.getFields();
		assertTrue(IntStream.range(0, fieldsDefault.size()).allMatch(i -> fieldsDefault.get(i).equals(fieldsDefaultRestored.get(i))));
		assertTrue(IntStream.range(0, fieldsDefault.size()).anyMatch(i ->  ! fieldsDefault.get(i).equals(fieldsSwapped.get(i)))); //redundant
		
		checkClient();
		testEnd();
	}
	
	
	
	@After
	public void cleanupManagementGridIT() {
		getJdbcTemplate().update(sqlSetDefaultPrivilegesOnClassBuildingForGroupHWHelpdesk);
	}
	
	String sqlSetDefaultPrivilegesOnClassBuildingForGroupHWHelpdesk = "update  \"Grant\" set \"AttributesPrivileges\" = null  " +
			" where \"Status\"='A' and \"IdRole\" = (select \"Id\" from \"Role\" where \"Status\"='A' and \"Code\"='HWHelpdesk') and \"IdGrantedClass\"='\"Building\"'::regclass;";

}
