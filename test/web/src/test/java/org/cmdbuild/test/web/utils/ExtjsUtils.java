package org.cmdbuild.test.web.utils;

import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.cmdbuild.test.web.WebUICMDBuidTestException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;


//TODO: implement combobox management
public class ExtjsUtils {
	
	protected static int defaultWaitSeconds = 5;//FIXME externalize to configuration
	protected static int timeoutWaitForElementPresenceSeconds = 10; //TODO make configurable
	protected static int timeoutWaitForElementVisibilitySeconds = 5; //TODO make configurable
	protected static int pollingIntervalMillis = 100; //TODO make configurable

	protected final static Logger logger = LoggerFactory.getLogger(ExtjsUtils.class);
	
	
	public static WebElement getParent(WebElement son) {
		return son.findElement(By.xpath(".."));
	}
	
	public static Optional<WebElement> getFirstAncestorByCssClass(WebElement inner, String searchAttribute, String searchContent) {
		try {
			WebElement current = inner;
			do {
				current = getParent(current);
				logger.info("PARENT: {}" , current.getAttribute(searchAttribute));
			} while (! current.getAttribute(searchAttribute).contains(searchContent) );
			return Optional.of(current);
		} catch (NoSuchElementException e) {
			return Optional.empty();
		}
	}
	
	public static boolean isClass(WebElement element,String cssClass) {
		try {
			return element.getAttribute("class").contains(cssClass);
		} catch (Exception e) {
			logger.warn("Problem in isClass: {}" , e.toString());
			return false;
		}
	}
	
	/**
	 * FIXME: unsafe / not telling if found, not waiting for element to be present /visible
	 * @param extjsElementWithInput
	 * @param content
	 */
	public static void sendKeys2ExtjsInput(@Nonnull WebElement extjsElementWithInput, String content) {
		extjsElementWithInput.findElement(By.tagName("input")).sendKeys(content);
	}
	
	/**
	 * @param driver 
	 * @param testId
	 * @return
	 * 
	 * Safely waits for the element to be present
	 */
	public static WebElement findElementByTestId(WebDriver driver, String testId) {
		 return (new WebDriverWait(driver, defaultWaitSeconds))
		 .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@data-testid='" + testId + "']")));
		 //return driver.findElement(By.xpath("//*[@data-testid='" + testId + "']"));
	}
	
	/**
	 * @param driver
	 * @param xPath
	 * @return
	 * 
	 * Safely waits for the element to be present
	 */
	public static WebElement findElementByXPath (WebDriver driver, String xPath) {
		return (new WebDriverWait(driver, defaultWaitSeconds))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));
	}
	
	/**
	 * @param driver
	 * @param id
	 * @return
	 * 
	 * Safely waits for the element to be present
	 */
	public static WebElement findElementById (WebDriver driver, String id) {
		return (new WebDriverWait(driver, defaultWaitSeconds))
				.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
	}
	
	public static WebElement fetchClickableWebElement (WebDriver driver, WebElement clickableWebElement) {
		return (new WebDriverWait(driver, defaultWaitSeconds))
				.until(ExpectedConditions.elementToBeClickable(clickableWebElement));
	}
	
	public static  boolean checkCorrectClassIsDisplayed(WebDriver driver, String expectedSelectedClassName ) {
		return ( (driver.getCurrentUrl().contains("#classes/" + expectedSelectedClassName +"/cards"))
				&&  (driver.findElement(By.id("CMDBuildManagementContent")).findElement(By.className("x-title-text"))
						.getText().toLowerCase().contains(expectedSelectedClassName.toLowerCase())));
	}
	
	//FIXME does not work with form combos
	@Deprecated
	public static void selectFirstOptionInExtjsCombo(WebDriver driver, WebElement combo) {
		
		WebElement languageInput = combo.findElement(By.tagName("input"));
		languageInput.click();
		String comboList = languageInput.getAttribute("aria-owns");
		List<String> comboOwnedNodeIds = Splitter.on(' ').trimResults().omitEmptyStrings().splitToList((comboList));
		Optional<String> comboListId = comboOwnedNodeIds.stream().filter(id -> id.contains("picker-list")).findFirst();
		WebElement comboListUL = driver.findElement(By.id(comboListId.get()));
		List<WebElement> options = comboListUL.findElements(By.tagName("li"));
		if (!options.isEmpty()) {
			WebElement clickableOption = (new WebDriverWait(driver, 10)) //TODO EXTERNALIZE
					.until(ExpectedConditions.elementToBeClickable(options.get(0)));
			clickableOption.click();
		}
	}
	
	//FORM RELATED
	public static Optional<WebElement> fetchCombo(WebElement formOrParent, String fieldName) {
		
		List<WebElement> fields = formOrParent.findElements(By.className("x-field"));
		String fieldNameVariant = fieldName + " *";
		for (WebElement field : fields) {
			if (field.getAttribute("id").startsWith("combo-") ){
				WebElement label = field.findElement(By.className("x-form-item-label-text")); //TODO check (perhaps too restrictive)
				if (fieldName.equals(label.getText()) || fieldNameVariant.equals(label.getText()) ) {
					return Optional.of(field);
				}
			}
		}
		return Optional.empty();
		
	}
	
	/**
	 * @param driver
	 * @param treePath tree nodes to open (last one must be leaf)
	 * @return true if leaf node was found and opened
	 * 
	 * @deprecated: use safe version with checks
	 */
	@Deprecated // use safe version with checks
	public static boolean clickOnSideNavLeaf(WebDriver driver, String... treePath) {
		
		WebElement navigationContainer = findElementByTestId(driver, "management-navigation-container");
		boolean foundAndClicked = recursiveLocateAndClickSideNavLeaf(navigationContainer, treePath, 0);
		return foundAndClicked;
	}
	
	/**
	 * 
	 * @deprecated Use safe version
	 * 
	 * @param driver
	 * @param treePath tree nodes to open (last one must be leaf)
	 * @return true if leaf node was found and opened
	 * @throws WebUICMDBuidTestException when leaf was not found, or default checks failed. Default checks test for:
	 * <ul>
	 * 	<li> URL path is correct</li>
	 * 	<li> Content section title reports rightclass name</li>
	 * 	<li> </li>
	 * </ul>
	 * 
	 * 
	 */
	@Deprecated
	public static void clickOnSideNavLeafWithChecks(WebDriver driver, String... treePath)  throws WebUICMDBuidTestException {
	
		if (! clickOnSideNavLeaf(driver, treePath))
			throw new WebUICMDBuidTestException("SideNav Path not found"); 
		sleep(1000); //FIXME
		assertTrue(driver.getCurrentUrl().contains("#classes/" +treePath[treePath.length-1] +"/cards"));
		WebElement content = driver.findElement(By.id("CMDBuildManagementContent"));
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards " + treePath[treePath.length-1].toLowerCase()));
		
	}
	/**
	 * Consider using ExtjsNavigationMenu methods, instead
	 * 
	 * @param driver
	 * @param expectedUrlTreePathFragment landing URL must contain: "#classes/" + expectedUrlTreePathFragment +"/cards") 
	 * @param expectedCmdbuildManagementSectionTitle This string must be contained in the Management section 
	 * @param treePath tree nodes to open (last one must be leaf)
	 * @return true if leaf node was found and opened
	 * @throws WebUICMDBuidTestException when leaf was not found, or default checks failed. Default checks test for:
	 * <ul>
	 * 	<li> URL path is correct</li>
	 * 	<li> Content section title reports rightclass name</li>
	 * 	<li> </li>
	 * </ul>
	 * 
	 * 
	 */
	//TODO deprecate and empower ExtjsNavigationMenu?
	public static void safelyClickOnSideNavLeafWithChecksAgainst(WebDriver driver, String expectedUrlTreePathFragment, String expectedCmdbuildManagementSectionTitle, String... treePath)  throws WebUICMDBuidTestException {
		
		if (! safelylickOnSideNavLeaf(driver, treePath))
			throw new WebUICMDBuidTestException("SideNav Path not found"); 
		waitForLoad(driver);
		sleep(1000); //FIXME
		assertTrue(driver.getCurrentUrl().contains("#classes/" + expectedUrlTreePathFragment +"/cards"));
		WebElement content = driver.findElement(By.id("CMDBuildManagementContent"));
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains(expectedCmdbuildManagementSectionTitle.toLowerCase()));
		
	}
	
	/**
	 * @deprecated Prefer safelyClickOnSideNavLeafWithChecksAgainst. Class names in navigation menu can be renamed
	 * 
	 * @param driver
	 * @param treePath tree nodes to open (last one must be leaf)
	 * @return true if leaf node was found and opened
	 * @throws WebUICMDBuidTestException when leaf was not found, or default checks failed. Default checks test for:
	 * <ul>
	 * 	<li> URL path is correct</li>
	 * 	<li> Content section title reports rightclass name</li>
	 * 	<li> </li>
	 * </ul>
	 * 
	 * 
	 */
	//TODO refactor using safelyClickOnSideNavLeafWithChecksAgainst
	@Deprecated // use ...Against version: Class names in navigation menu can be overridden
	public static void safelyClickOnSideNavLeafWithChecks(WebDriver driver, String... treePath)  throws WebUICMDBuidTestException {
		
		if (! safelylickOnSideNavLeaf(driver, treePath))
			throw new WebUICMDBuidTestException("SideNav Path not found"); 
		waitForLoad(driver);
		sleep(2000); //FIXME
		assertTrue(driver.getCurrentUrl().contains("#classes/" +treePath[treePath.length-1] +"/cards"));
		WebElement content = driver.findElement(By.id("CMDBuildManagementContent"));
		assertTrue(content.findElement(By.className("x-title-text")).getText().toLowerCase().contains("cards " + treePath[treePath.length-1].toLowerCase()));
		
	}
	
	public static boolean safelylickOnSideNavLeaf(WebDriver driver, String... treePath) {
		
		WebElement navigationContainer = findElementByTestId(driver, "management-navigation-container");
		boolean foundAndClicked = recursiveLocateAndSafelySelectSideNavLeaf(navigationContainer, treePath, 0);
		waitForLoad(driver);
		return foundAndClicked;
	}
		
	@Deprecated //fails if not leaf nodes are already open
	private static boolean recursiveLocateAndClickSideNavLeaf(WebElement currentNode,String[] treePath, int depth) {
		
		List<WebElement> navItems = currentNode.findElements(By.tagName("li"));
		boolean leaf = (treePath.length == depth +1) ;
		for (WebElement navItem : navItems) {
			WebElement textWrapper = navItem.findElement(By.className("x-treelist-item-text"));
			if (textWrapper != null && !Strings.isNullOrEmpty(textWrapper.getText()) 
					&& treePath[depth].equalsIgnoreCase(textWrapper.getText())) {
				if (leaf) {
					textWrapper.click();
					return true;
				} else {
					if (leaf)
						return false;
					WebElement expander = navItem.findElement(By.className("x-treelist-item-expander"));
					if (expander != null && expander.isEnabled()) {
						expander.click();
						sleep(200);
						boolean found = recursiveLocateAndClickSideNavLeaf(navItem, treePath, depth +1);
						if (found)
							return true;
						else {
							expander.click(); //collapse node
						}
					} //else {//already expanded or not present
					//	
					//}
				}
			}
			
		}
		return false;
	}
	
	//works with already open intermediate nodes as well
	private static boolean recursiveLocateAndSafelySelectSideNavLeaf(WebElement currentNode,String[] treePath, int depth) {
		
		List<WebElement> navItems = currentNode.findElements(By.tagName("li"));
		boolean leaf = (treePath.length == depth +1) ;
		for (WebElement navItem : navItems) {
			
			String cssClass = navItem.getAttribute("class");
			boolean collapsed = cssClass.contains("x-treelist-item-collapsed");
				
			WebElement textWrapper = navItem.findElement(By.className("x-treelist-item-text"));
			if (textWrapper != null && !Strings.isNullOrEmpty(textWrapper.getText()) 
					&& treePath[depth].equalsIgnoreCase(textWrapper.getText())) {
				if (leaf) {
					textWrapper.click();
					return true;
				} else {
					if (leaf)
						return false; //FIXME Remove
					if (collapsed) {
					WebElement expander = navItem.findElement(By.className("x-treelist-item-expander"));
						if (expander != null && expander.isEnabled()) {
							expander.click();
							sleep(200);
							boolean found = recursiveLocateAndClickSideNavLeaf(navItem, treePath, depth +1);
							if (found) 
								return true;
							else { //FIXME ???
								expander.click(); //collapse node
							}
						}
					} else {//already expanded
						boolean found =  recursiveLocateAndClickSideNavLeaf(navItem, treePath, depth +1);	
						return found;
					}
				}
			}
			
		}
		return false;
	}
	
	
	/**
	 * @param driver
	 * @return
	 * 
	 * Deprecated: use getCardGrid instead
	 */
	@Deprecated 
	public static List<List<String>> cardGridTextContent(WebDriver driver) {
		
		ArrayList<List<String>> rows = new ArrayList<>();
		
		WebElement content = driver.findElement(By.id("CMDBuildManagementContent"));
		List<WebElement> gridRows = content.findElements(By.tagName("table"));
		for (WebElement gridRow : gridRows) {
			ArrayList<String> row = new ArrayList<>();
			rows.add(row);
			List<WebElement> gridCells = gridRow.findElements(By.tagName("td"));
			for (WebElement gridCell : gridCells) {
				row.add(gridCell.getText());
			}
		}
		return rows;
	}
	
	public static ExtjsCardGrid getCardGrid(WebDriver driver) {
		return ExtjsCardGrid.extract(driver);
	}
	
	
	
	
	/**
	 * @param driver
	 * @param searchText 
	 * @param clearTextBeforeTyping Whether to clear previous text or not before typing. If null defaults to true.
	 * @param sendEnterAfterText Whether to press enter key after havng enetered the text or not before typing. If null defaults to true.
	 * @return
	 */
	public static boolean setQuickSearch4CardsText(WebDriver driver, String searchText, Boolean clearTextBeforeTyping ,Boolean sendEnterAfterText) {
		
		if (! Boolean.FALSE.equals(clearTextBeforeTyping)) {
			clearQuickSearch4CardsText(driver);
			sleep(1000); //FIXME
		}
		WebElement content = driver.findElement(By.id("CMDBuildManagementContent"));
		List<WebElement> inputs = content.findElements(By.tagName("input"));
		for (WebElement input : inputs) {
			if (input.isDisplayed() && input.isEnabled() && 
					"Search...".equalsIgnoreCase(input.getAttribute("placeholder"))) {//FIXME: Externalize strings
				input.sendKeys(searchText);
				if (! Boolean.FALSE.equals(sendEnterAfterText)) {
					sleep(1000); //FIXME
					input.sendKeys(Keys.ENTER);
				}
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * @param driver
	 * @return list of LogEntries related to browser.
	 * Call this method once because it consumes logs! //TODO check
	 */
	@Deprecated //use ClientLog
	public static List<LogEntry> fetchBrowserLogEntries(WebDriver driver) {
		 return driver.manage().logs().get(LogType.BROWSER).toJson();
	}
	
	@Deprecated //use ClientLog
	private static LogEntries  fetchLogs(WebDriver driver) {
		return driver.manage().logs().get(LogType.BROWSER);
	}
	
	@Deprecated //use ClientLog
	public static  List<LogEntry> filterLogEntries(List<LogEntry> logs, Level severity) {
		return logs.stream().filter(le -> le.getLevel().intValue() >= severity.intValue()).collect(Collectors.toList());
	}
	
	@Deprecated //use ClientLog
	public static List<LogEntry> filterLogEntries(List<LogEntry> logs, Level severity, String... triggers) {
		List<String> triggerList = Arrays.asList(triggers);
		return logs.stream().filter(le -> le.getLevel().intValue() >= severity.intValue())
				.filter(le -> triggerList.stream().anyMatch(le.toString()::contains))
				.collect(Collectors.toList());
	}
	
	@Deprecated //use ClientLog
	public static void printLogs (List<LogEntry> logs) {
		logger.info("Reporting Browser logs [unknown filters]");
		logs.stream().forEach(le -> logger.info("@: {} Severity: {} Message: {}" , le.getTimestamp() ,le.getLevel().getName(), le.getMessage()));
	}
	@Deprecated //use ClientLog
	public static void printLogs (List<LogEntry> logs , String title) {
		logger.info( title);
		logs.stream().forEach(le -> logger.info("@: {} Severity: {} Message: {}" , le.getTimestamp() ,le.getLevel().getName(), le.getMessage()));
	}
	@Deprecated //use ClientLog
	public static void printLogs (List<LogEntry> logs , Level severity, String... triggers) {
		List<String> triggerList = Arrays.asList(triggers);
		logger.info("Reporting Browser log with severity level of {} or greater and with one of the following keys: {} ..." , severity.getName(), triggerList.toString());
		logs.stream().forEach(le -> logger.info("@: {} Severity: {} Message: {}" , le.getTimestamp() ,le.getLevel().getName(), le.getMessage()));
	}
	
	public static WebElement getManagementDetailsWindow(WebDriver driver) {
		
		WebElement details = (new WebDriverWait(driver, 10)) //TODO EXTERNALIZE
				.ignoring(NoSuchElementException.class)
				.pollingEvery(100, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.presenceOfElementLocated(By.id("CMDBuildManagementDetailsWindow")));
		//now make sure details windows is shown by checking its visibility (so form fields and buttons are usable)
		details = (new WebDriverWait(driver, 10)) //TODO externalise? 
				.ignoring(NoSuchElementException.class)
				.pollingEvery(100, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.visibilityOf(details));
		
		return details;
	}
	
	public static String getFormTextFieldContent(WebElement formContainerWebElement, String fieldLabel ) {
		
		Optional<WebElement> field = fetchFormTextField(formContainerWebElement, fieldLabel);
		if (field.isPresent()) {
			WebElement input = field.get().findElement(By.tagName("input"));
			return input.getAttribute("value");
		} else {
			throw new WebUICMDBuidTestException("Cannot find form text field: " + fieldLabel);
		}
		
	}
	
	//FIXME refactor promoting field to "domain" class (because webelement is complex and tricky)
	public static Optional<WebElement> fetchFormTextField(WebElement formContainerWebElement, String fieldLabel) {
		List<WebElement> fields = formContainerWebElement.findElements(By.className("x-field"));
		String fieldLabelVariant = fieldLabel + " *"; //TODO not working?
		for (WebElement field : fields) {
			WebElement label = field.findElement(By.className("x-form-item-label-text")); //TODO check (perhaps too restrictive)
			if (fieldLabel.equals(label.getText()) || fieldLabelVariant.equals(label.getText())) {
				return Optional.of(field);
			}
		}
		return Optional.empty();
	}
	
	public static void fillFormTextField(WebElement formContainerWebElement, String fieldLabel, String content ) {
		
		Optional<WebElement> field = fetchFormTextField(formContainerWebElement, fieldLabel);
		if (field.isPresent()) {
			WebElement input = field.get().findElement(By.tagName("input"));
		//	input.click();
		//	input.sendKeys(Keys.chord(Keys.CONTROL,Keys.CANCEL));
		//	input.sendKeys(Keys.chord(Keys.CONTROL,Keys.BACK_SPACE));
			
			input.sendKeys(content);
			input.sendKeys(Keys.ENTER);
		} else {
			throw new WebUICMDBuidTestException("Cannot find form text field: " + fieldLabel);
		}
		
	}
	
	public static void clearFormTextField(WebElement formContainerWebElement, String fieldLabel) {
		
		Optional<WebElement> field = fetchFormTextField(formContainerWebElement, fieldLabel);
		if (field.isPresent()) {
			WebElement input = field.get().findElement(By.tagName("input"));
			//	input.click();
			//	input.sendKeys(Keys.chord(Keys.CONTROL,Keys.CANCEL));
			//	input.sendKeys(Keys.chord(Keys.CONTROL,Keys.BACK_SPACE));
			
			input.clear();
		} else {
			throw new WebUICMDBuidTestException("Cannot find form text field: " + fieldLabel);
		}
		
	}
	
	
	
	/**
	 * @param formContainerWebElement a form web element
	 * @param buttonCaption Caption of the button to click
	 * 
	 * This method throws exception if no button or more than one button with the same caption are found.
	 */
	//FIXME: refactor. return clickable or promote form to Class
	public static void clickDetailFormButton(WebElement formContainerWebElement, @Nonnull String buttonCaption) throws WebUICMDBuidTestException{
		List<WebElement> buttons = formContainerWebElement.findElements(By.className("x-btn-inner")); // TODO: refine isolating section
		List<WebElement> filteredButtons = buttons.stream().filter(b -> buttonCaption.equals(b.getText()))
				.filter(b -> b.isEnabled() && b.isDisplayed()).collect(Collectors.toList());
		if (filteredButtons.size() == 1) {
			filteredButtons.get(0).click();
		} else {
			if (filteredButtons.size() == 0)
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_NO_NAMED_BUTTON_FOUND + buttonCaption + "' found!");
			else
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_MORE_THAN_ONE_NAMED_BUTTON_FOUND+ buttonCaption + "' found!");
			
		}
	}
	
	public static boolean isDetailFormButtonDisabled(WebElement formContainerWebElement,String buttonCaption ) throws WebUICMDBuidTestException {
		List<WebElement> buttons = formContainerWebElement.findElements(By.className("x-btn-inner")); // TODO: refine isolating section
		List<WebElement> filteredButtons = buttons.stream().filter(b -> buttonCaption.equals(b.getText()))
				.filter(b -> b.isEnabled() && b.isDisplayed()).collect(Collectors.toList());
		if (filteredButtons.size() == 1) {
//			Optional<WebElement> button = getFirstAncestorByCssClass(filteredButtons.get(0), "class", "x-btn");
			Optional<WebElement> button = getFirstAncestorByCssClass(filteredButtons.get(0), "class", "x-toolbar-item");
			if (button.isPresent()) {
				return button.get().getAttribute("class").contains("x-btn-disabled");
			} else {
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_NO_NAMED_BUTTON_FOUND + buttonCaption + "' found! (2)");
			}
		} else {
			if (filteredButtons.size() == 0)
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_NO_NAMED_BUTTON_FOUND + buttonCaption + "' found!");
			else
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_MORE_THAN_ONE_NAMED_BUTTON_FOUND+ buttonCaption + "' found!");
			
		}
	}
	
	/**
	 * 
	 * Does not wait for element presence / visibility
	 * 
	 * @param formContainerWebElement
	 * @param buttonCaption
	 * @return
	 * @throws WebUICMDBuidTestException
	 */
	public static Optional<WebElement> getDetailFormButton(WebElement formContainerWebElement,String buttonCaption ) throws WebUICMDBuidTestException {
		List<WebElement> buttons = formContainerWebElement.findElements(By.className("x-btn-inner")); // TODO: refine isolating section
		List<WebElement> filteredButtons = buttons.stream().filter(b -> buttonCaption.equals(b.getText()))
				.filter(b -> b.isEnabled() && b.isDisplayed()).collect(Collectors.toList());
		if (filteredButtons.size() == 1) {
			Optional<WebElement> button = getFirstAncestorByCssClass(filteredButtons.get(0), "class", "x-toolbar-item");
			if (button.isPresent()) {
				return button;
			} else {
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_NO_NAMED_BUTTON_FOUND + buttonCaption + "' found! (2)");
			}
		} else {
			if (filteredButtons.size() == 0)
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_NO_NAMED_BUTTON_FOUND + buttonCaption + "' found!");
			else
				throw new WebUICMDBuidTestException(WebUICMDBuidTestException.MESSAGE_MORE_THAN_ONE_NAMED_BUTTON_FOUND+ buttonCaption + "' found!");
			
		}
	}
	
	
	/**
	 * Prefer filtered variants.
	 * 
	 * @param driver instance of webdriver used for current test
	 */
//	@Deprecated
//	public static void printAllLogEntries(WebDriver driver) {
//		
//		LogEntries wholeLog = fetchLogs(driver);
//		List<LogEntry> logs = wholeLog.toJson();
//		logs.stream().forEach(le -> logger.info("@: {} Severity: {} Message: {}" , le.getTimestamp() ,le.getLevel().getName(), le.getMessage()));
//	}
	
	/**
	 * @param driver instance of webdriver used for current test
	 * @param min severity severity level a log must have to be reported 
	 */
//	@Deprecated
//	public static void printLogEntries(WebDriver driver, Level severity) {
//		logger.info("Reporting Browser log with severity level of {} or greater..." , severity.getName());
//		List<LogEntry> logs = fetchLogs(driver).toJson();
//		if (logs.size() > 0)
//			logs.stream().filter(le -> le.getLevel().intValue() >= severity.intValue()) .forEach(le -> logger.info("Severity: {} Message: {}" ,le.getLevel().getName(), le.getMessage()));
//		else
//			logger.info("NO browser log reported with requested severity level");
//	}
	
	/**
	 * @param driver instance of webdriver used for current test
	 * @param min severity severity level a log must have to be reported
	 * @param triggers keyword(s) message must contain to be reported (ANY / OR)
	 */
	//TODO
//	@Deprecated
//	public static void printLogEntries(WebDriver driver, Level severity, String... triggers) {
//		List<String> triggerList = Arrays.asList(triggers);
//		logger.info("Reporting Browser log with severity level of {} or greater and with one of the following keys: {} ..." , severity.getName(), triggerList.toString());
//		List<LogEntry> logs = fetchLogs(driver).toJson();
//		if (logs.size() > 0)
//			logs.stream().filter(le -> le.getLevel().intValue() >= severity.intValue()) 
//			.filter(le -> triggerList.stream().anyMatch(le.toString()::contains))
//			.forEach(le -> logger.info("Severity: {} Message: {}" ,le.getLevel().getName(), le.getMessage()));
//		else
//			logger.info("NO browser log reported with requested severity level and triggers");
//	}
	
	//TODO NOT TESTED YET (feature not implemented)
	@Deprecated //refine search strategy, xpath probably not working...
	public static boolean clearQuickSearch4CardsText(WebDriver driver) {
		
		WebElement content = driver.findElement(By.id("CMDBuildManagementContent"));
		//TODO refine xpath as soon as possible (search for id = 'textfield-*-trigger-clear' AND classes) 
		//id=textfield-1048-trigger-clear ,  class: -form-trigger x-form-trigger-default x-form-clear-trigger x-form-clear-trigger-default  x-form-trigger-click
		//TODO VERIFY
//		List<WebElement> clearDivs = content.findElements(By.cssSelector("div#-trigger-clear.x-form-clear-trigger-default"));
//		List<WebElement> clearDivs = content.findElements(By.xpath("//div[@id contains('-trigger-clear')]"));
//		List<WebElement> clearDivs = content.findElements(By.xpath("//div[(@id='textfield-*-trigger-clear') and (@class='x-form-clear-trigger-default')]\""));
		List<WebElement> clearDivs = content.findElements(By.className("x-form-clear-trigger-default")); //FIXME: weak search constraint
		for (WebElement cd : clearDivs) {
			if (cd.isDisplayed() && cd.isEnabled()) {
				cd.click();
				return true;
			}
		}
		return false;
	}
	
	public static boolean isDropDownActive(By dropdownIdentificationCriteria, WebDriver driver) {
		
		try {
			List<WebElement> checkIfDropDownDivPresent = driver.findElements(dropdownIdentificationCriteria);
			if (checkIfDropDownDivPresent.size() == 0)
				return false;
			WebElement dropDownDiv = checkIfDropDownDivPresent.get(0);
			List<WebElement> checkIfDDInputPresent = dropDownDiv.findElements(By.tagName("input"));
			if (checkIfDDInputPresent.size() == 0) 
				return false;
			WebElement dropDownInput = checkIfDDInputPresent.get(0);
			return dropDownInput.isEnabled() && dropDownDiv.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	public static  boolean clickOnDropdownOption(By dropdownIdentificationCriteria, String option, WebDriver driver,
			boolean defaultToFirstifOptionNotFound) {

		boolean matched = false;
		WebElement dropDownDiv = driver.findElement(dropdownIdentificationCriteria);
		WebElement dropDownInput = dropDownDiv.findElement(By.tagName("input"));
		dropDownInput.click();
		waitForLoad(driver);
		sleep(300);
		String comboList = dropDownInput.getAttribute("aria-owns");
		List<String> comboOwnedNodeIds = Splitter.on(' ').trimResults().omitEmptyStrings().splitToList((comboList));
		Optional<String> comboListId = comboOwnedNodeIds.stream().filter(id -> id.contains("picker-list")).findFirst();
		WebElement comboUL = driver.findElement(By.id(comboListId.get()));
		List<WebElement> comboOptions = comboUL.findElements(By.tagName("li"));
		if (!comboOptions.isEmpty()) {
			int selectedOptionIndex = 0;
			if (!Strings.isNullOrEmpty(option)) {
				OptionalInt matchingOptionIndex = IntStream.range(0, comboOptions.size())
						.filter(i -> option.equalsIgnoreCase(comboOptions.get(i).getText())).findFirst();
				matched = matchingOptionIndex.isPresent();
				if (matched)
					selectedOptionIndex = matchingOptionIndex.getAsInt();
			}
			WebElement clickableOption = (new WebDriverWait(driver, 5))
					.pollingEvery(100, TimeUnit.MILLISECONDS)
					.until(ExpectedConditions.elementToBeClickable(comboOptions.get(selectedOptionIndex)));
			// sleep(300);
			if (!matched && !defaultToFirstifOptionNotFound)
				throw new WebUICMDBuidTestException("Dropdown has no entry matching " + option + " and default to first option is disabled");
			clickableOption.click();
			return matched;
		} else {
			throw new WebUICMDBuidTestException("Dropdown has no clickable option!");
		}
	}
	

	public static void waitForLoad(WebDriver driver) {
		
		LocalDateTime start = LocalDateTime.now();
		waitFor(driver, (WebDriver d) -> ((JavascriptExecutor) d).executeScript("return document.readyState").equals("complete"));
		logger.warn("Wait4Load took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
	}

	public static void waitFor(WebDriver driver,ExpectedCondition<Boolean> expectedCondition) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(expectedCondition);
	}
	
	
	/**
	 * @param driver
	 * @param locator
	 * @return
	 */
	//TODO if needed build full configurable overloaded method
	public static WebElement waitForElementPresence(WebDriver driver, By locator) {
		LocalDateTime start = LocalDateTime.now();
		WebElement details = (new WebDriverWait(driver, timeoutWaitForElementPresenceSeconds)) 
				.ignoring(NoSuchElementException.class) //TODO not useful here?? 
				.pollingEvery(pollingIntervalMillis, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.presenceOfElementLocated(locator));
		logger.warn("waitForElementPresence took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return details;
	}
	
	/**
	 * @param driver
	 * @param locator
	 * @return
	 */
	//TODO if needed build full configurable overloaded method
	public static WebElement waitForElementVisibility(WebDriver driver, By locator) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement details = waitForElementPresence(driver, locator);
		details = (new WebDriverWait(driver, timeoutWaitForElementVisibilitySeconds)) 
				.ignoring(NoSuchElementException.class) 
				.pollingEvery(pollingIntervalMillis, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.visibilityOf(details)); 
		logger.warn("waitForElementVisibility took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return details;
	}
	
	public static WebElement waitForElementVisibility(WebDriver driver, WebElement webElement) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement visibleWE = (new WebDriverWait(driver, timeoutWaitForElementVisibilitySeconds)) 
				.ignoring(NoSuchElementException.class) 
				.pollingEvery(pollingIntervalMillis, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.visibilityOf(webElement)); 
		logger.warn("waitForElementVisibility took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return visibleWE;
	}
	
	
	/**
	 * @param driver
	 * @param locator
	 * @param parentLocator
	 * @return
	 */
	public static WebElement waitForPresenceOfNestedElement(WebDriver driver, By locator, By parentLocator) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement details = waitForElementPresence(driver, locator);
		details = (new WebDriverWait(driver, timeoutWaitForElementPresenceSeconds)) 
				.ignoring(NoSuchElementException.class) 
				.pollingEvery(pollingIntervalMillis, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.presenceOfNestedElementLocatedBy(parentLocator, locator)); 
		logger.warn("waitForPresenceOfNestedElement took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return details;
	}
	
	public static WebElement waitForVisibilityOfNestedElement(WebDriver driver, By locator, By parentLocator) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement nested = waitForPresenceOfNestedElement(driver, locator, parentLocator);
		nested = waitForElementVisibility(driver, nested);
		
		logger.warn("waitForVisibilityOfNestedElement took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return nested;
	}
	
	public static WebElement waitForVisibilityOfNestedElement(WebDriver driver, By locator, WebElement parent) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement nested = waitForPresenceOfNestedElement(driver, locator, parent);
		nested = waitForElementVisibility(driver, nested);
		
		logger.warn("waitForVisibilityOfNestedElement took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return nested;
	}
	
public static WebElement waitForPresenceOfNestedElement(WebDriver driver, By locator, WebElement parent) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement details = waitForElementPresence(driver, locator);
		details = (new WebDriverWait(driver, timeoutWaitForElementPresenceSeconds)) 
				.ignoring(NoSuchElementException.class) 
				.pollingEvery(pollingIntervalMillis, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.presenceOfNestedElementLocatedBy(parent, locator)); 
		logger.warn("waitForPresenceOfNestedElement took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return details;
	}
	
	@Deprecated
	private static void sleep(long millis) {
		
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}
	}

	public static void setTimeoutWaitForElementPresenceSeconds(int timeoutWaitForElementPresenceSeconds) {
		ExtjsUtils.timeoutWaitForElementPresenceSeconds = timeoutWaitForElementPresenceSeconds;
	}

	public static void setTimeoutWaitForElementVisibilitySeconds(int timeoutWaitForElementVisibilitySeconds) {
		ExtjsUtils.timeoutWaitForElementVisibilitySeconds = timeoutWaitForElementVisibilitySeconds;
	}

	public static void setPollingIntervalMillis(int pollingIntervalMillis) {
		ExtjsUtils.pollingIntervalMillis = pollingIntervalMillis;
	}
	
	//TODO detailsForm parameter could be retrieved by the method itself
	/**
	 * @param driver
	 * @param detailsForm 
	 * @param fieldName name of the dropdown field
	 * @return list of options or null if dropdown not found for fieldName
	 */
	public static @Nullable List<String> getDetailFormDropDownFieldOptions(WebDriver driver , WebElement detailsForm , String fieldName) {
		
		 waitForVisibilityOfNestedElement(driver, By.tagName("input"), detailsForm);
		 //TODO extract method (with stability check?)
		 List<WebElement> formInputs = detailsForm.findElements(By.tagName("input")); //not relaying on name attribute alone...
		 Optional<WebElement> dropDownInput = formInputs.stream().filter(we ->  ( (we.getAttribute("name").equalsIgnoreCase(fieldName)) && (we.getAttribute("role").equalsIgnoreCase("combobox")))).findFirst();
		 if (dropDownInput == null) 
			 return null;
		 dropDownInput.get().click(); 
		 waitForLoad(driver);
		 String dropdownOptionListId = dropDownInput.get().getAttribute("id").replace("-inputEl", "-picker-listEl");
		 String dropdownTrriggerId = dropDownInput.get().getAttribute("id").replace("-inputEl", "-trigger-picker");
		 WebElement trigger = waitForElementVisibility(driver, By.id(dropdownTrriggerId));
		 //sleep(1500);
		 trigger.click();
		 //sleep(2500);
		 // WebElement ulOptions = driver.findElement(By.id(dropdownOptionListId));
		 WebElement ulOptions = waitForElementPresence(driver, By.id(dropdownOptionListId));
		 List<WebElement> optionList = ulOptions.findElements(By.tagName("li"));
		 ArrayList<String> options = new ArrayList<>();
		 optionList.stream().forEach(o -> options.add(o.getText()));
		 dropDownInput.get().sendKeys(Keys.ESCAPE);
		 waitForLoad(driver);
		 return options;
	}
	
	

	
	


	

}
