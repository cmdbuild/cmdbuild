package org.cmdbuild.test.web.utils;

import static org.cmdbuild.test.web.utils.ExtjsUtils.*;
import static org.cmdbuild.test.web.utils.UILocators.*;
//import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Strings.nullToEmpty;


import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Class for easy access and search the whole content of a generic grid of
 * cards. Also provides interaction whith the grid.
 * if you manipulate interface using search, ordering and CRUD
 * operations the instance must be refreshed.
 *
 */
public class ExtjsCardGrid {

	public static class Cell {
		
		public Cell(String fieldName, String content) {
			this.fieldName = fieldName;
			this.content = content;
		}

		public String getFieldName() {
			return fieldName;
		}

		public String getContent() {
			return content;
		}
	
		public String toString() {
			return "Grid cell [" + fieldName +"," + content + "]"; 
		}
		
		public boolean matches(@Nonnull Cell cell) {
			if (this.fieldName.equals(cell.fieldName) && this.content.equals(cell.content))
				return true;
			return false;
		}
		
		public boolean matchesIgnoreCase(@Nonnull Cell cell) {
			if (this.fieldName.equalsIgnoreCase(cell.fieldName) && this.content.equalsIgnoreCase(cell.content))
				return true;
			return false;
		}
		
		private String fieldName;
		private String content;
	}
	
	//Locators (configurable?)
	protected static By locatorCMDBuildManagementContent = By.id("CMDBuildManagementContent");
	protected static By locatorGridHeader = By.className("x-grid-header-ct");
	protected static By locatorColumnHeader = By.className("x-leaf-column-header");
	protected static By locatorRowsTable = By.tagName("table");
	protected static By locatorCardsToolbar = By.className("x-toolbar-default");

	protected ArtificialTestDelayListener artificialTestTimeListener;
	protected final WebDriver driver;
	protected  List<String> fields = new ArrayList<>();
	protected Map<String, String> fieldIds = new HashMap<>(); // maps dynamic ids assigned by extjs to fields (headers)
	protected List<List<Cell>> rows = new ArrayList<>();
	
	//Defaults
	protected static int defaultWaitForGridIsStablePollingIntervalMillis = 50;
	protected static int defaultMaxIterationsWithoutChange = 20;
	protected static int defaultMaxIterations = 60;

	public ExtjsCardGrid(@Nonnull WebDriver driver) {
		this.driver = driver;
	}
	
	public ExtjsCardGrid(@Nonnull WebDriver driver, ArtificialTestDelayListener artificialTestTimeListener) {
		this.driver = driver;
		this.artificialTestTimeListener = artificialTestTimeListener;
	}
	
	
	/**
	 * @param listener only one listener can be associated
	 */
	public void  registerArtificialTestTimeListener(ArtificialTestDelayListener listener) {
		this.artificialTestTimeListener = listener;
	}
	

	public List<List<Cell>> getRows() {
		return rows;
	}

	public List<String> getFields() {
		return fields;
	}
	
	public boolean containsAnyCells(Cell... cells) {
		if (getIndexOfRowsCointainingAnyCells(cells).size() > 0)
			return true;
		return false;
	}
	
	public boolean containsAnyCells(List<FormField> fields) {	
		List<Cell> cells = formfieldsToCells(fields);
		if (getIndexOfRowsCointainingAnyCells((Cell[]) cells.toArray()).size() > 0)
			return true;
		return false;
	}
	
	public boolean hasRowContainingAllCells(Cell...cells) {
		return  ! getRowsContainingAllCells(cells).isEmpty();
	}
	
	public boolean hasRowContainingAllFields(List<FormField> fields) {
		Cell[] cells = formfieldsToCellArray(fields);
		return  ! getRowsContainingAllCells(cells).isEmpty();
	}
	
	public List<List<Cell>> getRowsContainingAnyCells(Cell...cells) {
		
		List<List<Cell>> matchingRows = new ArrayList<>();
		for (int i = 0; i < rows.size(); i++) {
			List<Cell> row = rows.get(i);
			if (rowContainingAnyCells(row, cells))
				matchingRows.add(row);
		}
		return matchingRows;
	}
	
	public List<List<Cell>> getRowsContainingAllCells(Cell...cells) {
		
		List<List<Cell>> matchingRows = new ArrayList<>();
		for (int i = 0; i < rows.size(); i++) {
			List<Cell> row = rows.get(i);
			if (rowContainingAllCells(row, cells))
				matchingRows.add(row);
		}
		return matchingRows;
	}
	
	
	public List<Integer> getIndexOfRowsCointainingAnyCells(Cell... cells) {
		
		List<Integer> matchingRows = new ArrayList<>();
		for (int i = 0; i < rows.size(); i++) {
			List<Cell> row = rows.get(i);
			if (rowContainingAnyCells(row, cells))
				matchingRows.add(new Integer(i));
		}
		return matchingRows;
	}
	
	public List<Integer> getIndexOfRowsCointainingAllCells(Cell... cells) {
		
		List<Integer> matchingRows = new ArrayList<>();
		for (int i = 0; i < rows.size(); i++) {
			List<Cell> row = rows.get(i);
			if (rowContainingAllCells(row, cells))
				matchingRows.add(new Integer(i));
		}
		return matchingRows;
	}
	
	private boolean rowContainingAnyCells(List<Cell> row, Cell... searchCells) {
		for (Cell cell : row) {
			for (Cell sc : searchCells) {
				if (cell.matches(sc))
					return true;
			}
		}
		return false;
	}
	
	private boolean rowContainingAllCells(List<Cell> row, Cell... searchCells) {
		
		for (Cell cell : searchCells) {
			boolean found = false;
			for (Cell sc : row) {
				if (cell.matches(sc)) {
					found = true;
					break;
				}
			}
			if  (!found) return false;
		}
		return true;
	}
	
	//refactoring...
	public ExtjsCardGrid refresh() {
		
		rows.clear();
		fields.clear();
		fieldIds.clear();
		
		waitForLoad(driver);//almost useless
		
		//ExtjsCardGrid grid = this; //FIXME when finished refactoring (and code is stable)
		WebElement cmdbuildManagementContentSection = waitForElementVisibility(driver, locatorCMDBuildManagementContent);//  findElementById(driver,"CMDBuildManagementContent");
		WebElement gridHeaderWE = waitForPresenceOfNestedElement(driver,locatorGridHeader , locatorCMDBuildManagementContent);
		waitForElementVisibility(driver, gridHeaderWE);
		waitForElementVisibility(driver, 
				waitForPresenceOfNestedElement(driver, locatorColumnHeader  , locatorGridHeader));
		
		//waitForElementVisibility(driver, locatorGridHeader); //TODO does not guarantee it's CMDBuildManagementContent specific
		WebElement headerSection = cmdbuildManagementContentSection.findElement(locatorGridHeader);
		// extract headers (fields)
		List<WebElement> extHeaders = headerSection.findElements(locatorColumnHeader);
		for (WebElement extHeader : extHeaders) {
			WebElement headerTextSpan = extHeader.findElement(By.className("x-column-header-text-inner"));
			String fieldName = nullToEmpty(headerTextSpan.getText()).trim(); //found " " case as well...
			String fieldId = headerTextSpan.getAttribute("id").replace("-textInnerEl", "");
			if ((! fieldName.isEmpty()) ) {
				fields.add(fieldName);
				fieldIds.put(fieldId, fieldName);
			}
		}
		// extract cell content
		waitForElementVisibility(driver, waitForPresenceOfNestedElement(driver, locatorRowsTable, locatorCMDBuildManagementContent));
		
		//FIXME: code below is experimental and must moved out
		//FIXME: PAY ATTENTION: from runs there is evidence that we are still working with previous grid at this point
		//FIXME: !!!!!!!!!!!!
		//FIXME: So we need to move this checks to clickOnSideNav... or create a method to make sure we are not refreshing grid
		
		//TODO remove when moved out
//		int previoustableSize = 0;
//		int iterationsWithoutChange = 0;
//		for (int i = 0; iterationsWithoutChange <10 && i < 30 ; i++) { //TODO DEBUG REMOVE
//			List<WebElement> gridRows = cmdbuildManagementContentSection.findElements(locatorRowsTable);
//			logger.warn("gridRows found: {}", gridRows.size());
//			if (gridRows.size()==previoustableSize) {
//				iterationsWithoutChange++;
//			} else {
//				previoustableSize = gridRows.size();
//				iterationsWithoutChange = 0;
//			}
//			
//			try {
//				Thread.currentThread().sleep(100);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
		List<WebElement> gridRows = cmdbuildManagementContentSection.findElements(locatorRowsTable);
		for (WebElement gridRow : gridRows) {
			List<Cell> row = new ArrayList<>();
			rows.add(row);
			List<WebElement> gridCells = gridRow.findElements(By.tagName("td"));
			for (WebElement gridCell : gridCells) {
				try {// data-columnid not always present
					String field = nullToEmpty(fieldIds.get(gridCell.getAttribute("data-columnid"))).trim();//.replace("gridcolumn-", ""));
					if (! field.isEmpty()) {
						String content = gridCell.getText();
						row.add(new Cell(field, content));
					}
				} catch (Exception e) {//Silent
				}
			}
		}
		return this;
	}
	
	/**
	 * @param rowIndex zero based
	 * @return grid row webelement
	 */
	public WebElement expandGridRow(int rowIndex) {
		List<WebElement> gridRows = contentSection().findElements(locatorRowsTable);
		WebElement gridRow = gridRows.get(rowIndex);
		WebElement expander = gridRow.findElement(By.className("x-grid-row-expander"));
		expander.click();
		return gridRow;
	}
	
	public boolean isGridRowExpanded(int rowIndex) {
		List<WebElement> gridRows = contentSection().findElements(locatorRowsTable);
		WebElement gridRow = gridRows.get(rowIndex);
		if (gridRow.getAttribute("class").contains("x-grid-item-selected"))
			return true;
		else
			return false;
	}
	
	//TODO: we cannot easily identify action buttons using attributes (data-qtip , aria-label="Edit card"...) because of translation
	// nor we can using position (perhaps if action is not allowed for a user we don't find it at all)
	// so we go for for icon name which should be stable
	//UPDATE: no icon found here. ???, so falling back on aria-label
	//FIXME: find a better way for action identification
	//TODO static? indexed?
	//FIXME: must return details form webelement
	public void editCard(WebElement gridRowWebElement) {
		LocalDateTime start = LocalDateTime.now();
		WebElement editButton = waitForElementVisibility(driver, By.xpath("//div[@aria-label='Edit card']")); //works only for English but we can force language
		//Consider using translation if you need a more general condition
		//TODO if possible add condition on role=button
		editButton.click();
		logger.warn("editCard took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		
//		List<WebElement> actions = gridRowWebElement.findElements(By.xpath("//div[@role='button']"));
//		for (WebElement action : actions) {
//			//DEBUG
//			logger.info("Found card details button: {} {}" , action.getAttribute("aria-label") ,action.getAttribute("data-qtip"));
//			if ("Edit card".equalsIgnoreCase(action.getAttribute("aria-label")) ) {
//				action.click();
//				break;
//			}
		
		
////			WebElement icondDiv = action.findElement(By.xpath("//*[@style='font-family:FontAwesome;']"));
////			String icon = icondDiv.getText();
////			logger.error("Icon: {}" , icon);
////			icondDiv = action.findElement(By.className("fa"));
////			logger.error("Icon: {}" , icondDiv.getText());
////			
	}
	
	

	/**
	 * TODO modelled from editCard, needs a revision 
	 *
	 * @param gridRowWebElement row as returned from others methods of this class
	 * @return CMDBuildManagementDetailsWindow element for further actions
	 */
	public WebElement openCard(WebElement gridRowWebElement) {
		LocalDateTime start = LocalDateTime.now();
		WebElement openButton = waitForElementVisibility(driver, By.xpath("//div[@aria-label='Open card']")); //works only for English but we can force language
		openButton.click();
		WebElement cmdbuildManagementDetailsWindow = waitForElementVisibility(driver, cmdbuildManagementDetailsWindowLocator());
		logger.warn("openCard took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		return cmdbuildManagementDetailsWindow;
	}
	
	/**
	 * @param columnName 
	 * 
	 * fails if not found
	 */
	public void clickOnGridHeader(@Nonnull String  columnName) {
		WebElement gridHeader = contentSection().findElement(ByClassName.className("x-grid-header-ct"));
		List<WebElement> columnHeaders = gridHeader.findElements(ByClassName.className("x-column-header"));
		Optional<WebElement> header = columnHeaders.stream().filter(h -> columnName.equals(h.getText())).findFirst();
		header.get().click();
	}

	public int rows() {
		return getRows().size();
	}

	public static ExtjsCardGrid extract(WebDriver driver) {

		ExtjsCardGrid grid = new ExtjsCardGrid(driver);
		LocalDateTime start = LocalDateTime.now();
		grid.waitForGridIsStable();
		logger.info("GRID STABILITY took: {} ms to complete", 
				ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		grid.refresh();
		return grid;
	}
	
	public static ExtjsCardGrid extract(WebDriver driver, ArtificialTestDelayListener listener) {
		
		ExtjsCardGrid grid = new ExtjsCardGrid(driver, listener);
		LocalDateTime start = LocalDateTime.now();
		long artificialTime  = grid.waitForGridIsStable();
		logger.info("GRID STABILITY took: {} ms to complete, of which {} ms weere artificially introduced by stability check to  assure grid stability", 
				ChronoUnit.MILLIS.between(start,LocalDateTime.now()) ,
				artificialTime);
		grid.refresh();
		return grid;
	}
	
	private WebElement contentSection() {
		return driver.findElement(locatorCMDBuildManagementContent);
	}


	public int getColumnIndexOfColumn(String columnName) {
		return fields.indexOf(columnName);
	}
	
	/**
	 * 
	 * fails if not found
	 */
	public WebElement getHeaderWebElement(String columnName) {
		WebElement gridHeader = contentSection().findElement(ByClassName.className("x-grid-header-ct"));
		List<WebElement> columnHeaders = gridHeader.findElements(ByClassName.className("x-column-header"));
		Optional<WebElement> header = columnHeaders.stream().filter(h -> columnName.equals(h.getText())).findFirst();
		return header.get();
	}
	
	/**
	 * @return time spent (milliseconds) to assure the grid is stable, not imputable to client
	 * 
	 */
	public long waitForGridIsStable() {
		return waitForGridIsStable( defaultWaitForGridIsStablePollingIntervalMillis, defaultMaxIterationsWithoutChange, defaultMaxIterations);
	}
	/**
	 * @return time spent (milliseconds) to assure the grid is stable, not imputable to client
	 * 
	 */
	public long waitForGridIsStable(int pollingIntervalMillis, int maxIterationsWithoutChange, int maxIterations) {
		
		waitForLoad(driver);//almost useless
//		LocalDateTime stabilityCheckStart = LocalDateTime.now();
		
		WebElement cmdbuildManagementContentSection = waitForElementVisibility(driver, locatorCMDBuildManagementContent);
		WebElement gridHeaderWE = waitForPresenceOfNestedElement(driver,locatorGridHeader , locatorCMDBuildManagementContent);
		waitForElementVisibility(driver, gridHeaderWE);
		waitForElementVisibility(driver, 
				waitForPresenceOfNestedElement(driver, locatorColumnHeader  , locatorGridHeader));
		
		int previoustableSize = 0;
		int iterationsWithoutChange = 0;
		int lastIterationWithChange = 0;
		int i; 
		for (i =0; iterationsWithoutChange <maxIterationsWithoutChange && i < maxIterations ; i++) { 
			List<WebElement> gridRows = cmdbuildManagementContentSection.findElements(locatorRowsTable);
			if (gridRows.size()==previoustableSize) {
				iterationsWithoutChange++;
			} else {
				logger.info("Checking content grid stability: grid rows number change detected: from {} to {}", previoustableSize, gridRows.size());
				previoustableSize = gridRows.size();
				iterationsWithoutChange = 0;
				lastIterationWithChange = i;
			}
			try {
				Thread.sleep(pollingIntervalMillis);
			} catch (InterruptedException e) {	}
		}
		
		long timeArtificiallyIntroducedByStabilityCheck = 0;
	//	long timeSpent = ChronoUnit.MILLIS.between(stabilityCheckStart, LocalDateTime.now());
		if (lastIterationWithChange > 0 ) 
			timeArtificiallyIntroducedByStabilityCheck =  (i-lastIterationWithChange)* pollingIntervalMillis;
		timeArtificiallyIntroducedByStabilityCheck=  Math.max(timeArtificiallyIntroducedByStabilityCheck, 0);
		if (artificialTestTimeListener!=null)
			artificialTestTimeListener.notifyArtificialDelayTime(timeArtificiallyIntroducedByStabilityCheck);
		return timeArtificiallyIntroducedByStabilityCheck;
	}

	public void closeCardDetails() {
		WebElement closeButton = waitForVisibilityOfNestedElement(driver, cardDetailsCloseButton(), cmdbuildManagementDetailsWindowLocator());
		closeButton.click();
	}

	public void deleteCard(WebElement gridRow) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement openButton = waitForElementVisibility(driver, By.xpath("//div[@aria-label='Delete card']")); //works only for English but we can force language
		openButton.click();
		logger.warn("DeleteCard took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		
	}
	
	public void cloneCard(WebElement gridRow) {
		
		LocalDateTime start = LocalDateTime.now();
		WebElement openButton = waitForElementVisibility(driver, By.xpath("//div[@aria-label='Clone card']")); //works only for English but we can force language
		openButton.click();
		logger.warn("CloneCard took {} ms" ,ChronoUnit.MILLIS.between(start, LocalDateTime.now()));
		
	}
	
	//TODO check if move to ExtjsUtils
	/**
	 * @return Card details form. Waits for opening of the details form.
	 */
	public WebElement newCard() {
		
		WebElement toolbar = waitForVisibilityOfNestedElement(driver,  locatorCardsToolbar, locatorCMDBuildManagementContent);
		WebElement addCardButton = waitForVisibilityOfNestedElement(driver, By.tagName("a") ,toolbar );
		logger.warn("Add Card button text detected: {}" , addCardButton.getText()); //TODO remove
		addCardButton.click();
		return getManagementDetailsWindow(driver);
	}

	//FIXME: TO BE TESTED
	public void fillForm(List<FormField> testCreateCardFormFields) {
		
		WebElement detailsForm = ExtjsUtils.getManagementDetailsWindow(driver);
		for (FormField ff : testCreateCardFormFields) {
			clearFormTextField(detailsForm, ff.getName());
			fillFormTextField(detailsForm, ff.getName(), ff.getContent());
		}
		
	}
	
	
	///STATIC METHODS
	
	//TODO make public?
	public static String getContent(@Nonnull List<Cell> row, @Nonnull String fieldName) throws NoSuchElementException {
		return row.stream().filter(c -> fieldName.equalsIgnoreCase(c.getFieldName())).findFirst().get().getContent();
	}
	private static Cell formFieldToCell (@Nonnull FormField ff) {
		return new Cell(ff.getName(), ff.getContent());
	}
	
	private static List<Cell> formfieldsToCells(@Nonnull List<FormField> fields) {
		return fields.stream().map(ff -> new Cell(ff.getName(),ff.getContent())).collect(Collectors.toList());
	}
	private static @Nonnull Cell[] formfieldsToCellArray(@Nonnull List<FormField> fields) {
		Cell[] cells = new Cell[fields.size()];
		IntStream.range(0, fields.size()).forEach(i -> cells[i] = formFieldToCell(fields.get(i)));
		return cells;
	}
	
	


}
