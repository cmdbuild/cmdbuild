package org.cmdbuild.test.web.utils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

/**
 * Use with card details form.
 * Maybe can be specialized to address other use cases.
 *
 */
public interface FormField {
	
	//TODO check if differentiation beetween text and combos is needed 
	
	String getName();
	String getContent();
	
	static FormField of(String name, String content) {
		return new FormField() {
			
			
			@Override
			public String getName() {
				return name;
			}
			
			@Override
			public String getContent() {
				return content;
			}
		};
	}
	
	/**
	 * @param fieldNames Not null list of field names
	 * @param fieldContents Not null list of field contents
	 * @return
	 * 
	 * fieldNames and fieldContents must have the same dimension, otherwise an IllegalArgumentException is thrown
	 */
	static List<FormField> listOf(@Nonnull List<String> fieldNames, @Nonnull List<String> fieldContents) {
		
		if (fieldNames == null || fieldContents == null || fieldNames.size() != fieldContents.size())
			throw new IllegalArgumentException("Field names and contents must be not null and have the same size");
		ArrayList<FormField> fields = new ArrayList<>();
		for (int f = 0; f < fieldNames.size(); f++) {
			fields.add(of(fieldNames.get(f), fieldContents.get(f)));
		}
		return  fields;
	}

}
