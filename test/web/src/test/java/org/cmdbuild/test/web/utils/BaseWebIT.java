package org.cmdbuild.test.web.utils;

import static org.cmdbuild.test.web.utils.ExtjsUtils.*;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import javax.annotation.Nonnull;

import org.cmdbuild.client.rest.RestClient;
import org.cmdbuild.test.web.WebUICMDBuidTestException;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.dao.DataAccessException;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import org.cmdbuild.client.rest.RestClientImpl;

public class BaseWebIT extends AbstractWebIT {

	public String getBaseUrlUI() {
		return getBaseUrl() + "ui";
	}

	protected ClientLog currentClientLog = null;
	protected String lastCallerMethod = null;
	/**
	 * This solution implies only one test of this class instance is run at the same time
	 */
	protected Set<String> runningTestTouchedClasses = new HashSet<>();

	private RestClient restClient;
	private WebUILoginCredentials clientCredentials = defaultAdminCredentialsInstance();

	protected By languageDropDownLocator = By.xpath("//div[@data-testid='login-inputlanguage']");
	protected By roleDropDownLocator = By.xpath("//div[@data-testid='login-inputrole']");
	protected By tenantDropDownLocator = By.xpath("//div[@data-testid='login-inputtenant']");
	protected By managementContentLocator = By.id("CMDBuildManagementContent");
	protected By administrationContentLocator = By.id("CMDBuildAdministrationContent");

	public static final long SECONDS = 1000L;

	//TODO polish using dropdown methods
	protected void login(WebUILoginCredentials credentials) {

		logger.info("WebUITest Loggin in...");

		//FIRST STEP (always present)
		getUrl(getBaseUrlUI());
		waitForLoad();
		sleepDeprecated();
		RemoteWebDriver driver = getDriver();
		WebElement usernameDiv = driver.findElement(By.xpath("//div[@data-testid='login-inputusername']"));
		WebElement usernameField = usernameDiv.findElement(By.tagName("input"));
		usernameField.sendKeys(credentials.getUserName());
		WebElement passwordDiv = driver.findElement(By.xpath("//div[@data-testid='login-inputpassword']"));
		WebElement passwordField = passwordDiv.findElement(By.tagName("input"));
		passwordField.sendKeys(credentials.getPassword());
		WebElement languageDiv = driver.findElement(By.xpath("//div[@data-testid='login-inputlanguage']"));
		WebElement languageInput = languageDiv.findElement(By.tagName("input"));
		languageInput.click();
		sleepDeprecated(1000);
		// select language option
		//FIXME: currently first (and only)option is selected (English expected). Wanted option selection must be enforced as soon as possible
		// when combo selection becomes stable in order to avoid unexpected behaviours
		String comboList = languageInput.getAttribute("aria-owns");
		List<String> comboOwnedNodeIds = Splitter.on(' ').trimResults().omitEmptyStrings().splitToList((comboList));
		Optional<String> comboListId = comboOwnedNodeIds.stream().filter(id -> id.contains("picker-list")).findFirst();
		WebElement comboListUL = driver.findElementById(comboListId.get());
		List<WebElement> options = comboListUL.findElements(By.tagName("li"));
		if (!options.isEmpty()) {
			int selectedOptionIndex = 0;
			if (!Strings.isNullOrEmpty(credentials.getLanguage())) {
				OptionalInt matchingOptionIndex = IntStream.range(0, options.size())
						.filter(i -> credentials.getLanguage().equalsIgnoreCase(options.get(i).getText())).findFirst();
				assertTrue(matchingOptionIndex.isPresent());
				selectedOptionIndex = matchingOptionIndex.getAsInt();
			}
			WebElement clickableOption = (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.elementToBeClickable(options.get(selectedOptionIndex)));
			//sleep(300);
			clickableOption.click();
		}
		WebElement loginButtonDiv = waitForElementPresence(By.xpath("//div[@data-testid='login-btnlogin']"));
//		WebElement loginButtonDiv = driver.findElement(By.xpath("//div[@data-testid='login-btnlogin']"));
		// WebElement loginButton = languageDiv.findElement(By.tagName("button"));
		loginButtonDiv.click();

		waitForLoad();

		(new WebDriverWait(driver, timeoutWaitForElementPresenceSeconds)) //TODO EXTERNALIZE
				.ignoring(NoSuchElementException.class) //TODO not useful here?? 
				.pollingEvery(pollingIntervalMillis, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.or(
						ExpectedConditions.visibilityOfElementLocated(roleDropDownLocator),
						ExpectedConditions.visibilityOfElementLocated(tenantDropDownLocator),
						ExpectedConditions.presenceOfElementLocated(managementContentLocator),
						ExpectedConditions.presenceOfElementLocated(administrationContentLocator)
				));

		//SECOND STEP (present only if Tenant / Roles choices are available
		String landingUrlFirstStep = driver.getCurrentUrl();
		List<WebElement> userNameStillPresent = driver.findElements(By.xpath("//div[@data-testid='login-inputusername']"));
		if (userNameStillPresent.size() != 0) {
			assertTrue(landingUrlFirstStep.contains("#login"));

			if (isDropDownActive(roleDropDownLocator, getDriver())) {
				clickOnDropdownOption(roleDropDownLocator, credentials.getRole(), getDriver(), true);
			}

			if (isDropDownActive(tenantDropDownLocator, getDriver())) {
				clickOnDropdownOption(tenantDropDownLocator, credentials.getTenant(), getDriver(), true);
			}

			waitForElementPresence(By.xpath("//div[@data-testid='login-btnlogin']")).click();
			waitForLoad();
		}

		Boolean notLoginWebElement = (new WebDriverWait(driver, timeoutWaitForElementPresenceSeconds)) //TODO EXTERNALIZE
				.ignoring(NoSuchElementException.class) //TODO not useful here?? 
				.pollingEvery(pollingIntervalMillis, TimeUnit.MILLISECONDS)
				.until(ExpectedConditions.or(
						ExpectedConditions.presenceOfElementLocated(managementContentLocator),
						ExpectedConditions.presenceOfElementLocated(administrationContentLocator)
				));
		//CHECKS 
		assertTrue(Boolean.TRUE.equals(notLoginWebElement));
		String landingUrlFinal = driver.getCurrentUrl();
		Assert.assertFalse(landingUrlFinal.contains("#login"));
		Assert.assertTrue(landingUrlFinal.contains("/cmdbuild/ui/#classes") || landingUrlFinal.contains("/cmdbuild/ui/#management"));
		logger.info("WebUITest Logged in!");

	}

	//TODO move to extjsutils?
	protected void logout() {
		WebElement logoutDiv = findElementByXPath("//div[@data-testid='header-logout']");
		logoutDiv.click();
	}

//	protected boolean clickOnDropdownOption(By dropdownIdentificationCriteria, String option, WebDriver driver,
//			boolean defaultToFirstifNotFound) {
//
//		boolean matched = false;
//		WebElement dropDownDiv = driver.findElement(dropdownIdentificationCriteria);
//		WebElement dropDownInput = dropDownDiv.findElement(By.tagName("input"));
//		dropDownInput.click();
//		waitForLoad();
//		sleepDeprecated(300);
//		String comboList = dropDownInput.getAttribute("aria-owns");
//		List<String> comboOwnedNodeIds = Splitter.on(' ').trimResults().omitEmptyStrings().splitToList((comboList));
//		Optional<String> comboListId = comboOwnedNodeIds.stream().filter(id -> id.contains("picker-list")).findFirst();
//		WebElement comboUL = driver.findElement(By.id(comboListId.get()));
//		List<WebElement> comboOptions = comboUL.findElements(By.tagName("li"));
//		if (!comboOptions.isEmpty()) {
//			int selectedOptionIndex = 0;
//			if (!Strings.isNullOrEmpty(option)) {
//				OptionalInt matchingOptionIndex = IntStream.range(0, comboOptions.size())
//						.filter(i -> option.equalsIgnoreCase(comboOptions.get(i).getText())).findFirst();
//				matched = matchingOptionIndex.isPresent();
//				if (matched)
//					selectedOptionIndex = matchingOptionIndex.getAsInt();
//			}
//			WebElement clickableOption = (new WebDriverWait(driver, 5))
//					.until(ExpectedConditions.elementToBeClickable(comboOptions.get(selectedOptionIndex)));
//			// sleep(300);
//			if (!matched && !defaultToFirstifNotFound)
//				throw new WebUICMDBuidTestException("Dropdown has no entry matching " + option + " and default to first option is disabled");
//			clickableOption.click();
//			return matched;
//		} else {
//			throw new WebUICMDBuidTestException("Dropdown has no clickable option!");
//		}
//	}
	/**
	 * @param xPath
	 * @return found WebElement, waiting for the element to be present 
	 */
	protected WebElement findElementByXPath(String xPath) {
		return ExtjsUtils.findElementByXPath(getDriver(), xPath);
	}

	/**
	 * @param testId
	 * @return found WebElement, waiting for the element to be present 
	 */
	protected WebElement findElementByTestId(String testId) {
		return ExtjsUtils.findElementByTestId(getDriver(), testId);
	}

	/**
	 * @param testId
	 * @return found WebElement, waiting for the element to be present 
	 */
	protected WebElement findElementById(String testId) {
		return ExtjsUtils.findElementById(getDriver(), testId);
	}

	/**
	 * @param clickableWebElement
	 * @return the same clickableWebElement, when the WebElement is ready to be clicked
	 */
	protected WebElement fetchClickableWebElement(WebElement clickableWebElement) {
		return ExtjsUtils.fetchClickableWebElement(getDriver(), clickableWebElement);
	}

	protected String getCurrentUrl() {
		waitForLoad();
		return getDriver().getCurrentUrl();
	}

	//FIXME use callerMethod() ???
	@Deprecated //REMOVE, use testStart(UITestContext)
	protected UITestContext testStart() {

		currentClientLog = null;
		this.logTestPhase(Thread.currentThread().getStackTrace()[2].getMethodName(), "START");

		return new UITestContext().withWebDriver(getDriver());

	}

	/**
	 * @param testContex
	 * @return true on initialization successfull (to be used with asserts)
	 */
	protected boolean testStart(UITestContext testContex) {
		currentClientLog = null;
		//TODO substitute with id from context
		this.logTestPhase(Thread.currentThread().getStackTrace()[2].getMethodName(), "START");
		if (testContex != null) {
			runningTestTouchedClasses.addAll(testContex.getTouchedClasses());
			createTouchedClassesSnapshot(testContex.getTouchedClasses());
			testContex.setStart(LocalDateTime.now());
		}
		return true;
	}

	//FIXME extract suffix of temporary table
	protected void createTouchedClassesSnapshot(Set<String> touchedClasses) throws WebUICMDBuidTestException {

		for (String className : touchedClasses) {
			//CHECK name is valid
			logger.info("Creating snapshot for class: {}", className);
			Connection conn = null;
			Statement stm = null;
			boolean hasHistory = hasClassHistory(className);
			try {
				conn = getJdbcTemplate().getDataSource().getConnection();
				conn.setAutoCommit(false);
				stm = conn.createStatement();
				stm.execute("BEGIN;");
				//stm.execute("set session_replication_role = replica;");
				stm.execute("CREATE UNLOGGED TABLE IF NOT EXISTS \"?_test_copy\" AS (SELECT * FROM \"?\");".replace("?", className));
				stm.execute("COMMIT");
				//FIXME: should be executed only if class has history. Simple classes...?)
				if (hasHistory) {
					stm.execute("BEGIN;");
					stm.execute("CREATE UNLOGGED TABLE IF NOT EXISTS \"?_history_test_copy\" AS (SELECT * FROM \"?_history\");".replace("?", className));
					stm.execute("COMMIT");
				}
				conn.commit(); //SHOULD BE REDUNDANT (check please)
				logger.info("Snapshot for class: {} successfully created", className);
			} catch (Exception e) {
				logger.error("Could not create a snapshot of db tables for class{}", className);
				throw new WebUICMDBuidTestException("Could not create a snapshot of db tables for class " + className + ".   Cause: " + e.toString());
			} finally { // FIXME use safer methods
				if (stm != null) {
					try {
						stm.close();
					} catch (SQLException e) {
					}
				}
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						logger.error("Could not close connection!");
					}
				}
			}
		}
	}

	private boolean hasClassHistory(String className) {

		try {
			if (getJdbcTemplate().queryForObject("select _cm_comment_for_class('?');".replace("?", className), String.class).contains("TYPE: simpleclass")) {
				return false;
			}
		} catch (DataAccessException e) {
			logger.error("REMOVE ME PLEASE");
		}
		return true;
	}

	//FIXME extract suffix of temporary tables
	protected void restoreTouchedClassesSnapshot(Set<String> touchedClasses) throws WebUICMDBuidTestException {

		for (String className : touchedClasses) {
			//CHECK name is valid
			logger.info("Restoring snapshot for class: {} ", className);
			Connection conn = null;
			Statement stm = null;
			LocalDateTime restoreTMS = LocalDateTime.now();
			boolean hasHistory = hasClassHistory(className);
			try {
				conn = getJdbcTemplate().getDataSource().getConnection();
				conn.setAutoCommit(false);
				stm = conn.createStatement();
				stm.execute("BEGIN;");
				stm.execute("set session_replication_role = replica;");

				if (hasHistory) {
					stm.execute("TRUNCATE TABLE \"?_history\" CONTINUE IDENTITY".replace("?", className));
				}
				stm.execute("TRUNCATE TABLE \"?\" CONTINUE IDENTITY".replace("?", className));

				stm.execute("INSERT INTO \"?\" SELECT * FROM \"?_test_copy\";".replace("?", className));
				if (hasHistory) {
					stm.execute("INSERT INTO \"?_history\" SELECT * FROM \"?_history_test_copy\";".replace("?", className));
				}

				stm.execute("DROP TABLE IF EXISTS \"?_history_test_copy\";".replace("?", className));
				stm.execute("DROP TABLE IF EXISTS \"?_test_copy\";".replace("?", className));

				stm.execute("set session_replication_role = DEFAULT;");
				stm.execute("COMMIT");
				conn.commit(); //SHOULD BE REDUNDANT (check please)
				logger.info("Restored snapshot for class {} in {} ms", className, ChronoUnit.MILLIS.between(restoreTMS, LocalDateTime.now()));
			} catch (Exception e) {
				logger.error("Could not restore the snapshot of db tables for class{}", className);
				throw new WebUICMDBuidTestException("Could not restore the snapshot of db tables for class " + className + ".   Cause: " + e.toString());
			} finally { // FIXME use safer methods
				if (stm != null) {
					try {
						stm.close();
					} catch (SQLException e) {
					}
				}
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						logger.error("Could not close connection!");
					}
				}
			}
		}
	}

	protected void testEnd() {

		logTestPhase(callerMethod(), " END");
//		try { 
//			fetchClientLog().printLogs(logger, null);
//		} catch (Exception e) {
//			logger.error("Printing client logs failed");
//		}
		close();
	}

	//TODO check duration  / client errors here
	protected void testEnd(@Nonnull UITestContext context) {
		context.testEnd();
		logger.info("Test {} took {} to complete (full time: {} )", context.getDescription(), context.getMeasuredTimeSpent(), context.getFullTimeSpent());
		checkTest(context); //move to @after
		testEnd();
	}

	//FIX: make private
	protected void checkTest(@Nonnull UITestContext context) {
		boolean passesRules = (context.passesRules());
//		restoreTouchedClassesSnapshot(context.getTouchedClasses()); moved into @After...
		assertTrue(passesRules);

	}

	protected void checkClient() {
		assertTrue(passesDefaultClientLogsChecks());
	}

	//TODO add parameters to add checks
//	protected void checkClient(boolean performDefaultChecksAgainstClientLogs) {
//		ClientLog client = fetchClientLog();
//		if (performDefaultChecksAgainstClientLogs) {//TODO refactor move out
//			assertTrue(passesDefaultClientLogs());
//		}
//	}
	//TODO: manage log level?
	protected void logTestPhase(String testName, String phase) {

		StringBuilder message = new StringBuilder("WebUITest - UIBasics ");
		message.append(testName).append(" ").append(phase);
		logger.info(message.toString());
	}

	//Override in subclasses
	//Make configurable?
	//TODO: use rules instead
	/**
	 * @return true if default checks passed. Very hard to pass if not overridden.
	 * 
	 * Please override in subclasses according to specific needs
	 */
	@Deprecated
	protected boolean passesDefaultClientLogsChecks() {
		ClientLog log = fetchClientLog().printLogs(logger, null).supress("favicon");
		if(log.success()){
			return true;
		}else{
			logger.warn("found {} errors in client log",log.errors());
			return false;
		}
	}

	/**
	 * Preferred way to retrieve client log.
	 * 
	 * @return ClientLog immutable instance
	 */
	protected ClientLog fetchClientLog() {

		if (currentClientLog == null || (!callerMethod().equals(lastCallerMethod))) {
			currentClientLog = ClientLog.fetchFromWebDriver(getDriver());
		}
		lastCallerMethod = callerMethod();
		return currentClientLog;
	}

	protected String callerMethod() {
		return Thread.currentThread().getStackTrace()[2].getMethodName();
	}

	@Deprecated
	protected void sleepDeprecated() {
		sleep();
	}

	@Deprecated
	protected void sleepDeprecated(long time) {
		sleep(time);
	}

	/**
	 * 
	 * Pause for the given amount of milliseconds, notifying the test context to deduct this pause from test completion time
	 * 
	 * @param millis Pause length
	 * @params context Test context to be notified
	 */
	protected void artificialSleep(long millis, UITestContext context) {
		sleep(millis);
		if (context != null) {
			context.notifyArtificialDelayTime(millis);
		}
	}

	/**
	 * 
	 * Convenience method for getting the default (admin) rest client.
	 * Use WebServiceUtilities to obtain a client with specific credentials
	 * @return rest client logged in with default admin credentials
	 */
	protected RestClient getRestClient() {
		if (restClient == null) {
			restClient = getClientInstance(clientCredentials);
		}
		return restClient;
	}

	/**
	 * @param clientCredentials to be used for default rest client instantiation
	 */
	protected void setClientCredentials(WebUILoginCredentials clientCredentials) {
		this.clientCredentials = clientCredentials;
	}

	/**
	 * @return new admin credentials instance that can be manipulated
	 */
	protected WebUILoginCredentials defaultAdminCredentialsInstance() {
		return WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN);
	}

	/**
	 * @return new demouser credentials instance that can be manipulated
	 */
	protected WebUILoginCredentials defaultDemoUserCredentialsInstance() {
		return WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_DEMOUSER);
	}

	@Override
	public void waitForLoad() {
		LocalDateTime start = LocalDateTime.now();
		//super.waitForLoad();
		//waitFor((WebDriver thisDriver) -> ((JavascriptExecutor) thisDriver).executeScript("return document.readyState").equals("complete"));
		sleep();
		FluentWait<WebDriver> wait = (new WebDriverWait(getDriver(), 10))
				.pollingEvery(100, TimeUnit.MILLISECONDS);

		wait.until((WebDriver thisDriver) -> ((JavascriptExecutor) thisDriver).executeScript("return document.readyState").equals("complete"));
		logger.info("WAIT4LOAD took {} ms @ {}", ChronoUnit.MILLIS.between(start, LocalDateTime.now()), Thread.currentThread().getStackTrace()[2].getMethodName());
	}

	public WebElement waitForElementVisibility(By locator) {
		return ExtjsUtils.waitForElementVisibility(getDriver(), locator);
	}

	public WebElement waitForElementPresence(By locator) {
		return ExtjsUtils.waitForElementPresence(getDriver(), locator);
	}
//
//	JdbcTemplate jdbcTemplate;
//
//	//FIXME: CmdBuild integrated datasource should be provided by TomcatManager even if we are running tests
//	//against an external instance of tomcat
//	//This is just for test startup purpose
//	@Override
//	public JdbcTemplate getJdbcTemplate() {
//		if (jdbcTemplate == null) {
//			try {
//				jdbcTemplate = super.getJdbcTemplate();
//			} catch (Exception e) {
//				logger.warn("Could not obtain a proper JdbcTemplate from AbstractWebIt. Building a fallback one...");
//				PGPoolingDataSource ds = new PGPoolingDataSource();
//				ds.setServerName("localhost");
//				ds.setDatabaseName("cmdbuild_r2u");
//				ds.setUser("postgres");
//				ds.setPassword("postgres");
//				ds.setPortNumber(5432);
//				ds.setMaxConnections(2);
//				try {
//					ds.initialize();
//					jdbcTemplate = new JdbcTemplate(ds);
//				} catch (Exception eds) {
//					logger.error("COULD NOT CREATE A FALLBACK DATASOURCE! Exception was: {} ", eds.toString());
//				}
//			}
//		}
//		return jdbcTemplate;
//	}

	//
	/**
	 * Must be called in @After of sublcasses
	 */
	protected void cleanupTouchedClasses() {

		try {
			restoreTouchedClassesSnapshot(runningTestTouchedClasses);
			runningTestTouchedClasses.clear();
		} catch (WebUICMDBuidTestException e) {
			logger.error("UI Test cleanup failed");
		}
	}

	//TODO create overload with default rules?
	protected UITestContext getDefaultTestContextInstance() {
		return new UITestContext().withWebDriver(getDriver());
	}

	public String getCmdbuildSystemConfig(String property) {
		return getAdminClient().system().getConfig(property);
	}

	public String setCmdbuildSystemConfig(String property, String value) {

		String previous = getAdminClient().system().getConfig(property);
		getAdminClient().system().setConfig(property, value);
		return previous;
	}

	public RestClient getClientInstance(@Nonnull WebUILoginCredentials credentials) {
		RestClient client = RestClientImpl.builder().withServerUrl(getBaseUrl()).build();
		client = client.doLogin(credentials.getUserName(), credentials.getPassword());
		return client;
	}

	/**
	 * 
	 * @return a client instance logged in with default admin user
	 * 
	 * Prefer using specific credentials for each test
	 */
	public RestClient getAdminClient() {
		WebUILoginCredentials credentials = WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN);
		return getClientInstance(credentials);
	}

//	private  RestClient getAdminClient() {
////
////		if (LocalDateTime.now().isAfter(adminClientExpirationTms) || adminClient == null) {
////			adminClientExpirationTms = LocalDateTime.now().plusMinutes(clientTimeoutMinutes);
////			adminClient = RestClientImpl.builder().withServerUrl(
////					firstNonNull(System.getProperty("cmdbuild.test.base.url"), fallbackServerUrl)).build();
////			WebUILoginCredentials credentials = WebUILoginCredentials.defaultCredentialsInstance(WebUILoginCredentials.DEFAULT_USER_ADMIN);
////			adminClient = adminClient.doLogin(credentials.getUserName(), credentials.getPassword());
//
////		}
//		return getDefaultAdminClientInstance();
//	}
}
