/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.flow.xpdl;

import org.cmdbuild.river.xpdl.XpdlParser;
import static com.google.common.base.Objects.equal;
import com.google.common.base.Supplier;
import static com.google.common.collect.Iterables.getOnlyElement;
import java.io.IOException;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.apache.commons.io.IOUtils;
//import org.cmdbuild.flow.InlineScriptBuilder;
import org.cmdbuild.river.RiverFlow.FlowStatus;
import org.cmdbuild.river.core.BasicRiverFlowServiceImpl;
import org.cmdbuild.river.core.CompletedTaskImpl;
import org.cmdbuild.river.lock.AquiredLock;
import org.cmdbuild.flow.utils.InMemoryLockService;
import org.cmdbuild.flow.utils.SimpleTaskService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//import org.cmdbuild.flow.beanshell.BeanshellInlineScriptBuilder;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverFlowService;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.data.RiverPlanRepository;
import org.cmdbuild.river.data.RiverFlowRepository;
import org.cmdbuild.river.lock.RiverLockService;
import org.junit.Ignore;

/**
 *
 * @author davide
 */
public class SimpleTaskServiceTest {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private RiverPlanRepository planRepository;
	private RiverFlowRepository flowRepository;

	private RiverLockService lockService;
	private SimpleTaskService taskService;
	private RiverFlowService flowService;

	private RiverPlan complexProcessOne;

	private RiverFlow walk;

	@Before
	public void init() throws IOException {

		{
			String xpdlContent = IOUtils.toString(getClass().getResourceAsStream("/ComplexProcessOne.xpdl"));
			complexProcessOne = XpdlParser.parseXpdlWithDefaultOptions(xpdlContent);
		}

		planRepository = mock(RiverPlanRepository.class);
		when(planRepository.getPlanById(complexProcessOne.getId())).thenReturn(complexProcessOne);

		flowRepository = mock(RiverFlowRepository.class);
		when(flowRepository.getFlowById(anyString())).thenAnswer((i) -> walk);

		lockService = new InMemoryLockService();
		taskService = new SimpleTaskService(lockService, planRepository, (completedTask) -> {
			walk = flowService.completedTask(completedTask);//complete AND save (to global variable)
		});

		flowService = new BasicRiverFlowServiceImpl(planRepository, flowRepository, taskService);

	}

	@Test
	@Ignore//TODO fix this test, use a process with a script
	public void testLockService() {
		String entryPointId = getOnlyElement(complexProcessOne.getEntryPointStepIds());

		walk = flowService.createFlow(complexProcessOne.getId());
		assertNotNull(walk);
		assertEquals(FlowStatus.READY, walk.getStatus());

		AquiredLock lock = lockService.aquireLock(walk.getId()).aquired();

		walk = flowService.startFlow(walk, entryPointId);
		assertNotNull(walk);
		assertEquals(FlowStatus.RUNNING, walk.getStatus());

		try {
			Thread.sleep(300);
		} catch (InterruptedException ex) {
		}

		assertFalse(taskService.hasUserTasks());

		lockService.releaseLock(lock);

		checkAndCompleteNextUserTask("StepOne_user");
	}

	@Test
	public void testComplexProcessOneRunning() {
		String entryPointId = getOnlyElement(complexProcessOne.getEntryPointStepIds());

		walk = flowService.createFlow(complexProcessOne.getId());
		assertNotNull(walk);
		assertEquals(FlowStatus.READY, walk.getStatus());

		walk = flowService.startFlow(walk, entryPointId);
		assertNotNull(walk);
		assertEquals(FlowStatus.RUNNING, walk.getStatus());

		checkAndCompleteNextUserTask("StepOne_user");

		{
			waitForUserTasks(2);
			List<RiverLiveTask> tasks = asList(taskService.popUserTask(), taskService.popUserTask());
			assertFalse(taskService.hasUserTasks());
			checkAndCompleteUserTask(tasks, "StepTwo_user");
			assertFalse(taskService.hasUserTasks());
			checkAndCompleteUserTask(tasks, "StepThree_user");
		}

		{
			waitForUserTasks(2);
			List<RiverLiveTask> tasks = asList(taskService.popUserTask(), taskService.popUserTask());
			assertFalse(taskService.hasUserTasks());
			checkAndCompleteUserTask(tasks, "StepFour_user");
			assertFalse(taskService.hasUserTasks());
			checkAndCompleteUserTask(tasks, "StepFive_user");
		}

		waitFor(() -> equal(FlowStatus.COMPLETE, walk.getStatus()));
	}

	private void checkAndCompleteNextUserTask(String expectedTaskId) {
		waitForUserTasks(1);
		RiverLiveTask task = taskService.popUserTask();
		assertFalse(taskService.hasUserTasks());
		checkAndCompleteUserTask(task, expectedTaskId);
	}

	private void checkAndCompleteUserTask(Collection<RiverLiveTask> tasks, String expectedTaskId) {
		Optional<RiverLiveTask> task = tasks.stream().filter((t) -> t.getTaskId().equals(expectedTaskId)).findAny();
		assertTrue(task.isPresent());
		checkAndCompleteUserTask(task.get(), expectedTaskId);
	}

	private void checkAndCompleteUserTask(RiverLiveTask task, String expectedTaskId) {
		assertTrue(task.getTask().isUser());
		assertEquals(expectedTaskId, task.getTaskId());
		taskService.completeUserTask(new CompletedTaskImpl(task));
	}

	private void waitForUserTasks(int count) {
		waitFor(() -> taskService.countUserTasks() >= count);
	}

	private void waitFor(Supplier<Boolean> test) {
		for (int i = 0; i < 10; i++) {
			if (test.get()) {
				return;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException ex) {
			}
		}
		fail();
	}
}
