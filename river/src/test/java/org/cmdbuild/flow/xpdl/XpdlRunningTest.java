/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.flow.xpdl;

import org.cmdbuild.river.xpdl.XpdlParser;
import com.google.common.base.Function;
import com.google.common.base.Stopwatch;
import static com.google.common.collect.Iterables.getOnlyElement;
import java.io.IOException;
import static java.util.Arrays.asList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.IOUtils;
//import org.cmdbuild.flow.InlineScriptBuilder;
import org.cmdbuild.river.RiverFlow.FlowStatus;
import org.cmdbuild.river.core.BasicRiverFlowServiceImpl;
import org.cmdbuild.river.core.CompletedTaskImpl;
//import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutor;
import org.cmdbuild.flow.utils.DummyTaskService;
import org.cmdbuild.flow.utils.SimpleBeanToImport;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.workflow.type.LookupType;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
//import org.cmdbuild.flow.beanshell.BeanshellInlineScriptBuilder;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverFlowService;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.data.RiverPlanRepository;
import org.cmdbuild.river.data.RiverFlowRepository;
import org.cmdbuild.river.task.scriptexecutors.GroovyScriptServiceImpl;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorService;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorServiceImpl;
import org.junit.Ignore;

/**
 *
 * @author davide
 */
public class XpdlRunningTest {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private RiverPlanRepository planRepository;
	private RiverFlowRepository flowRepository;

	private DummyTaskService taskService;
	private RiverFlowService flowService;

	private RiverPlan requestForChange, complexProcessOne, requestForChangeWithGroovy;

	private RiverFlow walk;

	private final TaskScriptExecutorService taskScriptExecutorService = new TaskScriptExecutorServiceImpl(new GroovyScriptServiceImpl());

	@Before
	public void init() throws IOException {

		{
			String xpdlContent = IOUtils.toString(getClass().getResourceAsStream("/RequestForChange.xpdl"));
			requestForChange = XpdlParser.parseXpdlWithDefaultOptions(xpdlContent);
		}
		{
			String xpdlContent = IOUtils.toString(getClass().getResourceAsStream("/RequestForChange_groovy.xpdl"));
			requestForChangeWithGroovy = XpdlParser.parseXpdlWithDefaultOptions(xpdlContent);
		}
		{
			String xpdlContent = IOUtils.toString(getClass().getResourceAsStream("/ComplexProcessOne.xpdl"));
			complexProcessOne = XpdlParser.parseXpdlWithDefaultOptions(xpdlContent);
		}

		planRepository = mock(RiverPlanRepository.class);
		when(planRepository.getPlanById(requestForChange.getId())).thenReturn(requestForChange);
		when(planRepository.getPlanById(complexProcessOne.getId())).thenReturn(complexProcessOne);
		when(planRepository.getPlanById(requestForChangeWithGroovy.getId())).thenReturn(requestForChangeWithGroovy);

		flowRepository = mock(RiverFlowRepository.class);
		when(flowRepository.getFlowById(anyString())).thenAnswer((i) -> walk);

		taskService = new DummyTaskService();

		flowService = new BasicRiverFlowServiceImpl(planRepository, flowRepository, taskService);

	}

	@Test
	public void testRequestForChangeRunning() {
		runRequestForChange(requestForChange);
	}

	@Test
	public void testRequestForChangeRunningWithGroovy() {
		runRequestForChange(requestForChangeWithGroovy);
	}

	@Test
	@Ignore
	public void testRequestForChangeRunningPerformance() {
		runRequestForChange(requestForChange);//cache stuff
		Stopwatch stopwatch = Stopwatch.createStarted();
		for (int i = 0; i < 100; i++) {
			runRequestForChange(requestForChange);
		}
		stopwatch.stop();
		logger.info("testRequestForChangeRunningPerformance total time = {}ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
	}

	@Test
	@Ignore
	public void testRequestForChangeRunningWithGroovyPerformance() {
		runRequestForChange(requestForChangeWithGroovy);//cache stuff

		Stopwatch stopwatch = Stopwatch.createStarted();
		for (int i = 0; i < 100; i++) {
			runRequestForChange(requestForChangeWithGroovy);
		}
		stopwatch.stop();
		logger.info("testRequestForChangeRunningWithGroovyPerformance total time = {}ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
	}

	private void runRequestForChange(RiverPlan plan) {
		String entryPointId = getOnlyElement(plan.getEntryPointStepIds());

		walk = flowService.createFlow(plan.getId());
		assertNotNull(walk);
		assertEquals(FlowStatus.READY, walk.getStatus());

		walk = flowService.startFlow(walk, entryPointId);
		assertNotNull(walk);
		assertEquals(FlowStatus.RUNNING, walk.getStatus());

//		checkAndCompleteNextTask("Process_requestforchange_act1_noop", RiverTask::isNoop);
		checkAndCompleteNextTask("RegisterRFC_user", RiverTask::isUser);

		checkAndCompleteNextTask("SYS010_script", RiverTask::isInline);

		checkAndCompleteNextTask("FormalEvaluation_user", RiverTask::isUser);

		checkAndCompleteNextTask("SYS020_script", RiverTask::isInline);

		{
			RiverLiveTask task = taskService.popTask();
			assertTrue(taskService.isEmpty());
			assertTrue(task.getTask().isInline());
			assertEquals("check_SYS020_task", task.getTaskId());

			RiverTaskCompleted completedTask = taskScriptExecutorService.executeTask(task, map("FormalEvaluation", new LookupType(1, "myType", "desc", "REJECTED")));

			walk = flowService.completedTask(completedTask);
		}

		checkAndCompleteNextTask("FinalEvaluation_user", RiverTask::isUser);

		checkAndCompleteNextTask("SYS070_script", RiverTask::isInline);

		checkAndCompleteNextTask("Process_requestforchange_act2_noop", RiverTask::isNoop);

		assertEquals(FlowStatus.COMPLETE, walk.getStatus());
	}

	@Test
	public void testComplexProcessOneRunning() {
		String entryPointId = getOnlyElement(complexProcessOne.getEntryPointStepIds());

		walk = flowService.createFlow(complexProcessOne.getId());
		assertNotNull(walk);
		assertEquals(FlowStatus.READY, walk.getStatus());

		walk = flowService.startFlow(walk, entryPointId);
		assertNotNull(walk);
		assertEquals(FlowStatus.RUNNING, walk.getStatus());

//		checkAndCompleteNextTask("ComplexProcessOne_wp1_act1_noop", RiverTask::isNoop);
		checkAndCompleteNextTask("StepOne_user", RiverTask::isUser);

		checkAndCompleteNextTask("ComplexProcessOne_wp1_act4_noop", RiverTask::isNoop);

		{
			List<RiverLiveTask> tasks = asList(taskService.popTask(), taskService.popTask());
			assertTrue(taskService.isEmpty());
			checkAndCompleteTask(tasks, "StepTwo_user", RiverTask::isUser);
			assertTrue(taskService.isEmpty());
			checkAndCompleteTask(tasks, "StepThree_user", RiverTask::isUser);
		}

		checkAndCompleteNextTask("ComplexProcessOne_wp1_act11_noop", RiverTask::isNoop);

		{
			List<RiverLiveTask> tasks = asList(taskService.popTask(), taskService.popTask());
			assertTrue(taskService.isEmpty());
			checkAndCompleteTask(tasks, "StepFive_user", RiverTask::isUser);
			assertTrue(taskService.isEmpty());
			checkAndCompleteTask(tasks, "ComplexProcessOne_wp1_act9_noop", RiverTask::isNoop);
		}

		checkAndCompleteNextTask("StepFour_user", RiverTask::isUser);

		checkAndCompleteNextTask("ComplexProcessOne_wp1_act10_noop", RiverTask::isNoop);

		checkAndCompleteNextTask("ComplexProcessOne_wp1_act2_noop", RiverTask::isNoop);

		assertEquals(FlowStatus.COMPLETE, walk.getStatus());
	}

	private void checkAndCompleteNextTask(String expectedTaskId, Function<RiverTask, Boolean> taskTypeCheck) {
		RiverLiveTask task = taskService.popTask();
		assertTrue(taskService.isEmpty());
		checkAndCompleteTask(task, expectedTaskId, taskTypeCheck);
	}

	private void checkAndCompleteTask(Collection<RiverLiveTask> tasks, String expectedTaskId, Function<RiverTask, Boolean> taskTypeCheck) {
		Optional<RiverLiveTask> task = tasks.stream().filter((t) -> t.getTaskId().equals(expectedTaskId)).findAny();
		assertTrue(task.isPresent());
		checkAndCompleteTask(task.get(), expectedTaskId, taskTypeCheck);
	}

	private void checkAndCompleteTask(RiverLiveTask task, String expectedTaskId, Function<RiverTask, Boolean> taskTypeCheck) {
		assertTrue(taskTypeCheck.apply(task.getTask()));
		assertEquals(expectedTaskId, task.getTaskId());

		walk = flowService.completedTask(new CompletedTaskImpl(task));
	}

}
