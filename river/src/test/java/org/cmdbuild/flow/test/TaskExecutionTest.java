/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.flow.test;

import com.google.common.base.Stopwatch;
import java.io.IOException;
import static java.util.Collections.emptyMap;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.IOUtils;
import org.cmdbuild.flow.utils.SimpleBeanToImport;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.core.RiverFlowImpl;
import org.cmdbuild.river.task.ScriptTaskExtraAttr;
import org.cmdbuild.river.task.LiveTaskImpl;
import org.cmdbuild.river.task.TaskImpl;
import org.cmdbuild.river.task.scriptexecutors.GroovyScriptServiceImpl;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorService;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorServiceImpl;
import org.cmdbuild.river.task.scriptexecutors.WorkflowScriptProcessingException;
//import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutor;
import static org.cmdbuild.river.xpdl.XpdlConst.SCRIPT_ENGINE_HINT;
import static org.cmdbuild.river.xpdl.XpdlConst.SCRIPT_ENGINE_HINT_GROOVY;
import org.cmdbuild.river.xpdl.XpdlParser;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import static org.cmdbuild.river.xpdl.XpdlConst.TEXT_JAVA_MIME_TYPE;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskExecutionTest {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final TaskScriptExecutorService taskScriptExecutorService = new TaskScriptExecutorServiceImpl(new GroovyScriptServiceImpl());

	private RiverPlan plan;
	private String planId;
	private RiverFlow flow;
	private String taskId;

	@Before
	public void init() throws IOException {
		String complexProcessOneXpdlContent = IOUtils.toString(getClass().getResourceAsStream("/ComplexProcessOne.xpdl"));
		plan = XpdlParser.parseXpdlWithDefaultOptions(complexProcessOneXpdlContent);
		planId = plan.getId();
		String walkId = randomId();
		flow = RiverFlowImpl.builder()
				.withPlan(plan)
				.withFlowId(walkId)
				.build();
		taskId = randomId();
	}

	@Test
	public void testExceptionHandling() {
		try {
			RiverLiveTask task = new LiveTaskImpl(flow, TaskImpl.batch()
					.withPlanId(planId)
					.withTaskId(taskId)
					.withExtraAttr(new ScriptTaskExtraAttr(TEXT_JAVA_MIME_TYPE, "a = 1/0;"))
					.build());
			RiverTaskCompleted completed = taskScriptExecutorService.executeTask(task, emptyMap());
			fail();
		} catch (WorkflowScriptProcessingException ex) {
//			assertTrue(ex.getCause().getCause() instanceof ArithmeticException);
			assertEquals("Sourced file: inline evaluation of: ``a = 1/0;'' : Arithemetic Exception in binary op", ex.getCause().getCause().getMessage());
		}
	}

	@Test
	public void testConditionHandling() {
		RiverLiveTask task = new LiveTaskImpl(flow, TaskImpl.batch()
				.withPlanId(planId)
				.withTaskId(taskId)
				.withExtraAttr(new ScriptTaskExtraAttr(TEXT_JAVA_MIME_TYPE, "if ( Confirm || Message == null ) { Res = \"hello\"; }"))
				.build());
		RiverTaskCompleted completed = taskScriptExecutorService.executeTask(task, map("Confirm", true, "Message", null));
		assertEquals("hello", completed.getLocalVariables().get("Res"));
	}

	@Test
	public void testConditionHandling2() {
		RiverLiveTask task = new LiveTaskImpl(flow, TaskImpl.batch()
				.withPlanId(planId)
				.withTaskId(taskId)
				.withExtraAttr(new ScriptTaskExtraAttr(TEXT_JAVA_MIME_TYPE, "if ( Confirm || Message == null ) { Res = \"hello\"; }"))
				.build());
		RiverTaskCompleted completed = taskScriptExecutorService.executeTask(task, map("Confirm", "true", "Message", null));
		assertEquals("hello", completed.getLocalVariables().get("Res"));
	}

	@Test
	public void testConditionHandlingWithGroovy() {
		RiverLiveTask task = new LiveTaskImpl(flow, TaskImpl.batch()
				.withPlanId(planId)
				.withTaskId(taskId)
				.withExtraAttr(new ScriptTaskExtraAttr(TEXT_JAVA_MIME_TYPE, "if ( Confirm || Message == null ) { Res = \"hello\"; }"))
				.withAttributes(map(SCRIPT_ENGINE_HINT, SCRIPT_ENGINE_HINT_GROOVY))
				.build());
		RiverTaskCompleted completed = taskScriptExecutorService.executeTask(task, map("Confirm", true, "Message", null));
		assertEquals("hello", completed.getLocalVariables().get("Res"));
	}

	@Test
	public void testImportWithGroovy() {
		SimpleBeanToImport.count = 0;
		RiverLiveTask task = new LiveTaskImpl(flow, TaskImpl.batch()
				.withPlanId(planId)
				.withTaskId(taskId)
				.withExtraAttr(new ScriptTaskExtraAttr(TEXT_JAVA_MIME_TYPE, "\n"
						+ "									import org.cmdbuild.flow.utils.SimpleBeanToImport;\n"
						+ "									\n"
						+ "									def myBean = new SimpleBeanToImport();\n"
						+ "									\n"
						+ "									"))
				.withAttributes(map(SCRIPT_ENGINE_HINT, SCRIPT_ENGINE_HINT_GROOVY))
				.build());
		RiverTaskCompleted completed = taskScriptExecutorService.executeTask(task, map());
//		assertEquals("hello", completed.getLocalVariables().get("Res"));
		assertEquals(1, SimpleBeanToImport.count);
	}

	private final static String scriptForPerformaceTest = "int count=0; for(int i = 0;i<10000;i++){ count+=i; count/=2; };Res = \"helloo\";";

	@Test
	@Ignore
	public void testGroovyPerformance() {
		RiverLiveTask task = new LiveTaskImpl(flow, TaskImpl.batch()
				.withPlanId(planId)
				.withTaskId(taskId)
				.withExtraAttr(new ScriptTaskExtraAttr(TEXT_JAVA_MIME_TYPE, scriptForPerformaceTest))
				.withAttributes(map(SCRIPT_ENGINE_HINT, SCRIPT_ENGINE_HINT_GROOVY))
				.build());
		{
			taskScriptExecutorService.executeTask(task, map("Confirm", true, "Message", null));//load in cache
		}
		Stopwatch stopwatch = Stopwatch.createStarted();
		RiverTaskCompleted completed = taskScriptExecutorService.executeTask(task, map("Confirm", true, "Message", null));
		stopwatch.stop();
		assertEquals("helloo", completed.getLocalVariables().get("Res"));
		logger.info("testGroovyPerformance total time = {}ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
	}

	@Test
	@Ignore
	public void testBeanshellPerformance() {
		RiverLiveTask task = new LiveTaskImpl(flow, TaskImpl.batch()
				.withPlanId(planId)
				.withTaskId(taskId)
				.withExtraAttr(new ScriptTaskExtraAttr(TEXT_JAVA_MIME_TYPE, scriptForPerformaceTest))
				.build());
		Stopwatch stopwatch = Stopwatch.createStarted();
		RiverTaskCompleted completed = taskScriptExecutorService.executeTask(task, map("Confirm", true, "Message", null));
		stopwatch.stop();
		assertEquals("helloo", completed.getLocalVariables().get("Res"));
		logger.info("testBeanshellPerformance total time = {}ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
	}

}
