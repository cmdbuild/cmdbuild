/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.flow.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.LinkedList;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.task.RiverTaskService;
import static org.cmdbuild.river.task.SubmitTaskResponseForQueuedTask.queuedTask;

public class DummyTaskService implements RiverTaskService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Queue<RiverLiveTask> tasks = new LinkedList<>();

	@Override
	public SubmitTaskResponse submitTask(RiverLiveTask task) {
		logger.debug("submit task {}", task);
		tasks.add(task);
		return queuedTask();
	}

	public RiverLiveTask popTask() {
		return checkNotNull(tasks.poll());
	}

	public boolean isEmpty() {
		return tasks.isEmpty();
	}

}
