/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.flow.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import org.cmdbuild.river.core.CompletedTaskImpl;
import org.cmdbuild.river.lock.AquiredLock;
import org.cmdbuild.river.lock.LockResponse;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.river.data.RiverPlanRepository;
import org.cmdbuild.river.lock.RiverLockService;
import org.cmdbuild.river.task.RiverTaskService;
import static org.cmdbuild.river.task.SubmitTaskResponseForInlineTask.inlineTask;
import static org.cmdbuild.river.task.SubmitTaskResponseForQueuedTask.queuedTask;
import org.cmdbuild.river.task.scriptexecutors.GroovyScriptServiceImpl;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorService;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorServiceImpl;

public class SimpleTaskService implements RiverTaskService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final RiverPlanRepository planRepository;
	private final RiverLockService lockService;
	private final TaskScriptExecutorService taskScriptExecutorService = new TaskScriptExecutorServiceImpl(new GroovyScriptServiceImpl());
	private final Consumer<RiverTaskCompleted> completedTaskConsumer;

	private final ScheduledExecutorService waitService = Executors.newSingleThreadScheduledExecutor();
	private final ExecutorService executorService = Executors.newCachedThreadPool();

	private final Queue<RiverLiveTask> userTasks = new ConcurrentLinkedQueue<>();

	public SimpleTaskService(RiverLockService lockService, RiverPlanRepository planRepository, Consumer<RiverTaskCompleted> completedTaskConsumer) {
		this.lockService = checkNotNull(lockService);
		this.planRepository = checkNotNull(planRepository);
		this.completedTaskConsumer = checkNotNull(completedTaskConsumer);
	}

	public synchronized int countUserTasks() {
		return userTasks.size();
	}

	public synchronized boolean hasUserTasks() {
		return !userTasks.isEmpty();
	}

	public synchronized RiverLiveTask popUserTask() {
		return checkNotNull(userTasks.poll());
	}

	public void completeUserTask(RiverTaskCompleted completedTask) {
		completedTaskConsumer.accept(completedTask);
	}

	@Override
	public synchronized SubmitTaskResponse submitTask(RiverLiveTask task) {
		logger.debug("submit task {}", task);
		switch (task.getTaskType()) {
			case USER:
				userTasks.add(task);
				return queuedTask();
			case SCRIPT_BATCH:
				executorService.submit(() -> aquireLockAndExecuteTask(task));
				return queuedTask();
			case SCRIPT_INLINE:
			case NOP:
				return inlineTask(() -> executeTask(task));
			default:
				throw new IllegalArgumentException("unsupported task type " + task.getTaskType());
		}
	}

	private void aquireLockAndExecuteTask(RiverLiveTask task) {
		logger.debug("aquire lock for task {}", task);
		LockResponse lockResponse = lockService.aquireLock(task.getFlowId());
		if (lockResponse.isAquired()) {
			AquiredLock aquiredLock = lockResponse.aquired();
			try {
				completedTaskConsumer.accept(executeTask(task));
			} finally {
				lockService.releaseLock(aquiredLock);
			}
		} else {
			logger.debug("unable to aquire lock, re-scheduling");
			waitService.schedule(() -> {
				executorService.submit(() -> aquireLockAndExecuteTask(task));
			}, 100, TimeUnit.MILLISECONDS);
		}
	}

	private RiverTaskCompleted executeTask(RiverLiveTask liveTask) {
		logger.debug("execute task {}", liveTask);
		RiverTask task = liveTask.getTask();
		RiverTaskCompleted completedTask;
		if (task.isNoop()) {
			completedTask = new CompletedTaskImpl(liveTask);
		} else {
//			RiverPlan plan = planRepository.getPlanById(task.getPlanId());
			Map<String, Object> dataIn = map();//TODO
			completedTask = taskScriptExecutorService.executeTask(liveTask, dataIn);
//			Map<String, Object> dataOut = completedTask.getLocalVariables(); //TODO
		}
		return completedTask;
	}

}
