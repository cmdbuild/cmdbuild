package org.cmdbuild.river;

import static com.google.common.base.Objects.equal;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import javax.annotation.Nullable;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_ATTR_PERFORMER_TYPE;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_ATTR_PERFORMER_VALUE;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface RiverTask<T> {

	String getPlanId();

	String getId();

	RiverTaskType getTaskType();

	Multimap<String, String> getExtendedAttributes();

	T getTaskTypeData();

	enum RiverTaskType {

		SCRIPT_BATCH, USER, SCRIPT_INLINE, NOP
	}

	default @Nullable
	String getAttr(String key) {
		return Iterables.getOnlyElement(getExtendedAttributes().get(key), null);
	}

	default boolean isInline() {
		return equal(getTaskType(), RiverTaskType.SCRIPT_INLINE);
	}

	default boolean isNoop() {
		return equal(getTaskType(), RiverTaskType.NOP);
	}

	default boolean isUser() {
		return equal(getTaskType(), RiverTaskType.USER);
	}

	default boolean isBatch() {
		return equal(getTaskType(), RiverTaskType.SCRIPT_BATCH);
	}

	default String getPerformerValue() {
		return checkNotBlank(getAttr(TASK_ATTR_PERFORMER_VALUE));
	}

	default String getPerformerType() {
		return checkNotBlank(getAttr(TASK_ATTR_PERFORMER_TYPE));
	}

}
