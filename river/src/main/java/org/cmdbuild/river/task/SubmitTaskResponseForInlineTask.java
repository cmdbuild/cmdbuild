/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.task;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.task.RiverTaskService.SubmitTaskResponse;

public class SubmitTaskResponseForInlineTask implements SubmitTaskResponse {

	private final Supplier<RiverTaskCompleted> inner;

	public SubmitTaskResponseForInlineTask(Supplier<RiverTaskCompleted> inner) {
		this.inner = checkNotNull(inner);
	}

	public static SubmitTaskResponseForInlineTask inlineTask(Supplier<RiverTaskCompleted> inner) {
		return new SubmitTaskResponseForInlineTask(inner);
	}

	@Override
	public boolean isTaskReadyToBeExecutedInline() {
		return true;
	}

	@Override
	public RiverTaskCompleted executeInlineTask() {
		return inner.get();
	}

}
