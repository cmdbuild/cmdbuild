/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.task;

import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverTaskCompleted;

public interface RiverTaskService {

	SubmitTaskResponse submitTask(RiverLiveTask task);

	interface SubmitTaskResponse {

		boolean isTaskReadyToBeExecutedInline();

		default boolean isTaskQueuedForExecution() {
			return !isTaskReadyToBeExecutedInline();
		}

		RiverTaskCompleted executeInlineTask();

	}

}
