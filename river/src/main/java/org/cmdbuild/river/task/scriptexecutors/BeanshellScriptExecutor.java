/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.task.scriptexecutors;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.TargetError;
import static java.lang.String.format;
import java.util.Map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbStringUtils.addLineNumbers;

public class BeanshellScriptExecutor {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String scriptContent;

	public BeanshellScriptExecutor(String scriptContent) {
		this.scriptContent = checkNotBlank(scriptContent);
	}

	public Map<String, Object> execute(Map<String, Object> dataIn) {
		Interpreter interpreter = new Interpreter();
		dataIn.forEach((key, value) -> {
			try {
				interpreter.set(key, value);
			} catch (EvalError ex) {
				throw new WorkflowScriptProcessingException("error setting var = '" + key + "'", ex);
			}
		});
		try {
			interpreter.eval(scriptContent);
			return map((key) -> {

				try {
					return interpreter.get((String) key);
				} catch (EvalError ex) {
					throw new WorkflowScriptProcessingException("error getting var = '" + key + "'", ex);
				}
			});
		} catch (EvalError ex) {
			logger.warn("error executing beanshell script at line {}: {} {} {}\n\n{}\n", ex.getErrorLineNumber(), ex.getMessage(), ex.getErrorText(), ex.getScriptStackTrace(), addLineNumbers(scriptContent));

			String message = format("error executing beanshell script = \"%s\"; error on '%s' at line %s", abbreviate(scriptContent), ex.getErrorText(), ex.getErrorLineNumber());

			if (ex instanceof TargetError) {
				BeanshellTargetException beanshellTargetException = new BeanshellTargetException((TargetError) ex);
				logger.warn("inner beanshell exception cause:", beanshellTargetException.getCause());
				throw new WorkflowScriptProcessingException(beanshellTargetException, message);
			} else {
				throw new WorkflowScriptProcessingException(ex, message);
			}
		}
	}

	public final static class BeanshellTargetException extends RuntimeException {

		private BeanshellTargetException(TargetError targetError) {
			super(targetError.getMessage(), targetError.getTarget());
			this.setStackTrace(targetError.getStackTrace());
		}

	}

}
