/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.task.scriptexecutors;

import java.util.Map;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverTaskCompleted;

public interface TaskScriptExecutorService {
//
//	default RiverTaskCompleted executeTask(RiverLiveTask liveTask, Map<String, Object> data) {
//		return executeTask(liveTask, data, getDefaultConverter());
//	}

//	RiverTaskCompleted executeTask(RiverLiveTask liveTask, Map<String, Object> data, TaskVarConverter converter);
	RiverTaskCompleted executeTask(RiverLiveTask liveTask, Map<String, Object> data);

//	TaskVarConverter getDefaultConverter();
//
//	interface TaskVarConverter {
//
//		<A> A convert(@Nullable Object rawValue, Class<A> targetClass);
//	}
}
