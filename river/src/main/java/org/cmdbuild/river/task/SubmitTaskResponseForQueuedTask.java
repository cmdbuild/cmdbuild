/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.task;

import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.task.RiverTaskService.SubmitTaskResponse;

public class SubmitTaskResponseForQueuedTask implements SubmitTaskResponse {

	private static final SubmitTaskResponseForQueuedTask INSTANCE = new SubmitTaskResponseForQueuedTask();

	public static SubmitTaskResponseForQueuedTask queuedTask() {
		return INSTANCE;
	}

	@Override
	public boolean isTaskReadyToBeExecutedInline() {
		return false;
	}

	@Override
	public RiverTaskCompleted executeInlineTask() {
		throw new IllegalArgumentException("cannot execute inline task! this is not an inline task, this task was queued for background processing");
	}

}
