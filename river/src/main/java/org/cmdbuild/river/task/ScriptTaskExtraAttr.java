/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.task;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

/**
 *
 * @author davide
 */
public class ScriptTaskExtraAttr {

	private final String scriptContent, scriptType;

	public ScriptTaskExtraAttr(String scriptType, String scriptContent) {
		this.scriptContent = checkNotNull(scriptContent);
		this.scriptType = checkNotBlank(scriptType);
	}

	public String getScript() {
		return scriptContent;
	}

	public String getScriptType() {
		return scriptType;
	}

}
