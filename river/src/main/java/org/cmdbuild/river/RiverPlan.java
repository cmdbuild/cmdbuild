package org.cmdbuild.river;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.MoreCollectors.toOptional;
import org.cmdbuild.river.core.Step;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import javax.annotation.Nullable;
import org.cmdbuild.river.core.StepTransition;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface RiverPlan {

	String getId();

	String getName();

	String toXpdl();

	List<String> getEntryPointStepIds();

	Step getStepById(String stepId);

	StepTransition getStepTransitionById(String flagId);

	Collection<StepTransition> getFlags();

	Step getStepByTaskId(String taskId);

	Collection<StepTransition> getFlagsByTargetStepId(String stepId);

	default Collection<StepTransition> getFlagsBySourceStepId(String stepId) {
		checkNotBlank(stepId);
		return getFlags().stream().filter((t) -> equal(t.getSourceStepId(), stepId)).collect(toSet());
	}

	Map<String, RiverVariableInfo<?>> getGlobalVariables();

	Map<String, String> attributes();

	default RiverTask getTask(String taskId) {
		return getStepByTaskId(taskId).getTask();
	}

	default String getAttr(String key) {
		return checkNotNull(getAttOrNull(key), "plan attr not found for key = %s", key);
	}

	@Nullable
	default String getAttOrNull(String key) {
		return attributes().get(key);
	}

	default List<Step> getEntryPointSteps() {
		return getEntryPointStepIds().stream().map(this::getStepById).collect(toList());
	}

	default List<RiverTask> getEntryPointTasks() {
		return getEntryPointStepIds().stream().map(this::getStepById).map(Step::getTask).collect(toList());
	}

	default Step getNextStepByFlagId(String flagId) {
		return getStepById(getStepTransitionById(flagId).getTargetStepId());
	}

	default String getEntryPointIdByTaskId(String taskId) {
		Optional<String> optional = getEntryPointStepIds().stream().map(this::getStepById).filter((s) -> equal(s.getTask().getId(), taskId)).map(Step::getId).collect(toOptional());
		checkArgument(optional.isPresent(), "unable to find entry point for taskId = %s in plan = %s", taskId, this);
		return optional.get();
	}

	Collection<Step> getSteps();

	@Nullable
	default Object getDefaultValueOrNull(String key) {
		RiverVariableInfo variableInfo = getGlobalVariables().get(key);
		if (variableInfo != null) {
			return variableInfo.getDefaultValue().orElse(null);
		} else {
			return null;
		}
	}

	default Map<String, Object> getDefaultValues() {
		return transformValues(getGlobalVariables(), (v) -> v.getDefaultValue().orElse(null));
	}

	default boolean hasGlobalVariable(String k) {
		return getGlobalVariables().containsKey(k);
	}

}
