/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.data;

import org.cmdbuild.river.RiverFlow;

public interface RiverFlowRepository {

	RiverFlow getFlowById(String flowId);

}
