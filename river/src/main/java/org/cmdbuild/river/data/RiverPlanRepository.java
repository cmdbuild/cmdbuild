/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.data;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nullable;
import org.cmdbuild.river.RiverPlan;

public interface RiverPlanRepository {

	default RiverPlan getPlanById(String planId) {
		return checkNotNull(getPlanByIdOrNull(planId), "plan not found for planId = %s", planId);
	}

	@Nullable
	RiverPlan getPlanByIdOrNull(String planId);
}
