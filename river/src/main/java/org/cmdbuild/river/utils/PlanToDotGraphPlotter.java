/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.utils;

import static com.google.common.base.MoreObjects.firstNonNull;
import static java.lang.String.format;
import java.util.Collection;
import static java.util.Collections.emptySet;
import java.util.Queue;
import java.util.Set;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverTask.RiverTaskType;
import org.cmdbuild.river.core.Step;
import org.cmdbuild.river.core.StepTransition;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.queue;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;

public class PlanToDotGraphPlotter {

	public static String planToDotGraph(RiverPlan plan) {
		return planToDotGraph(plan, emptySet());
	}

	public static String planToDotGraph(RiverPlan plan, Set<String> activeTasksOrSteps) {
		return new Plotter().plot(plan, activeTasksOrSteps);
	}

	public static String planToSimplifiedDotGraph(RiverPlan plan, Set<String> activeTasksOrSteps) {
		return new SimplifiedPlotter().plotSimplifiedGraph(plan, activeTasksOrSteps);
	}

	private static class Plotter {

		private final StringBuilder stringBuilder = new StringBuilder();

		private String plot(RiverPlan plan, Set<String> activeTasksOrSteps) {
			stringBuilder.append("digraph \"")
					.append(plan.getName())
					.append("\" {\n\n");

			plan.getSteps().forEach((step) -> {
				boolean active = activeTasksOrSteps.contains(step.getId()) || activeTasksOrSteps.contains(step.getTask().getId());
				stringBuilder.append(format("\t\"%s\" [ shape = \"none\", label = <<table border=\"0\" cellspacing=\"0\"><tr><td border=\"1\">%s%s</td><td border=\"1\" bgcolor=\"%s\">%s</td></tr></table>>%s ];\n",
						step.getId(),
						step.getId(),
						step.getTask().isUser() ? format("<br />task: %s", step.getTask().getId()) : "",
						firstNonNull(map(RiverTaskType.SCRIPT_BATCH, "gold", RiverTaskType.SCRIPT_INLINE, "gold", RiverTaskType.USER, "palegreen").get(step.getTask().getTaskType()), "white"),
						step.getTask().getTaskType().name(),
						active ? ", fillcolor = \"greenyellow\", style = \"filled\"" : ""));
			});

			plan.getFlags().forEach((transition) -> {
				stringBuilder.append("\t\"")
						.append(transition.getSourceStepId())
						.append("\" -> \"")
						.append(transition.getTargetStepId())
						.append(format("\" [ label = \"%s\", color = \"dimgray\", fontcolor = \"dimgray\" ];\n", transition.getStepTransitionId()));
			});

			stringBuilder.append("}\n");
			return stringBuilder.toString();
		}
	}

	private static class SimplifiedPlotter extends Plotter {

		private final StringBuilder stringBuilder = new StringBuilder();

		private String plotSimplifiedGraph(RiverPlan plan, Set<String> activeTasksOrSteps) {
			stringBuilder.append("digraph \"")
					.append(plan.getName())
					.append("\" {\n\n\trankdir = \"LR\";\n\n");

			plan.getSteps().stream().filter((s) -> s.getTask().isUser()).forEach((step) -> {
				boolean active = activeTasksOrSteps.contains(step.getId()) || activeTasksOrSteps.contains(step.getTask().getId());
				stringBuilder.append(format("\t\"%s\" [ shape = \"record\", label = \"%s\"%s ];\n",
						step.getId(),
						step.getId(),
						active ? ", fillcolor = \"greenyellow\", style = \"filled\"" : ""));
			});

			Set<String> allEndSteps = set();

			plan.getSteps().stream().filter((s) -> s.getTask().isUser()).forEach((step) -> {

				Set<String> targetStepIds = getTargetUserSteps(plan, step);
				Set<String> endSteps = getEndSteps(plan, step);
				allEndSteps.addAll(endSteps);

				targetStepIds.forEach((targetStepId) -> {
					stringBuilder.append("\t\"")
							.append(step.getId())
							.append("\" -> \"")
							.append(targetStepId)
							.append("\" [ color = \"dimgray\" ];\n");
				});

				endSteps.forEach(allEndSteps::add);
				endSteps.forEach((endStepId) -> {
					stringBuilder.append("\t\"")
							.append(step.getId())
							.append("\" -> \"")
							.append(endStepId)
							.append("\" [ color = \"dimgray\" ];\n");
				});

			});

			allEndSteps.forEach((endStepId) -> {
				stringBuilder.append(format("\t\"%s\" [ label = \" \", shape = \"circle\" ];\n", endStepId));
			});

			stringBuilder.append("}\n");
			return stringBuilder.toString();
		}

		private Set<String> getTargetUserSteps(RiverPlan plan, Step step) {
			Queue<StepTransition> transitionsToProcess = queue();
			Set<String> targetStepIds = set();
			plan.getFlagsBySourceStepId(step.getId()).forEach(transitionsToProcess::add);
			while (!transitionsToProcess.isEmpty()) {
				StepTransition thisTransition = transitionsToProcess.poll();
				Step targetStep = thisTransition.getTargetStep();
				if (targetStep.getTask().isUser()) {
					targetStepIds.add(targetStep.getId());
				} else {
					plan.getFlagsBySourceStepId(targetStep.getId()).forEach(transitionsToProcess::add);
				}
			}
			return targetStepIds;
		}

		private Set<String> getEndSteps(RiverPlan plan, Step step) {
			Queue<StepTransition> transitionsToProcess = queue();
			Set<String> endStepIds = set();
			plan.getFlagsBySourceStepId(step.getId()).forEach(transitionsToProcess::add);
			while (!transitionsToProcess.isEmpty()) {
				StepTransition thisTransition = transitionsToProcess.poll();
				Step targetStep = thisTransition.getTargetStep();
				if (!targetStep.getTask().isUser()) {
					Collection<StepTransition> transitions = plan.getFlagsBySourceStepId(targetStep.getId());
					if (transitions.isEmpty()) {
						endStepIds.add(targetStep.getId());
					} else {
						transitions.forEach(transitionsToProcess::add);
					}
				}
			}
			return endStepIds;
		}

	}

}
