/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.xpdl;

/**
 *
 * @author davide
 */
public class XpdlParserException extends RuntimeException{

    public XpdlParserException() {
    }

    public XpdlParserException(String message) {
        super(message);
    }

    public XpdlParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public XpdlParserException(Throwable cause) {
        super(cause);
    }
    
}
