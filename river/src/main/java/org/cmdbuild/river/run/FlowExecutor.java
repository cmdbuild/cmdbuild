/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.run;

import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverTaskCompleted;

public interface FlowExecutor {

	RiverFlow startFlow(RiverFlow walk, String entryPoint);

	RiverFlow completedTask(RiverFlow flow, RiverTaskCompleted completedTask);
}
