/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.run;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.concat;
import com.google.common.collect.Queues;
import java.util.Collection;
import static java.util.Collections.singleton;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import org.cmdbuild.river.core.Step;
import org.cmdbuild.river.core.RiverFlowImpl;
import org.cmdbuild.river.task.LiveTaskImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.task.RiverTaskService;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.river.core.StepTransition;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.queue;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.set;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;

public class FlowExecutorImpl implements FlowExecutor {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final RiverTaskService taskService;

	public FlowExecutorImpl(RiverTaskService taskService) {
		this.taskService = checkNotNull(taskService);
	}

	@Override
	public RiverFlow startFlow(RiverFlow flow, String entryPoint) {
		Step startingStep = flow.getPlan().getStepById(entryPoint);
		return new FlowAdvanceOperation(flow).enterStep(startingStep).processAndReturnFlow();
	}

	@Override
	public RiverFlow completedTask(RiverFlow flow, RiverTaskCompleted completedTask) {
		flow = new FlowAdvanceOperation(flow).completeTask(completedTask).processAndReturnFlow();
		if (flow.getTasks().isEmpty()) {
			flow = RiverFlowImpl.copyOf(flow).withWalkStatus(RiverFlow.FlowStatus.COMPLETE).build();
			logger.debug("flow completed = {}", flow);
		}
		return flow;
	}

	private class FlowAdvanceOperation {

		private RiverFlow flow;
		private final Queue<Step> stepQueue = Queues.newConcurrentLinkedQueue();
		private final Queue<RiverTaskCompleted> completedTaskQueue = Queues.newConcurrentLinkedQueue();

		public FlowAdvanceOperation(RiverFlow flow) {
			this.flow = checkNotNull(flow);
		}

		public RiverFlow processAndReturnFlow() {
			while (!stepQueue.isEmpty() || !completedTaskQueue.isEmpty()) {
				while (!completedTaskQueue.isEmpty()) {
					completeTask(completedTaskQueue.poll());
				}
				if (!stepQueue.isEmpty()) {
					enterStep(stepQueue.poll());
				}
			}
			return flow;
		}

		public FlowAdvanceOperation enterStep(Step step) {
			logger.debug("activate step = {}", step);
			RiverTask task = step.getTask();
			RiverTaskService.SubmitTaskResponse submitTaskResponse = taskService.submitTask(new LiveTaskImpl(flow, task));
			flow = RiverFlowImpl.copyOf(flow)
					.withWalkStatus(RiverFlow.FlowStatus.RUNNING)
					.withTasks(concat(flow.getTasks(), singleton(task)))
					.build();
			if (!submitTaskResponse.isTaskQueuedForExecution()) {
				RiverTaskCompleted taskCompleted = submitTaskResponse.executeInlineTask();
				completedTaskQueue.add(taskCompleted);
			}
			return this;
		}

		public FlowAdvanceOperation completeTask(RiverTaskCompleted completedTask) {
			logger.debug("processing completed task output for flow = {} task = {}", flow.getId(), completedTask.getTaskId());
			RiverPlan plan = flow.getPlan();
			Step step = plan.getStepByTaskId(completedTask.getTask().getTaskId());

			logger.debug("merge flow data with task output");
			List<RiverTask> taskListAfterTaskCompletion = flow.getTasks()
					.stream().filter((task) -> !equal(task.getId(), completedTask.getTaskId()))
					.collect(toList());
			flow = RiverFlowImpl.copyOf(flow)
					.withTasks(taskListAfterTaskCompletion)
					.withData(mergeData(flow.getData(), completedTask.getLocalVariables()))
					.build();

			logger.debug("calculate next steps to activate");
			List<Step> stepsThatMayActivate = step.getOutgoingStepTransitionIds(completedTask).stream().map(plan::getStepTransitionById).map(StepTransition::getTargetStep).collect(toList());

			List<Step> stepsToActivate = stepsThatMayActivate.stream()
					.distinct()//warning: step must correctly handle 'equals' (or '==')
					.filter((stepThatMayActivate) -> {
						switch (stepThatMayActivate.getIncomingHandler()) {
							case ACTIVATE_WHEN_ANY_INCOMING_STEP_HAVE_COMPLETED:
								return true;
							case ACTIVATE_WHEN_ALL_INCOMING_STEPS_HAVE_COMPLETED:
								return allIncomingStepsHaveCompletedForStep(stepThatMayActivate);
							default:
								throw unsupported("unsupported incoming handler = %s", stepThatMayActivate.getIncomingHandler());
						}
					}).collect(toList());

			logger.debug("completed processing of task output for flow = {} task = {} ", completedTask.getTask(), flow);
			if (!stepsToActivate.isEmpty()) {
				stepQueue.addAll(stepsToActivate);
			}
			return this;
		}

		private Map<String, Object> mergeData(Map<String, Object> old, Map<String, Object> newData) {
			return map(old).with(newData);
		}

		private boolean allIncomingStepsHaveCompletedForStep(Step stepThatMayActivate) {
			Set<String> activeTasks = flow.getTaskIds();
			return !getAllParallelStepsConvergingOnThisStep(flow.getPlan(), stepThatMayActivate).stream().anyMatch((s) -> activeTasks.contains(s.getTask().getId()));
		}

	}

	private static Collection<Step> getAllParallelStepsConvergingOnThisStep(RiverPlan plan, Step convergingStep) {
		//TODO this algo must be tested
		//TODO: replace with better algorithm (explore parent nodes until we find the lowest common ancestor for all immediate ancestors of 'convergingStep'
		List<Step> steps = list();
		Queue<Step> stepsToProcess = queue(plan.getEntryPointSteps());
		Set<Step> processedSteps = set();
		while (!stepsToProcess.isEmpty()) {
			Step stepToProcess = stepsToProcess.poll();
			processedSteps.add(stepToProcess);
			if (!equal(convergingStep, stepToProcess)) {
				steps.add(stepToProcess);
				stepToProcess.getOutgoingHandler().getAllOutgoingStepTransitionIds().stream().map(plan::getStepTransitionById).map(StepTransition::getTargetStep)
						.filter(not(stepsToProcess::contains))
						.filter(not(processedSteps::contains))
						.forEach(stepsToProcess::add);
			}
		}
		return steps;
	}

}
