/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.core;

import java.util.Collection;
import static java.util.Collections.emptyList;
import org.cmdbuild.river.core.Step.OutgoingHandler;
import org.cmdbuild.river.RiverTaskCompleted;

/**
 *
 * @author davide
 */
public class EmptyOutgoingHandler implements OutgoingHandler {

	private final static EmptyOutgoingHandler INSTANCE = new EmptyOutgoingHandler();

	@Override
	public Collection<String> getOutgoingStepTransitionIdsForTask(RiverTaskCompleted completedTask) {
		return emptyList();
	}

	@Override
	public Collection<String> getAllOutgoingStepTransitionIds() {
		return emptyList();
	}

	public static OutgoingHandler goingNowere() {
		return INSTANCE;
	}

}
