/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;
import java.util.Map;
import org.cmdbuild.river.run.FlowExecutorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.RiverFlowService;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.data.RiverPlanRepository;
import org.cmdbuild.river.data.RiverFlowRepository;
import org.cmdbuild.river.run.FlowExecutor;
import org.cmdbuild.river.task.RiverTaskService;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.defaultValue;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toBooleanOrDefault;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import static org.cmdbuild.river.xpdl.XpdlConst.ALWAYS_INITIALIZE_GLOBAL_VARIABLES;

public class BasicRiverFlowServiceImpl implements RiverFlowService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final RiverPlanRepository planRepository;
	private final RiverFlowRepository flowRepository;
	private final FlowExecutor flowExecutor;

	public BasicRiverFlowServiceImpl(RiverPlanRepository planRepository, RiverFlowRepository flowRepository, RiverTaskService taskService) {
		this.planRepository = checkNotNull(planRepository);
		this.flowRepository = checkNotNull(flowRepository);
		flowExecutor = new FlowExecutorImpl(checkNotNull(taskService));
	}

	@Override
	public RiverPlan getPlanById(String planId) {
		return planRepository.getPlanById(planId);
	}

	@Override
	public RiverFlow createFlow(RiverPlan riverPlan) {
		logger.debug("create process for plan = {}", riverPlan);
		RiverFlow flow = RiverFlowImpl.builder()
				.withFlowId(randomId())
				.withPlan(riverPlan)
				.withTasks(emptyList())
				.withWalkStatus(RiverFlow.FlowStatus.READY)
				.withData(buildInitialData(riverPlan))
				.build();
		return flow;
	}

	@Override
	public RiverFlow startFlow(RiverFlow walk, String entryPointId) {
		logger.debug("start process for walk = {} entry point = {}", walk, entryPointId);
		return flowExecutor.startFlow(walk, entryPointId);
	}

	@Override
	public RiverFlow completedTask(RiverTaskCompleted completedTask) {
		RiverFlow flow = flowRepository.getFlowById(completedTask.getFlowId());
		return completedTask(flow, completedTask);
	}

	@Override
	public RiverFlow completedTask(RiverFlow flow, RiverTaskCompleted completedTask) {
		return flowExecutor.completedTask(flow, completedTask);
	}

	private Map<String, Object> buildInitialData(RiverPlan plan) {
		boolean initGlobalVars = toBooleanOrDefault(plan.getAttOrNull(ALWAYS_INITIALIZE_GLOBAL_VARIABLES), false);
		Map<String, Object> map = map();
		plan.getGlobalVariables().forEach((key, var) -> {
			Object value = null;
			if (var.getDefaultValue().isPresent()) {
				value = var.getDefaultValue().get();//TODO value conversion
			}
			if (value == null && initGlobalVars) {
				value = defaultValue(var.getJavaType());
			}
			map.put(key, value);
		});
		return map;
	}

}
