/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.river.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyMap;
import java.util.Map;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.river.task.LiveTaskImpl;

/**
 *
 * @author davide
 */
public class CompletedTaskImpl implements RiverTaskCompleted {

	private final RiverLiveTask task;
	private final Map<String, Object> data;

	public CompletedTaskImpl(RiverLiveTask task) {
		this(task, emptyMap());
	}

	public CompletedTaskImpl(RiverLiveTask task, Map<String, Object> data) {
		this.task = checkNotNull(task);
		this.data = checkNotNull(data);
	}

	@Override
	public RiverLiveTask getTask() {
		return task;
	}

	@Override
	public Map<String, Object> getLocalVariables() {
		return data;
	}

	public static CompletedTaskImpl of(RiverLiveTask task) {
		return new CompletedTaskImpl(task);
	}

	public static CompletedTaskImpl of(RiverFlow flow, RiverTask task) {
		return new CompletedTaskImpl(new LiveTaskImpl(flow, task));
	}
}
