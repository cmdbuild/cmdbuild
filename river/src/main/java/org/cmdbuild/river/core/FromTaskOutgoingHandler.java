package org.cmdbuild.river.core;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;
import org.cmdbuild.river.core.Step.OutgoingHandler;
import org.cmdbuild.river.RiverTaskCompleted;
import static org.cmdbuild.river.xpdl.XpdlConst.NEXT_FLAGS_TO_ACTIVATE_SCRIPT_VAR;

public class FromTaskOutgoingHandler implements OutgoingHandler {

	private final Collection<String> allOutgoingFlags;

	public FromTaskOutgoingHandler(Collection<String> allOutgoingFlags) {
		this.allOutgoingFlags = allOutgoingFlags;
	}

	@Override
	public Collection<String> getAllOutgoingStepTransitionIds() {
		return allOutgoingFlags;
	}

	@Override
	public Collection<String> getOutgoingStepTransitionIdsForTask(RiverTaskCompleted completedTask) {
		Collection<String> activeFlagsHint = (Collection<String>) completedTask.getLocalVariables().get(NEXT_FLAGS_TO_ACTIVATE_SCRIPT_VAR);
		checkNotNull(activeFlagsHint);
		return activeFlagsHint;
	}

}
