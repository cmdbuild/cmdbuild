package org.cmdbuild.river;

import static com.google.common.base.Objects.equal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.toSet;

public interface RiverFlow {

	List<RiverTask> getTasks();

	default Set<String> getTaskIds() {
		return getTasks().stream().map(RiverTask::getId).collect(toSet());
	}

	FlowStatus getStatus();

	RiverPlan getPlan();

	String getId();

	Map<String, Object> getData();

	default boolean isCompleted() {
		return equal(getStatus(), FlowStatus.COMPLETE);
	}

	default boolean isRunning() {
		return equal(getStatus(), FlowStatus.RUNNING);
	}

	default String getPlanId() {
		return getPlan().getId();
	}

	enum FlowStatus {
		READY, RUNNING, ERROR, COMPLETE
	}
}
