
# Workflow docs

## xpdl configuration

Here are some xpdl extended configs available for river:

* global configs (xpdl extended attributes):
 * CM_RIVER_EXPRESSION_SCRIPT_ENGINE_HINT: set preferred script engine to be used to evaluate expressions (not scripts); possible values are beanshell and groovy, default is beanshell;
 * CM_RIVER_SCRIPT_ENGINE_HINT: set preferred script engine to evaluate all scripts; possible values are beanshell and groovy, default is beanshell; this can also be set per-task;
 * CM_RIVER_GROOVY_SMART_VARIABLES: enable smart handling of variables in groovy (ie load only variables that are referenced by name in the script); this can also be set per-task; enabled by default;
 * CM_RIVER_ALWAYS_INITIALIZE_GLOBAL_VARIABLES: always initialize global variables (same default as shark); this defaults to false in river, but is set to true in cmdbuild; you should not have to set this directly (unless you're using river directly out of cmdbuild);



