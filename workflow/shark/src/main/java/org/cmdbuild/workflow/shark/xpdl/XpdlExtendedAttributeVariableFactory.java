package org.cmdbuild.workflow.shark.xpdl;

import org.cmdbuild.workflow.model.TaskVariable;

public interface XpdlExtendedAttributeVariableFactory {

	TaskVariable createVariable(final XpdlExtendedAttribute xa);
}
