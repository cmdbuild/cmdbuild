package org.cmdbuild.workflow.shark.xpdl;

import org.cmdbuild.workflow.model.TaskVariable;
import static org.cmdbuild.workflow.core.xpdl.XpdlTaskUtils.taskVariableFromXpdlKeyValue;
import org.springframework.stereotype.Component;

@Component
public class SharkStyleXpdlExtendedAttributeVariableFactory implements XpdlExtendedAttributeVariableFactory {

	@Override
	public TaskVariable createVariable(XpdlExtendedAttribute xa) {
		String key = xa.getKey();
		String value = xa.getValue();
		return taskVariableFromXpdlKeyValue(key, value);
	}

}
