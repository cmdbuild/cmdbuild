package org.cmdbuild.workflow.core.model;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.toImmutableList;
import java.util.List;

import java.util.Map.Entry;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;

import org.apache.commons.lang3.builder.Builder;
import org.joda.time.DateTime;

import javax.annotation.Nullable;
import org.cmdbuild.workflow.model.FlowStatus;
import org.cmdbuild.workflow.model.PlanInfo;
import org.cmdbuild.workflow.model.SimplePlanInfo;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_FLOW_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_NEXT_EXECUTOR;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_PLAN_INFO;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_TASK_DEFINITION_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_TASK_INSTANCE_ID;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.FlowActivity;
import org.cmdbuild.workflow.model.Process;

public class FlowImpl implements Flow {

	private final Process plan;
	private final Card card;
	private final FlowStatus flowStatus;
	private final List<FlowActivity> flowActivities;

	private FlowImpl(FlowCardBuilder builder) {
		this.plan = checkNotNull(builder.plan);
		if (builder.flowActivities == null) {
			this.card = checkNotNull(builder.card);
		} else {
			this.card = CardImpl.copyOf(builder.card)
					.addAttribute(ATTR_TASK_INSTANCE_ID, builder.flowActivities.stream().map(FlowActivity::getInstanceId).collect(toList()))
					.addAttribute(ATTR_TASK_DEFINITION_ID, builder.flowActivities.stream().map(FlowActivity::getDefinitionId).collect(toList()))
					.addAttribute(ATTR_NEXT_EXECUTOR, builder.flowActivities.stream().map(FlowActivity::getPerformerGroup).collect(toList()))
					.build();
		}

		this.flowStatus = checkNotNull(builder.flowStatus);
		checkArgument(equal(card.getType().getName(), plan.getName()), "planClasse name does not match flowCard.type name");

		List<String> activityIds = card.get(ATTR_TASK_INSTANCE_ID, List.class);
		List<String> activityDefintionIds = card.get(ATTR_TASK_DEFINITION_ID, List.class);
		List<String> activityPerformers = card.get(ATTR_NEXT_EXECUTOR, List.class);
		checkArgument(activityIds.size() == activityDefintionIds.size() && activityIds.size() == activityPerformers.size(), "activity info size mismatch");
		flowActivities = IntStream.range(0, activityIds.size()).mapToObj((i) -> new FlowActivityImpl(activityIds.get(i), activityDefintionIds.get(i), activityPerformers.get(i))).collect(toImmutableList());
	}

	@Override
	public List<FlowActivity> getFlowActivities() {
		return flowActivities;
	}

	@Override
	public Process getType() {
		return plan;
	}

	@Override
	public FlowStatus getStatus() {
		return flowStatus;
	}

	@Nullable
	@Override
	public PlanInfo getPlanInfo() {
		String value = card.get(ATTR_PLAN_INFO, String.class);
		return SimplePlanInfo.deserializeNullable(value);
	}

	@Override
	public Long getId() {
		return card.getId();
	}

	@Override
	public String getCode() {
		return card.getCode();
	}

	@Override
	public String getDescription() {
		return card.getDescription();
	}

	@Override
	public Object get(String key) {
		return card.get(key);
	}

	@Override
	public <T> T get(String key, Class<? extends T> requiredType) {
		return card.get(key, requiredType);
	}

	@Override
	public <T> T get(String key, Class<? extends T> requiredType, T defaultValue) {
		return card.get(key, requiredType, defaultValue);
	}

	@Override
	public Iterable<Entry<String, Object>> getAttributeValues() {
		return card.getAttributeValues();
	}

	@Override
	public Iterable<Entry<String, Object>> getRawValues() {
		return card.getRawValues();
	}

	@Override
	public String getUser() {
		return card.getUser();
	}

	@Override
	public DateTime getBeginDate() {
		return card.getBeginDate();
	}

	@Override
	public DateTime getEndDate() {
		return card.getEndDate();
	}

	@Override
	public Long getCurrentId() {
		return card.getCurrentId();
	}

	@Override
	public Long getCardId() {
		return card.getId();
	}

	@Override
	public String getFlowId() {
		return card.get(ATTR_FLOW_ID, String.class);
	}

	@Override
	public String toString() {
		PlanInfo planInfo = getPlanInfo();
		String planId = planInfo == null ? null : planInfo.getDefinitionId();
		return "FlowCardImpl{flowId=" + getFlowId() + ",cardId=" + getId() + ",planId=" + planId + '}';
	}

	public static FlowCardBuilder builder() {
		return new FlowCardBuilder();
	}

	public static FlowCardBuilder copyOf(Flow flow) {
		return builder()
				.withCard(flow)
				.withFlowStatus(flow.getStatus())
				.withPlan(flow.getType());
	}

	public static class FlowCardBuilder implements Builder<FlowImpl> {

		private Card card;
		private Process plan;
		private FlowStatus flowStatus;
		private List<FlowActivity> flowActivities;

		@Override
		public FlowImpl build() {
			return new FlowImpl(this);
		}

		public FlowCardBuilder withPlan(Process plan) {
			this.plan = plan;
			return this;
		}

		public FlowCardBuilder withFlowStatus(FlowStatus flowStatus) {
			this.flowStatus = flowStatus;
			return this;
		}

		public FlowCardBuilder withCard(Card value) {
			card = value;
			return this;
		}

		public FlowCardBuilder withFlowActivities(List<FlowActivity> flowActivities) {
			this.flowActivities = flowActivities;
			return this;
		}

	}
}
