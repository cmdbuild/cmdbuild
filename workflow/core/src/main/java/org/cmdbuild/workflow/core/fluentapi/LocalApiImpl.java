/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.core.fluentapi;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.api.fluent.Card;
import org.cmdbuild.api.fluent.CardDescriptor;
import org.cmdbuild.api.fluent.ExecutorBasedFluentApi;
import org.cmdbuild.api.fluent.FluentApiExecutor;
import org.cmdbuild.workflow.type.LookupType;
import org.cmdbuild.workflow.type.ReferenceType;
import org.springframework.stereotype.Component;

@Component
public class LocalApiImpl extends ExecutorBasedFluentApi implements ExtendedApi {

	private final ExtendedApiMethods extendedApi;

	public LocalApiImpl(FluentApiExecutor executor, ExtendedApiMethods extendedApi) {
		super(executor);
		this.extendedApi = checkNotNull(extendedApi);
	}

	@Override
	public ReferenceType referenceTypeFrom(Card card) {
		return extendedApi.referenceTypeFrom(card);
	}

	@Override
	public ReferenceType referenceTypeFrom(CardDescriptor cardDescriptor) {
		return extendedApi.referenceTypeFrom(cardDescriptor);
	}

	@Override
	public ReferenceType referenceTypeFrom(Object idAsObject) {
		return extendedApi.referenceTypeFrom(idAsObject);
	}

	@Override
	public CardDescriptor cardDescriptorFrom(ReferenceType referenceType) {
		return extendedApi.cardDescriptorFrom(referenceType);
	}

	@Override
	public Card cardFrom(ReferenceType referenceType) {
		return extendedApi.cardFrom(referenceType);
	}

	@Override
	public ClassInfo findClass(String className) {
		return extendedApi.findClass(className);
	}

	@Override
	public ClassInfo findClass(int classId) {
		return extendedApi.findClass(classId);
	}

	@Override
	public LookupType selectLookupById(long id) {
		return extendedApi.selectLookupById(id);
	}

	@Override
	public LookupType selectLookupByCode(String type, String code) {
		return extendedApi.selectLookupByCode(type, code);
	}

	@Override
	public LookupType selectLookupByDescription(String type, String description) {
		return extendedApi.selectLookupByDescription(type, description);
	}

}
