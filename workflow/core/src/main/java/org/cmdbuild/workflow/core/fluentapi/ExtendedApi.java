/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.core.fluentapi;

import org.cmdbuild.api.fluent.FluentApi;
import org.cmdbuild.workflow.inner.SchemaApiForWorkflow;

public interface ExtendedApi extends ExtendedApiMethods, FluentApi, SchemaApiForWorkflow {

}
