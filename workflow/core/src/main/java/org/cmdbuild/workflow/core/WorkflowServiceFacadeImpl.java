/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.core;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Iterables;
import com.google.common.collect.Streams;
import java.util.Collection;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javax.activation.DataSource;
import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.workflow.WorkflowConfiguration;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptions;
import org.cmdbuild.data2.impl.ProcessEntryFiller;
import org.cmdbuild.logic.data.access.resolver.AbstractSerializer;
import org.cmdbuild.logic.data.access.resolver.ForeignReferenceResolver;
import org.cmdbuild.workflow.WorkflowService;
import org.cmdbuild.workflow.inner.UserFlowWithPosition;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.inner.WorkflowServiceDelegate;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.workflow.inner.FlowCardRepository;
import org.cmdbuild.workflow.model.TaskDefinition;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_DISABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_ENABLED;
import org.cmdbuild.widget.WidgetService;
import org.cmdbuild.workflow.FlowAdvanceResponse;
import org.cmdbuild.workflow.core.inner.WorkflowServiceDelegates;
import org.cmdbuild.workflow.core.xpdl.XpdlTemplateService;
import org.cmdbuild.workflow.model.XpdlInfoImpl;
import org.cmdbuild.workflow.model.XpdlInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.widget.model.WidgetData;
import org.cmdbuild.workflow.inner.FlowMigrationService;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.Process;
import org.cmdbuild.workflow.inner.ProcessRepository;

@Primary
@Component
public class WorkflowServiceFacadeImpl implements WorkflowService, SystemService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final WorkflowConfiguration configuration;
	private final WorkflowServiceDelegates services;
	private final FlowCardRepository cardRepository;
	private final ProcessRepository classeRepository;
	private final WidgetService widgetService;
	private final XpdlTemplateService templateService;
	private final FlowMigrationService flowMigrationService;

	public WorkflowServiceFacadeImpl(WorkflowConfiguration configuration, WorkflowServiceDelegates services, FlowCardRepository cardRepository, ProcessRepository classeRepository, WidgetService widgetService, XpdlTemplateService templateService, FlowMigrationService flowMigrationService) {
		this.configuration = checkNotNull(configuration);
		this.services = checkNotNull(services);
		this.cardRepository = checkNotNull(cardRepository);
		this.classeRepository = checkNotNull(classeRepository);
		this.widgetService = checkNotNull(widgetService);
		this.templateService = checkNotNull(templateService);
		this.flowMigrationService = checkNotNull(flowMigrationService);
	}

	@Override
	public String getServiceName() {
		return "Workflow Engine";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (isWorkflowEnabled()) {
			return SS_ENABLED;
		} else {
			return SS_DISABLED;
		}
	}

	private void checkWorkflowEnabled() {
		checkArgument(isWorkflowEnabled(), "CM: operation not allowed: workflow service is not enabled");
	}

	private WorkflowServiceDelegate getDefault() {
		checkWorkflowEnabled();
		return services.getDefault();
	}

	private WorkflowServiceDelegate getService(String key) {
		checkWorkflowEnabled();
		return services.getService(key);
	}

	private WorkflowServiceDelegate getService(Process classe) {
		return getService(classe.getProviderOrDefault(configuration.getDefaultWorkflowProvider()));
	}

	private WorkflowServiceDelegate getService(Flow card) {
		return getService(card.getType());
	}

	private List<WorkflowServiceDelegate> getServicesDefaultFirst() {
		return services.getServicesDefaultFirst();
	}

	@Override
	public Process getProcess(String processClasseName) {
		return classeRepository.getProcessClassByName(processClasseName);
	}

	@Override
	public Collection<Process> getActiveProcessClasses() {
		return classeRepository.getAllPlanClasses().stream().filter(Process::isActive).collect(toList());//TODO check isActive filter
	}

	@Override
	public Collection<Process> getAllProcessClasses() {
		return classeRepository.getAllPlanClasses();
	}

	@Override
	public List<Task> getTaskListForCurrentUserByClassIdAndCardId(String classId, Long cardId) {
		Flow flowCard = getFlowCard(classId, cardId);
		return getService(flowCard).getTaskListForCurrentUserByClassIdAndCardId(classId, cardId);
	}

	@Override
	public PagedElements<Task> getTaskListForCurrentUserByClassId(String processId, DaoQueryOptions queryOptions) {
		return getService(getProcess(processId)).getTaskListForCurrentUserByClassId(processId, queryOptions);
	}

	@Override
	public PagedElements<UserFlowWithPosition> queryWithPosition(String className, DaoQueryOptions queryOptions, Iterable<Long> cardId) {
		return cardRepository.queryWithPosition(className, queryOptions, cardId);
	}

	@Override
	public Flow getFlowCardOrNull(Process classe, Long cardId) {
		return cardRepository.getFlowCardByPlanAndCardId(classe, cardId);
	}

	@Override
	public PagedElements<Flow> getFlowCardsByClasseIdAndQueryOptions(String className, DaoQueryOptions queryOptions) {
		return cardRepository.getCardsByClassIdAndQueryOptions(className, queryOptions);
	}

	@Override
	public PagedElements<Flow> getFlowCardsByClasseAndQueryOptions(Classe processClass, DaoQueryOptions queryOptions) {
		PagedElements<Flow> fetchedProcesses = getFlowCardsByClasseIdAndQueryOptions(processClass.getName(), queryOptions);
		Iterable<Flow> processes = resolve(fetchedProcesses); //TODO WHY WHY WHY this method behave differently than getFlowCardsByClasseIdAndQueryOptions ???
		return new PagedElements<>(processes, fetchedProcesses.totalSize());
	}

	private Iterable<Flow> resolve(Iterable<? extends Flow> fetchedProcesses) {
		return ForeignReferenceResolver.<Flow>newInstance() //
				.withEntries(fetchedProcesses) //
				.withEntryFiller(new ProcessEntryFiller()) //
				.withSerializer(new AbstractSerializer<Flow>() {
				}) //
				.build() //
				.resolve();
	}

	@Override
	public void sync() {
		getServicesDefaultFirst().forEach((d) -> d.sync());
	}

	@Override
	public boolean isWorkflowEnabledAndProcessRunnable(String className) {
		return configuration.isEnabled() && getProcess(className).isRunnable();
	}

	@Override
	public boolean isWorkflowEnabled() {
		return configuration.isEnabled();
	}

	@Override
	public Task getUserTask(Flow card, String userTaskId) {
		return getService(card).getUserTask(card, userTaskId);
	}

	@Override
	public FlowAdvanceResponse startProcess(String processClassName, Map<String, ?> vars, boolean advance) {
		Process classe = classeRepository.getProcessClass(processClassName);
		return getService(classe).startProcess(classe, vars, advance);
	}

	@Override
	public FlowAdvanceResponse updateProcess(String planClasseId, Long flowCardId, String taskId, Map<String, ?> vars, boolean advance) {
		Flow card = cardRepository.getFlowCardByClasseIdAndCardId(planClasseId, flowCardId);
		return getService(card).updateProcess(card, taskId, vars, advance);
	}

	@Override
	public void abortProcessInstance(Flow card) {
		getService(card).abortProcessInstance(card);
	}

	@Override
	public void suspendProcessInstance(Flow card) {
		getService(card).suspendProcessInstance(card);
	}

	@Override
	public void resumeProcessInstance(Flow card) {
		getService(card).resumeProcessInstance(card);
	}

	@Override
	public void suspendProcess(String classId, Long cardId) {
		suspendProcessInstance(getFlowCard(classId, cardId));
	}

	@Override
	public void resumeProcess(String classId, Long cardId) {
		resumeProcessInstance(getFlowCard(classId, cardId));
	}

	@Override
	public void abortProcess(String classId, Long cardId) {
		abortProcessInstance(getFlowCard(classId, cardId));
	}

	@Override
	public DataSource getXpdlTemplate(String planClasseId) {
		Process classe = classeRepository.getProcessClass(planClasseId);
		return templateService.getTemplate(classe);
	}

	@Override
	public List<XpdlInfo> getXpdlInfosOrderByVersionDesc(String planClasseId) {
		boolean[] foundDefault = {false};
		List<XpdlInfo> infos = getServicesDefaultFirst().stream()
				.map((d) -> (Iterable<XpdlInfo>) d.getXpdlInfosOrderByVersionDesc(planClasseId))
				.reduce((l1, l2) -> Iterables.concat(l1, l2)).map(Streams::stream).orElse(Stream.empty())
				.map((info) -> {
					if (info.isDefault()) {
						if (foundDefault[0]) {
							return XpdlInfoImpl.copyOf(info).withDefault(false).build();
						} else {
							foundDefault[0] = true;
						}
					}
					return info;
				})
				.collect(toList());
		return infos;
	}

	@Override
	public DataSource getXpdlByClasseIdAndPlanId(String classId, String planId) {
		Process classe = classeRepository.getPlanClasseByClassAndPlanId(classId, planId);
		checkArgument(equal(classId, classe.getName()), "planId = %s does not bind to class = %s", planId, classId);
		return getService(classe).getXpdlForClasse(classe);
	}

	@Override
	public XpdlInfo addXpdl(String classId, String provider, DataSource dataSource) {
		return getService(provider).addXpdl(classId, dataSource);
	}

	@Override
	public XpdlInfo addXpdl(String classId, DataSource dataSource) {
		return getDefault().addXpdl(classId, dataSource);
	}

	@Override
	public List<WidgetData> getWidgetsForUserTask(String classeId, Long cardId, String taskId) {
		Flow flowCard = getFlowCard(classeId, cardId);
		TaskDefinition taskDefinition = getTaskDefinition(flowCard, taskId);
		Map<String, Object> flowData = getFlowData(flowCard);
//		return widgetService.createWidgets(taskDefinition.getWidgets(), flowData); TODO
		return emptyList();
	}

	private TaskDefinition getTaskDefinition(Flow flowCard, String taskId) {
		return getService(flowCard).getTaskDefinition(flowCard, taskId);
	}

	private Map<String, Object> getFlowData(Flow flowCard) {
		return getService(flowCard).getFlowData(flowCard);
	}

	@Override
	public Task getTask(Flow flowCard, String taskId) {
		return getService(flowCard).getTask(flowCard, taskId);
	}

	@Override
	public Iterable<Task> getTaskList(Flow flowCard) {
		return getService(flowCard).getTaskList(flowCard);
	}

	@Override
	public void migrateFlowInstancesToNewProvider(Process process, String provider) {
		flowMigrationService.migrateFlowInstancesToNewProvider(process, provider);
	}
}
