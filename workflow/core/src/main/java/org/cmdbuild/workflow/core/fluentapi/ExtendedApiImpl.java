/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.core.fluentapi;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Math.toIntExact;
import static java.util.Optional.ofNullable;
import javax.annotation.Nullable;
import org.cmdbuild.api.fluent.Card;
import org.cmdbuild.api.fluent.CardDescriptor;
import org.cmdbuild.api.fluent.ExecutorBasedFluentApi;
import org.cmdbuild.api.fluent.FluentApi;
import org.cmdbuild.api.fluent.FluentApiExecutor;
import org.cmdbuild.common.Constants;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.lookup.Lookup;
import org.cmdbuild.lookup.LookupService;
import org.cmdbuild.utils.lang.CmdbConvertUtils;
import org.cmdbuild.workflow.type.LookupType;
import org.cmdbuild.workflow.type.ReferenceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.view.DataView;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.isNotNullAndGtZero;
import static org.cmdbuild.workflow.type.utils.WorkflowTypeUtils.emptyToNull;

@Component
public class ExtendedApiImpl implements ExtendedApiMethods {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LookupService lookupService;
	private final FluentApi fluentApi;
	private final DataView dataView;

	public ExtendedApiImpl(FluentApiExecutor executor, LookupService lookupService, DataView dataView) {
		fluentApi = new ExecutorBasedFluentApi(executor);
		this.lookupService = checkNotNull(lookupService);
		this.dataView = checkNotNull(dataView);
	}

	@Override
	public ClassInfo findClass(String className) {
		return toClassInfo(dataView.getClasse(className));
	}

	@Override
	public ClassInfo findClass(int oid) {
		return toClassInfo(dataView.getClass(oid));
	}

	private ClassInfo toClassInfo(Classe classe) {
		return new ClassInfo(classe.getName(), classe.getOid());
	}

	@Override
	public LookupType selectLookupById(long id) {
		return convertLookup(lookupService.getLookup(id));
	}

	@Override
	public LookupType selectLookupByCode(String type, String code) {
		return convertLookup(lookupService.getLookupByTypeAndCode(type, code));
	}

	@Override
	public LookupType selectLookupByDescription(String type, String description) {
		throw new UnsupportedOperationException();
	}

	private @Nullable
	LookupType convertLookup(@Nullable Lookup in) {
		if (in == null) {
			return null;
		} else {
			LookupType out = new LookupType();
			out.setType(in.getType().getName());
			out.setId(toIntExact(in.getId()));
			out.setCode(in.getCode());
			out.setDescription(in.getDescription());
			return out;
		}
	}

	@Override
	public CardDescriptor cardDescriptorFrom(ReferenceType referenceType) {
		checkNotNull(emptyToNull(referenceType), "reference type param is null");
		ClassInfo classInfo;
		try {
			classInfo = checkNotNull(findClass(referenceType.getIdClass()));
		} catch (Exception ex) {
			logger.warn("class not found for id = {} (trying fallback query on 'Class'): {}", referenceType.getIdClass(), ex);
			ReferenceType fallbackReferenceType = referenceTypeFrom(referenceType.getId());
			classInfo = checkNotNull(findClass(fallbackReferenceType.getIdClass()), "class not found for id = %s", referenceType.getIdClass());
		}
		return new CardDescriptor(classInfo.getName(), referenceType.getId());
	}

	@Override
	public Card cardFrom(ReferenceType referenceType) {
		return fluentApi.existingCard(cardDescriptorFrom(referenceType)).fetch();
	}

	@Override
	public ReferenceType referenceTypeFrom(Card card) {
		return new ReferenceType(
				toIntExact(card.getId()),
				toIntExact(findClass(card.getClassName()).getId()),
				ofNullable(card.getDescription()).orElseGet(() -> fluentApi.existingCard(card).limitAttributes(Constants.DESCRIPTION_ATTRIBUTE).fetch().getDescription()));
	}

	@Override
	public ReferenceType referenceTypeFrom(Object idAsObject) {
		Long id = CmdbConvertUtils.convert(idAsObject, Long.class);
		if (isNotNullAndGtZero(id)) {
			return referenceTypeFrom(fluentApi.existingCard(Constants.BASE_CLASS_NAME, id).limitAttributes(Constants.DESCRIPTION_ATTRIBUTE).fetch());
		} else {
			return new ReferenceType();
		}
	}

	@Override
	public ReferenceType referenceTypeFrom(CardDescriptor cardDescriptor) {
		return referenceTypeFrom(fluentApi.existingCard(cardDescriptor));
	}
}
