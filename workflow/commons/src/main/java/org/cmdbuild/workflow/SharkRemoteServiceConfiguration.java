package org.cmdbuild.workflow;

import com.google.common.eventbus.EventBus;
import java.util.Properties;

public interface SharkRemoteServiceConfiguration {

	String getServerUrl();

	String getUsername();

	String getPassword();

	EventBus getEventBus();

	default Properties getClientProperties() {
		Properties clientProps = new Properties();
		clientProps.put("ClientType", "WS");
		clientProps.put("SharkWSURLPrefix", getServerUrl());
		return clientProps;
	}

}
