package org.cmdbuild.workflow.inner;

import org.cmdbuild.workflow.model.Task;
import java.util.List;
import javax.activation.DataSource;
import javax.annotation.Nullable;
import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptions;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptionsImpl;
import org.cmdbuild.data.filter.CmdbSorter;
import org.cmdbuild.workflow.model.XpdlInfo;
import org.cmdbuild.workflow.model.Flow;

public interface WorkflowServiceBase {

	List<XpdlInfo> getXpdlInfosOrderByVersionDesc(String classId);

	XpdlInfo addXpdl(String classId, DataSource dataSource);

	List<Task> getTaskListForCurrentUserByClassIdAndCardId(String classId, Long cardId); //TODO move this to facade?

	PagedElements<Task> getTaskListForCurrentUserByClassId(String classId, DaoQueryOptions queryOptions); //TODO move this to facade?

	default PagedElements<Task> getTaskListForCurrentUserByClassId(String classId, @Nullable Integer offset, @Nullable Integer limit, CmdbSorter sort) {
		return getTaskListForCurrentUserByClassId(classId, DaoQueryOptionsImpl.builder().withOffset(offset).withLimit(limit).withSorter(sort).build());
	}

	Task getTask(Flow flowCard, String taskId);

	Iterable<Task> getTaskList(Flow flowCard);

	void abortProcessInstance(Flow flowCard);

	void suspendProcessInstance(Flow flowCard);

	void resumeProcessInstance(Flow flowCard);

	/**
	 * Synchronizes the local store with the workflow service.
	 *
	 * legacy stuff, used only by shark
	 */
	default void sync() {
	}
}
