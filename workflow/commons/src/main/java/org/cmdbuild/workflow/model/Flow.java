package org.cmdbuild.workflow.model;

import java.util.List;
import org.cmdbuild.dao.beans.Card;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;

public interface Flow extends Card {

	/**
	 * It should return {@link CMCard.getId()}. It is used to disambiguate
	 * between the card and process instance ids.
	 *
	 * @return identifier of the data store card
	 */
	Long getCardId();

	/**
	 * We cannot override {@link CMCard.getId()} because it would break the
	 * method semantics.
	 *
	 * @return identifier of the process instance
	 */
	String getFlowId();

	@Override
	Process getType();

	/**
	 * Get current process instance state.
	 *
	 * @return the current process state
	 */
	FlowStatus getStatus();

	/**
	 * Returns an object with the ids to uniquely identify a process definition.
	 *
	 * @return unique process definition informations
	 */
	PlanInfo getPlanInfo();

	default String getPlanId() {
		return getPlanInfo().getDefinitionId();
	}

	default List<FlowActivity> getFlowActivities() {
		throw new UnsupportedOperationException("TODO");
	}

	default boolean isCompleted() {
		switch (getStatus()) {
			case TERMINATED:
			case COMPLETED:
			case ABORTED:
				return true;
			case OPEN:
			case SUSPENDED:
				return false;
			default:
				throw unsupported("unsupported flow status = %s", getStatus());
		}
	}

}
