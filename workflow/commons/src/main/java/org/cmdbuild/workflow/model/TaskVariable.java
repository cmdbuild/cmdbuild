package org.cmdbuild.workflow.model;

public interface TaskVariable {

	String getName();

	boolean isWritable();

	boolean isMandatory();
	
	boolean isAction();

}
