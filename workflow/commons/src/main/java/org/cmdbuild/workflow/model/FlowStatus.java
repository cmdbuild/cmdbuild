package org.cmdbuild.workflow.model;

/**
 * Process states that we care about.
 */
public enum FlowStatus {
	OPEN, SUSPENDED, COMPLETED, TERMINATED, ABORTED, UNSUPPORTED, UNDEFINED
}
