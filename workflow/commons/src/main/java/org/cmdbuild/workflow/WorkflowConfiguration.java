package org.cmdbuild.workflow;

import java.util.Set;

public interface WorkflowConfiguration extends SharkRemoteServiceConfiguration {

	boolean isEnabled();

	boolean isSynchronizationOfMissingVariablesDisabled();

	String getDefaultWorkflowProvider();

	Set<String> getEnabledWorkflowProviders();
	
	boolean isUserTaskParametersValidationEnabled();

}
