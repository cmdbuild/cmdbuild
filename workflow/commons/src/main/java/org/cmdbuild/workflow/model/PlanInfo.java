package org.cmdbuild.workflow.model;

public interface PlanInfo extends PlanPackageDefinitionInfo {

	String getDefinitionId();

	String getPlanId();
}
