package org.cmdbuild.workflow.model;

import java.util.List;
import org.cmdbuild.widget.model.WidgetData;


/**
 * Definition of a process activity.
 */
public interface TaskDefinition {

	String getId();

	String getDescription();

	String getInstructions();

	/**
	 * Returns the performers defined for this activity.
	 *
	 * @return list of defined performers
	 */
	List<TaskPerformer> getPerformers();

	/**
	 * Returns the first non-admin performer defined for this activity.
	 *
	 * @return role or expression performer
	 */
	TaskPerformer getFirstNonAdminPerformer();

	/**
	 * Returns an ordered list of variables to be displayed on the form.
	 *
	 * @return
	 */
	List<TaskVariable> getVariables();

	Iterable<TaskMetadata> getMetadata();

	List<WidgetData> getWidgets();

}
