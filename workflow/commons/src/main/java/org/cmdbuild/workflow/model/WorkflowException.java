package org.cmdbuild.workflow.model;

import static java.lang.String.format;

public class WorkflowException extends RuntimeException {

	public WorkflowException(Throwable nativeException) {
		super(nativeException);
	}

	public WorkflowException(String message) {
		super(message);
	}

	public WorkflowException(String message, Throwable nativeException) {
		super(message, nativeException);
	}

	public WorkflowException(Throwable nativeException, String format, Object... params) {
		super(format(format, params), nativeException);
	}

	public WorkflowException(String format, Object... params) {
		super(format(format, params));
	}
}
