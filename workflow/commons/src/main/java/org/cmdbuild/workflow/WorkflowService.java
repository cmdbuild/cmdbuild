package org.cmdbuild.workflow;

import org.cmdbuild.workflow.inner.WorkflowServiceBase;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.activation.DataSource;
import javax.annotation.Nullable;
import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptions;
import org.cmdbuild.workflow.inner.UserFlowWithPosition;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.widget.model.WidgetData;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.Process;
import org.cmdbuild.workflow.model.XpdlInfo;

public interface WorkflowService extends WorkflowServiceBase {

	boolean isWorkflowEnabled();

	boolean isWorkflowEnabledAndProcessRunnable(String classId);

	PagedElements<Flow> getFlowCardsByClasseIdAndQueryOptions(String classId, DaoQueryOptions queryOptions);

	PagedElements<Flow> getFlowCardsByClasseAndQueryOptions(Classe classe, DaoQueryOptions queryOptions);

	PagedElements<UserFlowWithPosition> queryWithPosition(String className, DaoQueryOptions queryOptions, Iterable<Long> cardId);

	@Nullable
	Flow getFlowCardOrNull(Process classe, Long cardId);

	default Flow getFlowCard(Process classe, Long cardId) {
		return checkNotNull(getFlowCardOrNull(classe, cardId), "flow card not found for classe = %s cardId = %s", classe, cardId);
	}

	default Flow getFlowCard(String classId, Long cardId) {
		Process classe = getProcess(classId);
		return getFlowCard(classe, cardId);
	}

	Process getProcess(String classId);

	XpdlInfo addXpdl(String classId, String provider, DataSource dataSource);

	DataSource getXpdlByClasseIdAndPlanId(String classId, String planId);

	Collection<Process> getActiveProcessClasses();

	Collection<Process> getAllProcessClasses();

	Task getUserTask(Flow card, String activityInstanceId);

	List<WidgetData> getWidgetsForUserTask(String classeId, Long cardId, String taskId);

	FlowAdvanceResponse startProcess(String classId, Map<String, ?> vars, boolean advance);

	FlowAdvanceResponse updateProcess(String classId, Long cardId, String taskId, Map<String, ?> vars, boolean advance);

	DataSource getXpdlTemplate(String classId);

	void suspendProcess(String classId, Long cardId);

	void resumeProcess(String classId, Long cardId);

	void abortProcess(String classId, Long cardId);

	void migrateFlowInstancesToNewProvider(Process process, String provider);

	default void migrateFlowInstancesToNewProvider(String classId, String provider) {
		migrateFlowInstancesToNewProvider(getProcess(classId), provider);
	}

}
