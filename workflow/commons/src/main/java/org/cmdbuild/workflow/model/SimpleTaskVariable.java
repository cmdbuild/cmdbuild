package org.cmdbuild.workflow.model;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class SimpleTaskVariable implements TaskVariable {

	private final String name;
	private final boolean writable, mandatory, action;

	private SimpleTaskVariable(SimpleTaskVariableBuilder builder) {
		this.name = checkNotBlank(builder.name);
		this.writable = checkNotNull(builder.writable);
		this.mandatory = checkNotNull(builder.mandatory);
		this.action = checkNotNull(builder.action);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isWritable() {
		return writable;
	}

	@Override
	public boolean isMandatory() {
		return mandatory;
	}

	@Override
	public boolean isAction() {
		return action;
	}

	public static SimpleTaskVariableBuilder builder() {
		return new SimpleTaskVariableBuilder();
	}

	public static SimpleTaskVariableBuilder copyOf(SimpleTaskVariable source) {
		return new SimpleTaskVariableBuilder()
				.withName(source.getName())
				.withWritable(source.isWritable())
				.withMandatory(source.isMandatory())
				.withAction(source.isAction());
	}

	@Override
	public String toString() {
		return "SimpleTaskVariable{" + "name=" + name + ", writable=" + writable + ", mandatory=" + mandatory + ", action=" + action + '}';
	}

	public static class SimpleTaskVariableBuilder implements Builder<SimpleTaskVariable, SimpleTaskVariableBuilder> {

		private String name;
		private boolean writable = false;
		private boolean mandatory = false;
		private boolean action = false;

		public SimpleTaskVariableBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public SimpleTaskVariableBuilder withWritable(boolean writable) {
			this.writable = writable;
			return this;
		}

		public SimpleTaskVariableBuilder withMandatory(boolean mandatory) {
			this.mandatory = mandatory;
			return this;
		}

		public SimpleTaskVariableBuilder withAction(boolean action) {
			this.action = action;
			return this;
		}

		@Override
		public SimpleTaskVariable build() {
			return new SimpleTaskVariable(this);
		}

	}
}
