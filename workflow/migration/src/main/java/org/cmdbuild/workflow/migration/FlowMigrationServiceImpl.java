/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.migration;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.config.api.GlobalConfigService;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import org.cmdbuild.dao.beans.ClassMetadataImpl;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_FLOW_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_PLAN_INFO;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_TASK_DEFINITION_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_TASK_INSTANCE_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.FLOW_ATTR_DATA;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptionsImpl;
import org.cmdbuild.dao.driver.repository.ClasseRepository;
import org.cmdbuild.dao.entrytype.ClassDefinitionImpl;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverPlan;
import static org.cmdbuild.river.xpdl.XpdlUtils.buildStepIdFromParentActivityIdAndActivityId;
import static org.cmdbuild.river.xpdl.XpdlUtils.buildUserTaskId;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.isBlank;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.cmdbuild.utils.random.CmdbuildRandomUtils;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.cmdbuild.workflow.inner.FlowCardRepository;
import org.cmdbuild.workflow.inner.FlowMigrationService;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.FlowActivity;
import org.cmdbuild.workflow.model.Process;
import static org.cmdbuild.workflow.WorkflowCommonConst.RIVER;
import static org.cmdbuild.workflow.WorkflowCommonConst.SHARK;
import org.cmdbuild.workflow.model.WorkflowException;
import org.cmdbuild.workflow.river.dao.ExtendedRiverPlanRepository;
import static org.cmdbuild.workflow.river.utils.FlowDataSerializerUtils.serializeRiverFlowData;
import static org.cmdbuild.workflow.river.utils.WfRiverXpdlUtils.riverPlanIdToLegacyUniqueProcessDefinition;
import org.cmdbuild.workflow.shark.SharkFlowVariablesSynchronizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class FlowMigrationServiceImpl implements FlowMigrationService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final GlobalConfigService configService;
	private final ExtendedRiverPlanRepository riverPlanRepository;
	private final FlowCardRepository flowCardRepository;
	private final DaoService dao;
	private final ClasseRepository classeRepository;
	private final SharkFlowVariablesSynchronizer sharkFlowVariablesSynchronizer;

	public FlowMigrationServiceImpl(GlobalConfigService configService, ExtendedRiverPlanRepository riverPlanRepository, FlowCardRepository flowCardRepository, DaoService dao, ClasseRepository classeRepository, SharkFlowVariablesSynchronizer sharkFlowVariablesSynchronizer) {
		this.configService = checkNotNull(configService);
		this.riverPlanRepository = checkNotNull(riverPlanRepository);
		this.flowCardRepository = checkNotNull(flowCardRepository);
		this.dao = checkNotNull(dao);
		this.classeRepository = checkNotNull(classeRepository);
		this.sharkFlowVariablesSynchronizer = checkNotNull(sharkFlowVariablesSynchronizer);
	}

	@Override
	public void migrateFlowInstancesToNewProvider(Process process, String provider) {
		if (equal(process.getProviderOrNull(), provider)) {
			logger.info("new provider si same as old provider, nothing ro do");
		} else if (flowCardRepository.getCardsByClassIdAndQueryOptions(process.getName(), DaoQueryOptionsImpl.builder().build()).isEmpty()) {//TODO count only (?)
			updateProcessProvider(process, provider);
		} else {
			if (equal(provider, RIVER) && equal(process.getProviderOrNull(), SHARK)) {
				new SharkToRiverMigrationProcess(process).migrateFlowInstancesFromSharkToRiver();
			} else {
				throw unsupported("unsupported migration of process = %s to provider = %s", process, provider);
			}
		}
	}

	private void updateProcessProvider(Process process, String provider) {
		logger.info("update process = {}, set default provider = river", process);
		classeRepository.updateClass(ClassDefinitionImpl.copyOf(process).withMetadata(ClassMetadataImpl.copyOf(process.getMetadata()).withFlowProvider(provider).build()).build());
	}

	private class SharkToRiverMigrationProcess {

		private final JdbcTemplate sharkJdbcTemplate;
		private final Process process;
		private final RiverPlan riverPlan;

		public SharkToRiverMigrationProcess(Process process) {
			this.process = checkNotNull(process);

			logger.info("preparing shark to river migration process");

			riverPlan = riverPlanRepository.getPlanByClasseId(process.getName());
			logger.info("target plan is = {}", riverPlan);

			String url = checkNotBlank(configService.getString("org.cmdbuild.workflow.shark.db.url"), "shark db url is required for process migration (set config param 'org.cmdbuild.workflow.shark.db.url')"),
					username = checkNotBlank(configService.getString("org.cmdbuild.workflow.shark.db.username"), "shark db username is required for process migration (set config param 'org.cmdbuild.workflow.shark.db.username')"),
					password = checkNotBlank(configService.getString("org.cmdbuild.workflow.shark.db.password"), "shark db password is required for process migration (set config param 'org.cmdbuild.workflow.shark.db.password')");
			BasicDataSource dataSource = new BasicDataSource();
			dataSource.setDriverClassName("org.postgresql.Driver");
			dataSource.setUrl(url);
			dataSource.setUsername(username);
			dataSource.setPassword(password);
			sharkJdbcTemplate = new JdbcTemplate(dataSource);

			logger.info("shark database is = {}", url);
		}

		private void migrateFlowInstancesFromSharkToRiver() {
			logger.info("running shark to river migration process, for process = {}", process);

			List<Flow> flowList = flowCardRepository.getCardsByClassIdAndQueryOptions(process.getName(), DaoQueryOptionsImpl.builder().build()).elements();//TODO get only active instances
			logger.info("execute migration of {} instances", flowList.size());
			if (flowList.stream().map(Flow::getPlanId).distinct().count() > 1) {
				logger.warn("more than one xpdl found with active instances!! old instances may not be compatible with new xpdl supplied for migration");//TODO: throw exception??
			}
			flowList.forEach(this::migrateFlowInstance);

			updateProcessProvider(process, RIVER);
		}

		private void migrateFlowInstance(Flow flow) {
			logger.info("migrate flow instance = {}", flow);
			try {
				logger.info("get flow variables from shark for flow = {}", flow);
				Map<String, Object> data = sharkFlowVariablesSynchronizer.getSharkVariables(flow);

				List<String> riverTaskIds = flow.getFlowActivities().stream().map(FlowActivity::getInstanceId).map((sharkActivityId) -> {
					logger.info("process shark activity = {}", sharkActivityId);
					Pair<String, String> sharkActivityDefinitionIds = sharkJdbcTemplate.queryForObject("SELECT _activity.activitydefinitionid _activity_definition_id, _activity_parent.activitydefinitionid _parent_activity_definition_id FROM shark.shkactivities _activity LEFT JOIN shark.shkactivities _activity_parent ON _activity.blockactivityid = _activity_parent.id WHERE _activity.id = ?", (r, i) -> {
						return Pair.of(checkNotBlank(r.getString("_activity_definition_id")), r.getString("_parent_activity_definition_id"));
					}, sharkActivityId);
					logger.info("shark activity id = {} has activity definition id = {} and parent activity definition id = {}", sharkActivityId, sharkActivityDefinitionIds.getLeft(), sharkActivityDefinitionIds.getRight());
					String riverStepId;
					if (isBlank(sharkActivityDefinitionIds.getRight())) {
						riverStepId = sharkActivityDefinitionIds.getLeft();
					} else {
						riverStepId = buildStepIdFromParentActivityIdAndActivityId(sharkActivityDefinitionIds.getRight(), sharkActivityDefinitionIds.getLeft());
					}
					riverPlan.getStepById(riverStepId);//check that step exists for this id
					String riverTaskId = buildUserTaskId(riverStepId);
					logger.info("converting shark activity id = {} to river task id = {}", sharkActivityId, riverTaskId);
					riverPlan.getTask(riverTaskId);//check that task exists for this id
					return riverTaskId;
				}).collect(toList());

				String serializedData = serializeRiverFlowData(data, process, riverPlan, RiverFlow.FlowStatus.RUNNING);//TODO check state (suspended? aborted?)

				Card card = CardImpl.copyOf(flow)
						.addAttribute(ATTR_TASK_INSTANCE_ID, riverTaskIds.stream().map((x) -> CmdbuildRandomUtils.randomId()).collect(toList())) //TODO replace randomId with specific newTaskId function
						.addAttribute(ATTR_TASK_DEFINITION_ID, riverTaskIds)
						.addAttribute(ATTR_PLAN_INFO, riverPlanIdToLegacyUniqueProcessDefinition(riverPlan.getId()))
						.addAttribute(ATTR_FLOW_ID, randomId())//TODO use newRiverFlowId() function
						.addAttribute(FLOW_ATTR_DATA, serializedData)
						.build();

				logger.info("update flow with river data, flow = {}", flow);
				dao.update(card);
			} catch (Exception ex) {
				throw new WorkflowException(ex, "error migrating flow instance = %s", flow);
			}
		}
	}

}
