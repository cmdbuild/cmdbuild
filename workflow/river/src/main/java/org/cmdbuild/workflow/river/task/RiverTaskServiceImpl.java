/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.task;

import org.cmdbuild.jobs.JobExecutorService;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import java.lang.reflect.Method;
import static java.util.Arrays.asList;
import java.util.Map;
import java.util.function.Consumer;
import javax.inject.Provider;
import org.cmdbuild.river.core.CompletedTaskImpl;
import org.cmdbuild.river.lock.AquiredLock;
import org.cmdbuild.river.lock.LockResponse;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.RiverLiveTask;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.river.lock.RiverLockService;
import org.cmdbuild.river.task.RiverTaskService;
import static org.cmdbuild.utils.lang.CmdbReflectionUtils.wrapProxy;
import org.cmdbuild.utils.lang.ProxyWrapper;
import org.springframework.stereotype.Component;
import static org.cmdbuild.river.task.SubmitTaskResponseForInlineTask.inlineTask;
import static org.cmdbuild.river.task.SubmitTaskResponseForQueuedTask.queuedTask;
import org.cmdbuild.river.task.scriptexecutors.GroovyScriptServiceImpl;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorService;
import org.cmdbuild.river.task.scriptexecutors.TaskScriptExecutorServiceImpl;
import org.cmdbuild.workflow.core.fluentapi.ExtendedApi;

@Component
public class RiverTaskServiceImpl implements RiverTaskService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final RiverLockService lockService;
	private final JobExecutorService executorService;
	private final Consumer<RiverTaskCompleted> completedTaskConsumer;
	private final Provider<ExtendedApi> workflowApi;
	private final TaskScriptExecutorService taskScriptExecutorService = new TaskScriptExecutorServiceImpl(new GroovyScriptServiceImpl());

	public RiverTaskServiceImpl(RiverLockService lockService, Consumer<RiverTaskCompleted> completedTaskConsumer, JobExecutorService executorService, Provider<ExtendedApi> cmdbApi) {
		this.lockService = checkNotNull(lockService);
		this.completedTaskConsumer = checkNotNull(completedTaskConsumer);
		this.executorService = checkNotNull(executorService);
		this.workflowApi = checkNotNull(cmdbApi);
	}

	private void consumeCompletedTask(RiverTaskCompleted completedTask) {
		logger.debug("consuming completed task = {}", completedTask.getTask());
		completedTaskConsumer.accept(completedTask);
	}

	@Override
	public SubmitTaskResponse submitTask(RiverLiveTask task) {
		logger.debug("submit for execution task = {} (next action will depend on task type)", task);
		switch (task.getTaskType()) {
			case USER:
				//do nothing, will add user task later
//				logger.debug("add user task to task list");
//				taskRepository.createTask(task);
				return queuedTask();
			case SCRIPT_BATCH:
				submitJob(task);
				return queuedTask();
			case SCRIPT_INLINE:
			case NOP:
				return inlineTask(() -> executeTask(task));
			default:
				throw new IllegalArgumentException("unsupported task type " + task.getTaskType());
		}
	}

	private void submitJob(RiverLiveTask task) {
		String user = "admin";//TODO get user from live task
		executorService.executeJobAs(() -> {
			aquireLockAndExecuteTask(task);
		}, user);
	}

	private void aquireLockAndExecuteTask(RiverLiveTask task) {
		logger.debug("aquire lock for task {}", task);
		LockResponse lockResponse = lockService.aquireLock(task.getFlowId());
		if (lockResponse.isAquired()) {
			AquiredLock aquiredLock = lockResponse.aquired();
			try {
				RiverTaskCompleted completedTask = executeTask(task);
				consumeCompletedTask(completedTask);
			} finally {
				lockService.releaseLock(aquiredLock);
			}
		} else {
			logger.debug("unable to aquire lock, re-scheduling");
			String waitUser = "admin";//TODO replace with low power system user
			executorService.executeJobLaterAs(() -> {
				submitJob(task);
			}, waitUser, 100);
		}
	}

	private RiverTaskCompleted executeTask(RiverLiveTask liveTask) {
		logger.debug("execute task {}", liveTask);
		RiverTask task = liveTask.getTask();
		RiverTaskCompleted completedTask;
		if (task.isNoop()) {
			logger.debug("task is NOOP, no execution is required");
			completedTask = new CompletedTaskImpl(liveTask);
		} else {
			logger.debug("task is script, preparing data for execution");
			Map<String, Object> dataIn = map(liveTask.getFlow().getData()).with("cmdb", getWorkflowApi());
			logger.debug("begin task script execution for task = {}", liveTask);
			completedTask = taskScriptExecutorService.executeTask(liveTask, dataIn);
			logger.debug("completed task script execution for task = {}", liveTask);
		}
		return completedTask;
	}

	private ExtendedApi getWorkflowApi() {
		return wrapProxy(ExtendedApi.class, checkNotNull(workflowApi.get()), new LoggerProxyWrapper());
	}

	private class LoggerProxyWrapper extends ProxyWrapper {

		@Override
		public void afterFailedMethodInvocation(Method method, Object[] params, Throwable error) {
			logger.error(format("error invoking method = %s of cmdbuild api with params = %s", method.getName(), asList(params)), error);
		}

	}

}
