/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import com.google.common.collect.Maps;
import static com.google.common.collect.Maps.filterKeys;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverVariableInfo;
import static org.cmdbuild.utils.json.CmJsonUtils.fromJson;
import static org.cmdbuild.utils.json.CmJsonUtils.toJson;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.isPrimitiveOrWrapper;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;
import static org.cmdbuild.utils.lang.CmdbNullableUtils.getClassOfNullable;
import static org.cmdbuild.utils.lang.CmdbStringUtils.mapToLoggableString;
import org.cmdbuild.workflow.model.WorkflowException;
import org.cmdbuild.workflow.type.LookupType;
import org.cmdbuild.workflow.type.ReferenceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlowDataSerializerUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(FlowDataSerializerUtils.class);

	private final static int SERIALIZATION_VERSION = 2;

	public static String serializeRiverFlowData(Map<String, Object> data, org.cmdbuild.workflow.model.Process classe, RiverPlan plan, RiverFlow.FlowStatus flowStatus) {

		data = filterOutAttributesStoredInCard(data, classe);

		data = filterOutAttributesNotInPlan(data, plan);

		data = filterOutValuesEqualToDefault(data, plan);

		data = map(data).with("RiverFlowStatus", flowStatus.name());

		return serializeFlowData(data);
	}

	private static Map<String, Object> filterOutAttributesStoredInCard(Map<String, Object> data, org.cmdbuild.workflow.model.Process classe) {
		return Maps.filterKeys(data, not(classe::hasAttribute));
	}

	private static Map<String, Object> filterOutAttributesNotInPlan(Map<String, Object> data, RiverPlan plan) {
		return Maps.filterKeys(data, plan::hasGlobalVariable);
	}

	private static Map<String, Object> filterOutValuesEqualToDefault(Map<String, Object> data, RiverPlan plan) {
		return Maps.filterEntries(data, (e) -> {
			return !equal(e.getValue(), plan.getDefaultValueOrNull(e.getKey()));
		});
	}

	public static String serializeFlowData(Map<String, Object> map) {
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("serialize flow data = \n\n{}\n", mapToLoggableString(map));
		}
		Map<String, Object> data = map.entrySet().stream().map((e) -> {
			try {
				return Pair.of(e.getKey(), serializeValue(e.getValue()));
			} catch (Exception ex) {
				throw new WorkflowException(ex, "error serializing flow value for key = %s value = %s (%s)", e.getKey(), e.getValue(), getClassOfNullable(e.getValue()));
			}
		}).collect(toMap(Pair::getKey, Pair::getValue));
		return toJson(new SerializedData(SERIALIZATION_VERSION, data));
	}

	public static Map<String, Object> deserializeFlowData(@Nullable String data, RiverPlan riverPlan) {
		if (isBlank(data)) {
			return emptyMap();
		} else {
			SerializedData bean = fromJson(data, SerializedData.class);
			checkArgument(equal(SERIALIZATION_VERSION, bean.getVersion()), "serialized data version mismatch, expected = %s, got = %s", SERIALIZATION_VERSION, bean.getVersion());

			return riverPlan.getGlobalVariables().values().stream().collect(toMap(RiverVariableInfo::getKey, (variable) -> {
				if (bean.getData().containsKey(variable.getKey())) {
					return deserializeValue(bean.getData().get(variable.getKey()), variable);
				} else {
					return variable.getDefaultValue().orElse(null);
				}
			})).with(filterKeys(bean.getData(), not(riverPlan.getGlobalVariables()::containsKey)));
		}
	}

	public static @Nullable
	Object serializeValue(@Nullable Object value) {
		if (value == null) {
			return null;
		} else if (value instanceof ReferenceType) {
			return serializeReferenceType((ReferenceType) value);
		} else if (value instanceof ReferenceType[]) {
			return stream((ReferenceType[]) value).map(FlowDataSerializerUtils::serializeReferenceType).collect(toList());
		} else if (value instanceof LookupType) {
			return serializeLookupType((LookupType) value);
		} else if (value instanceof LookupType[]) {
			return stream((LookupType[]) value).map(FlowDataSerializerUtils::serializeLookupType).collect(toList());
		} else {
			checkArgument(isPrimitiveOrWrapper(value) || (value instanceof String[]), "unable to serialize value = %s, unsupported class = %s", value, getClassOfNullable(value));
			return value;
		}
	}

	public static @Nullable
	Object deserializeValue(@Nullable Object value, RiverVariableInfo variable) {
		if (value == null) {
			return null;
		} else if (isPrimitiveOrWrapper(variable.getJavaType())) {
			return convert(value, variable.getJavaType());
		} else if (ReferenceType.class.equals(variable.getJavaType())) {
			return deserializeReferenceType(value);
		} else if (ReferenceType[].class.equals(variable.getJavaType())) {
			return ((List) convert(value, List.class).stream().map(FlowDataSerializerUtils::deserializeReferenceType).collect(toList())).toArray(new ReferenceType[]{});
		} else if (LookupType.class.equals(variable.getJavaType())) {
			return deserializeLookupType(value);
		} else if (LookupType[].class.equals(variable.getJavaType())) {
			return ((List) convert(value, List.class).stream().map(FlowDataSerializerUtils::deserializeLookupType).collect(toList())).toArray(new LookupType[]{});
		} else {
			throw new WorkflowException("unable to deserialize value = '%s' with variable type = %s", value, variable);
		}
	}

	private static Object serializeReferenceType(ReferenceType referenceType) {
		return map("i", referenceType.getId(), "t", referenceType.getIdClass(), "d", referenceType.getDescription());
	}

	private static Object serializeLookupType(LookupType lookupType) {
		return map("i", lookupType.getId(), "t", lookupType.getType(), "c", lookupType.getCode(), "d", lookupType.getDescription());
	}

	private static ReferenceType deserializeReferenceType(Object value) {
		Map map = (Map) value;
		return new ReferenceType(convert(map.get("i"), Integer.class), convert(map.get("t"), Integer.class), convert(map.get("d"), String.class));//TODO replace reference type with new long id bean
	}

	private static LookupType deserializeLookupType(Object value) {
		Map map = (Map) value;
		return new LookupType(convert(map.get("i"), Integer.class), convert(map.get("t"), String.class), convert(map.get("d"), String.class), convert(map.get("c"), String.class));//TODO replace reference type with new long id bean
	}

	private static class SerializedData {

		private final Integer version;
		private final Map<String, Object> data;

		@JsonCreator
		public SerializedData(@JsonProperty("v") Integer version, @JsonProperty("d") Map<String, Object> data) {
			this.version = checkNotNull(version);
			this.data = checkNotNull(data);
		}

		@JsonProperty("v")
		public int getVersion() {
			return version;
		}

		@JsonProperty("d")
		public Map<String, Object> getData() {
			return data;
		}

	}
}
