/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.dao.converters;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.MoreCollectors.toOptional;
import static java.lang.String.format;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverTask;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import org.cmdbuild.widget.WidgetService;
import org.cmdbuild.widget.model.Widget;
import org.cmdbuild.workflow.core.model.TaskImpl;
import org.cmdbuild.workflow.model.TaskDefinition;
import org.cmdbuild.workflow.core.utils.WfWidgetUtils;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.workflow.model.WorkflowException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.widget.model.WidgetData;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.river.dao.RiverFlowConversionService;

@Component
public class TaskConversionServiceImpl implements TaskConversionService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final WidgetService widgetService;
	private final OperationUserSupplier userSupplier;
	private final RiverFlowConversionService conversionService;
	private final TaskDefinitionConversionService definitionConversionService;

	public TaskConversionServiceImpl(WidgetService widgetService, OperationUserSupplier userSupplier, RiverFlowConversionService conversionService, TaskDefinitionConversionService definitionConversionService) {
		this.widgetService = checkNotNull(widgetService);
		this.userSupplier = checkNotNull(userSupplier);
		this.conversionService = checkNotNull(conversionService);
		this.definitionConversionService = checkNotNull(definitionConversionService);
	}

	@Override
	public Task toUserTask(Flow flow, RiverTask task, String userTaskId, String taskPerformer) {
		RiverFlow riverFlow = conversionService.cardToRiverFlow(flow);//TODO: cache converted river flow in flow instance
		return toUserTask(flow, riverFlow, task, userTaskId, taskPerformer);
	}

	@Override
	public List<Task> getTaskList(Flow flow) {
		RiverFlow riverFlow = conversionService.cardToRiverFlow(flow);
		return flow.getFlowActivities().stream().map((a) -> toUserTask(flow, riverFlow, riverFlow.getPlan().getTask(a.getDefinitionId()), a.getInstanceId(), a.getPerformerGroup())).collect(toList());
	}

	@Override
	public Task getTask(Flow card, String userTaskId) {
		return getTaskList(card).stream().filter((t) -> equal(t.getId(), userTaskId)).collect(toOptional()).orElseThrow(() -> new WorkflowException("task not found for card = %s user task id = %s", card, userTaskId));
	}

	private Task toUserTask(Flow flow, RiverFlow riverFlow, RiverTask task, String userTaskId, String taskPerformer) {
		try {
			logger.debug("convert river task = {} from flow = {} to user task", task, flow);

			TaskDefinition taskDefinition = definitionConversionService.toTaskDefinition(task);

//			String taskPerformer = taskData.getAssignedGroup();
			OperationUser operationUser = userSupplier.getUser();

			Task res = TaskImpl.builder()
					.withTaskDefinition(taskDefinition)
					.withTaskId(userTaskId)
					.withFlowId(flow.getFlowId())
					.withTaskPerformer(taskPerformer)
					.withTaskWidgetSupplier(new TaskWidgetSupplier(riverFlow::getData, taskDefinition))
					.isAdvanceable(isAdvanceableByCurrentUser(operationUser, taskPerformer))
					.withCard(flow)
					.build();

			logger.debug("converted river task = {} from flow = {} to user task = {}", task, flow, res);

			return res;
		} catch (Exception ex) {
			throw new WorkflowException(format("error converting task = %s to user task", task), ex);
		}
	}

	private class TaskWidgetSupplier implements Supplier<List<Widget>> {

		private final Supplier<Map<String, Object>> flowDataSupplier;
		private final TaskDefinition taskDefinition;
		private List<Widget> widgets;

		public TaskWidgetSupplier(Supplier<Map<String, Object>> flowDataSupplier, TaskDefinition taskDefinition) {
			this.flowDataSupplier = checkNotNull(flowDataSupplier);
			this.taskDefinition = checkNotNull(taskDefinition);
		}

		@Override
		public List<Widget> get() {
			if (widgets == null) {

				Map<String, Object> dataForWidgets = flowDataSupplier.get();

				dataForWidgets = map(transformValues(dataForWidgets, WfWidgetUtils::convertValueForWidget));

				widgets = widgetService.widgetDataToWidget(taskDefinition.getWidgets(), dataForWidgets);
			}
			return widgets;
		}

	}

	private boolean isAdvanceableByCurrentUser(OperationUser operationUser, String groupName) {//TODO duplicate code
		if (operationUser.hasAdminAccess()) {
			return true;
		} else {
			return operationUser.getGroupNames().contains(groupName);
		}
	}

}
