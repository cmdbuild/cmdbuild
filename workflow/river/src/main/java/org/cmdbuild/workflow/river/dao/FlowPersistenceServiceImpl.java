/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.dao;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyMap;
import java.util.Map;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.workflow.model.FlowStatus;
import org.cmdbuild.workflow.model.SimpleFlowInfo;
import org.cmdbuild.workflow.inner.FlowCardRepository;
import org.cmdbuild.workflow.model.SimpleFlowData;
import org.cmdbuild.workflow.model.SimpleFlowData.SimpleFlowDataBuilder;
import static org.cmdbuild.workflow.river.dao.RiverPlanRepositoryImpl.ATTR_BIND_TO_CLASS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.WorkflowTypeConverter;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.Process;
import static org.cmdbuild.workflow.WorkflowCommonConst.RIVER;
import org.cmdbuild.workflow.inner.ProcessRepository;
import org.cmdbuild.workflow.model.PlanInfo;
import static org.cmdbuild.workflow.river.utils.WfRiverXpdlUtils.riverPlanIdToPlanInfo;

@Component
public class FlowPersistenceServiceImpl implements FlowPersistenceService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final FlowCardRepository cardRepository;
	private final ProcessRepository classeRepository;
	private final WorkflowTypeConverter typeConverter;
	private final RiverFlowConversionService conversionService;

	public FlowPersistenceServiceImpl(FlowCardRepository cardRepository, ProcessRepository classeRepository, WorkflowTypeConverter typeConverter, RiverFlowConversionService conversionService) {
		this.cardRepository = checkNotNull(cardRepository);
		this.classeRepository = checkNotNull(classeRepository);
		this.typeConverter = checkNotNull(typeConverter);
		this.conversionService = checkNotNull(conversionService);
	}

	@Override
	public Flow createFlowCard(RiverFlow flow) {
		logger.debug("create flow card for flow = {}", flow);
		Process classe = classeRepository.getProcessClassByName((flow.getPlan().getAttr(ATTR_BIND_TO_CLASS)));
		PlanInfo planInfo = riverPlanIdToPlanInfo(flow.getPlanId());
		Flow card = cardRepository.createFlowCard(classe, toFlowData(classe, flow)
				.withInfo(new SimpleFlowInfo(flow.getId(), riverStatusToFlowStatus(flow.getStatus()), planInfo.getPackageId(), planInfo.getVersion(), planInfo.getDefinitionId()))
				.build(), () -> emptyMap());
		return card;
	}

	@Override
	public Flow updateFlowAndCard(RiverFlow flow) {
		Flow card = cardRepository.getFlowCardByPlanIdAndFlowId(RIVER, flow.getPlanId(), flow.getId());
		return updateFlowAndCard(card, flow);
	}

	@Override
	public Flow updateFlowAndCard(Flow card, RiverFlow flow) {
		logger.debug("update flow card for flow = {} card = {}", flow, card);
		card = conversionService.riverFlowToCard(card, flow);
		card = cardRepository.updateThisFlowCard(card, toFlowData(card.getType(), flow).build());
		return card;
	}

	private SimpleFlowDataBuilder toFlowData(Process classe, RiverFlow riverFlow) {
		Map data = typeConverter.flowValuesToCardValues(classe, riverFlow.getData());
		return SimpleFlowData.builder()
				.withStatus(riverStatusToFlowStatus(riverFlow.getStatus()))
				.withValues(data);
	}

	public static FlowStatus riverStatusToFlowStatus(RiverFlow.FlowStatus status) {
		switch (status) {
			case COMPLETE:
				return FlowStatus.COMPLETED;
			case ERROR:
				return FlowStatus.TERMINATED;
			case READY:
			case RUNNING:
				return FlowStatus.OPEN;
			default:
				throw new IllegalStateException();
		}
	}

}
