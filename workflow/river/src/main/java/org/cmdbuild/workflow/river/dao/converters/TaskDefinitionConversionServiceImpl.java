/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.dao.converters;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.notNull;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.easytemplate.EasytemplateProcessor;
import org.cmdbuild.river.RiverTask;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_ATTR_DESCRIPTION;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_ATTR_NAME;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_PERFORMER_TYPE_EXPR;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_PERFORMER_TYPE_ROLE;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import org.cmdbuild.workflow.model.SimpleTaskDefinition;
import org.cmdbuild.workflow.model.TaskDefinition;
import org.cmdbuild.workflow.model.TaskMetadata;
import org.cmdbuild.workflow.model.TaskPerformer;
import org.cmdbuild.workflow.model.TaskVariable;
import org.cmdbuild.workflow.core.xpdl.XpdlTaskUtils;
import org.cmdbuild.workflow.core.utils.TaskPerformerExpressionProcessorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.widget.model.WidgetData;
import static org.cmdbuild.widget.utils.WidgetUtils.isWorwflowWidgetType;
import static org.cmdbuild.widget.utils.WidgetUtils.toWidgetData;
import static org.cmdbuild.workflow.model.TaskPerformer.newRolePerformer;

@Component
public class TaskDefinitionConversionServiceImpl implements TaskDefinitionConversionService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EasytemplateProcessor templateResolver;
	private final CmdbCache<String, TaskDefinition> taskDefinitionCacheByTaskId;

	public TaskDefinitionConversionServiceImpl(EasytemplateProcessor templateResolver, CacheService cacheService) {
		this.templateResolver = checkNotNull(templateResolver);
		taskDefinitionCacheByTaskId = cacheService.newCache("river_wf_task_definition_cache_by_task_def_id");
	}

	@Override
	public TaskDefinition toTaskDefinition(RiverTask task) {
		return taskDefinitionCacheByTaskId.get(task.getId(), () -> doToTaskDefinition(task)); //warning: will cache processed taskPerformers, for example with data from templateResolver
	}

	private TaskDefinition doToTaskDefinition(RiverTask task) {
		logger.debug("convert river task = {} to task definition", task);
		return new TaskConverter(task).toTaskDefinition();
	}

	@Nullable
	private TaskPerformer getFirstNonAdminPerformerOrNull(List<TaskPerformer> taskPerformers) {//TODO check this
		return taskPerformers.stream()
				.filter(not(TaskPerformer::isAdmin))
				.findFirst().orElse(null);
	}

	private List<TaskPerformer> getProcessedTaskPerformersForTask(RiverTask task, Map<String, Object> flowData) {
		if (task.isUser()) {
			logger.trace("evaluating task performers for task = {}", task);
			List<TaskPerformer> taskPerformers = getTaskPerformers(task.getPerformerValue(), task.getPerformerType(), flowData);
			logger.trace("task performers for task = {} are = {}", task, taskPerformers);
			return taskPerformers;
		} else {
			return emptyList();
		}
	}

	private List<TaskPerformer> getTaskPerformers(String value, String type, Map<String, Object> flowData) {
		switch (type) {
			case TASK_PERFORMER_TYPE_ROLE:
				return singletonList(newRolePerformer(value));
			case TASK_PERFORMER_TYPE_EXPR:
				logger.debug("evaluating task performer expression = {}", value);
				return TaskPerformerExpressionProcessorUtils.getPerformersFromExpression(templateResolver, flowData, value).stream()
						.filter(StringUtils::isNotBlank)
						.map(TaskPerformer::newRolePerformer).collect(toList());
			default:
				throw unsupported("unsupported task performer type = %s", type);
		}
	}

	private class TaskConverter {

		private final RiverTask task;

		public TaskConverter(RiverTask task) {
			this.task = checkNotNull(task);
		}

		public TaskDefinition toTaskDefinition() {
			List<TaskPerformer> taskPerformers = getProcessedTaskPerformersForTask(task, emptyMap());
			SimpleTaskDefinition taskDefinition = SimpleTaskDefinition.builder()
					.withId(task.getId())
					.withDescription(task.getAttr(TASK_ATTR_NAME))
					.withInstructions(task.getAttr(TASK_ATTR_DESCRIPTION))
					.withVariables(getTaskVariables())
					.withMetadata(getTaskMetadata())
					.withWidgets(getTaskWidgets())
					.withPerformers(taskPerformers)//TODO check this
					.withFirstNonAdminPerformer(getFirstNonAdminPerformerOrNull(taskPerformers))
					//TODO extra data
					.build();
			return taskDefinition;

		}

		private List<TaskVariable> getTaskVariables() {
			return entriesForTask().map((entry) -> XpdlTaskUtils.taskVariableFromXpdlKeyValue(entry.getKey(), entry.getValue())).filter(notNull()).collect(toList());
		}

		private List<TaskMetadata> getTaskMetadata() {
			return entriesForTask().map((entry) -> XpdlTaskUtils.taskMetadataFromXpdlKeyValue(entry.getKey(), entry.getValue())).filter(notNull()).collect(toList());
		}

		private List<WidgetData> getTaskWidgets() {
			return entriesForTask().filter((e) -> isWorwflowWidgetType(e.getKey())).map((e) -> toWidgetData(e.getKey(), e.getValue())).collect(toList());
		}

		private Stream<Map.Entry<String, String>> entriesForTask() {
			return task.getExtendedAttributes().entries().stream();
		}
	}

}
