/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.core.BasicRiverFlowServiceImpl;
import org.cmdbuild.river.data.RiverFlowRepository;
import org.cmdbuild.river.data.RiverPlanRepository;
import org.springframework.stereotype.Component;
import org.cmdbuild.river.task.RiverTaskService;
import org.cmdbuild.workflow.river.dao.FlowPersistenceService;
import org.cmdbuild.workflow.river.task.CompletedTaskConsumerBridge;

@Component
public class RiverFlowServiceImpl extends BasicRiverFlowServiceImpl {

	private final FlowPersistenceService persistenceService;

	public RiverFlowServiceImpl(FlowPersistenceService persistenceService, RiverPlanRepository planRepository, RiverFlowRepository flowRepository, RiverTaskService taskService, CompletedTaskConsumerBridge bridge) {
		super(planRepository, flowRepository, taskService);
		this.persistenceService = checkNotNull(persistenceService);
		bridge.setConsumer((completedTask) -> completeTaskAndSaveFlow(completedTask));
	}

	private void completeTaskAndSaveFlow(RiverTaskCompleted completedTask) {
		//TODO check that we have lock on flow !!!
		RiverFlow flow = completedTask(completedTask);
		persistenceService.updateFlowAndCard(flow);
	}

}
