/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import com.google.common.collect.Maps;
import static java.lang.String.format;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.concat;
import javax.activation.DataSource;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.common.utils.PagedElements;
import static org.cmdbuild.common.utils.PagedElements.paged;
import org.cmdbuild.workflow.WorkflowConfiguration;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptions;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverFlowService;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.river.RiverTaskCompleted;
import org.cmdbuild.river.core.CompletedTaskImpl;
import org.cmdbuild.river.core.RiverFlowImpl;
import org.cmdbuild.river.lock.AquiredLock;
import org.springframework.stereotype.Component;
import org.cmdbuild.workflow.inner.WorkflowServiceDelegate;
import org.cmdbuild.workflow.inner.FlowCardRepository;
import org.cmdbuild.workflow.river.dao.ExtendedRiverPlanRepository;
import org.cmdbuild.river.lock.RiverLockService;
import org.cmdbuild.workflow.model.AdvancedFlowStatus;
import static org.cmdbuild.workflow.WorkflowCommonConst.RIVER;
import org.cmdbuild.workflow.model.SimpleFlowAdvanceResponse;
import org.cmdbuild.workflow.model.TaskDefinition;
import org.cmdbuild.workflow.model.WorkflowException;
import org.cmdbuild.workflow.river.dao.FlowPersistenceService;
import org.cmdbuild.workflow.river.dao.converters.TaskConversionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.logic.data.QueryOptionsImpl;
import org.cmdbuild.river.RiverVariableInfo;
import static org.cmdbuild.utils.io.CmdbuildIoUtils.readToString;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.abbreviate;
import static org.cmdbuild.utils.lang.CmdbStringUtils.classNameOrVoid;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;
import org.cmdbuild.widget.WidgetService;
import org.cmdbuild.widget.model.Widget;
import org.cmdbuild.workflow.FlowAdvanceResponse;
import org.cmdbuild.workflow.core.utils.WorkflowUtils;
import static org.cmdbuild.workflow.core.utils.WorkflowUtils.getEntryTaskForCurrentUser;
import static org.cmdbuild.workflow.core.utils.XpdlUtils.xpdlToDatasource;
import org.cmdbuild.workflow.model.XpdlInfoImpl;
import static org.cmdbuild.workflow.model.WorkflowConstants.CURRENT_GROUP_NAME_VARIABLE;
import static org.cmdbuild.workflow.model.WorkflowConstants.CURRENT_PERFORMER_VARIABLE;
import static org.cmdbuild.workflow.model.WorkflowConstants.CURRENT_USER_USERNAME_VARIABLE;
import static org.cmdbuild.workflow.model.WorkflowConstants.CURRENT_USER_VARIABLE;
import org.cmdbuild.workflow.model.XpdlInfo;
import org.cmdbuild.workflow.river.dao.ExtendedRiverPlanRepository.RiverPlanVersionInfo;
import static org.cmdbuild.workflow.river.dao.RiverPlanRepositoryImpl.ATTR_BIND_TO_CLASS;
import static org.cmdbuild.workflow.river.utils.WfRiverXpdlUtils.parseXpdlForCmdb;
import org.cmdbuild.workflow.inner.WfReference;
import org.cmdbuild.workflow.model.WfReferenceImpl;
import org.cmdbuild.workflow.model.TaskVariable;
import static org.cmdbuild.workflow.model.WorkflowConstants.PROCESS_CARD_ID_VARIABLE;
import org.cmdbuild.workflow.type.ReferenceType;
import org.cmdbuild.workflow.WorkflowTypeConverter;
import org.cmdbuild.widget.model.WidgetData;
import static org.cmdbuild.widget.utils.WidgetConst.WIDGET_ACTION_SUBMIT;
import org.cmdbuild.workflow.model.Task;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.Process;
import org.cmdbuild.workflow.river.dao.RiverFlowConversionService;
import org.cmdbuild.workflow.river.dao.converters.TaskDefinitionConversionService;
import org.cmdbuild.auth.role.Role;
import org.cmdbuild.utils.lang.CmdbMapUtils;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbStringUtils.mapToLoggableString;
import static org.cmdbuild.workflow.model.WorkflowConstants.PROCESS_CLASSNAME_VARIABLE;
import static org.cmdbuild.workflow.model.WorkflowConstants.PROCESS_INSTANCE_ID_VARIABLE;
import org.cmdbuild.auth.login.AuthenticationService;

@Component
public class WorkflowServiceRiverImpl implements WorkflowServiceDelegate {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final WorkflowConfiguration workflowConfiguration;
	private final ExtendedRiverPlanRepository planRepository;
	private final RiverFlowService flowService;
	private final RiverFlowConversionService flowRepository;
	private final FlowCardRepository cardRepository;
	private final RiverLockService lockService;
	private final TaskConversionService taskConversionService;
	private final TaskDefinitionConversionService taskDefinitionConversionService;
	private final FlowPersistenceService persistenceService;
	private final WorkflowTypeConverter typeConverter;
	private final AuthenticationService authenticationService;
	private final OperationUserSupplier userSupplier;
	private final WidgetService widgetService;

	public WorkflowServiceRiverImpl(WorkflowConfiguration workflowConfiguration, ExtendedRiverPlanRepository planRepository, RiverFlowService flowService, RiverFlowConversionService flowRepository, FlowCardRepository cardRepository, RiverLockService lockService, TaskConversionService taskConversionService, TaskDefinitionConversionService taskDefinitionConversionService, FlowPersistenceService persistenceService, WorkflowTypeConverter typeConverter, AuthenticationService authenticationService, OperationUserSupplier userSupplier, WidgetService widgetService) {
		this.workflowConfiguration = checkNotNull(workflowConfiguration);
		this.planRepository = checkNotNull(planRepository);
		this.flowService = checkNotNull(flowService);
		this.flowRepository = checkNotNull(flowRepository);
		this.cardRepository = checkNotNull(cardRepository);
		this.lockService = checkNotNull(lockService);
		this.taskConversionService = checkNotNull(taskConversionService);
		this.taskDefinitionConversionService = checkNotNull(taskDefinitionConversionService);
		this.persistenceService = checkNotNull(persistenceService);
		this.typeConverter = checkNotNull(typeConverter);
		this.authenticationService = checkNotNull(authenticationService);
		this.userSupplier = checkNotNull(userSupplier);
		this.widgetService = checkNotNull(widgetService);
	}

	@Override
	public String getName() {
		return RIVER;
	}

	@Override
	public void abortProcessInstance(Flow processInstance) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void suspendProcessInstance(Flow processInstance) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void resumeProcessInstance(Flow processInstance) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Task> getTaskListForCurrentUserByClassIdAndCardId(String classId, Long cardId) {
		logger.debug("getTaskListForCurrentUser with classId = {} cardId = {}", classId, cardId);
		Flow card = cardRepository.getFlowCardByClasseIdAndCardId(classId, cardId);
		return taskConversionService.getTaskList(card);//TODO filter for user
	}

	@Override
	public PagedElements<Task> getTaskListForCurrentUserByClassId(String classId, DaoQueryOptions queryOptions) {
		logger.debug("getTaskListForCurrentUser with classId = {}", classId);
		PagedElements<Flow> cards = cardRepository.getCardsByClassIdAndQueryOptions(classId, queryOptions); //TODO TODO: filter user
		return paged(cards.stream().map(taskConversionService::getTaskList).flatMap(List::stream).collect(toList()));
	}

	@Override
	public Task getUserTask(Flow card, String userTaskId) {
		return taskConversionService.getTask(card, userTaskId);
	}

	@Override
	public FlowAdvanceResponse startProcess(Process process, Map<String, ?> vars, boolean advance) {
		return new StartProcessOperation(process, (Map) vars, advance).startProcess();
	}

	@Override
	public FlowAdvanceResponse updateProcess(Flow flowCard, String taskId, Map<String, ?> vars, boolean advance) {
		return new UpdateProcessOperation(flowCard, taskId, (Map) vars, advance).updateProcess();
	}

	@Override
	public List<XpdlInfo> getXpdlInfosOrderByVersionDesc(String classId) {
		List<RiverPlanVersionInfo> list = planRepository.getPlanVersionsByClassIdOrderByCreationDesc(classId);
		List<XpdlInfo> res = list();
		for (int i = 0; i < list.size(); i++) {
			RiverPlanVersionInfo riverPlan = list.get(i);
			int ver = list.size() - i;
			XpdlInfo xpdlInfo = XpdlInfoImpl.builder()
					.withDefault(i == 0)
					.withLastUpdate(riverPlan.getLastUpdate())
					.withVersion(Integer.toString(ver))
					.withProvider(getName())
					.withPlanId(riverPlan.getPlanId())
					.build();
			res.add(xpdlInfo);
		}
		return res;
	}

	@Override
	public DataSource getXpdlForClasse(Process classe) {
		RiverPlan plan = planRepository.getPlanById(classe.getPlanId());
		String xpdl = plan.toXpdl();
		return xpdlToDatasource(xpdl, format("%s_%s", classe.getName(), plan.getId()));
	}

	@Override
	public XpdlInfo addXpdl(String expectedClassId, DataSource wrapAsDataSource) {
		String xpdlContent = readToString(wrapAsDataSource);
		RiverPlan riverPlan = parseXpdlForCmdb(xpdlContent);
		String xpdlClassId = riverPlan.getAttr(ATTR_BIND_TO_CLASS);
		checkArgument(equal(xpdlClassId, expectedClassId), "xpdl binding mismatch, xpdl bind to %s = %s", ATTR_BIND_TO_CLASS, xpdlClassId);
		riverPlan = planRepository.storePlan(riverPlan);
		String planId = riverPlan.getId();
		XpdlInfo xpdlInfo = getXpdlInfosOrderByVersionDesc(xpdlClassId).stream().filter((i) -> equal(i.getPlanId(), planId)).findFirst().get();
		logger.info("uploaded new xpdl version = {}", xpdlInfo);
		return xpdlInfo;
	}

	@Override
	public TaskDefinition getTaskDefinition(Flow flowCard, String taskId) {
		RiverPlan plan = planRepository.getPlanById(flowCard.getPlanId());
		RiverTask task = plan.getTask(taskId);
		return taskDefinitionConversionService.toTaskDefinition(task);
	}

	@Override
	public Map<String, Object> getFlowData(Flow flowCard) {
		RiverFlow flow = flowRepository.cardToRiverFlow(flowCard);
		return flow.getData();//TODO ensure that flow.getData contains all flow data
	}

	@Override
	public Task getTask(Flow flowCard, String taskId) {
		return getUserTask(flowCard, taskId);
	}

	@Override
	public List<Task> getTaskList(Flow flowCard) {
		return taskConversionService.getTaskList(flowCard);
	}

	private RiverFlow completeTask(RiverFlow riverFlow, Task userTask, RiverTask riverTask) {
		RiverTaskCompleted completedTask = CompletedTaskImpl.of(riverFlow, riverTask);

//		taskRepository.removeTask(userTask.getId());
		return flowService.completedTask(riverFlow, completedTask);
	}

	private RiverFlow addCurrentPerformerDataInFlow(RiverFlow riverFlow, Task userTask) {
		OperationUser operationUser = userSupplier.getUser();//TODO the following lines are duplicated in shark impl; refactor and remove duplicate code
		Map<String, Object> map = map(
				CURRENT_USER_USERNAME_VARIABLE, operationUser.getUsername(),
				CURRENT_GROUP_NAME_VARIABLE, operationUser.getDefaultGroupOrNull().getName(),
				CURRENT_USER_VARIABLE, toReference(operationUser),
				CURRENT_PERFORMER_VARIABLE, getActualTaskPerformerAsGroupReference(riverFlow, userTask));
		return RiverFlowImpl.copyOf(riverFlow)
				.withData(map(riverFlow.getData()).with(map))
				.build();
	}

	private @Nullable
	ReferenceType getActualTaskPerformerAsGroupReference(RiverFlow riverFlow, Task task) {
		String taskPerformer = task.getPerformerName();
		Role group = authenticationService.getGroupWithName(taskPerformer);
		WfReference output = WorkflowUtils.workflowReferenceFromCmGroup(group);
		if (output == null) {
			logger.warn("unable to get group reference for task performer = {} within task = {}", taskPerformer, task);
		}
		return typeConverter.rawValueToFlowValue(output, ReferenceType.class);
	}

	private ReferenceType toReference(OperationUser operationUser) {
		return typeConverter.rawValueToFlowValue(new WfReferenceImpl(operationUser.getLoginUser().getId(), "User"), ReferenceType.class);
	}

	private class StartProcessOperation extends ProcessOperation {

		private final Process process;
		private final Map<String, Object> vars;
		private final boolean advance;
		private final RiverPlan riverPlan;
		private TaskDefinition entryTask;
		private String entryTaskId;

		public StartProcessOperation(Process process, Map<String, Object> vars, boolean advance) {
			this.process = checkNotNull(process);
			this.vars = checkNotNull(vars);
			this.advance = advance;
			riverPlan = planRepository.getPlanById(process.getPlanId());
		}

		public FlowAdvanceResponse startProcess() {
			logger.info("start process for classe = {}", process);
			prepareEntryTask();
			createFlowInstance();
			try (AquiredLock lock = lockService.aquireLock(riverFlow).aquired()) {
				createCardAndUpdateFlowWithCardStuff();
				if (advance) {
					advanceProcess();
				}
				flowCard = persistenceService.updateFlowAndCard(flowCard, riverFlow);
				return buildResponse(flowCard);
			}
		}

		private void prepareEntryTask() {
			entryTask = getEntryTaskForCurrentUser(process, userSupplier.getUser());
			entryTaskId = riverPlan.getEntryPointIdByTaskId(entryTask.getId());
		}

		private void createFlowInstance() {
			riverFlow = flowService.createFlow(riverPlan);
			riverFlow = setInitialDataInFlow(riverFlow, process);
		}

		private void createCardAndUpdateFlowWithCardStuff() {
			TaskDefinition taskDefinition = WorkflowUtils.getEntryTaskForCurrentUser(process, userSupplier.getUser());
			addTaskDataFromFormInFlow(vars, taskDefinition, widgetService.widgetDataToWidget(taskDefinition.getWidgets(), vars));
			flowCard = persistenceService.createFlowCard(riverFlow);
			riverFlow = RiverFlowImpl.copyOf(riverFlow).withData(map(riverFlow.getData()).with(
					PROCESS_CARD_ID_VARIABLE, flowCard.getCardId(),
					PROCESS_CLASSNAME_VARIABLE, flowCard.getType().getName(),
					PROCESS_INSTANCE_ID_VARIABLE, riverFlow.getId()
			)).build();
			riverFlow = flowService.startFlow(riverFlow, entryTaskId);
			flowCard = persistenceService.updateFlowAndCard(flowCard, riverFlow);
		}

		private void advanceProcess() {
			logger.info("starting process with flow = {} card = {}", riverFlow, flowCard);
			try {
				doAdvanceProcess();
			} catch (Exception ex) {
				throw new WorkflowException(ex, "error starting process flow = %s card = %s", riverFlow, flowCard);
			}
		}

		private void doAdvanceProcess() {
			List<Task> taskList = taskConversionService.getTaskList(flowCard);
			checkArgument(taskList.size() == 1, "we expected exactly one task for flow = %s at this time, found = %s", riverFlow.getId(), taskList);
			Task task = getOnlyElement(taskList);
			validateTaksParameters(vars, task);
//			Map<String, Object> varsAndWidgetData = saveWidgets(task, vars, widgets);
			Map<String, Object> varsAndWidgetData = saveWidgets(task, riverFlow.getData());//TODO get data from flow, filtered for form; is this correct?
			addDataFromFormInFlow(varsAndWidgetData);
			addCurrentPerformerDataInFlowAndCompleteTask(task);
		}

		private RiverFlow setInitialDataInFlow(RiverFlow riverFlow, org.cmdbuild.workflow.model.Process classe) {
			logger.debug("set initial data in flow = {}", riverFlow);
			CmdbMapUtils.FluentMap<String, Object> data = map();
			RiverPlan plan = riverFlow.getPlan();

			plan.getGlobalVariables().forEach((String key, RiverVariableInfo var) -> {
				Object value = var.getDefaultValue().orElse(typeConverter.defaultValueForFlowInitialization(var.getJavaType()));
				value = typeConverter.rawValueToFlowValue(value, var.getJavaType());
				data.put(key, value);
			});

			if (logger.isTraceEnabled()) {
				logger.trace("initial flow data = \n\n{}\n", mapToLoggableString(data));
			}

			return RiverFlowImpl.copyOf(riverFlow)
					.withData(data)
					.build();
		}

	}

	private class UpdateProcessOperation extends ProcessOperation {

		private final String taskId, flowId;
		private final Map<String, Object> vars;
		private final boolean advance;
		private Task task;

		public UpdateProcessOperation(Flow flowCard, String taskId, Map<String, Object> vars, boolean advance) {
			this.flowCard = checkNotNull(flowCard);
			this.taskId = checkNotBlank(taskId);
			this.vars = checkNotNull(vars);
			this.advance = advance;
			flowId = flowCard.getFlowId();
		}

		public FlowAdvanceResponse updateProcess() {
			logger.info("update process for classeId = {} cardId = {}", flowCard.getType().getName(), flowCard.getCardId());
			try (AquiredLock lock = lockService.aquireLock(flowId).aquired()) {
				updateCardAndFlow();
				if (advance) {
					advanceProcess();
				}
				return buildResponse(flowCard);
			}
		}

		private void updateCardAndFlow() {//TODO fix this method
			riverFlow = flowRepository.cardToRiverFlow(flowCard);
			task = taskConversionService.getTask(flowCard, taskId);
			addTaskDataFromFormInFlow(vars, task.getDefinition(), task.getWidgets());
			//TODO update flow card with river flow data
			task = taskConversionService.getTask(flowCard, taskId);//TODO avoid this refresh, retrieve only task definition before
//			Map<String, Object> varsAndWidgetData = saveWidgets(task, vars, widgets);
			Map<String, Object> varsAndWidgetData = saveWidgets(task, riverFlow.getData());//TODO get data from flow, filtered for form; is this correct?
			addDataFromFormInFlow(varsAndWidgetData);
			flowCard = persistenceService.updateFlowAndCard(flowCard, riverFlow);
		}

		private void advanceProcess() {
			try {
				doAdvanceProcess();
			} catch (Exception ex) {
				throw new WorkflowException(ex, "error advancing process = %s for task = %s", riverFlow.getPlanId(), task.getDefinition());
			}
		}

		private void doAdvanceProcess() {
			logger.debug("advance flow, complete user task = {}", task);
			validateTaksParameters(vars, task);
			addCurrentPerformerDataInFlowAndCompleteTask(task);
			flowCard = persistenceService.updateFlowAndCard(flowCard, riverFlow);
		}

	}

	private abstract class ProcessOperation {

		protected RiverFlow riverFlow;
		protected Flow flowCard;

		protected void validateTaksParameters(Map<String, ?> vars, Task task) {
			if (workflowConfiguration.isUserTaskParametersValidationEnabled()) {
				task.getDefinition().getVariables().stream().filter(TaskVariable::isMandatory).forEach((var) -> {
					Object formValue = vars.get(var.getName()), flowValue = riverFlow.getData().get(var.getName());
					checkArgument(!isBlank(toStringOrNull(formValue)) || !isBlank(toStringOrNull(flowValue)), "missing mandatory value for var = %s task = %s", var.getName(), task.getDefinition());
				});
			}
		}

		protected void addCurrentPerformerDataInFlowAndCompleteTask(Task userTask) {
			String taskId = userTask.getDefinition().getId();
			RiverTask riverTask = riverFlow.getPlan().getTask(taskId);

			riverFlow = addCurrentPerformerDataInFlow(riverFlow, userTask);

			riverFlow = completeTask(riverFlow, userTask, riverTask);
			flowCard = cardRepository.getFlowCard(flowCard);
		}

		protected void addTaskDataFromFormInFlow(Map<String, Object> data, TaskDefinition taskDefinition, List<Widget> widgets) {
			Set<String> formVars = taskDefinition.getVariables().stream().map(TaskVariable::getName).collect(toSet());
			addDataFromFormInFlow(map(Maps.filterKeys(data, formVars::contains)).accept((m) -> {
				widgets.stream().filter(Widget::hasOutputKey).forEach((w) -> {
					Object value = data.get(w.getOutputKey());
					if (w.hasOutputType()) {
						value = typeConverter.cardValueToFlowValue(value, w.getOutputType());
					}
					m.put(w.getOutputKey(), value);
				});
			}));
		}

		protected void addDataFromFormInFlow(Map data) {

			if (logger.isTraceEnabled()) {
				logger.trace("received raw flow data from user form = \n\n{}\n", mapToLoggableString(data));
			}

			data = convertFormDataToFlow(riverFlow.getPlan(), data);

			if (logger.isTraceEnabled()) {
				logger.trace("processed form data, adding to flow = \n\n{}\n", mapToLoggableString(data));
			}

			riverFlow = RiverFlowImpl.copyOf(riverFlow)
					.withData(map(riverFlow.getData()).with(data))
					.build();
		}
	}

	private Map<String, Object> convertFormDataToFlow(RiverPlan plan, Map<String, Object> data) {
		Map<String, Object> converted = map();
		data.forEach((key, value) -> {
			RiverVariableInfo varInfo = plan.getGlobalVariables().get(key);
			if (varInfo != null) {
				try {
					value = typeConverter.rawValueToFlowValue(value, varInfo.getJavaType());
				} catch (Exception ex) {
					throw new WorkflowException(ex, "error converting form value = %s to flow var = %s", value, varInfo);
				}
			} else {
				logger.warn("received form data {} = {} ({}) not defined in flow plan global variables; skipping conversion", key, abbreviate(value), classNameOrVoid(value));
			}
			converted.put(key, value);
		});
		return converted;
	}

	private Map<String, Object> saveWidgets(Task activityInstance, Map<String, ?> vars) {
		logger.debug("save data for widgets");
		Map<String, Object> varsAndWidgetData = map(vars);
		for (WidgetData widgetData : activityInstance.getWidgets()) {
			if (widgetService.hasWidgetAction(widgetData.getType(), WIDGET_ACTION_SUBMIT)) {
//				Map<String, Object> params = checkNotNull((Map<String, Object>) allWidgetSubmission.get(widget.getId()),"missing submitted params for widget = %s",widget); TODO enable check once widget ui code is ready
//				Map<String, Object> widgetActionParams = firstNonNull((Map<String, Object>) allWidgetSubmission.get(widgetData.getId()), emptyMap());
				Widget widget = widgetService.widgetDataToWidget(widgetData, (Map<String, Object>) vars); //TODO check context
				logger.debug("save data for widget = {}", widget);
				Map<String, Object> res = widgetService.executeWidgetAction(widget, WIDGET_ACTION_SUBMIT);
				varsAndWidgetData.putAll(res);
			}
		}
		return typeConverter.widgetValuesToFlowValues(varsAndWidgetData);
	}

	//TODO refactor cose, use command bean
	private FlowAdvanceResponse buildResponse(Flow flow) {
		logger.debug("build flow response for flow = {}", flow);
		boolean isCompleted = flow.isCompleted();
		List<Task> taskList = taskConversionService.getTaskList(flow);
		AdvancedFlowStatus status;
		if (isCompleted) {
			status = AdvancedFlowStatus.COMPLETED;
		} else if (taskList.isEmpty()) {
			status = AdvancedFlowStatus.PROCESSING_SCRIPT;
		} else {
			status = AdvancedFlowStatus.WAITING_FOR_USER_TASK;
		}
		return SimpleFlowAdvanceResponse.builder()
				.withFlowCard(flow)
				.withAdvancedFlowStatus(status)
				.withTasklist(taskList)
				.build();
	}
}
