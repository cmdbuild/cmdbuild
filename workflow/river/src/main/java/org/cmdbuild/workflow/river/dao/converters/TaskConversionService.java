/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.dao.converters;

import java.util.List;
import java.util.Map;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.workflow.model.Flow;
import org.cmdbuild.workflow.model.Task;

public interface TaskConversionService {

	Task toUserTask(Flow flow, RiverTask task, String userTaskId, String taskPerformer);

//	String getProcessedAssignedGroupForTask(RiverTask task, Map<String, Object> flowData);

	List<Task> getTaskList(Flow flow);

	Task getTask(Flow card, String userTaskId);
}
