/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.dao;

import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.workflow.model.Flow;

public interface FlowPersistenceService {

	Flow updateFlowAndCard(RiverFlow flow);

	Flow updateFlowAndCard(Flow card, RiverFlow flow);
	
	Flow createFlowCard(RiverFlow flow);
}
