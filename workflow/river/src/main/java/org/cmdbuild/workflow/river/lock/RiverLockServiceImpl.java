/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.lock;

import static com.google.common.base.Preconditions.checkNotNull;
import org.cmdbuild.lock.LockService;
import org.cmdbuild.lock.LockService.LockScope;
import org.cmdbuild.river.lock.AquiredLock;
import org.cmdbuild.river.lock.LockResponse;
import org.cmdbuild.river.lock.RiverLockService;
import static org.cmdbuild.river.lock.AquiredLockImpl.aquiredLock;
import static org.cmdbuild.river.lock.NotAquiredLockResponseImpl.notAquired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RiverLockServiceImpl implements RiverLockService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LockService lockService;

	public RiverLockServiceImpl(LockService lockService) {
		this.lockService = checkNotNull(lockService);
	}

	@Override
	public LockResponse aquireLock(String flowId) {
		LockService.LockResponse response = lockService.aquireLock("flow_" + flowId, LockScope.REQUEST);
		if (response.isAquired()) {
			return aquiredLock(flowId, response.aquired().getItemId(), this);
		} else {
			return notAquired();
		}
	}

	@Override
	public void releaseLock(AquiredLock lock) {
		lockService.releaseLock(lock.getLockId());
	}

}
