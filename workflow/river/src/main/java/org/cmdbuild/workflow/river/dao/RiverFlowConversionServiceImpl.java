/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.dao;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import com.google.common.collect.Streams;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import javax.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;
import org.cmdbuild.dao.beans.CardImpl;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_TASK_DEFINITION_ID;
import static org.cmdbuild.dao.constants.SystemAttributes.FLOW_ATTR_DATA;
import org.cmdbuild.easytemplate.EasytemplateProcessor;
import org.cmdbuild.river.RiverFlow;
import org.cmdbuild.river.RiverPlan;
import org.cmdbuild.river.RiverTask;
import org.cmdbuild.river.core.RiverFlowImpl;
import org.cmdbuild.river.data.RiverPlanRepository;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_PERFORMER_TYPE_EXPR;
import static org.cmdbuild.river.xpdl.XpdlConst.TASK_PERFORMER_TYPE_ROLE;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.convert;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.mapToLoggableString;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.workflow.WorkflowTypeConverter;
import org.cmdbuild.workflow.core.model.FlowActivityImpl;
import org.cmdbuild.workflow.core.model.FlowImpl;
import org.cmdbuild.workflow.core.utils.TaskPerformerExpressionProcessorUtils;
import org.cmdbuild.workflow.model.Flow;
import static org.cmdbuild.workflow.river.dao.RiverPlanRepositoryImpl.ATTR_BIND_TO_CLASS;
import static org.cmdbuild.workflow.river.utils.FlowDataSerializerUtils.deserializeFlowData; 
import org.cmdbuild.workflow.inner.ProcessRepository;
import org.cmdbuild.workflow.model.FlowActivity;
import org.cmdbuild.workflow.model.TaskPerformer;
import static org.cmdbuild.workflow.model.TaskPerformer.newRolePerformer;
import static org.cmdbuild.workflow.model.WorkflowConstants.PROCESS_CARD_ID_VARIABLE;
import static org.cmdbuild.workflow.model.WorkflowConstants.PROCESS_CLASSNAME_VARIABLE;
import static org.cmdbuild.workflow.model.WorkflowConstants.PROCESS_INSTANCE_ID_VARIABLE;
import static org.cmdbuild.workflow.river.dao.FlowPersistenceServiceImpl.riverStatusToFlowStatus;
import static org.cmdbuild.workflow.river.utils.FlowDataSerializerUtils.serializeRiverFlowData; 

@Component
public class RiverFlowConversionServiceImpl implements RiverFlowConversionService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final EasytemplateProcessor templateResolver;
	private final WorkflowTypeConverter typeConverter;
	private final ProcessRepository classeRepository;
	private final RiverPlanRepository planRepository;

	public RiverFlowConversionServiceImpl(EasytemplateProcessor templateResolver, WorkflowTypeConverter typeConverter, ProcessRepository classeRepository, RiverPlanRepository planRepository) {
		this.templateResolver = checkNotNull(templateResolver);
		this.typeConverter = checkNotNull(typeConverter);
		this.classeRepository = checkNotNull(classeRepository);
		this.planRepository = checkNotNull(planRepository);
	}

	@Override
	public RiverFlow cardToRiverFlow(Flow flowCard) {
		RiverPlan riverPlan = planRepository.getPlanById(flowCard.getPlanId());
		return flowCardToRiverFlow(riverPlan, flowCard);
	}

	private RiverFlow flowCardToRiverFlow(RiverPlan riverPlan, Flow flowCard) {

		Map<String, Object> data = deserializeFlowData(flowCard.getString(FLOW_ATTR_DATA), riverPlan);

		Map<String, Object> dataFromCard = Streams.stream(flowCard.getRawValues()).filter((e) -> flowCard.getType().isUserAttribute(e.getKey())).collect(toMap(Map.Entry::getKey, (entry) -> {//note: we add values even if they're not in flow global variables
			String key = entry.getKey();
			Object value = entry.getValue();
			return typeConverter.cardValueToFlowValue(value, flowCard.getType().getAttribute(key));
		}));

		data = map(data).with(dataFromCard).with(
				PROCESS_CARD_ID_VARIABLE, flowCard.getCardId(),
				PROCESS_CLASSNAME_VARIABLE, flowCard.getType().getName(),
				PROCESS_INSTANCE_ID_VARIABLE, flowCard.getFlowId()
		);

		if (logger.isTraceEnabled()) {
			logger.trace("loaded flow with data = \n\n{}\n", mapToLoggableString(data));
		}

		return RiverFlowImpl.builder()
				.withFlowId(flowCard.getFlowId())
				.withPlan(riverPlan)
				.withWalkStatus(convert(data.get("RiverFlowStatus"), RiverFlow.FlowStatus.class))
				.withTasks((List) flowCard.get(ATTR_TASK_DEFINITION_ID, List.class).stream().map((taskId) -> riverPlan.getStepByTaskId((String) taskId).getTask()).collect(toList()))
				.withData(data)
				.build();
	}

	@Override
	public Flow riverFlowToCard(Flow flow, RiverFlow riverFlow) {
		String serializedFlowData = riverFlowDataToFlowCardSerializedData(riverFlow);

		Set<String> newTaskIds = riverFlow.getTaskIds();
		Set<String> oldTaskIds = flow.getFlowActivities().stream().map(FlowActivity::getDefinitionId).collect(toSet());
		List<FlowActivity> flowActivities = list(flow.getFlowActivities().stream().filter((a) -> newTaskIds.contains(a.getDefinitionId())).collect(toList()))
				.with(newTaskIds.stream().filter(not(oldTaskIds::contains)).map((taskId) -> {
					String taskPerformer = getProcessedAssignedGroupForTask(riverFlow.getPlan().getTask(taskId), riverFlow.getData());
					return new FlowActivityImpl(randomId(), taskId, taskPerformer);
				}).collect(toList()));

		logger.info("convert river flow to card: old task ids = {}, new task ids = {}", oldTaskIds, newTaskIds);

		return FlowImpl.copyOf(flow)
				.withCard(CardImpl.copyOf(flow).addAttribute(FLOW_ATTR_DATA, serializedFlowData).build())
				.withFlowStatus(riverStatusToFlowStatus(riverFlow.getStatus()))
				.withFlowActivities(flowActivities)
				.build();
	}

	private String riverFlowDataToFlowCardSerializedData(RiverFlow flow) {
		org.cmdbuild.workflow.model.Process classe = classeRepository.getPlanClasseByClassAndPlanId(flow.getPlan().getAttr(ATTR_BIND_TO_CLASS), flow.getPlan().getId());
		return serializeRiverFlowData(flow.getData(), classe, flow.getPlan(), flow.getStatus());
	}

	private String getProcessedAssignedGroupForTask(RiverTask task, Map<String, Object> flowData) {
		logger.debug("getProcessedAssignedGroupForTask task = {}", task);
		List<TaskPerformer> list = getProcessedTaskPerformersForTask(task, flowData);
		logger.debug("processing performers = {}", list);
		TaskPerformer taskPerformer = checkNotNull(getFirstNonAdminPerformerOrNull(list), "task performer not found for task = %s", task);
		return checkNotBlank(taskPerformer.getValue(), "task performer value is blank for performer = %s task = %s", taskPerformer, task);//TODO fix bug, restore strong check
	}

	@Nullable
	private TaskPerformer getFirstNonAdminPerformerOrNull(List<TaskPerformer> taskPerformers) {//TODO check this
		return taskPerformers.stream()
				.filter(not(TaskPerformer::isAdmin))
				.findFirst().orElse(null);
	}

	private List<TaskPerformer> getProcessedTaskPerformersForTask(RiverTask task, Map<String, Object> flowData) {
		if (task.isUser()) {
			logger.trace("evaluating task performers for task = {}", task);
			List<TaskPerformer> taskPerformers = getTaskPerformers(task.getPerformerValue(), task.getPerformerType(), flowData);
			logger.trace("task performers for task = {} are = {}", task, taskPerformers);
			return taskPerformers;
		} else {
			return emptyList();
		}
	}

	private List<TaskPerformer> getTaskPerformers(String value, String type, Map<String, Object> flowData) {
		switch (type) {
			case TASK_PERFORMER_TYPE_ROLE:
				return singletonList(newRolePerformer(value));
			case TASK_PERFORMER_TYPE_EXPR:
				logger.debug("evaluating task performer expression = {}", value);
				return TaskPerformerExpressionProcessorUtils.getPerformersFromExpression(templateResolver, flowData, value).stream()
						.filter(StringUtils::isNotBlank)
						.map(TaskPerformer::newRolePerformer).collect(toList());
			default:
				throw unsupported("unsupported task performer type = %s", type);
		}
	}
}
