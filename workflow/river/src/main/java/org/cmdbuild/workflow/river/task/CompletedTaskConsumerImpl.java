/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.task;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.function.Consumer;
import org.cmdbuild.river.RiverTaskCompleted;
import org.springframework.stereotype.Component;

@Component
public class CompletedTaskConsumerImpl implements CompletedTaskConsumerBridge {

	private Consumer<RiverTaskCompleted> consumer;

	@Override
	public void accept(RiverTaskCompleted completedTask) {
		checkNotNull(consumer).accept(completedTask);
	}

	@Override
	public void setConsumer(Consumer<RiverTaskCompleted> consumer) {
		this.consumer = checkNotNull(consumer);
	}

}
