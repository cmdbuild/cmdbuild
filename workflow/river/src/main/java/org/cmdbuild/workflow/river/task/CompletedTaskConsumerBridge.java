/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.task;

import java.util.function.Consumer;
import org.cmdbuild.river.RiverTaskCompleted;

public interface CompletedTaskConsumerBridge extends Consumer<RiverTaskCompleted>{

	void setConsumer(Consumer<RiverTaskCompleted> consumer);
}
