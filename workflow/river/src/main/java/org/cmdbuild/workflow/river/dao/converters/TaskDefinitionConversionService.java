/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.workflow.river.dao.converters;

import org.cmdbuild.river.RiverTask;
import org.cmdbuild.workflow.model.TaskDefinition;

public interface TaskDefinitionConversionService {

	TaskDefinition toTaskDefinition(RiverTask task);
	
}
