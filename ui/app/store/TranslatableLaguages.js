Ext.define('CMDBuildUI.store.TranslatableLanguages', {

    extend: 'Ext.data.Store',

    alias: 'store.translatable-languages',

    model: 'CMDBuildUI.model.Language',
    
    proxy: {
        type: 'baseproxy',
        url: '/languages?active=true'
    },

    pageSize: 0,
    remoteFilter: true,
    remoteSort: true
});