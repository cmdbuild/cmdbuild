Ext.define('CMDBuildUI.store.attachments.Attachments', {
    extend: 'CMDBuildUI.store.Base',

    alias: 'store.attachments',

    model: 'CMDBuildUI.model.attachments.Attachment',

    sorters: ['_created'],
    groupField: '_category',
    autoLoad: true,
    pageSize: 0 // disable pagination
});