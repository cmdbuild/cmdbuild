Ext.define('CMDBuildUI.store.processes.Processes', {
    extend: 'CMDBuildUI.store.Base',

    requires: [
        'CMDBuildUI.store.Base',
        'CMDBuildUI.model.processes.Process'
    ],

    alias: 'store.processes',

    model: 'CMDBuildUI.model.processes.Process',

    sorters: ['description'],
    pageSize: 0, // disable pagination
    autoLoad: false

});