Ext.define('CMDBuildUI.store.Reports', {
    extend: 'CMDBuildUI.store.Base',

    requires: [
        'CMDBuildUI.store.Base',
        'CMDBuildUI.model.Report'
    ],

    alias: 'store.reports',

    model: 'CMDBuildUI.model.Report',

    sorters: ['description'],
    pageSize: 0, // disable pagination
    autoLoad: false

});