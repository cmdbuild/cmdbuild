Ext.define('CMDBuildUI.store.users.Preferences', {
    extend: 'CMDBuildUI.store.Base',

    requires: [
        'CMDBuildUI.store.Base',
        'CMDBuildUI.model.users.Preference'
    ],

    alias: 'store.user.preferences',

    model: 'CMDBuildUI.model.users.Preference'

});