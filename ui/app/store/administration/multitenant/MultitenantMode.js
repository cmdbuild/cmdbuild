Ext.define('CMDBuildUI.store.administration.multitenant.MultitenantMode', {
    extend: 'Ext.data.Store',  
    requires:['CMDBuildUI.model.base.ComboItem'],
    alias: 'store.multitenant-multitenantmode',
    model: 'CMDBuildUI.model.base.ComboItem',
    autoLoad: true,
    fields: ['value', 'label'],
    proxy: {
        type: 'memory'
    },
    data: [{
        value: 'never',
        label: 'Never' // TODO: translate
    }, {
        value: 'always',
        label: 'Always' // TODO: translate
    }, {
        value: 'mixed',
        label: 'Mixed' // TODO: translate
    }],

    pageSize: 0, // disable pagination
    autoDestroy: true

});