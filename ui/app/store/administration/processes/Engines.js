Ext.define('CMDBuildUI.store.administration.processes.Engines', {
    extend: 'Ext.data.Store',  
    alias: 'store.processes-Engines',
    autoLoad: true,
    fields: ['value', 'label'],
    proxy: {
        type: 'memory'
    },
    data: [{
            'value': 'river',
            'label': 'Tecnoteca River'// TODO: translate
        }, {
            'value': 'shark',
            'label': 'Enhydra Shark' // TODO: translate
        } 
    ],

    pageSize: 0 // disable pagination

});