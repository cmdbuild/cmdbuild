Ext.define('CMDBuildUI.store.Attributes', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        
    ],

    alias: 'store.attributes',

    pageSize: 100
});