Ext.define('CMDBuildUI.store.groups.Groups', {
    extend: 'CMDBuildUI.store.Base',

    requires: [
        'CMDBuildUI.store.Base'
    ],

    alias: 'store.groups',

    model: 'CMDBuildUI.model.users.Group',
    pageSize: 0
});