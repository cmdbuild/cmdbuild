Ext.define('CMDBuildUI.store.map.Tree', {
    extend: 'Ext.data.TreeStore',

    requires: ['CMDBuildUI.model.map.navigation.Tree'],
    alias: 'store.map-tree',

    config: {
        rootVisible: false
    },
    model: 'CMDBuildUI.model.map.navigation.Tree',

    root: {
        text: CMDBuildUI.locales.Locales.gis.root,
        leaf: false,
        children: [],
        checked: true,
        expanded: true,
        _type: '_root',
        _id : '_id_root',
        id: 'root'
    }
});