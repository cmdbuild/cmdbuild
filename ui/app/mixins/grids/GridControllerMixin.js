Ext.define('CMDBuildUI.mixins.grids.GridControllerMixin', {
    mixinId: 'grids-gridcontroller-mixin',

    /**
     * Updates add button by adding handler 
     * or menu with addable sub-types.
     * 
     * @param {Ext.button.Button} button
     * @param {String} handler
     * @param {String} objectTypeName
     */
    updateAddButton: function (button, handler, objectTypeName) {
        var object = CMDBuildUI.util.helper.ModelHelper.getObjectFromName(objectTypeName);

        // disable add button if class is undefined
        if (!object) {
            button.setDisabled(true);
            return;
        }

        if (object.get("prototype")) {
            var menu = [];
            var childrens = object.getChildren(true);

            // create menu definition by adding non-prototype classes
            childrens.forEach(function (child) {
                menu.push({
                    text: child.getTranslatedDescription(),
                    iconCls: 'x-fa fa-file-text-o',
                    listeners: {
                        click: handler
                    },
                    objectTypeName: child.get("name")
                });
            });

            // sort menu by description
            Ext.Array.sort(menu, function (a, b) {
                return a.text === b.text ? 0 : (a.text < b.text ? -1 : 1);
            });

            // add menu to button
            button.setMenu(menu);
        } else {
            button.objectTypeName = objectTypeName;
            button.addListener("click", this[handler], this);
        }
    }
});