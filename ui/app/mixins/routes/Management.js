Ext.define('CMDBuildUI.mixins.routes.Management', {
    mixinId: 'managementroutes-mixin',

    /******************* PATCHES ********************/
    showPatches: function () {
        if (this.getViewModel().get('patchesNeeded')) {
            CMDBuildUI.util.Navigation.addIntoMainContainer('patches-panel');
        } else {
            this.redirectTo('login', true);
        }
    },

    /******************* LOGIN ********************/
    /**
     * Show login form
     */
    onBeforeShowLogin: function (action) {
        var me = this;
        // redirect to patch manager if needed
        if (this.getViewModel().get('patchesNeeded')) {
            this.redirectTo("patches", true);
            action.stop();
            return;
        }
        // close details window
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();

        Ext.Promise.all([
            CMDBuildUI.util.helper.Configurations.loadPublicConfs()
        ]).then(function() {
            CMDBuildUI.util.helper.SessionHelper.checkSessionValidity().then(function (token) {
                action.stop();
                var url = CMDBuildUI.util.helper.SessionHelper.getStartingUrl();
                if (url && Ext.String.startsWith(url, "administration")) {
                    me.redirectTo('administration');
                } else {
                    me.redirectTo('management');
                }
            }).otherwise(function (err) {
                action.resume();
            });
        });

    },
    showLogin: function () {
        CMDBuildUI.util.Navigation.addIntoMainContainer('login-container');
    },

    /******************* LOGOUT ********************/
    /**
     * Do logout
     */
    doLogout: function () {
        this.getViewModel().get("theSession").set("username", null); // used to refresh session status
        // set action id
        CMDBuildUI.util.Ajax.setActionId('logout');
        // delete session
        this.getViewModel().get("theSession").erase({
            success: function (record, operation) {
                // blank session token
                CMDBuildUI.util.helper.SessionHelper.setToken(null);
                CMDBuildUI.util.Utilities.redirectTo("login", true);
            }
        });
    },

    /******************* MANAGEMENT ********************/
    /**
     * Show management page
     */
    onBeforeShowManagement: function (action) {
        var me = this;
        // redirect to patch manager if needed
        if (this.getViewModel().get('patchesNeeded')) {
            this.redirectTo("patches", true);
            action.stop();
            return;
        }
        this.getViewModel().set('isAdministrationModule', false);
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();
        // remove all from main container
        var container = CMDBuildUI.util.Navigation.getMainContainer(true);
        // add load mask
        var loadmask = new Ext.LoadMask({
            target: container
        });
        loadmask.show();

        CMDBuildUI.util.helper.SessionHelper.checkSessionValidity().then(function (token) {
            Ext.Promise.all([
                CMDBuildUI.util.Stores.loadClassesStore(),
                CMDBuildUI.util.Stores.loadProcessesStore(),
                CMDBuildUI.util.Stores.loadReportsStore(),
                CMDBuildUI.util.Stores.loadDashboardsStore(),
                CMDBuildUI.util.Stores.loadViewsStore(),
                CMDBuildUI.util.Stores.loadCustomPagesStore(),
                CMDBuildUI.util.Stores.loadMenuStore(),
                CMDBuildUI.util.Stores.loadLookupTypesStore(),
                CMDBuildUI.util.Stores.loadDomainsStore(),
                CMDBuildUI.util.helper.UserPreferences.load(),
                CMDBuildUI.util.helper.Configurations.loadSystemConfs()
            ]).then(function () {
                // laod mask
                CMDBuildUI.util.MenuStoreBuilder.initialize();
                Ext.getBody().removeCls('administration');
                Ext.getBody().addCls('management');
                // resume action
                action.resume();
                // destroy load mask
                loadmask.destroy();
            });
        }, function (err) {
            action.stop();
            // redirect to login
            me.redirectTo('login', true);
            // destroy load mask
            loadmask.destroy();
        });
    },
    showManagement: function () {
        CMDBuildUI.util.Navigation.addIntoMainContainer('management-maincontainer');
        this.redirectToStartingUrl();
    },

    /******************* CARDS GRID ********************/
    /**
     * Before show cards grid
     * 
     * @param {String} className
     * @param {Object} action
     */
    onBeforeShowCardsGrid: function (className, action) {
        CMDBuildUI.util.helper.ModelHelper.getModel(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
            className
        ).then(function (model) {
            action.resume();
        }, function () {
            action.stop();
        });
    },
    /**
     * Show cards grid
     * 
     * @param {String} className
     * @param {Numeric} cardId This attribute is used when the function
     * is called dicretly from code, not from router.
     */
    showCardsGrid: function (className, cardId) {
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();
        CMDBuildUI.util.Navigation.addIntoManagemenetContainer('classes-cards-grid-container', {
            objectTypeName: className,
            maingrid: true,
            viewModel: {
                data: {
                    selectedId: cardId
                },
                stores: {
                    cards: {
                        model: CMDBuildUI.util.helper.ModelHelper.getModelName('class', className),
                        autoLoad: true,
                        type: 'classes-cards'
                    }
                }
            }
        });

        // fire global event objecttypechanged
        Ext.GlobalEvents.fireEventArgs("objecttypechanged", [className]);

        // update current context
        CMDBuildUI.util.Navigation.updateCurrentManagementContext(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
            className
        );
    },

    /******************* CARD ********************/
    /**
     * Before show card view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     * @param {Object} action
     */
    onBeforeShowCard: function (className, cardId, action) {
        if (cardId === 'new') {
            return action.stop();
        }
        var type = CMDBuildUI.util.helper.ModelHelper.objecttypes.klass;
        CMDBuildUI.util.helper.ModelHelper.getModel(type, className).then(function (model) {
            if (CMDBuildUI.util.Navigation.checkCurrentContext(type, className)) {
                action.stop();
            } else {
                action.resume();
            }
        }, function () {
            action.stop();
        });
    },
    /**
     * Show card view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCard: function (className, cardId) {
        this.showCardsGrid(className, cardId);
    },

    /******************* CARD VIEW ********************/
    /**
     * Before show card view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     * @param {Object} action
     */
    onBeforeShowCardWindow: function (className, cardId, action) {
        var me = this;
        var type = CMDBuildUI.util.helper.ModelHelper.objecttypes.klass;
        CMDBuildUI.util.helper.ModelHelper.getModel(type, className).then(function (model) {
            if (!action) {
                action = cardId;
                cardId = null;
            }
            // check consisntence of main content
            if (!CMDBuildUI.util.Navigation.checkCurrentContext(type, className)) {
                // show cards grid for className
                me.showCardsGrid(className, cardId);
            }
            // resume action
            action.resume();
        }, function () {
            action.stop();
        });
    },
    /**
     * Show card view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardView: function (className, cardId) {
        this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.view);
    },

    /**
     * Show details view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardDetails: function (className, cardId) {
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");
        if (privileges.card_tab_detail_access) {
            this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.masterdetail);
        } else {
            this.redirectTo(Ext.String.format("classes/{0}/cards/{1}"), className, cardId);
        }
    },

    /**
     * Show notes view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardNotes: function (className, cardId) {
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");
        if (privileges.card_tab_note_access) {
            this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.notes);
        } else {
            this.redirectTo(Ext.String.format("classes/{0}/cards/{1}"), className, cardId);
        }
    },

    /**
     * Show relation view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardRelations: function (className, cardId) {
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");
        if (privileges.card_tab_relation_access) {
            this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.relations);
        } else {
            this.redirectTo(Ext.String.format("classes/{0}/cards/{1}"), className, cardId);
        }
    },

    /**
     * Show history view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardHistory: function (className, cardId) {
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");
        if (privileges.card_tab_history_access) {
            this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.history);
        } else {
            this.redirectTo(Ext.String.format("classes/{0}/cards/{1}"), className, cardId);
        }
    },

    /**
     * Show emails view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardEmails: function (className, cardId) {
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");
        if (privileges.card_tab_email_access) {
            this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.emails);
        } else {
            this.redirectTo(Ext.String.format("classes/{0}/cards/{1}"), className, cardId);
        }
    },

    /**
     * Show relation view
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardAttachments: function (className, cardId) {
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");
        if (CMDBuildUI.util.helper.Configurations.get("cm_system_dms_enabled") && privileges.card_tab_attachment_access) {
            this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.attachments);
        } else {
            this.redirectTo(Ext.String.format("classes/{0}/cards/{1}"), className, cardId);
        }
    },

    /**
     * Show card edit
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardEdit: function (className, cardId) {
        this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.edit);
    },

    /**
     * Show card clone
     * 
     * @param {String} className
     * @param {Numeric} cardId
     */
    showCardClone: function (className, cardId) {
        this.showCardTabPanel(className, cardId, CMDBuildUI.view.classes.cards.TabPanel.actions.clone);
    },

    /**
     * Show card create
     * 
     * @param {String} className
     */
    showCardCreate: function (className) {
        this.showCardTabPanel(className, null, CMDBuildUI.view.classes.cards.TabPanel.actions.create);
    },

    /******************* PROCESS INSTANCES GRID ********************/
    /**
     * Before show process instances grid
     * 
     * @param {String} processName
     * @param {Object} action
     */
    onBeforeShowProcessInstancesGrid: function (processName, action) {
        CMDBuildUI.util.helper.ModelHelper.getModel(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.process,
            processName
        ).then(function (model) {
            action.resume();
        }, function () {
            Ext.Msg.alert('Error', 'Process non found!');
            action.stop();
        });
    },
    /**
     * Show process instances grid
     * 
     * @param {String} processName
     * @param {Numeric} instanceId This attribute is used when the function
     * is called dicretly from code, not from router.
     */
    showProcessInstancesGrid: function (processName, instanceId) {
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();
        CMDBuildUI.util.Navigation.addIntoManagemenetContainer('processes-instances-grid', {
            objectTypeName: processName,
            viewModel: {
                data: {
                    objectTypeName: processName
                }
            }
        });

        // fire global event objecttypechanged
        Ext.GlobalEvents.fireEventArgs("objecttypechanged", [processName]);

        // update current context
        CMDBuildUI.util.Navigation.updateCurrentManagementContext(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.process,
            processName
        );
    },

    /******************* PROCESS INSTANCE VIEW ********************/
    /**
     * Before show process instance view
     * 
     * @param {String} processName
     * @param {Number} instanceId
     * @param {String} activityId
     * @param {Object} action
     */
    onBeforeShowProcessInstanceView: function (processName, instanceId, activityId, action) {
        // abort route if instanceId is "new"
        if (instanceId === 'new') {
            action = activityId;
            return action.stop();
        }
        var me = this;
        var type = CMDBuildUI.util.helper.ModelHelper.objecttypes.process;
        CMDBuildUI.util.helper.ModelHelper.getModel(type, processName).then(function (model) {
            // check consisntence of main content
            if (!CMDBuildUI.util.Navigation.checkCurrentContext(type, processName)) {
                // show cards grid for processName
                me.showProcessInstancesGrid(processName, instanceId);
            }
            // resume action
            action.resume();
        }, function () {
            Ext.Msg.alert('Error', 'Process non found!');
            action.stop();
        });
    },
    /**
     * Show process instance view
     * 
     * @param {String} processName
     * @param {Number} instanceId
     * @param {String} activityId
     */
    showProcessInstanceView: function (processName, instanceId, activityId) {
        CMDBuildUI.util.Navigation.addIntoManagementDetailsWindow('processes-instances-tabpanel', {
            viewModel: {
                data: {
                    objectTypeName: processName,
                    objectId: instanceId,
                    activityId: activityId,
                    action: CMDBuildUI.view.processes.instances.TabPanel.actions.view
                }
            }
        });
    },

    /******************* PROCESS INSTANCE EDIT ********************/
    /**
     * Before show process instance edit
     * 
     * @param {String} processName
     * @param {Number} instanceId
     * @param {String} activityId
     * @param {Object} action
     */
    onBeforeShowProcessInstanceEdit: function (processName, instanceId, activityId, action) {
        var me = this;
        var type = CMDBuildUI.util.helper.ModelHelper.objecttypes.process;
        CMDBuildUI.util.helper.ModelHelper.getModel(type, processName).then(function (model) {
            // check consisntence of main content
            if (!CMDBuildUI.util.Navigation.checkCurrentContext(type, processName)) {
                // show cards grid for processName
                me.showProcessInstancesGrid(processName, instanceId);
            }
            // resume action
            action.resume();
        }, function () {
            Ext.Msg.alert('Error', 'Process non found!');
            action.stop();
        });
    },
    /**
     * Show process instance edit
     * 
     * @param {String} processName
     * @param {Number} instanceId
     * @param {String} activityId
     */
    showProcessInstanceEdit: function (processName, instanceId, activityId) {
        CMDBuildUI.util.Navigation.addIntoManagementDetailsWindow('processes-instances-tabpanel', {
            viewModel: {
                data: {
                    objectTypeName: processName,
                    objectId: instanceId,
                    activityId: activityId,
                    action: CMDBuildUI.view.processes.instances.TabPanel.actions.edit
                }
            }
        });
    },

    /******************* PROCESS INSTANCE CREATE ********************/
    /**
     * Before show process instance create
     * 
     * @param {String} processName
     * @param {Object} action
     */
    onBeforeShowProcessInstanceCreate: function (processName, action) {
        var me = this;
        var type = CMDBuildUI.util.helper.ModelHelper.objecttypes.process;
        CMDBuildUI.util.helper.ModelHelper.getModel(type, processName).then(function (model) {
            // check consisntence of main content
            if (!CMDBuildUI.util.Navigation.checkCurrentContext(type, processName)) {
                // show cards grid for processName
                me.showProcessInstancesGrid(processName);
            }
            // resume action
            action.resume();
        }, function () {
            Ext.Msg.alert('Error', 'Process non found!');
            action.stop();
        });
    },
    /**
     * Show process instance create
     * 
     * @param {String} processName
     * @param {Object} action
     */
    showProcessInstanceCreate: function (processName) {
        CMDBuildUI.util.Navigation.addIntoManagementDetailsWindow('processes-instances-tabpanel', {
            viewModel: {
                data: {
                    objectTypeName: processName,
                    action: CMDBuildUI.view.processes.instances.TabPanel.actions.create
                }
            }
        });
    },

    /******************* CUSTOM PAGES ********************/
    /**
     * Before show custom page
     * 
     * @param {String} pageName
     * @param {Object} action
     */
    onBeforeShowCustomPage: function (pageName, action) {
        var page = Ext.getStore("custompages.CustomPages").findRecord("name", pageName);
        if (page) {
            Ext.require(page.get("componentId"), function () {
                action.resume();
            });
        } else {
            action.stop();
        }
    },
    /**
     * Show custom page
     * 
     * @param {String} pageName
     */
    showCustomPage: function (pageName) {
        var page = Ext.getStore("custompages.CustomPages").findRecord("name", pageName);
        CMDBuildUI.util.Navigation.addIntoManagemenetContainer('panel', {
            title: page.getTranslatedDescription(),
            layout: 'fit',
            items: [{
                xtype: page.get("alias").replace("widget.", "")
            }]
        });

        // fire global event objecttypechanged
        Ext.GlobalEvents.fireEventArgs("objecttypechanged", [pageName]);

        // update current context
        CMDBuildUI.util.Navigation.updateCurrentManagementContext(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.custompage,
            pageName
        );
    },

    /******************* REPORTS ********************/
    /**
     * report id
     * 
     * @param {String} reportid
     * @param {Object} action
     */
    onBeforeShowReport: function (reportid, action) {
        action.resume();
    },
    /**
     * Show report
     * 
     * @param {String} className
     * @param {Numeric} reportId
     */

    showReport: function (reportid) {
        var extension = 0;
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();
        CMDBuildUI.util.Navigation.addIntoManagemenetContainer('reports-container', {
            objectTypeId: reportid,
            extension: extension
        });

        // fire global event objecttypechanged
        Ext.GlobalEvents.fireEventArgs("objectidchanged", [reportid]);

        // update current context
        CMDBuildUI.util.Navigation.updateCurrentManagementContext(
            'ReportInstance',
            reportid
        );
    },


    /**
     * report id
     * 
     * @param {String} reportid
     * @param {string} extension
     * @param {Object} action
     */
    onBeforeShowReportExtension: function (reportid, extension, action) {
        action.resume();
    },

    /**
     * Show report
     * 
     * @param {string} reportId
     * @param {string} extension
     */

    showReportExtension: function (reportid, extension) {
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();
        CMDBuildUI.util.Navigation.addIntoManagemenetContainer('reports-container', {
            objectTypeId: reportid,
            extension: extension
        });

        // fire global event objecttypechanged
        Ext.GlobalEvents.fireEventArgs("objectidchanged", [reportid, extension]);

        // update current context
        CMDBuildUI.util.Navigation.updateCurrentManagementContext(
            'ReportInstance',
            reportid,
            extension
        );
    },

    /******************* CUSTOM PAGES ********************/
    /**
     * Before show view
     * 
     * @param {String} viewName
     * @param {Object} action
     */
    onBeforeShowView: function (viewName, action) {
        var objectType, objectTypeName;
        var object = CMDBuildUI.util.helper.ModelHelper.getViewFromName(viewName);

        if (object && object.get("type") === CMDBuildUI.model.views.View.types.filter) {
            // define class or process type and typename
            objectTypeName = object.get("sourceClassName");
            objectType = CMDBuildUI.util.helper.ModelHelper.getObjectTypeByName(objectTypeName);
        } else {
            // define view type and typename
            objectType = CMDBuildUI.util.helper.ModelHelper.objecttypes.view;
            objectTypeName = viewName;
        }

        // get model
        CMDBuildUI.util.helper.ModelHelper.getModel(
            objectType,
            objectTypeName
        ).then(function (model) {
            action.resume();
        }, function () {
            action.stop();
        });
    },
    /**
     * Show view
     * 
     * @param {String} viewName
     */
    showView: function (viewName) {
        var xtype, config;
        var object = CMDBuildUI.util.helper.ModelHelper.getViewFromName(viewName);
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();

        if (object && object.get("type") === CMDBuildUI.model.views.View.types.filter) {
            var objectType = CMDBuildUI.util.helper.ModelHelper.getObjectTypeByName(object.get("sourceClassName"));
            switch (objectType) {
                case CMDBuildUI.util.helper.ModelHelper.objecttypes.klass:
                    xtype = 'classes-cards-grid-container';
                    config = {
                        objectTypeName: object.get("sourceClassName"),
                        maingrid: true,
                        filter: object.get("filter")
                    };
                    break;
                case CMDBuildUI.util.helper.ModelHelper.objecttypes.process:
                    xtype = 'processes-instances-grid';
                    config = {
                        objectTypeName: object.get("sourceClassName"),
                        filter: object.get("filter"),
                        viewModel: {
                            data: {
                                objectTypeName: object.get("sourceClassName")
                            }
                        }
                    };
                    break;
            }
            CMDBuildUI.util.Navigation.addIntoManagemenetContainer(xtype, config);

            // update current context
            CMDBuildUI.util.Navigation.updateCurrentManagementContext(
                objectType,
                object.get("sourceClassName")
            );
        } else {
            CMDBuildUI.util.Navigation.removeManagementDetailsWindow();
            CMDBuildUI.util.Navigation.addIntoManagemenetContainer('views-items-grid', {
                viewModel: {
                    data: {
                        objectTypeName: viewName
                    }
                }
            });

            CMDBuildUI.util.Navigation.updateCurrentManagementContext(
                CMDBuildUI.util.helper.ModelHelper.objecttypes.view,
                viewName
            );
        }

    },

    privates: {
        redirectToStartingUrl: function () {
            if (CMDBuildUI.util.helper.SessionHelper.getStartingUrl()) {
                this.redirectTo(CMDBuildUI.util.helper.SessionHelper.getStartingUrl());
                CMDBuildUI.util.helper.SessionHelper.clearStartingUrl();
            }
        },

        /**
         * Show card tab panel
         * @param {String} className 
         * @param {Number} cardId 
         * @param {String} action 
         */
        showCardTabPanel: function (className, cardId, action) {
            if (!CMDBuildUI.util.Navigation.checkCurrentManagementContextAction(action)) {
                CMDBuildUI.util.Navigation.addIntoManagementDetailsWindow('classes-cards-tabpanel', {
                    viewModel: {
                        data: {
                            objectTypeName: className,
                            objectId: cardId,
                            action: action
                        }
                    }
                });
                CMDBuildUI.util.Navigation.updateCurrentManagementContextAction(action);
            }
        }
    }
});