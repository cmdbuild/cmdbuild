Ext.define('CMDBuildUI.mixins.routes.Administration', {
    imports: ['CMDBuildUI.util.Navigation'],
    mixinId: 'administrationroutes-mixin',

    currentmaincontent: null,

    /**
     * Administration routes
     */
    /**
     * Show administration page
     */
    onBeforeShowAdministration: function (action) {
        var me = this;
        me.getViewModel().set('isAdministrationModule', true);
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();

        // remove all from main container
        var container = CMDBuildUI.util.Navigation.clearMainContainer(true);
        // add load mask
        var loadmask = new Ext.LoadMask({
            target: container
        });
        loadmask.show();
        CMDBuildUI.util.helper.SessionHelper.checkSessionValidity().then(function (token) {

            Ext.Promise.all([
                CMDBuildUI.util.helper.UserPreferences.load(),
                CMDBuildUI.util.administration.MenuStoreBuilder.initialize()
            ]).then(function () {

                Ext.getBody().removeCls('management');
                Ext.getBody().addCls('administration');
                // resume action
                action.resume();
                // destroy load mask
                loadmask.destroy();
            });

        }, function () {
            action.stop();
            me.redirectTo('login', true);
            // destroy load mask
            loadmask.destroy();
        });

    },

    /**
     * Show administration  main container
     */
    showAdministration: function () {
        CMDBuildUI.util.Navigation.addIntoMainContainer('administration-maincontainer');
        this.redirectToStartingUrl();
    },

    /**
     * Show class add
     */
    showClassAdministrationAdd: function (classType, action) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-classes-view', {
            viewModel: {
                links: {
                    theObject: {
                        reference: 'CMDBuildUI.model.classes.Class',
                        create: true
                    }
                },
                data: {
                    objectType: 'Class',
                    classType: classType,
                    title: 'Classes',
                    actions: {
                        view: false,
                        edit: false,
                        add: true
                    }
                }
            }
        });
    },

    /**
     * Show class add
     */
    showClassAdministration_empty: function (classType, action) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-classes-topbar', {
            viewModel: {}
        });
    },

    /**
     * Show class view
     */
    showClassAdministrationView: function (className, action) {

        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-classes-view', {
            viewModel: {
                links: {
                    theObject: {
                        reference: 'CMDBuildUI.model.classes.Class',
                        id: className
                    }
                },
                data: {
                    objectTypeName: className,
                    objectType: 'Class',
                    title: 'Classes',
                    actions: {
                        view: true,
                        edit: false,
                        add: false
                    }
                }
            }
        });
    },

    /**
     * Show class edit
     */
    showClassAdministrationEdit: function (className) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-classes-view', {
            viewModel: {
                links: {
                    theObject: {
                        reference: 'CMDBuildUI.model.classes.Class',
                        id: className
                    }
                },
                data: {
                    objectTypeName: className,
                    objectType: 'Class',
                    action: CMDBuildUI.view.administration.content.classes.TabPanel.formmodes.edit
                }
            }
        });
        return;
    },

    /**
     * Show class attribute edit
     */
    showClassAttributeAdministrationEdit: function (className, attributeName, attributes) {
        this.addIntoAdministrationDetailsWindow('administration-content-classes-tabitems-attributes-card-edit', {
            viewModel: {
                data: {
                    className: className,
                    attributeName: attributeName,
                    attributes: attributes
                }
            }
        });
    },
    /**
     * Show class attribute view
     */
    showClassAttributeAdministrationView: function (className, attributeName, attributes) {
        this.addIntoAdministrationDetailsWindow('administration-content-classes-tabitems-attributes-card-view', {
            viewModel: {
                data: {
                    className: className,
                    attributeName: attributeName,
                    attributes: attributes
                }
            }
        });
    },

    /**
     * Show lookup type add
     */
    showLookupTypeAdministrationAdd: function (lookupName) {

        lookupName = decodeURI(lookupName);
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-lookuptypes-view', {

            viewModel: {
                links: {
                    theLookupType: {
                        reference: 'CMDBuildUI.model.lookups.LookupType',
                        create: true
                    }
                },
                data: {
                    objectTypeName: lookupName,
                    objectType: 'Lookup',
                    action: CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.create,
                    title: 'Lookup'
                }
            }
        });
    },

    /**
     * Show lookup type empty
     */
    showLookupTypeAdministration_empty: function (lookupName) {

        lookupName = decodeURI(lookupName);
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-lookuptypes-topbar', {
            viewModel: {}
        });
    },

    /**
     * Show lookup type view
     */
    showLookupTypeAdministrationView: function (lookupName) {
        lookupName = decodeURI(lookupName);
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-lookuptypes-view', {

            viewModel: {
                links: {
                    theLookupType: {
                        reference: 'CMDBuildUI.model.lookups.LookupType',
                        id: lookupName
                    }
                },
                data: {
                    objectTypeName: lookupName,
                    objectType: 'Lookup',
                    action: CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.view,
                    title: 'Lookup'
                }
            }
        });
    },

    /**
     * show domain view
     * @param {String} domain
     */
    showDomainAdministrationView: function (domain) {
        domain = decodeURI(domain);
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-domains-view', {
            viewModel: {
                links: {
                    theDomain: {
                        reference: 'CMDBuildUI.model.domains.Domain',
                        id: domain
                    }
                },
                data: {
                    objectTypeName: domain,
                    objectType: 'Domain',
                    action: CMDBuildUI.view.administration.content.domains.TabPanel.formmodes.view,
                    title: 'Domain'
                }
            }
        });
    },

    /**
     * show domain create
     */
    showDomainAdministrationCreate: function () {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-domains-view', {
            viewModel: {
                links: {
                    theDomain: {
                        reference: 'CMDBuildUI.model.domains.Domain',
                        create: true
                    }
                },
                data: {
                    actions: {
                        view: false,
                        edit: false,
                        add: true
                    },
                    objectType: 'Domain',
                    title: 'Domain'
                }
            }
        });
    },

    /**
     * show domain empty
     */
    showDomainAdministration_empty: function () {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-domains-topbar', {
            viewModel: {}
        });
    },


    /**
     * show menu add
     */
    showMenuAdministrationAdd: function () {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-menu-view', {
            viewModel: {
                links: {
                    theMenu: {
                        reference: 'CMDBuildUI.model.menu.Menu',
                        create: true
                    }
                },
                data: {
                    actions: {
                        view: false,
                        edit: false,
                        add: true
                    },
                    action: 'ADD',
                    objectType: 'Menu',
                    title: 'Menu'
                }
            }
        });
    },
    /**
     * show menu view
     */
    showMenuAdministrationView: function (menu) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-menu-view', {
            viewModel: {
                links: {
                    theMenu: {
                        reference: 'CMDBuildUI.model.menu.Menu',
                        id: menu
                    }
                },
                data: {
                    actions: {
                        view: true,
                        edit: false,
                        add: false
                    },
                    action: 'VIEW',
                    objectType: 'Menu',
                    title: 'Menu'
                }
            }
        });
    },

    /**
     * show menu empty
     */
    showMenuAdministration_empty: function (menu) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-menus-topbar', {
            viewModel: {}

        });
    },

    showProcessAdministration_empty: function (process) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-processes-topbar', {
            viewModel: {}
        });
    },

    /**
     * Show process view
     */
    showProcessAdministrationView: function (processName, action) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-processes-view', {
            viewModel: {
                links: {
                    theProcess: {
                        reference: 'CMDBuildUI.model.processes.Process',
                        id: processName
                    }
                },
                data: {
                    objectTypeName: processName,
                    objectType: 'Process',
                    title: 'Processes',
                    actions: {
                        view: true,
                        edit: false,
                        add: false
                    }
                }
            }
        });
    },

    /**
     * show process add
     */
    showProcessesAdministrationAdd: function () {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-processes-view', {
            viewModel: {
                links: {
                    theProcess: {
                        reference: 'CMDBuildUI.model.processes.Process',
                        create: true
                    }
                },
                data: {
                    actions: {
                        view: false,
                        edit: false,
                        add: true
                    },
                    action: 'ADD',
                    objectType: 'Process',
                    title: 'Processes' // TODO: translate
                }
            }
        });
    },
    showReportAdministration_empty: function (showForm) {
        var hideForm = (showForm === 'true') ? false : true;
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-reports-view', {
            viewModel: {
                links: {
                    theReport: {
                        type: 'CMDBuildUI.model.Report',
                        create: true
                    }
                },
                data: {
                    actions: {
                        view: (hideForm) ? true : false,
                        edit: false,
                        add: (hideForm) ? false : true
                    },
                    hideForm: hideForm
                }
            }
        });
    },

    showReportAdministrationView: function (reportId) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-reports-view', {
            viewModel: {
                links: {
                    theReport: {
                        type: 'CMDBuildUI.model.Report',
                        id: reportId
                    }
                },

                data: {
                    reportId: reportId,
                    actions: {
                        view: true,
                        edit: false,
                        add: false
                    }
                }
            }
        });
    },
    showUsersAdministrationView_empty: function () {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-users-view', {
            viewModel: {
                data: {
                    isGridHidden: true
                }
            }
        });
    },
    showUsersAdministrationView: function () {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-users-view', {});
    },

    /**
     * Show settings view pages
     */
    showSetupAdministrationView: function (setupPage, action) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-setup-view', {
            viewModel: {
                data: {
                    currentPage: setupPage,
                    actions: {
                        view: true,
                        edit: false,
                        add: false
                    }
                }
            }
        });
    },

    showGroupsandpermissionsAdministrationView: function (roleId) {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-groupsandpermissions-view', {
            viewModel: {
                links: {
                    theGroup: {
                        reference: 'CMDBuildUI.model.users.Group',
                        id: roleId
                    }
                },
                data: {
                    objectType: roleId,
                    action: 'VIEW',
                    actions: {
                        view: true,
                        edit: false,
                        add: false
                    }
                }
            }
        });
    },

    showGroupsandpermissionsAdministration_empty: function () {
        CMDBuildUI.util.Navigation.addIntoMainAdministrationContent('administration-content-groupsandpermissions-view', {
            viewModel: {
                links: {
                    theGroup: {
                        reference: 'CMDBuildUI.model.users.Group',
                        create: true
                    }
                },
                data: {
                    action: 'ADD',
                    actions: {
                        view: false,
                        edit: false,
                        add: true
                    }
                }
            }
        });
    }
});