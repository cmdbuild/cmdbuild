/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('CMDBuildUI.Application', {
    extend: 'Ext.app.Application',
    requires: [
        // grid features
        'Ext.grid.feature.*',

        // validators
        'Ext.data.validator.*',

        // mixins
        'CMDBuildUI.mixins.*',

        // models
        'CMDBuildUI.model.*',

        // stores
        'CMDBuildUI.store.*',

        // helpers
        'CMDBuildUI.util.*',

        // components
        'CMDBuildUI.components.*',

        // views
        'CMDBuildUI.view.*',

        // locales
        'CMDBuildUI.locales.Locales'
    ],

    name: 'CMDBuildUI',

    stores: [
        'users.Tenants',
        'users.Preferences',
        'groups.Grants',
        'groups.Groups',
        'classes.Classes',
        'menu.Menu',
        'administration.MenuAdministration',
        'processes.Processes',
        'Reports',
        'Dashboards',
        'views.Views',
        'custompages.CustomPages',
        'domains.Domains',
        'map.ExternalLayerExtends',
        'lookups.LookupTypes',
        'bim.Projects'
    ],

    launch: function () {
        // Initialize Ajax
        CMDBuildUI.util.Ajax.init();

        // load localization
        CMDBuildUI.util.helper.SessionHelper.loadLocale();
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?', // TODO: translate
            function (choice) {
                if (choice === CMDBuildUI.locales.administration.common.actions.yes) {
                    window.location.reload();
                }
            }
        );
    }
});
