Ext.define('CMDBuildUI.model.attachments.Attachment', {
    extend: 'CMDBuildUI.model.base.Base',

    statics: {
        descriptionmodes: {
            hidden: 'hidden',
            optional: 'optional',
            mandatory: 'mandatory'
        }
    },

    fields: [{
        name: '_name',
        type: 'string'
    }, {
        name: '_author',
        type: 'string'
    }, {
        name: '_created',
        type: 'date'
    }, {
        name: '_category',
        type: 'string'
    }, {
        name: '_description',
        type: 'string'
    }, {
        name: 'version',
        type: 'string'
    }, {
        name_: '_modified',
        type: 'string'
    }, {
        name: '_file'
    }],

    proxy: {
        type: 'baseproxy'
    }
});
