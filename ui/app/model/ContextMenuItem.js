Ext.define('CMDBuildUI.model.ContextMenuItem', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [
        { name: 'label', type: 'string' , defaultValue:""},
        { name: 'type', type: 'string', defaultValue: "custom" }, //"custom"/"component"/"separator",
        { name: 'active', type: 'boolean', defaultValue: true },
        { name: 'visibility', type: 'string', defaultValue: 'all' }, //"one"/"many"/"all",
        { name: 'componentId', type: 'string' }, //only if type==component
        { name: 'script', type: 'string' }, //only if type==custom
        { name: 'config', type: 'string' }, //only if type==component
        {
            name: 'separator', type: 'boolean', calculate: function (data) {
                return data.type === 'separator';
            }
        }, {
            name: '_isComponent',
            type: 'boolean',
            defaultValue: false,            
            calculate: function (data) {
                
                return (data.type == 'component') ? true : false;
            }
        }
    ],
    proxy: {
        type: 'memory'
    }
});
