Ext.define('CMDBuildUI.model.Attribute', {
    requires: [
        'Ext.data.validator.Presence'
    ],

    extend: 'CMDBuildUI.model.base.Base',
    fields: [{
        // reference, date, decimal, lookup, string, text, iPaddress, boolean, timestamp, dateTime, double
        name: 'type',
        type: 'string',
        persist: true,
        critical: true,
        validators: [ 'presence']
    }, {
        name: 'name',
        type: 'string',
        persist: true,
        critical: true,
        validators:  [ 'presence']
    }, {
        name: 'metadata',
        type: 'auto',
        persist: true,
        critical: true
    }, {
        name: 'mode',
        type: 'string',
        persist: true,
        critical: true,
        defaultValue: 'write'
    }, {
        name: 'description',
        type: 'string',
        persist: true,
        critical: true,
        validators:  [ 'presence']
    }, {
        name: 'showInGrid',
        type: 'boolean',
        persist: true,
        critical: true,
        defaultValue: false,
        convert: function (value, record) {
            return value !== undefined ? value : record.get("displayableInList");
        }
    }, {
        name: 'showInReducedGrid',
        type: 'boolean',
        persist: true,
        critical: true,
        defaultValue: false,
        convert: function (value, record) {
            var metadata = record.get('metadata');
            var show = (metadata.cm_showInReducedGrid == 'true' || value === true) ? true : false;
            return show;
        },
        calculate: function (data) {
            return (data.metadata && data.metadata.cm_showInReducedGrid == "true") ? true : false;
        }
    }, {
        name: 'domain',
        type: 'string',
        persist: true,
        critical: true,
        defaultValue: ''
        // calculate: function (data) {
        //     
        //     return (data.metadata && data.metadata.cm_domain) ? data.metadata.cm_domain : null;
        // },
        // convert: function (value, record) {
        //     var metadata = record.get('metadata');
        //     return metadata.cm_domain;           
        // },
        // convertOnSet: false,
    }, {
        name: 'help',
        type: 'string',
        persist: false,
        critical: false,
        calculate: function (data) {
            return (data.metadata && data.metadata.cm_help) ? data.metadata.cm_help : null;
        }
    }, {
        name: 'showIf',
        type: 'string',
        persist: false,
        critical: false,
        calculate: function (data) {
            return (data.metadata && data.metadata.cm_showIf) ? data.metadata.cm_showIf : null;
        }
    }, {
        name: 'validationRules',
        type: 'string',
        persist: false,
        critical: false,
        calculate: function (data) {
            return (data.metadata && data.metadata.cm_validationRules) ? data.metadata.cm_validationRules : null;
        }
    }, {
        name: 'actionPostValidation',
        type: 'string',
        persist: false,
        critical: false,
        calculate: function (data) {
            return (data.metadata && data.metadata.cm_actionPostValidation) ? data.metadata.cm_actionPostValidation : null;
        }
    }, {
        name: 'unique',
        type: 'boolean',
        persist: true,
        critical: true
    }, {
        name: 'mandatory',
        type: 'boolean',
        persist: true,
        critical: true
    }, {
        name: 'inherited',
        type: 'boolean',
        defaultValue: null,
        persist: true,
        critical: true
    }, {
        name: 'active',
        type: 'boolean',
        persist: true,
        critical: true,
        defaultValue: true
    }, {
        name: 'index',
        type: 'integer',
        defaultValue: -1, // server change -1 to to maxValue + 1
        persist: true,
        critical: true
    }, {
        name: 'defaultValue',
        type: 'auto',
        persist: true,
        critical: true
    }, {
        name: 'group',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'writable',
        type: 'boolean',
        persist: true,
        critical: true
    }, {
        name: 'hidden',
        type: 'boolean',
        persist: true,
        critical: true
    }, {
        name: 'maxLength',
        type: 'integer',
        defaultValue: 50,
        persist: true,
        critical: true
    }, {
        name: 'precision',
        type: 'integer',
        defaultValue: 10,
        persist: true,
        critical: true
    }, {
        name: 'scale',
        type: 'integer',
        defaultValue: 2,
        persist: true,
        critical: true
    }, {
        name: 'lookupType',
        type: 'string',
        defaultValue: '',
        persist: true,
        critical: true
    }, {
        name: 'targetClass',
        type: 'string',
        defaultValue: '',
        persist: true,
        critical: true
    }],

    proxy: {
        type: 'cmdbuildattributesproxy'
    },


    /**
     * TODO: validation not work
     * @param {} options 
     */
    validate: function (options) {
        var errors = this.callParent(arguments),
            precision = this.get('precision'),
            scale = this.get('scale'),
            type = this.get('type');

        if (type === 'decimal') {
            if (scale >= precision) {
                errors.add({
                    field: 'scale',
                    message: 'Scale can\'t be greater o equal to precision'
                });
                errors.add({
                    field: 'precision',
                    message: 'Precision can\'t be greater o equal to precision'
                });
            } 
        }

        return errors;
    },
    /**
     * Get group info
     * 
     * @return {Object} Returns an object containing group `name` and `label`.
     */
    getGroupInfo: function () {
        var name = this.get("group");
        var label = this.get("_group_description_translation") || this.get("_group_description");
        if (!name) {
            name = "__NOGROUP";
            label = CMDBuildUI.locales.Locales.common.attributes.nogroup;
        }
        return {
            name: name,
            label: label
        };
    },

    /**
     * Return translated description for the attribute.
     * 
     * @return {String} The translated description if exists. Otherwise the description.
     */
    getTranslatedDescription: function () {
        return this.get("_description_translation") || this.get("description");
    }

});
