Ext.define('CMDBuildUI.model.AttributeOrder', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.validator.Presence'
    ],
    fields: [{
        name: 'attribute',
        type: 'string',
        defaultValue: '',
        validators:  [ 'presence']
    }, {
        name: 'direction',
        type: 'string',
        defaultValue: '',
        validators: {
            type: 'inclusion',
            list: ['ascending', 'descending'],
            message: CMDBuildUI.locales.Locales.administration.common.messages.ascendingordescending
        }
    }],
    proxy: {
        type: 'memory'
    }
});
