(function () {
    var ID_ATTRIBUTE = '_id';

    Ext.define('CMDBuildUI.model.base.Base', {
        extend: 'Ext.data.Model',

        statics: {
            permissions: {
                add: '_can_create',
                clone: '_can_clone',
                delete: '_can_delete',
                edit: '_can_update'
            }
        },

        requires: [
            'CMDBuildUI.proxy.BaseProxy'
        ],

        fields: [{
            name: ID_ATTRIBUTE,
            type: 'string',
            persist: false
            // critical: true // This field is allways sent to server even if it has hot changed
        }],
        idProperty: ID_ATTRIBUTE,

        /**
         * @return {Numeric|String} Record id. Can be different from the value returned by this.getId() function.
         */
        getRecordId: function() {
            throw "The function getRecordId is not defined for the model " + Ext.ClassManager.getName(this);
        },
        /**
         * @return {Numeric|String} Record type. Can be different from the value returned by this.get("_type") function.
         */
        getRecordType: function() {
            throw "The function getRecordType is not defined for the model " + Ext.ClassManager.getName(this);
        }
    });
})();
