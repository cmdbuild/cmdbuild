Ext.define('CMDBuildUI.model.views.View', {
    extend: 'CMDBuildUI.model.base.Base',

    statics: {
        types: {
            sql: 'SQL',
            filter: 'FILTER'
        }
    },

    fields: [{
        name: 'name',
        type: 'string'
    }, {
        name: 'description',
        type: 'string'
    }, {
        name: 'filter',
        type: 'string'
    }, {
        name: 'sourceClassName',
        type: 'string'
    }, {
        name: 'sourceFunction',
        type: 'string'
    }, {
        name: 'type',
        type: 'string'
    }],

    idProperty: 'name',

    proxy: {
        url: '/views/',
        type: 'baseproxy'
    },

    /**
     * Get translated description
     * @return {String}
     */
    getTranslatedDescription: function () {
        return this.get("_description_translation") || this.get("description");
    },

    /**
     * Get object for menu
     * @return {String}
     */
    getObjectTypeForMenu: function () {
        return this.get('name');
    }
});
