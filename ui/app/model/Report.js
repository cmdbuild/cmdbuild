Ext.define('CMDBuildUI.model.Report', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [{
        name: 'description',
        type: 'string'
    }],

    proxy: {
        url: '/reports/',
        type: 'baseproxy'
    },

    hasMany: [{
        name: 'attributes',
        model: 'CMDBuildUI.model.Attribute'
    }],

    /**
     * Get translated description
     * @return {String}
     */
    getTranslatedDescription: function () {
        return this.get("_description_translation") || this.get("description");
    },

    /**
     * Get object for menu
     * @return {String}
     */
    getObjectTypeForMenu: function () {
        return this.get('code');
    }

});
