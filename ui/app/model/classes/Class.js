Ext.define('CMDBuildUI.model.classes.Class', {
    extend: 'CMDBuildUI.model.base.Base',
    requires: [
        'Ext.data.validator.Format',
        'Ext.data.validator.Length',
        'Ext.data.validator.Presence'
    ],
    fields: [{
        name: 'name',
        type: 'string',
        critical: true
    }, {
        name: 'description',
        type: 'string',
        critical: true
    }, {
        name: 'parent',
        type: 'string',
        critical: true,
        defaultValue: 'Class'
    }, {
        name: 'prototype',
        type: 'boolean',
        critical: true
    }, {
        name: 'type',
        type: 'string',
        defaultValue: 'standard',
        critical: true
    }, {
        name: 'system',
        type: 'boolean'
    }, {
        name: 'attachmentTypeLookup',
        type: 'string',
        critical: true,
        persist: true
    }, {
        name: 'attachmentDescriptionMode',
        type: 'string',
        critical: true,
        persist: true
    }, {
        name: 'active',
        type: 'boolean',
        critical: true,
        defaultValue: true
    }, {
        name: 'defaultOrder',
        type: 'auto',
        critical: true
    }, {
        name: 'formTriggers',
        type: 'auto',
        critical: true
    }, {
        name: 'contextMenuItems',
        type: 'auto',
        critical: true
    }, {
        name: 'widgets',
        type: 'auto',
        critical: true
    }, {
        name: 'multitenantMode',
        type: 'string',
        critical: true,
        defaultValue: 'never' // values ca be: never, always, mixed
    }],

    hasMany: [{
        model: 'CMDBuildUI.model.domains.Domain',
        name: 'domains'
    }, {
        model: 'CMDBuildUI.model.AttributeOrder',
        name: 'defaultOrder',
        associationKey: 'defaultOrder'
    }, {
        model: 'CMDBuildUI.model.FormTrigger',
        name: 'formTriggers',
        associationKey: 'formTriggers'
    }, {
        model: 'CMDBuildUI.model.ContextMenuItem',
        name: 'contextMenuItems',
        associationKey: 'contextMenuItems'
    }, {
        model: 'CMDBuildUI.model.WidgetDefinition',
        name: 'widgets',
        associationKey: 'widgets'
    }, {
        model: 'CMDBuildUI.model.base.Filter',
        name: 'filters'
    }],

    validators: {
        name: [
            'presence',
            {
                type: 'length',
                max: 20,
                message: Ext.String.format(CMDBuildUI.locales.Locales.administration.common.messages.greaterthen, 20)
            }, {
                type: 'format',
                matcher: /^((?!_).)*$/,
                message: Ext.String.format(CMDBuildUI.locales.Locales.administration.common.messages.cantcontainchar, '_ (underscore)')
            }
        ],
        description: ['presence']
    },

    proxy: {
        url: '/classes/',
        type: 'baseproxy',
        reader: {
            type: 'json'
        }
    },

    /**
     * Get translated description
     * @return {String}
     */
    getTranslatedDescription: function () {
        return this.get("_description_translation") || this.get("description");
    },

    /**
     * Get CMDBuild hierarchy
     * @return {String[]} A list of class names
     */
    getHierarchy: function () {
        var hierarchy = [];
        var parentName = this.get("parent");
        if (parentName) {
            var parent = Ext.getStore("classes.Classes").getById(parentName);
            if (parent) {
                hierarchy = parent.getHierarchy();
            }
            hierarchy.push(this.getId());
        }
        return hierarchy;
    },

    /**
     * Get all children
     * @param {Boolean} leafs Get only leafs
     * @return {CMDBuild.model.Class[]} A list of children classes
     */
    getChildren: function (leafs) {
        var children = [];
        var store = Ext.getStore("classes.Classes");

        store.filter({
            property: "parent",
            value: this.get("name"),
            exactMatch: true
        });

        var allitems = store.getRange();
        store.clearFilter();

        allitems.forEach(function (p) {
            if (!leafs || (leafs && !p.get("prototype"))) {
                children.push(p);
            }
            children = Ext.Array.merge(children, p.getChildren(leafs));
        });

        return children;
    },

    /**
     * Get all children as tree
     * @param {Boolean} leafs Get only leafs
     * @param {Function} [itemCorrection] Manage single record
     * @return {CMDBuild.model.Class[]} A list of children classes
     */
    getChildrenAsTree: function (leafs, itemCorrection) {
        var children = [];
        var store = Ext.getStore("classes.Classes");
        store.filter({
            property: "parent",
            value: this.get("name"),
            exactMatch: true
        });

        var allitems = store.getRange();
        store.clearFilter();

        allitems.forEach(function (p) {
            if (itemCorrection) {
                p = itemCorrection(p);
            }
            var treeItem = {
                menutype: 'folder',
                objecttype: 'Class',
                enabled: p.get('enabled'),
                text: p.get("description"),
                name: p.get('name')
            };
            if ((leafs && p.get("prototype"))) {
                treeItem.leaf = false;
                treeItem.menutype = 'folder';
                treeItem.expanded = true;
                treeItem.enabled = true;
                var childrens = p.getChildrenAsTree(leafs, itemCorrection);
                treeItem.children = childrens;
            } else {
                treeItem.children = [];
                treeItem.leaf = true;
            }
            children.push(treeItem);
        });
        return children;
    },

    /**
     * Get object for menu
     * @return {String}
     */
    getObjectTypeForMenu: function () {
        return this.get('name');
    },

    /**
     * Load domains relation
     * @param {Boolean} force If `true` load the store also if it is already loaded.
     * @return {Ext.Deferred} The promise has as paramenters the domains store and a boolean field.
     */

    getDomains: function (force) {
        var deferred = new Ext.Deferred();
        var domains = this.domains();

        if (!domains.isLoaded() || force) {
            // get class hieararchy
            var hierarchy = this.getHierarchy();
            // generate filter
            var filter = {
                attribute: {
                    or: [{
                        simple: {
                            attribute: "source",
                            operator: "in",
                            value: hierarchy
                        }
                    }, {
                        simple: {
                            attribute: "destination",
                            operator: "in",
                            value: hierarchy
                        }
                    }]
                }
            };
            // set extra params
            domains.getProxy().setExtraParams({
                filter: Ext.JSON.encode(filter),
                ext: true
            });
            // load store
            domains.load({
                callback: function (records, operation, success) {
                    if (success) {
                        deferred.resolve(domains, true);
                    }
                }
            });
        } else {
            // return promise
            deferred.resolve(domains, false);
        }
        return deferred.promise;
    }
});