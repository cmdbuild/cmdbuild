Ext.define('CMDBuildUI.model.emails.Email', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [
        {
            name: 'keepSynchronization',
            type: 'boolean'
        },
        {
            name: 'account',
            type: 'string'
        },
        {
            name: 'bcc',
            type: 'string'
        },
        {
            name: 'body',
            type: 'string'
        },
        {
            name: 'cc',
            type: 'string'
        },
        {
            name: 'date',
            type: 'string'
        },
        {
            name: 'delay',
            type: 'auto'
        },
        {
            name: 'from',
            type: 'string'
        },
        {
            name: '_id',
            type: 'auto'
        },
        {
            name: 'noSubjectPrefix',
            type: 'boolean'
        },
        {
            name: 'notifyWith',
            type: 'string'
        },
        {
            name: 'promptSynchronization',
            type: 'boolean'
        },
        {
            name: 'status',
            type: 'string'
        },
        {
            name: 'subject',
            type: 'string'
        },
        {
            name: 'template',
            type: 'string'
        },
        {
            name: 'to',
            type: 'string'
        }
    ],
    proxy: {
        type: 'baseproxy'
    }
});