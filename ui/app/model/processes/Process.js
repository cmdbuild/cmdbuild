Ext.define('CMDBuildUI.model.processes.Process', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [{
        name: 'name',
        type: 'string',
        critical: true
    }, {
        name: 'description',
        type: 'string',
        critical: true
    }, {
        name: 'parent',
        type: 'string',
        critical: true
    }, {
        name: 'prototype',
        type: 'boolean',
        critical: true
    }, {
        name: 'flowStatusAttr',
        type: 'string',
        critical: true
    }, {
        name: 'engine',
        type: 'string',
        critical: true
    }, {
        name: 'messageAttr',
        type: 'string',
        critical: true
    }, {
        name: 'enableSaveButton',
        type: 'boolean',
        critical: true
    }, {
        name: 'defaultOrder',
        type: 'auto',
        critical: true
    }, {
        name: 'formTriggers',
        type: 'auto',
        critical: true
    }, {
        name: 'contextMenuItems',
        type: 'auto',
        critical: true
    }, {
        name: 'widgets',
        type: 'auto',
        critical: true
    }, {
        name: 'multitenantMode',
        type: 'string',
        critical: true,
        defaultValue: 'none' // values ca be: none, always, mixed
    }, {
        name: 'active',
        type: 'boolean',
        critical: true,
        defaultValue: true
    }, {
        name: 'attachmentDescriptionMode',
        type: 'string',
        critical: true
    }, {
        name: 'attachmentTypeLookup',
        type: 'string',
        critical: true
    }, {
        name: 'noteInline',
        type: 'boolean',
        critical: true
    }, {
        name: 'noteInlineClosed',
        type: 'boolean',
        critical: true
    }, {
        name: 'stoppableByUser',
        type: 'boolean',
        critical: true
    }],

    hasMany: [{
        model: 'CMDBuildUI.model.domains.Domain',
        name: 'domains'
    }, {
        model: 'CMDBuildUI.model.process.ProcessVersion',
        name: 'versions'
    }, {
        model: 'CMDBuildUI.model.AttributeOrder',
        name: 'defaultOrder',
        associationKey: 'defaultOrder'
    }, {
        model: 'CMDBuildUI.model.FormTrigger',
        name: 'formTriggers',
        associationKey: 'formTriggers'
    }, {
        model: 'CMDBuildUI.model.ContextMenuItem',
        name: 'contextMenuItems',
        associationKey: 'contextMenuItems'
    }, {
        model: 'CMDBuildUI.model.WidgetDefinition',
        name: 'widgets',
        associationKey: 'widgets'
    }, {
        model: 'CMDBuildUI.model.base.Filter',
        name: 'filters'
    }],

    proxy: {
        url: '/processes/',
        type: 'baseproxy'
    },

    /**
     * Get translated description
     * @return {String}
     */
    getTranslatedDescription: function () {
        return this.get("_description_translation") || this.get("description");
    },

    /**
     * Get CMDBuild hierarchy
     * @return {String[]} A list of class names
     */
    getHierarchy: function () {
        var hierarchy = [];
        if (this.get("parent")) {
            var parent = CMDBuildUI.util.helper.ModelHelper.getProcessFromName(this.get("parent"));
            if (parent) {
                hierarchy = parent.getHierarchy();
            }
            hierarchy.push(this.getId());
        }
        return hierarchy;
    },

    /**
     * Get all children
     * @param {Boolean} leafs Get only leafs
     * @return {CMDBuildUI.model.processes.Process[]} A list of children processes
     */
    getChildren: function (leafs) {
        var children = [];
        var store = Ext.getStore("processes.Processes");

        store.filter({
            property: "parent",
            value: this.get("name"),
            exactMatch: true
        });

        var allitems = store.getRange();
        store.clearFilter();

        allitems.forEach(function (p) {
            if (!leafs || (leafs && !p.get("prototype"))) {
                children.push(p);
            }
            children = Ext.Array.merge(children, p.getChildren(leafs));
        });

        return children;
    },

    /**
     * Get all children as tree
     * @param {Boolean} leafs Get only leafs
     * @return {CMDBuild.model.Process[]} A list of children processes
     */
    getChildrenAsTree: function (leafs) {
        var children = [];
        var store = Ext.getStore("processes.Processes");
        store.filter({
            property: "parent",
            value: this.get("name"),
            exactMatch: true
        });
        var allitems = store.getRange();
        store.clearFilter();

        allitems.forEach(function (p) {
            if ((leafs && p.get("prototype"))) {
                p.set('childrens', p.getChildrenAsTree(leafs));
            } else {
                p.set('childrens', []);
            }
            children.push(p);
        });
        return children;
    },

    /**
     * Get object for menu
     * @return {String}
     */
    getObjectTypeForMenu: function () {
        return this.get('name');
    },

    /**
     * Load domains relation
     * @param {Boolean} force If `true` load the store also if it is already loaded.
     * @return {Ext.Deferred} The promise has as paramenters the domains store and a boolean field.
     */
    getDomains: function (force) {
        var deferred = new Ext.Deferred();
        var domains = this.domains();

        if (!domains.isLoaded() || force) {
            // get class hieararchy
            var hierarchy = this.getHierarchy();
            // generate filter
            var filter = {
                attribute: {
                    or: [{
                        simple: {
                            attribute: "source",
                            operator: "in",
                            value: hierarchy
                        }
                    }, {
                        simple: {
                            attribute: "destination",
                            operator: "in",
                            value: hierarchy
                        }
                    }]
                }
            };
            // set extra params
            domains.getProxy().setExtraParams({
                filter: Ext.JSON.encode(filter),
                ext: true
            });
            // load store
            domains.load({
                callback: function (records, operation, success) {
                    if (success) {
                        deferred.resolve(domains, true);
                    }
                }
            });
        } else {
            // return promise
            deferred.resolve(domains, false);
        }
        return deferred.promise;
    }
});