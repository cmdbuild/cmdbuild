(function () {
    var statics = {
        bim : {
            enabled: 'cm_system_bim_enabled',
            password: 'bimPassword',
            user: 'bimEmail'
        },
        common : {
            defaultlanguage: 'cm_system_language_default',
            instancename: 'cm_system_instance_name',
            uselanguageprompt: 'cm_system_use_language_prompt'
        },
        gis : {
            enabled: 'cm_system_gis_enabled',
            geoserverEnabled: 'cm_system_gis_geoserver_enable',
            navigationTreeEnabled: 'gis_navigation_enabled'
        },
        multitenant : {
            enabled: 'cm_system_multitenant_enabled'
        },
        relgraph : {
            enabled: 'cm_system_relgraph_enabled',
            baselevel: 'cm_system_relgraph_baseLevel',
            clusteringThreshold: 'cm_system_relgraph_clusteringThreshold',
            displayLabel: 'cm_system_relgraph_displayLabel',
            edge: {
                color: 'cm_system_relgraph_edgeColor',
                tooltipEnabled: 'cm_system_relgraph_enableEdgeTooltip'
            },
            node: {
                tooltipEnabled: 'cm_system_relgraph_enableNodeTooltip',
                spriteDimension: 'cm_system_relgraph_spriteDimension',
                stepRadius: 'cm_system_relgraph_stepRadius'
            },
            viewport: {
                distance: 'cm_system_relgraph_viewPointDistance',
                height: 'cm_system_relgraph_viewPointHeight'
            }
        }
    };

    Ext.define('CMDBuildUI.model.Configuration', {
        extend: 'Ext.data.Model',

        statics: statics,
        fields: [{
            name: 'cm_system_dms_enabled',
            type: 'boolean'
        }, {
            name: 'cm_system_dms_category_lookup',
            type: 'string'
        }, {
            name: 'cm_system_workflow_enabled',
            type: 'boolean'
        }, {
            name: statics.common.instancename,
            type: 'string'
        }, {
            name: statics.common.defaultlanguage,
            type: 'string',
            defaultValue: 'en'
        }, {
            name: statics.common.uselanguageprompt,
            type: 'string'
        }, {
            name: statics.multitenant.enabled,
            type: 'boolean'
        },{
            name: statics.gis.enabled,//'cm_system_gis_enabled',
            type: 'boolean'
        }, {
            name: statics.gis.geoserverEnabled,//'cm_system_gis_geoserver_enabled',
            type: 'boolean'
        }, {
            name: statics.bim.enabled,//'cm_system_bim_enabled',
            type: 'boolean'
        }, {
            name: statics.bim.user,//'bimEmail',
            type: 'string',
            defaultValue: 'admin@tecnoteca.com'
        }, {
            name: statics.bim.password, //'bimPassword',
            type: 'string',
            defaultValue: 'admin'
        }, {
            name: statics.gis.navigationTreeEnabled,//'gis_navigation_enabled',
            type: 'boolean',
            defaultValue: true
        }, { //RELATION GRAPH
            name: statics.relgraph.enabled, //cm_system_relgraph_enabled ok 
            type: 'boolean',
            defaultValue: true
        }, {
            name: statics.relgraph.baselevel,//'cm_system_relgraph_baseLevel',
            type: 'integer',
            defaultValue: 3
        }, {
            name: statics.relgraph.clusteringThreshold,//'cm_system_relgraph_clusteringThreshold', //ok
            type: 'integer',
            defaultValue: 20
        }, {
            name: statics.relgraph.displayLabel,//'cm_system_relgraph_displayLabel',
            type: 'string', //
            defaultValue: 'none' //
        }, {
            name: statics.relgraph.edge.color,//'cm_system_relgraph_edgeColor',
            type: 'string',
            defaultValue: '#3D85C6'
        }, {
            name: statics.relgraph.edge.tooltipEnabled, //cm_system_relgraph_nodeTooltipEnabled
            type: 'boolean',
            defaultValue: true
        }, {
            name: statics.relgraph.node.tooltipEnabled,//'cm_system_relgraph_nodeTooltipEnabled',
            type: 'boolean',
            defaultValue: true
        }, {
            name: statics.relgraph.node.spriteDimension,//'cm_system_relgraph_spriteDimension',
            type: 'integer',
            defaultValue: 20
        }, {
            name: statics.relgraph.node.stepRadius,//'cm_system_relgraph_stepRadius',
            type: 'integer',
            defaultValue: 60
        }, {
            name: statics.relgraph.viewport.distance,//'cm_system_relgraph_viewPointDistance',
            type: 'integer',
            defaultValue: 50
        }, {
            name: statics.relgraph.viewport.height,//'cm_system_relgraph_viewPointHeight',
            type: 'integer',
            defaultValue: 50
        }]
    });
}());
