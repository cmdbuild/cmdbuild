Ext.define('CMDBuildUI.model.map.GeoElement', {
    extend: 'Ext.data.Model',

    fields: [{
        name: '_type',
        type: 'string'
    }, {
        name: '_id',
        type: 'auto'
    }, {
        name: 'x',  //TODO: //define type
        type: 'auto'
    }, {
        name: 'y',
        type: 'auto'    //TODO: define type
    }, {
        name: '_owner_type',
        type: 'string'    //TODO: define type
    }, {
        name: '_owner_id',
        type: 'integer'    //TODO: define type
    }]
});
