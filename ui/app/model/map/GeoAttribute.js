Ext.define('CMDBuildUI.model.map.GeoAttribute', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'name',
        type: 'string'
    }, {
        name: 'geoAttributeName',
        type: 'string'
    }, {
        name: 'owner_type',
        type: 'string'
    } ,{
        name: 'description',
        type: 'string'
    }, {
        name: 'type',
        type: 'string'
    }, {
        name: 'subtype',
        type: 'string'
    }, {
        name: 'index',
        type: 'integer'
    }, {
        name: 'zoomMin',    // Vanno bene separati i livelli dello zoom?
        type: 'integer'
    }, {
        name: 'zoomDef',
        type: 'integer'
    }, {
        name: 'zoomMax',
        type: 'integer'
    }, {
        name: 'style',      //e cosi che si specifica un oggetto //e' stato omesso il campo visibility
        type: 'auto'
    }]  //omessi i campi meta
});
