Ext.define('CMDBuildUI.model.map.navigation.Tree', {
    extend: 'Ext.data.Model',
    
    fields: [{
        name: 'Name',
        type: 'string'
    }],
    proxy: {
        type: 'memory'
    }
});
