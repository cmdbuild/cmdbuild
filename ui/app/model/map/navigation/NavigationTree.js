Ext.define('CMDBuildUI.model.map.navigation.NavigationTree', {
    extend: 'CMDBuildUI.model.base.Base',
    
    fields: [{
        name: '_id',
        type: 'string',
        mapping: '_id'
    }, {
        name: '_type',
        type: 'string',
        mapping: 'type'
    },{
        name: 'description',
        type: 'string',
        mapping: 'description'
    },{
        name: 'parentid',
        type: 'number',
        mapping: 'parentid'
    },{
        name: 'parenttype',
        type: 'string',
        mapping: 'parenttype'
    }]
    
    /* proxy: {
        type: 'baseproxy',
        url: CMDBuildUI.util.api.
    } */
});
