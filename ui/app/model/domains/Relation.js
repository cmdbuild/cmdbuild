Ext.define('CMDBuildUI.model.domains.Relation', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [{
        name: 'type',
        type: 'string',
        mapping: '_type'
    }, {
        name: 'destinationId',
        type: 'integer',
        mapping: '_destinationId'
    }, {
        name: 'destinationType',
        type: 'string',
        mapping: '_destinationType'
    }, {
        name: 'destinationDescription',
        type: 'string',
        mapping: '_destinationDescription'
    }, {
        name: 'destinationCode',
        type: 'string',
        mapping: '_destinationCode'
    }],

    /**
     * @property {proxy.type} For URL generation
     */
    proxy: {
        type: 'baseproxy'
    },
    /**
     * @return {Numeric|String} Record id. The same value returned by this.get("destinationId") function.
     */
    getRecordId: function () {
        return this.get("destinationId");
    },
    /**
     * @return {Numeric|String} Record type. The same value returned by this.get("destinationType") function.
     */
    getRecordType: function () {
        return this.get("destinationType");
    }
});
