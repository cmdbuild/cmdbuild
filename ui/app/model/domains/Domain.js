Ext.define('CMDBuildUI.model.domains.Domain', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [{
        name: 'name',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'description',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'source',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'sourceProcess',
        type: 'boolean',
        persist: true,
        critical: true
    }, {
        name: 'destination',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'destinationProcess',
        type: 'boolean',
        persist: true,
        critical: true
    }, {
        name: 'cardinality',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'descriptionDirect',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'descriptionInverse',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: "indexDirect",
        type: "number",
        persist: true,
        critical: true
    }, {
        name: "indexInverse",
        type: "number",
        persist: true,
        critical: true
    }, {
        name: 'isMasterDetail',
        type: 'boolean',
        persist: true,
        critical: true
    }, {
        name: 'descriptionMasterDetail',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'active',
        type: 'boolean',
        defaultValue: true,
        persist: true,
        critical: true
    }, {
        name: "disabledDestinationDescendants",
        type: "auto",
        defaultValue: [],
        persist: true,
        critical: true
    }, {
        name: "disabledSourceDescendants",
        type: 'auto',
        defaultValue: [],
        persist: true,
        critical: true
    }],

    proxy: {
        url: '/domains/',
        type: 'baseproxy',
        extraParams: {
            ext: true
        }
    },

    hasMany: [{
        name: 'attributes',
        model: 'CMDBuildUI.model.Attribute'
    }],

    /**
     * Get translation for Master/Detail description.
     * 
     * @return {String}
     */
    getTranslatedDescriptionMasterDetail: function () {
        return this.get("_descriptionMasterDetail_translation") || this.get("descriptionMasterDetail");
    },

    /**
     * Get translation for direct description.
     * 
     * @return {String}
     */
    getTranslatedDescriptionDirect: function () {
        return this.get("_descriptionDirect_translation") || this.get("descriptionDirect");
    },

    /**
     * Get translation for inverse description.
     * 
     * @return {String}
     */
    getTranslatedDescriptionInverse: function () {
        return this.get("_descriptionInverse_translation") || this.get("descriptionInverse");
    }


});
