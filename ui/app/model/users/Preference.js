Ext.define('CMDBuildUI.model.users.Preference', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'startingMenuItem',
        type: 'string',
        mapping: function(data) {
            return data['org.cmdbuild.ui.startingClass'];
        }
    }]
});
