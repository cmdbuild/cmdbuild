Ext.define('CMDBuildUI.model.users.Tenant', {
    extend: 'CMDBuildUI.model.base.Base',

    statics: {
        tenantmodes: {
            never: 'never',
            mixed: 'mixed',
            always: 'always'
        },
    },

    fields: [{
        name: 'description',
        type: 'string'
    }]
});