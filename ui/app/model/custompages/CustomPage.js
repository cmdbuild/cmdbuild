Ext.define('CMDBuildUI.model.custompages.CustomPage', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [{
        name: 'name',
        type: 'string'
    }, {
        name: 'description',
        type: 'string'
    }, {
        name: 'alias',
        type: 'string'
    }, {
        name: 'componentId',
        type: 'string'
    }],

    proxy: {
        url: '/custompages/',
        type: 'baseproxy'
    },

    /**
     * Get translated description
     * @return {String}
     */
    getTranslatedDescription: function () {
        return this.get("_description_translation") || this.get("description");
    },

    /**
    * Get object for menu
    * @return {String}
    */
    getObjectTypeForMenu: function () {
        return this.get('name');
    }
});
