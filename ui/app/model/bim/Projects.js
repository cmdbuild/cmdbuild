Ext.define('CMDBuildUI.model.bim.Projects', {
    extend: 'CMDBuildUI.model.base.Base',
    
    fields: [{
        name: '_id',
        type: 'number'
    }, {
        name: 'name',
        type: 'string'
    }, {
        name: 'description',
        type: 'string'
    }, {
        name: 'lastCheckin',
        type: 'string'
    }, {
        name: 'projectId',
        type: 'string'
    }, {
        name: 'active',
        type: 'boolean'
    }, {
        name: 'ownerClass',
        type: 'string'
    }, {
        name: 'ownerCard',
        type: 'string'
    }]
});