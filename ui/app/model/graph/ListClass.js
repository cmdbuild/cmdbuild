Ext.define('CMDBuildUI.model.graph.ListClass', {
    extend: 'CMDBuildUI.model.base.Base',
    alias: 'store.graph.ListClass',
    fields: [{
        name: 'classTarget', type: 'string'
    }, {
        name: 'qt', type: 'int'
    }]
})