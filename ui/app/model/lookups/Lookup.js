Ext.define('CMDBuildUI.model.lookups.Lookup', {
    extend: 'CMDBuildUI.model.base.Base',

    fields: [{
        name: '_type',
        type: 'string',
        critical: true
    }, {
        name: 'code',
        type: 'string',
        critical: true
    }, {
        name: 'description',
        type: 'string',
        critical: true
    }, {
        name: '_description_translation',
        type: 'string',
        persist: false
    }, {
        name: 'text',
        type: 'string',
        calculate: function (data) {
            return data._description_translation || data.description;
        }
    }, {
        name: 'number',
        type: 'integer',
        critical: true
    }, {
        name: 'active',
        type: 'boolean',
        defualtValue: true,
        critical: true
    }, {
        name: 'default',
        type: 'boolean',
        critical: true
    }, {
        name: 'parent_id',
        type: 'integer',
        critical: true
    }, {
        name: 'parent_description',
        type: 'string',
        critical: true
    }, {
        name: 'parent_type',
        type: 'string',
        critical: true
    }, {
        name: 'note',
        type: 'string',
        critical: true
    }, {
        name: 'icon_type',
        type: 'string',
        defaultValue: 'none',
        critical: true
    }, {
        name: 'icon_image',
        type: 'string',
        critical: true
    }, {
        name: 'icon_font',
        type: 'string',
        critical: true
    }, {
        name: 'icon_color',
        type: 'string',
        defaultValue: '#30373D',
        critical: true
    }, {
        name: 'text_color',
        type: 'string',
        defaultValue: '#30373D',
        critical: true
    }, {
        name: 'index',
        type: 'number',
        critical: true
    }],

    proxy: {
        type: 'baseproxy'
    },

    /**
     * Get translated description
     * @return {String}
     */
    getTranslatedDescription: function () {
        return this.get("_description_translation") || this.get("description");
    }
});
