Ext.define('CMDBuildUI.model.lookups.LookupType', {
    imports:[
        'CMDBuildUI.util.Utilities',
        'CMDBuildUI.util.api.Lookups'
    ],
    extend: 'CMDBuildUI.model.base.Base',

    fields: [{
        name: '_id',
        type: 'string',
        persist: true,
        critical: true,
        convert: function(data){
            return CMDBuildUI.util.Utilities.stringToHex(data);
        }
    },{
        name: 'name',
        type: 'string',
        persist: true,
        critical: true
    }, {
        name: 'description',
        type: 'string',
        calculate: function (data) {
            return data.name;
        }
    }, {
        name: 'parent',
        type: 'string',
        defaultValue: null
    }],

    proxy: {
        url: CMDBuildUI.util.api.Lookups.getLookupTypes(),
        type: 'baseproxy'
    }
});
