Ext.define('CMDBuildUI.locales.pt_BR.LocalesAdministration', {
    "singleton": true,
    "localization": "pt_BR",
    "administration": {
        "attributes": {
            "attribute": "<em>Attribute</em>",
            "attributes": "Atributos",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Ativo",
                "description": "Descrição",
                "domain": "Domínio",
                "editortype": "Tipo do Editor",
                "filter": "Filtro",
                "group": "Grupo",
                "help": "<em>Help</em>",
                "includeinherited": "Herança incluida",
                "iptype": "<em>IP type</em>",
                "lookup": "Pesquisa",
                "mandatory": "Obrigatório",
                "maxlength": "<em>Max Length</em>",
                "mode": "<em>Mode</em>",
                "name": "Nome",
                "precision": "Precisão",
                "preselectifunique": "<em>Preselect if unique</em>",
                "scale": "Escala",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tipo",
                "unique": "Único",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Qualquer",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Editável",
                "editorhtml": "Html",
                "hidden": "Oculto",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Texto sem formatação",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Somente leitura",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Ativo",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Cancelar",
                "description": "Descrição",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Editar metadata",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obrigatório",
                "name": "Nome",
                "save": "Salvar",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tipo",
                "unique": "Único",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "<em>Delete</em>",
                "disableattribute": "<em>Disable</em>",
                "editattribute": "<em>Edit</em>",
                "enableattribute": "<em>Enable</em>",
                "openattribute": "Abrir",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "<em>Delete</em>"
                                },
                                "edit": {
                                    "tooltip": "<em>Edit</em>"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "<em>All</em>"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "<em>Current</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "<em>Status</em>",
                                    "values": {
                                        "active": {
                                            "label": "<em>Status</em>"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "<em>Delete</em>"
                                },
                                "editTrigger": {
                                    "tooltip": "<em>Edit</em>"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "<em>Status</em>"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "classType": {
                                    "label": "Tipo"
                                },
                                "description": {
                                    "label": "Descrição"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Pai"
                                },
                                "superclass": {
                                    "label": "Superclasse"
                                }
                            }
                        },
                        "icon": "Ícone",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "<em>Status</em>"
                    },
                    "values": {
                        "active": "Ativo"
                    }
                },
                "title": "Propriedades",
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "Alterar classe"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "printBtn": {
                        "printAsOdt": "<em>OpenOffice Odt</em>",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Imprimir classe"
                    },
                    "saveBtn": "Salvar"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "<em>Classes</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "Adicione classe"
                },
                "classLabel": "<em>Class</em>",
                "printSchemaBtn": {
                    "text": "Imprimir Schema"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "<em>All</em>",
                "cancel": "Cancelar",
                "clone": "Clonar",
                "close": "Fechar",
                "create": "<em>Create</em>",
                "delete": "<em>Delete</em>",
                "disable": "<em>Disable</em>",
                "edit": "<em>Edit</em>",
                "enable": "<em>Enable</em>",
                "no": "<em>No</em>",
                "print": "Imprimir",
                "save": "Salvar",
                "update": "Atualizar",
                "yes": "<em>Yes</em>"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Atenção",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Erro",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Carregando...",
                "saving": "<em>Saving...</em>",
                "success": "Successo",
                "warning": "Aviso",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "<em>Edit</em>"
            }
        },
        "domains": {
            "domain": "Domínio",
            "fieldlabels": {
                "destination": "Destino",
                "enabled": "Habilitado",
                "origin": "Origem"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Descrição"
                                },
                                "descriptionDirect": {
                                    "label": "Descrição direta"
                                },
                                "descriptionInverse": {
                                    "label": "Descrição inversa"
                                },
                                "name": {
                                    "label": "Nome"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "saveBtn": "Salvar"
                }
            },
            "singularTitle": "Domínio",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Propriedades"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Adicionar domínio"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Ativo",
                "attachments": "Anexos",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Descrição",
                "detail": "Detalhe",
                "email": "<em>E-mail</em>",
                "exportcsv": "Exportar Arquivo CSV",
                "filters": "<em>Filters</em>",
                "history": "Histórico",
                "importcsvfile": "Importar Arquivo CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Nome",
                "note": "Nota",
                "relations": "Relacionamentos",
                "type": "Tipo",
                "username": "<em>Username</em>"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Administrador limitado",
                "normal": "Normal",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "<em>Default filters</em>",
                "defaultread": "<em>Def. + R.</em>",
                "description": "Descrição",
                "filters": "<em>Filters</em>",
                "group": "Grupo",
                "name": "Nome",
                "none": "Nenhum",
                "permissions": "Permissões",
                "read": "Leitura",
                "uiconfig": "Configuração UI",
                "userslist": "<em>Users list</em>",
                "write": "Escrita"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Pai"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Ativo"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "saveBtn": "Salvar"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "saveBtn": "Salvar"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menu",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Propriedades do Tipo"
            }
        },
        "navigation": {
            "bim": "<em>BIM</em>",
            "classes": "<em>Classes</em>",
            "custompages": "<em>Custom pages</em>",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domínios",
            "email": "<em>E-mail</em>",
            "generaloptions": "<em>General options</em>",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "<em>Languages</em>",
            "lookuptypes": "Tipo Pesquisa",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Processos",
            "reports": "<em>Reports</em>",
            "searchfilters": "Filtros de procura",
            "servermanagement": "Gerenciamento do Servidor",
            "simples": "<em>Simples</em>",
            "standard": "Padrão",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "<em>Task manager</em>",
            "title": "Navegação",
            "users": "Usuários",
            "views": "Visualizações",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "description": {
                                    "label": "Descrição"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Herda de"
                                },
                                "stoppableByUser": {
                                    "label": "Usúario pode parar"
                                },
                                "superclass": {
                                    "label": "Superclasse"
                                }
                            }
                        },
                        "icon": "Ícone",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "<em>Status</em>"
                    },
                    "values": {
                        "active": "Ativo"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "saveBtn": "Salvar",
                    "versionBtn": {
                        "tooltip": "Versão"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Adicionar processo"
                },
                "printSchemaBtn": {
                    "text": "Imprimir Schema"
                },
                "processLabel": "<em>Workflow</em>",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "description": {
                                    "label": "Descrição"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "stoppableByUser": {
                                    "label": "Usúario pode parar"
                                }
                            }
                        },
                        "icon": "Ícone"
                    }
                }
            },
            "title": "Usuários",
            "toolbar": {
                "addUserBtn": {
                    "text": "Adicionar usuário"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});