Ext.define('CMDBuildUI.locales.pt_BR.Locales', {
    "requires": ["CMDBuildUI.locales.pt_BR.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "pt_BR",
    "administration": CMDBuildUI.locales.pt_BR.LocalesAdministration.administration,
    "attachments": {
        "add": "Adicionar anexo",
        "author": "Autor",
        "category": "<em>Category</em>",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Descrição",
        "download": "<em>Download</em>",
        "editattachment": "<em>Modifica allegato</em>",
        "file": "Arquivo",
        "filename": "<em>File name</em>",
        "majorversion": "<em>Major version</em>",
        "modificationdate": "Data de modificação",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Versão",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Visibilidade"
        },
        "menu": {
            "camera": "<em>Camera</em>",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "<em>Pan</em>",
            "resetView": "<em>Reset View</em>",
            "rotate": "<em>Rotate</em>",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Adicionar cartão",
            "clone": "Clonar",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Remover cartão",
            "modifycard": "Modificar cartão",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Adicionar",
            "apply": "Aplicar",
            "cancel": "Cancelar",
            "close": "Fechar",
            "delete": "<em>Delete</em>",
            "edit": "<em>Edit</em>",
            "execute": "<em>Execute</em>",
            "remove": "Remover",
            "save": "Salvar",
            "saveandapply": "Salvar e Aplicar",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Atividade",
            "attachments": "Anexos",
            "card": "Cartão",
            "details": "Detalhes",
            "emails": "<em>Emails</em>",
            "history": "Histórico",
            "notes": "Notas",
            "relations": "Relacionamentos"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Adicionar Filtro",
        "any": "Qualquer",
        "attribute": "Escolha um atributo",
        "attributes": "Atributos",
        "clearfilter": "<em>Clear Filter</em>",
        "clone": "Clonar",
        "copyof": "Copia de",
        "description": "Descrição",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Seleção Origem",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Nome",
        "newfilter": "<em>New filter</em>",
        "noone": "Nenhum",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Inicia com",
            "between": "Entre",
            "contained": "<em>Contained</em>",
            "containedorequal": "<em>Contained or equal</em>",
            "contains": "Contém",
            "containsorequal": "<em>Contains or equal</em>",
            "different": "Diferente",
            "doesnotbeginwith": "Não inicia com",
            "doesnotcontain": "Não contém",
            "doesnotendwith": "Não finaliza com",
            "endswith": "Finaliza com",
            "equals": "É igual a",
            "greaterthan": "Maior do que",
            "isnotnull": "Não é nulo",
            "isnull": "É nulo",
            "lessthan": "Menor do que"
        },
        "relations": "Relacionamentos",
        "type": "Tipo",
        "typeinput": "<em>Input Parameter</em>",
        "value": "<em>Value</em>"
    },
    "gis": {
        "card": "Cartão",
        "externalServices": "Servicos Externos",
        "geographicalAttributes": "Atributos geográficos",
        "geoserverLayers": "Camadas Geoserver",
        "layers": "<em>Layers</em>",
        "list": "Lista",
        "mapServices": "<em>Map Services</em>",
        "root": "<em>Root</em>",
        "tree": "<em>Navigation tree</em>",
        "view": "<em>View</em>"
    },
    "history": {
        "begindate": "Data de Início",
        "enddate": "Data de Términio",
        "user": "Usuário"
    },
    "login": {
        "buttons": {
            "login": "Login",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Idioma",
            "password": "<em>Password</em>",
            "tenants": "<em>Tenants</em>",
            "username": "<em>Username</em>"
        },
        "title": "Login"
    },
    "main": {
        "administrationmodule": "Módulo de Administração",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Sair",
        "managementmodule": "Módulo de Gerenciamento de Dados",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "<em>Classes</em>",
        "custompages": "<em>Custom pages</em>",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Processos",
        "reports": "<em>Reports</em>",
        "views": "Visualizações"
    },
    "notes": {
        "edit": "Editar nota"
    },
    "notifier": {
        "error": "Erro",
        "genericerror": "<em>Generic error</em>",
        "info": "Info",
        "success": "Successo",
        "warning": "Aviso"
    },
    "processes": {
        "action": {
            "advance": "Avançar",
            "label": "<em>Action</em>"
        },
        "startworkflow": "<em>Start</em>",
        "workflow": "<em>Workflow</em>"
    },
    "relationGraph": {
        "card": "Cartão",
        "cardList": "<em>Card List</em>",
        "cardRelation": "<em>Relation</em>",
        "class": "<em>Class</em>",
        "class:": "<em>Class</em>",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Abrir gráfico de relacionamento",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "<em>Relation</em>",
        "relationGraph": "Gráfico de relacionamento"
    },
    "relations": {
        "adddetail": "Adiconar detalhe",
        "addrelations": "Adicionar relacionamentos",
        "attributes": "Atributos",
        "code": "Código",
        "deletedetail": "Remover detalhe",
        "deleterelation": "Remover relacionamento",
        "description": "Descrição",
        "editcard": "Modificar cartão",
        "editdetail": "Editar Detalhe",
        "editrelation": "Editar relacionamento",
        "mditems": "<em>items</em>",
        "opencard": "Abrir cartão relacionado",
        "opendetail": "Visualizar detalhe",
        "type": "Tipo"
    },
    "widgets": {
        "customform": {
            "addrow": "<em>Add row</em>",
            "clonerow": "<em>Clone row</em>",
            "deleterow": "<em>Delete row</em>",
            "editrow": "<em>Edit row</em>",
            "export": "Exportar",
            "import": "<em>Import</em>",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "<em>Apply default selection</em>",
            "togglefilterdisabled": "<em>Disable grid filter</em>",
            "togglefilterenabled": "<em>Enable grid filter</em>"
        }
    }
});