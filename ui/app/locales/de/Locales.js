Ext.define('CMDBuildUI.locales.de.Locales', {
    "requires": ["CMDBuildUI.locales.de.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "de",
    "administration": CMDBuildUI.locales.de.LocalesAdministration.administration,
    "attachments": {
        "add": "Anlage hinzufügen",
        "author": "Autor",
        "category": "Kategorie",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Beschreibung",
        "download": "Herunterladen",
        "editattachment": "Anlage bearbeiten",
        "file": "Datei",
        "filename": "File-Name",
        "majorversion": "Hauptversion",
        "modificationdate": "Änderungsdatum",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Version",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Alle verstecken",
                "showAll": "Alle zeigen"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Sichtbarkeit"
        },
        "menu": {
            "camera": "Kamera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Schwenken",
            "resetView": "<em>Reset View</em>",
            "rotate": "Rotieren",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Karte hinzufügen",
            "clone": "Klonen",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Karte löschen",
            "modifycard": "Karte bearbeiten",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Hinzufügen",
            "apply": "Anwenden",
            "cancel": "Abbrechen",
            "close": "Schließen",
            "delete": "Löschen",
            "edit": "Bearbeiten",
            "execute": "<em>Execute</em>",
            "remove": "Löschen",
            "save": "Bestätigen",
            "saveandapply": "Speichern und Anwenden",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Aktivität",
            "attachments": "Anlagen",
            "card": "Karte",
            "details": "Details",
            "emails": "<em>Emails</em>",
            "history": "Chronologie",
            "notes": "Notizen",
            "relations": "Beziehungen"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Filter hinzufügen",
        "any": "Irgendein",
        "attribute": "Ein Attribut auswählen",
        "attributes": "Attribute",
        "clearfilter": "Filter leeren",
        "clone": "Klonen",
        "copyof": "Kopien von",
        "description": "Beschreibung",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Aus der Auswahl",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Name",
        "newfilter": "<em>New filter</em>",
        "noone": "Keine",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Starten mit",
            "between": "Zwischen",
            "contained": "Enthalten",
            "containedorequal": "Enthalten oder gleich",
            "contains": "Enthält",
            "containsorequal": "Enthält oder gleich",
            "different": "Verschieden",
            "doesnotbeginwith": "Es startet nicht mit",
            "doesnotcontain": "Es enthält nicht",
            "doesnotendwith": "Es endet nicht mit",
            "endswith": "Es endet mit",
            "equals": "Gleich",
            "greaterthan": "Höher",
            "isnotnull": "Not null",
            "isnull": "Null",
            "lessthan": "Niedriger"
        },
        "relations": "Beziehungen",
        "type": "Typ",
        "typeinput": "Input Parameter",
        "value": "Wert"
    },
    "gis": {
        "card": "Karte",
        "externalServices": "Externe Dienste",
        "geographicalAttributes": "Geographische Attribute",
        "geoserverLayers": "Ebenen von Geoserver",
        "layers": "Ebenen",
        "list": "Liste",
        "mapServices": "<em>Map Services</em>",
        "root": "Quelle",
        "tree": "Baumansicht",
        "view": "Ansichten"
    },
    "history": {
        "begindate": "Anfangsdatum",
        "enddate": "Enddatum",
        "user": "Benutzer"
    },
    "login": {
        "buttons": {
            "login": "Anmelden",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Sprache",
            "password": "Passwort",
            "tenants": "<em>Tenants</em>",
            "username": "Benutzername"
        },
        "title": "Anmelden"
    },
    "main": {
        "administrationmodule": "Verwaltungsformular",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Abmeldung",
        "managementmodule": "Datenmanagement-Modul",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Klassen",
        "custompages": "Maßseiten",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Vorgänge",
        "reports": "<em>Reports</em>",
        "views": "Sicht"
    },
    "notes": {
        "edit": "Notizen öffnen"
    },
    "notifier": {
        "error": "Fehler",
        "genericerror": "<em>Generic error</em>",
        "info": "Information",
        "success": "Erfolg",
        "warning": "Achtung"
    },
    "processes": {
        "action": {
            "advance": "Weiter",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Anlassen",
        "workflow": "Prozesse"
    },
    "relationGraph": {
        "card": "Karte",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Beziehung",
        "class": "<em>Class</em>",
        "class:": "Klasse",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Beziehungsgraph öffnen",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Beziehung",
        "relationGraph": "Beziehungsgraph"
    },
    "relations": {
        "adddetail": "Detail hinzufügen",
        "addrelations": "Beziehungen hinzufügen",
        "attributes": "Attribute",
        "code": "Code",
        "deletedetail": "Detail löschen",
        "deleterelation": "Beziehung löschen",
        "description": "Beschreibung",
        "editcard": "Karte bearbeiten",
        "editdetail": "Detail bearbeiten",
        "editrelation": "Beziehung bearbeiten",
        "mditems": "<em>items</em>",
        "opencard": "Verbundene Karte öffnen",
        "opendetail": "Ansicht Detail",
        "type": "Typ"
    },
    "widgets": {
        "customform": {
            "addrow": "Zeile hinzufügen",
            "clonerow": "Zeile klonen",
            "deleterow": "Zeile löschen",
            "editrow": "Zeile verändern",
            "export": "Exportieren",
            "import": "Importieren",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Default Auswahl einsetzen",
            "togglefilterdisabled": "Filter des Gitters deaktivieren",
            "togglefilterenabled": "Filter Gitter aktivieren"
        }
    }
});