Ext.define('CMDBuildUI.locales.de.LocalesAdministration', {
    "singleton": true,
    "localization": "de",
    "administration": {
        "attributes": {
            "attribute": "Attribut",
            "attributes": "Attribute",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktiv",
                "description": "Beschreibung",
                "domain": "Domain",
                "editortype": "Typ-Editor",
                "filter": "Filter",
                "group": "Gruppe",
                "help": "<em>Help</em>",
                "includeinherited": "Geerbte enthalten",
                "iptype": "IP Typ",
                "lookup": "Lookup",
                "mandatory": "Verbindlich",
                "maxlength": "<em>Max Length</em>",
                "mode": "Modus",
                "name": "Name",
                "precision": "Genauigkeit",
                "preselectifunique": "Wenn einzig vorwählen",
                "scale": "Skala",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Typ",
                "unique": "Eindeutig",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Irgendein",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Veränderbar",
                "editorhtml": "Editor HTML",
                "hidden": "Versteckt",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Klartext",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Schreibgeschützt",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Aktiv",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Abbrechen",
                "description": "Beschreibung",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Metadaten bearbeiten",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Verbindlich",
                "name": "Name",
                "save": "Bestätigen",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Typ",
                "unique": "Eindeutig",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Löschen",
                "disableattribute": "Ausschalten",
                "editattribute": "Bearbeiten",
                "enableattribute": "Aktivieren",
                "openattribute": "Aktiv",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Löschen"
                                },
                                "edit": {
                                    "tooltip": "Bearbeiten"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Alle"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Derzeitig"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Status",
                                    "values": {
                                        "active": {
                                            "label": "Status"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Löschen"
                                },
                                "editTrigger": {
                                    "tooltip": "Bearbeiten"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Status"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiv"
                                },
                                "classType": {
                                    "label": "Typ"
                                },
                                "description": {
                                    "label": "Beschreibung"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "parent": {
                                    "label": "Superklasse"
                                },
                                "superclass": {
                                    "label": "Superklasse"
                                }
                            }
                        },
                        "icon": "Icon",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktiv"
                    }
                },
                "title": "Eigenschaften",
                "toolbar": {
                    "cancelBtn": "Abbrechen",
                    "closeBtn": "Schließen",
                    "deleteBtn": {
                        "tooltip": "Löschen"
                    },
                    "disableBtn": {
                        "tooltip": "Ausschalten"
                    },
                    "editBtn": {
                        "tooltip": "Klasse bearbeiten"
                    },
                    "enableBtn": {
                        "tooltip": "Aktivieren"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Klasse ausdrucken"
                    },
                    "saveBtn": "Bestätigen"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Klassen",
            "toolbar": {
                "addClassBtn": {
                    "text": "Klasse hinzufügen"
                },
                "classLabel": "Klasse",
                "printSchemaBtn": {
                    "text": "Schema ausdrucken"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Alle",
                "cancel": "Abbrechen",
                "clone": "Klonen",
                "close": "Schließen",
                "create": "Erstellen",
                "delete": "Löschen",
                "disable": "Ausschalten",
                "edit": "Bearbeiten",
                "enable": "Aktivieren",
                "no": "Nein",
                "print": "Ausdrucken",
                "save": "Bestätigen",
                "update": "Auffrischen",
                "yes": "Ja"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Achtung",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Fehler",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Wird geladen...",
                "saving": "<em>Saving...</em>",
                "success": "Erfolg",
                "warning": "Achtung",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Bearbeiten"
            }
        },
        "domains": {
            "domain": "Domain",
            "fieldlabels": {
                "destination": "Ziel",
                "enabled": "Freigegeben",
                "origin": "Ursprung"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Beschreibung"
                                },
                                "descriptionDirect": {
                                    "label": "Direkte Beschreibung"
                                },
                                "descriptionInverse": {
                                    "label": "Umgekehrte Beschreibung"
                                },
                                "name": {
                                    "label": "Name"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Abbrechen",
                    "deleteBtn": {
                        "tooltip": "Löschen"
                    },
                    "disableBtn": {
                        "tooltip": "Ausschalten"
                    },
                    "editBtn": {
                        "tooltip": "Bearbeiten"
                    },
                    "enableBtn": {
                        "tooltip": "Aktivieren"
                    },
                    "saveBtn": "Bestätigen"
                }
            },
            "singularTitle": "Domain",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Eigenschaften"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Domain hinzufügen"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Aktiv",
                "attachments": "Anlagen",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Beschreibung",
                "detail": "Details",
                "email": "E-mail",
                "exportcsv": "CSV Dateien exportieren",
                "filters": "<em>Filters</em>",
                "history": "Chronologie",
                "importcsvfile": "CSV Dateien importieren",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Name",
                "note": "Notizen",
                "relations": "Beziehungen",
                "type": "Typ",
                "username": "Benutzername"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Begrenzter administrator",
                "normal": "Normal",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Defaultfilter",
                "defaultread": "Def. + R.",
                "description": "Beschreibung",
                "filters": "<em>Filters</em>",
                "group": "Gruppe",
                "name": "Name",
                "none": "Kein",
                "permissions": "Erlaubnis",
                "read": "Lesen",
                "uiconfig": "UI konfiguration",
                "userslist": "<em>Users list</em>",
                "write": "Schreiben"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiv"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "parent": {
                                    "label": "Superklasse"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Aktiv"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Abbrechen",
                    "closeBtn": "Schließen",
                    "deleteBtn": {
                        "tooltip": "Löschen"
                    },
                    "editBtn": {
                        "tooltip": "Bearbeiten"
                    },
                    "saveBtn": "Bestätigen"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Abbrechen",
                    "closeBtn": "Schließen",
                    "deleteBtn": {
                        "tooltip": "Löschen"
                    },
                    "editBtn": {
                        "tooltip": "Bearbeiten"
                    },
                    "saveBtn": "Bestätigen"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menü",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Typ-Eigenschaften"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Klassen",
            "custompages": "Maßseiten",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domains",
            "email": "E-mail",
            "generaloptions": "Allgemeine Wahlmöglichkeiten",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Sprachen",
            "lookuptypes": "Lookup Typ",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Vorgänge",
            "reports": "<em>Reports</em>",
            "searchfilters": "Suchfilter",
            "servermanagement": "Serververwaltung",
            "simples": "<em>Simples</em>",
            "standard": "Standard",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Prozessmanager",
            "title": "Navigieren",
            "users": "Benutzer",
            "views": "Sicht",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiv"
                                },
                                "description": {
                                    "label": "Beschreibung"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "parent": {
                                    "label": "Vererbt von"
                                },
                                "stoppableByUser": {
                                    "label": "Durch den Benutzer anhaltbar"
                                },
                                "superclass": {
                                    "label": "Superklasse"
                                }
                            }
                        },
                        "icon": "Icon",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktiv"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Abbrechen",
                    "closeBtn": "Schließen",
                    "deleteBtn": {
                        "tooltip": "Löschen"
                    },
                    "disableBtn": {
                        "tooltip": "Ausschalten"
                    },
                    "editBtn": {
                        "tooltip": "Bearbeiten"
                    },
                    "enableBtn": {
                        "tooltip": "Aktivieren"
                    },
                    "saveBtn": "Bestätigen",
                    "versionBtn": {
                        "tooltip": "Version"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Prozess hinzufügen"
                },
                "printSchemaBtn": {
                    "text": "Schema ausdrucken"
                },
                "processLabel": "Prozesse",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiv"
                                },
                                "description": {
                                    "label": "Beschreibung"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "stoppableByUser": {
                                    "label": "Durch den Benutzer anhaltbar"
                                }
                            }
                        },
                        "icon": "Icon"
                    }
                }
            },
            "title": "Benutzer",
            "toolbar": {
                "addUserBtn": {
                    "text": "Benutzer hinzufügen"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});