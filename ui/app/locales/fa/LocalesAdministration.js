Ext.define('CMDBuildUI.locales.fa.LocalesAdministration', {
    "singleton": true,
    "localization": "fa",
    "administration": {
        "attributes": {
            "attribute": "خاصیت",
            "attributes": "خواص",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "فعال",
                "description": "توضیحات",
                "domain": "دامنه",
                "editortype": "نوع ویرایشگر",
                "filter": "فیلتر",
                "group": "گروه",
                "help": "<em>Help</em>",
                "includeinherited": "شامل وراثت شود",
                "iptype": "IP type",
                "lookup": "مراجعه",
                "mandatory": "اجباری",
                "maxlength": "<em>Max Length</em>",
                "mode": "مد",
                "name": "نام",
                "precision": "دقت",
                "preselectifunique": "Preselect if unique",
                "scale": "اندازه",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "نوع",
                "unique": "یکتا",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "همه",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "قابل ویرایش",
                "editorhtml": "Html",
                "hidden": "مخفی",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "متن ساده",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "فقط خواندن",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "فعال",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "انصراف",
                "description": "توضیحات",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Edit metadata",
                "grouping": "<em>Grouping</em>",
                "mandatory": "اجباری",
                "name": "نام",
                "save": "ذخیره",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "نوع",
                "unique": "یکتا",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Delete",
                "disableattribute": "غیر فعال",
                "editattribute": "ویرایش",
                "enableattribute": "فعال",
                "openattribute": "باز",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Delete"
                                },
                                "edit": {
                                    "tooltip": "ویرایش"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "همه"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "جاری"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "وضعیت",
                                    "values": {
                                        "active": {
                                            "label": "وضعیت"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Delete"
                                },
                                "editTrigger": {
                                    "tooltip": "ویرایش"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "وضعیت"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "فعال"
                                },
                                "classType": {
                                    "label": "نوع"
                                },
                                "description": {
                                    "label": "توضیحات"
                                },
                                "name": {
                                    "label": "نام"
                                },
                                "parent": {
                                    "label": "والدین"
                                },
                                "superclass": {
                                    "label": "کلاس بالایی"
                                }
                            }
                        },
                        "icon": "Icon",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "وضعیت"
                    },
                    "values": {
                        "active": "فعال"
                    }
                },
                "title": "ویژگیها",
                "toolbar": {
                    "cancelBtn": "انصراف",
                    "closeBtn": "بستن",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "disableBtn": {
                        "tooltip": "غیر فعال"
                    },
                    "editBtn": {
                        "tooltip": "تغییر کلاس"
                    },
                    "enableBtn": {
                        "tooltip": "فعال"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "چاپ کلاس"
                    },
                    "saveBtn": "ذخیره"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "کلاسها",
            "toolbar": {
                "addClassBtn": {
                    "text": "اضافه کردن کلاس"
                },
                "classLabel": "کلاس",
                "printSchemaBtn": {
                    "text": "چاپ طرح"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "همه",
                "cancel": "انصراف",
                "clone": "مشابه گیری",
                "close": "بستن",
                "create": "Create",
                "delete": "Delete",
                "disable": "غیر فعال",
                "edit": "ویرایش",
                "enable": "فعال",
                "no": "خیر",
                "print": "چاپ",
                "save": "ذخیره",
                "update": "بخ روز رسانی",
                "yes": "بله"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "توجه",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "خطا",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "در حال بارگیری",
                "saving": "<em>Saving...</em>",
                "success": "موفقیت",
                "warning": "هشدار",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "ویرایش"
            }
        },
        "domains": {
            "domain": "دامنه",
            "fieldlabels": {
                "destination": "مقصد",
                "enabled": "فعال شده",
                "origin": "مبدا"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "توضیحات"
                                },
                                "descriptionDirect": {
                                    "label": "توضیحات مستقیم"
                                },
                                "descriptionInverse": {
                                    "label": "توضیحات معکوس"
                                },
                                "name": {
                                    "label": "نام"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "انصراف",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "disableBtn": {
                        "tooltip": "غیر فعال"
                    },
                    "editBtn": {
                        "tooltip": "ویرایش"
                    },
                    "enableBtn": {
                        "tooltip": "فعال"
                    },
                    "saveBtn": "ذخیره"
                }
            },
            "singularTitle": "دامنه",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "ویژگیها"
            },
            "toolbar": {
                "addBtn": {
                    "text": "اضافه کردن دامنه"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "فعال",
                "attachments": "ضمیمه",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "توضیحات",
                "detail": "جزئیات",
                "email": "ایمیل",
                "exportcsv": "Export CSV file",
                "filters": "<em>Filters</em>",
                "history": "تاریخچه",
                "importcsvfile": "Import CSV file",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "نام",
                "note": "یاداشت",
                "relations": "روابط",
                "type": "نوع",
                "username": "نام کاربری"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "ادمین با دسترسی محدود",
                "normal": "نرمال",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "فیلترهای پیشفرض",
                "defaultread": "Def. + R.",
                "description": "توضیحات",
                "filters": "<em>Filters</em>",
                "group": "گروه",
                "name": "نام",
                "none": "هیچ",
                "permissions": "اجازه",
                "read": "خواندن",
                "uiconfig": "تنظیمات واسط کاربری",
                "userslist": "<em>Users list</em>",
                "write": "نوشتن"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "فعال"
                                },
                                "name": {
                                    "label": "نام"
                                },
                                "parent": {
                                    "label": "والدین"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "فعال"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "انصراف",
                    "closeBtn": "بستن",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "editBtn": {
                        "tooltip": "ویرایش"
                    },
                    "saveBtn": "ذخیره"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "انصراف",
                    "closeBtn": "بستن",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "editBtn": {
                        "tooltip": "ویرایش"
                    },
                    "saveBtn": "ذخیره"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "منو",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "ویژگیهای نوع"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "کلاسها",
            "custompages": "صفحات سفارشی",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "دامنه ها",
            "email": "ایمیل",
            "generaloptions": "گزینه های عمومی",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "زبانها",
            "lookuptypes": "انواع جستجو",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "فرآیندها",
            "reports": "<em>Reports</em>",
            "searchfilters": "فیلتر جستجو",
            "servermanagement": "مدیریت سرور",
            "simples": "<em>Simples</em>",
            "standard": "استاندارد",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "مدیریت کارها",
            "title": "هدایت",
            "users": "کاربران",
            "views": "نمایش",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "فعال"
                                },
                                "description": {
                                    "label": "توضیحات"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "نام"
                                },
                                "parent": {
                                    "label": "به ارث رسیده از"
                                },
                                "stoppableByUser": {
                                    "label": "کاربر قابل بازداری"
                                },
                                "superclass": {
                                    "label": "کلاس بالایی"
                                }
                            }
                        },
                        "icon": "Icon",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "وضعیت"
                    },
                    "values": {
                        "active": "فعال"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "انصراف",
                    "closeBtn": "بستن",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "disableBtn": {
                        "tooltip": "غیر فعال"
                    },
                    "editBtn": {
                        "tooltip": "ویرایش"
                    },
                    "enableBtn": {
                        "tooltip": "فعال"
                    },
                    "saveBtn": "ذخیره",
                    "versionBtn": {
                        "tooltip": "نسخه"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "اضافه کردن پروسه"
                },
                "printSchemaBtn": {
                    "text": "چاپ طرح"
                },
                "processLabel": "Workflow",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "فعال"
                                },
                                "description": {
                                    "label": "توضیحات"
                                },
                                "name": {
                                    "label": "نام"
                                },
                                "stoppableByUser": {
                                    "label": "کاربر قابل بازداری"
                                }
                            }
                        },
                        "icon": "Icon"
                    }
                }
            },
            "title": "کاربران",
            "toolbar": {
                "addUserBtn": {
                    "text": "اضافه کردن کاربر"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});