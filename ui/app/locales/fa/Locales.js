Ext.define('CMDBuildUI.locales.fa.Locales', {
    "requires": ["CMDBuildUI.locales.fa.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "fa",
    "administration": CMDBuildUI.locales.fa.LocalesAdministration.administration,
    "attachments": {
        "add": "اضافه کردن ضمیمه",
        "author": "نویسنده",
        "category": "دسته بندی",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "توضیحات",
        "download": "دانلود",
        "editattachment": "ویرایش ضمیمه",
        "file": "فایل",
        "filename": "نام فایل",
        "majorversion": "Major version",
        "modificationdate": "تاریخ تغییر",
        "uploadfile": "<em>Upload file...</em>",
        "version": "نسخه",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "نمایش هیچکدام",
                "showAll": "نمایش همه"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Visibility"
        },
        "menu": {
            "camera": "Camera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Pan",
            "resetView": "<em>Reset View</em>",
            "rotate": "چرخش",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "اضافه کردن کارت",
            "clone": "مشابه گیری",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "حذف کارت",
            "modifycard": "ویرایش کارت",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "اضافه",
            "apply": "اعمال کردن",
            "cancel": "انصراف",
            "close": "بستن",
            "delete": "Delete",
            "edit": "ویرایش",
            "execute": "<em>Execute</em>",
            "remove": "حذف",
            "save": "ذخیره",
            "saveandapply": "ذخیره و اعمال کردن",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "فعالیت",
            "attachments": "ضمیمه",
            "card": "کارت",
            "details": "جزئیات",
            "emails": "<em>Emails</em>",
            "history": "تاریخچه",
            "notes": "یاداشتها",
            "relations": "روابط"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "اضافه کردن فیلتر",
        "any": "همه",
        "attribute": "یک خاصیت را انتخاب کنید",
        "attributes": "خواص",
        "clearfilter": "پاک کردن فیلتر",
        "clone": "مشابه گیری",
        "copyof": "کپی از",
        "description": "توضیحات",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "FromSelection",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "نام",
        "newfilter": "<em>New filter</em>",
        "noone": "هیچ کدام",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "شروع میشود با",
            "between": "مابین",
            "contained": "شامل است",
            "containedorequal": "شامل یا برابر است",
            "contains": "شامل",
            "containsorequal": "شامل یا برابر",
            "different": "تفاوت",
            "doesnotbeginwith": "شروع نشود با ",
            "doesnotcontain": "شامل نباشد",
            "doesnotendwith": "تمام نشود با",
            "endswith": "پایان یابد با ",
            "equals": "مساوی باشد با",
            "greaterthan": "بزرگتر از",
            "isnotnull": "تهی نیست",
            "isnull": "تهی است",
            "lessthan": "کوچکتر از"
        },
        "relations": "روابط",
        "type": "نوع",
        "typeinput": "پارامتر ورودی",
        "value": "مقدار"
    },
    "gis": {
        "card": "کارت",
        "externalServices": "سرویسهای خارجی",
        "geographicalAttributes": "خواص جئوگرافیک",
        "geoserverLayers": "Geoserver layers",
        "layers": "لایه ها",
        "list": "لیست",
        "mapServices": "<em>Map Services</em>",
        "root": "ریشه",
        "tree": "درخت هدایت",
        "view": "مشاهده"
    },
    "history": {
        "begindate": "تاریخ شروع",
        "enddate": "تاریخ اتمام",
        "user": "کاربر"
    },
    "login": {
        "buttons": {
            "login": "ورود",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "زبان",
            "password": "رمز عبور",
            "tenants": "<em>Tenants</em>",
            "username": "نام کاربری"
        },
        "title": "ورود"
    },
    "main": {
        "administrationmodule": "ماژول ادمین",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "خروج",
        "managementmodule": "ماژول مدیریت اطلاعات",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "کلاسها",
        "custompages": "صفحات سفارشی",
        "dashboards": "<em>Dashboards</em>",
        "processes": "فرآیندها",
        "reports": "<em>Reports</em>",
        "views": "نمایش"
    },
    "notes": {
        "edit": "ویرایش یاداشت"
    },
    "notifier": {
        "error": "خطا",
        "genericerror": "<em>Generic error</em>",
        "info": "اطلاعات",
        "success": "موفقیت",
        "warning": "هشدار"
    },
    "processes": {
        "action": {
            "advance": "جلو",
            "label": "<em>Action</em>"
        },
        "startworkflow": "شروع",
        "workflow": "Workflow"
    },
    "relationGraph": {
        "card": "کارت",
        "cardList": "<em>Card List</em>",
        "cardRelation": "رابطه",
        "class": "<em>Class</em>",
        "class:": "کلاس",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "باز کردن گراف رابطه",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "رابطه",
        "relationGraph": "گراف رابطه"
    },
    "relations": {
        "adddetail": "اضافه کردن جزئیات",
        "addrelations": "اضافه کردن رابطه",
        "attributes": "خواص",
        "code": "کد",
        "deletedetail": "حذف جزئیات",
        "deleterelation": "حذف رابطه",
        "description": "توضیحات",
        "editcard": "ویرایش کارت",
        "editdetail": "ویرایش جزئیات",
        "editrelation": "ویرایش رابطه",
        "mditems": "<em>items</em>",
        "opencard": "بازکردن کارتهای مرتبط",
        "opendetail": "نمایش جزئیات",
        "type": "نوع"
    },
    "widgets": {
        "customform": {
            "addrow": "اضافه کردن سطر",
            "clonerow": "شبیه سازی سطر",
            "deleterow": "حذف سطر",
            "editrow": "ویرایش سطر",
            "export": "خروچی گرفتن",
            "import": "بارگذاری",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "انتخابهای پیشفرض را انجام بده",
            "togglefilterdisabled": "Disable grid filter",
            "togglefilterenabled": "Enable grid filter"
        }
    }
});