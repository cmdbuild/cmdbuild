Ext.define('CMDBuildUI.locales.LocalesAdministration', {
    singleton: true,

    localization: 'en',

    administration: {
        attributes: {
            attribute: 'Attribute', // mapped
            attributes: 'Attributes', // mapped
            emptytexts: {
                search: 'Search...' // no map
            },
            fieldlabels: {
                actionpostvalidation: 'Action post validation', // no map
                active: 'Active', // mapped
                description: 'Description', // mapped
                domain: 'Domain', // mapped
                editortype: 'Editor type', // mapped
                filter: 'Filter', // mapped
                group: 'Group', // mapped
                help: 'Help', // no map
                includeinherited: 'Include Inherited', // mapped
                iptype: 'Ip type', // mapped
                lookup: 'Lookup', // mapped
                mandatory: 'Mandatory', // mapped
                maxlength: 'Max Length', // mapped
                mode: 'Mode', // mapped
                name: 'Name', // mapped
                precision: 'Precision', // mapped
                preselectifunique: 'Preselect if unique', // mapped
                scale: 'Scale', // mapped
                showif: 'Show if', // no map
                showingrid: 'Show in grid', // no map
                showinreducedgrid: 'Show in reduced grid', // no map
                type: 'Type', // mapped
                unique: 'Unique', // mapped
                validationrules: 'Validation rules' // no map
            },
            strings: {
                any: 'Any',  // mapped
                draganddrop: 'Drag and drop to reorganize', // no map
                editable: 'Editable', // mapped
                editorhtml: 'Editor HTML', // mapped
                hidden: 'Hidden', // mapped
                immutable: 'Immutable', // no map
                ipv4: 'IPV4', // mapped
                ipv6: 'IPV6', // mapped
                plaintext: 'Plain text', // mapped
                precisionmustbebiggerthanscale: 'Precision must be bigger than Scale', // no map
                readonly: 'Read only', // mapped
                scalemustbesmallerthanprecision: 'Scale must be smaller than Precision', // no map
                thefieldmandatorycantbechecked: 'The field "Mandatory" can\'t be checked', // no map
                thefieldmodeishidden: 'The field "Mode" is hidden', // no map
                thefieldshowingridcantbechecked: 'The field "Show in grid" can\'t be checked', // no map
                thefieldshowinreducedgridcantbechecked: 'The field "Show in reduced grid" can\'t be checked' // no map
            },

            texts: {
                active: 'Active', // mapped
                addattribute: 'Add attribute',
                cancel: 'Cancel', // mapped
                description: 'Description', // mapped
                editingmode: 'Editing mode',
                editmetadata: 'Edit metadata', // mapped
                grouping: 'Grouping', // no map
                mandatory: 'Mandatory', // mapped
                name: 'Name', // mapped
                save: 'Save', // mapped
                saveandadd: 'Save and Add',
                showingrid: 'Show in grid',
                type: 'Type', // mapped
                unique: 'Unique', // mapped
                viewmetadata: 'View metadata' // no map
            },
            titles: {
                generalproperties: 'General properties', // no map
                typeproperties: 'Type properties' // mapped
            },
            tooltips: {
                deleteattribute: 'Delete', // mapped
                disableattribute: 'Disable', // mapped
                editattribute: 'Edit', // mapped
                enableattribute: 'Enable', // mapped
                openattribute: 'Open', //mapped
                translate: 'Translate' // no map
            }
        },
        classes: {
            texts:{

            },
            strings: {
                geaoattributes: 'Geo attributes', // no map
                levels: 'Levels' // no map
            },
            properties: {
                form: {
                    fieldsets: {
                        ClassAttachments: 'Class Attachments', // no map
                        classParameters: 'Class Parameters', // no map
                        contextMenus: {
                            actions: {
                                delete: {
                                    tooltip: 'Delete' // mapped
                                },
                                edit: {
                                    tooltip: 'Edit' // mapped
                                },
                                moveDown: {
                                    tooltip: 'Move Down' // no map
                                },
                                moveUp: {
                                    tooltip: 'Move Up' // no map
                                }
                            },
                            inputs: {
                                applicability: {
                                    label: 'Applicability', // no map
                                    values: {
                                        all: {
                                            label: 'All' // mapping
                                        },
                                        many: {
                                            label: 'Current and selected' // no map
                                        },
                                        one: {
                                            label: 'Current' // mapping
                                        }
                                    }
                                },
                                javascriptScript: {
                                    label: 'Javascript script / custom GUI Paramenters'// no map
                                },
                                menuItemName: {
                                    label: 'Menu item name', // no map
                                    values: {
                                        separator: {
                                            label: '[---------]' // no map
                                        }
                                    }
                                },
                                status: {
                                    label: 'Status', // mapped
                                    values: {
                                        active: {
                                            label: 'Active' // mapped
                                        }
                                    }
                                },
                                typeOrGuiCustom: {
                                    label: 'Type / GUI custom', // no map
                                    values: {
                                        component: {
                                            label: 'Custom GUI' // no map
                                        },
                                        custom: {
                                            label: 'Script Javascript' // no map
                                        },
                                        separator: {
                                            label: ''
                                        }
                                    }
                                }
                            },
                            title: 'Context Menus' // no map
                        },
                        defaultOrders: 'Default Orders', // no map
                        formTriggers: {
                            actions: {
                                addNewTrigger: {
                                    tooltip: 'Add new Trigger' // no map
                                },
                                deleteTrigger: {
                                    tooltip: 'Delete' // no map
                                },
                                editTrigger: {
                                    tooltip: 'Edit' // mapped
                                },
                                moveDown: {
                                    tooltip: 'Move Down' // no map
                                },
                                moveUp: {
                                    tooltip: 'Move Up' // no map
                                }
                            },
                            inputs: {
                                createNewTrigger: {
                                    label: 'Create new form trigger' // no map
                                },
                                events: {
                                    label: 'Events', // no map
                                    values: {
                                        afterClone: {
                                            label: 'After Clone' // no map
                                        },
                                        afterEdit: {
                                            label: 'After Edit' // no map
                                        },
                                        afterInsert: {
                                            label: 'After Insert' // no map
                                        },
                                        beforeClone: {
                                            label: 'Before Clone' // no map
                                        },
                                        beforeEdit: {
                                            label: 'Before Edit' // no map
                                        },
                                        beforeInsert: {
                                            label: 'Before Insert' // no map
                                        },
                                        beforView: {
                                            label: 'Before View' // no map
                                        }
                                    }
                                },
                                javascriptScript: {
                                    label: 'Javascript script' // no map
                                },
                                status: {
                                    label: 'Status' // mapped
                                }
                            },
                            title: 'Form Triggers' // no map
                        },
                        formWidgets: 'Form Widgets', // no map
                        generalData: {
                            inputs: {
                                active: {
                                    label: 'Active' // mapped
                                },
                                classType: {
                                    label: 'Type' // mapped
                                },
                                description: {
                                    label: 'Description' // mapped
                                },
                                name: {
                                    label: 'Name' // mapped
                                },
                                parent: {
                                    label: 'Parent' // mapped
                                },
                                superclass: {
                                    label: 'Supeclass' // mapped
                                }
                            }
                        },
                        icon: 'Icon',  // mapped
                        validation: {
                            inputs: {
                                validationRule: {
                                    label: 'Validation Rule' // no map
                                }
                            },
                            title: 'Validation' // no map
                        }
                    },
                    inputs: {
                        events: 'Events', // no map
                        javascriptScript: 'Javascript Script', // no map
                        status: 'Status' // mapped
                    },
                    tooltips: {},
                    values: {
                        active: 'Active'  // mapped
                    }
                },
                title: 'Properties', // mapped
                toolbar: {
                    cancelBtn: 'Cancel', // mapped
                    closeBtn: 'Close', // mapped
                    deleteBtn: {
                        tooltip: 'Delete' // mapped
                    },
                    disableBtn: {
                        tooltip: 'Disable' // mapped
                    },
                    editBtn: {
                        tooltip: 'Modify class' // mapped
                    },
                    enableBtn: {
                        tooltip: 'Enable' // mapped
                    },
                    printBtn: {
                        printAsOdt: 'OpenOffice Odt', // mapped
                        printAsPdf: 'Adobe Pdf', // mapped
                        tooltip: 'Print class' // mapped
                    },
                    saveBtn: 'Save' // mapped
                }
            },
            title: 'Classes', // mapped
            toolbar: {
                addClassBtn: {
                    text: 'Add class' // mapped
                },
                classLabel: 'Class', // mapped
                printSchemaBtn: {
                    text: 'Print schema' // mapped
                },
                searchTextInput: {
                    emptyText: 'Search all classes' // no map
                }
            }
        },
        common: {
            actions: {
                add: 'Add', // mapped
                activate: 'Activate',
                cancel: 'Cancel', // mapped
                clone: 'Clone', // mapped
                close: 'Close', // mapped
                create: 'Create', // mapped
                delete: 'Delete', // mapped
                disable: 'Disable', // mapped
                edit: 'Edit', // mapped
                enable: 'Enable', // mapped
                no: 'No', // mapped
                print: 'Print', // mapped
                save: 'Save', // mapped
                update: 'Update', // mapped
                yes: 'Yes' // mapped
            },
            messages: {
                areyousuredeleteitem: 'Are you sure you want to delete this item?', // no map
                ascendingordescending: 'This value is not valid, please select "Ascending" or "Descending"', // no map
                attention: 'Attention', // mapped
                cannotsortitems: 'You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.', // no map
                cantcontainchar: 'The class name can\'t contain {0} character.', // no map
                correctformerrors: 'Please correct indicated errors', // no map
                disabled: 'disabled', // no map
                enabled: 'enabled', // no map
                error: 'Error', // mapped
                greaterthen: 'The class name can\'t be greater than {0} characters', // no map
                itemwascreated: 'Item was created.', // no map
                loading: 'Loading...', // mapped
                saving: 'Saving...', // no map
                success: 'Success', // mapped
                warning: 'Warning', // mapped
                was: 'was', // no map
                wasdeleted: 'was deleted' // no map
            },
            tooltips: {
                edit: 'Edit' // mapped
            }
        },
        domains: {
            pluralTitle: 'Domains', // TODO: change with domains
            fieldlabels: {
                destination: 'Destination', // mapped
                enabled: 'Enabled', // mapped
                origin: 'Origin' // mapped
            },

            domain: 'Domain', // mapped
            strings: {

            },    
            texts: {
                properties: 'Properties', // mapped
                enabledomains: 'Enabled domains' // no map
            },   
            properties: { // remove
                form: {
                    fieldsets: {
                        
                        generalData: {
                            inputs: {
                                description: {
                                    label: 'Description' // mapped
                                },
                                descriptionDirect: {
                                    label: 'Direct description' // mapped
                                },
                                descriptionInverse: {
                                    label: 'Inverse description' // mapped
                                },
                                name: {
                                    label: 'Name' // mapped
                                }
                            }
                        }
                    }
                },
            
                toolbar: {
                    cancelBtn: 'Cancel', // mappped
                    deleteBtn: {
                        tooltip: 'Delete' // mapped
                    },
                    disableBtn: {
                        tooltip: 'Disable' // mapped
                    },
                    editBtn: {
                        tooltip: 'Edit' // mapped
                    },
                    enableBtn: {
                        tooltip: 'Enable' // mapped
                    },
                    saveBtn: 'Save' // mapped
                }
            },
            singularTitle: 'Domain', // mapped
            toolbar: {
                addBtn: {
                    text: 'Add domain' // mapped
                },
                searchTextInput: {
                    emptyText: 'Search all domains' // no map
                }
            }
        },
        groupandpermissions: {
            singular: 'Group and permission', // no map
            plural: 'Groups and permissions', // no map
            emptytexts: {
                searchingrid: 'Search in grid...', // no map
                searchusers: 'Search users...' // no map
            },
            fieldlabels: {
                active: 'Active', // mapped
                actions: 'Actions', // mapped
                attachments: 'Attachments', // mapped
                datasheet: 'Data sheet', // no map
                defaultpage: 'Default page', // no map
                description: 'Description', // mapped
                detail: 'Detail', // mapped
                email: 'E-mail', // mapped
                exportcsv: 'Export CSV file', // mapped
                filters: 'Filters', // no map
                history: 'History', // mapped
                importcsvfile: 'Import CSV file', // mapped
                massiveeditingcards: 'Massive editing of cards', // no map
                name: 'Name', // mapped
                note: 'Note', // mapped
                relations: 'Relations', // mapped
                type: 'Type', // mapped
                username: 'Username' // mapped
            },
            strings: {
                admin: 'Admin', // no map
                limitedadmin: 'Limited administrator', // mapped
                normal: 'Normal', // mapped
                readonlyadmin: 'Read-only admin',
                displaytotalrecords: '{2} records',
                displaynousersmessage: "No users to display"
            },
            texts: {
                class: 'Class',
                copyfrom: 'Copy from', // new
                default: 'Default',
                defaultfilter: 'Default filter', // no map
                defaultfilters: 'Default filters', // mapped
                defaultread: 'Def. + R', // mapped
                description: 'Description', // mapped
                filters: 'Filters', // no map
                group: 'Group', // mapped
                name: 'Name', // mapped
                none: 'None', // mapped
                permissions: 'Permissions', // mapped
                read: 'Read', // mapped
                userslist: 'Users list', // no map
                uiconfig: 'UI configuration', // mapped
                write: 'Write' // mapped
            },
            titles: {
                allusers: 'All users', // no map
                generalattributes: 'General Attributes', // no map
                disabledactions: 'Disabled actions', // no map
                disabledallelements: 'Functionality disabled Navigation menu "All Elements"', // no map
                disabledmanagementprocesstabs: 'Tabs Disabled Management Processes', // no map
                disabledutilitymenu: 'Functionality disabled Utilities Menu', // no map
                managementdisabledtabs: 'Tabs Disabled Management Classes', // no map
                usersassigned: 'Users assigned' // no map
            },
            tooltips: {
                filters: 'Filters', // no map
                removefilters: 'Remove filters', // no map
                disabledactions: 'Disabled actions', // no map
                removedisabledactions: 'Remove disabled actions' // no map
            }
        },
        lookuptypes: {
            title: 'Lookup Types',
            toolbar: {
                addClassBtn: {
                    text: 'Add lookup'
                },
                classLabel: 'List',
                printSchemaBtn: {
                    text: 'Print lookup'
                },
                searchTextInput: {
                    emptyText: 'Search all lookups...'
                }
            },
            type: {
                form: {
                    fieldsets: {
                        generalData: {
                            inputs: {
                                active: {
                                    label: 'Active' // mapped
                                },
                                name: {
                                    label: 'Name' // mapped
                                },
                                parent: {
                                    label: 'Parent' // mapped
                                }
                            }
                        }
                    },
                    tooltips: {},
                    values: {
                        active: 'Active' // mapped
                    }
                },
                title: 'Lookup lists',
                toolbar: {
                    cancelBtn: 'Cancel', // mapped
                    closeBtn: 'Close', // mapped
                    deleteBtn: {
                        tooltip: 'Delete' // mapped
                    },
                    editBtn: {
                        tooltip: 'Edit' // mapped
                    },
                    saveBtn: 'Save' // mapped
                }
            }
        },
        menus: {
            main: {
                toolbar: {
                    cancelBtn: 'Cancel', // mapped
                    closeBtn: 'Close', // mapped
                    deleteBtn: {
                        tooltip: 'Delete' // mapped
                    },
                    editBtn: {
                        tooltip: 'Edit' // mapped
                    },
                    saveBtn: 'Save' // mapped
                }
            },
            pluralTitle: 'Menus', // no map
            singularTitle: 'Menu', // no map
            toolbar: {
                addBtn: {
                    text: 'Add menu' // no map
                }
            }
        },
        navigation: {
            bim: 'BIM', // mapped
            classes: 'Classes', // mapped
            custompages: 'Custom pages', // mapped
            dashboards: 'Dashboards', // no map
            dms: 'DMS', // mapped
            domains: 'Domains', // mapped
            email: 'Email', // mapped
            generaloptions: 'General options', // mapped
            gis: 'GIS', // mapped
            groupsandpermissions: 'Groups and permissions', // no map
            languages: 'Languages', // mapped
            lookuptypes: 'Lookup types', // mapped
            menus: 'Menus', // no map
            multitenant: 'Multitenant', // no map
            navigationtrees: 'Navigation trees', // mapped
            processes: 'Processes', // mapped
            reports: 'Reports', // no map
            searchfilters: 'Search filters', // mapped
            servermanagement: 'Server management',
            simples: 'Simples', // no map
            standard: 'Standard', // mapped
            systemconfig: 'System config', // no map
            taskmanager: 'Task manager', // no map
            title: 'Navigation', // mapped
            users: 'Users', // mapped
            views: 'Views', // mapped
            workflow: 'Workflow' // no map
        },
        processes: {
            properties: {
                form: {
                    fieldsets: {
                        defaultOrders: 'Default Orders', // no map
                        generalData: {
                            inputs: {
                                active: {
                                    label: 'Active' // mapped
                                },
                                description: {
                                    label: 'Description' // mapped
                                },
                                enableSaveButton: {
                                    label: 'Hide "Save" button' // no map
                                },
                                name: {
                                    label: 'Name' // mapped
                                },
                                parent: {
                                    label: 'Inherits from' // mapped
                                },
                                stoppableByUser: {
                                    label: 'Stoppable by User' // mapped
                                },
                                superclass: {
                                    label: 'Superclass' // mapped
                                }
                            }
                        },
                        icon: 'Icon',
                        processParameter: {
                            inputs: {
                                defaultFilter: {
                                    label: 'Default filter' // no map
                                },
                                messageAttribute: {
                                    label: 'Message attribute' // no map
                                },
                                stateAttribute: {
                                    label: 'State attribute' // no map
                                }
                            },
                            title: 'Process parameters' // no map
                        },
                        validation: {
                            inputs: {
                                validationRule: {
                                    label: 'Validation Rule' // no map
                                }
                            },
                            title: 'Validation' // no map
                        }
                    },
                    inputs: {
                        status: 'Status' // mapped
                    },
                    tooltips: {},
                    values: {
                        active: 'Active' // mapped
                    }
                },
                title: 'Properties',
                toolbar: {
                    cancelBtn: 'Cancel', // mapped
                    closeBtn: 'Close', // mapped
                    deleteBtn: {
                        tooltip: 'Delete' // mapped
                    },
                    disableBtn: {
                        tooltip: 'Disable' // mapped
                    },
                    editBtn: {
                        tooltip: 'Edit' // mapped
                    },
                    enableBtn: {
                        tooltip: 'Enable' // mapped
                    },
                    saveBtn: 'Save', // mapped
                    versionBtn: {
                        tooltip: 'Version' // mapped
                    }
                }
            },
            title: 'Processes',
            toolbar: {
                addProcessBtn: {
                    text: 'Add process' // mapped
                },
                printSchemaBtn: {
                    text: 'Print schema' // mapped
                },
                processLabel: 'Process', // mapped
                searchTextInput: {
                    emptyText: 'Search all processes' // no map
                }
            }
        },
        title: 'Administration', // no map
        users: {
            properties: {
                form: {
                    fieldsets: {
                        generalData: {
                            inputs: {
                                active: {
                                    label: 'Active' // mapped
                                },
                                description: {
                                    label: 'Description' // mapped
                                },
                                name: {
                                    label: 'Name' // mapped
                                },
                                stoppableByUser: {
                                    label: 'User stoppable' // mapped
                                }
                            }
                        }
                    }
                }
            },
            title: 'Users', // mapped
            toolbar: {
                addUserBtn: {
                    text: 'Add user' // mapped
                },
                searchTextInput: {
                    emptyText: 'Search all users' // no map
                }
            }
        }
    }
});