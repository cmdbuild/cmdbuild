Ext.define('CMDBuildUI.locales.fr.LocalesAdministration', {
    "singleton": true,
    "localization": "fr",
    "administration": {
        "attributes": {
            "attribute": "Attribut",
            "attributes": "Attributs",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Actif",
                "description": "Libellé",
                "domain": "Lien",
                "editortype": "Libellé",
                "filter": "Filtre",
                "group": "Groupe",
                "help": "<em>Help</em>",
                "includeinherited": "Inclure les éléments hérités",
                "iptype": "Type IP",
                "lookup": "valeur de liste",
                "mandatory": "Obligatoire",
                "maxlength": "<em>Max Length</em>",
                "mode": "Mode",
                "name": "Nom",
                "precision": "Précision",
                "preselectifunique": "Préselectionner si unique",
                "scale": "Échelle",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Type du serveur de fichiers",
                "unique": "Unique",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Tous",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Modifiable",
                "editorhtml": "Html",
                "hidden": "Caché",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Texte brut",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Lecture Seule",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Actif",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Abandonner",
                "description": "Libellé",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Modifier la méta donnée",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obligatoire",
                "name": "Nom",
                "save": "Enregistrer",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Type du serveur de fichiers",
                "unique": "Unique",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Supprimer",
                "disableattribute": "Désactiver",
                "editattribute": "Modifier",
                "enableattribute": "Activer",
                "openattribute": "En cours",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Supprimer"
                                },
                                "edit": {
                                    "tooltip": "Modifier"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Tous"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Actuel"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Statut",
                                    "values": {
                                        "active": {
                                            "label": "Statut"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Supprimer"
                                },
                                "editTrigger": {
                                    "tooltip": "Modifier"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Statut"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Actif"
                                },
                                "classType": {
                                    "label": "Type"
                                },
                                "description": {
                                    "label": "Libellé"
                                },
                                "name": {
                                    "label": "Nom"
                                },
                                "parent": {
                                    "label": "Parent"
                                },
                                "superclass": {
                                    "label": "Classe héritable"
                                }
                            }
                        },
                        "icon": "Icone",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Statut"
                    },
                    "values": {
                        "active": "Actif"
                    }
                },
                "title": "Propriétés",
                "toolbar": {
                    "cancelBtn": "Abandonner",
                    "closeBtn": "Fermer",
                    "deleteBtn": {
                        "tooltip": "Supprimer"
                    },
                    "disableBtn": {
                        "tooltip": "Désactiver"
                    },
                    "editBtn": {
                        "tooltip": "Modifier la classe"
                    },
                    "enableBtn": {
                        "tooltip": "Activer"
                    },
                    "printBtn": {
                        "printAsOdt": "ODT",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Imprimer la classe"
                    },
                    "saveBtn": "Enregistrer"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Classe",
            "toolbar": {
                "addClassBtn": {
                    "text": "Ajouter une classe"
                },
                "classLabel": "Classe",
                "printSchemaBtn": {
                    "text": "Imprimer le schéma"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Tous",
                "cancel": "Abandonner",
                "clone": "Copier",
                "close": "Fermer",
                "create": "Créer",
                "delete": "Supprimer",
                "disable": "Désactiver",
                "edit": "Modifier",
                "enable": "Activer",
                "no": "Non",
                "print": "Imprimer",
                "save": "Enregistrer",
                "update": "Mettre à jour",
                "yes": "Oui"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Attention",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Erreur",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Chargement...",
                "saving": "<em>Saving...</em>",
                "success": "Succès",
                "warning": "Attention",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Modifier"
            }
        },
        "domains": {
            "domain": "Lien",
            "fieldlabels": {
                "destination": "Destination",
                "enabled": "Activé",
                "origin": "Origine"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Libellé"
                                },
                                "descriptionDirect": {
                                    "label": "Libellé, sens direct"
                                },
                                "descriptionInverse": {
                                    "label": "Libellé, sens inverse"
                                },
                                "name": {
                                    "label": "Nom"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Abandonner",
                    "deleteBtn": {
                        "tooltip": "Supprimer"
                    },
                    "disableBtn": {
                        "tooltip": "Désactiver"
                    },
                    "editBtn": {
                        "tooltip": "Modifier"
                    },
                    "enableBtn": {
                        "tooltip": "Activer"
                    },
                    "saveBtn": "Enregistrer"
                }
            },
            "singularTitle": "Lien",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Propriétés"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Ajouter un lien"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Actif",
                "attachments": "Pièces jointes",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Libellé",
                "detail": "Détail",
                "email": "Email",
                "exportcsv": "Export fichier CSV",
                "filters": "<em>Filters</em>",
                "history": "Historique",
                "importcsvfile": "Import fichier CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Nom",
                "note": "Notes",
                "relations": "Relations",
                "type": "Type du serveur de fichiers",
                "username": "Nom d'utilisateur"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Administrateur limité",
                "normal": "Normal",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Filtres par défaut",
                "defaultread": "Def. + R.",
                "description": "Libellé",
                "filters": "<em>Filters</em>",
                "group": "Groupe",
                "name": "Nom",
                "none": "Aucun",
                "permissions": "Permissions",
                "read": "Lire",
                "uiconfig": "Configuration de l'IHM",
                "userslist": "<em>Users list</em>",
                "write": "Écrire"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Actif"
                                },
                                "name": {
                                    "label": "Nom"
                                },
                                "parent": {
                                    "label": "Parent"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Actif"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Abandonner",
                    "closeBtn": "Fermer",
                    "deleteBtn": {
                        "tooltip": "Supprimer"
                    },
                    "editBtn": {
                        "tooltip": "Modifier"
                    },
                    "saveBtn": "Enregistrer"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Abandonner",
                    "closeBtn": "Fermer",
                    "deleteBtn": {
                        "tooltip": "Supprimer"
                    },
                    "editBtn": {
                        "tooltip": "Modifier"
                    },
                    "saveBtn": "Enregistrer"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menu principal",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Type de propriétés"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Classe",
            "custompages": "Pages personnalisées",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Liens",
            "email": "Email",
            "generaloptions": "Options générales",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Langues",
            "lookuptypes": "Listes de valeurs",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Processus",
            "reports": "<em>Reports</em>",
            "searchfilters": "Chercher des filtres",
            "servermanagement": "Gestion du serveur",
            "simples": "<em>Simples</em>",
            "standard": "Standard",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Gestionnaire de tâches",
            "title": "Navigation",
            "users": "Utilisateurs",
            "views": "Vues",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Actif"
                                },
                                "description": {
                                    "label": "Libellé"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Nom"
                                },
                                "parent": {
                                    "label": "Hérite de"
                                },
                                "stoppableByUser": {
                                    "label": "Arrêtable par un utilisateur"
                                },
                                "superclass": {
                                    "label": "Classe héritable"
                                }
                            }
                        },
                        "icon": "Icone",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Statut"
                    },
                    "values": {
                        "active": "Actif"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Abandonner",
                    "closeBtn": "Fermer",
                    "deleteBtn": {
                        "tooltip": "Supprimer"
                    },
                    "disableBtn": {
                        "tooltip": "Désactiver"
                    },
                    "editBtn": {
                        "tooltip": "Modifier"
                    },
                    "enableBtn": {
                        "tooltip": "Activer"
                    },
                    "saveBtn": "Enregistrer",
                    "versionBtn": {
                        "tooltip": "Version"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Ajouter un processus"
                },
                "printSchemaBtn": {
                    "text": "Imprimer le schéma"
                },
                "processLabel": "Processus",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Actif"
                                },
                                "description": {
                                    "label": "Libellé"
                                },
                                "name": {
                                    "label": "Nom"
                                },
                                "stoppableByUser": {
                                    "label": "Arrêtable par un utilisateur"
                                }
                            }
                        },
                        "icon": "Icone"
                    }
                }
            },
            "title": "Utilisateurs",
            "toolbar": {
                "addUserBtn": {
                    "text": "Ajouter un utilisateur"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});