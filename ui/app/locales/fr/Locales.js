Ext.define('CMDBuildUI.locales.fr.Locales', {
    "requires": ["CMDBuildUI.locales.fr.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "fr",
    "administration": CMDBuildUI.locales.fr.LocalesAdministration.administration,
    "attachments": {
        "add": "Ajouter des pièces jointes",
        "author": "Auteur",
        "category": "Catégorie",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Description",
        "download": "Télécharger",
        "editattachment": "Modifier la pièce jointe",
        "file": "Fichier",
        "filename": "Nom de fichier",
        "majorversion": "Version majeure",
        "modificationdate": "Date de modification",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Version",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Tout masquer",
                "showAll": "Tout afficher"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Visibilité"
        },
        "menu": {
            "camera": "Appareil photo",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Panoramique",
            "resetView": "<em>Reset View</em>",
            "rotate": "Pivoter",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Ajouter une fiche",
            "clone": "Copier",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Supprimer la fiche",
            "modifycard": "Modifier la fiche",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Ajouter",
            "apply": "Appliquer",
            "cancel": "Abandonner",
            "close": "Fermer",
            "delete": "Supprimer",
            "edit": "Modifier",
            "execute": "<em>Execute</em>",
            "remove": "Supprimer",
            "save": "Enregistrer",
            "saveandapply": "Enregistrer et appliquer",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Activité",
            "attachments": "Pièces jointes",
            "card": "Fiche",
            "details": "Détails",
            "emails": "<em>Emails</em>",
            "history": "Historique",
            "notes": "Notes",
            "relations": "Relations"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Ajouter un filtre",
        "any": "Tous",
        "attribute": "Choisir un attribut",
        "attributes": "Attributs",
        "clearfilter": "Supprimer les filtres",
        "clone": "Copier",
        "copyof": "Copie de",
        "description": "Description",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "À partir de la sélection",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Nom",
        "newfilter": "<em>New filter</em>",
        "noone": "Pas Un",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Commence par",
            "between": "Entre",
            "contained": "Contenu",
            "containedorequal": "Contenu ou égal",
            "contains": "Contient",
            "containsorequal": "Contient ou est égal",
            "different": "Différent",
            "doesnotbeginwith": "Ne commence pas par",
            "doesnotcontain": "Ne contient pas",
            "doesnotendwith": "Ne se termine pas par",
            "endswith": "Se termine par",
            "equals": "Égale",
            "greaterthan": "Majeur",
            "isnotnull": "N'est pas nul",
            "isnull": "Est nul",
            "lessthan": "Mineur"
        },
        "relations": "Relations",
        "type": "Type du serveur de fichiers",
        "typeinput": "Paramètre d'entrée",
        "value": "Valeur"
    },
    "gis": {
        "card": "Fiche",
        "externalServices": "Services externes",
        "geographicalAttributes": "Attributs géographiques",
        "geoserverLayers": "Calques Geoserver",
        "layers": "Calques",
        "list": "Liste",
        "mapServices": "<em>Map Services</em>",
        "root": "Racine",
        "tree": "Arbre de navigation",
        "view": "Vue"
    },
    "history": {
        "begindate": "Date de début",
        "enddate": "Date de fin",
        "user": "Utilisateur"
    },
    "login": {
        "buttons": {
            "login": "Se connecter",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Langue",
            "password": "Mot de passe",
            "tenants": "<em>Tenants</em>",
            "username": "Nom d'utilisateur"
        },
        "title": "Se connecter"
    },
    "main": {
        "administrationmodule": "Module d'administration",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Déconnexion",
        "managementmodule": "Module de gestion des données",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Classe",
        "custompages": "Pages personnalisées",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Processus",
        "reports": "<em>Reports</em>",
        "views": "Vues"
    },
    "notes": {
        "edit": "Modifier la note"
    },
    "notifier": {
        "error": "Erreur",
        "genericerror": "<em>Generic error</em>",
        "info": "Info",
        "success": "Succès",
        "warning": "Attention"
    },
    "processes": {
        "action": {
            "advance": "Avancer",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Démarrer",
        "workflow": "Processus"
    },
    "relationGraph": {
        "card": "Fiche",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Relation",
        "class": "<em>Class</em>",
        "class:": "Classe",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Ouvrir le graphe de relations",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Relation",
        "relationGraph": "Graphe des relations"
    },
    "relations": {
        "adddetail": "Ajouter un détail",
        "addrelations": "Ajouter des relations",
        "attributes": "Attributs",
        "code": "Code",
        "deletedetail": "Supprimer le détail",
        "deleterelation": "Supprimer la relation",
        "description": "Description",
        "editcard": "Modifier la fiche",
        "editdetail": "Modifier le détail",
        "editrelation": "Modifier la relation",
        "mditems": "<em>items</em>",
        "opencard": "Ouvrir la fiche associée",
        "opendetail": "Voir le détail",
        "type": "Type du serveur de fichiers"
    },
    "widgets": {
        "customform": {
            "addrow": "Ajouter une ligne",
            "clonerow": "Cloner la ligne",
            "deleterow": "Supprimer la ligne",
            "editrow": "Modifier la ligne",
            "export": "Exporter",
            "import": "Importer",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Appliquer la sélection par défaut",
            "togglefilterdisabled": "Désactiver le filtre de la table",
            "togglefilterenabled": "Activer le filtre de table"
        }
    }
});