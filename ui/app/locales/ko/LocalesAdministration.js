Ext.define('CMDBuildUI.locales.ko.LocalesAdministration', {
    "singleton": true,
    "localization": "ko",
    "administration": {
        "attributes": {
            "attribute": "속성",
            "attributes": "액티비티",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "액티브",
                "description": "설명",
                "domain": "도메인",
                "editortype": "에디터 타입",
                "filter": "필터",
                "group": "그룹",
                "help": "<em>Help</em>",
                "includeinherited": "상속을 포함",
                "iptype": "IP타입",
                "lookup": "룩업",
                "mandatory": "필수",
                "maxlength": "<em>Max Length</em>",
                "mode": "모드",
                "name": "이름",
                "precision": "정확도",
                "preselectifunique": "유일한 경우 미리 선택",
                "scale": "스케일",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "타입",
                "unique": "유니크",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "모든",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "편집 가능",
                "editorhtml": "Html",
                "hidden": "숨김",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "플레인 텍스트",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "읽기 전용",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "액티브",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "취소",
                "description": "설명",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "메타 데이터 편집",
                "grouping": "<em>Grouping</em>",
                "mandatory": "필수",
                "name": "이름",
                "save": "저장",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "타입",
                "unique": "유니크",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "삭제",
                "disableattribute": "무효",
                "editattribute": "편집",
                "enableattribute": "유효하게 한다",
                "openattribute": "열기",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "삭제"
                                },
                                "edit": {
                                    "tooltip": "편집"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "모두"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "현재"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "상태",
                                    "values": {
                                        "active": {
                                            "label": "상태"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "삭제"
                                },
                                "editTrigger": {
                                    "tooltip": "편집"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "상태"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "액티브"
                                },
                                "classType": {
                                    "label": "타입"
                                },
                                "description": {
                                    "label": "설명"
                                },
                                "name": {
                                    "label": "이름"
                                },
                                "parent": {
                                    "label": "부모"
                                },
                                "superclass": {
                                    "label": "슈퍼클래스"
                                }
                            }
                        },
                        "icon": "아이콘",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "상태"
                    },
                    "values": {
                        "active": "액티브"
                    }
                },
                "title": "속성",
                "toolbar": {
                    "cancelBtn": "취소",
                    "closeBtn": "클로스",
                    "deleteBtn": {
                        "tooltip": "삭제"
                    },
                    "disableBtn": {
                        "tooltip": "무효"
                    },
                    "editBtn": {
                        "tooltip": "클래스 변경"
                    },
                    "enableBtn": {
                        "tooltip": "유효하게 한다"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "클래스 인쇄"
                    },
                    "saveBtn": "저장"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "반",
            "toolbar": {
                "addClassBtn": {
                    "text": "클래스 추가"
                },
                "classLabel": "반",
                "printSchemaBtn": {
                    "text": "스키마 인쇄"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "모두",
                "cancel": "취소",
                "clone": "복제",
                "close": "클로스",
                "create": "작성",
                "delete": "삭제",
                "disable": "무효",
                "edit": "편집",
                "enable": "유효하게 한다",
                "no": "아니오",
                "print": "인쇄",
                "save": "저장",
                "update": "갱신",
                "yes": "네"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "주의",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "에러",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "로드 중...",
                "saving": "<em>Saving...</em>",
                "success": "성공",
                "warning": "경고",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "편집"
            }
        },
        "domains": {
            "domain": "도메인",
            "fieldlabels": {
                "destination": "목적지",
                "enabled": "유효",
                "origin": "타깃(기점)"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "설명"
                                },
                                "descriptionDirect": {
                                    "label": "순방향 설명"
                                },
                                "descriptionInverse": {
                                    "label": "역 방향 설명"
                                },
                                "name": {
                                    "label": "이름"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "취소",
                    "deleteBtn": {
                        "tooltip": "삭제"
                    },
                    "disableBtn": {
                        "tooltip": "무효"
                    },
                    "editBtn": {
                        "tooltip": "편집"
                    },
                    "enableBtn": {
                        "tooltip": "유효하게 한다"
                    },
                    "saveBtn": "저장"
                }
            },
            "singularTitle": "도메인",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "속성"
            },
            "toolbar": {
                "addBtn": {
                    "text": "도메인 추가"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</e",
                "active": "액티브",
                "attachments": "첨부",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "설명",
                "detail": "상세",
                "email": "이메일",
                "exportcsv": "CSV파일 익스포트",
                "filters": "<em>Filters</em>",
                "history": "이력",
                "importcsvfile": "CSV파일 가져옵니다",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "이름",
                "note": "노트",
                "relations": "릴레이션",
                "type": "타입",
                "username": "유저명"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "제한된 관리자",
                "normal": "일반적",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "기본 필터",
                "defaultread": "Def.+R.",
                "description": "설명",
                "filters": "<em>Filters</em>",
                "group": "그룹",
                "name": "이름",
                "none": "권한 없음",
                "permissions": "허가",
                "read": "읽기 권한",
                "uiconfig": "UI 구성",
                "userslist": "<em>Users list</em>",
                "write": "쓰기 권한"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "액티브"
                                },
                                "name": {
                                    "label": "이름"
                                },
                                "parent": {
                                    "label": "부모"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "액티브"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "취소",
                    "closeBtn": "클로스",
                    "deleteBtn": {
                        "tooltip": "삭제"
                    },
                    "editBtn": {
                        "tooltip": "편집"
                    },
                    "saveBtn": "저장"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "취소",
                    "closeBtn": "클로스",
                    "deleteBtn": {
                        "tooltip": "삭제"
                    },
                    "editBtn": {
                        "tooltip": "편집"
                    },
                    "saveBtn": "저장"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "메뉴",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "타입 속성"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "반",
            "custompages": "커스텀 페이지",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "도메인",
            "email": "이메일",
            "generaloptions": "일반 옵션",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "언어",
            "lookuptypes": "룩업 타입",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "프로세스",
            "reports": "<em>Reports</em>",
            "searchfilters": "검색 필터",
            "servermanagement": "서버 관리",
            "simples": "<em>Simples</em>",
            "standard": "표준",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "태스크 매니저",
            "title": "네비게이션",
            "users": "유저",
            "views": "뷰",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "액티브"
                                },
                                "description": {
                                    "label": "설명"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "이름"
                                },
                                "parent": {
                                    "label": "상속"
                                },
                                "stoppableByUser": {
                                    "label": "사용자에 의한 중단 가능"
                                },
                                "superclass": {
                                    "label": "슈퍼클래스"
                                }
                            }
                        },
                        "icon": "아이콘",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "상태"
                    },
                    "values": {
                        "active": "액티브"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "취소",
                    "closeBtn": "클로스",
                    "deleteBtn": {
                        "tooltip": "삭제"
                    },
                    "disableBtn": {
                        "tooltip": "무효"
                    },
                    "editBtn": {
                        "tooltip": "편집"
                    },
                    "enableBtn": {
                        "tooltip": "유효하게 한다"
                    },
                    "saveBtn": "저장",
                    "versionBtn": {
                        "tooltip": "버전"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "프로세스 추가"
                },
                "printSchemaBtn": {
                    "text": "스키마 인쇄"
                },
                "processLabel": "워크플로우",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "액티브"
                                },
                                "description": {
                                    "label": "설명"
                                },
                                "name": {
                                    "label": "이름"
                                },
                                "stoppableByUser": {
                                    "label": "사용자에 의한 중단 가능"
                                }
                            }
                        },
                        "icon": "아이콘"
                    }
                }
            },
            "title": "유저",
            "toolbar": {
                "addUserBtn": {
                    "text": "사용자 추가"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});