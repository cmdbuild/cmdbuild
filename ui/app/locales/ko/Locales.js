Ext.define('CMDBuildUI.locales.ko.Locales', {
    "requires": ["CMDBuildUI.locales.ko.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "ko",
    "administration": CMDBuildUI.locales.ko.LocalesAdministration.administration,
    "attachments": {
        "add": "첨부 추가",
        "author": "저자",
        "category": "카테고리",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "설명",
        "download": "다운로드",
        "editattachment": "첨부 변경",
        "file": "파일",
        "filename": "파일명",
        "majorversion": "메이저 버전",
        "modificationdate": "갱신일",
        "uploadfile": "<em>Upload file...</em>",
        "version": "버전",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "모두 숨기",
                "showAll": "모두 표시"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "표시"
        },
        "menu": {
            "camera": "카메라",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "팬/이동",
            "resetView": "<em>Reset View</em>",
            "rotate": "회전",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "카드 추가",
            "clone": "복제",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "카드 삭제",
            "modifycard": "카드 갱신",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "추가",
            "apply": "적용",
            "cancel": "취소",
            "close": "클로스",
            "delete": "삭제",
            "edit": "편집",
            "execute": "<em>Execute</em>",
            "remove": "삭제",
            "save": "저장",
            "saveandapply": "저장하고 적용",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "액티비티",
            "attachments": "첨부",
            "card": "카드",
            "details": "상세",
            "emails": "<em>Emails</em>",
            "history": "이력",
            "notes": "노트",
            "relations": "릴레이션"
        }
    },
    "filters": {
        "actions": "<em>Actions</e",
        "addfilter": "필터를 추가",
        "any": "모든",
        "attribute": "속성을 선택",
        "attributes": "액티비티",
        "clearfilter": "필터 클리어",
        "clone": "복제",
        "copyof": "복사",
        "description": "설명",
        "domain": "<em>Actions</e",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "선택에서",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrates</em>",
        "name": "이름",
        "newfilter": "<em>New filter</em>",
        "noone": "아무것도 없음",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "시작하는",
            "between": "간",
            "contained": "포함된다",
            "containedorequal": "포함됨 또는 동일",
            "contains": "포함",
            "containsorequal": "포함함 또는 동일",
            "different": "다른",
            "doesnotbeginwith": "시작하지 않는",
            "doesnotcontain": "포함하지 않는",
            "doesnotendwith": "끝나지 않는",
            "endswith": "~로 끝나는",
            "equals": "동일",
            "greaterthan": "크게",
            "isnotnull": "null값 아님",
            "isnull": "null값",
            "lessthan": "작은"
        },
        "relations": "릴레이션",
        "type": "타입",
        "typeinput": "입력 파라미터",
        "value": "값"
    },
    "gis": {
        "card": "카드",
        "externalServices": "외부 서비스",
        "geographicalAttributes": "지리적 속성",
        "geoserverLayers": "Geo서버 레이어",
        "layers": "층",
        "list": "리스트",
        "mapServices": "<em>Map Services</em>",
        "root": "루트",
        "tree": "네비게이션 트리",
        "view": "뷰"
    },
    "history": {
        "begindate": "시작",
        "enddate": "종료일",
        "user": "유저"
    },
    "login": {
        "buttons": {
            "login": "로그인",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "언어",
            "password": "비밀번호",
            "tenants": "<em>Tenants</em>",
            "username": "유저명"
        },
        "title": "로그인"
    },
    "main": {
        "administrationmodule": "관리자 모듈",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "로그 아웃",
        "managementmodule": "데이터 관리 모듈",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "반",
        "custompages": "커스텀 페이지",
        "dashboards": "<em>Dashboards</em>",
        "processes": "프로세스",
        "reports": "<em>Reports</em>",
        "views": "뷰"
    },
    "notes": {
        "edit": "노트 편집"
    },
    "notifier": {
        "error": "에러",
        "genericerror": "<em>Generic error</em>",
        "info": "정보",
        "success": "성공",
        "warning": "경고"
    },
    "processes": {
        "action": {
            "advance": "다음에",
            "label": "<em>Action</em>"
        },
        "startworkflow": "시작",
        "workflow": "워크플로우"
    },
    "relationGraph": {
        "card": "카드",
        "cardList": "<em>Card List</em>",
        "cardRelation": "릴레이션",
        "class": "<em>Class</em>",
        "class:": "반",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "릴레이션 그래프 열기",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "릴레이션",
        "relationGraph": "릴레이션 그래프"
    },
    "relations": {
        "adddetail": "상세 추가",
        "addrelations": "릴레이션 추가",
        "attributes": "액티비티",
        "code": "코드",
        "deletedetail": "상세 삭제",
        "deleterelation": "릴레이션 삭제",
        "description": "설명",
        "editcard": "카드 갱신",
        "editdetail": "상세 편집",
        "editrelation": "릴레이션을 편집",
        "mditems": "<em>items</em>",
        "opencard": "관련하는 카드를 열",
        "opendetail": "상세 보기",
        "type": "타입"
    },
    "widgets": {
        "customform": {
            "addrow": "행을 추가",
            "clonerow": "행을 복제",
            "deleterow": "행을 삭제",
            "editrow": "행 편집",
            "export": "익스포트",
            "import": "임포트",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "기본 선택을 적용",
            "togglefilterdisabled": "그리드 필터 무효화",
            "togglefilterenabled": "그리드 필터 유효화"
        }
    }
});