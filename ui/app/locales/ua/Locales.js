Ext.define('CMDBuildUI.locales.ua.Locales', {
    "requires": ["CMDBuildUI.locales.ua.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "ua",
    "administration": CMDBuildUI.locales.ua.LocalesAdministration.administration,
    "attachments": {
        "add": "Додати вкладення",
        "author": "Автор",
        "category": "Категорія",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Опис",
        "download": "Завантажити",
        "editattachment": "Змінити вкладення",
        "file": "Файл",
        "filename": "Ім'я файла",
        "majorversion": "Основна версія",
        "modificationdate": "Дата зміни",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Версія",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Приховати все",
                "showAll": "Показати все"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Видимість"
        },
        "menu": {
            "camera": "Камера",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Панорамування",
            "resetView": "<em>Reset View</em>",
            "rotate": "Повернути",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Додати картку",
            "clone": "Клонувати",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Видалити картку",
            "modifycard": "Змінити картку",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Додати",
            "apply": "Застосувати",
            "cancel": "Скасувати",
            "close": "Закрити",
            "delete": "Видалити",
            "edit": "Редагувати",
            "execute": "<em>Execute</em>",
            "remove": "Видалити",
            "save": "Зберегти",
            "saveandapply": "Зберегти і застосувати",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Активність",
            "attachments": "Вкладення",
            "card": "Картка",
            "details": "Подробиці",
            "emails": "<em>Emails</em>",
            "history": "Історія",
            "notes": "Замітки",
            "relations": "Зв'язки"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Додати фільтр",
        "any": "Будь-який",
        "attribute": "Вибрати атрибут",
        "attributes": "Атрибути",
        "clearfilter": "Очистити фільтр",
        "clone": "Клонувати",
        "copyof": "Копія з",
        "description": "Опис",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Із виділенного",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Ім'я",
        "newfilter": "<em>New filter</em>",
        "noone": "Жодного",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Починається з ",
            "between": "Поміж",
            "contained": "Міститься",
            "containedorequal": "Міститься або рівний",
            "contains": "Містить",
            "containsorequal": "Містить або рівні",
            "different": "Різний",
            "doesnotbeginwith": "Не починається з ",
            "doesnotcontain": "Не містить",
            "doesnotendwith": "Не закінчується на ",
            "endswith": "Закінчується на ",
            "equals": "Рівний",
            "greaterthan": "Більше ніж",
            "isnotnull": "Не пусте",
            "isnull": "Пусте",
            "lessthan": "Менше ніж "
        },
        "relations": "Зв'язки",
        "type": "Тип",
        "typeinput": "Вхідний параметр",
        "value": "Значення"
    },
    "gis": {
        "card": "Картка",
        "externalServices": "Зовнішні сервіси",
        "geographicalAttributes": "Географічні атрибути",
        "geoserverLayers": "Географічні шари",
        "layers": "Шари",
        "list": "Список",
        "mapServices": "<em>Map Services</em>",
        "root": "Корінь",
        "tree": "Дерево навігації",
        "view": "Перегляд"
    },
    "history": {
        "begindate": "Дата початку",
        "enddate": "Дата закінчення",
        "user": "Користувач"
    },
    "login": {
        "buttons": {
            "login": "Вхід",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Мова",
            "password": "Пароль",
            "tenants": "<em>Tenants</em>",
            "username": "Ім'я користувача"
        },
        "title": "Вхід"
    },
    "main": {
        "administrationmodule": "Модуль адміністрування",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Вийти",
        "managementmodule": "Модуль керування даними",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Класи",
        "custompages": "Спеціальні сторінки",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Процеси",
        "reports": "<em>Reports</em>",
        "views": "Перегляди"
    },
    "notes": {
        "edit": "Змінити замітку"
    },
    "notifier": {
        "error": "Помилка",
        "genericerror": "<em>Generic error</em>",
        "info": "Інформація",
        "success": "Успішно",
        "warning": "Попередження"
    },
    "processes": {
        "action": {
            "advance": "Поширений",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Початок",
        "workflow": "Робочі процеси"
    },
    "relationGraph": {
        "card": "Картка",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Зв'язок",
        "class": "<em>Class</em>",
        "class:": "Клас",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Відкрити граф відношень",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Зв'язок",
        "relationGraph": "Граф зв'язків"
    },
    "relations": {
        "adddetail": "Додати детальне",
        "addrelations": "Додати відношення",
        "attributes": "Атрибути",
        "code": "Код",
        "deletedetail": "Видалити детальне",
        "deleterelation": "Видалити відношення",
        "description": "Опис",
        "editcard": "Змінити картку",
        "editdetail": "Редагувати детальне",
        "editrelation": "Редагувати відношення",
        "mditems": "<em>items</em>",
        "opencard": "Відкрити пов'язану картку",
        "opendetail": "Показати детальне",
        "type": "Тип"
    },
    "widgets": {
        "customform": {
            "addrow": "Додати рядок",
            "clonerow": "Клонувати рядок",
            "deleterow": "Видалити рядок",
            "editrow": "Редагувати рядок",
            "export": "Експорт",
            "import": "Імпорт",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Застосувати вибір за замовчуванням",
            "togglefilterdisabled": "Вимкнути фільтр сітки",
            "togglefilterenabled": "Увімкнути фільтр в таблиці"
        }
    }
});