Ext.define('CMDBuildUI.locales.ua.LocalesAdministration', {
    "singleton": true,
    "localization": "ua",
    "administration": {
        "attributes": {
            "attribute": "Атрибут",
            "attributes": "Атрибути",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Активний",
                "description": "Опис",
                "domain": "Домен",
                "editortype": "Тип редактора",
                "filter": "Фільтр",
                "group": "Група",
                "help": "<em>Help</em>",
                "includeinherited": "Включити успадкування",
                "iptype": "Тип IP адреси",
                "lookup": "Довідник",
                "mandatory": "Обов'язковий",
                "maxlength": "<em>Max Length</em>",
                "mode": "Режим",
                "name": "Назва",
                "precision": "Точність",
                "preselectifunique": "Попередній вибір якщо унікальне",
                "scale": "Масштаб",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Тип",
                "unique": "Унікальний",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Будь-який",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Редагуєме",
                "editorhtml": "Html",
                "hidden": "Приховане",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Простий текст",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Лише для читання",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Активний",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Скасувати",
                "description": "Опис",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Редагувати метадані",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Обов'язковий",
                "name": "Назва",
                "save": "Зберегти",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Тип",
                "unique": "Унікальний",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Видалити",
                "disableattribute": "Вимкнути",
                "editattribute": "Редагувати",
                "enableattribute": "Увімкнути",
                "openattribute": "Відкрити",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Видалити"
                                },
                                "edit": {
                                    "tooltip": "Редагувати"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Все"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Поточний"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Статус",
                                    "values": {
                                        "active": {
                                            "label": "Статус"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Видалити"
                                },
                                "editTrigger": {
                                    "tooltip": "Редагувати"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Статус"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активний"
                                },
                                "classType": {
                                    "label": "Тип"
                                },
                                "description": {
                                    "label": "Опис"
                                },
                                "name": {
                                    "label": "Назва"
                                },
                                "parent": {
                                    "label": "Батько"
                                },
                                "superclass": {
                                    "label": "Суперклас"
                                }
                            }
                        },
                        "icon": "Іконка",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Статус"
                    },
                    "values": {
                        "active": "Активний"
                    }
                },
                "title": "Властивості",
                "toolbar": {
                    "cancelBtn": "Скасувати",
                    "closeBtn": "Закрити",
                    "deleteBtn": {
                        "tooltip": "Видалити"
                    },
                    "disableBtn": {
                        "tooltip": "Вимкнути"
                    },
                    "editBtn": {
                        "tooltip": "Змінити клас"
                    },
                    "enableBtn": {
                        "tooltip": "Увімкнути"
                    },
                    "printBtn": {
                        "printAsOdt": "Формат .odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Клас друку"
                    },
                    "saveBtn": "Зберегти"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Класи",
            "toolbar": {
                "addClassBtn": {
                    "text": "Додати клас"
                },
                "classLabel": "Клас",
                "printSchemaBtn": {
                    "text": "Схема друку"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Все",
                "cancel": "Скасувати",
                "clone": "Клонувати",
                "close": "Закрити",
                "create": "Створити",
                "delete": "Видалити",
                "disable": "Вимкнути",
                "edit": "Редагувати",
                "enable": "Увімкнути",
                "no": "Ні",
                "print": "Друк",
                "save": "Зберегти",
                "update": "Оновити",
                "yes": "Так"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Увага",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Помилка",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Завантаження...",
                "saving": "<em>Saving...</em>",
                "success": "Успішно",
                "warning": "Попередження",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Редагувати"
            }
        },
        "domains": {
            "domain": "Домен",
            "fieldlabels": {
                "destination": "Призначення",
                "enabled": "Увімкнено",
                "origin": "Походження"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Опис"
                                },
                                "descriptionDirect": {
                                    "label": "Прямий опис"
                                },
                                "descriptionInverse": {
                                    "label": "Зворотній опис"
                                },
                                "name": {
                                    "label": "Назва"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Скасувати",
                    "deleteBtn": {
                        "tooltip": "Видалити"
                    },
                    "disableBtn": {
                        "tooltip": "Вимкнути"
                    },
                    "editBtn": {
                        "tooltip": "Редагувати"
                    },
                    "enableBtn": {
                        "tooltip": "Увімкнути"
                    },
                    "saveBtn": "Зберегти"
                }
            },
            "singularTitle": "Домен",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Властивості"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Додати домен"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Активний",
                "attachments": "Вкладення",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Опис",
                "detail": "Деталі",
                "email": "Електронна пошта",
                "exportcsv": "Експортувати CSV файл",
                "filters": "<em>Filters</em>",
                "history": "Історія",
                "importcsvfile": "Імпортувати CSV файл",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Назва",
                "note": "Замітка",
                "relations": "Зв'язки",
                "type": "Тип",
                "username": "Ім'я користувача"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Обмежений адміністратор",
                "normal": "Нормальний",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Фільтр за замовчуванням",
                "defaultread": "Def. + R.",
                "description": "Опис",
                "filters": "<em>Filters</em>",
                "group": "Група",
                "name": "Назва",
                "none": "Ні",
                "permissions": "Дозволи",
                "read": "Читати",
                "uiconfig": "Конфігурація UI",
                "userslist": "<em>Users list</em>",
                "write": "Написати"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активний"
                                },
                                "name": {
                                    "label": "Назва"
                                },
                                "parent": {
                                    "label": "Батько"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Активний"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Скасувати",
                    "closeBtn": "Закрити",
                    "deleteBtn": {
                        "tooltip": "Видалити"
                    },
                    "editBtn": {
                        "tooltip": "Редагувати"
                    },
                    "saveBtn": "Зберегти"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Скасувати",
                    "closeBtn": "Закрити",
                    "deleteBtn": {
                        "tooltip": "Видалити"
                    },
                    "editBtn": {
                        "tooltip": "Редагувати"
                    },
                    "saveBtn": "Зберегти"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Меню",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Властивості типу"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Класи",
            "custompages": "Спеціальні сторінки",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Домени",
            "email": "Електронна пошта",
            "generaloptions": "Основні параметри",
            "gis": "ГІС",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Мови",
            "lookuptypes": "Типи довідника",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Процеси",
            "reports": "<em>Reports</em>",
            "searchfilters": "Фільтри пошуку",
            "servermanagement": "Управління сервером",
            "simples": "<em>Simples</em>",
            "standard": "Стандартний",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Менеджер завдань",
            "title": "Навігація",
            "users": "Користувачі",
            "views": "Перегляди",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активний"
                                },
                                "description": {
                                    "label": "Опис"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Назва"
                                },
                                "parent": {
                                    "label": "Успадковуються від"
                                },
                                "stoppableByUser": {
                                    "label": "Зупиняєме користувачем"
                                },
                                "superclass": {
                                    "label": "Суперклас"
                                }
                            }
                        },
                        "icon": "Іконка",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Статус"
                    },
                    "values": {
                        "active": "Активний"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Скасувати",
                    "closeBtn": "Закрити",
                    "deleteBtn": {
                        "tooltip": "Видалити"
                    },
                    "disableBtn": {
                        "tooltip": "Вимкнути"
                    },
                    "editBtn": {
                        "tooltip": "Редагувати"
                    },
                    "enableBtn": {
                        "tooltip": "Увімкнути"
                    },
                    "saveBtn": "Зберегти",
                    "versionBtn": {
                        "tooltip": "Версія"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Додати процес"
                },
                "printSchemaBtn": {
                    "text": "Схема друку"
                },
                "processLabel": "Робочі процеси",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активний"
                                },
                                "description": {
                                    "label": "Опис"
                                },
                                "name": {
                                    "label": "Назва"
                                },
                                "stoppableByUser": {
                                    "label": "Зупиняєме користувачем"
                                }
                            }
                        },
                        "icon": "Іконка"
                    }
                }
            },
            "title": "Користувачі",
            "toolbar": {
                "addUserBtn": {
                    "text": "Додати користувача"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});