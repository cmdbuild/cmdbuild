Ext.define('CMDBuildUI.locales.sr_RS.Locales', {
    "requires": ["CMDBuildUI.locales.sr_RS.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "sr_RS",
    "administration": CMDBuildUI.locales.sr_RS.LocalesAdministration.administration,
    "attachments": {
        "add": "Додај прилог",
        "author": "Аутор",
        "category": "Категорија",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Опис",
        "download": "Преузми",
        "editattachment": "Модификуј прилог",
        "file": "Датотека",
        "filename": "Назив датотеке",
        "majorversion": "Главна верзија",
        "modificationdate": "Датум измене",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Верзија",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Сакриј све",
                "showAll": "Прикажи све"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Видљивост"
        },
        "menu": {
            "camera": "Камера",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Померање",
            "resetView": "<em>Reset View</em>",
            "rotate": "Ротирај",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Додај картицу",
            "clone": "Клонирај",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Уклони картицу",
            "modifycard": "Измени картицу",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Додај",
            "apply": "Примени",
            "cancel": "Одустани",
            "close": "Затвори",
            "delete": "Уклони",
            "edit": "Измени",
            "execute": "<em>Execute</em>",
            "remove": "Уклони",
            "save": "Сними",
            "saveandapply": "Сними и примени",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Активност",
            "attachments": "Прилози",
            "card": "Картица",
            "details": "Детаљи",
            "emails": "<em>Emails</em>",
            "history": "Историја",
            "notes": "Напомене",
            "relations": "Релације"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Додај филтер",
        "any": "Било који",
        "attribute": "Изабери атрибут",
        "attributes": "Атрибути",
        "clearfilter": "Очисти филтер",
        "clone": "Клонирај",
        "copyof": "Копија",
        "description": "Опис",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Из селекције",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Назив",
        "newfilter": "<em>New filter</em>",
        "noone": "Ниједан",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Који почињу са",
            "between": "Између",
            "contained": "Садржан",
            "containedorequal": "Садржан или једнак",
            "contains": "Садржи",
            "containsorequal": "Садржи или је једнак",
            "different": "Различите од",
            "doesnotbeginwith": "Који не почињу са",
            "doesnotcontain": "Који не садрже",
            "doesnotendwith": "Не завршава са",
            "endswith": "Завршава са",
            "equals": "Једнаке",
            "greaterthan": "Веће",
            "isnotnull": "Не може бити null",
            "isnull": "Са null вредношћу",
            "lessthan": "Мање"
        },
        "relations": "Релације",
        "type": "Тип",
        "typeinput": "Улазни параметар",
        "value": "Вредности"
    },
    "gis": {
        "card": "Картица",
        "externalServices": "Спољни сервиси",
        "geographicalAttributes": "Географски атрибути",
        "geoserverLayers": "Geoserver слојеви",
        "layers": "Слојеви",
        "list": "Листа",
        "mapServices": "<em>Map Services</em>",
        "root": "Корен",
        "tree": "Стабло навигације",
        "view": "Приказ"
    },
    "history": {
        "begindate": "Датум почетка",
        "enddate": "Датум завршетка",
        "user": "Корисник"
    },
    "login": {
        "buttons": {
            "login": "Пријави се",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Језик",
            "password": "Лозинка",
            "tenants": "<em>Tenants</em>",
            "username": "Корисничко име"
        },
        "title": "Пријави се"
    },
    "main": {
        "administrationmodule": "Администрациони модул",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Изађи",
        "managementmodule": "Модул за управљање подацима",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Класе",
        "custompages": "Посебне странице",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Картице процеса",
        "reports": "<em>Reports</em>",
        "views": "Прикази"
    },
    "notes": {
        "edit": "Измени напомену"
    },
    "notifier": {
        "error": "Грешка",
        "genericerror": "<em>Generic error</em>",
        "info": "Информација",
        "success": "Успех",
        "warning": "Пажња"
    },
    "processes": {
        "action": {
            "advance": "Даље",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Старт",
        "workflow": "Радни процеси"
    },
    "relationGraph": {
        "card": "Картица",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Веза",
        "class": "<em>Class</em>",
        "class:": "Класа",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Отвори граф релација",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Веза",
        "relationGraph": "Граф релација"
    },
    "relations": {
        "adddetail": "Додај детаље",
        "addrelations": "Додај релацију",
        "attributes": "Атрибути",
        "code": "Код",
        "deletedetail": "Уклони детаљ",
        "deleterelation": "Уклони релацију",
        "description": "Опис",
        "editcard": "Измени картицу",
        "editdetail": "Измени детаље",
        "editrelation": "Измени релацију",
        "mditems": "<em>items</em>",
        "opencard": "Отвори припадајућу картицу ",
        "opendetail": "Прикажи детаље",
        "type": "Тип"
    },
    "widgets": {
        "customform": {
            "addrow": "Додај ред",
            "clonerow": "Клонирај ред",
            "deleterow": "Обриши ред",
            "editrow": "Измени ред",
            "export": "Извези",
            "import": "Увези",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Примени подразумевани избор",
            "togglefilterdisabled": "Искључи филтрирање табеле",
            "togglefilterenabled": "Укључи филтрирање табеле"
        }
    }
});