Ext.define('CMDBuildUI.locales.sr_RS.LocalesAdministration', {
    "singleton": true,
    "localization": "sr_RS",
    "administration": {
        "attributes": {
            "attribute": "Атрибут",
            "attributes": "Атрибути",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Активна",
                "description": "Опис",
                "domain": "Релација",
                "editortype": "Врста едитора",
                "filter": "Филтер",
                "group": "Група",
                "help": "<em>Help</em>",
                "includeinherited": "Укључујући наслеђене",
                "iptype": "IP тип",
                "lookup": "Шифарник",
                "mandatory": "Обавезан",
                "maxlength": "<em>Max Length</em>",
                "mode": "Режим",
                "name": "Назив",
                "precision": "Прецизност",
                "preselectifunique": "Селектуј ако је јединствен",
                "scale": "Скала",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Тип",
                "unique": "Јединствен",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Било који",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Дозвољене измене",
                "editorhtml": "Html",
                "hidden": "Скривен",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Обични текст",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Само за читање",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Активан",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Одустани",
                "description": "Опис",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Измени метаподатке",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Обавезан",
                "name": "Назив",
                "save": "Сними",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Тип",
                "unique": "Јединствен",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Уклони",
                "disableattribute": "Онемогући",
                "editattribute": "Измени",
                "enableattribute": "Омогући",
                "openattribute": "Активан",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Уклони"
                                },
                                "edit": {
                                    "tooltip": "Измени"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Све"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Тренутни"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Статус",
                                    "values": {
                                        "active": {
                                            "label": "Статус"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Уклони"
                                },
                                "editTrigger": {
                                    "tooltip": "Измени"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Статус"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активна"
                                },
                                "classType": {
                                    "label": "Тип"
                                },
                                "description": {
                                    "label": "Опис"
                                },
                                "name": {
                                    "label": "Назив"
                                },
                                "parent": {
                                    "label": "Родитељ"
                                },
                                "superclass": {
                                    "label": "Суперкласа"
                                }
                            }
                        },
                        "icon": "Икона",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Статус"
                    },
                    "values": {
                        "active": "Активна"
                    }
                },
                "title": "Обележја",
                "toolbar": {
                    "cancelBtn": "Одустани",
                    "closeBtn": "Затвори",
                    "deleteBtn": {
                        "tooltip": "Уклони"
                    },
                    "disableBtn": {
                        "tooltip": "Онемогући"
                    },
                    "editBtn": {
                        "tooltip": "Измени класу"
                    },
                    "enableBtn": {
                        "tooltip": "Омогући"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Одштампај класу"
                    },
                    "saveBtn": "Сними"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Класе",
            "toolbar": {
                "addClassBtn": {
                    "text": "Додај класу"
                },
                "classLabel": "Класа",
                "printSchemaBtn": {
                    "text": "Одштампај шему"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Све",
                "cancel": "Одустани",
                "clone": "Клонирај",
                "close": "Затвори",
                "create": "Креирај",
                "delete": "Уклони",
                "disable": "Онемогући",
                "edit": "Измени",
                "enable": "Омогући",
                "no": "Не",
                "print": "Штампај",
                "save": "Сними",
                "update": "Ажурирај",
                "yes": "Да"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Пажња",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Грешка",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Учитавање у току...",
                "saving": "<em>Saving...</em>",
                "success": "Успех",
                "warning": "Пажња",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Измени"
            }
        },
        "domains": {
            "domain": "Релација",
            "fieldlabels": {
                "destination": "Одредиште",
                "enabled": "Укључен",
                "origin": "Полазна класа"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Опис"
                                },
                                "descriptionDirect": {
                                    "label": "Директни опис"
                                },
                                "descriptionInverse": {
                                    "label": "Инверзни опис"
                                },
                                "name": {
                                    "label": "Назив"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Одустани",
                    "deleteBtn": {
                        "tooltip": "Уклони"
                    },
                    "disableBtn": {
                        "tooltip": "Онемогући"
                    },
                    "editBtn": {
                        "tooltip": "Измени"
                    },
                    "enableBtn": {
                        "tooltip": "Омогући"
                    },
                    "saveBtn": "Сними"
                }
            },
            "singularTitle": "Релација",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Обележја"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Додај релацију"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Активан",
                "attachments": "Прилози",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Опис",
                "detail": "Детаљи",
                "email": "E-пошта",
                "exportcsv": "Извези CSV датотеку",
                "filters": "<em>Filters</em>",
                "history": "Историја",
                "importcsvfile": "Увези CSV датотеку",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Назив",
                "note": "Напомена",
                "relations": "Релације",
                "type": "Тип",
                "username": "Корисничко име"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Администратор с ограниченим правима",
                "normal": "Нормално",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Подразумевани филтер",
                "defaultread": "Def. + R.",
                "description": "Опис",
                "filters": "<em>Filters</em>",
                "group": "Група",
                "name": "Назив",
                "none": "Без икаквих права",
                "permissions": "Права",
                "read": "Читање",
                "uiconfig": "Конфигурација ИО",
                "userslist": "<em>Users list</em>",
                "write": "Писање"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активан"
                                },
                                "name": {
                                    "label": "Назив"
                                },
                                "parent": {
                                    "label": "Родитељ"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Активан"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Одустани",
                    "closeBtn": "Затвори",
                    "deleteBtn": {
                        "tooltip": "Уклони"
                    },
                    "editBtn": {
                        "tooltip": "Измени"
                    },
                    "saveBtn": "Сними"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Одустани",
                    "closeBtn": "Затвори",
                    "deleteBtn": {
                        "tooltip": "Уклони"
                    },
                    "editBtn": {
                        "tooltip": "Измени"
                    },
                    "saveBtn": "Сними"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Мени",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Обележја типа"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Класе",
            "custompages": "Посебне странице",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Релације",
            "email": "E-пошта",
            "generaloptions": "Општа подешавања",
            "gis": "ГИС",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Језици",
            "lookuptypes": "Тип у шифарнику",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Картице процеса",
            "reports": "<em>Reports</em>",
            "searchfilters": "Филтери за претраживање",
            "servermanagement": "Подешавање сервера",
            "simples": "<em>Simples</em>",
            "standard": "Стандардна",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Управљање задацима",
            "title": "Навигација",
            "users": "Корисници",
            "views": "Прикази",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активан"
                                },
                                "description": {
                                    "label": "Опис"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Назив"
                                },
                                "parent": {
                                    "label": "Наследник"
                                },
                                "stoppableByUser": {
                                    "label": "Корисник може прекинути процес"
                                },
                                "superclass": {
                                    "label": "Суперкласа"
                                }
                            }
                        },
                        "icon": "Икона",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Статус"
                    },
                    "values": {
                        "active": "Активан"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Одустани",
                    "closeBtn": "Затвори",
                    "deleteBtn": {
                        "tooltip": "Уклони"
                    },
                    "disableBtn": {
                        "tooltip": "Онемогући"
                    },
                    "editBtn": {
                        "tooltip": "Измени"
                    },
                    "enableBtn": {
                        "tooltip": "Омогући"
                    },
                    "saveBtn": "Сними",
                    "versionBtn": {
                        "tooltip": "Верзија"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Додај процес"
                },
                "printSchemaBtn": {
                    "text": "Одштампај шему"
                },
                "processLabel": "Радни процеси",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активан"
                                },
                                "description": {
                                    "label": "Опис"
                                },
                                "name": {
                                    "label": "Назив"
                                },
                                "stoppableByUser": {
                                    "label": "Корисник може прекинути процес"
                                }
                            }
                        },
                        "icon": "Икона"
                    }
                }
            },
            "title": "Корисници",
            "toolbar": {
                "addUserBtn": {
                    "text": "Додај корисника"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});