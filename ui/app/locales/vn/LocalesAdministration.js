Ext.define('CMDBuildUI.locales.vn.LocalesAdministration', {
    "singleton": true,
    "localization": "vn",
    "administration": {
        "attributes": {
            "attribute": "<em>Attribute</em>",
            "attributes": "Các thuộc tính",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Đang sử dụng",
                "description": "Mô tả",
                "domain": "Tên miền",
                "editortype": " Trình soạn thảo thường",
                "filter": "Bộ lọc",
                "group": "Nhóm",
                "help": "<em>Help</em>",
                "includeinherited": "Bao gồm các kế thừa",
                "iptype": "<em>IP type</em>",
                "lookup": "Tra cứu",
                "mandatory": "Bắt buộc",
                "maxlength": "<em>Max Length</em>",
                "mode": "<em>Mode</em>",
                "name": "Tên",
                "precision": "Ðộ chính xác",
                "preselectifunique": "<em>Preselect if unique</em>",
                "scale": "Thang chia",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Kiểu",
                "unique": "Duy nhất",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Tùy chọn",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Có thể chỉnh sửa",
                "editorhtml": "Html",
                "hidden": "Ẩn",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Văn bản đơn giản",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Chỉ đọc",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Hoạt động",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Hủy bỏ",
                "description": "Mô tả",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Sửa mô tả dữ liệu",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Bắt buộc",
                "name": "Tên",
                "save": "Lưu",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Kiểu",
                "unique": "Duy nhất",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "<em>Delete</em>",
                "disableattribute": "<em>Disable</em>",
                "editattribute": "<em>Edit</em>",
                "enableattribute": "<em>Enable</em>",
                "openattribute": "Mở",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "<em>Delete</em>"
                                },
                                "edit": {
                                    "tooltip": "<em>Edit</em>"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "<em>All</em>"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "<em>Current</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "<em>Status</em>",
                                    "values": {
                                        "active": {
                                            "label": "<em>Status</em>"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "<em>Delete</em>"
                                },
                                "editTrigger": {
                                    "tooltip": "<em>Edit</em>"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "<em>Status</em>"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Đang sử dụng"
                                },
                                "classType": {
                                    "label": "Loại"
                                },
                                "description": {
                                    "label": "Mô tả"
                                },
                                "name": {
                                    "label": "Tên"
                                },
                                "parent": {
                                    "label": "Trước đó"
                                },
                                "superclass": {
                                    "label": "Cao cấp"
                                }
                            }
                        },
                        "icon": "Biểu tượng",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "<em>Status</em>"
                    },
                    "values": {
                        "active": "Đang sử dụng"
                    }
                },
                "title": "Đặc tính",
                "toolbar": {
                    "cancelBtn": "Hủy bỏ",
                    "closeBtn": "Đóng",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "Chỉnh sửa Lớp"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "printBtn": {
                        "printAsOdt": "<em>OpenOffice Odt</em>",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "In Lớp"
                    },
                    "saveBtn": "Lưu"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Các lớp",
            "toolbar": {
                "addClassBtn": {
                    "text": "Thêm Lớp mới"
                },
                "classLabel": "<em>Class</em>",
                "printSchemaBtn": {
                    "text": "In lược đồ"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "<em>All</em>",
                "cancel": "Hủy bỏ",
                "clone": "Sao chép",
                "close": "Đóng",
                "create": "<em>Create</em>",
                "delete": "<em>Delete</em>",
                "disable": "<em>Disable</em>",
                "edit": "<em>Edit</em>",
                "enable": "<em>Enable</em>",
                "no": "<em>No</em>",
                "print": "In",
                "save": "Lưu",
                "update": "Cập nhật",
                "yes": "<em>Yes</em>"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Chú ý",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Lỗi",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Đang tải...",
                "saving": "<em>Saving...</em>",
                "success": "Thành công",
                "warning": "Cảnh báo",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "<em>Edit</em>"
            }
        },
        "domains": {
            "domain": "Tên miền",
            "fieldlabels": {
                "destination": "Ðiểm đến",
                "enabled": " Kích hoạt",
                "origin": "Ðiểm bắt đầu"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Mô tả"
                                },
                                "descriptionDirect": {
                                    "label": "Mô tả trực tiếp"
                                },
                                "descriptionInverse": {
                                    "label": "Mô tả ngược"
                                },
                                "name": {
                                    "label": "Tên"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Hủy bỏ",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "saveBtn": "Lưu"
                }
            },
            "singularTitle": "Tên miền",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Đặc tính"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Thêm tên miền"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Hoạt động",
                "attachments": "Các tập tin đính kèm",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Mô tả",
                "detail": "Chi tiết",
                "email": "<em>E-mail</em>",
                "exportcsv": "Xuất tập tin CSV",
                "filters": "<em>Filters</em>",
                "history": "Lich su",
                "importcsvfile": "Nhập tập tin CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Tên",
                "note": "Ghi chú",
                "relations": "Các mối quan hệ",
                "type": "Kiểu",
                "username": "<em>Username</em>"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Người quản trị cấp dưới",
                "normal": "Thông thường",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "<em>Default filters</em>",
                "defaultread": "<em>Def. + R.</em>",
                "description": "Mô tả",
                "filters": "<em>Filters</em>",
                "group": "Nhóm",
                "name": "Tên",
                "none": "Không có",
                "permissions": "Quyền truy cập",
                "read": "Đọc",
                "uiconfig": "Cấu hình UI",
                "userslist": "<em>Users list</em>",
                "write": "Viết"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Hoạt động"
                                },
                                "name": {
                                    "label": "Tên"
                                },
                                "parent": {
                                    "label": "Trước đó"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Hoạt động"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Hủy bỏ",
                    "closeBtn": "Đóng",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "saveBtn": "Lưu"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Hủy bỏ",
                    "closeBtn": "Đóng",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "saveBtn": "Lưu"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menu",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Các loại đặc tính"
            }
        },
        "navigation": {
            "bim": "<em>BIM</em>",
            "classes": "Các lớp",
            "custompages": "<em>Custom pages</em>",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Tên miền",
            "email": "<em>E-mail</em>",
            "generaloptions": "<em>General options</em>",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "<em>Languages</em>",
            "lookuptypes": "Loại tra cứu",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Các quy trình",
            "reports": "<em>Reports</em>",
            "searchfilters": "Tìm kiềm các bộ lọc",
            "servermanagement": " Quản lý máy chủ ",
            "simples": "<em>Simples</em>",
            "standard": "Tiêu chuẩn",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "<em>Task manager</em>",
            "title": "Hướng",
            "users": "Người dùng",
            "views": "Lượt xem",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Hoạt động"
                                },
                                "description": {
                                    "label": "Mô tả"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Tên"
                                },
                                "parent": {
                                    "label": "Kế thừa từ"
                                },
                                "stoppableByUser": {
                                    "label": "Người dùng có thể ngưng sử dụng"
                                },
                                "superclass": {
                                    "label": "Cao cấp"
                                }
                            }
                        },
                        "icon": "Biểu tượng",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "<em>Status</em>"
                    },
                    "values": {
                        "active": "Hoạt động"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Hủy bỏ",
                    "closeBtn": "Đóng",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "saveBtn": "Lưu",
                    "versionBtn": {
                        "tooltip": "Phiên bản"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Thêm quy trình"
                },
                "printSchemaBtn": {
                    "text": "In lược đồ"
                },
                "processLabel": "<em>Workflow</em>",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Hoạt động"
                                },
                                "description": {
                                    "label": "Mô tả"
                                },
                                "name": {
                                    "label": "Tên"
                                },
                                "stoppableByUser": {
                                    "label": "Người dùng có thể ngưng sử dụng"
                                }
                            }
                        },
                        "icon": "Biểu tượng"
                    }
                }
            },
            "title": "Người dùng",
            "toolbar": {
                "addUserBtn": {
                    "text": "Thêm người dùng"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});