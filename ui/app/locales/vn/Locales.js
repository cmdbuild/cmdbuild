Ext.define('CMDBuildUI.locales.vn.Locales', {
    "requires": ["CMDBuildUI.locales.vn.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "vn",
    "administration": CMDBuildUI.locales.vn.LocalesAdministration.administration,
    "attachments": {
        "add": "Thêm tập tin đính kèm",
        "author": "Người cài đặt",
        "category": "<em>Category</em>",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Mô tả",
        "download": "<em>Download</em>",
        "editattachment": "<em>Modifica allegato</em>",
        "file": "Tập tin",
        "filename": "<em>File name</em>",
        "majorversion": "<em>Major version</em>",
        "modificationdate": "sửa đổi ngày thang",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Phiên bản",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Hiển thị"
        },
        "menu": {
            "camera": "<em>Camera</em>",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "<em>Pan</em>",
            "resetView": "<em>Reset View</em>",
            "rotate": "<em>Rotate</em>",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Thêm thẻ",
            "clone": "Sao chép",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Xóa thẻ",
            "modifycard": "Sửa đổi thẻ",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Thêm",
            "apply": "Áp dụng",
            "cancel": "Hủy bỏ",
            "close": "Đóng",
            "delete": "<em>Delete</em>",
            "edit": "<em>Edit</em>",
            "execute": "<em>Execute</em>",
            "remove": "Gỡ bỏ",
            "save": "Lưu",
            "saveandapply": "Lưu và áp dụng",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Hoạt động",
            "attachments": "Các tập tin đính kèm",
            "card": "Thẻ",
            "details": "Chi tiết",
            "emails": "<em>Emails</em>",
            "history": "Lich su",
            "notes": "Các ghi chú",
            "relations": "Các mối quan hệ"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Thêm bộ lọc",
        "any": "Tùy chọn",
        "attribute": "Chọn một thuộc tính",
        "attributes": "Các thuộc tính",
        "clearfilter": "Làm sạch bộ lọc",
        "clone": "Sao chép",
        "copyof": "Sao chép của",
        "description": "Mô tả",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Lựa chọn từ",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Tên",
        "newfilter": "<em>New filter</em>",
        "noone": "Không có",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Bắt đầu với",
            "between": "Ở giữa",
            "contained": "<em>Contained</em>",
            "containedorequal": "<em>Contained or equal</em>",
            "contains": "Bao gồm",
            "containsorequal": "<em>Contains or equal</em>",
            "different": "Khác biệt",
            "doesnotbeginwith": "Không bắt đầu với",
            "doesnotcontain": "Koji ne sadrže",
            "doesnotendwith": "Không kết thúc tại",
            "endswith": "Kết thúc tại",
            "equals": "Cân bằng",
            "greaterthan": "Nhiều hơn",
            "isnotnull": "Co phai no vo hieu luc",
            "isnull": "No vo hieu luc",
            "lessthan": "Ít hơn"
        },
        "relations": "Các mối quan hệ",
        "type": "Kiểu",
        "typeinput": "Thông số đầu vào",
        "value": "<em>Value</em>"
    },
    "gis": {
        "card": "Thẻ",
        "externalServices": "Các dịch vụ ngoài",
        "geographicalAttributes": "Thuộc tính địa lí",
        "geoserverLayers": "Các lớp Geoserver",
        "layers": "<em>Layers</em>",
        "list": "Danh sách",
        "mapServices": "<em>Map Services</em>",
        "root": "<em>Root</em>",
        "tree": "<em>Navigation tree</em>",
        "view": "<em>View</em>"
    },
    "history": {
        "begindate": "Ngày bắt đầu",
        "enddate": "Ngày kết thúc",
        "user": "Người dùng"
    },
    "login": {
        "buttons": {
            "login": "Đăng nhập",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "<em>Language</em>",
            "password": "<em>Password</em>",
            "tenants": "<em>Tenants</em>",
            "username": "<em>Username</em>"
        },
        "title": "Đăng nhập"
    },
    "main": {
        "administrationmodule": "Mô-đun Quản lý",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Đăng xuất",
        "managementmodule": "Danh muc quản lí dữ liệu",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Các lớp",
        "custompages": "<em>Custom pages</em>",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Các quy trình",
        "reports": "<em>Reports</em>",
        "views": "Lượt xem"
    },
    "notes": {
        "edit": "Chỉnh sửa ghi chú"
    },
    "notifier": {
        "error": "Lỗi",
        "genericerror": "<em>Generic error</em>",
        "info": "Thông tin",
        "success": "Thành công",
        "warning": "Cảnh báo"
    },
    "processes": {
        "action": {
            "advance": "Thành quả",
            "label": "<em>Action</em>"
        },
        "startworkflow": "<em>Start</em>",
        "workflow": "<em>Workflow</em>"
    },
    "relationGraph": {
        "card": "Thẻ",
        "cardList": "<em>Card List</em>",
        "cardRelation": "<em>Relation</em>",
        "class": "<em>Class</em>",
        "class:": "<em>Class</em>",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Mở rộng đồ thị mối quan hệ",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "<em>Relation</em>",
        "relationGraph": "Đồ thị mối quan hệ"
    },
    "relations": {
        "adddetail": "Thêm chi tiết",
        "addrelations": "Thêm các mối quan hệ",
        "attributes": "Các thuộc tính",
        "code": "Mã",
        "deletedetail": "Xóa chi tiết",
        "deleterelation": "Xóa mối quan hệ",
        "description": "Mô tả",
        "editcard": "Sửa đổi thẻ",
        "editdetail": "Chỉnh sửa chi tiết",
        "editrelation": "Chỉnh sửa mối quan hệ",
        "mditems": "<em>items</em>",
        "opencard": "Mở thẻ liên quan",
        "opendetail": "Hiện thị chi tiết",
        "type": "Kiểu"
    },
    "widgets": {
        "customform": {
            "addrow": "<em>Add row</em>",
            "clonerow": "<em>Clone row</em>",
            "deleterow": "<em>Delete row</em>",
            "editrow": "<em>Edit row</em>",
            "export": "Xuất",
            "import": "<em>Import</em>",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "<em>Apply default selection</em>",
            "togglefilterdisabled": "<em>Disable grid filter</em>",
            "togglefilterenabled": "<em>Enable grid filter</em>"
        }
    }
});