Ext.define('CMDBuildUI.locales.pt_PT.Locales', {
    "requires": ["CMDBuildUI.locales.pt_PT.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "pt_PT",
    "administration": CMDBuildUI.locales.pt_PT.LocalesAdministration.administration,
    "attachments": {
        "add": "Adicionar anexo",
        "author": "Autor",
        "category": "Categoria",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Descrição",
        "download": "Descarregar",
        "editattachment": "<em>Modifica allegato</em>",
        "file": "Ficheiro",
        "filename": "Nome do ficheiro",
        "majorversion": "<em>Major version</em>",
        "modificationdate": "Data de modificação",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Versão",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Visibilidade"
        },
        "menu": {
            "camera": "Câmara",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Deslocar",
            "resetView": "<em>Reset View</em>",
            "rotate": "Rodar",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Adicionar cartão",
            "clone": "Copiar",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Remover cartão",
            "modifycard": "Modificar cartão",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Adicionar",
            "apply": "Aplicar",
            "cancel": "Cancelar",
            "close": "Fechar",
            "delete": "Remover",
            "edit": "Editar",
            "execute": "<em>Execute</em>",
            "remove": "Remover",
            "save": "Gravar",
            "saveandapply": "Guardar e aplicar",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Atividade",
            "attachments": "Anexos",
            "card": "Cartão",
            "details": "Detalhes",
            "emails": "<em>Emails</em>",
            "history": "Histórico",
            "notes": "Notes",
            "relations": "Relações"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Adicionar filtro",
        "any": "Qualquer",
        "attribute": "Escolher um atributo",
        "attributes": "Atributos",
        "clearfilter": "Limpar filtro",
        "clone": "Copiar",
        "copyof": "Cópia de",
        "description": "Descrição",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "<em>From selection</em>",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Nome",
        "newfilter": "<em>New filter</em>",
        "noone": "Ninguém",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Inicia com",
            "between": "Entre",
            "contained": "Contido",
            "containedorequal": "Contido ou igual",
            "contains": "Contêm",
            "containsorequal": "Contêm ou igual",
            "different": "Diferente",
            "doesnotbeginwith": "Não inicia com",
            "doesnotcontain": "Não contém",
            "doesnotendwith": "Não finaliza com",
            "endswith": "Finaliza com",
            "equals": "É igual a",
            "greaterthan": "Maior do que",
            "isnotnull": "Não é nulo",
            "isnull": "É nulo",
            "lessthan": "Menor do que"
        },
        "relations": "Relações",
        "type": "Tipo",
        "typeinput": "Parâmetro de entrada",
        "value": "Valor"
    },
    "gis": {
        "card": "Cartão",
        "externalServices": "Serviços externos",
        "geographicalAttributes": "Atributos geográficos",
        "geoserverLayers": "Camadas Geoserver",
        "layers": "Camadas",
        "list": "Lista",
        "mapServices": "<em>Map Services</em>",
        "root": "Raíz",
        "tree": "Árvore de navegação",
        "view": "Vista"
    },
    "history": {
        "begindate": "Data de início",
        "enddate": "Data de fim",
        "user": "Utilizador"
    },
    "login": {
        "buttons": {
            "login": "Login",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Idioma",
            "password": "Password",
            "tenants": "<em>Tenants</em>",
            "username": "Nome de utilizador"
        },
        "title": "Login"
    },
    "main": {
        "administrationmodule": "Módulo de Administração",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Sair",
        "managementmodule": "Módulo de Gestão de Dados",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Classes",
        "custompages": "Páginas personalizadas",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Processos",
        "reports": "<em>Reports</em>",
        "views": "Visualizações"
    },
    "notes": {
        "edit": "Editar nota"
    },
    "notifier": {
        "error": "Erro",
        "genericerror": "<em>Generic error</em>",
        "info": "Info",
        "success": "Successo",
        "warning": "Aviso"
    },
    "processes": {
        "action": {
            "advance": "Avançar",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Iniciar",
        "workflow": "Processo"
    },
    "relationGraph": {
        "card": "Cartão",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Relação",
        "class": "<em>Class</em>",
        "class:": "Classe",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Abrir grafo de relacionamentos",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Relação",
        "relationGraph": "Gráfico de relacionamento"
    },
    "relations": {
        "adddetail": "Adiconar detalhe",
        "addrelations": "Adicionar relacionamentos",
        "attributes": "Atributos",
        "code": "Código",
        "deletedetail": "Remover detalhe",
        "deleterelation": "Remover relacionamento",
        "description": "Descrição",
        "editcard": "Modificar cartão",
        "editdetail": "Editar Detalhe",
        "editrelation": "Editar relacionamento",
        "mditems": "<em>items</em>",
        "opencard": "Abrir cartão relacionado",
        "opendetail": "Visualizar detalhe",
        "type": "Tipo"
    },
    "widgets": {
        "customform": {
            "addrow": "Adicionar linha",
            "clonerow": "Copiar linha",
            "deleterow": "Remover linha",
            "editrow": "Editar linha",
            "export": "Exportar",
            "import": "Importar",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Aplicação seleção padrão",
            "togglefilterdisabled": "Desativar filtro de grelha",
            "togglefilterenabled": "Ativar filtro de grelha"
        }
    }
});