Ext.define('CMDBuildUI.locales.pt_PT.LocalesAdministration', {
    "singleton": true,
    "localization": "pt_PT",
    "administration": {
        "attributes": {
            "attribute": "Atributo",
            "attributes": "Atributos",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Ativo",
                "description": "Descrição",
                "domain": "Domínio",
                "editortype": "Tipo do Editor",
                "filter": "Filtro",
                "group": "Grupo",
                "help": "<em>Help</em>",
                "includeinherited": "Herança incluida",
                "iptype": "Tipo de IP",
                "lookup": "Lista de Valores",
                "mandatory": "Obrigatório",
                "maxlength": "<em>Max Length</em>",
                "mode": "Modo",
                "name": "Nome",
                "precision": "Precisão",
                "preselectifunique": "Pré-selecionar se for único",
                "scale": "Escala",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tipo",
                "unique": "Único",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Qualquer",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Editável",
                "editorhtml": "Html",
                "hidden": "Oculto",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Texto sem formatação",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Apenas leitura",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Ativo",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Cancelar",
                "description": "Descrição",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Editar metadados",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obrigatório",
                "name": "Nome",
                "save": "Gravar",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tipo",
                "unique": "Único",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Remover",
                "disableattribute": "Desativar",
                "editattribute": "Editar",
                "enableattribute": "Ativar",
                "openattribute": "Abrir",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Remover"
                                },
                                "edit": {
                                    "tooltip": "Editar"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Todos"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Atual"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Estado",
                                    "values": {
                                        "active": {
                                            "label": "Estado"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Remover"
                                },
                                "editTrigger": {
                                    "tooltip": "Editar"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Estado"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "classType": {
                                    "label": "Tipo"
                                },
                                "description": {
                                    "label": "Descrição"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Pai"
                                },
                                "superclass": {
                                    "label": "Superclasse"
                                }
                            }
                        },
                        "icon": "Ícone",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Estado"
                    },
                    "values": {
                        "active": "Ativo"
                    }
                },
                "title": "Propriedades",
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "Remover"
                    },
                    "disableBtn": {
                        "tooltip": "Desativar"
                    },
                    "editBtn": {
                        "tooltip": "Alterar classe"
                    },
                    "enableBtn": {
                        "tooltip": "Ativar"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice ODT",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Imprimir classe"
                    },
                    "saveBtn": "Gravar"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Classes",
            "toolbar": {
                "addClassBtn": {
                    "text": "Adicionar classe"
                },
                "classLabel": "Classe",
                "printSchemaBtn": {
                    "text": "Imprimir schema"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Todos",
                "cancel": "Cancelar",
                "clone": "Copiar",
                "close": "Fechar",
                "create": "Criar",
                "delete": "Remover",
                "disable": "Desativar",
                "edit": "Editar",
                "enable": "Ativar",
                "no": "<em>No</em>",
                "print": "Imprimir",
                "save": "Gravar",
                "update": "Atualizar",
                "yes": "<em>Yes</em>"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Atenção",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Erro",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Por favor aguarde...",
                "saving": "<em>Saving...</em>",
                "success": "Successo",
                "warning": "Aviso",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Editar"
            }
        },
        "domains": {
            "domain": "Domínio",
            "fieldlabels": {
                "destination": "Destino",
                "enabled": "Ativo",
                "origin": "Origem"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Descrição"
                                },
                                "descriptionDirect": {
                                    "label": "Descrição direta"
                                },
                                "descriptionInverse": {
                                    "label": "Descrição inversa"
                                },
                                "name": {
                                    "label": "Nome"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "deleteBtn": {
                        "tooltip": "Remover"
                    },
                    "disableBtn": {
                        "tooltip": "Desativar"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "enableBtn": {
                        "tooltip": "Ativar"
                    },
                    "saveBtn": "Gravar"
                }
            },
            "singularTitle": "Domínio",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Propriedades"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Adicionar domínio"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Ativo",
                "attachments": "Anexos",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Descrição",
                "detail": "Detalhe",
                "email": "Email",
                "exportcsv": "Exportar ficheiro CSV",
                "filters": "<em>Filters</em>",
                "history": "Histórico",
                "importcsvfile": "Importar ficheiro CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Nome",
                "note": "Nota",
                "relations": "Relações",
                "type": "Tipo",
                "username": "Nome de utilizador"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Administrador limitado",
                "normal": "Normal",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Filtros padrão",
                "defaultread": "Def. + R.",
                "description": "Descrição",
                "filters": "<em>Filters</em>",
                "group": "Grupo",
                "name": "Nome",
                "none": "Nenhum",
                "permissions": "Permissões",
                "read": "Leitura",
                "uiconfig": "Configuração Interface",
                "userslist": "<em>Users list</em>",
                "write": "Escrita"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Pai"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Ativo"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "Remover"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "saveBtn": "Gravar"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "Remover"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "saveBtn": "Gravar"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menu",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Propriedades do Tipo"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Classes",
            "custompages": "Páginas personalizadas",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domínios",
            "email": "Email",
            "generaloptions": "Opções gerais",
            "gis": "SIG",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Idiomas",
            "lookuptypes": "Tipos de listas de valores",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Processos",
            "reports": "<em>Reports</em>",
            "searchfilters": "Filtros de procura",
            "servermanagement": "Gestão do Servidor",
            "simples": "<em>Simples</em>",
            "standard": "Standard",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Gestor de tarefas",
            "title": "Navegação",
            "users": "Utilizadores",
            "views": "Visualizações",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "description": {
                                    "label": "Descrição"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Herda de"
                                },
                                "stoppableByUser": {
                                    "label": "Interrupção pelo utilizador"
                                },
                                "superclass": {
                                    "label": "Superclasse"
                                }
                            }
                        },
                        "icon": "Ícone",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Estado"
                    },
                    "values": {
                        "active": "Ativo"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Cancelar",
                    "closeBtn": "Fechar",
                    "deleteBtn": {
                        "tooltip": "Remover"
                    },
                    "disableBtn": {
                        "tooltip": "Desativar"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "enableBtn": {
                        "tooltip": "Ativar"
                    },
                    "saveBtn": "Gravar",
                    "versionBtn": {
                        "tooltip": "Versão"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Adicionar processo"
                },
                "printSchemaBtn": {
                    "text": "Imprimir schema"
                },
                "processLabel": "Processo",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ativo"
                                },
                                "description": {
                                    "label": "Descrição"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "stoppableByUser": {
                                    "label": "Interrupção pelo utilizador"
                                }
                            }
                        },
                        "icon": "Ícone"
                    }
                }
            },
            "title": "Utilizadores",
            "toolbar": {
                "addUserBtn": {
                    "text": "Adicionar utilizador"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});