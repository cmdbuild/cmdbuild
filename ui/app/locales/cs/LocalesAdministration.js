Ext.define('CMDBuildUI.locales.cs.LocalesAdministration', {
    "singleton": true,
    "localization": "cs",
    "administration": {
        "attributes": {
            "attribute": "Atribut",
            "attributes": "Atributy",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktivní",
                "description": "Popis",
                "domain": "Doména",
                "editortype": "Typ editoru",
                "filter": "Filtr",
                "group": "Skupina",
                "help": "<em>Help</em>",
                "includeinherited": "Včetně zděděných",
                "iptype": "Typ IP adresy",
                "lookup": "Rozbalovací seznam",
                "mandatory": "Povinný",
                "maxlength": "<em>Max Length</em>",
                "mode": "Mód",
                "name": "Název",
                "precision": "Počet číslic celkem",
                "preselectifunique": "Přednaplnit, pokud je unikátní",
                "scale": "Počet desetinných míst",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Typ",
                "unique": "Unikátní",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Jakýkoliv",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Pro editaci",
                "editorhtml": "HTML",
                "hidden": "Skrytý",
                "immutable": "<em>Immutable</em>",
                "ipv4": "IPv4",
                "ipv6": "IPv6",
                "plaintext": "Prostý text",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Pro čtení",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Aktivní",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Storno",
                "description": "Popis",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Proměnné",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Povinný",
                "name": "Název",
                "save": "Uložit",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Typ",
                "unique": "Unikátní",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Odstranit",
                "disableattribute": "Zakázat",
                "editattribute": "Upravit",
                "enableattribute": "Povolit",
                "openattribute": "Spuštěné",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Odstranit"
                                },
                                "edit": {
                                    "tooltip": "Upravit"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Všechny"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Aktuální"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Stav",
                                    "values": {
                                        "active": {
                                            "label": "Stav"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Odstranit"
                                },
                                "editTrigger": {
                                    "tooltip": "Upravit"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Stav"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivní"
                                },
                                "classType": {
                                    "label": "Typ"
                                },
                                "description": {
                                    "label": "Popis"
                                },
                                "name": {
                                    "label": "Název"
                                },
                                "parent": {
                                    "label": "Nadřazený seznam"
                                },
                                "superclass": {
                                    "label": "Nadtřída"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Stav"
                    },
                    "values": {
                        "active": "Aktivní"
                    }
                },
                "title": "Vlastnosti",
                "toolbar": {
                    "cancelBtn": "Storno",
                    "closeBtn": "Zavřít",
                    "deleteBtn": {
                        "tooltip": "Odstranit"
                    },
                    "disableBtn": {
                        "tooltip": "Zakázat"
                    },
                    "editBtn": {
                        "tooltip": "Upravit třídu"
                    },
                    "enableBtn": {
                        "tooltip": "Povolit"
                    },
                    "printBtn": {
                        "printAsOdt": "Open Office ODT",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Tisknout třídu"
                    },
                    "saveBtn": "Uložit"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Třídy",
            "toolbar": {
                "addClassBtn": {
                    "text": "Nová třída"
                },
                "classLabel": "Třída",
                "printSchemaBtn": {
                    "text": "Tisknout schéma"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Všechny",
                "cancel": "Storno",
                "clone": "Klonovat",
                "close": "Zavřít",
                "create": "Vytvořit",
                "delete": "Odstranit",
                "disable": "Zakázat",
                "edit": "Upravit",
                "enable": "Povolit",
                "no": "Ne",
                "print": "Tisknout",
                "save": "Uložit",
                "update": "Upravit",
                "yes": "Ano"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Pozor",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Chyba",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Načítání...",
                "saving": "<em>Saving...</em>",
                "success": "Úspěch",
                "warning": "Varování",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Upravit"
            }
        },
        "domains": {
            "domain": "Doména",
            "fieldlabels": {
                "destination": "Cílová třída",
                "enabled": "Povoleno",
                "origin": "Zdrojová třída"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Popis"
                                },
                                "descriptionDirect": {
                                    "label": "Popis zdroj -> cíl"
                                },
                                "descriptionInverse": {
                                    "label": "Popis cíl -> zdroj"
                                },
                                "name": {
                                    "label": "Název"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Storno",
                    "deleteBtn": {
                        "tooltip": "Odstranit"
                    },
                    "disableBtn": {
                        "tooltip": "Zakázat"
                    },
                    "editBtn": {
                        "tooltip": "Upravit"
                    },
                    "enableBtn": {
                        "tooltip": "Povolit"
                    },
                    "saveBtn": "Uložit"
                }
            },
            "singularTitle": "Doména",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Vlastnosti"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Nová doména"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Aktivní",
                "attachments": "Přílohy",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Popis",
                "detail": "Detaily",
                "email": "E-mail",
                "exportcsv": "Export CSV souboru",
                "filters": "<em>Filters</em>",
                "history": "Historie",
                "importcsvfile": "Import CSV souboru",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Název",
                "note": "Poznámka",
                "relations": "Vazby",
                "type": "Typ",
                "username": "Uživatelské jméno"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Správce s omezenými oprávněními",
                "normal": "Normální",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Implicitní filtry",
                "defaultread": "+ Číst",
                "description": "Popis",
                "filters": "<em>Filters</em>",
                "group": "Skupina",
                "name": "Název",
                "none": "Žádný",
                "permissions": "Oprávnění",
                "read": "Číst",
                "uiconfig": "Konfigurace UI",
                "userslist": "<em>Users list</em>",
                "write": "Zapsat"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivní"
                                },
                                "name": {
                                    "label": "Název"
                                },
                                "parent": {
                                    "label": "Nadřazený seznam"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Aktivní"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Storno",
                    "closeBtn": "Zavřít",
                    "deleteBtn": {
                        "tooltip": "Odstranit"
                    },
                    "editBtn": {
                        "tooltip": "Upravit"
                    },
                    "saveBtn": "Uložit"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Storno",
                    "closeBtn": "Zavřít",
                    "deleteBtn": {
                        "tooltip": "Odstranit"
                    },
                    "editBtn": {
                        "tooltip": "Upravit"
                    },
                    "saveBtn": "Uložit"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Nabídky",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Vlastnosti datového typu"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Třídy",
            "custompages": "Uživatelské stránky",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domény",
            "email": "E-mail",
            "generaloptions": "Obecné možnosti",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Jazyky",
            "lookuptypes": "Rozbalovací seznamy",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Procesy",
            "reports": "<em>Reports</em>",
            "searchfilters": "Vyhledávací filtry",
            "servermanagement": "Správa serveru",
            "simples": "<em>Simples</em>",
            "standard": "Standardní",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Správce úkolů",
            "title": "Navigace",
            "users": "Uživatelé",
            "views": "Pohledy",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivní"
                                },
                                "description": {
                                    "label": "Popis"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Název"
                                },
                                "parent": {
                                    "label": "Dědí z"
                                },
                                "stoppableByUser": {
                                    "label": "Lze uživatelsky přerušit"
                                },
                                "superclass": {
                                    "label": "Nadtřída"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Stav"
                    },
                    "values": {
                        "active": "Aktivní"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Storno",
                    "closeBtn": "Zavřít",
                    "deleteBtn": {
                        "tooltip": "Odstranit"
                    },
                    "disableBtn": {
                        "tooltip": "Zakázat"
                    },
                    "editBtn": {
                        "tooltip": "Upravit"
                    },
                    "enableBtn": {
                        "tooltip": "Povolit"
                    },
                    "saveBtn": "Uložit",
                    "versionBtn": {
                        "tooltip": "Verze"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Nový proces"
                },
                "printSchemaBtn": {
                    "text": "Tisknout schéma"
                },
                "processLabel": "Proces",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivní"
                                },
                                "description": {
                                    "label": "Popis"
                                },
                                "name": {
                                    "label": "Název"
                                },
                                "stoppableByUser": {
                                    "label": "Lze uživatelsky přerušit"
                                }
                            }
                        },
                        "icon": "Ikona"
                    }
                }
            },
            "title": "Uživatelé",
            "toolbar": {
                "addUserBtn": {
                    "text": "Nový uživatel"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});