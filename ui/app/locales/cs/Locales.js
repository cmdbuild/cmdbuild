Ext.define('CMDBuildUI.locales.cs.Locales', {
    "requires": ["CMDBuildUI.locales.cs.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "cs",
    "administration": CMDBuildUI.locales.cs.LocalesAdministration.administration,
    "attachments": {
        "add": "Nová příloha",
        "author": "Autor",
        "category": "Kategorie",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Popis",
        "download": "Uložit",
        "editattachment": "Upravit přílohu",
        "file": "Soubor",
        "filename": "Jméno souboru",
        "majorversion": "Hlavní verze",
        "modificationdate": "Datum změny",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Verze",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Viditelnost"
        },
        "menu": {
            "camera": "Kamera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Panorama",
            "resetView": "<em>Reset View</em>",
            "rotate": "Otočit",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Nový záznam",
            "clone": "Klonovat",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Odstranit záznam",
            "modifycard": "Upravit záznam",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Nový",
            "apply": "Použít",
            "cancel": "Storno",
            "close": "Zavřít",
            "delete": "Odstranit",
            "edit": "Upravit",
            "execute": "<em>Execute</em>",
            "remove": "Odstranit",
            "save": "Uložit",
            "saveandapply": "Uložit a použít",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Krok procesu",
            "attachments": "Přílohy",
            "card": "Záznam",
            "details": "Detaily",
            "emails": "<em>Emails</em>",
            "history": "Historie",
            "notes": "Poznámky",
            "relations": "Vazby"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Nový filtr",
        "any": "Jakýkoliv",
        "attribute": "Vyberte atribut",
        "attributes": "Atributy",
        "clearfilter": "Zrušit filtr",
        "clone": "Klonovat",
        "copyof": "Kopírovat",
        "description": "Popis",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Z výběru",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Název",
        "newfilter": "<em>New filter</em>",
        "noone": "Nikdo",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Začíná",
            "between": "Mezi",
            "contained": "Je obsažen",
            "containedorequal": "Je obsažen nebo se rovná ",
            "contains": "Obsahuje",
            "containsorequal": "Obsahuje nebo se rovná",
            "different": "Odlišný",
            "doesnotbeginwith": "Nezačíná",
            "doesnotcontain": "Neobsahuje",
            "doesnotendwith": "Nekončí",
            "endswith": "Končí",
            "equals": "Rovná se",
            "greaterthan": "Více než",
            "isnotnull": "Je vyplněno",
            "isnull": "Není vyplněno",
            "lessthan": "Méně než"
        },
        "relations": "Vazby",
        "type": "Typ",
        "typeinput": "<em>Input Parameter</em>",
        "value": "Hodnota"
    },
    "gis": {
        "card": "Záznam",
        "externalServices": "Externí služby",
        "geographicalAttributes": "Zeměpisné atributy",
        "geoserverLayers": "Vrstvy geoserveru",
        "layers": "Vrstvy",
        "list": "Seznam",
        "mapServices": "<em>Map Services</em>",
        "root": "Kořen",
        "tree": "Navigační strom",
        "view": "Pohledy"
    },
    "history": {
        "begindate": "Datum počástku platnosti",
        "enddate": "Datum konce platnosti",
        "user": "Uživatel"
    },
    "login": {
        "buttons": {
            "login": "Přihlásit",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Jazyk",
            "password": "Heslo",
            "tenants": "<em>Tenants</em>",
            "username": "Uživatelské jméno"
        },
        "title": "Přihlášení"
    },
    "main": {
        "administrationmodule": "Administrační modul",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Odhlásit",
        "managementmodule": "Modul pro správu dat",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Třídy",
        "custompages": "Uživatelské stránky",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Procesy",
        "reports": "<em>Reports</em>",
        "views": "Pohledy"
    },
    "notes": {
        "edit": "Upravit poznámku"
    },
    "notifier": {
        "error": "Chyba",
        "genericerror": "<em>Generic error</em>",
        "info": "Info",
        "success": "Provedeno úspěšně",
        "warning": "Varování"
    },
    "processes": {
        "action": {
            "advance": "Pokročit",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Spustit",
        "workflow": "Proces"
    },
    "relationGraph": {
        "card": "Záznam",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Vazba",
        "class": "<em>Class</em>",
        "class:": "Třída",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Otevřít graf vazeb",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Vazba",
        "relationGraph": "Graf vazeb"
    },
    "relations": {
        "adddetail": "Nový detail",
        "addrelations": "Nové vazby",
        "attributes": "Atributy",
        "code": "Kód",
        "deletedetail": "Odstranit detail",
        "deleterelation": "Odstranit vazbu",
        "description": "Popis",
        "editcard": "Upravit záznam",
        "editdetail": "Upravit detail",
        "editrelation": "Upravit vazbu",
        "mditems": "<em>items</em>",
        "opencard": "Otevřít navázaný záznam",
        "opendetail": "Zobrazit detail",
        "type": "Typ"
    },
    "widgets": {
        "customform": {
            "addrow": "Nový řádek",
            "clonerow": "Klonovat řádek",
            "deleterow": "Odstranit řádek",
            "editrow": "Upravit řádek",
            "export": "Export",
            "import": "Import",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Použít implicitní výběr",
            "togglefilterdisabled": "Zakázat filtr v mřížkách",
            "togglefilterenabled": "Povolit filtr v mřížkách"
        }
    }
});