Ext.define('CMDBuildUI.locales.nl.LocalesAdministration', {
    "singleton": true,
    "localization": "nl",
    "administration": {
        "attributes": {
            "attribute": "Attribuut",
            "attributes": "Attributen",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktief",
                "description": "Omschrijving",
                "domain": "Domein",
                "editortype": "Editor type",
                "filter": "Filter",
                "group": "Groep",
                "help": "<em>Help</em>",
                "includeinherited": "Inclusief ge-erfte",
                "iptype": "IP type",
                "lookup": "Opzoeken",
                "mandatory": "Verplicht",
                "maxlength": "<em>Max Length</em>",
                "mode": "Mode",
                "name": "Naam",
                "precision": "Nauwkeurigheid",
                "preselectifunique": "Voorselectie indien uniek",
                "scale": "Schaal",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Soort",
                "unique": "Uniek",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Alle",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Aanpasbaar",
                "editorhtml": "Html",
                "hidden": "Verborgen",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Platte tekst",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Alleen lezen",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Aktief",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Annuleren",
                "description": "Omschrijving",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Aanpassen metadata",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Verplicht",
                "name": "Naam",
                "save": "Opslaan",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Soort",
                "unique": "Uniek",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Verwijderen",
                "disableattribute": "Blokkeer",
                "editattribute": "Modificeer",
                "enableattribute": "Activeer",
                "openattribute": "Open",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Verwijderen"
                                },
                                "edit": {
                                    "tooltip": "Modificeer"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Alles"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Huidig"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Status",
                                    "values": {
                                        "active": {
                                            "label": "Status"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Verwijderen"
                                },
                                "editTrigger": {
                                    "tooltip": "Modificeer"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Status"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktief"
                                },
                                "classType": {
                                    "label": "Soort"
                                },
                                "description": {
                                    "label": "Omschrijving"
                                },
                                "name": {
                                    "label": "Naam"
                                },
                                "parent": {
                                    "label": "Bovenliggend"
                                },
                                "superclass": {
                                    "label": "Superklas"
                                }
                            }
                        },
                        "icon": "Ikoon",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktief"
                    }
                },
                "title": "Eigenschappen",
                "toolbar": {
                    "cancelBtn": "Annuleren",
                    "closeBtn": "Sluiten",
                    "deleteBtn": {
                        "tooltip": "Verwijderen"
                    },
                    "disableBtn": {
                        "tooltip": "Blokkeer"
                    },
                    "editBtn": {
                        "tooltip": "Klas aanpassen"
                    },
                    "enableBtn": {
                        "tooltip": "Activeer"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Klas afdrukken"
                    },
                    "saveBtn": "Opslaan"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Klassen",
            "toolbar": {
                "addClassBtn": {
                    "text": "Klas toevoegen"
                },
                "classLabel": "Klas",
                "printSchemaBtn": {
                    "text": "Schema afdrukken"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Alles",
                "cancel": "Annuleren",
                "clone": "Dupliceer",
                "close": "Sluiten",
                "create": "Aanmaken",
                "delete": "Verwijderen",
                "disable": "Blokkeer",
                "edit": "Modificeer",
                "enable": "Activeer",
                "no": "Nee",
                "print": "Afdrukken",
                "save": "Opslaan",
                "update": "Bijwerken",
                "yes": "Ja"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Attentie",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Fout",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Laden ...",
                "saving": "<em>Saving...</em>",
                "success": "Succes",
                "warning": "Waarschuwing",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Modificeer"
            }
        },
        "domains": {
            "domain": "Domein",
            "fieldlabels": {
                "destination": "Omschrijving",
                "enabled": "Geactiveerd",
                "origin": "Beginpunt"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Omschrijving"
                                },
                                "descriptionDirect": {
                                    "label": "Directe omschrijving"
                                },
                                "descriptionInverse": {
                                    "label": "Omgekeerde omschrijving"
                                },
                                "name": {
                                    "label": "Naam"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Annuleren",
                    "deleteBtn": {
                        "tooltip": "Verwijderen"
                    },
                    "disableBtn": {
                        "tooltip": "Blokkeer"
                    },
                    "editBtn": {
                        "tooltip": "Modificeer"
                    },
                    "enableBtn": {
                        "tooltip": "Activeer"
                    },
                    "saveBtn": "Opslaan"
                }
            },
            "singularTitle": "Domein",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Eigenschappen"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Domein toevoegen"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Aktief",
                "attachments": "Bijlage",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Omschrijving",
                "detail": "Detail",
                "email": "E-mail",
                "exportcsv": "Exporteer CSV bestand",
                "filters": "<em>Filters</em>",
                "history": "Geschiedenis",
                "importcsvfile": "Importeer CSV bestand",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Naam",
                "note": "Notitie",
                "relations": "Relaties",
                "type": "Soort",
                "username": "Gebruikersnaam"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Beperkte beheerder",
                "normal": "Normaal",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Standaard filters",
                "defaultread": "Def. + R.",
                "description": "Omschrijving",
                "filters": "<em>Filters</em>",
                "group": "Groep",
                "name": "Naam",
                "none": "Geen",
                "permissions": "Permissies",
                "read": "Lees",
                "uiconfig": "UI configuration",
                "userslist": "<em>Users list</em>",
                "write": "Schrijf"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktief"
                                },
                                "name": {
                                    "label": "Naam"
                                },
                                "parent": {
                                    "label": "Bovenliggend"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Aktief"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Annuleren",
                    "closeBtn": "Sluiten",
                    "deleteBtn": {
                        "tooltip": "Verwijderen"
                    },
                    "editBtn": {
                        "tooltip": "Modificeer"
                    },
                    "saveBtn": "Opslaan"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Annuleren",
                    "closeBtn": "Sluiten",
                    "deleteBtn": {
                        "tooltip": "Verwijderen"
                    },
                    "editBtn": {
                        "tooltip": "Modificeer"
                    },
                    "saveBtn": "Opslaan"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menu",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Soort eigenschappen"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Klassen",
            "custompages": "Gebruikers bladzijden",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domeinen",
            "email": "E-mail",
            "generaloptions": "Algemene opties",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Talen",
            "lookuptypes": "Zoeklijst soort",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Processen",
            "reports": "<em>Reports</em>",
            "searchfilters": "Zoek filters",
            "servermanagement": "Server beheer",
            "simples": "<em>Simples</em>",
            "standard": "Standaard",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Taak beheerder",
            "title": "Navigatie",
            "users": "Gebruikers",
            "views": "Vensters",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktief"
                                },
                                "description": {
                                    "label": "Omschrijving"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Naam"
                                },
                                "parent": {
                                    "label": "Ge-erft van"
                                },
                                "stoppableByUser": {
                                    "label": "Stopbare gebruiker"
                                },
                                "superclass": {
                                    "label": "Superklas"
                                }
                            }
                        },
                        "icon": "Ikoon",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktief"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Annuleren",
                    "closeBtn": "Sluiten",
                    "deleteBtn": {
                        "tooltip": "Verwijderen"
                    },
                    "disableBtn": {
                        "tooltip": "Blokkeer"
                    },
                    "editBtn": {
                        "tooltip": "Modificeer"
                    },
                    "enableBtn": {
                        "tooltip": "Activeer"
                    },
                    "saveBtn": "Opslaan",
                    "versionBtn": {
                        "tooltip": "Versie"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Proces toevoegen"
                },
                "printSchemaBtn": {
                    "text": "Schema afdrukken"
                },
                "processLabel": "Procesgang",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktief"
                                },
                                "description": {
                                    "label": "Omschrijving"
                                },
                                "name": {
                                    "label": "Naam"
                                },
                                "stoppableByUser": {
                                    "label": "Stopbare gebruiker"
                                }
                            }
                        },
                        "icon": "Ikoon"
                    }
                }
            },
            "title": "Gebruikers",
            "toolbar": {
                "addUserBtn": {
                    "text": "Gebruiker toevoegen"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});