Ext.define('CMDBuildUI.locales.nl.Locales', {
    "requires": ["CMDBuildUI.locales.nl.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "nl",
    "administration": CMDBuildUI.locales.nl.LocalesAdministration.administration,
    "attachments": {
        "add": "Bijlage toevoegen",
        "author": "Schrijver",
        "category": "Categorie",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Omschrijving",
        "download": "Ophalen",
        "editattachment": "Modificeer bijlage",
        "file": "Bestand",
        "filename": "Bestandsnaam",
        "majorversion": "Hoofd versie",
        "modificationdate": "Modificatie datum",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Versie",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Verberg alles",
                "showAll": "Toon alles"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Zichtbaarheid"
        },
        "menu": {
            "camera": "Camera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Schuiven",
            "resetView": "<em>Reset View</em>",
            "rotate": "Draaien",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Kaart toevoegen",
            "clone": "Dupliceer",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Verwijder kaart",
            "modifycard": "Kaart aanpassen",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Toevoegen",
            "apply": "Toepassen",
            "cancel": "Annuleren",
            "close": "Sluiten",
            "delete": "Verwijderen",
            "edit": "Modificeer",
            "execute": "<em>Execute</em>",
            "remove": "Verwijderen",
            "save": "Opslaan",
            "saveandapply": "Opslaan en toepassen",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Aktiviteit",
            "attachments": "Bijlage",
            "card": "Kaart",
            "details": "Details",
            "emails": "<em>Emails</em>",
            "history": "Geschiedenis",
            "notes": "Notities",
            "relations": "Relaties"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Filter toevoegen",
        "any": "Alle",
        "attribute": "Kies een attribuut",
        "attributes": "Attributen",
        "clearfilter": "Leegmaken filter",
        "clone": "Dupliceer",
        "copyof": "Kopie van",
        "description": "Omschrijving",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "VanSelectie",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Naam",
        "newfilter": "<em>New filter</em>",
        "noone": "Geen enkele",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Begin met",
            "between": "Tussen",
            "contained": "Bevat",
            "containedorequal": "Bevat of is gelijk aan",
            "contains": "Bevat",
            "containsorequal": "Bevat of is gelijk aan",
            "different": "Verschillend",
            "doesnotbeginwith": "Begint niet met ",
            "doesnotcontain": "Bevat geen",
            "doesnotendwith": "Eindigd niet op ",
            "endswith": "Eindigd op",
            "equals": "Gelijk",
            "greaterthan": "Grooter dan",
            "isnotnull": "Is niet leeg",
            "isnull": "Null",
            "lessthan": "Kleiner dan"
        },
        "relations": "Relaties",
        "type": "Soort",
        "typeinput": "Input Parameter",
        "value": "Waarde"
    },
    "gis": {
        "card": "Kaart",
        "externalServices": "Externe Services",
        "geographicalAttributes": "Geografische attributen",
        "geoserverLayers": "Geoserver lagen",
        "layers": "Lagen",
        "list": "Lijst",
        "mapServices": "<em>Map Services</em>",
        "root": "Begin",
        "tree": "Navigatie boomstructuur",
        "view": "Zicht"
    },
    "history": {
        "begindate": "Begin datum",
        "enddate": "Eind datum",
        "user": "Gebruiker"
    },
    "login": {
        "buttons": {
            "login": "Login",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Taal",
            "password": "Wachtwoord",
            "tenants": "<em>Tenants</em>",
            "username": "Gebruikersnaam"
        },
        "title": "Login"
    },
    "main": {
        "administrationmodule": "Administratie module",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Uitloggen",
        "managementmodule": "Gegevens beheer module",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Klassen",
        "custompages": "Gebruikers bladzijden",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Processen",
        "reports": "<em>Reports</em>",
        "views": "Vensters"
    },
    "notes": {
        "edit": "Notitie aanpassen"
    },
    "notifier": {
        "error": "Fout",
        "genericerror": "<em>Generic error</em>",
        "info": "Info",
        "success": "Succes",
        "warning": "Waarschuwing"
    },
    "processes": {
        "action": {
            "advance": "Doorschuiven",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Start",
        "workflow": "Procesgang"
    },
    "relationGraph": {
        "card": "Kaart",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Relatie",
        "class": "<em>Class</em>",
        "class:": "Klas",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Open relatie grafiek",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Relatie",
        "relationGraph": "Relatie grafiek"
    },
    "relations": {
        "adddetail": "Detail toevoegen",
        "addrelations": "Relaties toevoegen",
        "attributes": "Attributen",
        "code": "Code",
        "deletedetail": "Detail verwijderen",
        "deleterelation": "Verwijder relatie",
        "description": "Omschrijving",
        "editcard": "Kaart aanpassen",
        "editdetail": "Detail aanpassen",
        "editrelation": "Aanpassen relatie",
        "mditems": "<em>items</em>",
        "opencard": "Open gerelateerde kaart",
        "opendetail": "Toon detail",
        "type": "Soort"
    },
    "widgets": {
        "customform": {
            "addrow": "Rij toevoegen",
            "clonerow": "Kloon rij",
            "deleterow": "Rij verwijderen",
            "editrow": "Rij aanpassen",
            "export": "Export",
            "import": "Importeer",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Standaard selectie toepassen",
            "togglefilterdisabled": "Verwijder raster filter",
            "togglefilterenabled": "Instellen raster filter"
        }
    }
});