Ext.define('CMDBuildUI.locales.es.LocalesAdministration', {
    "singleton": true,
    "localization": "es",
    "administration": {
        "attributes": {
            "attribute": "Atributo",
            "attributes": "Atributos",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Activa",
                "description": "Descripción",
                "domain": "Dominio",
                "editortype": "Tipo de editor",
                "filter": "Filtro",
                "group": "Grupo",
                "help": "<em>Help</em>",
                "includeinherited": "Incluye heredados",
                "iptype": "Tipo de IP",
                "lookup": "Lookup",
                "mandatory": "Obligatorio",
                "maxlength": "<em>Max Length</em>",
                "mode": "Modo",
                "name": "Nombre",
                "precision": "Precisión",
                "preselectifunique": "Preseleccionar si es único",
                "scale": "Escala",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tipo",
                "unique": "Unívoco",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Uno cualquiera",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Modificable",
                "editorhtml": "Editor HTML",
                "hidden": "Oculto",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Texo simple",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Sólo lectura",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Activo",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Cancela",
                "description": "Descripción",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Modificar metadatos",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obligatorio",
                "name": "Nombre",
                "save": "Confirma",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tipo",
                "unique": "Unívoco",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Borra",
                "disableattribute": "Inhabilitar",
                "editattribute": "Editar",
                "enableattribute": "Habilita",
                "openattribute": "Activo",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Borra"
                                },
                                "edit": {
                                    "tooltip": "Editar"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Todos"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Corriente"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Estado",
                                    "values": {
                                        "active": {
                                            "label": "Estado"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Borra"
                                },
                                "editTrigger": {
                                    "tooltip": "Editar"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Estado"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Activa"
                                },
                                "classType": {
                                    "label": "Tipo"
                                },
                                "description": {
                                    "label": "Descripción"
                                },
                                "name": {
                                    "label": "Nombre"
                                },
                                "parent": {
                                    "label": "Padre"
                                },
                                "superclass": {
                                    "label": "Superclase"
                                }
                            }
                        },
                        "icon": "Icono",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Estado"
                    },
                    "values": {
                        "active": "Activa"
                    }
                },
                "title": "Propiedad",
                "toolbar": {
                    "cancelBtn": "Cancela",
                    "closeBtn": "Cierra",
                    "deleteBtn": {
                        "tooltip": "Borra"
                    },
                    "disableBtn": {
                        "tooltip": "Inhabilitar"
                    },
                    "editBtn": {
                        "tooltip": "Modificar clase"
                    },
                    "enableBtn": {
                        "tooltip": "Habilita"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Imprimir clase"
                    },
                    "saveBtn": "Confirma"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Clases",
            "toolbar": {
                "addClassBtn": {
                    "text": "Agrega clase"
                },
                "classLabel": "Clase",
                "printSchemaBtn": {
                    "text": "Imprimir esquema"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Todos",
                "cancel": "Cancela",
                "clone": "Clonar",
                "close": "Cierra",
                "create": "Crea",
                "delete": "Borra",
                "disable": "Inhabilitar",
                "edit": "Editar",
                "enable": "Habilita",
                "no": "No",
                "print": "Imprimir",
                "save": "Confirma",
                "update": "Actualiza",
                "yes": "Sí"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Atención",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Error",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Cargando...",
                "saving": "<em>Saving...</em>",
                "success": "Éxito",
                "warning": "Atención",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Editar"
            }
        },
        "domains": {
            "domain": "Dominio",
            "fieldlabels": {
                "destination": "Destino",
                "enabled": "Habilitado",
                "origin": "Origen"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Descripción"
                                },
                                "descriptionDirect": {
                                    "label": "Descripción directa"
                                },
                                "descriptionInverse": {
                                    "label": "Descripción inversa"
                                },
                                "name": {
                                    "label": "Nombre"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Cancela",
                    "deleteBtn": {
                        "tooltip": "Borra"
                    },
                    "disableBtn": {
                        "tooltip": "Inhabilitar"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "enableBtn": {
                        "tooltip": "Habilita"
                    },
                    "saveBtn": "Confirma"
                }
            },
            "singularTitle": "Dominio",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Propiedad"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Agregar Dominio"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Activo",
                "attachments": "Adjuntos",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Descripción",
                "detail": "Detalles",
                "email": "E-mail",
                "exportcsv": "Exportar archivo CSV",
                "filters": "<em>Filters</em>",
                "history": "Historia",
                "importcsvfile": "Importar archivo CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Nombre",
                "note": "Notas",
                "relations": "Relaciones",
                "type": "Tipo",
                "username": "Nombre de usuario"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Administrador limitado",
                "normal": "Normal",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Filtros por defecto",
                "defaultread": "Def. + R.",
                "description": "Descripción",
                "filters": "<em>Filters</em>",
                "group": "Grupo",
                "name": "Nombre",
                "none": "Ninguno",
                "permissions": "Permisos",
                "read": "Lectura",
                "uiconfig": "Configuración UI",
                "userslist": "<em>Users list</em>",
                "write": "Escritura"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Activo"
                                },
                                "name": {
                                    "label": "Nombre"
                                },
                                "parent": {
                                    "label": "Padre"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Activo"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Cancela",
                    "closeBtn": "Cierra",
                    "deleteBtn": {
                        "tooltip": "Borra"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "saveBtn": "Confirma"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Cancela",
                    "closeBtn": "Cierra",
                    "deleteBtn": {
                        "tooltip": "Borra"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "saveBtn": "Confirma"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menú",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Propiedad del tipo"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Clases",
            "custompages": "Páginas personalizadas",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Dominios",
            "email": "E-mail",
            "generaloptions": "Opciones Generales",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Idiomas",
            "lookuptypes": "Tipo Lookup",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Procesos",
            "reports": "<em>Reports</em>",
            "searchfilters": "Filtros de búsqueda",
            "servermanagement": "Gestión del servidor",
            "simples": "<em>Simples</em>",
            "standard": "Estándar",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Administrador de tareas",
            "title": "Navegación",
            "users": "Usuarios",
            "views": "Vistas",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Activo"
                                },
                                "description": {
                                    "label": "Descripción"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Nombre"
                                },
                                "parent": {
                                    "label": "Herencia de"
                                },
                                "stoppableByUser": {
                                    "label": "Detenible por el usuario"
                                },
                                "superclass": {
                                    "label": "Superclase"
                                }
                            }
                        },
                        "icon": "Icono",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Estado"
                    },
                    "values": {
                        "active": "Activo"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Cancela",
                    "closeBtn": "Cierra",
                    "deleteBtn": {
                        "tooltip": "Borra"
                    },
                    "disableBtn": {
                        "tooltip": "Inhabilitar"
                    },
                    "editBtn": {
                        "tooltip": "Editar"
                    },
                    "enableBtn": {
                        "tooltip": "Habilita"
                    },
                    "saveBtn": "Confirma",
                    "versionBtn": {
                        "tooltip": "Versión"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Agrega proceso"
                },
                "printSchemaBtn": {
                    "text": "Imprimir esquema"
                },
                "processLabel": "Flujo de trabajo",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Activo"
                                },
                                "description": {
                                    "label": "Descripción"
                                },
                                "name": {
                                    "label": "Nombre"
                                },
                                "stoppableByUser": {
                                    "label": "Detenible por el usuario"
                                }
                            }
                        },
                        "icon": "Icono"
                    }
                }
            },
            "title": "Usuarios",
            "toolbar": {
                "addUserBtn": {
                    "text": "Agregar usuario"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});