Ext.define('CMDBuildUI.locales.es.Locales', {
    "requires": ["CMDBuildUI.locales.es.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "es",
    "administration": CMDBuildUI.locales.es.LocalesAdministration.administration,
    "attachments": {
        "add": "Agrega adjunto",
        "author": "Autor",
        "category": "Categoria",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Descripción",
        "download": "Descargar",
        "editattachment": "Modificar adjunto",
        "file": "Archivo",
        "filename": "Nombre archivo",
        "majorversion": "Versión principal",
        "modificationdate": "Fecha de modificación",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Versión",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Esconder todos",
                "showAll": "Mostrar todos"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Visibilidad"
        },
        "menu": {
            "camera": "Cámara",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Desplaza",
            "resetView": "<em>Reset View</em>",
            "rotate": "Rota",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Agrega tarjeta",
            "clone": "Clonar",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Elimina tarjeta",
            "modifycard": "Modificar tarjeta",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Agrega",
            "apply": "Aplica",
            "cancel": "Cancela",
            "close": "Cierra",
            "delete": "Borra",
            "edit": "Editar",
            "execute": "<em>Execute</em>",
            "remove": "Eliminar",
            "save": "Confirma",
            "saveandapply": "Guarda y aplica",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Actividad",
            "attachments": "Adjuntos",
            "card": "Tarjeta",
            "details": "Detalles",
            "emails": "<em>Emails</em>",
            "history": "Historia",
            "notes": "Notas",
            "relations": "Relaciones"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Añadir filtro",
        "any": "Uno cualquiera",
        "attribute": "Elige un atributo",
        "attributes": "Atributos",
        "clearfilter": "Limpia filtro",
        "clone": "Clonar",
        "copyof": "Copia de",
        "description": "Descripción",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Desde la Selección",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Nombre",
        "newfilter": "<em>New filter</em>",
        "noone": "Ninguno",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Inicia con",
            "between": "Entre",
            "contained": "Contenido",
            "containedorequal": "Contenido o igual",
            "contains": "Contiene",
            "containsorequal": "Contiene o igual",
            "different": "Diferente",
            "doesnotbeginwith": "No inicia con",
            "doesnotcontain": "No contiene",
            "doesnotendwith": "No termina con",
            "endswith": "Termina con",
            "equals": "Iguales",
            "greaterthan": "Mayor",
            "isnotnull": "No es nulo",
            "isnull": "Es nulo",
            "lessthan": "Menor"
        },
        "relations": "Relaciones",
        "type": "Tipo",
        "typeinput": "Parámetro de input",
        "value": "Valor"
    },
    "gis": {
        "card": "Tarjeta",
        "externalServices": "Servicios externos",
        "geographicalAttributes": "Atributos geográficos",
        "geoserverLayers": "Capas del Geoserver",
        "layers": "Layers",
        "list": "Lista",
        "mapServices": "<em>Map Services</em>",
        "root": "Raíz",
        "tree": "Vista de árbol",
        "view": "Vista"
    },
    "history": {
        "begindate": "Fecha de inicio",
        "enddate": "Fecha de finalización",
        "user": "Usuario"
    },
    "login": {
        "buttons": {
            "login": "Accede",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Idioma",
            "password": "Clave",
            "tenants": "<em>Tenants</em>",
            "username": "Nombre de usuario"
        },
        "title": "Accede"
    },
    "main": {
        "administrationmodule": "Módulo de Administración",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Salir",
        "managementmodule": "Módulo gestión de datos",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Clases",
        "custompages": "Páginas personalizadas",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Procesos",
        "reports": "<em>Reports</em>",
        "views": "Vistas"
    },
    "notes": {
        "edit": "Modifica nota"
    },
    "notifier": {
        "error": "Error",
        "genericerror": "<em>Generic error</em>",
        "info": "Información",
        "success": "Éxito",
        "warning": "Atención"
    },
    "processes": {
        "action": {
            "advance": "Continúa",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Inicio",
        "workflow": "Flujo de trabajo"
    },
    "relationGraph": {
        "card": "Tarjeta",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Relación",
        "class": "<em>Class</em>",
        "class:": "Clase",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Abre el gráfico de las relaciones",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Relación",
        "relationGraph": "Gráfico de las Relaciones"
    },
    "relations": {
        "adddetail": "Agregar Detalle",
        "addrelations": "Agregar relaciones",
        "attributes": "Atributos",
        "code": "Código",
        "deletedetail": "Eliminar Detalle",
        "deleterelation": "Eliminar relación",
        "description": "Descripción",
        "editcard": "Modificar tarjeta",
        "editdetail": "Modificar Detalle",
        "editrelation": "Modificar relación",
        "mditems": "<em>items</em>",
        "opencard": "Abrir tarjeta relacionada",
        "opendetail": "Visualizar detalle",
        "type": "Tipo"
    },
    "widgets": {
        "customform": {
            "addrow": "Agrega fila",
            "clonerow": "Clonar fila",
            "deleterow": "Borra fila",
            "editrow": "Modifica fila",
            "export": "Exporta",
            "import": "Importar",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Aplicar selección por defecto",
            "togglefilterdisabled": "Deshabilitar filtro de tabla",
            "togglefilterenabled": "Habilitar filtro de tabla"
        }
    }
});