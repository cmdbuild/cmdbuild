Ext.define('CMDBuildUI.locales.sl.LocalesAdministration', {
    "singleton": true,
    "localization": "sl",
    "administration": {
        "attributes": {
            "attribute": "Atribut",
            "attributes": "Atributi",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktiven",
                "description": "Opis",
                "domain": "Domena",
                "editortype": "Tip urednika",
                "filter": "Filter",
                "group": "Skupina",
                "help": "<em>Help</em>",
                "includeinherited": "Vključi prevzete",
                "iptype": "IP tip",
                "lookup": "Seznam",
                "mandatory": "Obvezno",
                "maxlength": "<em>Max Length</em>",
                "mode": "Način",
                "name": "Ime",
                "precision": "Natančnost",
                "preselectifunique": "Predizberi, če je unikaten",
                "scale": "Merilo",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tip",
                "unique": "Edinstven",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Poljuben",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Za urejanje",
                "editorhtml": "Html",
                "hidden": "Skrito",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Navadni tekst",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Samo za branje",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Aktiven",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Prekliči",
                "description": "Opis",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Uredi metapodatke",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obvezno",
                "name": "Ime",
                "save": "Shrani",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tip",
                "unique": "Edinstven",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Izbriši",
                "disableattribute": "Onemogoči",
                "editattribute": "Uredi",
                "enableattribute": "Omogoči",
                "openattribute": "Odpri",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Izbriši"
                                },
                                "edit": {
                                    "tooltip": "Uredi"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Vsi"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Trenutni"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Status",
                                    "values": {
                                        "active": {
                                            "label": "Status"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Izbriši"
                                },
                                "editTrigger": {
                                    "tooltip": "Uredi"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Status"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiven"
                                },
                                "classType": {
                                    "label": "Tip"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "name": {
                                    "label": "Ime"
                                },
                                "parent": {
                                    "label": "Nadrejen"
                                },
                                "superclass": {
                                    "label": "Nadrazred"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktiven"
                    }
                },
                "title": "Lastnosti",
                "toolbar": {
                    "cancelBtn": "Prekliči",
                    "closeBtn": "Zapri",
                    "deleteBtn": {
                        "tooltip": "Izbriši"
                    },
                    "disableBtn": {
                        "tooltip": "Onemogoči"
                    },
                    "editBtn": {
                        "tooltip": "Spremeni razred"
                    },
                    "enableBtn": {
                        "tooltip": "Omogoči"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Tiskaj razred"
                    },
                    "saveBtn": "Shrani"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Razredi",
            "toolbar": {
                "addClassBtn": {
                    "text": "Dodaj razred"
                },
                "classLabel": "Razred",
                "printSchemaBtn": {
                    "text": "Tiskaj shemo"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Vsi",
                "cancel": "Prekliči",
                "clone": "Kloniraj",
                "close": "Zapri",
                "create": "Ustvari",
                "delete": "Izbriši",
                "disable": "Onemogoči",
                "edit": "Uredi",
                "enable": "Omogoči",
                "no": "Ne",
                "print": "Tiskaj",
                "save": "Shrani",
                "update": "Posodobi",
                "yes": "Da"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Pozor",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Napaka",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Nalaganje...",
                "saving": "<em>Saving...</em>",
                "success": "Uspeh",
                "warning": "Opozorilo",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Uredi"
            }
        },
        "domains": {
            "domain": "Domena",
            "fieldlabels": {
                "destination": "Lokacija",
                "enabled": "Omogočeno",
                "origin": "Izvor"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Opis"
                                },
                                "descriptionDirect": {
                                    "label": "Neposredni opis"
                                },
                                "descriptionInverse": {
                                    "label": "Inverzni opis"
                                },
                                "name": {
                                    "label": "Ime"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Prekliči",
                    "deleteBtn": {
                        "tooltip": "Izbriši"
                    },
                    "disableBtn": {
                        "tooltip": "Onemogoči"
                    },
                    "editBtn": {
                        "tooltip": "Uredi"
                    },
                    "enableBtn": {
                        "tooltip": "Omogoči"
                    },
                    "saveBtn": "Shrani"
                }
            },
            "singularTitle": "Domena",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Lastnosti"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Dodaj domeno"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Aktiven",
                "attachments": "Priponke",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Opis",
                "detail": "Lastnosti",
                "email": "E-pošta",
                "exportcsv": "Izvozi CSV datoteko",
                "filters": "<em>Filters</em>",
                "history": "Zgodovina",
                "importcsvfile": "Uvozi CSV datoteko",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Ime",
                "note": "Opomba",
                "relations": "Povezave",
                "type": "Tip",
                "username": "Uporabniško ime"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Administrator z omejenimi pravicami",
                "normal": "Normalen",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Privzeti filtri",
                "defaultread": "Def. + R.",
                "description": "Opis",
                "filters": "<em>Filters</em>",
                "group": "Skupina",
                "name": "Ime",
                "none": "Nič",
                "permissions": "Dovoljenja",
                "read": "Beri",
                "uiconfig": "UI konfiguracija",
                "userslist": "<em>Users list</em>",
                "write": "Zapiši"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiven"
                                },
                                "name": {
                                    "label": "Ime"
                                },
                                "parent": {
                                    "label": "Nadrejen"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Aktiven"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Prekliči",
                    "closeBtn": "Zapri",
                    "deleteBtn": {
                        "tooltip": "Izbriši"
                    },
                    "editBtn": {
                        "tooltip": "Uredi"
                    },
                    "saveBtn": "Shrani"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Prekliči",
                    "closeBtn": "Zapri",
                    "deleteBtn": {
                        "tooltip": "Izbriši"
                    },
                    "editBtn": {
                        "tooltip": "Uredi"
                    },
                    "saveBtn": "Shrani"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Meni",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Lastnosti tipa"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Razredi",
            "custompages": "Strani po meri",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domene",
            "email": "E-pošta",
            "generaloptions": "Splošne možnosti",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Jeziki",
            "lookuptypes": "Tipi seznama",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Procesi",
            "reports": "<em>Reports</em>",
            "searchfilters": "Filtri iskanja",
            "servermanagement": "Upravljanje strežnika",
            "simples": "<em>Simples</em>",
            "standard": "Standardni",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Upravitelj opravil",
            "title": "Navigacija",
            "users": "Uporabniki",
            "views": "Pogledi",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiven"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Ime"
                                },
                                "parent": {
                                    "label": "Prevzemi"
                                },
                                "stoppableByUser": {
                                    "label": "Uporabniška možnost ustavitve"
                                },
                                "superclass": {
                                    "label": "Nadrazred"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktiven"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Prekliči",
                    "closeBtn": "Zapri",
                    "deleteBtn": {
                        "tooltip": "Izbriši"
                    },
                    "disableBtn": {
                        "tooltip": "Onemogoči"
                    },
                    "editBtn": {
                        "tooltip": "Uredi"
                    },
                    "enableBtn": {
                        "tooltip": "Omogoči"
                    },
                    "saveBtn": "Shrani",
                    "versionBtn": {
                        "tooltip": "Verzija"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Dodaj proces"
                },
                "printSchemaBtn": {
                    "text": "Tiskaj shemo"
                },
                "processLabel": "Delovni proces",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktiven"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "name": {
                                    "label": "Ime"
                                },
                                "stoppableByUser": {
                                    "label": "Uporabniška možnost ustavitve"
                                }
                            }
                        },
                        "icon": "Ikona"
                    }
                }
            },
            "title": "Uporabniki",
            "toolbar": {
                "addUserBtn": {
                    "text": "Dodaj uporabnika"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});