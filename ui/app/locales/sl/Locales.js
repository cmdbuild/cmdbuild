Ext.define('CMDBuildUI.locales.sl.Locales', {
    "requires": ["CMDBuildUI.locales.sl.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "sl",
    "administration": CMDBuildUI.locales.sl.LocalesAdministration.administration,
    "attachments": {
        "add": "Dodaj priponko",
        "author": "Avtor",
        "category": "Kategorija",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Opis",
        "download": "Prenesi",
        "editattachment": "Spremeni priponko",
        "file": "Datoteka",
        "filename": "Ime datoteke",
        "majorversion": "Velika verzija",
        "modificationdate": "Datum spremembe",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Verzija",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Skrij vse",
                "showAll": "Prikaži vse"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Vidnost"
        },
        "menu": {
            "camera": "Kamera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Premikanje",
            "resetView": "<em>Reset View</em>",
            "rotate": "Rotiraj",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Dodaj kartico",
            "clone": "Kloniraj",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Izbriši kartico",
            "modifycard": "Spremeni kartico",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Dodaj",
            "apply": "Potrdi",
            "cancel": "Prekliči",
            "close": "Zapri",
            "delete": "Izbriši",
            "edit": "Uredi",
            "execute": "<em>Execute</em>",
            "remove": "Odstrani",
            "save": "Shrani",
            "saveandapply": "Shrani in potrdi",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Aktivnost",
            "attachments": "Priponke",
            "card": "Kartica",
            "details": "Podrobnosti",
            "emails": "<em>Emails</em>",
            "history": "Zgodovina",
            "notes": "Opombe",
            "relations": "Povezave"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Dodaj filter",
        "any": "Poljuben",
        "attribute": "Izberi atribut",
        "attributes": "Atributi",
        "clearfilter": "Počisti filter",
        "clone": "Kloniraj",
        "copyof": "Kopija od",
        "description": "Opis",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Iz izbranih",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrates</em>",
        "name": "Ime",
        "newfilter": "<em>New filter</em>",
        "noone": "Nobeden",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Začni s/z",
            "between": "Med",
            "contained": "Vključen",
            "containedorequal": "Vključen ali enak",
            "contains": "Vsebuje",
            "containsorequal": "Vsebuje ali enak",
            "different": "Drugačen",
            "doesnotbeginwith": "Se ne prične s/z",
            "doesnotcontain": "Ne vsebuje",
            "doesnotendwith": "Se ne konča s/z",
            "endswith": "Konča se s/z",
            "equals": "Je enako",
            "greaterthan": "Večji kot",
            "isnotnull": "Ni nič (0)",
            "isnull": "Je nič (0)",
            "lessthan": "Manj kot"
        },
        "relations": "Povezave",
        "type": "Tip",
        "typeinput": "Vnosni parameter",
        "value": "Vrednost"
    },
    "gis": {
        "card": "Kartica",
        "externalServices": "Zunanje storitve",
        "geographicalAttributes": "Geografski atributi",
        "geoserverLayers": "Geoserver sloji",
        "layers": "Sloji",
        "list": "Seznam",
        "mapServices": "<em>Map Services</em>",
        "root": "Root",
        "tree": "Navigacijsko drevo",
        "view": "Pogled"
    },
    "history": {
        "begindate": "Datum začetka",
        "enddate": "Datum zaključka",
        "user": "Uporabnik"
    },
    "login": {
        "buttons": {
            "login": "Vpis",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Jezik",
            "password": "Geslo",
            "tenants": "<em>Tenants</em>",
            "username": "Uporabniško ime"
        },
        "title": "Vpis"
    },
    "main": {
        "administrationmodule": "Administrativni modul",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Odjava",
        "managementmodule": "Modul za upravljenje podatkov",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Razredi",
        "custompages": "Strani po meri",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Procesi",
        "reports": "<em>Reports</em>",
        "views": "Pogledi"
    },
    "notes": {
        "edit": "Spremeni opombo"
    },
    "notifier": {
        "error": "Napaka",
        "genericerror": "<em>Generic error</em>",
        "info": "Info",
        "success": "Uspeh",
        "warning": "Opozorilo"
    },
    "processes": {
        "action": {
            "advance": "Napredek",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Začetek",
        "workflow": "Delovni proces"
    },
    "relationGraph": {
        "card": "Kartica",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Povezava",
        "class": "<em>Class</em>",
        "class:": "Razred",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Odpri diagram povezav",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Povezava",
        "relationGraph": "Diagram povezav"
    },
    "relations": {
        "adddetail": "Dodaj lastnosti",
        "addrelations": "Dodaj povezavo",
        "attributes": "Atributi",
        "code": "Koda",
        "deletedetail": "Izbriši lastnost",
        "deleterelation": "Izbriši povezavo",
        "description": "Opis",
        "editcard": "Spremeni kartico",
        "editdetail": "Uredi lastnost",
        "editrelation": "Uredi povezavo",
        "mditems": "<em>items</em>",
        "opencard": "Odpri povezano kartico",
        "opendetail": "Prikaži lastnost",
        "type": "Tip"
    },
    "widgets": {
        "customform": {
            "addrow": "Dodaj vrstico",
            "clonerow": "Kloniraj vrstico",
            "deleterow": "Izbriši vrstico",
            "editrow": "Uredi vrstico",
            "export": "Izvozi",
            "import": "Uvozi",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Potrdi privzeto izbiro",
            "togglefilterdisabled": "Onemogoči mrežni filter",
            "togglefilterenabled": "Omogoči mrežni filter"
        }
    }
});