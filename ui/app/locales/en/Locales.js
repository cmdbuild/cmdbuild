Ext.define('CMDBuildUI.locales.en.Locales', {
    "requires": ["CMDBuildUI.locales.en.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "en",
    "administration": CMDBuildUI.locales.en.LocalesAdministration.administration,
    "attachments": {
        "add": "Add attachment",
        "author": "Author",
        "category": "Category",
        "creationdate": "Creation date",
        "deleteattachment": "Delete attachment",
        "deleteattachment_confirmation": "Are you sure you want to delete this attachment?",
        "description": "Description",
        "download": "Download",
        "editattachment": "Modify attachment",
        "file": "File",
        "filename": "File name",
        "majorversion": "Major version",
        "modificationdate": "Modification date",
        "uploadfile": "Upload file...",
        "version": "Version",
        "viewhistory": "View attachment history"
    },
    "bim": {
        "bimViewer": "Bim Viewer",
        "layers": {
            "label": "Layers",
            "menu": {
                "hideAll": "Hide all",
                "showAll": "Show all"
            },
            "name": "Name",
            "qt": "Qt",
            "visibility": "Visibility",
            "visivility": "Visibility"
        },
        "menu": {
            "camera": "Camera",
            "frontView": "Front View",
            "mod": "mod",
            "pan": "Pan",
            "resetView": "Reset View",
            "rotate": "Rotate",
            "sideView": "Side View",
            "topView": "Top View"
        },
        "showBimCard": "BimCard",
        "tree": {
            "arrowTooltip": "TODO",
            "columnLabel": "Tree",
            "label": "Tree"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Add card",
            "clone": "Clone",
            "clonewithrelations": "Clone card and relations",
            "deletecard": "Delete card",
            "modifycard": "Modify card",
            "opencard": "Open card"
        }
    },
    "common": {
        "actions": {
            "add": "Add",
            "apply": "Apply",
            "cancel": "Cancel",
            "close": "Close",
            "delete": "Delete",
            "edit": "Edit",
            "execute": "Execute",
            "remove": "Remove",
            "save": "Save",
            "saveandapply": "Save and apply",
            "search": "Search",
            "searchtext": "Search..."
        },
        "attributes": {
            "nogroup": "Base data"
        },
        "dates": {
            "date": "d/m/Y",
            "datetime": "d/m/Y H:i:s",
            "time": "H:i:s"
        },
        "grid": {
            "list": "List",
            "row": "Item",
            "rows": "Items",
            "subtype": "Subtype"
        },
        "tabs": {
            "activity": "Activity",
            "attachments": "Attachments",
            "card": "Card",
            "details": "Details",
            "emails": "Emails",
            "history": "History",
            "notes": "Notes",
            "relations": "Relations"
        }
    },
    "filters": {
        "actions": "Actions",
        "addfilter": "Add filter",
        "any": "Any",
        "attribute": "Choose an attribute",
        "attributes": "Attributes",
        "clearfilter": "Clear filter",
        "clone": "Clone",
        "copyof": "Copy of",
        "description": "Description",
        "domain": "Actions",
        "filterdata": "Filter data",
        "fromselection": "From selection",
        "ignore": "Ignore",
        "migrate": "Migrates",
        "name": "Name",
        "newfilter": "New filter",
        "noone": "No one",
        "operator": "Operator",
        "operators": {
            "beginswith": "Begins with",
            "between": "Between",
            "contained": "Contained",
            "containedorequal": "Contained or equal",
            "contains": "Contains",
            "containsorequal": "Contains or equal",
            "different": "Different",
            "doesnotbeginwith": "Does not begin with",
            "doesnotcontain": "Does not contain",
            "doesnotendwith": "Does not end with",
            "endswith": "Ends with",
            "equals": "Equals",
            "greaterthan": "Greater than",
            "isnotnull": "Is not null",
            "isnull": "Is null",
            "lessthan": "Less than"
        },
        "relations": "Relations",
        "type": "Type",
        "typeinput": "Input Parameter",
        "value": "Value"
    },
    "gis": {
        "card": "Card",
        "externalServices": "External services",
        "geographicalAttributes": "Geographical attributes",
        "geoserverLayers": "Geoserver layers",
        "layers": "Layers",
        "list": "List",
        "mapServices": "Map Services",
        "root": "Root",
        "tree": "Navigation tree",
        "view": "View"
    },
    "history": {
        "begindate": "Begin date",
        "enddate": "End date",
        "user": "User"
    },
    "login": {
        "buttons": {
            "login": "Login",
            "logout": "Change user"
        },
        "fields": {
            "group": "Group",
            "language": "Language",
            "password": "Password",
            "tenants": "Tenants",
            "username": "Username"
        },
        "title": "Login"
    },
    "main": {
        "administrationmodule": "Administration module",
        "changegroup": "Change group",
        "changetenant": "Change tenant",
        "logout": "Logout",
        "managementmodule": "Data management module",
        "multitenant": "Multi tenant",
        "searchinallitems": "Search in all items",
        "userpreferences": "Preferences"
    },
    "menu": {
        "allitems": "All items",
        "classes": "Classes",
        "custompages": "Custom pages",
        "dashboards": "Dashboards",
        "processes": "Processes",
        "reports": "Reports",
        "views": "Views"
    },
    "notes": {
        "edit": "Modify note"
    },
    "notifier": {
        "error": "Error",
        "genericerror": "Generic error",
        "info": "Info",
        "success": "Success",
        "warning": "Warning"
    },
    "processes": {
        "action": {
            "advance": "Advance",
            "label": "Action"
        },
        "startworkflow": "Start",
        "workflow": "Process"
    },
    "relationGraph": {
        "card": "Card",
        "cardList": "Card List",
        "cardRelation": "Relation",
        "class": "Class",
        "class:": "Class",
        "classList": "Class List",
        "level": "Level",
        "openRelationGraph": "Open relation graph",
        "qt": "Qt",
        "refresh": "Refresh",
        "relation": "Relation",
        "relationGraph": "Relation graph"
    },
    "relations": {
        "adddetail": "Add detail",
        "addrelations": "Add relations",
        "attributes": "Attributes",
        "code": "Code",
        "deletedetail": "Delete detail",
        "deleterelation": "Delete relation",
        "description": "Description",
        "editcard": "Modify card",
        "editdetail": "Edit detail",
        "editrelation": "Edit relation",
        "mditems": "items",
        "opencard": "Open related card",
        "opendetail": "Show detail",
        "type": "Type"
    },
    "widgets": {
        "customform": {
            "addrow": "Add row",
            "clonerow": "Clone row",
            "deleterow": "Delete row",
            "editrow": "Edit row",
            "export": "Export",
            "import": "Import",
            "refresh": "Refresh to defaults"
        },
        "linkcards": {
            "editcard": "Edit card",
            "opencard": "Open card",
            "refreshselection": "Apply default selection",
            "togglefilterdisabled": "Disable grid filter",
            "togglefilterenabled": "Enable grid filter"
        }
    }
});