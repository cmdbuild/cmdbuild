Ext.define('CMDBuildUI.locales.en.LocalesAdministration', {
    "singleton": true,
    "localization": "en",
    "administration": {
        "attributes": {
            "attribute": "Attribute",
            "attributes": "Attributes",
            "emptytexts": {
                "search": "Search..."
            },
            "fieldlabels": {
                "actionpostvalidation": "Action post validation",
                "active": "Active",
                "description": "Description",
                "domain": "Domain",
                "editortype": "Editor type",
                "filter": "Filter",
                "group": "Group",
                "help": "Help",
                "includeinherited": "Include inherited",
                "iptype": "IP type",
                "lookup": "Lookup",
                "mandatory": "Mandatory",
                "maxlength": "Max Length",
                "mode": "Mode",
                "name": "Name",
                "precision": "Precision",
                "preselectifunique": "Preselect if unique",
                "scale": "Scale",
                "showif": "Show if",
                "showingrid": "Show in grid",
                "showinreducedgrid": "Show in reduced grid",
                "type": "Type",
                "unique": "Unique",
                "validationrules": "Validation rules"
            },
            "strings": {
                "any": "Any",
                "draganddrop": "Drag and drop to reorganize",
                "editable": "Editable",
                "editorhtml": "Html",
                "hidden": "Hidden",
                "immutable": "Immutable",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Plain text",
                "precisionmustbebiggerthanscale": "Precision must be bigger than Scale",
                "readonly": "Read only",
                "scalemustbesmallerthanprecision": "Scale must be smaller than Precision",
                "thefieldmandatorycantbechecked": "The field \"Mandatory\" can't be checked",
                "thefieldmodeishidden": "The field \"Mode\" is hidden",
                "thefieldshowingridcantbechecked": "The field \"Show in grid\" can't be checked",
                "thefieldshowinreducedgridcantbechecked": "The field \"Show in reduced grid\" can't be checked"
            },
            "texts": {
                "active": "Active",
                "addattribute": "Add attribute",
                "cancel": "Cancel",
                "description": "Description",
                "editingmode": "Editing mode",
                "editmetadata": "Edit metadata",
                "grouping": "Grouping",
                "mandatory": "Mandatory",
                "name": "Name",
                "save": "Save",
                "saveandadd": "Save and Add",
                "showingrid": "Show in grid",
                "type": "Type",
                "unique": "Unique",
                "viewmetadata": "View metadata"
            },
            "titles": {
                "generalproperties": "General properties",
                "typeproperties": "Type properties"
            },
            "tooltips": {
                "deleteattribute": "Delete",
                "disableattribute": "Disable",
                "editattribute": "Edit",
                "enableattribute": "Enable",
                "openattribute": "Open",
                "translate": "Translate"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "Class Attachments",
                        "classParameters": "Class Parameters",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Delete"
                                },
                                "edit": {
                                    "tooltip": "Edit"
                                },
                                "moveDown": {
                                    "tooltip": "Move Down"
                                },
                                "moveUp": {
                                    "tooltip": "Move Up"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "Applicability",
                                    "values": {
                                        "all": {
                                            "label": "All"
                                        },
                                        "many": {
                                            "label": "Current and selected"
                                        },
                                        "one": {
                                            "label": "Current"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "Javascript script / custom GUI Paramenters"
                                },
                                "menuItemName": {
                                    "label": "Menu item name",
                                    "values": {
                                        "separator": {
                                            "label": "[---------]"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Status",
                                    "values": {
                                        "active": {
                                            "label": "Status"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "Type / GUI custom",
                                    "values": {
                                        "component": {
                                            "label": "Custom GUI"
                                        },
                                        "custom": {
                                            "label": "Script Javascript"
                                        },
                                        "separator": {
                                            "label": ""
                                        }
                                    }
                                }
                            },
                            "title": "Context Menus"
                        },
                        "defaultOrders": "Default Orders",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "Add new Trigger"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Delete"
                                },
                                "editTrigger": {
                                    "tooltip": "Edit"
                                },
                                "moveDown": {
                                    "tooltip": "Move Down"
                                },
                                "moveUp": {
                                    "tooltip": "Move Up"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "Create new form trigger"
                                },
                                "events": {
                                    "label": "Events",
                                    "values": {
                                        "afterClone": {
                                            "label": "After Clone"
                                        },
                                        "afterEdit": {
                                            "label": "After Edit"
                                        },
                                        "afterInsert": {
                                            "label": "After Insert"
                                        },
                                        "beforView": {
                                            "label": "Before View"
                                        },
                                        "beforeClone": {
                                            "label": "Before Clone"
                                        },
                                        "beforeEdit": {
                                            "label": "Before Edit"
                                        },
                                        "beforeInsert": {
                                            "label": "Before Insert"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "Javascript script"
                                },
                                "status": {
                                    "label": "Status"
                                }
                            },
                            "title": "Form Triggers"
                        },
                        "formWidgets": "Form Widgets",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Active"
                                },
                                "classType": {
                                    "label": "Type"
                                },
                                "description": {
                                    "label": "Description"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "parent": {
                                    "label": "Parent"
                                },
                                "superclass": {
                                    "label": "Superclass"
                                }
                            }
                        },
                        "icon": "Icon",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "Validation Rule"
                                }
                            },
                            "title": "Validation"
                        }
                    },
                    "inputs": {
                        "events": "Events",
                        "javascriptScript": "Javascript Script",
                        "status": "Status"
                    },
                    "values": {
                        "active": "Active"
                    }
                },
                "title": "Properties",
                "toolbar": {
                    "cancelBtn": "Cancel",
                    "closeBtn": "Close",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "disableBtn": {
                        "tooltip": "Disable"
                    },
                    "editBtn": {
                        "tooltip": "Modify class"
                    },
                    "enableBtn": {
                        "tooltip": "Enable"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "Adobe Pdf",
                        "tooltip": "Print class"
                    },
                    "saveBtn": "Save"
                }
            },
            "strings": {
                "geaoattributes": "Geo attributes",
                "levels": "Levels"
            },
            "title": "Classes",
            "toolbar": {
                "addClassBtn": {
                    "text": "Add class"
                },
                "classLabel": "Class",
                "printSchemaBtn": {
                    "text": "Print schema"
                },
                "searchTextInput": {
                    "emptyText": "Search all classes"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "Activate",
                "add": "All",
                "cancel": "Cancel",
                "clone": "Clone",
                "close": "Close",
                "create": "Create",
                "delete": "Delete",
                "disable": "Disable",
                "edit": "Edit",
                "enable": "Enable",
                "no": "No",
                "print": "Print",
                "save": "Save",
                "update": "Update",
                "yes": "Yes"
            },
            "messages": {
                "areyousuredeleteitem": "Are you sure you want to delete this item?",
                "ascendingordescending": "This value is not valid, please select \"Ascending\" or \"Descending\"",
                "attention": "Attention",
                "cannotsortitems": "You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.",
                "cantcontainchar": "The class name can't contain {0} character.",
                "correctformerrors": "Please correct indicated errors",
                "disabled": "disabled",
                "enabled": "enabled",
                "error": "Error",
                "greaterthen": "The class name can't be greater than {0} characters",
                "itemwascreated": "Item was created.",
                "loading": "Loading...",
                "saving": "Saving...",
                "success": "Success",
                "warning": "Warning",
                "was": "was",
                "wasdeleted": "was deleted"
            },
            "tooltips": {
                "edit": "Edit"
            }
        },
        "domains": {
            "domain": "Domain",
            "fieldlabels": {
                "destination": "Destination",
                "enabled": "Enabled",
                "origin": "Origin"
            },
            "pluralTitle": "Domains",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Description"
                                },
                                "descriptionDirect": {
                                    "label": "Direct description"
                                },
                                "descriptionInverse": {
                                    "label": "Inverse description"
                                },
                                "name": {
                                    "label": "Name"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Cancel",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "disableBtn": {
                        "tooltip": "Disable"
                    },
                    "editBtn": {
                        "tooltip": "Edit"
                    },
                    "enableBtn": {
                        "tooltip": "Enable"
                    },
                    "saveBtn": "Save"
                }
            },
            "singularTitle": "Domain",
            "texts": {
                "enabledomains": "Enabled domains",
                "properties": "Properties"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Add Domain"
                },
                "searchTextInput": {
                    "emptyText": "Search all domains"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "Search in grid...",
                "searchusers": "Search users..."
            },
            "fieldlabels": {
                "actions": "Actions",
                "active": "Active",
                "attachments": "Attachments",
                "datasheet": "Data sheet",
                "defaultpage": "Default page",
                "description": "Description",
                "detail": "Detail",
                "email": "E-mail",
                "exportcsv": "Export CSV file",
                "filters": "Filters",
                "history": "History",
                "importcsvfile": "Import CSV file",
                "massiveeditingcards": "Massive editing of cards",
                "name": "Name",
                "note": "Note",
                "relations": "Relations",
                "type": "Type",
                "username": "Username"
            },
            "plural": "Groups and permissions",
            "singular": "Group and permission",
            "strings": {
                "admin": "Admin",
                "displaynousersmessage": "No users to display",
                "displaytotalrecords": "{2} records",
                "limitedadmin": "Limited administrator",
                "normal": "Normal",
                "readonlyadmin": "Read-only admin"
            },
            "texts": {
                "class": "Class",
                "default": "Default",
                "defaultfilter": "Default filter",
                "defaultfilters": "Default filters",
                "defaultread": "Def. + R.",
                "description": "Description",
                "filters": "Filters",
                "group": "Group",
                "name": "Name",
                "none": "None",
                "permissions": "Permissions",
                "read": "Read",
                "uiconfig": "UI configuration",
                "userslist": "Users list",
                "write": "Write"
            },
            "titles": {
                "allusers": "All users",
                "disabledactions": "Disabled actions",
                "disabledallelements": "Functionality disabled Navigation menu \"All Elements\"",
                "disabledmanagementprocesstabs": "Tabs Disabled Management Processes",
                "disabledutilitymenu": "Functionality disabled Utilities Menu",
                "generalattributes": "General Attributes",
                "managementdisabledtabs": "Tabs Disabled Management Classes",
                "usersassigned": "Users assigned"
            },
            "tooltips": {
                "disabledactions": "Disabled actions",
                "filters": "Filters",
                "removedisabledactions": "Remove disabled actions",
                "removefilters": "Remove filters"
            }
        },
        "lookuptypes": {
            "title": "Lookup Types",
            "toolbar": {
                "addClassBtn": {
                    "text": "Add lookup"
                },
                "classLabel": "List",
                "printSchemaBtn": {
                    "text": "Print lookup"
                },
                "searchTextInput": {
                    "emptyText": "Search all lookups..."
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Active"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "parent": {
                                    "label": "Parent"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Active"
                    }
                },
                "title": "Lookup lists",
                "toolbar": {
                    "cancelBtn": "Cancel",
                    "closeBtn": "Close",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "editBtn": {
                        "tooltip": "Edit"
                    },
                    "saveBtn": "Save"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Cancel",
                    "closeBtn": "Close",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "editBtn": {
                        "tooltip": "Edit"
                    },
                    "saveBtn": "Save"
                }
            },
            "pluralTitle": "Menus",
            "singularTitle": "Menu",
            "toolbar": {
                "addBtn": {
                    "text": "Add menu"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Type Properties"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Classes",
            "custompages": "Custom pages",
            "dashboards": "Dashboards",
            "dms": "DMS",
            "domains": "Domains",
            "email": "E-mail",
            "generaloptions": "General options",
            "gis": "GIS",
            "groupsandpermissions": "Groups and permissions",
            "languages": "Languages",
            "lookuptypes": "Lookup types",
            "menus": "Menus",
            "multitenant": "Multitenant",
            "navigationtrees": "Navigation trees",
            "processes": "Processes",
            "reports": "Reports",
            "searchfilters": "Search filters",
            "servermanagement": "Server management",
            "simples": "Simples",
            "standard": "Standard",
            "systemconfig": "System config",
            "taskmanager": "Task manager",
            "title": "Navigation",
            "users": "Users",
            "views": "Views",
            "workflow": "Workflow"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "Default Orders",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Active"
                                },
                                "description": {
                                    "label": "Description"
                                },
                                "enableSaveButton": {
                                    "label": "Hide \"Save\" button"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "parent": {
                                    "label": "Inherits from"
                                },
                                "stoppableByUser": {
                                    "label": "User stoppable"
                                },
                                "superclass": {
                                    "label": "Superclass"
                                }
                            }
                        },
                        "icon": "Icon",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "Default filter"
                                },
                                "messageAttribute": {
                                    "label": "Message attribute"
                                },
                                "stateAttribute": {
                                    "label": "State attribute"
                                }
                            },
                            "title": "Process parameters"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "Validation Rule"
                                }
                            },
                            "title": "Validation"
                        }
                    },
                    "inputs": {
                        "status": "Status"
                    },
                    "values": {
                        "active": "Active"
                    }
                },
                "title": "Properties",
                "toolbar": {
                    "cancelBtn": "Cancel",
                    "closeBtn": "Close",
                    "deleteBtn": {
                        "tooltip": "Delete"
                    },
                    "disableBtn": {
                        "tooltip": "Disable"
                    },
                    "editBtn": {
                        "tooltip": "Edit"
                    },
                    "enableBtn": {
                        "tooltip": "Enable"
                    },
                    "saveBtn": "Save",
                    "versionBtn": {
                        "tooltip": "Version"
                    }
                }
            },
            "title": "Processes",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Add process"
                },
                "printSchemaBtn": {
                    "text": "Print schema"
                },
                "processLabel": "Process",
                "searchTextInput": {
                    "emptyText": "Search all processes"
                }
            }
        },
        "title": "Administration",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Active"
                                },
                                "description": {
                                    "label": "Description"
                                },
                                "name": {
                                    "label": "Name"
                                },
                                "stoppableByUser": {
                                    "label": "User stoppable"
                                }
                            }
                        },
                        "icon": "Icon"
                    }
                }
            },
            "title": "Users",
            "toolbar": {
                "addUserBtn": {
                    "text": "Add user"
                },
                "searchTextInput": {
                    "emptyText": "Search all users"
                }
            }
        }
    }
});