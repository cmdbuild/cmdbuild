Ext.define('CMDBuildUI.locales.Locales', {
    requires: ['CMDBuildUI.locales.LocalesAdministration'],
    singleton: true,
    localization: 'en',
    administration: CMDBuildUI.locales.LocalesAdministration.administration,
    common: {
        actions: {
            add: 'Add',
            apply: 'Apply',
            cancel: 'Cancel',
            close: 'Close',
            delete: 'Delete',
            edit: 'Edit',
            execute: 'Execute',
            remove: 'Remove',
            save: 'Save',
            saveandapply: 'Save and apply',
            search: 'Search',
            searchtext: 'Search...'
        },
        attributes: {
            nogroup: 'Base data'
        },
        dates: {
            // Reffer to https://docs.sencha.com/extjs/6.2.0/classic/Ext.Date.html
            date: 'd/m/Y',
            datetime: 'd/m/Y H:i:s',
            time: 'H:i:s'
        },
        grid: {
            subtype: 'Subtype',
            list: 'List',
            row: 'Item',
            rows: 'Items'
        },
        tabs: {
            activity: 'Activity',
            attachments: 'Attachments',
            card: 'Card',
            details: 'Details',
            emails: 'Emails',
            history: 'History',
            notes: 'Notes',
            relations: 'Relations'
        }
    },
    main: {
        administrationmodule: 'Administration module',
        changegroup: 'Change group',
        changetenant: 'Change tenant',
        confirmchangegroup: 'Do you really want to change the group?',
        confirmchangetenants: 'Do you really want to change active tenants?',
        confirmdisabletenant: 'Do you really want to disable "Ignore tenants" flag?',
        confirmenabletenant: 'Do you really want to enable "Ignore tenants" flag?',
        ignoretenants: 'Ignore tenants',
        logout: 'Logout',
        managementmodule: 'Data management module',
        multitenant: 'Multi tenant',
        searchinallitems: 'Search in all items',
        userpreferences: 'Preferences'
    },
    login: {
        title: 'Login',
        fields: {
            username: 'Username',
            password: 'Password',
            language: 'Language',
            group: 'Group',
            tenants: 'Tenants'
        },
        buttons: {
            login: 'Login',
            logout: 'Change user'
        }
    },
    notifier: {
        attention: 'Attention',
        error: 'Error',
        info: 'Info',
        success: 'Success',
        warning: 'Warning',
        genericerror: 'Generic error'
    },
    menu: {
        allitems: 'All items',
        classes: 'Classes',
        processes: 'Processes',
        reports: 'Reports',
        dashboards: 'Dashboards',
        views: 'Views',
        custompages: 'Custom pages'
    },
    classes: {
        cards: {
            addcard: 'Add card',
            clone: 'Clone',
            clonewithrelations: 'Clone card and relations',
            deletecard: 'Delete card',
            modifycard: 'Modify card',
            opencard: 'Open card'
        }
    },
    processes: {
        action: {
            advance: 'Advance',
            label: 'Action'
        },
        startworkflow: 'Start',
        workflow: 'Workflow'
    },
    notes: {
        edit: 'Modify notes'
    },
    relations: {
        adddetail: 'Add detail',
        addrelations: 'Add relations',
        attributes: 'Attributes',
        code: 'Code',
        deletedetail: 'Delete detail',
        deleterelation: 'Delete relation',
        description: 'Description',
        editcard: 'Edit card',
        editdetail: 'Edit detail',
        editrelation: 'Edit relation',
        mditems: 'items',
        opencard: 'Open related card',
        opendetail: 'Show detail',
        type: 'Type'
    },
    history: {
        begindate: 'Begin date',
        enddate: 'End date',
        user: 'User'
    },
    attachments: {
        add: 'Add attachment',
        author: 'Author',
        category: 'Category',
        creationdate: 'Creation date',
        deleteattachment: 'Delete attachment',
        deleteattachment_confirmation: 'Are you sure you want to delete this attachment?',
        description: 'Description',
        download: 'Download',
        editattachment: 'Modify attachment',
        file: 'File',
        filename: 'File name',
        majorversion: 'Major version',
        modificationdate: 'Modification date',
        uploadfile: 'Upload file...',
        version: 'Version',
        viewhistory: 'View attachment history'
    },
    emails: {
        keepsynchronization: 'Keep synchronization',
        delay: 'Delay',
        from: 'From',
        to: 'To',
        cc: 'Cc',
        bcc: 'Bcc',
        subject: 'Subject',
        message: 'Message',
        attachfile: 'Attach file',
        addattachmentsfromdms: 'Add attachments from DMS',
        composefromtemplate: 'Compose from template',
        composeemail: 'Compose e-mail',
        regenerateallemails: "Regenerate all e-mails",
        gridrefresh: 'Grid refresh',
        archivingdate: "Archiving date",
    },
    widgets: {
        linkcards: {
            togglefilterenabled: 'Disable grid filter',
            togglefilterdisabled: 'Enable grid filter',
            refreshselection: 'Apply default selection',
            opencard: 'Open card',
            editcard: 'Edit card'
        },
        customform: {
            addrow: 'Add row',
            clonerow: 'Clone row',
            deleterow: 'Delete row',
            editrow: 'Edit row',
            export: 'Export',
            import: 'Import',
            refresh: 'Refresh to defaults'
        }
    },
    filters: {
        addfilter: 'Add filter',
        any: 'Any',
        attributes: 'Attributes',
        attribute: 'Choose an attribute',
        clearfilter: 'Clear filter',
        copyof: 'Copy of',
        description: 'Description',
        filterdata: 'Filter data',
        fromselection: 'From selection',
        name: 'Name',
        newfilter: 'New filter',
        noone: 'No one',
        operator: 'Operator',
        relations: 'Relations',
        type: 'Type',
        typeinput: 'Input Parameter',
        value: 'Value',
        actions: 'Actions',
        ignore: 'Ignore',
        migrate: 'Migrates',
        clone: 'Clone',
        domain: 'Domain',
        operators: {
            beginswith: 'Begins with',
            between: 'Between',
            contained: 'Contained',
            containedorequal: 'Contained or equal',
            contains: 'Contains',
            containsorequal: 'Contains or equal',
            different: 'Different',
            doesnotbeginwith: 'Does not begin with',
            doesnotcontain: 'Does not contain',
            doesnotendwith: 'Does not end with',
            endswith: 'Ends with',
            equals: 'Equals',
            greaterthan: 'Greater than',
            isnotnull: 'Is not null',
            isnull: 'Is null',
            lessthan: 'Less than'
        }
    },
    gis: {
        tree: 'Tree',
        list: 'List',
        card: 'Card',
        layers: 'Layers',
        geographicalAttributes: 'Geo Attributes',
        root: 'Root',
        externalServices: 'External services',
        geoserverLayers: 'Geoserver layers',
        mapServices: 'Map Services',
        view: 'View'

    },
    relationGraph: {
        openRelationGraph: 'Open Relation Graph',
        relationGraph: 'Relation Graph',
        card: 'Card',
        cardRelation: 'Card Relation',
        cardList: 'Card List',
        classList: 'Class List',
        relation: 'relation',
        level: 'Level',
        refresh: 'Refresh',
        class: 'Class',
        qt: 'Qt'
    },
    bim: {
        showBimCard: 'BimCard',
        bimViewer: 'Bim Viewer',
        tree: {
            label: 'Tree',
            columnLabel: 'Tree',
            arrowTooltip: 'TODO'
        },
        layers: {
            label: 'Layers',
            name: 'Name',
            visibility: 'Visibility',
            qt: 'Qt',
            menu: {
                showAll: 'Show All',
                hideAll: 'Hide All'
            }
        },
        menu: {
            camera: 'Camera',
            resetView: 'Reset View',
            frontView: 'Front View',
            sideView: 'Side View',
            topView: 'Top View',
            mod: 'mod',
            rotate: 'rotate',
            pan: 'pan'
        }
    }
});