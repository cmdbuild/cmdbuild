Ext.define('CMDBuildUI.locales.ja.LocalesAdministration', {
    "singleton": true,
    "localization": "ja",
    "administration": {
        "attributes": {
            "attribute": "属性",
            "attributes": "属性",
            "emptytexts": {
                "search": "全属性検索..."
            },
            "fieldlabels": {
                "actionpostvalidation": "入力チェック",
                "active": "有効",
                "description": "説明",
                "domain": "ドメイン",
                "editortype": "エディタタイプ",
                "filter": "フィルター",
                "group": "グループ",
                "help": "ヘルプ",
                "includeinherited": "継承を含む",
                "iptype": "IPタイプ",
                "lookup": "ルックアップ",
                "mandatory": "必須",
                "maxlength": "最大長",
                "mode": "モード",
                "name": "名前",
                "precision": "精度",
                "preselectifunique": "ユニークな値を自動選択",
                "scale": "桁数",
                "showif": "表示条件",
                "showingrid": "リスト表示",
                "showinreducedgrid": "省略リスト表示",
                "type": "タイプ",
                "unique": "ユニーク",
                "validationrules": "入力チェック条件"
            },
            "strings": {
                "any": "すべての",
                "draganddrop": "ドラッグ・アンド・ドロップ",
                "editable": "編集可能",
                "editorhtml": "Html",
                "hidden": "非表示",
                "immutable": "イミュータブル",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "プレーンテキスト",
                "precisionmustbebiggerthanscale": "精度は桁数より大きくなければなりません",
                "readonly": "読み取り専用",
                "scalemustbesmallerthanprecision": "桁数は精度より小さくなければなりません",
                "thefieldmandatorycantbechecked": "\"必須\"はチェックできません",
                "thefieldmodeishidden": "\"編集モード\"は表示されません",
                "thefieldshowingridcantbechecked": "\"リスト表示\"はチェックできません",
                "thefieldshowinreducedgridcantbechecked": "\"省略リスト表示\"はチェックできません"
            },
            "texts": {
                "active": "有効",
                "addattribute": "属性追加",
                "cancel": "キャンセル",
                "description": "説明",
                "editingmode": "編集モード",
                "editmetadata": "メタデータ編集",
                "grouping": "グループ",
                "mandatory": "必須",
                "name": "名前",
                "save": "保存",
                "saveandadd": "保存して追加",
                "showingrid": "リスト表示",
                "type": "タイプ",
                "unique": "ユニーク",
                "viewmetadata": "メタデータ表示"
            },
            "titles": {
                "generalproperties": "基本プロパティ",
                "typeproperties": "タイププロパティ"
            },
            "tooltips": {
                "deleteattribute": "削除",
                "disableattribute": "無効",
                "editattribute": "編集",
                "enableattribute": "有効にする",
                "openattribute": "開く",
                "translate": "翻訳"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "添付",
                        "classParameters": "パラメータ",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "削除"
                                },
                                "edit": {
                                    "tooltip": "編集"
                                },
                                "moveDown": {
                                    "tooltip": "下げる"
                                },
                                "moveUp": {
                                    "tooltip": "上げる"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "適用性",
                                    "values": {
                                        "all": {
                                            "label": "全て"
                                        },
                                        "many": {
                                            "label": "選択"
                                        },
                                        "one": {
                                            "label": "最新"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "Javascript / カスタムGUIパラメータ"
                                },
                                "menuItemName": {
                                    "label": "メニューアイテム名",
                                    "values": {
                                        "separator": {
                                            "label": "[---------]"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "ステータス",
                                    "values": {
                                        "active": {
                                            "label": "ステータス"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "タイプ / GUIカスタム",
                                    "values": {
                                        "component": {
                                            "label": "カスタムGUI"
                                        },
                                        "custom": {
                                            "label": "Javascript"
                                        },
                                        "separator": {
                                            "label": ""
                                        }
                                    }
                                }
                            },
                            "title": "コンテキストメニュー"
                        },
                        "defaultOrders": "デフォルト順",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "トリガー追加"
                                },
                                "deleteTrigger": {
                                    "tooltip": "削除"
                                },
                                "editTrigger": {
                                    "tooltip": "編集"
                                },
                                "moveDown": {
                                    "tooltip": "下げる"
                                },
                                "moveUp": {
                                    "tooltip": "上げる"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "フォームトリガー作成"
                                },
                                "events": {
                                    "label": "イベントEvents",
                                    "values": {
                                        "afterClone": {
                                            "label": "クローン後"
                                        },
                                        "afterEdit": {
                                            "label": "編集後"
                                        },
                                        "afterInsert": {
                                            "label": "挿入後"
                                        },
                                        "beforView": {
                                            "label": "表示前"
                                        },
                                        "beforeClone": {
                                            "label": "クローン前"
                                        },
                                        "beforeEdit": {
                                            "label": "編集前"
                                        },
                                        "beforeInsert": {
                                            "label": "挿入前"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "Javascript"
                                },
                                "status": {
                                    "label": "ステータス"
                                }
                            },
                            "title": "フォームトリガー"
                        },
                        "formWidgets": "フォームウィジェット",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "有効"
                                },
                                "classType": {
                                    "label": "タイプ"
                                },
                                "description": {
                                    "label": "説明"
                                },
                                "name": {
                                    "label": "名前"
                                },
                                "parent": {
                                    "label": "親"
                                },
                                "superclass": {
                                    "label": "スーパークラス"
                                }
                            }
                        },
                        "icon": "アイコン",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "入力チェック条件"
                                }
                            },
                            "title": "入力チェック"
                        }
                    },
                    "inputs": {
                        "events": "イベント",
                        "javascriptScript": "Javascript",
                        "status": "ステータス"
                    },
                    "values": {
                        "active": "有効"
                    }
                },
                "title": "プロパティ",
                "toolbar": {
                    "cancelBtn": "キャンセル",
                    "closeBtn": "クローズ",
                    "deleteBtn": {
                        "tooltip": "削除"
                    },
                    "disableBtn": {
                        "tooltip": "無効"
                    },
                    "editBtn": {
                        "tooltip": "クラス変更"
                    },
                    "enableBtn": {
                        "tooltip": "有効にする"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "Adobe Pdf",
                        "tooltip": "クラス印刷"
                    },
                    "saveBtn": "保存"
                }
            },
            "strings": {
                "geaoattributes": "GEOアトリビュート",
                "levels": "レベル"
            },
            "title": "クラス",
            "toolbar": {
                "addClassBtn": {
                    "text": "クラス追加"
                },
                "classLabel": "クラス",
                "printSchemaBtn": {
                    "text": "スキーマ印刷"
                },
                "searchTextInput": {
                    "emptyText": "全クラス検索"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "アクティベート",
                "add": "全て",
                "cancel": "キャンセル",
                "clone": "クローン",
                "close": "クローズ",
                "create": "作成",
                "delete": "削除",
                "disable": "無効",
                "edit": "編集",
                "enable": "有効化",
                "no": "No",
                "print": "印刷",
                "save": "保存",
                "update": "更新",
                "yes": "はい"
            },
            "messages": {
                "areyousuredeleteitem": "本当にこのアイテムを削除しますか?",
                "ascendingordescending": "値が無効です, \"昇順\"か\"降順\"を選択してください",
                "attention": "注意",
                "cannotsortitems": "順序変更できません。フィルターや継承されている属性がある場合はそれらを削除してください",
                "cantcontainchar": "クラス名には {0} を使用できません",
                "correctformerrors": "エラーを修正してください",
                "disabled": "無効化",
                "enabled": "有効化",
                "error": "エラー",
                "greaterthen": "クラス名は {0} 字より大きくできません",
                "itemwascreated": "アイテムが作成されました",
                "loading": "ロード中...",
                "saving": "保存中...",
                "success": "成功",
                "warning": "警告",
                "was": "was",
                "wasdeleted": "削除されました/em>"
            },
            "tooltips": {
                "edit": "編集"
            }
        },
        "domains": {
            "domain": "ドメイン",
            "fieldlabels": {
                "destination": "宛先",
                "enabled": "有効",
                "origin": "ターゲット（起点)"
            },
            "pluralTitle": "ドメイン",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "説明"
                                },
                                "descriptionDirect": {
                                    "label": "順方向の説明"
                                },
                                "descriptionInverse": {
                                    "label": "逆方向の説明"
                                },
                                "name": {
                                    "label": "名前"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "キャンセル",
                    "deleteBtn": {
                        "tooltip": "削除"
                    },
                    "disableBtn": {
                        "tooltip": "無効"
                    },
                    "editBtn": {
                        "tooltip": "編集"
                    },
                    "enableBtn": {
                        "tooltip": "有効にする"
                    },
                    "saveBtn": "保存"
                }
            },
            "singularTitle": "ドメイン",
            "texts": {
                "enabledomains": "有効ドメイン",
                "properties": "プロパティ"
            },
            "toolbar": {
                "addBtn": {
                    "text": "ドメイン追加"
                },
                "searchTextInput": {
                    "emptyText": "全ドメインを検索"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "リスト表示検索...",
                "searchusers": "ユーザー検索..."
            },
            "fieldlabels": {
                "actions": "アクション",
                "active": "有効",
                "attachments": "添付",
                "datasheet": "データシート",
                "defaultpage": "初期ページ",
                "description": "説明",
                "detail": "詳細",
                "email": "メール",
                "exportcsv": "CSV ファイルエキスポート",
                "filters": "フィルター",
                "history": "履歴",
                "importcsvfile": "CSVファイルインポート",
                "massiveeditingcards": "一括編集",
                "name": "名前",
                "note": "ノート",
                "relations": "リレーション",
                "type": "タイプ",
                "username": "ユーザー名"
            },
            "plural": "グループと権限",
            "singular": "グループと権限",
            "strings": {
                "admin": "アドミニストレータ",
                "displaynousersmessage": "ユーザーはありません",
                "displaytotalrecords": "{2} 件",
                "limitedadmin": "制限されたアドミニストレータ",
                "normal": "通常",
                "readonlyadmin": "読取のみアドミニストレータ"
            },
            "texts": {
                "class": "クラス",
                "default": "デフォルト",
                "defaultfilter": "既定のフィルター",
                "defaultfilters": "既定のフィルター",
                "defaultread": "デフォルト+読取",
                "description": "説明",
                "filters": "フィルター",
                "group": "グループ",
                "name": "名前",
                "none": "権限なし",
                "permissions": "許可",
                "read": "読取権限",
                "uiconfig": "UIの構成",
                "userslist": "ユーザーリスト",
                "write": "書込権限"
            },
            "titles": {
                "allusers": "全ユーザー",
                "disabledactions": "無効のアクション",
                "disabledallelements": "\"全項目\"無効のナビゲーションメニュー\"全項目\"",
                "disabledmanagementprocesstabs": "プロセスで無効なタブ",
                "disabledutilitymenu": "ユーティリティで無効な機能",
                "generalattributes": "基本設定",
                "managementdisabledtabs": "クラスで無効なタブ",
                "usersassigned": "許可ユーザー"
            },
            "tooltips": {
                "disabledactions": "無効なアクション",
                "filters": "フィルター",
                "removedisabledactions": "無効アクションを削除",
                "removefilters": "フィルター削除"
            }
        },
        "lookuptypes": {
            "title": "ルックアップ",
            "toolbar": {
                "addClassBtn": {
                    "text": "ルックアップ追加"
                },
                "classLabel": "リスト",
                "printSchemaBtn": {
                    "text": "ルックアップ印刷"
                },
                "searchTextInput": {
                    "emptyText": "全ルックアップ検索..."
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "有効"
                                },
                                "name": {
                                    "label": "名前"
                                },
                                "parent": {
                                    "label": "親"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "有効"
                    }
                },
                "title": "ルックアップリスト",
                "toolbar": {
                    "cancelBtn": "キャンセル",
                    "closeBtn": "クローズ",
                    "deleteBtn": {
                        "tooltip": "削除"
                    },
                    "editBtn": {
                        "tooltip": "編集"
                    },
                    "saveBtn": "保存"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "キャンセル",
                    "closeBtn": "クローズ",
                    "deleteBtn": {
                        "tooltip": "削除"
                    },
                    "editBtn": {
                        "tooltip": "編集"
                    },
                    "saveBtn": "保存"
                }
            },
            "pluralTitle": "メニュー",
            "singularTitle": "メニュー",
            "toolbar": {
                "addBtn": {
                    "text": "メニュー作成"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "タイププロパティ"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "クラス",
            "custompages": "カスタムページ",
            "dashboards": "ダッシュボード",
            "dms": "DMS",
            "domains": "ドメイン",
            "email": "メール",
            "generaloptions": "基本設定",
            "gis": "GIS",
            "groupsandpermissions": "グループと権限",
            "languages": "言語",
            "lookuptypes": "ルックアップ",
            "menus": "メニュー",
            "multitenant": "マルチテナント",
            "navigationtrees": "ナビゲーションメニュー",
            "processes": "プロセス",
            "reports": "レポート",
            "searchfilters": "検索フィルタ",
            "servermanagement": "サーバ管理",
            "simples": "シンプルクラス",
            "standard": "標準",
            "systemconfig": "システム設定",
            "taskmanager": "タスクマネージャ",
            "title": "ナビゲーション",
            "users": "ユーザー",
            "views": "ビュー",
            "workflow": "Workflowワークフロー"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "デフォルト順",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "有効"
                                },
                                "description": {
                                    "label": "説明"
                                },
                                "enableSaveButton": {
                                    "label": "\"保存\"ボタン非表示"
                                },
                                "name": {
                                    "label": "名前"
                                },
                                "parent": {
                                    "label": "継承元"
                                },
                                "stoppableByUser": {
                                    "label": "ユーザーによる中断可能"
                                },
                                "superclass": {
                                    "label": "スーパークラス"
                                }
                            }
                        },
                        "icon": "アイコン",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "規定のフィルター"
                                },
                                "messageAttribute": {
                                    "label": "メッセージ属性"
                                },
                                "stateAttribute": {
                                    "label": "ステート属性"
                                }
                            },
                            "title": "プロセスパラメータ"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "入力チェック条件"
                                }
                            },
                            "title": "入力チェック"
                        }
                    },
                    "inputs": {
                        "status": "ステータス"
                    },
                    "values": {
                        "active": "有効"
                    }
                },
                "title": "プロパティ",
                "toolbar": {
                    "cancelBtn": "キャンセル",
                    "closeBtn": "クローズ",
                    "deleteBtn": {
                        "tooltip": "削除"
                    },
                    "disableBtn": {
                        "tooltip": "無効"
                    },
                    "editBtn": {
                        "tooltip": "編集"
                    },
                    "enableBtn": {
                        "tooltip": "有効化"
                    },
                    "saveBtn": "保存",
                    "versionBtn": {
                        "tooltip": "バージョン"
                    }
                }
            },
            "title": "プロセス",
            "toolbar": {
                "addProcessBtn": {
                    "text": "プロセス追加"
                },
                "printSchemaBtn": {
                    "text": "スキーマ印刷"
                },
                "processLabel": "ワークフロー",
                "searchTextInput": {
                    "emptyText": "全プロセス検索"
                }
            }
        },
        "title": "管理",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "有効"
                                },
                                "description": {
                                    "label": "説明"
                                },
                                "name": {
                                    "label": "名前"
                                },
                                "stoppableByUser": {
                                    "label": "ユーザーによる中断可能"
                                }
                            }
                        },
                        "icon": "アイコン"
                    }
                }
            },
            "title": "ユーザー",
            "toolbar": {
                "addUserBtn": {
                    "text": "ユーザー追加"
                },
                "searchTextInput": {
                    "emptyText": "全ユーザー検索"
                }
            }
        }
    }
});