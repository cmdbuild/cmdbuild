Ext.define('CMDBuildUI.locales.ja.Locales', {
    "requires": ["CMDBuildUI.locales.ja.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "ja",
    "administration": CMDBuildUI.locales.ja.LocalesAdministration.administration,
    "attachments": {
        "add": "添付追加",
        "author": "作成者",
        "category": "カテゴリー",
        "creationdate": "作成日",
        "deleteattachment": "添付を削除",
        "deleteattachment_confirmation": "本当にこの添付を削除しますか?",
        "description": "説明",
        "download": "ダウンロード",
        "editattachment": "添付変更",
        "file": "ファイル",
        "filename": "ファイル名",
        "majorversion": "メジャーバージョン",
        "modificationdate": "更新日",
        "uploadfile": "アップロード中...",
        "version": "バージョン",
        "viewhistory": "添付の履歴表示"
    },
    "bim": {
        "bimViewer": "BIMビューワー",
        "layers": {
            "label": "レイヤー",
            "menu": {
                "hideAll": "全て隠す",
                "showAll": "全て表示"
            },
            "name": "名前",
            "qt": "Qt",
            "visibility": "表示",
            "visivility": "表示"
        },
        "menu": {
            "camera": "カメラ",
            "frontView": "フロントビュー",
            "mod": "mod",
            "pan": "パン",
            "resetView": "リセットビュー",
            "rotate": "回転",
            "sideView": "サイドビュー",
            "topView": "トップビュー"
        },
        "showBimCard": "BIMカード",
        "tree": {
            "arrowTooltip": "TODO",
            "columnLabel": "ツリー",
            "label": "ツリー"
        }
    },
    "classes": {
        "cards": {
            "addcard": "カード追加",
            "clone": "クローン",
            "clonewithrelations": "クローン(カード/リレーション)",
            "deletecard": "カード削除",
            "modifycard": "カード更新",
            "opencard": "カード表示"
        }
    },
    "common": {
        "actions": {
            "add": "追加",
            "apply": "適用",
            "cancel": "キャンセル",
            "close": "クローズ",
            "delete": "削除",
            "edit": "を編集",
            "execute": "実行",
            "remove": "削除",
            "save": "保存",
            "saveandapply": "保存して適用",
            "search": "検索",
            "searchtext": "検索中..."
        },
        "attributes": {
            "nogroup": "基本データ"
        },
        "dates": {
            "date": "d/m/Y",
            "datetime": "d/m/Y H:i:s",
            "time": "H:i:s"
        },
        "grid": {
            "list": "リスト",
            "row": "アイテム",
            "rows": "アイテム",
            "subtype": "サブタイプ"
        },
        "tabs": {
            "activity": "アクティビティ",
            "attachments": "添付",
            "card": "カード",
            "details": "詳細",
            "emails": "Eメール",
            "history": "履歴",
            "notes": "ノート",
            "relations": "リレーション"
        }
    },
    "filters": {
        "actions": "アクション",
        "addfilter": "フィルタを追加",
        "any": "すべての",
        "attribute": "属性を選択",
        "attributes": "属性",
        "clearfilter": "フィルタークリアー",
        "clone": "クローン",
        "copyof": "コピー",
        "description": "説明",
        "domain": "アクション",
        "filterdata": "フィルター",
        "fromselection": "選択から",
        "ignore": "除外",
        "migrate": "マイグレート",
        "name": "名前",
        "newfilter": "新規フィルター",
        "noone": "何もない",
        "operator": "演算子",
        "operators": {
            "beginswith": "開始する",
            "between": "間",
            "contained": "含まれる",
            "containedorequal": "含まれるまたは同じ",
            "contains": "含む",
            "containsorequal": "含むまたは同じ",
            "different": "異なる",
            "doesnotbeginwith": "開始しない",
            "doesnotcontain": "含まない",
            "doesnotendwith": "終了しない",
            "endswith": "終了する",
            "equals": "同じ",
            "greaterthan": "大きい",
            "isnotnull": "null値でない",
            "isnull": "null値",
            "lessthan": "小さい"
        },
        "relations": "リレーション",
        "type": "タイプ",
        "typeinput": "入力パラメータ",
        "value": "値"
    },
    "gis": {
        "card": "カード",
        "externalServices": "外部サービス",
        "geographicalAttributes": "地理的属性",
        "geoserverLayers": "Geoサーバレイヤ",
        "layers": "レイヤー",
        "list": "リスト",
        "mapServices": "地図サービス",
        "root": "ルート",
        "tree": "ナビゲーションツリー",
        "view": "ビュー"
    },
    "history": {
        "begindate": "開始日",
        "enddate": "終了日",
        "user": "ユーザー"
    },
    "login": {
        "buttons": {
            "login": "ログイン",
            "logout": "ユーザー変更"
        },
        "fields": {
            "group": "グループ",
            "language": "言語",
            "password": "パスワード",
            "tenants": "テナント",
            "username": "ユーザー名"
        },
        "title": "ログイン"
    },
    "main": {
        "administrationmodule": "アドミニストレーションモジュール",
        "changegroup": "グループ変更",
        "changetenant": "テナント変更",
        "logout": "ログアウト",
        "managementmodule": "データ管理モジュール",
        "multitenant": "マルチテナント",
        "searchinallitems": "全アイテム検索",
        "userpreferences": "表示設定"
    },
    "menu": {
        "allitems": "全アイテム",
        "classes": "クラス",
        "custompages": "カスタムページ",
        "dashboards": "ダッシュボード",
        "processes": "プロセス",
        "reports": "レポート",
        "views": "ビュー"
    },
    "notes": {
        "edit": "ノート編集"
    },
    "notifier": {
        "error": "エラー",
        "genericerror": "エラー",
        "info": "情報",
        "success": "成功",
        "warning": "警告"
    },
    "processes": {
        "action": {
            "advance": "次へ",
            "label": "アクション"
        },
        "startworkflow": "開始",
        "workflow": "ワークフロー"
    },
    "relationGraph": {
        "card": "カード",
        "cardList": "カード一覧",
        "cardRelation": "リレーション",
        "class": "クラス",
        "class:": "クラス",
        "classList": "Cクラス一覧",
        "level": "レベル",
        "openRelationGraph": "リレーショングラフを開く",
        "qt": "Qt",
        "refresh": "リフレッシュ",
        "relation": "リレーション",
        "relationGraph": "リレーショングラフ"
    },
    "relations": {
        "adddetail": "詳細追加",
        "addrelations": "リレーション追加",
        "attributes": "属性",
        "code": "コード",
        "deletedetail": "詳細削除",
        "deleterelation": "リレーション削除",
        "description": "説明",
        "editcard": "カード更新",
        "editdetail": "詳細編集",
        "editrelation": "リレーションを編集",
        "mditems": "items",
        "opencard": "関連するカードを開く",
        "opendetail": "詳細表示",
        "type": "タイプ"
    },
    "widgets": {
        "customform": {
            "addrow": "行を追加",
            "clonerow": "行をクローン",
            "deleterow": "行を削除",
            "editrow": "行を編集",
            "export": "エキスポート",
            "import": "インポート",
            "refresh": "初期設定に戻す"
        },
        "linkcards": {
            "editcard": "カード編集",
            "opencard": "カード表示",
            "refreshselection": "デフォルトを適用",
            "togglefilterdisabled": "グリッドフィルターを無効に",
            "togglefilterenabled": "グリッドフィルターを有効化"
        }
    }
});