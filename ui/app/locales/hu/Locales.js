Ext.define('CMDBuildUI.locales.hu.Locales', {
    "requires": ["CMDBuildUI.locales.hu.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "hu",
    "administration": CMDBuildUI.locales.hu.LocalesAdministration.administration,
    "attachments": {
        "add": "Csatolmány hozzáadása",
        "author": "Szerző",
        "category": "<em>Category</em>",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Leírás",
        "download": "Letöltés",
        "editattachment": "<em>Modify attachment</em>",
        "file": "Fájl",
        "filename": "Fájl neve",
        "majorversion": "<em>Major version</em>",
        "modificationdate": "Módosítás dátuma",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Verzija",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Láthatóság"
        },
        "menu": {
            "camera": "Kamera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Pan",
            "resetView": "<em>Reset View</em>",
            "rotate": "Forgatás",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Kártya hozzáadása a következőhöz: ",
            "clone": "Másolás",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Kártya törlése",
            "modifycard": "Kártya módosítása",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Hozzáadás",
            "apply": "Alkalmaz",
            "cancel": "Mégse",
            "close": "Bezárás",
            "delete": "Törlés",
            "edit": "Szerkesztés",
            "execute": "<em>Execute</em>",
            "remove": "Eltávolítás",
            "save": "Mentés",
            "saveandapply": "Mentés és alkalmazás",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Aktivitás",
            "attachments": "Csatolmányok",
            "card": "Kártya",
            "details": "<em>Details</em>",
            "emails": "<em>Emails</em>",
            "history": "Történet",
            "notes": "Jegyzetek",
            "relations": "Relációk"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Szűrő hozzáadása",
        "any": "Bármely",
        "attribute": "Válasszon attribútumot",
        "attributes": "Attribútumok",
        "clearfilter": "Szűrő törlése",
        "clone": "Másolás",
        "copyof": "Amelyik másolata",
        "description": "Leírás",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Szelektáltakból",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Név",
        "newfilter": "<em>New filter</em>",
        "noone": "Semmi",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Amivel kezdődik",
            "between": "Között",
            "contained": "Tartalmaz",
            "containedorequal": "Tartalmaz vagy egyenlő",
            "contains": "Tartalmazza",
            "containsorequal": "Tartalmaz vagy egyenlő",
            "different": "Különböző",
            "doesnotbeginwith": "Amivel nem kezdődik ",
            "doesnotcontain": "Nem tartalmazza",
            "doesnotendwith": "Amivel nem fejeződik be",
            "endswith": "Amivel befejeződik",
            "equals": "Egyenlő",
            "greaterthan": "Nagyobb mint",
            "isnotnull": "Nem nulla",
            "isnull": "Nulla",
            "lessthan": "Kisebb mint"
        },
        "relations": "Relációk",
        "type": "Típus",
        "typeinput": "Bemeneti paraméter",
        "value": "Érték"
    },
    "gis": {
        "card": "Kártya",
        "externalServices": "Külső szolgáltatás",
        "geographicalAttributes": "Földrajzi attribútumok",
        "geoserverLayers": "Geoserver rétegek",
        "layers": "Rétegek",
        "list": "Lista",
        "mapServices": "<em>Map Services</em>",
        "root": "Root",
        "tree": "Navigációs fa",
        "view": "Nézet"
    },
    "history": {
        "begindate": "Kezdő dátum",
        "enddate": "Befejező dátum",
        "user": "Felhasználó"
    },
    "login": {
        "buttons": {
            "login": "Bejelentkezés",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Nyelv",
            "password": "Jelszó",
            "tenants": "<em>Tenants</em>",
            "username": "Felhasználónév"
        },
        "title": "Bejelentkezés"
    },
    "main": {
        "administrationmodule": "Adminisztrációs modul",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Kijelentkezés",
        "managementmodule": "Adatkezelő modul",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Osztályok",
        "custompages": "<em>Custom pages</em>",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Folyamatok",
        "reports": "<em>Reports</em>",
        "views": "Nézetek"
    },
    "notes": {
        "edit": "Jegyzet szerkesztése"
    },
    "notifier": {
        "error": "Hiba",
        "genericerror": "<em>Generic error</em>",
        "info": "Információ",
        "success": "<em>Success</em>",
        "warning": "<em>Warning</em>"
    },
    "processes": {
        "action": {
            "advance": "Tovább",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Indítás",
        "workflow": "Folyamat"
    },
    "relationGraph": {
        "card": "Kártya",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Reláció",
        "class": "<em>Class</em>",
        "class:": "Osztály",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Relációs gráf megnyitása",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Reláció",
        "relationGraph": "Relációs gráf"
    },
    "relations": {
        "adddetail": "Részlet hozzáadása",
        "addrelations": "Reláció hozzáadása",
        "attributes": "Attribútumok",
        "code": "<em>Code</em>",
        "deletedetail": "Részlet törlése",
        "deleterelation": "Reláció törlése",
        "description": "Leírás",
        "editcard": "Kártya módosítása",
        "editdetail": "Részlet szerkesztése",
        "editrelation": "Reláció szerkesztése",
        "mditems": "<em>items</em>",
        "opencard": "Kapcsolt kártya megnyitása",
        "opendetail": "Részlet megjelenítése",
        "type": "Típus"
    },
    "widgets": {
        "customform": {
            "addrow": "Sor hozzáadása",
            "clonerow": "<em>Clone row</em>",
            "deleterow": "Sor törlése",
            "editrow": "Sor szerkesztése",
            "export": "Exportálás",
            "import": "<em>Import</em>",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "<em>Apply default selection</em>",
            "togglefilterdisabled": "Rács szűrő tiltása",
            "togglefilterenabled": "<em>Rács szűrő engedélyezése</em>"
        }
    }
});