Ext.define('CMDBuildUI.locales.hu.LocalesAdministration', {
    "singleton": true,
    "localization": "hu",
    "administration": {
        "attributes": {
            "attribute": "Attribútum",
            "attributes": "Attribútumok",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktív",
                "description": "Leírás",
                "domain": "Domain",
                "editortype": "Szerkesztő típusa",
                "filter": "Szűrő",
                "group": "Csoport",
                "help": "<em>Help</em>",
                "includeinherited": "Örököltek belefoglalása",
                "iptype": "IP típus",
                "lookup": "Lista",
                "mandatory": "Kötelező",
                "maxlength": "<em>Max Length</em>",
                "mode": "Mód",
                "name": "Név",
                "precision": "Pontosság",
                "preselectifunique": "Előszelektálás ha egyedi",
                "scale": "Skála",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Típus",
                "unique": "Egyedi",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Bármely",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Szerkeszthető",
                "editorhtml": "Html",
                "hidden": "Rejtett",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Egyszerű szöveg",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Csak olvasható",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Aktív",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Mégse",
                "description": "Leírás",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Metaadat szerkesztése",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Kötelező",
                "name": "Név",
                "save": "Mentés",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Típus",
                "unique": "Egyedi",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Törlés",
                "disableattribute": "Tiltás",
                "editattribute": "Szerkesztés",
                "enableattribute": "Engedélyezés",
                "openattribute": "Nyitva",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Törlés"
                                },
                                "edit": {
                                    "tooltip": "Szerkesztés"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "<em>All</em>"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "<em>Current</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Státusz",
                                    "values": {
                                        "active": {
                                            "label": "Státusz"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Törlés"
                                },
                                "editTrigger": {
                                    "tooltip": "Szerkesztés"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Státusz"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktív"
                                },
                                "classType": {
                                    "label": "Típus"
                                },
                                "description": {
                                    "label": "Leírás"
                                },
                                "name": {
                                    "label": "Név"
                                },
                                "parent": {
                                    "label": "<em>Parent</em>"
                                },
                                "superclass": {
                                    "label": "Szuperosztály"
                                }
                            }
                        },
                        "icon": "Ikon",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Státusz"
                    },
                    "values": {
                        "active": "Aktív"
                    }
                },
                "title": "Tulajdonságok",
                "toolbar": {
                    "cancelBtn": "Mégse",
                    "closeBtn": "Bezárás",
                    "deleteBtn": {
                        "tooltip": "Törlés"
                    },
                    "disableBtn": {
                        "tooltip": "Tiltás"
                    },
                    "editBtn": {
                        "tooltip": "Osztály módosítása"
                    },
                    "enableBtn": {
                        "tooltip": "Engedélyezés"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Osztály nyomtatása"
                    },
                    "saveBtn": "Mentés"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Osztályok",
            "toolbar": {
                "addClassBtn": {
                    "text": "Osztály hozzáadása"
                },
                "classLabel": "Osztály",
                "printSchemaBtn": {
                    "text": "Séma nyomtatása"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "<em>All</em>",
                "cancel": "Mégse",
                "clone": "Másolás",
                "close": "Bezárás",
                "create": "Létrehozás",
                "delete": "Törlés",
                "disable": "Tiltás",
                "edit": "Szerkesztés",
                "enable": "Engedélyezés",
                "no": "<em>No</em>",
                "print": "Nyomtatás",
                "save": "Mentés",
                "update": "Frissítés",
                "yes": "<em>Yes</em>"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Figyelem",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Hiba",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Betöltés...",
                "saving": "<em>Saving...</em>",
                "success": "Sikeres",
                "warning": "<em>Warning</em>",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Szerkesztés"
            }
        },
        "domains": {
            "domain": "Domain",
            "fieldlabels": {
                "destination": "Cél",
                "enabled": "Engedélyezve",
                "origin": "Eredet"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Leírás"
                                },
                                "descriptionDirect": {
                                    "label": "Direkt leírás"
                                },
                                "descriptionInverse": {
                                    "label": "Inverz leírás"
                                },
                                "name": {
                                    "label": "Név"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Mégse",
                    "deleteBtn": {
                        "tooltip": "Törlés"
                    },
                    "disableBtn": {
                        "tooltip": "Tiltás"
                    },
                    "editBtn": {
                        "tooltip": "Szerkesztés"
                    },
                    "enableBtn": {
                        "tooltip": "Engedélyezés"
                    },
                    "saveBtn": "Mentés"
                }
            },
            "singularTitle": "Domain",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Tulajdonságok"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Domain hozzáadása"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Aktív",
                "attachments": "Csatolmányok",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Leírás",
                "detail": "Részletek",
                "email": "E-mail",
                "exportcsv": "Exportálás CSV fájlba",
                "filters": "<em>Filters</em>",
                "history": "Történet",
                "importcsvfile": "CSV fájl importálása",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Név",
                "note": "Jegyzetek",
                "relations": "Relációk",
                "type": "Típus",
                "username": "Felhasználónév"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Limitált adminisztrátor",
                "normal": "Normál",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "<em>Default filters</em>",
                "defaultread": "<em>Def. + R.</em>",
                "description": "Leírás",
                "filters": "<em>Filters</em>",
                "group": "Csoport",
                "name": "Név",
                "none": "Semmi",
                "permissions": "Engedélyek",
                "read": "Olvasás",
                "uiconfig": "UI konfiguráció",
                "userslist": "<em>Users list</em>",
                "write": "Írás"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktív"
                                },
                                "name": {
                                    "label": "Név"
                                },
                                "parent": {
                                    "label": "<em>Parent</em>"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Aktív"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Mégse",
                    "closeBtn": "Bezárás",
                    "deleteBtn": {
                        "tooltip": "Törlés"
                    },
                    "editBtn": {
                        "tooltip": "Szerkesztés"
                    },
                    "saveBtn": "Mentés"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Mégse",
                    "closeBtn": "Bezárás",
                    "deleteBtn": {
                        "tooltip": "Törlés"
                    },
                    "editBtn": {
                        "tooltip": "Szerkesztés"
                    },
                    "saveBtn": "Mentés"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "<em>Menu</em>",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Típus tulajdonságok"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Osztályok",
            "custompages": "<em>Custom pages</em>",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domains",
            "email": "E-mail",
            "generaloptions": "Általános opciók",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "<em>Languages</em>",
            "lookuptypes": "<em>Lookup types</em>",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Folyamatok",
            "reports": "<em>Reports</em>",
            "searchfilters": "Kereső szűrő",
            "servermanagement": "Szerver kezelés",
            "simples": "<em>Simples</em>",
            "standard": "Általános",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Feladat kezelő",
            "title": "Navigation",
            "users": "Felhasználók",
            "views": "Nézetek",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktív"
                                },
                                "description": {
                                    "label": "Leírás"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Név"
                                },
                                "parent": {
                                    "label": "Öröklés innen"
                                },
                                "stoppableByUser": {
                                    "label": "Felhasználói megállíthatóság"
                                },
                                "superclass": {
                                    "label": "Szuperosztály"
                                }
                            }
                        },
                        "icon": "Ikon",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Státusz"
                    },
                    "values": {
                        "active": "Aktív"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Mégse",
                    "closeBtn": "Bezárás",
                    "deleteBtn": {
                        "tooltip": "Törlés"
                    },
                    "disableBtn": {
                        "tooltip": "Tiltás"
                    },
                    "editBtn": {
                        "tooltip": "Szerkesztés"
                    },
                    "enableBtn": {
                        "tooltip": "Engedélyezés"
                    },
                    "saveBtn": "Mentés",
                    "versionBtn": {
                        "tooltip": "Verzija"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Folyamat hozzáadása"
                },
                "printSchemaBtn": {
                    "text": "Séma nyomtatása"
                },
                "processLabel": "Folyamat",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktív"
                                },
                                "description": {
                                    "label": "Leírás"
                                },
                                "name": {
                                    "label": "Név"
                                },
                                "stoppableByUser": {
                                    "label": "Felhasználói megállíthatóság"
                                }
                            }
                        },
                        "icon": "Ikon"
                    }
                }
            },
            "title": "Felhasználók",
            "toolbar": {
                "addUserBtn": {
                    "text": "Felhasználó hozzáadása"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});