Ext.define('CMDBuildUI.locales.ar.LocalesAdministration', {
    "singleton": true,
    "localization": "ar",
    "administration": {
        "attributes": {
            "attribute": "السمة",
            "attributes": "السمات",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "مفعل",
                "description": "الوصف",
                "domain": "العلاقة",
                "editortype": "نوع المحرر",
                "filter": "الفرز",
                "group": "المجموعة",
                "help": "<em>Help</em>",
                "includeinherited": "أدرج الموروث",
                "iptype": "نوع العنوان الشبكي",
                "lookup": "القائمة المنسدلة",
                "mandatory": "إجباري",
                "maxlength": "<em>Max Length</em>",
                "mode": "الوضع",
                "name": "الاسم",
                "precision": "عدد الأرقام",
                "preselectifunique": "انتقاء مسبق إذا فريد",
                "scale": "عدد أرقام العشري منها",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "النوع",
                "unique": "فريد",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "أي",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "قابل للتحرير",
                "editorhtml": "Html",
                "hidden": "مخفي",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "نص عادي",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "للقراءة فقط",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "مفعل",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "إلغاء",
                "description": "الوصف",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "تحرير البيانات الوصفية",
                "grouping": "<em>Grouping</em>",
                "mandatory": "إجباري",
                "name": "الاسم",
                "save": "حفظ",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "النوع",
                "unique": "فريد",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "حذف",
                "disableattribute": "معطل",
                "editattribute": "تحرير",
                "enableattribute": "تمكين",
                "openattribute": "مفتوح",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "حذف"
                                },
                                "edit": {
                                    "tooltip": "تحرير"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "الكل"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "الحالي"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "الحالة",
                                    "values": {
                                        "active": {
                                            "label": "الحالة"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "حذف"
                                },
                                "editTrigger": {
                                    "tooltip": "تحرير"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "الحالة"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "مفعل"
                                },
                                "classType": {
                                    "label": "النوع"
                                },
                                "description": {
                                    "label": "الوصف"
                                },
                                "name": {
                                    "label": "الاسم"
                                },
                                "parent": {
                                    "label": "الوالد"
                                },
                                "superclass": {
                                    "label": "صنف أب"
                                }
                            }
                        },
                        "icon": "أيقونة",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "الحالة"
                    },
                    "values": {
                        "active": "مفعل"
                    }
                },
                "title": "الخواص",
                "toolbar": {
                    "cancelBtn": "إلغاء",
                    "closeBtn": "غلق",
                    "deleteBtn": {
                        "tooltip": "حذف"
                    },
                    "disableBtn": {
                        "tooltip": "معطل"
                    },
                    "editBtn": {
                        "tooltip": "تعديل صنف"
                    },
                    "enableBtn": {
                        "tooltip": "تمكين"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "طباعة الصنف"
                    },
                    "saveBtn": "حفظ"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "الأصناف",
            "toolbar": {
                "addClassBtn": {
                    "text": "أضف صنف"
                },
                "classLabel": "الصنف",
                "printSchemaBtn": {
                    "text": "طباعة المخطط"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "الكل",
                "cancel": "إلغاء",
                "clone": "نسخ",
                "close": "غلق",
                "create": "أنشئ",
                "delete": "حذف",
                "disable": "معطل",
                "edit": "تحرير",
                "enable": "تمكين",
                "no": "لا",
                "print": "طباعة",
                "save": "حفظ",
                "update": "تحديث",
                "yes": "نعم"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "انتباه",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "خطأ",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "تحميل...",
                "saving": "<em>Saving...</em>",
                "success": "نجاح",
                "warning": "تنبيه",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "تحرير"
            }
        },
        "domains": {
            "domain": "العلاقة",
            "fieldlabels": {
                "destination": "المنتهى",
                "enabled": "ممكن",
                "origin": "المبتدى"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "الوصف"
                                },
                                "descriptionDirect": {
                                    "label": "وصف مباشر"
                                },
                                "descriptionInverse": {
                                    "label": "وصف معكوس"
                                },
                                "name": {
                                    "label": "الاسم"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "إلغاء",
                    "deleteBtn": {
                        "tooltip": "حذف"
                    },
                    "disableBtn": {
                        "tooltip": "معطل"
                    },
                    "editBtn": {
                        "tooltip": "تحرير"
                    },
                    "enableBtn": {
                        "tooltip": "تمكين"
                    },
                    "saveBtn": "حفظ"
                }
            },
            "singularTitle": "العلاقة",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "الخواص"
            },
            "toolbar": {
                "addBtn": {
                    "text": "إضافة علاقة"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "مفعل",
                "attachments": "المرفقات",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "الوصف",
                "detail": "التفصيل",
                "email": "البريد الإلكتروني",
                "exportcsv": "تصدير ملف CSV",
                "filters": "<em>Filters</em>",
                "history": "التاريخ",
                "importcsvfile": "استيراد ملف CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "الاسم",
                "note": "الملاحظة",
                "relations": "العلاقات",
                "type": "النوع",
                "username": "اسم المستخدم"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "مدير محدود",
                "normal": "عادي",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "الفِرَز الأساسية",
                "defaultread": "Def. + R.",
                "description": "الوصف",
                "filters": "<em>Filters</em>",
                "group": "المجموعة",
                "name": "الاسم",
                "none": "لا",
                "permissions": "التراخيص",
                "read": "قراءة",
                "uiconfig": "إعدادات الواجهة",
                "userslist": "<em>Users list</em>",
                "write": "كتابة"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "مفعل"
                                },
                                "name": {
                                    "label": "الاسم"
                                },
                                "parent": {
                                    "label": "الوالد"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "مفعل"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "إلغاء",
                    "closeBtn": "غلق",
                    "deleteBtn": {
                        "tooltip": "حذف"
                    },
                    "editBtn": {
                        "tooltip": "تحرير"
                    },
                    "saveBtn": "حفظ"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "إلغاء",
                    "closeBtn": "غلق",
                    "deleteBtn": {
                        "tooltip": "حذف"
                    },
                    "editBtn": {
                        "tooltip": "تحرير"
                    },
                    "saveBtn": "حفظ"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "القائمة",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "خواص النوع"
            }
        },
        "navigation": {
            "bim": "نمذجة معلومات المبنى",
            "classes": "الأصناف",
            "custompages": "الصفحات الخيارية",
            "dashboards": "<em>Dashboards</em>",
            "dms": "نظام إدارة الملفات",
            "domains": "العلاقات",
            "email": "البريد الإلكتروني",
            "generaloptions": "الخيارات العامة",
            "gis": "نظام المعلومات الجغرافي",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "اللغات",
            "lookuptypes": "أنواع القائمة المنسدلة",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "الآليات",
            "reports": "<em>Reports</em>",
            "searchfilters": "فِرَز البحث",
            "servermanagement": "إدارة الخادم",
            "simples": "<em>Simples</em>",
            "standard": "قياسي",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "إدارة المهمات",
            "title": "الملاحة",
            "users": "المستخدمين",
            "views": "الاشتقاقات",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "مفعل"
                                },
                                "description": {
                                    "label": "الوصف"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "الاسم"
                                },
                                "parent": {
                                    "label": "يرث من"
                                },
                                "stoppableByUser": {
                                    "label": "المستخدم يمكن إيقافه"
                                },
                                "superclass": {
                                    "label": "صنف أب"
                                }
                            }
                        },
                        "icon": "أيقونة",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "الحالة"
                    },
                    "values": {
                        "active": "مفعل"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "إلغاء",
                    "closeBtn": "غلق",
                    "deleteBtn": {
                        "tooltip": "حذف"
                    },
                    "disableBtn": {
                        "tooltip": "معطل"
                    },
                    "editBtn": {
                        "tooltip": "تحرير"
                    },
                    "enableBtn": {
                        "tooltip": "تمكين"
                    },
                    "saveBtn": "حفظ",
                    "versionBtn": {
                        "tooltip": "الإصدار"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "أضف آلية"
                },
                "printSchemaBtn": {
                    "text": "طباعة المخطط"
                },
                "processLabel": "الآلية",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "مفعل"
                                },
                                "description": {
                                    "label": "الوصف"
                                },
                                "name": {
                                    "label": "الاسم"
                                },
                                "stoppableByUser": {
                                    "label": "المستخدم يمكن إيقافه"
                                }
                            }
                        },
                        "icon": "أيقونة"
                    }
                }
            },
            "title": "المستخدمين",
            "toolbar": {
                "addUserBtn": {
                    "text": "أضف مستخدم"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});