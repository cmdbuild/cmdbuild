Ext.define('CMDBuildUI.locales.ar.Locales', {
    "requires": ["CMDBuildUI.locales.ar.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "ar",
    "administration": CMDBuildUI.locales.ar.LocalesAdministration.administration,
    "attachments": {
        "add": "أضف مرفق",
        "author": "المؤلف",
        "category": "الفئة",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "الوصف",
        "download": "تنزيل",
        "editattachment": "تعديل المرفق",
        "file": "ملف",
        "filename": "اسم الملف",
        "majorversion": "الإصدار الرئيسي",
        "modificationdate": "تاريخ التعديل",
        "uploadfile": "<em>Upload file...</em>",
        "version": "الإصدار",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "إخفاء الكل",
                "showAll": "عرض الكل"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "المرئية"
        },
        "menu": {
            "camera": "الكاميرا",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "طست",
            "resetView": "<em>Reset View</em>",
            "rotate": "تدوير",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "أضف بطاقة",
            "clone": "نسخ",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "حذف بطاقة",
            "modifycard": "تعديل البطاقة",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "أضف",
            "apply": "إعمال",
            "cancel": "إلغاء",
            "close": "غلق",
            "delete": "حذف",
            "edit": "تحرير",
            "execute": "<em>Execute</em>",
            "remove": "حذف",
            "save": "حفظ",
            "saveandapply": "حفظ وإعمال",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "النشاط",
            "attachments": "المرفقات",
            "card": "البطاقة",
            "details": "التفاصيل",
            "emails": "<em>Emails</em>",
            "history": "التاريخ",
            "notes": "الملاحظات",
            "relations": "العلاقات"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "أضف فرز",
        "any": "أي",
        "attribute": "اختر سمة",
        "attributes": "السمات",
        "clearfilter": "مسح الفرز",
        "clone": "نسخ",
        "copyof": "نسخة من",
        "description": "الوصف",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "من المُختار",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "اسم",
        "newfilter": "<em>New filter</em>",
        "noone": "لا أحد",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "يبدأ بـ",
            "between": "بين",
            "contained": "تتضمن",
            "containedorequal": "تتضمن أو تساوي",
            "contains": "تحتوي",
            "containsorequal": "تحتوي أو تساوي",
            "different": "مختلف",
            "doesnotbeginwith": "لا يبدأ بـ ",
            "doesnotcontain": "لا يحتوي على",
            "doesnotendwith": "لا ينتهي بـ",
            "endswith": "ينتهي بـ",
            "equals": "يساوي",
            "greaterthan": "أكبر من",
            "isnotnull": "ليس بفارغ",
            "isnull": "فارغ",
            "lessthan": "أصغر من"
        },
        "relations": "العلاقات",
        "type": "النوع",
        "typeinput": "المعامل المدخل",
        "value": "القيمة"
    },
    "gis": {
        "card": "البطاقة",
        "externalServices": "الخدمات الخارجية",
        "geographicalAttributes": "السمات الجغرافية",
        "geoserverLayers": "طبقات الـ Geoserver",
        "layers": "الطبقات",
        "list": "اللائحة",
        "mapServices": "<em>Map Services</em>",
        "root": "الجذر",
        "tree": "شجرة الملاحة",
        "view": "عرض"
    },
    "history": {
        "begindate": "تاريخ البداية",
        "enddate": "تاريخ النهاية",
        "user": "المستخدم"
    },
    "login": {
        "buttons": {
            "login": "الدخول",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "اللغة",
            "password": "كلمة السر",
            "tenants": "<em>Tenants</em>",
            "username": "اسم المستخدم"
        },
        "title": "الدخول"
    },
    "main": {
        "administrationmodule": "لوحة الإدارة",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "خروج",
        "managementmodule": "وحدة إدارة البيانات",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "الأصناف",
        "custompages": "الصفحات الخيارية",
        "dashboards": "<em>Dashboards</em>",
        "processes": "الآليات",
        "reports": "<em>Reports</em>",
        "views": "الاشتقاقات"
    },
    "notes": {
        "edit": "تعديل الملاحظة"
    },
    "notifier": {
        "error": "خطأ",
        "genericerror": "<em>Generic error</em>",
        "info": "معلومات",
        "success": "نجاح",
        "warning": "تنبيه"
    },
    "processes": {
        "action": {
            "advance": "تقدم",
            "label": "<em>Action</em>"
        },
        "startworkflow": "بدء",
        "workflow": "الآلية"
    },
    "relationGraph": {
        "card": "البطاقة",
        "cardList": "<em>Card List</em>",
        "cardRelation": "العلاقة",
        "class": "<em>Class</em>",
        "class:": "الصنف",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "فتح مخطوطة العلاقات",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "العلاقة",
        "relationGraph": "رسمة العلاقات"
    },
    "relations": {
        "adddetail": "أضف تفصيل",
        "addrelations": "أضف علاقات",
        "attributes": "السمات",
        "code": "الرمز",
        "deletedetail": "حذف تفصيل",
        "deleterelation": "حذف علاقة",
        "description": "الوصف",
        "editcard": "تعديل البطاقة",
        "editdetail": "تحرير تفصيل",
        "editrelation": "تحرير العلاقة",
        "mditems": "<em>items</em>",
        "opencard": "فتح البطاقة ذات الصلة",
        "opendetail": "أظهر التفصيل",
        "type": "النوع"
    },
    "widgets": {
        "customform": {
            "addrow": "أضف صف",
            "clonerow": "انسخ صف",
            "deleterow": "حذف صف",
            "editrow": "تحرير صف",
            "export": "تصدير",
            "import": "استيراد",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "إعمال الخيارات الأساسية",
            "togglefilterdisabled": "تعطيل الفرز على لائحة البطاقات",
            "togglefilterenabled": "تمكين الفرز على لائحة البطاقات"
        }
    }
});