Ext.define('CMDBuildUI.locales.ru.LocalesAdministration', {
    "singleton": true,
    "localization": "ru",
    "administration": {
        "attributes": {
            "attribute": "Атрибут",
            "attributes": "Атрибуты",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Активный",
                "description": "Описание",
                "domain": "Связь",
                "editortype": "Тип редактора",
                "filter": "Фильтр",
                "group": "Группа",
                "help": "<em>Help</em>",
                "includeinherited": "Показывать наследуемые",
                "iptype": "Тип IP",
                "lookup": "Список",
                "mandatory": "Обязательный",
                "maxlength": "<em>Max Length</em>",
                "mode": "Режим",
                "name": "Имя",
                "precision": "Точность",
                "preselectifunique": "Предварительно выберите, если уникально",
                "scale": "Масштаб",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Тип",
                "unique": "Уникальный",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Любой",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Чтение/запись",
                "editorhtml": "Html",
                "hidden": "Скрытый",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Чистый текст",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Только чтение",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Активный",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Отменить",
                "description": "Описание",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Редактировать метаданные",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Обязательный",
                "name": "Имя",
                "save": "Сохранить",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Тип",
                "unique": "Уникальный",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Удаление",
                "disableattribute": "Отключить",
                "editattribute": "Редактировать",
                "enableattribute": "Включить",
                "openattribute": "Открытые",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Удаление"
                                },
                                "edit": {
                                    "tooltip": "Редактировать"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Все"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Текущий"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Состояние",
                                    "values": {
                                        "active": {
                                            "label": "Состояние"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Удаление"
                                },
                                "editTrigger": {
                                    "tooltip": "Редактировать"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Состояние"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активный"
                                },
                                "classType": {
                                    "label": "Тип"
                                },
                                "description": {
                                    "label": "Описание"
                                },
                                "name": {
                                    "label": "Имя"
                                },
                                "parent": {
                                    "label": "Родитель"
                                },
                                "superclass": {
                                    "label": "Суперкласс"
                                }
                            }
                        },
                        "icon": "Значок",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Состояние"
                    },
                    "values": {
                        "active": "Активный"
                    }
                },
                "title": "Свойства",
                "toolbar": {
                    "cancelBtn": "Отменить",
                    "closeBtn": "Закрыть",
                    "deleteBtn": {
                        "tooltip": "Удаление"
                    },
                    "disableBtn": {
                        "tooltip": "Отключить"
                    },
                    "editBtn": {
                        "tooltip": "Редактировать класс"
                    },
                    "enableBtn": {
                        "tooltip": "Включить"
                    },
                    "printBtn": {
                        "printAsOdt": "Документ OpenOffice",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Распечатать"
                    },
                    "saveBtn": "Сохранить"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Классы",
            "toolbar": {
                "addClassBtn": {
                    "text": "Создать класс"
                },
                "classLabel": "Класс",
                "printSchemaBtn": {
                    "text": "Распечатать схему"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Все",
                "cancel": "Отменить",
                "clone": "Дублировать",
                "close": "Закрыть",
                "create": "Создание",
                "delete": "Удаление",
                "disable": "Отключить",
                "edit": "Редактировать",
                "enable": "Включить",
                "no": "Нет",
                "print": "Распечатать",
                "save": "Сохранить",
                "update": "Обновить",
                "yes": "Да"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Внимание",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Ошибка",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Загрузка...",
                "saving": "<em>Saving...</em>",
                "success": "Успешно выполнено",
                "warning": "Предупреждение",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Редактировать"
            }
        },
        "domains": {
            "domain": "Связь",
            "fieldlabels": {
                "destination": "Назначение",
                "enabled": "Включено",
                "origin": "Исходная таблица"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Описание"
                                },
                                "descriptionDirect": {
                                    "label": "Прямое отношение"
                                },
                                "descriptionInverse": {
                                    "label": "Обратное отношение"
                                },
                                "name": {
                                    "label": "Имя"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Отменить",
                    "deleteBtn": {
                        "tooltip": "Удаление"
                    },
                    "disableBtn": {
                        "tooltip": "Отключить"
                    },
                    "editBtn": {
                        "tooltip": "Редактировать"
                    },
                    "enableBtn": {
                        "tooltip": "Включить"
                    },
                    "saveBtn": "Сохранить"
                }
            },
            "singularTitle": "Связь",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Свойства"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Создать связь"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Активный",
                "attachments": "Вложенные файлы",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Описание",
                "detail": "Дополнительно",
                "email": "E-mail",
                "exportcsv": "Экспорт в CSV файл",
                "filters": "<em>Filters</em>",
                "history": "История",
                "importcsvfile": "Импорт из CSV файла",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Имя",
                "note": "Комментарии",
                "relations": "Связи",
                "type": "Тип",
                "username": "Имя пользователя"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Администратор с ограниченным доступом",
                "normal": "Обычный",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Фильтры по умолчанию",
                "defaultread": "Def. + R.",
                "description": "Описание",
                "filters": "<em>Filters</em>",
                "group": "Группа",
                "name": "Имя",
                "none": "Нет",
                "permissions": "Разрешения",
                "read": "Чтение",
                "uiconfig": "Конфигурация интерфейса",
                "userslist": "<em>Users list</em>",
                "write": "Запись"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активный"
                                },
                                "name": {
                                    "label": "Имя"
                                },
                                "parent": {
                                    "label": "Родитель"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Активный"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Отменить",
                    "closeBtn": "Закрыть",
                    "deleteBtn": {
                        "tooltip": "Удаление"
                    },
                    "editBtn": {
                        "tooltip": "Редактировать"
                    },
                    "saveBtn": "Сохранить"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Отменить",
                    "closeBtn": "Закрыть",
                    "deleteBtn": {
                        "tooltip": "Удаление"
                    },
                    "editBtn": {
                        "tooltip": "Редактировать"
                    },
                    "saveBtn": "Сохранить"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Меню",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Тип данных"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Классы",
            "custompages": "Пользовательские страницы",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Связи",
            "email": "E-mail",
            "generaloptions": "Общие настройки",
            "gis": "ГИС",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Языки",
            "lookuptypes": "Перечень",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Рабочие процессы",
            "reports": "<em>Reports</em>",
            "searchfilters": "Поисковые фильтры",
            "servermanagement": "Настройки сервера",
            "simples": "<em>Simples</em>",
            "standard": "Стандартный",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Диспетчер задач",
            "title": "Навигация",
            "users": "Пользователи",
            "views": "Представления",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активный"
                                },
                                "description": {
                                    "label": "Описание"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Имя"
                                },
                                "parent": {
                                    "label": "Наследуется от"
                                },
                                "stoppableByUser": {
                                    "label": "Может быть остановлен пользователем"
                                },
                                "superclass": {
                                    "label": "Суперкласс"
                                }
                            }
                        },
                        "icon": "Значок",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Состояние"
                    },
                    "values": {
                        "active": "Активный"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Отменить",
                    "closeBtn": "Закрыть",
                    "deleteBtn": {
                        "tooltip": "Удаление"
                    },
                    "disableBtn": {
                        "tooltip": "Отключить"
                    },
                    "editBtn": {
                        "tooltip": "Редактировать"
                    },
                    "enableBtn": {
                        "tooltip": "Включить"
                    },
                    "saveBtn": "Сохранить",
                    "versionBtn": {
                        "tooltip": "Версия"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Добавить процесс"
                },
                "printSchemaBtn": {
                    "text": "Распечатать схему"
                },
                "processLabel": "Процесс",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Активный"
                                },
                                "description": {
                                    "label": "Описание"
                                },
                                "name": {
                                    "label": "Имя"
                                },
                                "stoppableByUser": {
                                    "label": "Может быть остановлен пользователем"
                                }
                            }
                        },
                        "icon": "Значок"
                    }
                }
            },
            "title": "Пользователи",
            "toolbar": {
                "addUserBtn": {
                    "text": "Добавить пользователя"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});