Ext.define('CMDBuildUI.locales.ru.Locales', {
    "requires": ["CMDBuildUI.locales.ru.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "ru",
    "administration": CMDBuildUI.locales.ru.LocalesAdministration.administration,
    "attachments": {
        "add": "Вложить файл",
        "author": "Автор",
        "category": "Категория",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Описание",
        "download": "Скачать",
        "editattachment": "Изменить вложение",
        "file": "File",
        "filename": "Имя файла",
        "majorversion": "Основная версия",
        "modificationdate": "Дата изменения",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Версия",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Скрыть все",
                "showAll": "Показать все"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Видимость"
        },
        "menu": {
            "camera": "Камера",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Переместить",
            "resetView": "<em>Reset View</em>",
            "rotate": "Повернуть",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Добавить запись",
            "clone": "Дублировать",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Удалить запись",
            "modifycard": "Редактировать запись",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Добавить",
            "apply": "Применить",
            "cancel": "Отменить",
            "close": "Закрыть",
            "delete": "Удаление",
            "edit": "Редактировать",
            "execute": "<em>Execute</em>",
            "remove": "Удалить",
            "save": "Сохранить",
            "saveandapply": "Сохранить и применить",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Процесс",
            "attachments": "Вложенные файлы",
            "card": "Данные",
            "details": "Подробно",
            "emails": "<em>Emails</em>",
            "history": "История",
            "notes": "Комментарии",
            "relations": "Связи"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Создать фильтр",
        "any": "Любой",
        "attribute": "Выберите атрибут",
        "attributes": "Атрибуты",
        "clearfilter": "Сбросить фильтр",
        "clone": "Дублировать",
        "copyof": "Копия",
        "description": "Описание",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Из отбора",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Имя",
        "newfilter": "<em>New filter</em>",
        "noone": "Ни один из",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "начинается с",
            "between": "между",
            "contained": "Содержится",
            "containedorequal": "Содержится или равен",
            "contains": "содержит",
            "containsorequal": "Содержится или равен",
            "different": "не равно",
            "doesnotbeginwith": "не начинается с",
            "doesnotcontain": "Não contém",
            "doesnotendwith": "не заканчивается на",
            "endswith": "заканчиватся на",
            "equals": "равно",
            "greaterthan": "больше чем",
            "isnotnull": "не null",
            "isnull": "пустое",
            "lessthan": "меньше чем"
        },
        "relations": "Связи",
        "type": "Тип",
        "typeinput": "Переменный параметр",
        "value": "Значение"
    },
    "gis": {
        "card": "Данные",
        "externalServices": "Внешние сервисы",
        "geographicalAttributes": "Гео. атрибуты",
        "geoserverLayers": "Слои Geoserver",
        "layers": "Слои",
        "list": "Список",
        "mapServices": "<em>Map Services</em>",
        "root": "Корень",
        "tree": "Навигационное дерево",
        "view": "Представление данных"
    },
    "history": {
        "begindate": "Начальная дата",
        "enddate": "Конечная дата",
        "user": "Пользователь"
    },
    "login": {
        "buttons": {
            "login": "Вход в систему",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Язык",
            "password": "Пароль",
            "tenants": "<em>Tenants</em>",
            "username": "Имя пользователя"
        },
        "title": "Вход в систему"
    },
    "main": {
        "administrationmodule": "Администрирование",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Выход",
        "managementmodule": "Управление данными",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Классы",
        "custompages": "Пользовательские страницы",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Рабочие процессы",
        "reports": "<em>Reports</em>",
        "views": "Представления"
    },
    "notes": {
        "edit": "Редактировать комментарий"
    },
    "notifier": {
        "error": "Ошибка",
        "genericerror": "<em>Generic error</em>",
        "info": "Информация",
        "success": "Успешно выполнено",
        "warning": "Предупреждение"
    },
    "processes": {
        "action": {
            "advance": "Продолжить",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Начало",
        "workflow": "Процесс"
    },
    "relationGraph": {
        "card": "Данные",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Связь",
        "class": "<em>Class</em>",
        "class:": "Класс",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Показать диаграмму связей",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Связь",
        "relationGraph": "Отображение связей"
    },
    "relations": {
        "adddetail": "Добавить элемент",
        "addrelations": "Добавить связь",
        "attributes": "Атрибуты",
        "code": "Код",
        "deletedetail": "Удалить элемент",
        "deleterelation": "Удалить связь",
        "description": "Описание",
        "editcard": "Редактировать запись",
        "editdetail": "Редактировать элемент",
        "editrelation": "Редактировать связь",
        "mditems": "<em>items</em>",
        "opencard": "Открыть связанную запись",
        "opendetail": "Показать элемент",
        "type": "Тип"
    },
    "widgets": {
        "customform": {
            "addrow": "Добавить строку",
            "clonerow": "Строка клонов",
            "deleterow": "Удалить строку",
            "editrow": "Редактировать строку",
            "export": "Экспорт",
            "import": "Импортировать",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Применить выбор по умолчанию",
            "togglefilterdisabled": "Отключить фильтр сетки",
            "togglefilterenabled": "Включить фильтр сетки"
        }
    }
});