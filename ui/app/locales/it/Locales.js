Ext.define('CMDBuildUI.locales.it.Locales', {
    "requires": ["CMDBuildUI.locales.it.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "it",
    "administration": CMDBuildUI.locales.it.LocalesAdministration.administration,
    "attachments": {
        "add": "Aggiungi allegato",
        "author": "Autore",
        "category": "Categoria",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Descrizione",
        "download": "Scarica",
        "editattachment": "<em>Modifica allegato</em>",
        "file": "File",
        "filename": "Nome del file",
        "majorversion": "<em>Major version</em>",
        "modificationdate": "Data modifica",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Versione",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Nascondi Tutto",
                "showAll": "Mostra Tutto"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Visibilità"
        },
        "menu": {
            "camera": "Camera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Sposta",
            "resetView": "<em>Reset View</em>",
            "rotate": "Ruota",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Aggiungi scheda",
            "clone": "Clona",
            "clonewithrelations": "Clona scheda e relazioni",
            "deletecard": "Cancella scheda",
            "modifycard": "Modifica scheda",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Aggiungi",
            "apply": "Applica",
            "cancel": "Annulla",
            "close": "Chiudi",
            "delete": "Cancella",
            "edit": "Modifica",
            "execute": "<em>Execute</em>",
            "remove": "Rimuovi",
            "save": "Salva",
            "saveandapply": "Salva e applica",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Attività",
            "attachments": "Allegati",
            "card": "Scheda",
            "details": "Dettagli",
            "emails": "<em>Emails</em>",
            "history": "Storia",
            "notes": "Note",
            "relations": "Relazioni"
        }
    },
    "filters": {
        "actions": "Azioni",
        "addfilter": "Aggiungi filtro",
        "any": "Una qualsiasi",
        "attribute": "Scegli un attributo",
        "attributes": "Attributi",
        "clearfilter": "Cancella Filtro",
        "clone": "Clona",
        "copyof": "Copia di",
        "description": "Descrizione",
        "domain": "Azioni",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Dalla selezione",
        "ignore": "Ignora",
        "migrate": "Migra",
        "name": "Nome",
        "newfilter": "<em>New filter</em>",
        "noone": "Nessuna",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Inizia con",
            "between": "Compreso",
            "contained": "Contenuto",
            "containedorequal": "Contenuto o uguale",
            "contains": "Contiene",
            "containsorequal": "Contiene o uguale",
            "different": "Diverso",
            "doesnotbeginwith": "Non inizia con",
            "doesnotcontain": "Non contiene",
            "doesnotendwith": "Non finisce con",
            "endswith": "Finisce con",
            "equals": "Uguale",
            "greaterthan": "Maggiore",
            "isnotnull": "Non è nullo",
            "isnull": "È nullo",
            "lessthan": "Minore"
        },
        "relations": "Relazioni",
        "type": "Tipo",
        "typeinput": "Parametro di input",
        "value": "Valore"
    },
    "gis": {
        "card": "Scheda",
        "externalServices": "Servizi esterni",
        "geographicalAttributes": "Attributi geografici",
        "geoserverLayers": "Layers di Geoserver",
        "layers": "Livelli",
        "list": "Lista",
        "mapServices": "<em>Map Services</em>",
        "root": "Root",
        "tree": "Albero di navigazione",
        "view": "Vista"
    },
    "history": {
        "begindate": "Data inizio",
        "enddate": "Data fine",
        "user": "Utente"
    },
    "login": {
        "buttons": {
            "login": "Accedi",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Lingua",
            "password": "Password",
            "tenants": "<em>Tenants</em>",
            "username": "Username"
        },
        "title": "Accedi"
    },
    "main": {
        "administrationmodule": "Modulo di Amministrazione",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Esci",
        "managementmodule": "Modulo gestione dati",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Classi",
        "custompages": "Pagine custom",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Processi",
        "reports": "<em>Reports</em>",
        "views": "Viste"
    },
    "notes": {
        "edit": "Modifica nota"
    },
    "notifier": {
        "error": "Errore",
        "genericerror": "<em>Generic error</em>",
        "info": "Informazione",
        "success": "Successo",
        "warning": "Attenzione"
    },
    "processes": {
        "action": {
            "advance": "Continua",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Avvia",
        "workflow": "Processo"
    },
    "relationGraph": {
        "card": "Scheda",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Relazioni",
        "class": "<em>Class</em>",
        "class:": "Classe",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Apri grafo delle relazioni",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Relazioni",
        "relationGraph": "Grafo delle relazioni"
    },
    "relations": {
        "adddetail": "Aggiungi Dettaglio",
        "addrelations": "Aggiungi relazioni",
        "attributes": "Attributi",
        "code": "Codice",
        "deletedetail": "Elimina dettaglio",
        "deleterelation": "Cancella relazione",
        "description": "Descrizione",
        "editcard": "Modifica scheda",
        "editdetail": "Modifica dettaglio",
        "editrelation": "Modifica relazione",
        "mditems": "<em>items</em>",
        "opencard": "Apri scheda collegata",
        "opendetail": "Visualizza dettaglio",
        "type": "Tipo"
    },
    "widgets": {
        "customform": {
            "addrow": "Aggiungi riga",
            "clonerow": "Clona riga",
            "deleterow": "Cancella riga",
            "editrow": "Modifica riga",
            "export": "Esporta",
            "import": "Importa",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Applica selezione di default",
            "togglefilterdisabled": "Disabilita filtro griglia",
            "togglefilterenabled": "Abilita filtro griglia"
        }
    }
});