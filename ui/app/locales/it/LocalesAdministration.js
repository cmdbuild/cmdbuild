Ext.define('CMDBuildUI.locales.it.LocalesAdministration', {
    "singleton": true,
    "localization": "it",
    "administration": {
        "attributes": {
            "attribute": "Attributo",
            "attributes": "Attributi",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Attiva",
                "description": "Descrizione",
                "domain": "Dominio",
                "editortype": "Tipo di editor",
                "filter": "Filtro",
                "group": "Gruppo",
                "help": "<em>Help</em>",
                "includeinherited": "Includi ereditati",
                "iptype": "Tipo IP",
                "lookup": "Lookup",
                "mandatory": "Obbligatorio",
                "maxlength": "<em>Max Length</em>",
                "mode": "Modalità",
                "name": "Nome",
                "precision": "Precisione",
                "preselectifunique": "Preseleziona se unico",
                "scale": "Scala",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tipo",
                "unique": "Univoco",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Una qualsiasi",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Modificabile",
                "editorhtml": "Editor HTML",
                "hidden": "Nascosto",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Testo semplice",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Sola lettura",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Attivo",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Annulla",
                "description": "Descrizione",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Modifica metadati",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obbligatorio",
                "name": "Nome",
                "save": "Salva",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tipo",
                "unique": "Univoco",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Cancella",
                "disableattribute": "Disabilita",
                "editattribute": "Modifica",
                "enableattribute": "Abilita",
                "openattribute": "Attivo",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Cancella"
                                },
                                "edit": {
                                    "tooltip": "Modifica"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Tutti"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Attuale"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Stato",
                                    "values": {
                                        "active": {
                                            "label": "Stato"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Cancella"
                                },
                                "editTrigger": {
                                    "tooltip": "Modifica"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Stato"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Attiva"
                                },
                                "classType": {
                                    "label": "Tipo"
                                },
                                "description": {
                                    "label": "Descrizione"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Padre"
                                },
                                "superclass": {
                                    "label": "Superclasse"
                                }
                            }
                        },
                        "icon": "Icona",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Stato"
                    },
                    "values": {
                        "active": "Attiva"
                    }
                },
                "title": "Proprietà",
                "toolbar": {
                    "cancelBtn": "Annulla",
                    "closeBtn": "Chiudi",
                    "deleteBtn": {
                        "tooltip": "Cancella"
                    },
                    "disableBtn": {
                        "tooltip": "Disabilita"
                    },
                    "editBtn": {
                        "tooltip": "<em>Modify class</em>"
                    },
                    "enableBtn": {
                        "tooltip": "Abilita"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Stampa classe"
                    },
                    "saveBtn": "Salva"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Classi",
            "toolbar": {
                "addClassBtn": {
                    "text": "Aggiungi classe"
                },
                "classLabel": "Classe",
                "printSchemaBtn": {
                    "text": "Stampa schema"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Tutti",
                "cancel": "Annulla",
                "clone": "Clona",
                "close": "Chiudi",
                "create": "Crea",
                "delete": "Cancella",
                "disable": "Disabilita",
                "edit": "Modifica",
                "enable": "Abilita",
                "no": "No",
                "print": "Stampa",
                "save": "Salva",
                "update": "Aggiorna",
                "yes": "Si"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Attenzione",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Errore",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Caricamento in corso...",
                "saving": "<em>Saving...</em>",
                "success": "Successo",
                "warning": "Attenzione",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Modifica"
            }
        },
        "domains": {
            "domain": "Dominio",
            "fieldlabels": {
                "destination": "Destinazione",
                "enabled": "Abilitato",
                "origin": "Origine"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Descrizione"
                                },
                                "descriptionDirect": {
                                    "label": "Descrizione diretta"
                                },
                                "descriptionInverse": {
                                    "label": "Descrizione inversa"
                                },
                                "name": {
                                    "label": "Nome"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Annulla",
                    "deleteBtn": {
                        "tooltip": "Cancella"
                    },
                    "disableBtn": {
                        "tooltip": "Disabilita"
                    },
                    "editBtn": {
                        "tooltip": "Modifica"
                    },
                    "enableBtn": {
                        "tooltip": "Abilita"
                    },
                    "saveBtn": "Salva"
                }
            },
            "singularTitle": "Dominio",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Proprietà"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Aggiungi dominio"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "Azioni",
                "active": "Attivo",
                "attachments": "Allegati",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Descrizione",
                "detail": "Dettagli",
                "email": "E-mail",
                "exportcsv": "Esporta file CSV",
                "filters": "<em>Filters</em>",
                "history": "Storia",
                "importcsvfile": "Importa file CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Nome",
                "note": "Note",
                "relations": "Relazioni",
                "type": "Tipo",
                "username": "Username"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Amministratore limitato",
                "normal": "Normale",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Filtri di default",
                "defaultread": "Def. + R.",
                "description": "Descrizione",
                "filters": "<em>Filters</em>",
                "group": "Gruppo",
                "name": "Nome",
                "none": "Nessuno",
                "permissions": "Permessi",
                "read": "Lettura",
                "uiconfig": "Configurazione UI",
                "userslist": "<em>Users list</em>",
                "write": "Scrittura"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Attivo"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Padre"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Attivo"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Annulla",
                    "closeBtn": "Chiudi",
                    "deleteBtn": {
                        "tooltip": "Cancella"
                    },
                    "editBtn": {
                        "tooltip": "Modifica"
                    },
                    "saveBtn": "Salva"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Annulla",
                    "closeBtn": "Chiudi",
                    "deleteBtn": {
                        "tooltip": "Cancella"
                    },
                    "editBtn": {
                        "tooltip": "Modifica"
                    },
                    "saveBtn": "Salva"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menu",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Proprietà del tipo"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Classi",
            "custompages": "Pagine custom",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domini",
            "email": "E-mail",
            "generaloptions": "Opzioni generali",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Lingue",
            "lookuptypes": "Tipo lookup",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Processi",
            "reports": "<em>Reports</em>",
            "searchfilters": "Filtri di ricerca",
            "servermanagement": "Gestione server",
            "simples": "<em>Simples</em>",
            "standard": "Standard",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Task manager",
            "title": "Navigazione",
            "users": "Utenti",
            "views": "Viste",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Attivo"
                                },
                                "description": {
                                    "label": "Descrizione"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "parent": {
                                    "label": "Eredita da"
                                },
                                "stoppableByUser": {
                                    "label": "Fermabile dall'utente"
                                },
                                "superclass": {
                                    "label": "Superclasse"
                                }
                            }
                        },
                        "icon": "Icona",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Stato"
                    },
                    "values": {
                        "active": "Attivo"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Annulla",
                    "closeBtn": "Chiudi",
                    "deleteBtn": {
                        "tooltip": "Cancella"
                    },
                    "disableBtn": {
                        "tooltip": "Disabilita"
                    },
                    "editBtn": {
                        "tooltip": "Modifica"
                    },
                    "enableBtn": {
                        "tooltip": "Abilita"
                    },
                    "saveBtn": "Salva",
                    "versionBtn": {
                        "tooltip": "Versione"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Aggiungi processo"
                },
                "printSchemaBtn": {
                    "text": "Stampa schema"
                },
                "processLabel": "Processo",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Attivo"
                                },
                                "description": {
                                    "label": "Descrizione"
                                },
                                "name": {
                                    "label": "Nome"
                                },
                                "stoppableByUser": {
                                    "label": "Fermabile dall'utente"
                                }
                            }
                        },
                        "icon": "Icona"
                    }
                }
            },
            "title": "Utenti",
            "toolbar": {
                "addUserBtn": {
                    "text": "Aggiungi utente"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});