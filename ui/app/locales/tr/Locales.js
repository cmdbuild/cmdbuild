Ext.define('CMDBuildUI.locales.tr.Locales', {
    "requires": ["CMDBuildUI.locales.tr.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "tr",
    "administration": CMDBuildUI.locales.tr.LocalesAdministration.administration,
    "attachments": {
        "add": "Ek ekle",
        "author": "Yazar",
        "category": "Kategori",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Açıklama",
        "download": "İndir",
        "editattachment": "Eki değiştir",
        "file": "Dosya",
        "filename": "Dosya Adı",
        "majorversion": "Geçerli versiyon",
        "modificationdate": "Düzenlenme Tarihi",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Versiyon",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Tümünü Gizle",
                "showAll": "Tümünü Göster"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Görüş Mesafesi"
        },
        "menu": {
            "camera": "Kamera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Kaydırma",
            "resetView": "<em>Reset View</em>",
            "rotate": "Döndür",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Kart Ekle",
            "clone": "Klonla",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Kartı Sil",
            "modifycard": "Kartı değiştir",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Ekle",
            "apply": "Uygula",
            "cancel": "İptal",
            "close": "Kapat",
            "delete": "Sil",
            "edit": "Düzenle",
            "execute": "<em>Execute</em>",
            "remove": "Kaldır",
            "save": "Kaydet",
            "saveandapply": "Kaydet ve uygula",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Etkinlik",
            "attachments": "Ekler",
            "card": "Kart",
            "details": "Detaylar",
            "emails": "<em>Emails</em>",
            "history": "Tarihçe",
            "notes": "Notlar",
            "relations": "İlişkiler"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Filtre Ekle",
        "any": "Hiçbiri",
        "attribute": "Bir nitelik seçin",
        "attributes": "Öznitellikler",
        "clearfilter": "Filtreyi Temizle",
        "clone": "Klonla",
        "copyof": "için Kopya",
        "description": "Açıklama",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Seçimden",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrates</em>",
        "name": "Adı",
        "newfilter": "<em>New filter</em>",
        "noone": "Hiç kimse",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "İle başlar",
            "between": "Arasında",
            "contained": "Varolan",
            "containedorequal": "Varolan veya eşit",
            "contains": "Kapsananlar",
            "containsorequal": "Kapsar veya eşittir",
            "different": "Farklı",
            "doesnotbeginwith": "Başlamaz",
            "doesnotcontain": "İçermiyor",
            "doesnotendwith": "Bitmiyor",
            "endswith": "İle biter",
            "equals": "Eşittir",
            "greaterthan": "Bundan büyük",
            "isnotnull": "Boş değil",
            "isnull": "Boş",
            "lessthan": "Bundan küçük"
        },
        "relations": "İlişkiler",
        "type": "Tip",
        "typeinput": "örnek Parametre",
        "value": "Değer"
    },
    "gis": {
        "card": "Kart",
        "externalServices": "Dış Hizmetler",
        "geographicalAttributes": "Coğrafi özellikler",
        "geoserverLayers": "CBS server katmanları",
        "layers": "Katmanlar",
        "list": "Liste",
        "mapServices": "<em>Map Services</em>",
        "root": "Kök dizin",
        "tree": "Navigasyon ağacı",
        "view": "Görünüm"
    },
    "history": {
        "begindate": "Başlangıç tarihi",
        "enddate": "Bitiş tarihi",
        "user": "Kullanıcı"
    },
    "login": {
        "buttons": {
            "login": "Giriş",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Dil",
            "password": "Şifre",
            "tenants": "<em>Tenants</em>",
            "username": "Kullanıcı Adı"
        },
        "title": "Kullanıcı Girişi"
    },
    "main": {
        "administrationmodule": "Admin Paneli",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Çıkış",
        "managementmodule": "Veri yönetimi modülü",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Sınıflar",
        "custompages": "Özel sayfalar",
        "dashboards": "<em>Dashboards</em>",
        "processes": "İşlemler",
        "reports": "<em>Reports</em>",
        "views": "Görünümler"
    },
    "notes": {
        "edit": "Notu değiştir"
    },
    "notifier": {
        "error": "Hata",
        "genericerror": "<em>Generic error</em>",
        "info": "Bilgi",
        "success": "Başarılı",
        "warning": "UYARI"
    },
    "processes": {
        "action": {
            "advance": "İleri Düzey",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Başlangıç",
        "workflow": "Süreç"
    },
    "relationGraph": {
        "card": "Kart",
        "cardList": "<em>Card List</em>",
        "cardRelation": "İlişki",
        "class": "<em>Class</em>",
        "class:": "Sınıf",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "ilişki grafiğini Aç",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "İlişki",
        "relationGraph": "Grafik ilişkileri"
    },
    "relations": {
        "adddetail": "Ayrıntı ekle",
        "addrelations": "İlişki Ekle",
        "attributes": "Öznitellikler",
        "code": "Kod",
        "deletedetail": "Ayrıntı Sil",
        "deleterelation": "Ilişkiyi sil",
        "description": "Açıklama",
        "editcard": "Kartı değiştir",
        "editdetail": "Ayrıntı Düzenle",
        "editrelation": "İlişkim Düzenle",
        "mditems": "<em>items</em>",
        "opencard": "Ilgili kartı aç",
        "opendetail": "Detayları göster",
        "type": "Tip"
    },
    "widgets": {
        "customform": {
            "addrow": "Satır Ekle",
            "clonerow": "Satırı Klonla",
            "deleterow": "Satır Sil",
            "editrow": "Satırı düzenle",
            "export": "Dışarı Aktar",
            "import": "İçeri Aktar",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Varsayılan seçimi uygula",
            "togglefilterdisabled": "Izgara filtresini devre dışı bırak",
            "togglefilterenabled": "Izgara filtresini etkinleştir"
        }
    }
});