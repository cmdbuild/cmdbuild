Ext.define('CMDBuildUI.locales.tr.LocalesAdministration', {
    "singleton": true,
    "localization": "tr",
    "administration": {
        "attributes": {
            "attribute": "Nitelik",
            "attributes": "Öznitellikler",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktif",
                "description": "Açıklama",
                "domain": "Domain",
                "editortype": "Editör Türü",
                "filter": "Filtre",
                "group": "Grup",
                "help": "<em>Help</em>",
                "includeinherited": "Miras a eklemek",
                "iptype": "IP Tipi",
                "lookup": "Sorgu",
                "mandatory": "Zorunlu",
                "maxlength": "<em>Max Length</em>",
                "mode": "Durum",
                "name": "Adı",
                "precision": "Hassasiyet",
                "preselectifunique": "Benzersizleri Önceden Seç",
                "scale": "Ölçek",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tip",
                "unique": "Benzersiz",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Hiçbiri",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Düzenlenebilir",
                "editorhtml": "Html",
                "hidden": "Gizli",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Düz Yazı",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Okunabilir",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Etkin",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "İptal",
                "description": "Açıklama",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Meta verileri düzenle",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Zorunlu",
                "name": "Adı",
                "save": "Kaydet",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tip",
                "unique": "Benzersiz",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Sil",
                "disableattribute": "Devre dışı",
                "editattribute": "Düzenle",
                "enableattribute": "Etkinleştirme",
                "openattribute": "Açık",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Sil"
                                },
                                "edit": {
                                    "tooltip": "Düzenle"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Tümü"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "şimdiki"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Durum",
                                    "values": {
                                        "active": {
                                            "label": "Durum"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Sil"
                                },
                                "editTrigger": {
                                    "tooltip": "Düzenle"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Durum"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktif"
                                },
                                "classType": {
                                    "label": "Tip"
                                },
                                "description": {
                                    "label": "Açıklama"
                                },
                                "name": {
                                    "label": "Adı"
                                },
                                "parent": {
                                    "label": "Ebeveyn"
                                },
                                "superclass": {
                                    "label": "Üst sınıf"
                                }
                            }
                        },
                        "icon": "Icon",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Durum"
                    },
                    "values": {
                        "active": "Aktif"
                    }
                },
                "title": "Özellikleri",
                "toolbar": {
                    "cancelBtn": "İptal",
                    "closeBtn": "Kapat",
                    "deleteBtn": {
                        "tooltip": "Sil"
                    },
                    "disableBtn": {
                        "tooltip": "Devre dışı"
                    },
                    "editBtn": {
                        "tooltip": "Sınıfı değiştir"
                    },
                    "enableBtn": {
                        "tooltip": "Etkinleştirme"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Sınıfları Yazdır"
                    },
                    "saveBtn": "Kaydet"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Sınıflar",
            "toolbar": {
                "addClassBtn": {
                    "text": "Sınıf Ekle"
                },
                "classLabel": "Sınıf",
                "printSchemaBtn": {
                    "text": "Şemayı yazdır"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Tümü",
                "cancel": "İptal",
                "clone": "Klonla",
                "close": "Kapat",
                "create": "Oluştur",
                "delete": "Sil",
                "disable": "Devre dışı",
                "edit": "Düzenle",
                "enable": "Etkinleştirme",
                "no": "Hayır",
                "print": "Yazdır",
                "save": "Kaydet",
                "update": "Güncelle",
                "yes": "Evet"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Dikkat",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Hata",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Yükleniyor...",
                "saving": "<em>Saving...</em>",
                "success": "Başarı",
                "warning": "UYARI",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Düzenle"
            }
        },
        "domains": {
            "domain": "Domain",
            "fieldlabels": {
                "destination": "Hedef",
                "enabled": "Etkin",
                "origin": "Başlangıç noktası"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Açıklama"
                                },
                                "descriptionDirect": {
                                    "label": "Doğrudan Açıklama"
                                },
                                "descriptionInverse": {
                                    "label": "Tersi Açıklama"
                                },
                                "name": {
                                    "label": "Adı"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "İptal",
                    "deleteBtn": {
                        "tooltip": "Sil"
                    },
                    "disableBtn": {
                        "tooltip": "Devre dışı"
                    },
                    "editBtn": {
                        "tooltip": "Düzenle"
                    },
                    "enableBtn": {
                        "tooltip": "Etkinleştirme"
                    },
                    "saveBtn": "Kaydet"
                }
            },
            "singularTitle": "Domain",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Özellikleri"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Domain Ekle"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Etkin",
                "attachments": "Ekler",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Açıklama",
                "detail": "Detay",
                "email": "E-mail",
                "exportcsv": "CSV dosyası dışa aktarma",
                "filters": "<em>Filters</em>",
                "history": "Tarihçe",
                "importcsvfile": "CSV Dosyası içeri aktar",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Adı",
                "note": "Not",
                "relations": "İlişkiler",
                "type": "Tip",
                "username": "Kullanıcı Adı"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Sınırlı yönetici",
                "normal": "Normal",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Varsayılan Filtreler",
                "defaultread": "Varsayılan + R.",
                "description": "Açıklama",
                "filters": "<em>Filters</em>",
                "group": "Grup",
                "name": "Adı",
                "none": "Yok",
                "permissions": "İzinler",
                "read": "Oku",
                "uiconfig": "UI yapılandırması",
                "userslist": "<em>Users list</em>",
                "write": "Yaz"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Etkin"
                                },
                                "name": {
                                    "label": "Adı"
                                },
                                "parent": {
                                    "label": "Ebeveyn"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Etkin"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "İptal",
                    "closeBtn": "Kapat",
                    "deleteBtn": {
                        "tooltip": "Sil"
                    },
                    "editBtn": {
                        "tooltip": "Düzenle"
                    },
                    "saveBtn": "Kaydet"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "İptal",
                    "closeBtn": "Kapat",
                    "deleteBtn": {
                        "tooltip": "Sil"
                    },
                    "editBtn": {
                        "tooltip": "Düzenle"
                    },
                    "saveBtn": "Kaydet"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Menu",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Tip Özellikleri"
            }
        },
        "navigation": {
            "bim": "BIM/BBG (Building Information Modeling = Bina Bilgi Modellemesi)",
            "classes": "Sınıflar",
            "custompages": "Özel sayfalar",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domains",
            "email": "E-mail",
            "generaloptions": "General Ayarlar",
            "gis": "CBS/GIS (Coğrafi Bilgi Sistemi = Geographycal informations system)",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Diller",
            "lookuptypes": "Arama Tipi",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "İşlemler",
            "reports": "<em>Reports</em>",
            "searchfilters": "Arama filtreleri",
            "servermanagement": "Sunucu yönetimi",
            "simples": "<em>Simples</em>",
            "standard": "Standart",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Görev Yöneticisi",
            "title": "Navigasyon",
            "users": "Kullanıcılar",
            "views": "Görünümler",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Etkin"
                                },
                                "description": {
                                    "label": "Açıklama"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Adı"
                                },
                                "parent": {
                                    "label": "Devir Alınan"
                                },
                                "stoppableByUser": {
                                    "label": "Kullanıcı Engellenebilir"
                                },
                                "superclass": {
                                    "label": "süper Sınıf"
                                }
                            }
                        },
                        "icon": "Icon",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Durum"
                    },
                    "values": {
                        "active": "Etkin"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "İptal",
                    "closeBtn": "Kapat",
                    "deleteBtn": {
                        "tooltip": "Sil"
                    },
                    "disableBtn": {
                        "tooltip": "Devre dışı"
                    },
                    "editBtn": {
                        "tooltip": "Düzenle"
                    },
                    "enableBtn": {
                        "tooltip": "Etkinleştirme"
                    },
                    "saveBtn": "Kaydet",
                    "versionBtn": {
                        "tooltip": "Versiyon"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "İşlem Ekle"
                },
                "printSchemaBtn": {
                    "text": "Şemayı yazdır"
                },
                "processLabel": "Süreç",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Etkin"
                                },
                                "description": {
                                    "label": "Açıklama"
                                },
                                "name": {
                                    "label": "Adı"
                                },
                                "stoppableByUser": {
                                    "label": "Kullanıcı Engellenebilir"
                                }
                            }
                        },
                        "icon": "Icon"
                    }
                }
            },
            "title": "Kullanıcılar",
            "toolbar": {
                "addUserBtn": {
                    "text": "Kullanıcı Ekle"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});