Ext.define('CMDBuildUI.locales.zh_CN.LocalesAdministration', {
    "singleton": true,
    "localization": "zh_CN",
    "administration": {
        "attributes": {
            "attribute": "属性",
            "attributes": "属性",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "活动状态",
                "description": "描述",
                "domain": "域",
                "editortype": "编辑器类型",
                "filter": "过滤器",
                "group": "组",
                "help": "<em>Help</em>",
                "includeinherited": "包括继承的",
                "iptype": "IP类型",
                "lookup": "Lookup",
                "mandatory": "强制型",
                "maxlength": "<em>Max Length</em>",
                "mode": "模式",
                "name": "名称",
                "precision": "精度",
                "preselectifunique": "如果唯一，可以预选",
                "scale": "比例",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "类型",
                "unique": "唯一性",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "任何",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "可编辑",
                "editorhtml": "Html",
                "hidden": "隐藏",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "纯文本",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "只读",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "活动状态",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "取消",
                "description": "描述",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "编辑元数据",
                "grouping": "<em>Grouping</em>",
                "mandatory": "强制型",
                "name": "名称",
                "save": "保存",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "类型",
                "unique": "唯一性",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "删除",
                "disableattribute": "无效的",
                "editattribute": "编辑",
                "enableattribute": "使生效",
                "openattribute": "打开",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "删除"
                                },
                                "edit": {
                                    "tooltip": "编辑"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "<em>All</em>"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "<em>Current</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "状态",
                                    "values": {
                                        "active": {
                                            "label": "状态"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "删除"
                                },
                                "editTrigger": {
                                    "tooltip": "编辑"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "状态"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "活动状态"
                                },
                                "classType": {
                                    "label": "类型"
                                },
                                "description": {
                                    "label": "描述"
                                },
                                "name": {
                                    "label": "名称"
                                },
                                "parent": {
                                    "label": "<em>Parent</em>"
                                },
                                "superclass": {
                                    "label": "超类"
                                }
                            }
                        },
                        "icon": "图标",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "状态"
                    },
                    "values": {
                        "active": "活动状态"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "取消",
                    "closeBtn": "关闭",
                    "deleteBtn": {
                        "tooltip": "删除"
                    },
                    "disableBtn": {
                        "tooltip": "无效的"
                    },
                    "editBtn": {
                        "tooltip": "变更类"
                    },
                    "enableBtn": {
                        "tooltip": "使生效"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "打印类"
                    },
                    "saveBtn": "保存"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "<em>Classes</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "增加类"
                },
                "classLabel": "类",
                "printSchemaBtn": {
                    "text": "打印图线"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "<em>All</em>",
                "cancel": "取消",
                "clone": "克隆",
                "close": "关闭",
                "create": "创建",
                "delete": "删除",
                "disable": "无效的",
                "edit": "编辑",
                "enable": "使生效",
                "no": "<em>No</em>",
                "print": "打印",
                "save": "保存",
                "update": "更新",
                "yes": "<em>Yes</em>"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "注意　",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "错误",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "加载...",
                "saving": "<em>Saving...</em>",
                "success": "成功",
                "warning": "<em>Warning</em>",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "编辑"
            }
        },
        "domains": {
            "domain": "域",
            "fieldlabels": {
                "destination": "目的地",
                "enabled": "使生效的",
                "origin": "起源"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "描述"
                                },
                                "descriptionDirect": {
                                    "label": "直接描述"
                                },
                                "descriptionInverse": {
                                    "label": "反向描述"
                                },
                                "name": {
                                    "label": "名称"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "取消",
                    "deleteBtn": {
                        "tooltip": "删除"
                    },
                    "disableBtn": {
                        "tooltip": "无效的"
                    },
                    "editBtn": {
                        "tooltip": "编辑"
                    },
                    "enableBtn": {
                        "tooltip": "使生效"
                    },
                    "saveBtn": "保存"
                }
            },
            "singularTitle": "域",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "<em>Properties</em>"
            },
            "toolbar": {
                "addBtn": {
                    "text": "增加域"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "活动状态",
                "attachments": "<em>Attachments</em>",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "描述",
                "detail": "细节",
                "email": "E-mail",
                "exportcsv": "导出CSV文件",
                "filters": "<em>Filters</em>",
                "history": "历史",
                "importcsvfile": "导入CSV文件",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "名称",
                "note": "注释",
                "relations": "<em>Relations</em>",
                "type": "类型",
                "username": "用户名"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "<em>Limited administrator</em>",
                "normal": "<em>Normal</em>",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "<em>Default filters</em>",
                "defaultread": "<em>Def. + R.</em>",
                "description": "描述",
                "filters": "<em>Filters</em>",
                "group": "组",
                "name": "名称",
                "none": "<em>None</em>",
                "permissions": "<em>Permissions</em>",
                "read": "<em>Read</em>",
                "uiconfig": "<em>UI configuration</em>",
                "userslist": "<em>Users list</em>",
                "write": "<em>Write</em>"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "活动状态"
                                },
                                "name": {
                                    "label": "名称"
                                },
                                "parent": {
                                    "label": "<em>Parent</em>"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "活动状态"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "取消",
                    "closeBtn": "关闭",
                    "deleteBtn": {
                        "tooltip": "删除"
                    },
                    "editBtn": {
                        "tooltip": "编辑"
                    },
                    "saveBtn": "保存"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "取消",
                    "closeBtn": "关闭",
                    "deleteBtn": {
                        "tooltip": "删除"
                    },
                    "editBtn": {
                        "tooltip": "编辑"
                    },
                    "saveBtn": "保存"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "<em>Menu</em>",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "类型特性"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "<em>Classes</em>",
            "custompages": "<em>Custom pages</em>",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "<em>Domains</em>",
            "email": "E-mail",
            "generaloptions": "一般的选择",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "<em>Languages</em>",
            "lookuptypes": "<em>Lookup types</em>",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "<em>Processes</em>",
            "reports": "<em>Reports</em>",
            "searchfilters": "搜索过滤器",
            "servermanagement": "服务器管理",
            "simples": "<em>Simples</em>",
            "standard": "<em>Standard</em>",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "任务管理者",
            "title": "<em>Navigation</em>",
            "users": "用户",
            "views": "浏览",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "活动状态"
                                },
                                "description": {
                                    "label": "描述"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "名称"
                                },
                                "parent": {
                                    "label": "从...继承"
                                },
                                "stoppableByUser": {
                                    "label": "用户可停止的"
                                },
                                "superclass": {
                                    "label": "超类"
                                }
                            }
                        },
                        "icon": "图标",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "状态"
                    },
                    "values": {
                        "active": "活动状态"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "取消",
                    "closeBtn": "关闭",
                    "deleteBtn": {
                        "tooltip": "删除"
                    },
                    "disableBtn": {
                        "tooltip": "无效的"
                    },
                    "editBtn": {
                        "tooltip": "编辑"
                    },
                    "enableBtn": {
                        "tooltip": "使生效"
                    },
                    "saveBtn": "保存",
                    "versionBtn": {
                        "tooltip": "版本"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "增加进程"
                },
                "printSchemaBtn": {
                    "text": "打印图线"
                },
                "processLabel": "过程",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "活动状态"
                                },
                                "description": {
                                    "label": "描述"
                                },
                                "name": {
                                    "label": "名称"
                                },
                                "stoppableByUser": {
                                    "label": "用户可停止的"
                                }
                            }
                        },
                        "icon": "图标"
                    }
                }
            },
            "title": "用户",
            "toolbar": {
                "addUserBtn": {
                    "text": "增加用户"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});