Ext.define('CMDBuildUI.locales.zh_CN.Locales', {
    "requires": ["CMDBuildUI.locales.zh_CN.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "zh_CN",
    "administration": CMDBuildUI.locales.zh_CN.LocalesAdministration.administration,
    "attachments": {
        "add": "增加附件",
        "author": "作者",
        "category": "<em>Category</em>",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "描述",
        "download": "下载",
        "editattachment": "<em>Modifica allegato</em>",
        "file": "<em>File</em>",
        "filename": "文件名称",
        "majorversion": "<em>Major version</em>",
        "modificationdate": "变更日期",
        "uploadfile": "<em>Upload file...</em>",
        "version": "版本",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "可视"
        },
        "menu": {
            "camera": "相机",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Pan",
            "resetView": "<em>Reset View</em>",
            "rotate": "旋转",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "增加卡片",
            "clone": "克隆",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "删除卡片",
            "modifycard": "修改卡片",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "增加",
            "apply": "应用",
            "cancel": "取消",
            "close": "关闭",
            "delete": "删除",
            "edit": "编辑",
            "execute": "<em>Execute</em>",
            "remove": "删除",
            "save": "保存",
            "saveandapply": "保存并应用",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "活动",
            "attachments": "<em>Attachments</em>",
            "card": "卡片",
            "details": "<em>Details</em>",
            "emails": "<em>Emails</em>",
            "history": "历史",
            "notes": "<em>Notes</em>",
            "relations": "<em>Relations</em>"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "增加过滤器",
        "any": "任何",
        "attribute": "选择一个属性",
        "attributes": "属性",
        "clearfilter": "清除过滤器",
        "clone": "克隆",
        "copyof": "拷贝",
        "description": "描述",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "从...选择",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "名称",
        "newfilter": "<em>New filter</em>",
        "noone": "没人",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "以　开始",
            "between": "在　之间",
            "contained": "受限的",
            "containedorequal": "受限或相当的",
            "contains": "包涵",
            "containsorequal": "Contains or equal",
            "different": "不同",
            "doesnotbeginwith": "不以...开始",
            "doesnotcontain": "为包括",
            "doesnotendwith": "不以...结束",
            "endswith": "以...结束",
            "equals": "相等",
            "greaterthan": "大于",
            "isnotnull": "是非零型",
            "isnull": "是非零型",
            "lessthan": "小于"
        },
        "relations": "<em>Relations</em>",
        "type": "类型",
        "typeinput": "输入参数",
        "value": "值"
    },
    "gis": {
        "card": "卡片",
        "externalServices": "外部服务",
        "geographicalAttributes": "地理属性",
        "geoserverLayers": "地理信息服务器层",
        "layers": "层",
        "list": "清单",
        "mapServices": "<em>Map Services</em>",
        "root": "根",
        "tree": "导航树",
        "view": "浏览"
    },
    "history": {
        "begindate": "开始日期",
        "enddate": "结束日期",
        "user": "用户"
    },
    "login": {
        "buttons": {
            "login": "<em>Login</em>",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "语言",
            "password": "密码",
            "tenants": "<em>Tenants</em>",
            "username": "用户名"
        },
        "title": "<em>Login</em>"
    },
    "main": {
        "administrationmodule": "管理模块",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "退出",
        "managementmodule": "数据管理模块",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "<em>Classes</em>",
        "custompages": "<em>Custom pages</em>",
        "dashboards": "<em>Dashboards</em>",
        "processes": "<em>Processes</em>",
        "reports": "<em>Reports</em>",
        "views": "浏览"
    },
    "notes": {
        "edit": "编辑注释"
    },
    "notifier": {
        "error": "错误",
        "genericerror": "<em>Generic error</em>",
        "info": "信息",
        "success": "<em>Success</em>",
        "warning": "<em>Warning</em>"
    },
    "processes": {
        "action": {
            "advance": "高级",
            "label": "<em>Action</em>"
        },
        "startworkflow": "开始",
        "workflow": "过程"
    },
    "relationGraph": {
        "card": "卡片",
        "cardList": "<em>Card List</em>",
        "cardRelation": "关系",
        "class": "<em>Class</em>",
        "class:": "类",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "打开关系图表",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "关系",
        "relationGraph": "关系图"
    },
    "relations": {
        "adddetail": "增加细节",
        "addrelations": "增加关系",
        "attributes": "属性",
        "code": "<em>Code</em>",
        "deletedetail": "删除细节",
        "deleterelation": "删除关系",
        "description": "描述",
        "editcard": "修改卡片",
        "editdetail": "编辑细节",
        "editrelation": "编辑关系",
        "mditems": "<em>items</em>",
        "opencard": "打开关系卡片",
        "opendetail": "显示细节",
        "type": "类型"
    },
    "widgets": {
        "customform": {
            "addrow": "增加行",
            "clonerow": "<em>Clone row</em>",
            "deleterow": "删除行",
            "editrow": "编辑行",
            "export": "导出",
            "import": "<em>Import</em>",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "<em>Apply default selection</em>",
            "togglefilterdisabled": "无效的方格过滤器",
            "togglefilterenabled": "使方格过滤器生效"
        }
    }
});