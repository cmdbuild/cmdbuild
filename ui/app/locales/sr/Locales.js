Ext.define('CMDBuildUI.locales.sr.Locales', {
    "requires": ["CMDBuildUI.locales.sr.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "sr",
    "administration": CMDBuildUI.locales.sr.LocalesAdministration.administration,
    "attachments": {
        "add": "Dodaj prilog",
        "author": "Autor",
        "category": "Kategorija",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Opis",
        "download": "Preuzmi",
        "editattachment": "Modifikuj prilog",
        "file": "Datoteka",
        "filename": "Naziv datoteke",
        "majorversion": "Glavna verzija",
        "modificationdate": "Datum izmene",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Verzija",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Vidljivost"
        },
        "menu": {
            "camera": "Kamera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Pomeranje",
            "resetView": "<em>Reset View</em>",
            "rotate": "Rotiraj",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Dodaj karticu",
            "clone": "Kloniraj",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Ukloni karticu",
            "modifycard": "Izmeni karticu",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Dodaj",
            "apply": "Primeni",
            "cancel": "Odustani",
            "close": "Zatvori",
            "delete": "Ukloni",
            "edit": "Izmeni",
            "execute": "<em>Execute</em>",
            "remove": "Ukloni",
            "save": "Snimi",
            "saveandapply": "Snimi i primeni",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Aktivnost",
            "attachments": "Prilozi",
            "card": "Kartica",
            "details": "Detalji",
            "emails": "<em>Emails</em>",
            "history": "Istorija",
            "notes": "Napomene",
            "relations": "Relacije"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Dodaj filter",
        "any": "Bilo koji",
        "attribute": "Izaberi atribut",
        "attributes": "Atributi",
        "clearfilter": "Očisti filter",
        "clone": "Kloniraj",
        "copyof": "Kopija",
        "description": "Opis",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Iz selekcije",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Naziv",
        "newfilter": "<em>New filter</em>",
        "noone": "Nijedan",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Koji počinju sa",
            "between": "Između",
            "contained": "Sadržan",
            "containedorequal": "Sadržan ili jednak",
            "contains": "Sadrži",
            "containsorequal": "Sadrži ili je jednak",
            "different": "Različite od",
            "doesnotbeginwith": "Koji ne počinju sa",
            "doesnotcontain": "Koji ne sadrže",
            "doesnotendwith": "Ne završava sa",
            "endswith": "Završava sa",
            "equals": "Jednake",
            "greaterthan": "Veće",
            "isnotnull": "Ne može biti null",
            "isnull": "Sa null vrednošću",
            "lessthan": "Manje"
        },
        "relations": "Relacije",
        "type": "Tip",
        "typeinput": "Ulazni parametar",
        "value": "Vrednosti"
    },
    "gis": {
        "card": "Kartica",
        "externalServices": "Spoljni servisi",
        "geographicalAttributes": "Geografski atributi",
        "geoserverLayers": "Geoserver slojevi",
        "layers": "Slojevi",
        "list": "Lista",
        "mapServices": "<em>Map Services</em>",
        "root": "Koren",
        "tree": "Stablo navigacije",
        "view": "Prikaz"
    },
    "history": {
        "begindate": "Datum početka",
        "enddate": "Datum završetka",
        "user": "Korisnik"
    },
    "login": {
        "buttons": {
            "login": "Prijavi se",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Jezik",
            "password": "Lozinka",
            "tenants": "<em>Tenants</em>",
            "username": "Korisničko ime"
        },
        "title": "Prijavi se"
    },
    "main": {
        "administrationmodule": "Administracioni modul",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Izađi",
        "managementmodule": "Modul za upravljanje podacima",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Klase",
        "custompages": "Posebne stranice",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Kartice procesa",
        "reports": "<em>Reports</em>",
        "views": "Prikazi"
    },
    "notes": {
        "edit": "Izmeni napomenu"
    },
    "notifier": {
        "error": "Greška",
        "genericerror": "<em>Generic error</em>",
        "info": "Informacija",
        "success": "Uspeh",
        "warning": "Pažnja"
    },
    "processes": {
        "action": {
            "advance": "Dalje",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Start",
        "workflow": "Radni procesi"
    },
    "relationGraph": {
        "card": "Kartica",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Veza",
        "class": "<em>Class</em>",
        "class:": "Klasa",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Otvori graf relacija",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Veza",
        "relationGraph": "Graf relacija"
    },
    "relations": {
        "adddetail": "Dodaj detalje",
        "addrelations": "Dodaj relaciju",
        "attributes": "Atributi",
        "code": "Kod",
        "deletedetail": "Ukloni detalj",
        "deleterelation": "Ukloni relaciju",
        "description": "Opis",
        "editcard": "Izmeni karticu",
        "editdetail": "Izmeni detalje",
        "editrelation": "Izmeni relaciju",
        "mditems": "<em>items</em>",
        "opencard": "Otvori pripadajuću karticu ",
        "opendetail": "Prikaži detalje",
        "type": "Tip"
    },
    "widgets": {
        "customform": {
            "addrow": "Dodaj red",
            "clonerow": "Kloniraj red",
            "deleterow": "Obriši red",
            "editrow": "Izmeni red",
            "export": "Izvezi",
            "import": "Uvezi",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Primeni podrazumevani izbor",
            "togglefilterdisabled": "Isključi filtriranje tabele",
            "togglefilterenabled": "Uključi filtriranje tabele"
        }
    }
});