Ext.define('CMDBuildUI.locales.sr.LocalesAdministration', {
    "singleton": true,
    "localization": "sr",
    "administration": {
        "attributes": {
            "attribute": "Atribut",
            "attributes": "Atributi",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktivna",
                "description": "Opis",
                "domain": "Relacija",
                "editortype": "Vrsta editora",
                "filter": "Filter",
                "group": "Grupa",
                "help": "<em>Help</em>",
                "includeinherited": "Uključujući nasleđene",
                "iptype": "IP tip",
                "lookup": "Šifarnik",
                "mandatory": "Obavezan",
                "maxlength": "<em>Max Length</em>",
                "mode": "Režim",
                "name": "Naziv",
                "precision": "Preciznost",
                "preselectifunique": "Selektuj ako je jedinstven",
                "scale": "Skala",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tip",
                "unique": "Jedinstven",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Bilo koji",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Dozvoljene izmene",
                "editorhtml": "Html",
                "hidden": "Skriven",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Obični tekst",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Samo za čitanje",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Aktivan",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Odustani",
                "description": "Opis",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Izmeni metapodatke",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obavezan",
                "name": "Naziv",
                "save": "Snimi",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tip",
                "unique": "Jedinstven",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Ukloni",
                "disableattribute": "Onemogući",
                "editattribute": "Izmeni",
                "enableattribute": "Omogući",
                "openattribute": "Aktivan",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Ukloni"
                                },
                                "edit": {
                                    "tooltip": "Izmeni"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Sve"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Trenutni"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Status",
                                    "values": {
                                        "active": {
                                            "label": "Status"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Ukloni"
                                },
                                "editTrigger": {
                                    "tooltip": "Izmeni"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Status"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivna"
                                },
                                "classType": {
                                    "label": "Tip"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "parent": {
                                    "label": "Roditelj"
                                },
                                "superclass": {
                                    "label": "Superklasa"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktivna"
                    }
                },
                "title": "Obeležja",
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "Ukloni"
                    },
                    "disableBtn": {
                        "tooltip": "Onemogući"
                    },
                    "editBtn": {
                        "tooltip": "Izmeni klasu"
                    },
                    "enableBtn": {
                        "tooltip": "Omogući"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Odštampaj klasu"
                    },
                    "saveBtn": "Snimi"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Klase",
            "toolbar": {
                "addClassBtn": {
                    "text": "Dodaj klasu"
                },
                "classLabel": "Klasa",
                "printSchemaBtn": {
                    "text": "Odštampaj šemu"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Sve",
                "cancel": "Odustani",
                "clone": "Kloniraj",
                "close": "Zatvori",
                "create": "Kreiraj",
                "delete": "Ukloni",
                "disable": "Onemogući",
                "edit": "Izmeni",
                "enable": "Omogući",
                "no": "Ne",
                "print": "Štampaj",
                "save": "Snimi",
                "update": "Ažuriraj",
                "yes": "Da"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Pažnja",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Greška",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Učitavanje u toku...",
                "saving": "<em>Saving...</em>",
                "success": "Uspeh",
                "warning": "Pažnja",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Izmeni"
            }
        },
        "domains": {
            "domain": "Relacija",
            "fieldlabels": {
                "destination": "Odredište",
                "enabled": "Uključen",
                "origin": "Polazna klasa"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Opis"
                                },
                                "descriptionDirect": {
                                    "label": "Direktni opis"
                                },
                                "descriptionInverse": {
                                    "label": "Inverzni opis"
                                },
                                "name": {
                                    "label": "Naziv"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "deleteBtn": {
                        "tooltip": "Ukloni"
                    },
                    "disableBtn": {
                        "tooltip": "Onemogući"
                    },
                    "editBtn": {
                        "tooltip": "Izmeni"
                    },
                    "enableBtn": {
                        "tooltip": "Omogući"
                    },
                    "saveBtn": "Snimi"
                }
            },
            "singularTitle": "Relacija",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Obeležja"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Dodaj relaciju"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Aktivan",
                "attachments": "Prilozi",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Opis",
                "detail": "Detalji",
                "email": "E-pošta",
                "exportcsv": "Izvezi CSV datoteku",
                "filters": "<em>Filters</em>",
                "history": "Istorija",
                "importcsvfile": "Uvezi CSV datoteku",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Naziv",
                "note": "Napomena",
                "relations": "Relacije",
                "type": "Tip",
                "username": "Korisničko ime"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Administrator s ograničenim pravima",
                "normal": "Normalno",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Podrazumevani filter",
                "defaultread": "Def. + R.",
                "description": "Opis",
                "filters": "<em>Filters</em>",
                "group": "Grupa",
                "name": "Naziv",
                "none": "Bez ikakvih prava",
                "permissions": "Prava",
                "read": "Čitanje",
                "uiconfig": "Konfiguracija IO",
                "userslist": "<em>Users list</em>",
                "write": "Pisanje"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivan"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "parent": {
                                    "label": "Roditelj"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Aktivan"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "Ukloni"
                    },
                    "editBtn": {
                        "tooltip": "Izmeni"
                    },
                    "saveBtn": "Snimi"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "Ukloni"
                    },
                    "editBtn": {
                        "tooltip": "Izmeni"
                    },
                    "saveBtn": "Snimi"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Meni",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Obeležja tipa"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Klase",
            "custompages": "Posebne stranice",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Relacije",
            "email": "E-pošta",
            "generaloptions": "Opšta podešavanja",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Jezici",
            "lookuptypes": "Tip u šifarniku",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Kartice procesa",
            "reports": "<em>Reports</em>",
            "searchfilters": "Filteri za pretraživanje",
            "servermanagement": "Podešavanje servera",
            "simples": "<em>Simples</em>",
            "standard": "Standardna",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Upravljanje zadacima",
            "title": "Navigacija",
            "users": "Korisnici",
            "views": "Prikazi",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivan"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "parent": {
                                    "label": "Naslednik"
                                },
                                "stoppableByUser": {
                                    "label": "Korisnik može prekinuti proces"
                                },
                                "superclass": {
                                    "label": "Superklasa"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Status"
                    },
                    "values": {
                        "active": "Aktivan"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "Ukloni"
                    },
                    "disableBtn": {
                        "tooltip": "Onemogući"
                    },
                    "editBtn": {
                        "tooltip": "Izmeni"
                    },
                    "enableBtn": {
                        "tooltip": "Omogući"
                    },
                    "saveBtn": "Snimi",
                    "versionBtn": {
                        "tooltip": "Verzija"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Dodaj proces"
                },
                "printSchemaBtn": {
                    "text": "Odštampaj šemu"
                },
                "processLabel": "Radni procesi",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivan"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "stoppableByUser": {
                                    "label": "Korisnik može prekinuti proces"
                                }
                            }
                        },
                        "icon": "Ikona"
                    }
                }
            },
            "title": "Korisnici",
            "toolbar": {
                "addUserBtn": {
                    "text": "Dodaj korisnika"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});