Ext.define('CMDBuildUI.locales.hr.Locales', {
    "requires": ["CMDBuildUI.locales.hr.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "hr",
    "administration": CMDBuildUI.locales.hr.LocalesAdministration.administration,
    "attachments": {
        "add": "Dodaj prilog",
        "author": "Autor",
        "category": "<em>Category</em>",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Opis",
        "download": "<em>Download</em>",
        "editattachment": "<em>Modify attachment</em>",
        "file": "Datoteka",
        "filename": "<em>File name</em>",
        "majorversion": "<em>Major version</em>",
        "modificationdate": "Datum izmjene",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Version",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "<em>Hide all</em>",
                "showAll": "<em>Show all</em>"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Vidljivost"
        },
        "menu": {
            "camera": "<em>Camera</em>",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "<em>Pan</em>",
            "resetView": "<em>Reset View</em>",
            "rotate": "<em>Rotate</em>",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Dodaj karticu",
            "clone": "Kloniraj",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Ukloni karticu",
            "modifycard": "Izmjeni karticu",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Dodaj",
            "apply": "Primjeni",
            "cancel": "Odustani",
            "close": "Zatvori",
            "delete": "<em>Delete</em>",
            "edit": "<em>Edit</em>",
            "execute": "<em>Execute</em>",
            "remove": "Ukloni",
            "save": "Potvrdi",
            "saveandapply": "Snimi i primjeni",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Aktivnost",
            "attachments": "Prilozi",
            "card": "Kartica",
            "details": "Detalji",
            "emails": "<em>Emails</em>",
            "history": "Povijest",
            "notes": "Napomene",
            "relations": "Relacije"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Dodaj filter",
        "any": "Bilo koji",
        "attribute": "Izaberi atribut",
        "attributes": "Atributi",
        "clearfilter": "Očisti Filter",
        "clone": "Kloniraj",
        "copyof": "Kopija",
        "description": "Opis",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Iz odabira",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Naziv",
        "newfilter": "<em>New filter</em>",
        "noone": "Nijedan",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Koji počinju sa",
            "between": "Između",
            "contained": "<em>Contained</em>",
            "containedorequal": "<em>Contained or equal</em>",
            "contains": "Koji sadrže",
            "containsorequal": "<em>Contains or equal</em>",
            "different": "Različite od",
            "doesnotbeginwith": "Koji ne počinju sa",
            "doesnotcontain": "Koji ne sadrže",
            "doesnotendwith": "Ne završava sa",
            "endswith": "Završava sa",
            "equals": "Jednake",
            "greaterthan": "Veće",
            "isnotnull": "Ne može biti null",
            "isnull": "Sa null vrijednošću",
            "lessthan": "Manje"
        },
        "relations": "Relacije",
        "type": "Tip",
        "typeinput": "Ulazni Parametar",
        "value": "<em>Value</em>"
    },
    "gis": {
        "card": "Kartica",
        "externalServices": "Vanjski servisi",
        "geographicalAttributes": "Zemljopisni atributi",
        "geoserverLayers": "Geoserver slojevi",
        "layers": "<em>Layers</em>",
        "list": "Lista",
        "mapServices": "<em>Map Services</em>",
        "root": "<em>Root</em>",
        "tree": "<em>Navigation tree</em>",
        "view": "<em>View</em>"
    },
    "history": {
        "begindate": "Datum početka",
        "enddate": "Datum završetka",
        "user": "Korisnik"
    },
    "login": {
        "buttons": {
            "login": "Prijavi se",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "<em>Language</em>",
            "password": "<em>Password</em>",
            "tenants": "<em>Tenants</em>",
            "username": "<em>Username</em>"
        },
        "title": "Prijavi se"
    },
    "main": {
        "administrationmodule": "Upravljački modul",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Izađi",
        "managementmodule": "Modul za upravljanje podacima",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Klase",
        "custompages": "<em>Custom pages</em>",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Kartice procesa",
        "reports": "<em>Reports</em>",
        "views": "Prikazi"
    },
    "notes": {
        "edit": "Izmjeni napomenu"
    },
    "notifier": {
        "error": "Greška",
        "genericerror": "<em>Generic error</em>",
        "info": "Informacija",
        "success": "Uspjeh",
        "warning": "Pažnja"
    },
    "processes": {
        "action": {
            "advance": "Dalje",
            "label": "<em>Action</em>"
        },
        "startworkflow": "<em>Start</em>",
        "workflow": "<em>Workflow</em>"
    },
    "relationGraph": {
        "card": "Kartica",
        "cardList": "<em>Card List</em>",
        "cardRelation": "<em>Relation</em>",
        "class": "<em>Class</em>",
        "class:": "<em>Class</em>",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Otvori graf relacija",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "<em>Relation</em>",
        "relationGraph": "Graf relacija"
    },
    "relations": {
        "adddetail": "Dodaj detalje",
        "addrelations": "Dodaj relaciju",
        "attributes": "Atributi",
        "code": "Kod",
        "deletedetail": "Ukloni detalj",
        "deleterelation": "Ukloni relaciju",
        "description": "Opis",
        "editcard": "Izmjeni karticu",
        "editdetail": "Izmjeni detalje",
        "editrelation": "Izmjeni relaciju",
        "mditems": "<em>items</em>",
        "opencard": "Otvori pripadajuću karticu ",
        "opendetail": "Prikaži detalje",
        "type": "Tip"
    },
    "widgets": {
        "customform": {
            "addrow": "<em>Add row</em>",
            "clonerow": "<em>Clone row</em>",
            "deleterow": "<em>Delete row</em>",
            "editrow": "<em>Edit row</em>",
            "export": "Izvezi",
            "import": "<em>Import</em>",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "<em>Apply default selection</em>",
            "togglefilterdisabled": "<em>Disable grid filter</em>",
            "togglefilterenabled": "<em>Enable grid filter</em>"
        }
    }
});