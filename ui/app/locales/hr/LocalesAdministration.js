Ext.define('CMDBuildUI.locales.hr.LocalesAdministration', {
    "singleton": true,
    "localization": "hr",
    "administration": {
        "attributes": {
            "attribute": "<em>Attribute</em>",
            "attributes": "Atributi",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Aktivna",
                "description": "Opis",
                "domain": "Domena",
                "editortype": "Vrsta editora",
                "filter": "Filter",
                "group": "Grupa",
                "help": "<em>Help</em>",
                "includeinherited": "Uključujući naslijeđene",
                "iptype": "<em>IP type</em>",
                "lookup": "Šifarnik",
                "mandatory": "Obavezan",
                "maxlength": "<em>Max Length</em>",
                "mode": "<em>Mode</em>",
                "name": "Naziv",
                "precision": "Preciznost",
                "preselectifunique": "<em>Preselect if unique</em>",
                "scale": "Skala",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Tip",
                "unique": "Jedinstven",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Bilo koji",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Dozvoljene izmjene",
                "editorhtml": "Html",
                "hidden": "Skriven",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Obični tekst",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Samo za čitanje",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Aktivan",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Odustani",
                "description": "Opis",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Izmjeni metapodatke",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Obavezan",
                "name": "Naziv",
                "save": "Potvrdi",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Tip",
                "unique": "Jedinstven",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "<em>Delete</em>",
                "disableattribute": "<em>Disable</em>",
                "editattribute": "<em>Edit</em>",
                "enableattribute": "<em>Enable</em>",
                "openattribute": "Aktivan",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "<em>Delete</em>"
                                },
                                "edit": {
                                    "tooltip": "<em>Edit</em>"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "<em>All</em>"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "<em>Current</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "<em>Status</em>",
                                    "values": {
                                        "active": {
                                            "label": "<em>Status</em>"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "<em>Delete</em>"
                                },
                                "editTrigger": {
                                    "tooltip": "<em>Edit</em>"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "<em>Status</em>"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivna"
                                },
                                "classType": {
                                    "label": "Tip"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "parent": {
                                    "label": "Roditelj"
                                },
                                "superclass": {
                                    "label": "Superklasa"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "<em>Status</em>"
                    },
                    "values": {
                        "active": "Aktivna"
                    }
                },
                "title": "Obilježja",
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "Izmijeni klasu"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "printBtn": {
                        "printAsOdt": "<em>OpenOffice Odt</em>",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Ispiši klasu"
                    },
                    "saveBtn": "Potvrdi"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Klase",
            "toolbar": {
                "addClassBtn": {
                    "text": "Dodaj klasu"
                },
                "classLabel": "<em>Class</em>",
                "printSchemaBtn": {
                    "text": "Ispiši shemu"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "<em>All</em>",
                "cancel": "Odustani",
                "clone": "Kloniraj",
                "close": "Zatvori",
                "create": "<em>Create</em>",
                "delete": "<em>Delete</em>",
                "disable": "<em>Disable</em>",
                "edit": "<em>Edit</em>",
                "enable": "<em>Enable</em>",
                "no": "<em>No</em>",
                "print": "Ispiši",
                "save": "Potvrdi",
                "update": "Ažuriraj",
                "yes": "<em>Yes</em>"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Pažnja",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Greška",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Učitavanje u tijeku...",
                "saving": "<em>Saving...</em>",
                "success": "Uspjeh",
                "warning": "Pažnja",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "<em>Edit</em>"
            }
        },
        "domains": {
            "domain": "Domena",
            "fieldlabels": {
                "destination": "Odredište",
                "enabled": "Uključen",
                "origin": "Početna klasa"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Opis"
                                },
                                "descriptionDirect": {
                                    "label": "Izravni opis"
                                },
                                "descriptionInverse": {
                                    "label": "Inverzni opis"
                                },
                                "name": {
                                    "label": "Naziv"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "saveBtn": "Potvrdi"
                }
            },
            "singularTitle": "Domena",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Obilježja"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Dodaj domenu"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Aktivan",
                "attachments": "Prilozi",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Opis",
                "detail": "Detalji",
                "email": "<em>E-mail</em>",
                "exportcsv": "Izvezi CSV datoteku",
                "filters": "<em>Filters</em>",
                "history": "Povijest",
                "importcsvfile": "Uvezi CSV datoteku",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Naziv",
                "note": "Napomena",
                "relations": "Relacije",
                "type": "Tip",
                "username": "<em>Username</em>"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Administrator s ograničenim pravima",
                "normal": "Normalno",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "<em>Default filters</em>",
                "defaultread": "<em>Def. + R.</em>",
                "description": "Opis",
                "filters": "<em>Filters</em>",
                "group": "Grupa",
                "name": "Naziv",
                "none": "Bez ikakvih",
                "permissions": "Prava",
                "read": "Čitanje",
                "uiconfig": "Podešavanje IO",
                "userslist": "<em>Users list</em>",
                "write": "Pisanje"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivan"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "parent": {
                                    "label": "Roditelj"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Aktivan"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "saveBtn": "Potvrdi"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "saveBtn": "Potvrdi"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Meni",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Obilježja tipa"
            }
        },
        "navigation": {
            "bim": "<em>BIM</em>",
            "classes": "Klase",
            "custompages": "<em>Custom pages</em>",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Domene",
            "email": "<em>E-mail</em>",
            "generaloptions": "<em>General options</em>",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "<em>Languages</em>",
            "lookuptypes": "Tip u šifarniku",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Kartice procesa",
            "reports": "<em>Reports</em>",
            "searchfilters": "Filteri za pretraživanje",
            "servermanagement": "Postavke servera",
            "simples": "<em>Simples</em>",
            "standard": "Standardna",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "<em>Task manager</em>",
            "title": "Navigacija",
            "users": "Korisnici",
            "views": "Prikazi",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivan"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "parent": {
                                    "label": "Nasljednik"
                                },
                                "stoppableByUser": {
                                    "label": "Korisnik može prekinuti proces"
                                },
                                "superclass": {
                                    "label": "Superklasa"
                                }
                            }
                        },
                        "icon": "Ikona",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "<em>Status</em>"
                    },
                    "values": {
                        "active": "Aktivan"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Odustani",
                    "closeBtn": "Zatvori",
                    "deleteBtn": {
                        "tooltip": "<em>Delete</em>"
                    },
                    "disableBtn": {
                        "tooltip": "<em>Disable</em>"
                    },
                    "editBtn": {
                        "tooltip": "<em>Edit</em>"
                    },
                    "enableBtn": {
                        "tooltip": "<em>Enable</em>"
                    },
                    "saveBtn": "Potvrdi",
                    "versionBtn": {
                        "tooltip": "Version"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Dodaj proces"
                },
                "printSchemaBtn": {
                    "text": "Ispiši shemu"
                },
                "processLabel": "<em>Workflow</em>",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Aktivan"
                                },
                                "description": {
                                    "label": "Opis"
                                },
                                "name": {
                                    "label": "Naziv"
                                },
                                "stoppableByUser": {
                                    "label": "Korisnik može prekinuti proces"
                                }
                            }
                        },
                        "icon": "Ikona"
                    }
                }
            },
            "title": "Korisnici",
            "toolbar": {
                "addUserBtn": {
                    "text": "Dodaj korisnika"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});