Ext.define('CMDBuildUI.locales.el_GR.Locales', {
    "requires": ["CMDBuildUI.locales.el_GR.LocalesAdministration"],
    "override": "CMDBuildUI.locales.Locales",
    "singleton": true,
    "localization": "el_GR",
    "administration": CMDBuildUI.locales.el_GR.LocalesAdministration.administration,
    "attachments": {
        "add": "Προσθήκη συνημμένου",
        "author": "Συγγραφέας",
        "category": "Κατηγορία",
        "creationdate": "<em>Creation date</em>",
        "deleteattachment": "<em>Delete attachment</em>",
        "deleteattachment_confirmation": "<em>Are you sure you want to delete this attachment?</em>",
        "description": "Περιγραφή",
        "download": "Κατέβασμα",
        "editattachment": "Τροποποίηση συνημμένου",
        "file": "Αρχείο",
        "filename": "Όνομα αρχείου",
        "majorversion": "Κύρια έκδοση",
        "modificationdate": "Ημερομηνία τροποποίησης",
        "uploadfile": "<em>Upload file...</em>",
        "version": "Έκδοση",
        "viewhistory": "<em>View attachment history</em>"
    },
    "bim": {
        "bimViewer": "<em>Bim Viewer</em>",
        "layers": {
            "label": "<em>Layers</em>",
            "menu": {
                "hideAll": "Απόκρυψη όλων",
                "showAll": "Εμφάνιση όλων"
            },
            "name": "<em>Name</em>",
            "qt": "<em>Qt</em>",
            "visibility": "<em>Visibility</em>",
            "visivility": "Ορατότητα"
        },
        "menu": {
            "camera": "Camera",
            "frontView": "<em>Front View</em>",
            "mod": "<em>mod</em>",
            "pan": "Pan",
            "resetView": "<em>Reset View</em>",
            "rotate": "Περιστροφή",
            "sideView": "<em>Side View</em>",
            "topView": "<em>Top View</em>"
        },
        "showBimCard": "<em>BimCard</em>",
        "tree": {
            "arrowTooltip": "<em>TODO</em>",
            "columnLabel": "<em>Tree</em>",
            "label": "<em>Tree</em>"
        }
    },
    "classes": {
        "cards": {
            "addcard": "Προσθήκη κάρτας",
            "clone": "Κλώνος",
            "clonewithrelations": "<em>Clone card and relations</em>",
            "deletecard": "Διαγραφή κάρτας",
            "modifycard": "Τροποποίηση κάρτας",
            "opencard": "<em>Open card</em>"
        }
    },
    "common": {
        "actions": {
            "add": "Προσθήκη",
            "apply": "Εφαρμογή",
            "cancel": "Ακύρωση",
            "close": "Κλείσιμο",
            "delete": "Διαγραφή",
            "edit": "Επεξεργασία",
            "execute": "<em>Execute</em>",
            "remove": "Αφαίρεση",
            "save": "Αποθήκευση",
            "saveandapply": "Αποθήκευση και εφαρμογή",
            "search": "<em>Search</em>",
            "searchtext": "<em>Search...</em>"
        },
        "attributes": {
            "nogroup": "<em>Base data</em>"
        },
        "dates": {
            "date": "<em>d/m/Y</em>",
            "datetime": "<em>d/m/Y H:i:s</em>",
            "time": "<em>H:i:s</em>"
        },
        "grid": {
            "list": "<em>List</em>",
            "row": "<em>Item</em>",
            "rows": "<em>Items</em>",
            "subtype": "<em>Subtype</em>"
        },
        "tabs": {
            "activity": "Δραστηριότητα",
            "attachments": "Συνημμένα αρχεία",
            "card": "Κάρτα",
            "details": "Λεπτομέρειες",
            "emails": "<em>Emails</em>",
            "history": "Ιστορικό",
            "notes": "Σημειώσεις",
            "relations": "Συσχετίσεις"
        }
    },
    "filters": {
        "actions": "<em>Actions</em>",
        "addfilter": "Προσθήκη φίλτρου",
        "any": "Οποιοδήποτε",
        "attribute": "Επιλογή χαρακτηριστικού",
        "attributes": "Χαρακτηριστικά",
        "clearfilter": "Καθαρισμός φίλτρου",
        "clone": "Κλώνος",
        "copyof": "Αντίγραφο του",
        "description": "Περιγραφή",
        "domain": "<em>Actions</em>",
        "filterdata": "<em>Filter data</em>",
        "fromselection": "Από επιλογή",
        "ignore": "<em>Ignore</em>",
        "migrate": "<em>Migrate</em>",
        "name": "Όνομα",
        "newfilter": "<em>New filter</em>",
        "noone": "Κανένα",
        "operator": "<em>Operator</em>",
        "operators": {
            "beginswith": "Έναρξη με",
            "between": "Ανάμεσα",
            "contained": "Contained",
            "containedorequal": "Contained or equal",
            "contains": "Περιλαμβάνει",
            "containsorequal": "Περιλαμβάνει ή είναι ίσο",
            "different": "Διαφορετικό",
            "doesnotbeginwith": "Δεν ξεκινάει με",
            "doesnotcontain": "Δεν περιέχει",
            "doesnotendwith": "Δεν τελειώνει με",
            "endswith": "Τελειώνει με",
            "equals": "Ισούται",
            "greaterthan": "Μεγαλύτερο από",
            "isnotnull": "Δεν είναι κενό",
            "isnull": "Είναι κενό",
            "lessthan": "Λιγότερο από"
        },
        "relations": "Συσχετίσεις",
        "type": "Τύπος",
        "typeinput": "Παράμετρος εισαγωγής",
        "value": "Τιμή"
    },
    "gis": {
        "card": "Κάρτα",
        "externalServices": "Εξωτερικές υπηρεσίες",
        "geographicalAttributes": "Γεωγραφικά χαρακτηριστικά",
        "geoserverLayers": "Επίπεδα Geoserver",
        "layers": "Επίπεδα",
        "list": "Λίστα",
        "mapServices": "<em>Map Services</em>",
        "root": "Root",
        "tree": "Δέντρο πλοήγησης",
        "view": "Προβολή"
    },
    "history": {
        "begindate": "Ημερομηνία έναρξης",
        "enddate": "Ημερομηνία λήξης",
        "user": "Χρήστης"
    },
    "login": {
        "buttons": {
            "login": "Σύνδεση",
            "logout": "<em>Change user</em>"
        },
        "fields": {
            "group": "<em>Group</em>",
            "language": "Γλώσσα",
            "password": "Κωδικός πρόσβασης",
            "tenants": "<em>Tenants</em>",
            "username": "Όνομα χρήστη"
        },
        "title": "Σύνδεση"
    },
    "main": {
        "administrationmodule": "Ενότητα Διαχείρισης",
        "changegroup": "<em>Change group</em>",
        "changetenant": "<em>Change tenant</em>",
        "logout": "Αποσύνδεση",
        "managementmodule": "Ενότητα χειρισμού δεδομένων",
        "multitenant": "<em>Multi tenant</em>",
        "searchinallitems": "<em>Search in all items</em>",
        "userpreferences": "<em>Preferences</em>"
    },
    "menu": {
        "allitems": "<em>All items</em>",
        "classes": "Κλάσεις",
        "custompages": "προσαρμοσμένες σελίδες",
        "dashboards": "<em>Dashboards</em>",
        "processes": "Διαδικασίες",
        "reports": "<em>Reports</em>",
        "views": "Προβολές"
    },
    "notes": {
        "edit": "Τροποποίηση σημείωσης"
    },
    "notifier": {
        "error": "Σφάλμα",
        "genericerror": "<em>Generic error</em>",
        "info": "Πληροφορίες",
        "success": "Επιτυχία",
        "warning": "Προειδοποίηση"
    },
    "processes": {
        "action": {
            "advance": "Advance",
            "label": "<em>Action</em>"
        },
        "startworkflow": "Έναρξη",
        "workflow": "Διαδικασία"
    },
    "relationGraph": {
        "card": "Κάρτα",
        "cardList": "<em>Card List</em>",
        "cardRelation": "Συσχέτιση",
        "class": "<em>Class</em>",
        "class:": "Κλάση",
        "classList": "<em>Class List</em>",
        "level": "<em>Level</em>",
        "openRelationGraph": "Άνοιγμα γράφου συσχέτισης",
        "qt": "<em>Qt</em>",
        "refresh": "<em>Refresh</em>",
        "relation": "Συσχέτιση",
        "relationGraph": "Γράφος συσχέτισης"
    },
    "relations": {
        "adddetail": "Προσθήκη λεπτομέρειας",
        "addrelations": "Προσθήκη συσχετίσεων",
        "attributes": "Χαρακτηριστικά",
        "code": "Κωδικός",
        "deletedetail": "Διαγραφή λεπτομέρειας",
        "deleterelation": "Διαγραφή συσχέτισης",
        "description": "Περιγραφή",
        "editcard": "Τροποποίηση κάρτας",
        "editdetail": "Επεξεργασία λεπτομέρειας",
        "editrelation": "Επεξεργασία συσχέτισης",
        "mditems": "<em>items</em>",
        "opencard": "Άνοιγμα σχετικής κάρτας",
        "opendetail": "Εμφάνιση λεπτομέρειας",
        "type": "Τύπος"
    },
    "widgets": {
        "customform": {
            "addrow": "Προσθήκη γραμμής",
            "clonerow": "Κλωνοποίηση γραμμής",
            "deleterow": "Διαγραφή γραμμής",
            "editrow": "Επεξεργασία γραμμής",
            "export": "Εξαγωγή",
            "import": "Εισαγωγή",
            "refresh": "<em>Refresh to defaults</em>"
        },
        "linkcards": {
            "editcard": "<em>Edit card</em>",
            "opencard": "<em>Open card</em>",
            "refreshselection": "Εφαρμογή προεπιλογής",
            "togglefilterdisabled": "Απενεργοποίηση φίλτρου πλέγματος",
            "togglefilterenabled": "Ενεργοποίηση φίλτρου πλέγματος"
        }
    }
});