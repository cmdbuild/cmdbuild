Ext.define('CMDBuildUI.locales.el_GR.LocalesAdministration', {
    "singleton": true,
    "localization": "el_GR",
    "administration": {
        "attributes": {
            "attribute": "Χαρακτηριστικό",
            "attributes": "Χαρακτηριστικά",
            "emptytexts": {
                "search": "<em>Search...</em>"
            },
            "fieldlabels": {
                "actionpostvalidation": "<em>Action post validation</em>",
                "active": "Ενεργό",
                "description": "Περιγραφή",
                "domain": "Πεδίο ορισμού",
                "editortype": "Τύπος επεξεργαστή",
                "filter": "Φίλτρο",
                "group": "Ομάδα",
                "help": "<em>Help</em>",
                "includeinherited": "Συμπερίληψη κληρονομικότητας",
                "iptype": "Τύπος IP",
                "lookup": "Lookup",
                "mandatory": "Υποχρεωτικό",
                "maxlength": "<em>Max Length</em>",
                "mode": "Mode",
                "name": "Όνομα",
                "precision": "Ακρίβεια",
                "preselectifunique": "Προεπιλογή εάν είναι μοναδικό",
                "scale": "Κλίμακα",
                "showif": "<em>Show if</em>",
                "showingrid": "<em>Show in grid</em>",
                "showinreducedgrid": "<em>Show in reduced grid</em>",
                "type": "Τύπος",
                "unique": "Μοναδικό",
                "validationrules": "<em>Validation rules</em>"
            },
            "strings": {
                "any": "Οποιοδήποτε",
                "draganddrop": "<em>Drag and drop to reorganize</em>",
                "editable": "Επεξεργάσιμο",
                "editorhtml": "Html",
                "hidden": "Κρυφό",
                "immutable": "<em>Immutable</em>",
                "ipv4": "ipv4",
                "ipv6": "ipv6",
                "plaintext": "Απλό κείμενο",
                "precisionmustbebiggerthanscale": "<em>Precision must be bigger than Scale</em>",
                "readonly": "Μόνο για ανάγνωση",
                "scalemustbesmallerthanprecision": "<em>Scale must be smaller than Precision</em>",
                "thefieldmandatorycantbechecked": "<em>The field \"Mandatory\" can't be checked</em>",
                "thefieldmodeishidden": "<em>The field \"Mode\" is hidden</em>",
                "thefieldshowingridcantbechecked": "<em>The field \"Show in grid\" can't be checked</em>",
                "thefieldshowinreducedgridcantbechecked": "<em>The field \"Show in reduced grid\" can't be checked</em>"
            },
            "texts": {
                "active": "Ενεργός",
                "addattribute": "<em>Add attribute</em>",
                "cancel": "Ακύρωση",
                "description": "Περιγραφή",
                "editingmode": "<em>Editing mode</em>",
                "editmetadata": "Επεξεργασία μεταδεδομένων",
                "grouping": "<em>Grouping</em>",
                "mandatory": "Υποχρεωτικό",
                "name": "Όνομα",
                "save": "Αποθήκευση",
                "saveandadd": "<em>Save and Add</em>",
                "showingrid": "<em>Show in grid</em>",
                "type": "Τύπος",
                "unique": "Μοναδικό",
                "viewmetadata": "<em>View metadata</em>"
            },
            "titles": {
                "generalproperties": "<em>General properties</em>",
                "typeproperties": "<em>Type properties</em>"
            },
            "tooltips": {
                "deleteattribute": "Διαγραφή",
                "disableattribute": "Απενεργοποίηση",
                "editattribute": "Επεξεργασία",
                "enableattribute": "Ενεργοποίηση",
                "openattribute": "Ανοιχτό",
                "translate": "<em>Translate</em>"
            }
        },
        "classes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "ClassAttachments": "<em>Class Attachments</em>",
                        "classParameters": "<em>Class Parameters</em>",
                        "contextMenus": {
                            "actions": {
                                "delete": {
                                    "tooltip": "Διαγραφή"
                                },
                                "edit": {
                                    "tooltip": "Επεξεργασία"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "applicability": {
                                    "label": "<em>Applicability</em>",
                                    "values": {
                                        "all": {
                                            "label": "Όλα"
                                        },
                                        "many": {
                                            "label": "<em>Current and selected</em>"
                                        },
                                        "one": {
                                            "label": "Τρέχον"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script / custom GUI Paramenters</em>"
                                },
                                "menuItemName": {
                                    "label": "<em>Menu item name</em>",
                                    "values": {
                                        "separator": {
                                            "label": "<em>[---------]</em>"
                                        }
                                    }
                                },
                                "status": {
                                    "label": "Κατάσταση",
                                    "values": {
                                        "active": {
                                            "label": "Κατάσταση"
                                        }
                                    }
                                },
                                "typeOrGuiCustom": {
                                    "label": "<em>Type / GUI custom</em>",
                                    "values": {
                                        "component": {
                                            "label": "<em>Custom GUI</em>"
                                        },
                                        "custom": {
                                            "label": "<em>Script Javascript</em>"
                                        },
                                        "separator": {
                                            "label": "<em></em>"
                                        }
                                    }
                                }
                            },
                            "title": "<em>Context Menus</em>"
                        },
                        "defaultOrders": "<em>Default Orders</em>",
                        "formTriggers": {
                            "actions": {
                                "addNewTrigger": {
                                    "tooltip": "<em>Add new Trigger</em>"
                                },
                                "deleteTrigger": {
                                    "tooltip": "Διαγραφή"
                                },
                                "editTrigger": {
                                    "tooltip": "Επεξεργασία"
                                },
                                "moveDown": {
                                    "tooltip": "<em>Move Down</em>"
                                },
                                "moveUp": {
                                    "tooltip": "<em>Move Up</em>"
                                }
                            },
                            "inputs": {
                                "createNewTrigger": {
                                    "label": "<em>Create new form trigger</em>"
                                },
                                "events": {
                                    "label": "<em>Events</em>",
                                    "values": {
                                        "afterClone": {
                                            "label": "<em>After Clone</em>"
                                        },
                                        "afterEdit": {
                                            "label": "<em>After Edit</em>"
                                        },
                                        "afterInsert": {
                                            "label": "<em>After Insert</em>"
                                        },
                                        "beforView": {
                                            "label": "<em>Before View</em>"
                                        },
                                        "beforeClone": {
                                            "label": "<em>Before Clone</em>"
                                        },
                                        "beforeEdit": {
                                            "label": "<em>Before Edit</em>"
                                        },
                                        "beforeInsert": {
                                            "label": "<em>Before Insert</em>"
                                        }
                                    }
                                },
                                "javascriptScript": {
                                    "label": "<em>Javascript script</em>"
                                },
                                "status": {
                                    "label": "Κατάσταση"
                                }
                            },
                            "title": "<em>Form Triggers</em>"
                        },
                        "formWidgets": "<em>Form Widgets</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ενεργό"
                                },
                                "classType": {
                                    "label": "Τύπος"
                                },
                                "description": {
                                    "label": "Περιγραφή"
                                },
                                "name": {
                                    "label": "Όνομα"
                                },
                                "parent": {
                                    "label": "Γονέας"
                                },
                                "superclass": {
                                    "label": "Υπερκλάση"
                                }
                            }
                        },
                        "icon": "Εικονίδιο",
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "events": "<em>Events</em>",
                        "javascriptScript": "<em>Javascript Script</em>",
                        "status": "Κατάσταση"
                    },
                    "values": {
                        "active": "Ενεργό"
                    }
                },
                "title": "Ιδιότητες",
                "toolbar": {
                    "cancelBtn": "Ακύρωση",
                    "closeBtn": "Κλείσιμο",
                    "deleteBtn": {
                        "tooltip": "Διαγραφή"
                    },
                    "disableBtn": {
                        "tooltip": "Απενεργοποίηση"
                    },
                    "editBtn": {
                        "tooltip": "Τροποποίηση κλάσης"
                    },
                    "enableBtn": {
                        "tooltip": "Ενεργοποίηση"
                    },
                    "printBtn": {
                        "printAsOdt": "OpenOffice Odt",
                        "printAsPdf": "<em>Adobe Pdf</em>",
                        "tooltip": "Εκτύπωση κλάσης"
                    },
                    "saveBtn": "Αποθήκευση"
                }
            },
            "strings": {
                "geaoattributes": "<em>Geo attributes</em>",
                "levels": "<em>Levels</em>"
            },
            "title": "Κλάσεις",
            "toolbar": {
                "addClassBtn": {
                    "text": "Προσθήκη κλάσης"
                },
                "classLabel": "Κλάση",
                "printSchemaBtn": {
                    "text": "Εκτύπωση σχήματος"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all classes</em>"
                }
            }
        },
        "common": {
            "actions": {
                "activate": "<em>Activate</em>",
                "add": "Όλα",
                "cancel": "Ακύρωση",
                "clone": "Κλώνος",
                "close": "Κλείσιμο",
                "create": "Δημιουργία",
                "delete": "Διαγραφή",
                "disable": "Απενεργοποίηση",
                "edit": "Επεξεργασία",
                "enable": "Ενεργοποίηση",
                "no": "Όχι",
                "print": "Εκτύπωση",
                "save": "Αποθήκευση",
                "update": "Ενημέρωση",
                "yes": "Ναι"
            },
            "messages": {
                "areyousuredeleteitem": "<em>Are you sure you want to delete this item?</em>",
                "ascendingordescending": "<em>This value is not valid, please select \"Ascending\" or \"Descending\"</em>",
                "attention": "Προσοχή",
                "cannotsortitems": "<em>You can not reorder the items if some filters are present or the inherited attributes are hidden. Please remove them and try again.</em>",
                "cantcontainchar": "<em>The class name can't contain {0} character.</em>",
                "correctformerrors": "<em>Please correct indicated errors</em>",
                "disabled": "<em>disabled</em>",
                "enabled": "<em>enabled</em>",
                "error": "Σφάλμα",
                "greaterthen": "<em>The class name can't be greater than {0} characters</em>",
                "itemwascreated": "<em>Item was created.</em>",
                "loading": "Φόρτωση...",
                "saving": "<em>Saving...</em>",
                "success": "Επιτυχία",
                "warning": "Προειδοποίηση",
                "was": "<em>was</em>",
                "wasdeleted": "<em>was deleted</em>"
            },
            "tooltips": {
                "edit": "Επεξεργασία"
            }
        },
        "domains": {
            "domain": "Πεδίο ορισμού",
            "fieldlabels": {
                "destination": "Προορισμός",
                "enabled": "Ενεργό",
                "origin": "Προέλευση"
            },
            "pluralTitle": "<em>Domains</em>",
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "description": {
                                    "label": "Περιγραφή"
                                },
                                "descriptionDirect": {
                                    "label": "Απευθείας περιγραφή"
                                },
                                "descriptionInverse": {
                                    "label": "Αντίστροφη περιγραφή"
                                },
                                "name": {
                                    "label": "Όνομα"
                                }
                            }
                        }
                    }
                },
                "toolbar": {
                    "cancelBtn": "Ακύρωση",
                    "deleteBtn": {
                        "tooltip": "Διαγραφή"
                    },
                    "disableBtn": {
                        "tooltip": "Απενεργοποίηση"
                    },
                    "editBtn": {
                        "tooltip": "Επεξεργασία"
                    },
                    "enableBtn": {
                        "tooltip": "Ενεργοποίηση"
                    },
                    "saveBtn": "Αποθήκευση"
                }
            },
            "singularTitle": "Πεδίο ορισμού",
            "texts": {
                "enabledomains": "<em>Enabled domains</em>",
                "properties": "Ιδιότητες"
            },
            "toolbar": {
                "addBtn": {
                    "text": "Προσθήκη Πεδίου ορισμού"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all domains</em>"
                }
            }
        },
        "groupandpermissions": {
            "emptytexts": {
                "searchingrid": "<em>Search in grid...</em>",
                "searchusers": "<em>Search users...</em>"
            },
            "fieldlabels": {
                "actions": "<em>Actions</em>",
                "active": "Ενεργός",
                "attachments": "Συνημμένα αρχεία",
                "datasheet": "<em>Data sheet</em>",
                "defaultpage": "<em>Default page</em>",
                "description": "Περιγραφή",
                "detail": "Λεπτομέρειες",
                "email": "E-mail",
                "exportcsv": "Εξαγωγή αρχείου CSV",
                "filters": "<em>Filters</em>",
                "history": "Ιστορικό",
                "importcsvfile": "Εισαγωγή αρχείου CSV",
                "massiveeditingcards": "<em>Massive editing of cards</em>",
                "name": "Όνομα",
                "note": "Σημείωση",
                "relations": "Συσχετίσεις",
                "type": "Τύπος",
                "username": "Όνομα χρήστη"
            },
            "plural": "<em>Groups and permissions</em>",
            "singular": "<em>Group and permission</em>",
            "strings": {
                "admin": "<em>Admin</em>",
                "displaynousersmessage": "<em>No users to display</em>",
                "displaytotalrecords": "<em>{2} records</em>",
                "limitedadmin": "Διαχειριστής με περιορισμούς",
                "normal": "Κανονικό",
                "readonlyadmin": "<em>Read-only admin</em>"
            },
            "texts": {
                "class": "<em>Class</em>",
                "default": "<em>Default</em>",
                "defaultfilter": "<em>Default filter</em>",
                "defaultfilters": "Προεπιλεγμένα φίλτρα",
                "defaultread": "Def. + R.",
                "description": "Περιγραφή",
                "filters": "<em>Filters</em>",
                "group": "Ομάδα",
                "name": "Όνομα",
                "none": "Κανένα",
                "permissions": "Δικαιώματα",
                "read": "Ανάγνωση",
                "uiconfig": "Προσαρμογή διεπαφής χρήστη",
                "userslist": "<em>Users list</em>",
                "write": "Εγγραφή"
            },
            "titles": {
                "allusers": "<em>All users</em>",
                "disabledactions": "<em>Disabled actions</em>",
                "disabledallelements": "<em>Functionality disabled Navigation menu \"All Elements\"</em>",
                "disabledmanagementprocesstabs": "<em>Tabs Disabled Management Processes</em>",
                "disabledutilitymenu": "<em>Functionality disabled Utilities Menu</em>",
                "generalattributes": "<em>General Attributes</em>",
                "managementdisabledtabs": "<em>Tabs Disabled Management Classes</em>",
                "usersassigned": "<em>Users assigned</em>"
            },
            "tooltips": {
                "disabledactions": "<em>Disabled actions</em>",
                "filters": "<em>Filters</em>",
                "removedisabledactions": "<em>Remove disabled actions</em>",
                "removefilters": "<em>Remove filters</em>"
            }
        },
        "lookuptypes": {
            "title": "<em>Lookup Types</em>",
            "toolbar": {
                "addClassBtn": {
                    "text": "<em>Add lookup</em>"
                },
                "classLabel": "<em>List</em>",
                "printSchemaBtn": {
                    "text": "<em>Print lookup</em>"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all lookups...</em>"
                }
            },
            "type": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ενεργός"
                                },
                                "name": {
                                    "label": "Όνομα"
                                },
                                "parent": {
                                    "label": "Γονέας"
                                }
                            }
                        }
                    },
                    "values": {
                        "active": "Ενεργός"
                    }
                },
                "title": "<em>Lookup lists</em>",
                "toolbar": {
                    "cancelBtn": "Ακύρωση",
                    "closeBtn": "Κλείσιμο",
                    "deleteBtn": {
                        "tooltip": "Διαγραφή"
                    },
                    "editBtn": {
                        "tooltip": "Επεξεργασία"
                    },
                    "saveBtn": "Αποθήκευση"
                }
            }
        },
        "menus": {
            "main": {
                "toolbar": {
                    "cancelBtn": "Ακύρωση",
                    "closeBtn": "Κλείσιμο",
                    "deleteBtn": {
                        "tooltip": "Διαγραφή"
                    },
                    "editBtn": {
                        "tooltip": "Επεξεργασία"
                    },
                    "saveBtn": "Αποθήκευση"
                }
            },
            "pluralTitle": "<em>Menus</em>",
            "singularTitle": "Μενού",
            "toolbar": {
                "addBtn": {
                    "text": "<em>Add menu</em>"
                }
            }
        },
        "modClass": {
            "attributeProperties": {
                "typeProperties": "Ιδιότητες τύπου"
            }
        },
        "navigation": {
            "bim": "BIM",
            "classes": "Κλάσεις",
            "custompages": "προσαρμοσμένες σελίδες",
            "dashboards": "<em>Dashboards</em>",
            "dms": "DMS",
            "domains": "Πεδία ορισμού",
            "email": "E-mail",
            "generaloptions": "Γενικές επιλογές",
            "gis": "GIS",
            "groupsandpermissions": "<em>Groups and permissions</em>",
            "languages": "Γλώσσες",
            "lookuptypes": "Τύποι Lookup",
            "menus": "<em>Menus</em>",
            "multitenant": "<em>Multitenant</em>",
            "navigationtrees": "<em>Navigation trees</em>",
            "processes": "Διαδικασίες",
            "reports": "<em>Reports</em>",
            "searchfilters": "Φίλτρα αναζήτησης",
            "servermanagement": "Διαχείριση Server",
            "simples": "<em>Simples</em>",
            "standard": "Standard",
            "systemconfig": "<em>System config</em>",
            "taskmanager": "Διαχειριστής Εργασιών",
            "title": "Πλοήγηση",
            "users": "Χρήστες",
            "views": "Προβολές",
            "workflow": "<em>Workflow</em>"
        },
        "processes": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "defaultOrders": "<em>Default Orders</em>",
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ενεργός"
                                },
                                "description": {
                                    "label": "Περιγραφή"
                                },
                                "enableSaveButton": {
                                    "label": "<em>Hide \"Save\" button</em>"
                                },
                                "name": {
                                    "label": "Όνομα"
                                },
                                "parent": {
                                    "label": "Κληρονομεί από"
                                },
                                "stoppableByUser": {
                                    "label": "Μπορεί να διακοπεί από τον χρήστη"
                                },
                                "superclass": {
                                    "label": "Υπερκλάση"
                                }
                            }
                        },
                        "icon": "Εικονίδιο",
                        "processParameter": {
                            "inputs": {
                                "defaultFilter": {
                                    "label": "<em>Default filter</em>"
                                },
                                "messageAttribute": {
                                    "label": "<em>Message attribute</em>"
                                },
                                "stateAttribute": {
                                    "label": "<em>State attribute</em>"
                                }
                            },
                            "title": "<em>Process parameters</em>"
                        },
                        "validation": {
                            "inputs": {
                                "validationRule": {
                                    "label": "<em>Validation Rule</em>"
                                }
                            },
                            "title": "<em>Validation</em>"
                        }
                    },
                    "inputs": {
                        "status": "Κατάσταση"
                    },
                    "values": {
                        "active": "Ενεργός"
                    }
                },
                "title": "<em>Properties</em>",
                "toolbar": {
                    "cancelBtn": "Ακύρωση",
                    "closeBtn": "Κλείσιμο",
                    "deleteBtn": {
                        "tooltip": "Διαγραφή"
                    },
                    "disableBtn": {
                        "tooltip": "Απενεργοποίηση"
                    },
                    "editBtn": {
                        "tooltip": "Επεξεργασία"
                    },
                    "enableBtn": {
                        "tooltip": "Ενεργοποίηση"
                    },
                    "saveBtn": "Αποθήκευση",
                    "versionBtn": {
                        "tooltip": "Έκδοση"
                    }
                }
            },
            "title": "<em>Processes</em>",
            "toolbar": {
                "addProcessBtn": {
                    "text": "Προσθήκη διαδικασίας"
                },
                "printSchemaBtn": {
                    "text": "Εκτύπωση σχήματος"
                },
                "processLabel": "Διαδικασία",
                "searchTextInput": {
                    "emptyText": "<em>Search all processes</em>"
                }
            }
        },
        "title": "<em>Administration</em>",
        "users": {
            "properties": {
                "form": {
                    "fieldsets": {
                        "generalData": {
                            "inputs": {
                                "active": {
                                    "label": "Ενεργός"
                                },
                                "description": {
                                    "label": "Περιγραφή"
                                },
                                "name": {
                                    "label": "Όνομα"
                                },
                                "stoppableByUser": {
                                    "label": "Μπορεί να διακοπεί από τον χρήστη"
                                }
                            }
                        },
                        "icon": "Εικονίδιο"
                    }
                }
            },
            "title": "Χρήστες",
            "toolbar": {
                "addUserBtn": {
                    "text": "Προσθήκη χρήστη"
                },
                "searchTextInput": {
                    "emptyText": "<em>Search all users</em>"
                }
            }
        }
    }
});