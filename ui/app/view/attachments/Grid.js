
Ext.define('CMDBuildUI.view.attachments.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.attachments.GridController',
        'CMDBuildUI.view.attachments.GridModel'
    ],

    alias: 'widget.attachments-grid',
    controller: 'attachments-grid',
    viewModel: {
        type: 'attachments-grid'
    },

    forceFit: true,
    loadMask: true,

    columns: [{
        text: CMDBuildUI.locales.Locales.attachments.filename,
        dataIndex: '_name',
        align: 'left',
        hidden: false,
        localized: {
            text: 'CMDBuildUI.locales.Locales.attachments.filename'
        }
    }, {
        text: CMDBuildUI.locales.Locales.attachments.description,
        dataIndex: '_description',
        align: 'left',
        hidden: false,
        localized: {
            text: 'CMDBuildUI.locales.Locales.attachments.description'
        }
    }, {
        text: CMDBuildUI.locales.Locales.attachments.version,
        dataIndex: '_version',
        align: 'left',
        hidden: false,
        localized: {
            text: 'CMDBuildUI.locales.Locales.attachments.version'
        }
    }, {
        text: CMDBuildUI.locales.Locales.attachments.creationdate,
        dataIndex: '_created',
        align: 'left',
        hidden: false,
        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
            return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.datetime);
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.attachments.creationdate'
        }
    }, {
        text: CMDBuildUI.locales.Locales.attachments.modificationdate,
        dataIndex: '_modified',
        align: 'left',
        hidden: true,
        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
            return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.datetime);
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.attachments.modificationdate'
        }
    }, {
        text: CMDBuildUI.locales.Locales.attachments.category,
        dataIndex: '_category',
        align: 'left',
        hidden: true,
        localized: {
            text: 'CMDBuildUI.locales.Locales.attachments.category'
        }
    }, {
        text: CMDBuildUI.locales.Locales.attachments.author,
        dataIndex: '_author',
        align: 'left',
        hidden: true,
        localized: {
            text: 'CMDBuildUI.locales.Locales.attachments.category'
        }
    }, {
        xtype: 'actioncolumn',
        minWidth: 104, // width property not works. Use minWidth.
        items: [{
            iconCls: 'attachments-grid-action x-fa fa-download',
            getTip: function () {
                return CMDBuildUI.locales.Locales.attachments.download;
            },
            handler: function (grid, rowIndex, colIndex) {
                var record = grid.getStore().getAt(rowIndex);
                grid.fireEvent("actiondownload", grid, record, rowIndex, colIndex);
            }
        }, {
            iconCls: 'attachments-grid-action x-fa fa-pencil',
            getTip: function () {
                return CMDBuildUI.locales.Locales.attachments.editattachment;
            },
            handler: function (grid, rowIndex, colIndex) {
                var record = grid.getStore().getAt(rowIndex);
                grid.fireEvent("actionedit", grid, record, rowIndex, colIndex);
            }
        }, {
            iconCls: 'attachments-grid-action x-fa fa-trash',
            getTip: function () {
                return CMDBuildUI.locales.Locales.attachments.deleteattachment;
            },
            handler: function (grid, rowIndex, colIndex) {
                var record = grid.getStore().getAt(rowIndex);
                grid.fireEvent("actiondelete", grid, record, rowIndex, colIndex);
            }
        }, {
            iconCls: 'attachments-grid-action x-fa fa-history',
            getTip: function () {
                return CMDBuildUI.locales.Locales.attachments.viewhistory;
            },
            handler: function (grid, rowIndex, colIndex) {
                var rec = grid.getStore().getAt(rowIndex);
                Ext.Msg.alert("Do something!");
            }
        }]
    }],

    features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{name}',
        depthToIndent: 50
    }],

    bind: {
        store: '{attachments}'
    }
});
