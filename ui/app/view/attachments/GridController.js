Ext.define('CMDBuildUI.view.attachments.GridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.attachments-grid',

    control: {
        'tableview': {
            actiondownload: 'onActionDownload',
            actionedit: 'onActionEdit',
            actiondelete: 'onActionDelete'
        }
    },

    /**
     * @param {CMDBuildUI.view.attachments.Grid} grid
     * @param {Ext.data.Model} record
     * @param {Number} rowIndex
     * @param {Number} colIndex
     * 
     */
    onActionDownload: function (grid, record, rowIndex, colIndex) {
        var url = Ext.String.format(
            "{0}/{1}/{2}?CMDBuild-Authorization={3}",
            grid.getStore().getProxy().getUrl(), // base url 
            record.getId(), // attachment id
            record.get("_name"), // file name 
            CMDBuildUI.util.helper.SessionHelper.getToken() // session tocken
        );
        // open the url in new tab
        window.open(url, "_blank");
    },

    /**
     * @param {CMDBuildUI.view.attachments.Grid} grid
     * @param {Ext.data.Model} record
     * @param {Number} rowIndex
     * @param {Number} colIndex
     * 
     */
    onActionEdit: function (grid, record, rowIndex, colIndex) {
        var vm = this.getViewModel();
        var url = Ext.String.format('{0}/{1}', this.getViewModel().get("attachments").getProxy().getUrl(), record.getId());

        // atachments form definition
        var config = {
            xtype: 'attachments-form',
            viewModel: {
                data: {
                    newAttachment: false,
                    url: url,
                    theAttachment: record
                }
            }
        };

        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                vm.get("attachments").load();
            }
        };
        // create panel
        CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-attachment', 
            CMDBuildUI.locales.Locales.attachments.editattachment, 
            config, 
            listeners
        );
    },

    /**
     * @param {CMDBuildUI.view.attachments.Grid} grid
     * @param {Ext.data.Model} record
     * @param {Number} rowIndex
     * @param {Number} colIndex
     * 
     */
    onActionDelete: function (grid, record, rowIndex, colIndex) {
        var vm = this.getViewModel();
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.attachments.deleteattachment,
            CMDBuildUI.locales.Locales.attachments.deleteattachment_confirmation,
            function (action) {
                if (action === "yes") {
                    record.getProxy().setUrl(vm.get("attachments").getProxy().getUrl());
                    CMDBuildUI.util.Ajax.setActionId('attachment.delete');
                    record.erase();
                }
            }
        );
    }

});
