Ext.define('CMDBuildUI.view.attachments.FormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.attachments-form',

    data: {
        majorVersionValue: false,

        // store params
        storeAutoLoad: false,
        storeProxyUrl: '',

        // fields properties
        description: {
            hidden: true
        },
        majorversion: {
            hidden: true
        }
    },

    formulas: {

        /**
         * hide/show major version field
         */
        hideMajorVersion: {
            bind: '{newAttachment}',
            get: function (isNewAttachment) {
                return isNewAttachment;
            }
        },

        /**
         * update categories store paramaters
         */
        updateCategoriesStoreParameters: {
            bind: {
                targettypeobject : '{targetTypeObject}'
            },
            get: function (data) {
                // get store url
                var ltype = CMDBuildUI.util.helper.Configurations.get("cm_system_dms_category_lookup");
                if (data.targettypeobject.get("attachmentTypeLookup")) {
                    ltype = data.targettypeobject.get("attachmentTypeLookup");
                }
                this.set("storeProxyUrl", CMDBuildUI.util.api.Lookups.getLookupValues(ltype));

                // set descripion properties
                if (data.targettypeobject.get("attachmentDescriptionMode") === CMDBuildUI.model.attachments.Attachment.descriptionmodes.optional) {
                    this.set("description.hidden", false);
                } else if (data.targettypeobject.get("attachmentDescriptionMode") === CMDBuildUI.model.attachments.Attachment.descriptionmodes.mandatory) {
                    this.set("description.hidden", false);
                    this.set("description.label", CMDBuildUI.locales.Locales.attachments.description + " *");
                }
            }
        }
    },

    stores: {
        categories: {
            model: 'CMDBuildUI.model.lookups.Lookup',
            proxy: {
                url: '{storeProxyUrl}',
                type: 'baseproxy'
            },
            autoLoad: '{storeAutoLoad}'
        }
    }
});
