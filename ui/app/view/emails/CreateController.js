Ext.define('CMDBuildUI.view.emails.CreateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emails-create',

    control: {
        '#': {
            beforeRender: 'onBeforeRender'
        },
        '#saveBtn': {
            click: 'onSaveBtn'
        },
    },

    /**
     * @param {CMDBuildUI.view.emails.Container} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
    },


    /**
     * @param {CMDBuildUI.view.emails.Create} view
     * @param {Object} eOpts
     */
    onSaveBtn: function (view, eOpts) {
        var createView = this.getView();
        var vm = createView.getViewModel();
        var className = vm.get('objectTypeName');
        var objectId = vm.get('objectId');
        var url = Ext.String.format(
            '{0}/classes/{1}/cards/{2}/emails',
            CMDBuildUI.util.Config.baseUrl,
            className,
            objectId
        );
        var theEmail = vm.get('theEmail');
        theEmail.set('status', 'DRAFT');
        theEmail.getProxy().setUrl(url);
        theEmail.save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: 'Email saved correctly.',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
            }
        });
    }

});
