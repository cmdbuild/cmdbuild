
Ext.define('CMDBuildUI.view.emails.Container', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.emails.ContainerController',
        'CMDBuildUI.view.emails.ContainerModel'
    ],

    alias: 'widget.emails-container',
    controller: 'emails-container',
    viewModel: {
        type: 'emails-container'
    },
    tbar: [
        {
            xtype: 'button',
            text: CMDBuildUI.locales.Locales.emails.composeemail,
            reference: 'composeemail',
            itemId: 'composeemail',
            iconCls: 'x-fa fa-plus',
            ui: 'management-action-small',
            bind: {
                disabled: '{disableAddRelationButton}'
            },
            localized: {
                text: 'CMDBuildUI.locales.Locales.emails.composeemail'
            },
        },
        {
            xtype: 'button',
            text: CMDBuildUI.locales.Locales.emails.regenerateallemails,
            reference: 'regenerateallemails',
            itemId: 'regenerateallemails',
            iconCls: 'x-fa fa-envelope',
            ui: 'management-action-small',
            bind: {
                disabled: '{disableAddRelationButton}'
            },
            localized: {
                text: 'CMDBuildUI.locales.Locales.emails.regenerateallemails'
            },
        },
        {
            xtype: 'button',
            text: CMDBuildUI.locales.Locales.emails.gridrefresh,
            reference: 'gridrefresh',
            itemId: 'gridrefresh',
            iconCls: 'x-fa fa-refresh',
            ui: 'management-action-small',
            bind: {
                disabled: '{disableAddRelationButton}'
            },
            localized: {
                text: 'CMDBuildUI.locales.Locales.emails.gridrefresh'
            },
        }

    ]
});
