Ext.define('CMDBuildUI.view.emails.ContainerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emails-container',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#composeemail': {
            click: 'onComposeEmail'
        },
        '#regenerateallemails': {
            click: 'onRegenerateAllEmails'
        },
        '#gridrefresh': {
            click: 'onGridRefresh'
        }
    },

    /**
     * @param {CMDBuildUI.view.emails.Container} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        this.addMailsGrid();
    },

    /**
     * @param {CMDBuildUI.view..emails.Container.button} view
     * @param {Object} eOpts
     */
    onComposeEmail: function (view, eOpts) {
        var vm = this.getViewModel();
        var email = Ext.create("CMDBuildUI.model.emails.Email");
        var title = CMDBuildUI.locales.Locales.emails.composeemail;
        var config = {
            xtype: 'emails-create',
            viewModel: {
                data: {
                    objectId: vm.get('objectId'),
                    objectType: vm.get('objectType'),
                    objectTypeName: vm.get('objectTypeName'),
                    theEmail: email
                }
            }
        };

        var popup = CMDBuildUI.util.Utilities.openPopup('popup-compose-email', title, config, null);
    },

    onRegenerateAllEmails: function () { },

    onGridRefresh: function () { },

    privates: {
        addMailsGrid: function () {
            this.getView().add({
                xtype: 'emails-grid'
            });
        }
    }
});
