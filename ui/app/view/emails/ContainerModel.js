Ext.define('CMDBuildUI.view.emails.ContainerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.emails-container',
    data: {

        storeAutoLoad: false,
        storeProxyUrl: ''
    },

    formulas: {

        updateStoreVariables: {
            bind: {
                type: '{objectType}',
                typename: '{objectTypeName}',
                id: '{objectId}'
            },
            get: function (data) {
                this.set(
                    "storeProxyUrl",
                    Ext.String.format(
                        '{0}/classes/{1}/cards/{2}/emails',
                        CMDBuildUI.util.Config.baseUrl,
                        data.typename,
                        data.id
                    )
                );
                // set auto load
                this.set("storeAutoLoad", true);
            }
        }
    },

    stores: {
        emails: {
            type: 'emails',
            autoLoad: '{storeAutoLoad}',
            proxy: {
                url: '{storeProxyUrl}',
                type: 'baseproxy'
            }
        }
    }
});