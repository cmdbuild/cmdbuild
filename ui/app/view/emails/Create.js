
Ext.define('CMDBuildUI.view.emails.Create', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.emails.CreateController',
        'CMDBuildUI.view.emails.CreateModel'
    ],

    alias: 'widget.emails-create',
    controller: 'emails-create',
    viewModel: {
        type: 'emails-create'
    },

    scrollable: true,
    items: [{
        xtype: 'fieldcontainer',
        forceFit: true,
        padding: '0 15 0 15',
        layout: 'fit',
        fieldDefaults: CMDBuildUI.util.helper.FormHelper.fieldDefaults,
        items: [{
            layout: 'column',
            items: [
                {
                    xtype: 'combobox',
                    reference: 'composefromtemplate',
                    fieldLabel: CMDBuildUI.locales.Locales.emails.composefromtemplate,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.emails.composefromtemplate'
                    },
                    columnWidth: 0.4
                },
                {
                    xtype: 'checkboxfield',
                    reference: 'keepSynchronization',
                    fieldLabel: CMDBuildUI.locales.Locales.emails.keepsynchronization,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.emails.keepsynchronization'
                    },
                    dataIndex: 'enabled',
                    disabled: true,
                    bind: {
                        value: '{theEmail.checkboxfield}'
                    },
                    columnWidth: 0.2
                },
                {
                    xtype: 'combobox',
                    reference: 'delay',
                    fieldLabel: CMDBuildUI.locales.Locales.emails.delay,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.emails.delay'
                    },
                    bind: {
                        value: '{theEmail.delay}'
                    },
                    columnWidth: 0.4
                }]
        },
        {
            xtype: 'textfield',
            reference: 'from',
            fieldLabel: CMDBuildUI.locales.Locales.emails.from,
            disabled: true,
            localized: {
                fieldLabel: 'CMDBuildUI.locales.Locales.emails.from'
            },
            bind: {
                value: '{theEmail.from}'
            },
        },
        {
            xtype: 'textfield',
            reference: 'to',
            fieldLabel: CMDBuildUI.locales.Locales.emails.to,
            localized: {
                fieldLabel: 'CMDBuildUI.locales.Locales.emails.to'
            },
            bind: {
                value: '{theEmail.to}'
            },
        },
        {
            xtype: 'textfield',
            reference: 'cc',
            fieldLabel: CMDBuildUI.locales.Locales.emails.cc,

            localized: {
                fieldLabel: 'CMDBuildUI.locales.Locales.emails.cc'
            },
            bind: {
                value: '{theEmail.cc}'
            }
        },
        {
            xtype: 'textfield',
            reference: 'bcc',
            fieldLabel: CMDBuildUI.locales.Locales.emails.bcc,
            localized: {
                fieldLabel: 'CMDBuildUI.locales.Locales.emails.bcc'
            },
            bind: {
                value: '{theEmail.bcc}'
            }
        },
        {
            xtype: 'textfield',
            reference: 'subject',
            fieldLabel: CMDBuildUI.locales.Locales.emails.subject,

            localized: {
                fieldLabel: 'CMDBuildUI.locales.Locales.emails.subject'
            },
            bind: {
                value: '{theEmail.subject}'
            }
        },
        {
            xtype: 'htmleditor',
            reference: 'body',
            fieldLabel: CMDBuildUI.locales.Locales.emails.message,
            localized: {
                fieldLabel: 'CMDBuildUI.locales.Locales.emails.message'
            },
            bind: {
                value: '{theEmail.body}'
            }
        },
        {
            padding: '15 0 15 0',
            items: [
                {
                    xtype: 'button',
                    itemId: 'attachfile',
                    reference: 'attachfile',
                    text: CMDBuildUI.locales.Locales.emails.attachfile,
                    ui: 'secondary-action',
                    localized: {
                        text: 'CMDBuildUI.locales.Locales.emails.attachfile'
                    }
                }, {
                    xtype: 'button',
                    margin: '0 0 0 15',
                    itemId: 'attachfilefromDMS',
                    reference: 'attachfilefromDMS',
                    text: CMDBuildUI.locales.Locales.emails.addattachmentsfromdms,
                    ui: 'secondary-action',
                    localized: {
                        text: 'CMDBuildUI.locales.Locales.emails.addattachmentsfromdms'
                    }
                }],
        }]
    }],

    buttons: [{
        text: CMDBuildUI.locales.Locales.common.actions.save,
        reference: 'saveBtn',
        itemId: 'saveBtn',
        ui: 'management-action',
        localized: {
            text: 'CMDBuildUI.locales.Locales.common.actions.save'
        }
    }, {
        text: CMDBuildUI.locales.Locales.common.actions.cancel,
        reference: 'cancelBtn',
        itemId: 'cancelBtn',
        ui: 'secondary-action',
        localized: {
            text: 'CMDBuildUI.locales.Locales.common.actions.cancel'
        }
    }]

});
