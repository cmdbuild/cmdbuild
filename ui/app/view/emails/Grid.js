
Ext.define('CMDBuildUI.view.emails.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.emails.GridController',
        'CMDBuildUI.view.emails.GridModel'
    ],

    alias: 'widget.emails-grid',
    controller: 'emails-grid',
    viewModel: {
        type: 'emails-grid'
    },
    forceFit: true,
    loadMask: true,

    columns: [
        {
            text: CMDBuildUI.locales.Locales.emails.archivingdate,
            dataIndex: 'date',
            align: 'left',
            localized: {
                text: 'CMDBuildUI.locales.Locales.emails.archivingdate'
            },
        },
        {
            text: CMDBuildUI.locales.Locales.emails.from,
            dataIndex: 'from',
            align: 'left',
            localized: {
                text: 'CMDBuildUI.locales.Locales.emails.from'
            },
        },
        {
            text: CMDBuildUI.locales.Locales.emails.to,
            dataIndex: 'to',
            align: 'left',
            localized: {
                text: 'CMDBuildUI.locales.Locales.emails.to'
            },
        },
        {
            text: CMDBuildUI.locales.Locales.emails.subject,
            dataIndex: 'subject',
            align: 'left',
            localized: {
                text: 'CMDBuildUI.locales.Locales.emails.subject'
            },
        }
    ],

    bind: {
        store: '{emails}'
    }
});
