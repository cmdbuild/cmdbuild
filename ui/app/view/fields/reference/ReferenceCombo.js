
Ext.define('CMDBuildUI.view.fields.reference.ReferenceCombo', {
    extend: 'Ext.form.field.ComboBox',

    requires: [
        'CMDBuildUI.view.fields.reference.ReferenceComboController',
        'CMDBuildUI.view.fields.reference.ReferenceComboModel'
    ],

    alias: 'widget.referencecombofield',
    controller: 'fields-referencecombofield',
    viewModel: {
        type: 'fields-referencecombofield'
    },

    valueField: '_id',
    displayField: 'Description',
    autoLoadOnValue: false,
    autoSelect: false,
    forceSelection: true,
    typeAhead: true,

    config: {
        /**
         * @cfg {String} recordLinkName (required)
         * The name of the full record in ViewModel used for 
         * value binding.
         */
        recordLinkName: null
    },

    bind: {
        store: '{options}',
        selection: '{selection}'
    },

    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function (combo, trigger, eOpts) {
                combo.fireEvent("cleartrigger", combo, trigger, eOpts);
            }
        },
        search: {
            cls: 'x-form-search-trigger',
            handler: function (combo, trigger, eOpts) {
                combo.fireEvent("searchtrigger", combo, trigger, eOpts);
            }
        }
    },

    /**
     * @override
     * @method
     * Template method, it is called when a new store is bound
     * to the current instance.
     * @protected
     * @param {Ext.data.AbstractStore} store The store being bound
     * @param {Boolean} initial True if this store is being bound as initialization of the instance.
     */
    onBindStore: function (store, initial) {
        var me = this;
        var vm = this.getViewModel();
        if (store.getModel().getName()) {
            // bind reference to dependences
            if (this.metadata.ecqlFilter && this.getRecordLinkName()) {
                var binds = CMDBuildUI.util.ecql.Resolver.getViewModelBindings(
                    this.metadata.ecqlFilter,
                    this.getRecordLinkName()
                );
                if (!Ext.Object.isEmpty(binds)) {
                    vm.bind({
                        bindTo: binds
                    }, function (data) {
                        var storeFilters = store.getFilters();
                        storeFilters.add({
                            property: CMDBuildUI.proxy.BaseProxy.filter.ecql,
                            value: me.getEcqlFilter()
                        });
                    });
                } else if (!Ext.Object.isEmpty(me.getEcqlFilter())) {
                    store.getFilters().add({
                        property: CMDBuildUI.proxy.BaseProxy.filter.ecql,
                        value: me.getEcqlFilter()
                    });
                }
            }

            // add load listener
            store.addListener("load", this.onStoreLoaded, this);
        }
        if (this.getInitialConfig().bind && !Ext.Object.isEmpty(this.getInitialConfig().bind)) {
            vm.bind({
                bindTo: this.getInitialConfig().bind.value
            }, function(value) {
                vm.set("initialvalue", value);
            });
        } else {
            vm.set("initialvalue", null);
        }
        this.callParent(arguments);
    },

    /**
     * Called when store is loaded
     * @param {Ext.data.Store} store
     * @param {Ext.data.Model[]} records An array of records
     * @param {Boolean} successful True if the operation was successful.
     * @param {Ext.data.operation.Read} operation The {@link Ext.data.operation.Read Operation} object that was used in the data load call
     * @param {Object} eOpts
     */
    onStoreLoaded: function (store, records, successful, operation, eOpts) {
        if ((
            this.metadata.cm_preselectIfUnique === true ||
            this.metadata.cm_preselectIfUnique === "true") && store.getTotalCount() === 1
        ) {
            this.setSelection(store.getAt(0));
        }
        if (store.getTotalCount() > 20) { // TODO: make dynamic
            this._allowexpand = false;
        } else {
            this._allowexpand = true;
        }
    },

    /**
     * Get proxy store extra params
     * @return {Object}
     */
    getEcqlFilter: function () {
        var ecql;
        var obj = this._ownerRecord;
        if (this.metadata.ecqlFilter && obj) {
            ecql = CMDBuildUI.util.ecql.Resolver.resolve(this.metadata.ecqlFilter, obj);
        }
        return ecql || {};
    },

    /**
     * @override
     * Expands this field's picker dropdown.
     */
    expand: function () {
        if (this._allowexpand === true) {
            this.callParent(arguments);
        } else {
            this.fireEvent("searchtrigger", this, null, {});
        }
    },

    privates: {
        _allowexpand: null
    }
});
