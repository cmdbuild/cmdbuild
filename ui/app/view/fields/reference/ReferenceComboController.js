Ext.define('CMDBuildUI.view.fields.reference.ReferenceComboController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.fields-referencecombofield',

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            change: 'onChange',
            cleartrigger: 'onClearTrigger',
            searchtrigger: 'onSearchTrigger'
        }
    },

    /**
     * 
     * @param {CMDBuildUI.view.fields.reference.Reference} view
     * @param {Object} eOpts
     */
    onBeforeRender: function(view, eOpts) {
        var me = this;
        if (view.column) {
            view.column.getView().ownerGrid.getPlugins().forEach(function(p) {
                if (p.ptype === "cellediting") {
                    me._ownerRecord = p.activeRecord;
                }
            });
        }
    },

    /**
     * @param {CMDBuildUI.view.fields.reference.Reference} view
     * @param {Numeric|String} newvalue
     * @param {Numeric|String} oldvalue
     * @param {Object} eOpts
     */
    onChange: function (view, newvalue, oldvalue, eOpts) {
        var object = view._ownerRecord;
        if (object) {
            var selected = view.getSelection();
            object.set(Ext.String.format("_{0}_description", view.getName()), selected ? selected.get("Description") : null);
        }
    },

    /**
     * @param {CMDBuildUI.view.fields.Reference} combo
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onClearTrigger: function (combo, trigger, eOpts) {
        combo.clearValue();
        if (combo.hasBindingValue) {
            combo.getBind().value.setValue(null);
        }
    },

    /**
    * @param {CMDBuildUI.view.fields.Reference} view
    * @param {Ext.form.trigger.Trigger} trigger
    * @param {Object} eOpts
    */
    onSearchTrigger: function (view, trigger, eOpts) {
        var object = CMDBuildUI.util.helper.ModelHelper.getObjectFromName(view.metadata.targetClass);
        var objectTypeDescription = object.getTranslatedDescription();
        var title = Ext.String.format(
            "{0} - {1}",
            CMDBuildUI.locales.Locales.common.grid.list,
            objectTypeDescription
        );
        var popup = CMDBuildUI.util.Utilities.openPopup(null, title, {
            xtype: 'fields-reference-selectionpopup',
            viewModel: {
                data: {
                    objectType: view.metadata.targetType,
                    objectTypeName: view.metadata.targetClass,
                    objectTypeDescription: objectTypeDescription,
                    storeinfo: {
                        type: null,
                        proxyurl: view.getViewModel().get("storeinfo.proxyurl"),
                        autoload: false,
                        ecqlfilter: view.getEcqlFilter()
                    },
                    selection: view.getSelection()
                }
            },

            setValueOnParentCombo: function (record) {
                if (view.column) {
                    var object = view._ownerRecord;
                    object.set(view.getName(), record[0].getId());
                }
                view.setSelection(record);
            },

            closePopup: function () {
                popup.removeAll(true);
                popup.close();
            }
        });
    }
});
