Ext.define('CMDBuildUI.view.fields.lookup.LookupComboController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.fields-lookupcombofield',

    control: {
        '#': {
            cleartrigger: 'onClearTrigger'
        }
    },

    /**
     * @param {CMDBuildUI.view.fields.Reference} combo
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onClearTrigger: function (combo, trigger, eOpts) {
        combo.clearValue();
        if (combo.hasBindingValue) {
            combo.getBind().value.setValue(null);
        }
    }

});
