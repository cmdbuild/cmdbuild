
Ext.define('CMDBuildUI.view.fields.lookup.LookupCombo', {
    extend: 'Ext.form.field.ComboBox',

    requires: [
        'CMDBuildUI.view.fields.lookup.LookupComboController'
    ],

    alias: 'widget.lookupcombofield',
    controller: 'fields-lookupcombofield',
    viewModel: {},

    config: {
        /**
         * @cfg {String} recordLinkName
         * The name of the full record in ViewModel used for 
         * value binding.
         */
        recordLinkName: null,

        /**
         * @cfg {Object} filter
         * Ecql filter definition.
         */
        filter: null
    },

    valueField: '_id',
    displayField: 'text',
    forceSelection: true,
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function (combo, trigger, eOpts) {
                combo.fireEvent("cleartrigger", combo, trigger, eOpts);
            }
        }
    },

    /**
     * @override
     * @method
     * Template method, it is called when a new store is bound
     * to the current instance.
     * @protected
     * @param {Ext.data.AbstractStore} store The store being bound
     * @param {Boolean} initial True if this store is being bound as initialization of the instance.
     */
    onBindStore: function (store, initial) {
        var me = this;
        var vm = this.getViewModel();
        if (store.getModel().getName()) {
            if (me.getRecordLinkName() && me.getFilter()) {
                var binds = CMDBuildUI.util.ecql.Resolver.getViewModelBindings(
                    me.getFilter(),
                    me.getRecordLinkName()
                );
                if (!Ext.Object.isEmpty(binds)) {
                    vm.bind({
                        bindTo: binds
                    }, function (data) {
                        store.getFilters().add({
                            property: CMDBuildUI.proxy.BaseProxy.filter.ecql,
                            value: me.getEcqlFilter()
                        });
                    });
                } else {
                    vm.bind({
                        ownerRecord: '{' + me.getRecordLinkName() + '}'
                    }, function () {
                        if (!Ext.Object.isEmpty(me.getEcqlFilter())) {
                            store.addFilter([{
                                property: CMDBuildUI.proxy.BaseProxy.filter.ecql,
                                value: me.getEcqlFilter()
                            }]);
                        }
                    });
                }
            }
        }
        this.callParent(arguments);
    },

    /**
     * Get proxy store extra params
     * @return {Object}
     */
    getEcqlFilter: function () {
        var ecql;
        var obj = this._ownerRecord;
        if (this.getFilter() && obj) {
            ecql = CMDBuildUI.util.ecql.Resolver.resolve(this.getFilter(), obj);
        }
        return ecql || {};
    }
});