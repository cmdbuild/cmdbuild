
Ext.define('CMDBuildUI.view.fields.lookup.Lookup', {
    extend: 'Ext.form.FieldContainer',

    requires: [
        'CMDBuildUI.view.fields.lookup.LookupController',
        'CMDBuildUI.view.fields.lookup.LookupModel'
    ],

    mixins: [
        'Ext.form.field.Field'
    ],

    alias: 'widget.lookupfield',
    controller: 'fields-lookupfield',
    viewModel: {
        type: 'fields-lookupfield'
    },

    config: {
        /**
         * @cfg {String} recordLinkName
         * The name of the full record in ViewModel used for 
         * value binding.
         */
        recordLinkName: null
    },

    layout: 'anchor',

    /**
     * @property {Boolean} isFieldContainer
     */
    isFieldContainer: true,

    defaults: {
        xtype: 'lookupcombofield',
        anchor: '100%',
        margin: 0
    },

    /**
     * Get lookup type.
     * @return {String}
     */
    getLookupType: function () {
        return this.metadata.lookupType;
    },

    /**
     * 
     */
    getValue: function() {
        var combo = this.lookup("combo0");
        return combo && combo.getValue() || null;
    }
});
