Ext.define('CMDBuildUI.view.fields.lookup.LookupController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.fields-lookupfield',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#combo0': {
            change: 'onMainComboChange'
        }
    },

    /**
     * On before render
     * @param {CMDBuildUI.view.fields.lookup.Lookup} view
     * @param {Object} eOpts 
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        var items = [];
        var stores = {};
        var values = {};
        var comboscount = 0;

        var singlelookup;

        var lt = Ext.getStore("lookups.LookupTypes").getById(CMDBuildUI.util.Utilities.stringToHex(view.getLookupType()));
        while (lt) {
            var hasparent = !Ext.isEmpty(lt.get("parent"));

            singlelookup = comboscount === 0 && hasparent === false ? true : false;

            // add store
            var storename = 'options' + comboscount;
            var store = {
                model: 'CMDBuildUI.model.lookups.Lookup',
                proxy: {
                    url: CMDBuildUI.util.api.Lookups.getLookupValues(lt.get("name")),
                    type: 'baseproxy'
                },
                remoteFilter: singlelookup ? true : false,
                remoteSort: true,
                autoLoad: true,
                autoDestroy: true
            };

            if (hasparent) {
                var currentindex = comboscount;
                store.filters = [{
                    property: 'parent_id',
                    value: '{value' + (comboscount + 1) + '}'
                }];
                store.listeners = {
                    filterchange: function (s, f) {
                        var depcombo = view.lookupReference(Ext.String.format("combo{0}", currentindex));
                        if (s.find("_id", depcombo.getValue()) - 1) {
                            depcombo.clearValue();
                        }
                    }
                };
            }
            stores[storename] = store;

            if (comboscount !== 0) {
                values["value" + comboscount] = null;
            }

            // define bind
            var bind = (comboscount === 0 && view.initialConfig.bind) ? view.initialConfig.bind : {};
            bind = Ext.applyIf(bind, {
                store: Ext.String.format("{{0}}", storename),
                value: Ext.String.format("{value{0}}", comboscount)
            });

            var combo = {
                bind: bind,
                name: comboscount === 0 ? view.getName() : undefined,
                reference: Ext.String.format("combo{0}", comboscount),
                itemId: Ext.String.format("combo{0}", comboscount),
                queryMode: 'local',
                tabIndex: view.tabIndex
            };

            // add filter configuration
            if (singlelookup && view.metadata.ecqlFilter) {
                Ext.apply(combo, {
                    recordLinkName: view.getRecordLinkName(),
                    filter: view.metadata.ecqlFilter
                });
            }

            // define item
            Ext.Array.insert(items, 0, [combo]);

            comboscount++;
            if (hasparent) {
                lt = Ext.getStore("lookups.LookupTypes").getById(lt.get("parent"));
            } else {
                lt = null;
            }
        }

        vm.setStores(stores);
        vm.set(values);
        view.add(items);
    },

    /**
     * @param {Ext.form.field.ComboBox} combo
     * @param {Numeric|String} newvalue
     * @param {Numeric|String} oldvalue
     * @param {Object} eOpts
     */
    onMainComboChange: function (combo, newvalue, oldvalue, eOpts) {
        var parent = this.getView();

        function updateObjectMetaFrom(object) {
            if (object && object.isModel) {
                var selected = combo.getSelection();
                object.set(Ext.String.format("_{0}_code", parent.getName()), selected ? selected.get("code") : null);
                object.set(Ext.String.format("_{0}_description", parent.getName()), selected ? selected.get("description") : null);
                object.set(Ext.String.format("_{0}_description_translation", parent.getName()), selected ? selected.get("_description_translation") : null);
            }
        }

        if (parent.column) {
            parent.column.getView().ownerGrid.on({
                edit: {
                    fn: function (editor, context, eOpts) {
                        context.record.set(parent.getName(), newvalue);
                        updateObjectMetaFrom(context.record);
                    },
                    scope: this,
                    single: true
                }
            });
        } else {
            updateObjectMetaFrom(combo._ownerRecord);
        }
    }

});
