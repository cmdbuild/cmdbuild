Ext.define('CMDBuildUI.view.bim.ContainerBimController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.bim-containerbim',

    /**
     * @param  {Number} poid the identifier for the bim project
     * @param  {String} type Ifc4 or Ifc2x3tc1
     * @param  {} observer
     */
    onDivRendered: function (poid, type, observer) {
        CMDBuildUI.util.bim.Viewer.show(poid, type);
    }
});
