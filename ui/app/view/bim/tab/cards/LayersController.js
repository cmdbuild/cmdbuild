Ext.define('CMDBuildUI.view.bim.tab.cards.LayersController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.bim-tab-cards-layers',
    listen: {
        global: {
            highlitedifcobject: 'onHighlitedIfcObject'
        }
    },

    /**
     * @param {[Stringi]} ifcNames contains the name of ifcLayer wich will change the transparence
     * @param {Number} value the state of transparence: 0, 1, 2 
     */
    onLayerCheckDidChange: function (ifcNames, value) {
        ifcNames.forEach(function (name) {
            name = 'Ifc'+name;
            switch (value) {
                case 0:
                    CMDBuildUI.util.bim.Viewer.showLayer(name);
                    break;
                case 1:
                    CMDBuildUI.util.bim.Viewer.semiHideLayer(name);
                    break;
                case 2:
                    CMDBuildUI.util.bim.Viewer.hideLayer(name);
                    break;
                default:
                    console.log('Unvalid value');
            }
        }, this);

        CMDBuildUI.util.bim.Viewer.changeTransparence(1); //FUTURE: Operate here to set a global transparency to full opacity elements        
    },
    /**
     * 
     */
    onHighlitedIfcObject: function (highlited) {
        store = this.getViewModel().get('bimIfcLayerStore');
        if (store) {
            var name = highlited.data.object.object._t.replace('Ifc',"");

            var record = store.findRecord('name', name);
            var gridPanel = this.getView().lookupReference('bim-gridpanel');
            // gridPanel.setSelection(record);

            var view = this.getView();
            var gridView = view.lookupReference('bim-gridpanel');
            gridView.ensureVisible(record, {
                select: true,
                focus: true
            });
        }


    }
});
