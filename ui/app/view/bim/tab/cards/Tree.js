
Ext.define('CMDBuildUI.view.bim.tab.cards.Tree', {
    extend: 'Ext.tree.Panel',

    requires: [
        'CMDBuildUI.view.bim.tab.cards.TreeController',
        'CMDBuildUI.view.bim.tab.cards.TreeModel'
    ],

    alias: 'widget.bim-tab-cards-tree',

    controller: 'bim-tab-cards-tree',

    viewModel: {
        type: 'bim-tab-cards-tree'
    },

    root: {
        text: 'ifcTree Root',
        children: []
    },

    rootVisible: false,
    layout: 'fit',

    columns: [{
        xtype: 'treecolumn',
        text: CMDBuildUI.locales.Locales.bim.tree.columnLabel,
        localize: {
            text: 'CMDBuildUI.locales.Locales.bim.tree.columnLabel'
        },
        dataIndex: 'text',
        menuDisabled: true,
        sortable: false,
        flex: 1
    }, {
        xtype: 'actioncolumn',
        align: 'center',
        tooltip: CMDBuildUI.locales.Locales.bim.tree.arrowTooltip,
        localize: {
            tooltip: 'CMDBuildUI.locales.Locales.bim.tree.arrowTooltip'
        },
        menuDisabled: true,
        sortable: false,
        width: 50,
        items: [{
            getClass: function (v, meta, row, rowIndex, colIndex, store) {
                var leaf = row.get('leaf');
                if (leaf) {
                    return 'x-fa fa-arrow-right';
                }
                return null;
            },
            handler: function (v, rowIndex, colIndex, item, e, record, row) {
                //NOTE:in cmdbuild 2.5 si passava per piu' funzioni, qui chiamao direttamente chi fa la chiamata
                var oid = record.get('oid');
                var leaf = record.get('leaf');
                CMDBuildUI.util.bim.Viewer.select(oid, leaf);
            }
        }]
    }]

    /**
     * NOTE:
     * The tree is popolated in the controller. The function wich popolates the tree
     * is called after a global event is fired
     */
});
