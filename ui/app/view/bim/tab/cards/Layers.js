
Ext.define('CMDBuildUI.view.bim.tab.cards.Layers', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.bim.tab.cards.LayersController',
        'CMDBuildUI.view.bim.tab.cards.LayersModel'
    ],

    alias: 'widget.bim-tab-cards-layers',
    controller: 'bim-tab-cards-layers',
    viewModel: {
        type: 'bim-tab-cards-layers'
    },
    scrollable: 'y',
    items: [{
        xtype: 'panel',
        items: [{
            xtype: 'toolbar',
            items: [{
                xtype: 'tbfill'
            }, {
                iconCls: 'x-fa fa-eye',
                tooltip: CMDBuildUI.locales.Locales.bim.layers.menu.showAll,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.layers.menu.showAll'
                },
                cls: 'management-tool',
                xtype: 'tool',
                handler: function (button, e) {
                    var view = this.up().up().up(); //FIXME: Dont use up() method
                    var store = view.getViewModel().get('bimIfcLayerStore');
                    var range = store.getRange();

                    var ifcNames = [];
                    range.forEach(function (element) {
                        element.set('clicks', 0); // set each action column with the correct value
                        ifcNames.push(element.get('name'));
                    }, this);
                    view.getController().onLayerCheckDidChange(ifcNames, 0);
                }
            }, {
                iconCls: 'x-fa fa-eye-slash',
                tooltip: CMDBuildUI.locales.Locales.bim.layers.menu.hideAll,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.layers.menu.hideAll'
                },
                cls: 'management-tool',
                xtype: 'tool',
                handler: function (button, e) {
                    var view = this.up().up().up(); //FIXME: Dont use up() method
                    var store = view.getViewModel().get('bimIfcLayerStore');
                    var range = store.getRange();

                    var ifcNames = [];
                    range.forEach(function (element) {
                        element.set('clicks', 2); // set each action column with the correct value
                        ifcNames.push(element.get('name'));
                    }, this);

                    view.getController().onLayerCheckDidChange(ifcNames, 2);
                }
            }]
        }]
    }, {
        xtype: 'gridpanel',
        layout: 'fit',
        reference: 'bim-gridpanel',
        bind: {
            store: '{bimIfcLayerStore}'
        },
        columns: [{
            text: CMDBuildUI.locales.Locales.bim.layers.name,
            localize: {
                text: 'CMDBuildUI.locales.Locales.bim.layers.name',
            },
            dataIndex: 'name',
            flex: 3,
            align: 'left'
        }, {
            text: CMDBuildUI.locales.Locales.bim.layers.visivility,
            localize: {
                text: 'CMDBuildUI.locales.Locales.bim.layers.visivility'
            },
            //dataIndex: 'clicks', //LOOK: If i enable the dataIndex then the style when clicking the icon changes
            flex: 3,
            align: 'center',
            xtype: 'actioncolumn',
            menuDisabled: true,
            sortable: false,
            items: [{ //IMPROVE: Forse non ho bisogno di items ma posso direttamente inserire handler e getClass
                handler: function (actioncolumn, rowIndex, colIndex, item, event, record, row) {
                    view = this.up().up().up();

                    var clicks = (record.get('clicks') + 1) % 3;
                    record.set('clicks', clicks);
                    view.getController().onLayerCheckDidChange([record.get('name')], clicks);
                },
                getClass: function (v, meta, row, rowIndex, colIndex, store) {
                    switch (row.get('clicks')) {
                        case 0:
                            return 'full-eye';
                        case 1:
                            return 'half-eye';
                        case 2:
                            return 'gray-eye';
                    }
                }
            }]
        }, {
            text: CMDBuildUI.locales.Locales.bim.layers.qt,
            localize: {
                text: 'CMDBuildUI.locales.Locales.bim.layers.qt'
            },
            dataIndex: 'qt',
            flex: 2,
            align: 'left'
        }]
    }]
});
