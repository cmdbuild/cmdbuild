
Ext.define('CMDBuildUI.view.bim.tab.cards.Controls', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.bim.tab.cards.ControlsController',
        'CMDBuildUI.view.bim.tab.cards.ControlsModel'
    ],
    alias: 'widget.bim-tab-cards-controls',
    controller: 'bim-tab-cards-controls',
    viewModel: {
        type: 'bim-tab-cards-controls'
    },


    items: [
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [{
                xtype: 'button',
                text: 'Reset',
                flex: 1,
                handler: function() {
                    CMDBuildUI.util.bim.Viewer.defaultView();
                }
            }, {
                xtype: 'button',
                text: 'Front',
                flex: 1,
                handler: function() {
                    CMDBuildUI.util.bim.Viewer.frontView();
                }
            }, {
                xtype: 'button',
                text: 'Side',
                flex: 1,
                handler: function() {
                    CMDBuildUI.util.bim.Viewer.sideView();
                }
            }, {
                xtype: 'button',
                text: 'Top',
                flex: 1,
                handler: function() {
                    CMDBuildUI.util.bim.Viewer.topView();
                }
            }]
        }
    ]
});
