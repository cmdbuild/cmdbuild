Ext.define('CMDBuildUI.view.bim.tab.cards.TreeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.bim-tab-cards-tree',
    listen: {
        global: {
            ifctreeready: 'onIfcTreeReady',
            highlitedifcobject: 'onHighlitedIfcObject'
        }
    },

    init: function () {
        this.ifcLayers = [];
    },

    /**
     * This function is executed when the ifctreeready is fired
     */
    onIfcTreeReady: function () {
        console.log('OnIfcTreeReady');
        var ifcRoot = CMDBuildUI.util.bim.IfcTree.getIfcRoot();
        var ifcTreeCreation = this.ifcTreeCreationRecursive(ifcRoot[ifcRoot.length - 1], {});
        this.getView().getRootNode().appendChild(ifcTreeCreation);
        this.openDepth(this.getView().getRootNode(), this.DEPTH);

        this.getViewModel().getParent().set('ifcLayers', this.computeObject());
    },
    /**
     * this function takes an object and removes the reference they had and creates an array with those values
     * @example 
     * the input:
     * {
     *  'name' : 'Ugo',
     *  'age' : '27',
     *  'color' : [125,255,17]
     * }
     * the output: 
     * [Ugo,27,[125,255,17]]
     * @returns {[*]} look the example  
     */
    computeObject: function () {
        //removes the unwanted types
        delete this._tmpLayers.IfcProject;

        var ifcLayers = [];
        for (var ifcName in this._tmpLayers) {
            if (this._tmpLayers.hasOwnProperty(ifcName)) {

                // sets some default values to action column
                var mode = CMDBuildUI.util.bim.Viewer.transparentLayers[ifcName];
                if (mode != null) {
                    this._tmpLayers[ifcName].clicks = mode;
                } else {
                    this._tmpLayers[ifcName].clicks = 0;
                }

                ifcLayers.push(this._tmpLayers[ifcName]);
            }
        }

        return ifcLayers;
    },


    /**
     * recursive function for generating from the raw ifcTree the EXTJS root tree with childs
     * The function also makes operatins for the layer tab
     * @param {Object} node the current analized node 
     */
    ifcTreeCreationRecursive: function (node) {

        /**
          * manage the modification of this._tmpLayers for another file (Layers.js)
        */
        var name = node.ifcObject.object._t;
        if (!this._tmpLayers[name]) {
            this._tmpLayers[name] = {
                name: name.replace('Ifc', ""),
                qt: 1
            };
        } else {
            this._tmpLayers[name].qt++;
        }


        /**
         * manage the creation of the ifcTree
        */
        var text;
        if (node.ifcObject.object._t == 'IfcProject') {
            text = node.ifcObject.object.LongName || 'IfcProject';
        } else {
            text = (node.ifcObject.object._t || "").replace("Ifc", "") + " " + (node.ifcObject.object.Name || "");
        }
        var tmpNode = {
            text: text,
            children: [],
            oid: node.ifcObject.oid,
            leaf: true
        };
        node.children.forEach(function (childNode) {
            tmpNode.leaf = false;

            var childTemp = this.ifcTreeCreationRecursive(childNode);
            tmpNode.children.push(childTemp);
        }, this);

        return tmpNode;
    },

    /**
     * This functions expands the tree from selected ifcObject in canvas to the root
     * @param {Object} highlited The higlited object in the canvas
     */
    onHighlitedIfcObject: function (highlited) {
        var oid = highlited.getData().object.oid;

        var storeRoot = this.getView().getStore().getRoot();
        var node = this.recursiveFound(storeRoot, oid);

        if (node == null) {
            console.log("Attenction, node not found");
        } else {
            // collapse the tree
            var tmpNode = this.getView().getSelection()[0];

            if (tmpNode && tmpNode.get('oid') == oid) {
                return;
            }

            while (tmpNode != null) {
                tmpNode.collapse();
                tmpNode = tmpNode.parentNode;
            }
        }

        //set selection in treePanel
        this.getView().selectPath(node.getPath());
        console.log(highlited);
    },

    /**
     * This function gets a node and looks in hi children in or to find the one with a specific value
     * @param {} node The node to inspect
     * @param {} value the value of the node we are looking for
     * @returns the node with node.value = value
     */
    recursiveFound: function (node, value) {
        if (node.data.oid == value) {
            return node;
        } else {
            for (var i = 0; i < node.childNodes.length; i++) {
                var tmpChild = this.recursiveFound(node.childNodes[i], value);
                if (tmpChild) {
                    return tmpChild;
                }
            }
        }

        return null;
    },

    privates: {
        /**
         * This array contains the ifcLayers found wile exlplorying the tree 
         */
        _tmpLayers: [],

        /**
         * 
         */
        DEPTH: 3,

        /**
         * This function expands all the nodes wit a depth value <= depth
         * @param {object} node the node we start;
         * @param {depth} depth the value of depth to reach from that node;
         */
        openDepth: function (node, depth) {
            var nodeDepth = node.getDepth();
            if (nodeDepth <= depth) {
                node.expand();

                var childNodes = node.childNodes;
                for (var i = 0; i < childNodes.length; i++) {
                    this.openDepth(childNodes[i], depth);
                }
            }


            // this.recursiveOpenDepth(node,0,depth);
        }
    }
});
