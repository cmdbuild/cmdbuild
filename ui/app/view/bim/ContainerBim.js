
Ext.define('CMDBuildUI.view.bim.ContainerBim', {
    extend: 'Ext.container.Container',
    requires: [
        'CMDBuildUI.view.bim.ContainerBimController',
        'CMDBuildUI.view.bim.ContainerBimModel'
    ],

    alias: 'widget.bim-container',
    controller: 'bim-containerbim',
    viewModel: {
        type: 'bim-containerbim'
    },

    layout: 'border',
    items: [{ //TODO: Set a maxWidth
        region: 'west',
        width: '33%',
        split: true,
        collapsible: false,
        xtype: 'tabpanel',
        layout: 'fit',
        ui: 'managementlighttabpanel',
        items: [{
            xtype: 'bim-tab-cards-tree',
            title: CMDBuildUI.locales.Locales.bim.tree.label,
            localize: {
                title: 'CMDBuildUI.locales.Locales.bim.tree.label'
            }

        }, {
            xtype: 'bim-tab-cards-layers',
            title: CMDBuildUI.locales.Locales.bim.layers.label,
            localize: {
                title: 'CMDBuildUI.locales.Locales.bim.layers.label'
            },
            reference: 'bim-tab-cards-layers'
        }]
    }, {
        xtype: 'panel',
        region: 'center',
        layout: 'fit',
        html: '<div id="divBim3DView" style="height: inherit;width: inherit;">CANVAS</div>',
        fbar: [{
            xtype: 'toolbar',
            flex: 1,
            items: [{
                xtype: 'tbtext',
                html: CMDBuildUI.locales.Locales.bim.menu.camera + ":"
            }, {
                iconCls: 'cmdbuildicon-default-bim',
                tooltip: CMDBuildUI.locales.Locales.bim.menu.resetView,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.menu.resetView'
                },
                cls: 'management-tool',
                xtype: 'tool',
                handler: function () {
                    CMDBuildUI.util.bim.Viewer.defaultView();
                }
            }, {
                iconCls: 'cmdbuildicon-front-bim',
                tooltip: CMDBuildUI.locales.Locales.bim.menu.frontView,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.menu.frontView'
                },
                cls: 'management-tool',
                xtype: 'tool',
                handler: function () {
                    CMDBuildUI.util.bim.Viewer.frontView();
                }
            }, {
                iconCls: 'cmdbuildicon-side-bim',
                tooltip: CMDBuildUI.locales.Locales.bim.menu.sideView,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.menu.sideView'
                },
                cls: 'management-tool',
                xtype: 'tool',
                handler: function () {
                    CMDBuildUI.util.bim.Viewer.sideView();
                }
            }, {
                iconCls: 'cmdbuildicon-top-bim',
                tooltip: CMDBuildUI.locales.Locales.bim.menu.topView,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.menu.topView'
                },
                cls: 'management-tool',
                xtype: 'tool',
                handler: function () {
                    CMDBuildUI.util.bim.Viewer.topView();
                }
            }, {
                xtype: 'tbseparator'
            },
            //  {
            //      xtype: 'tbtext', html: 'Zoom:'
            // }, {
            //     xtype: 'slider',
            //     width: 200,
            //     value: 50,
            //     increment: 10,
            //     minValue: 0,
            //     maxValue: 100,
            //     controller: {
            //         control: {
            //             '#': {
            //                 change: 'onChange'
            //             }
            //         },

            //         /**
            //          * 
            //          */
            //         onChange: function (slider, newValue, thumb, eOpts) {
            //             console.log('zoom Changed');

            //             var MAX_ZOOM = 15;
            //             var zoom = MAX_ZOOM - (newValue / 5);
            //             CMDBuildUI.util.bim.SceneTree.setZoomLevel(zoom);
            //         }
            //     }
            // }, {
            //     xtype: 'tbseparator'
            // }, 61511
            {
                xtype: 'tbtext',
                html: CMDBuildUI.locales.Locales.bim.menu.mod + ':',
                localize: {
                    html: 'CMDBuildUI.locales.Locales.bim.menu.mod' + ':'
                }
            }, {
                iconCls: 'x-fa fa-arrows',
                tooltip: CMDBuildUI.locales.Locales.bim.menu.pan,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.menu.pan'
                },
                cls: 'management-tool',
                xtype: 'tool',
                disabled: true,
                handler: function () {
                    console.log('To Implement'); //TODO:
                }
            }, {
                iconCls: 'x-fa fa-repeat',
                tooltip: CMDBuildUI.locales.Locales.bim.menu.rotate,
                localize: {
                    tooltip: 'CMDBuildUI.locales.Locales.bim.menu.rotate'
                },
                cls: 'management-tool',
                xtype: 'tool',
                disabled: true,
                handler: function () {
                    console.log('TO IMPLEMENT'); // TODO:
                }
            }, {
                xtype: 'tbfill'
            }]
        }],
        controller: {
            control: {
                '#': {
                    afterrender: 'onAfterRender',
                    resize: 'onResize'
                }
            },
            onAfterRender: function () {
                var pId = this.getView().getBubbleParent().projectId;
                this.getView().getBubbleParent().getController().onDivRendered(pId, 'ifc2x3tc1'); //TODO: make dinamic the ifcType
            },
            /**
             * This function handles the resize of the canvas
             * @param panel the panel
             * @param height the height of the panel
             * @param width the width of the panel 
             */
            onResize: function (panel, height, width) {
                CMDBuildUI.util.bim.SceneTree.resize(height, width);
            }
        }
    }]
});
