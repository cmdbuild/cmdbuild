Ext.define('CMDBuildUI.view.classes.cards.TabPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.classes-cards-tabpanel',

    control: {
        '#': {
            beforerender: "onBeforeRender",
            tabchange: 'onTabChange'
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.TabPanel} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        // set view model variables
        var vm = this.getViewModel();
        var action = vm.get("action");
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");

        // update url on window close
        var detailsWindow = Ext.getCmp('CMDBuildManagementDetailsWindow');
        view.mon(detailsWindow, 'beforeclose', this.closingManagedEvent, this);

        // init tabs
        var configAttachments = CMDBuildUI.util.helper.Configurations.get("cm_system_dms_enabled");
        var card, tabcard, tabmasterdetail, tabnotes, tabrelations, tabhistory, tabemails, tabattachments;

        if (action === CMDBuildUI.view.classes.cards.TabPanel.actions.edit) {
            card = {
                xtype: 'classes-cards-card-edit',
                tab: 'edit',
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.edit
            };
        } else if (
            action === CMDBuildUI.view.classes.cards.TabPanel.actions.create ||
            action === CMDBuildUI.view.classes.cards.TabPanel.actions.clone
        ) {
            card = {
                xtype: 'classes-cards-card-create',
                tab: 'new',
                autoScroll: true,
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.create
            };
        } else {
            card = {
                xtype: 'classes-cards-card-view',
                objectTypeName: vm.get("objectTypeName"),
                objectId: vm.get("objectId"),
                shownInPopup: true,
                autoScroll: true,
                tab: 'view',
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.view
            };
        }

        // Card tab
        tabcard = view.add({
            xtype: "panel",
            iconCls: 'x-fa fa-file-text',
            items: [card],
            reference: card.tab,
            layout: 'fit',
            autoScroll: true,
            padding: 0,
            tabAction: card.tabAction,
            tabConfig: {
                tabIndex: 0,
                title: null,
                tooltip: CMDBuildUI.locales.Locales.common.tabs.card
            },
            bind: {
                disabled: '{disabled.card}'
            }
        });

        // Master/Detail tab
        if (privileges.card_tab_detail_access) {
            tabmasterdetail = view.add({
                xtype: 'relations-masterdetail-tabpanel',
                iconCls: 'x-fa fa-th-list',
                reference: "details",
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.masterdetail,
                tabConfig: {
                    tabIndex: 1,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.details
                }
            });
        }

        // Notes tab
        if (privileges.card_tab_note_access) {
            tabnotes = view.add({
                xtype: 'notes-panel',
                iconCls: 'x-fa fa-sticky-note',
                reference: "notes",
                autoScroll: true,
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.notes,
                tabConfig: {
                    tabIndex: 2,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.notes
                },
                bind: {
                    disabled: '{disabled.notes}'
                }
            });
        }

        // Relations tab
        if (privileges.card_tab_relation_access) {
            tabrelations = view.add({
                xtype: 'relations-list-container',
                iconCls: 'x-fa fa-link',
                reference: 'relations',
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.relations,
                tabConfig: {
                    tabIndex: 3,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.relations
                },
                autoScroll: true,
                bind: {
                    disabled: '{disabled.relations}'
                }
            });
        }


        // History tab
        if (privileges.card_tab_history_access) {
            tabhistory = view.add({
                xtype: "panel",
                iconCls: 'x-fa fa-history',
                items: [{
                    xtype: 'history-grid',
                    viewModel: { // TODO: modify when className in view model become name
                        data: {
                            targetType: 'class',
                            targetTypeName: vm.get("objectTypeName"),
                            targetId: vm.get("objectId")
                        }
                    },
                    autoScroll: true
                }],
                reference: 'history',
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.history,
                tabConfig: {
                    tabIndex: 4,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.history
                },
                layout: 'fit',
                autoScroll: true,
                padding: 0,
                bind: {
                    disabled: '{disabled.history}'
                }
            });
        }

        // Email tab
        if (privileges.card_tab_email_access) {
            tabemails = view.add({
                xtype: "emails-container",
                iconCls: 'x-fa fa-envelope',
                reference: 'emails',
                autoScroll: true,
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.emails,
                tabConfig: {
                    tabIndex: 5,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.emails
                },
                viewModel: {
                    data: {
                        objectId: vm.get('objectId'),
                        objectTypeName: vm.get('objectTypeName'),
                        objectType: vm.get('objectType')
                    }
                },
                bind: {
                    disabled: '{disabled.email}'
                }
            });
        }

        // Attachments tab
        if (configAttachments && privileges.card_tab_attachment_access) {
            tabattachments = view.add({
                xtype: "attachments-container",
                iconCls: 'x-fa fa-paperclip',
                reference: 'attachments',
                autoScroll: true,
                tabAction: CMDBuildUI.view.classes.cards.TabPanel.actions.attachments,
                tabConfig: {
                    tabIndex: 6,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.attachments
                },
                padding: 0,
                viewModel: { // TODO: modify when className in view model become name
                    data: {
                        targetType: 'class',
                        targetTypeName: vm.get("objectTypeName"),
                        targetId: vm.get("objectId")
                    }
                },
                bind: {
                    disabled: '{disabled.attachments}'
                }
            });
        }

        // set view active tab
        var activetab;
        switch (action) {
            case CMDBuildUI.view.classes.cards.TabPanel.actions.masterdetail:
                activetab = tabmasterdetail;
                break;
            case CMDBuildUI.view.classes.cards.TabPanel.actions.notes:
                activetab = tabnotes;
                break;
            case CMDBuildUI.view.classes.cards.TabPanel.actions.relations:
                activetab = tabrelations;
                break;
            case CMDBuildUI.view.classes.cards.TabPanel.actions.history:
                activetab = tabhistory;
                break;
            case CMDBuildUI.view.classes.cards.TabPanel.actions.emails:
                activetab = tabemails;
                break;
            case CMDBuildUI.view.classes.cards.TabPanel.actions.attachments:
                activetab = tabattachments;
                break;
        }
        if (!activetab) {
            activetab = tabcard;
        }
        view.setActiveTab(activetab);
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.TabPanel} view
     * @param {Ext.Component} newtab
     * @param {Ext.Component} oldtab
     * @param {Object} eOpts
     */
    onTabChange: function (view, newtab, oldtab, eOpts) {
        CMDBuildUI.util.Navigation.updateCurrentManagementContextAction(newtab.tabAction);
        this.getViewModel().getParent().set("actionDescription", newtab.tabConfig.tooltip);
        this.redirectTo(this.getBasePath() + '/' + newtab.tabAction);
    },

    /**
     * @param {CMDBuildUI.model.classes.Card} record
     * @param {Object} eOpts
     */
    onItemCreated: function (record, eOpts) {
        Ext.ComponentQuery.query('classes-cards-grid-grid')[0].fireEventArgs('reload', [record, 'add']);
        this.redirectTo('classes/' + record.getRecordType() + '/cards/' + record.getRecordId(), true);
    },

    /**
     * @param {CMDBuildUI.model.classes.Card} record
     * @param {Object} eOpts
     */
    onItemUpdated: function (record, eOpts) {
        Ext.ComponentQuery.query('classes-cards-grid-grid')[0].fireEventArgs('reload', [record, 'update']);
        this.redirectTo('classes/' + record.getRecordType() + '/cards/' + record.getRecordId(), true);
    },

    /**
     * @param {Object} eOpts
     */
    onCancelCreation: function (eOpts) {

        var detailsWindow = Ext.getCmp('CMDBuildManagementDetailsWindow');
        detailsWindow.fireEvent('closed');
    },

    /**
     * @param {Object} eOpts
     */
    onCancelUpdating: function (eOpts) {

        var detailsWindow = Ext.getCmp('CMDBuildManagementDetailsWindow');
        detailsWindow.fireEvent('closed');
    },

    closingManagedEvent: function () {
        var me = this;
        CMDBuildUI.util.Navigation.updateCurrentManagementContextAction(undefined);
        CMDBuildUI.util.Utilities.redirectTo(me.getBasePath(), true);
    },

    privates: {
        /**
         * Get resource base path for routing.
         * @return {String}
         */
        getBasePath: function () {
            var vm = this.getViewModel();
            var url = 'classes/' + vm.get("objectTypeName") + '/cards';
            if (vm.get("objectId")) {
                url += '/' + vm.get("objectId");
            }
            return url;
        }
    }
});
