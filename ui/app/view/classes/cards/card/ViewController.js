Ext.define('CMDBuildUI.view.classes.cards.card.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.classes-cards-card-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            objecttypenamechanged: 'onObjectTypeNameChanged',
            objectidchanged: 'onObjectIdChanged'
        },
        '#editBtn': {
            click: 'onEditBtnClick'
        },
        '#openBtn': {
            click: 'onOpenBtnClick'
        },
        '#deleteBtn': {
            click: 'onDeleteBtnClick'
        },
        '#cloneBtn': {
            click: 'onCloneBtnClick'
        },
        '#bimBtn': {
            click: 'onBimButtonClick'
        },
        '#relgraphBtn': {
            click: 'onRelationGraphBtnClick'
        },
        '#openTabsBtn': {
            click: 'onOpenTabsBtnClick'
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.card.View} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        if (!view.getObjectTypeName() && !view.getObjectId()) {
            var config = view.getInitialConfig();
            if (!Ext.isEmpty(config._rowContext)) {
                var record = config._rowContext.record; // get widget record
                if (record && record.getData()) {
                    view.setObjectTypeName(record.getRecordType());
                    view.setObjectId(record.getRecordId());
                }
            }
        } else {
            this.onObjectTypeNameChanged(view, view.getObjectTypeName());
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.card.View} view
     * @param {String} newValue
     * @param {String} oldValue
     */
    onObjectTypeNameChanged: function (view, newValue, oldValue) {
        var vm = this.getViewModel();
        var me = this;
        CMDBuildUI.util.helper.ModelHelper.getModel(CMDBuildUI.util.helper.ModelHelper.objecttypes.klass, newValue).then(function (model) {
            vm.set("objectTypeName", newValue);
            vm.set("objectModel", model);

            if (view.getObjectId()) {
                me.onObjectIdChanged(view, view.getObjectId());
            }
        });

        // bind theObject to add form
        vm.bind({
            bindTo: {
                theobjecttype: '{theObject._type}',
                objectmodel: '{objectModel}'
            }
        }, function (data) {
            if (data.theobjecttype && data.objectmodel) {
                var items = [];
                if (view.getShownInPopup()) {
                    // get form fields as fieldsets
                    items = view.getDynFormFields();

                    if (!view.getHideTools()) {
                        // add toolbar
                        var toolbar = {
                            xtype: 'toolbar',
                            cls: 'fieldset-toolbar',
                            items: Ext.Array.merge([{ xtype: 'tbfill' }], view.tabpaneltools)
                        };
                        Ext.Array.insert(items, 0, [toolbar]);
                    }
                } else {
                    // get form fields as tab panel
                    var panel = CMDBuildUI.util.helper.FormHelper.renderForm(vm.get("objectModel"), {
                        mode: CMDBuildUI.util.helper.FormHelper.formmodes.read,
                        showAsFieldsets: false
                    });

                    if (!view.getHideTools()) {
                        // add toolbar
                        Ext.apply(panel, {
                            tools: view.tabpaneltools
                        });
                    }

                    items.push(panel);
                }
                view.removeAll(true);
                view.add(items);

                // add conditional visibility rules
                view.addConditionalVisibilityRules();
            }
        });
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.card.View} view
     * @param {Numeric} newValue
     * @param {Numeric} oldValue
     */
    onObjectIdChanged: function (view, newValue, oldValue) {
        var vm = this.getViewModel();
        if (view.getObjectTypeName()) {
            var modelname = CMDBuildUI.util.helper.ModelHelper.getModelName('class', view.getObjectTypeName());
            if (Ext.ClassManager.isCreated(modelname)) {
                vm.set("objectId", newValue);
                vm.linkTo("theObject", {
                    type: modelname,
                    id: newValue
                });
            }
        }
    },

    /**
     * Triggered on edit tool click.
     * 
     * @param {Ext.panel.Tool} tool
     * @param {Ext.Event} event
     * @param {Object} eOpts
     */
    onEditBtnClick: function (tool, event, eOpts) {
        var url = this.getBasePath() + '/edit';
        this.redirectTo(url, true);
    },

    /**
     * Triggered on delete tool click.
     * 
     * @param {Ext.panel.Tool} tool
     * @param {Ext.Event} event
     * @param {Object} eOpts
     */
    onDeleteBtnClick: function (tool, event, eOpts) {
        var vm = this.getViewModel();

        Ext.Msg.confirm(
            "Attention",    //TODO:translate
            "Are you sure you want to delete this card?",   //TODO:translate
            function (btnText) {
                if (btnText === "yes") {
                    CMDBuildUI.util.Ajax.setActionId('class.card.delete');
                    // get the object
                    vm.get("theObject").erase({
                        success: function (record, operation) {
                            Ext.ComponentQuery.query('classes-cards-grid-grid')[0].fireEventArgs('reload', [record, 'delete']);
                        }
                    });
                }
            }, this);
    },

    /**
     * Triggered on open tool click.
     * 
     * @param {Ext.panel.Tool} tool
     * @param {Ext.Event} event
     * @param {Object} eOpts
     */
    onOpenBtnClick: function (tool, event, eOpts) {
        var url = this.getBasePath() + '/view';
        this.redirectTo(url, true);
    },

    /**
     * 
     * @param {Ext.panel.Tool} tool
     * @param {Ext.Event} event
     * @param {Object} eOpts
     */
    onBimButtonClick: function (tool, event, eOpts) {
        CMDBuildUI.util.Utilities.openPopup('bimPopup', CMDBuildUI.locales.Locales.bim.showBimCard.bimViewer, { //FUTURE: create a configuration for passing the poid and ifctype
            xtype: 'bim-container',
            projectId: this.getViewModel().get('bim.projectid')
        });
    },

    /**
     * triggered on the relation graph btn click
     * 
     * @param {Ext.panel.Tool} tool
     * @param {Ext.Event} event
     * @param {Object} eOpts
     */
    onRelationGraphBtnClick: function (tool, event, eOpts) {
        var me = this;
        CMDBuildUI.util.Utilities.openPopup('graphPopup', CMDBuildUI.locales.Locales.relationGraph.relationGraph, {
            xtype: 'graph-graphcontainer',
            _id: me.getViewModel().get('theObject').get('_id'),
            _type_name: me.getViewModel().get('theObject').get('_type_name'),
            _type: me.getViewModel().get('theObject').get('_type')
        });
    },

    /**
     * Triggered on open tabs button click.
     * 
     * @param {Ext.panel.Tool} tool
     * @param {Ext.Event} event
     * @param {Object} eOpts
     */
    onOpenTabsBtnClick: function (tool, event, eOpts) {
        var me = this;
        var configAttachments = CMDBuildUI.util.helper.Configurations.get("cm_system_dms_enabled");
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");
        var items = [];

        // details action
        if (privileges.card_tab_detail_access) {
            items.push({
                tooltip: CMDBuildUI.locales.Locales.common.tabs.details,
                iconCls: 'x-fa fa-th-list',
                cls: 'management-tool',
                height: 32,
                listeners: {
                    click: function (menuitem, eOpts) {
                        me.redirectTo(me.getBasePath() + '/details', true);
                    }
                }
            });
        }

        // notes action
        if (privileges.card_tab_note_access) {
            items.push({
                tooltip: CMDBuildUI.locales.Locales.common.tabs.notes,
                iconCls: 'x-fa fa-sticky-note',
                height: 32,
                listeners: {
                    click: function (menuitem, eOpts) {
                        me.redirectTo(me.getBasePath() + '/notes', true);
                    }
                }
            });
        }

        // relations action
        if (privileges.card_tab_relation_access) {
            items.push({
                tooltip: CMDBuildUI.locales.Locales.common.tabs.relations,
                iconCls: 'x-fa fa-link',
                height: 32,
                listeners: {
                    click: function (menuitem, eOpts) {
                        me.redirectTo(me.getBasePath() + '/relations', true);
                    }
                }
            });
        }

        // history action
        if (privileges.card_tab_history_access) {
            items.push({
                tooltip: CMDBuildUI.locales.Locales.common.tabs.history,
                iconCls: 'x-fa fa-history',
                height: 32,
                listeners: {
                    click: function (menuitem, eOpts) {
                        me.redirectTo(me.getBasePath() + '/history', true);
                    }
                }
            });
        }

        // email action
        if (privileges.card_tab_email_access) {
            items.push({
                tooltip: CMDBuildUI.locales.Locales.common.tabs.emails,
                iconCls: 'x-fa fa-envelope',
                height: 32,
                listeners: {
                    click: function (menuitem, eOpts) {
                        me.redirectTo(me.getBasePath() + '/emails', true);
                    }
                }
            });
        }

        // attachments action
        if (configAttachments && privileges.card_tab_attachment_access) {
            items.push({
                tooltip: CMDBuildUI.locales.Locales.common.tabs.attachments,
                iconCls: 'x-fa fa-paperclip',
                height: 32,
                hidden: !configAttachments,
                listeners: {
                    click: function (menuitem, eOpts) {
                        me.redirectTo(me.getBasePath() + '/attachments', true);
                    }
                }
            });
        }

        if (items.length) {
            var menu = Ext.create('Ext.menu.Menu', {
                autoShow: true,
                items: items
            });
            menu.setMinWidth(35);
            menu.setWidth(35);
            menu.alignTo(tool.el.id, 't-b?');
        }
    },

    /**
     * 
     * @param {Ext.panel.Tool} tool
     * @param {Ext.Event} event
     * @param {Object} eOpts
     */
    onCloneBtnClick: function (tool, event, eOpts) {
        var me = this;
        var menu = Ext.create('Ext.menu.Menu', {
            autoShow: true,
            items: [{
                tooltip: CMDBuildUI.locales.Locales.classes.cards.clone,
                iconCls: 'x-fa fa-clone',
                height: 32,
                listeners: {
                    click: function (menuitem, eOpts) {
                        var view = me.getView();
                        var defaultValues = [];
                        Ext.Object.each(me.getViewModel().get("theObject").getData(), function (key, value, myself) {
                            if (key !== '_id' && value) {
                                defaultValues.push({
                                    attribute: key,
                                    value: value
                                });
                            }
                        });

                        CMDBuildUI.util.Navigation.addIntoManagementDetailsWindow('classes-cards-tabpanel', {
                            viewModel: {
                                data: {
                                    objectTypeName: view.getObjectTypeName(),
                                    action: CMDBuildUI.view.classes.cards.TabPanel.actions.clone,
                                    defaultValues: defaultValues
                                }
                            }
                        });
                    }
                }
            }, {
                tooltip: CMDBuildUI.locales.Locales.classes.cards.clonewithrelations,
                iconCls: 'x-fa fa-clone',
                height: 32,
                listeners: {
                    click: function (menuitem, eOpts) {
                        var vm = me.getViewModel();
                        var title = CMDBuildUI.locales.Locales.classes.cards.clonewithrelations;
                        var popupId = 'popup-clone-card-and-relations';

                        var config = {
                            xtype: 'classes-cards-relations-panel',
                            viewModel: {
                                data: vm.getData()
                            }
                        };
                        CMDBuildUI.util.Utilities.openPopup(
                            popupId,
                            title,
                            config,
                            {}
                        );

                    }
                }
            }
            ]
        });
        menu.setMinWidth(35);
        menu.setWidth(35);
        menu.alignTo(tool.el.id, 't-b?');
    },

    privates: {
        /**
         * Get resource base path for routing.
         * @return {String}
         */
        getBasePath: function () {
            var view = this.getView();
            return 'classes/' + view.getObjectTypeName() + '/cards/' + view.getObjectId();
        }
    }

});
