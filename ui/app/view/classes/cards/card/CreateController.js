Ext.define('CMDBuildUI.view.classes.cards.card.CreateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.classes-cards-card-create',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#savebtn': {
            click: 'onSaveBtnClick'
        },
        '#cancelbtn': {
            click: 'onCancelBtnClick'
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.card.Create} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        var me = this;

        // get model 
        CMDBuildUI.util.helper.ModelHelper
            .getModel('class', vm.get("objectTypeName"))
            .then(function (model) {
                vm.set("objectModel", model);

                // create new instance
                vm.linkTo('theObject', {
                    type: model.getName(),
                    create: true
                });

                // get form fields
                view.add(view.getDynFormFields());

                // add conditional visibility rules
                view.addConditionalVisibilityRules();
            });
    },

    /**
     * Save function
     */
    onSaveBtnClick: function () {
        var me = this;
        var vm = this.getViewModel();
        var form = this.getView();
        if (form.isValid()) {
            var theObject = vm.get("theObject");

            theObject.save({
                success: function (record, operation) {
                    if (!record.getRecordType()) {
                        record.set('_type', form.getObjectTypeName());
                    }
                    form.fireEventArgs("itemcreated", [record]);
                    if (form.getRedirectAfterSave()) {
                        me.redirectTo(Ext.String.format("classes/{0}/cards/{1}/view", record.get("_type"), record.getId()));
                    }
                },
                callback: function (record, operation, success) {

                }
            });
        } else {
            var w = Ext.create('Ext.window.Toast', {
                html: 'Please correct indicted errors',
                title: 'Error!',
                iconCls: 'x-fa fa-check-circle',
                align: 'br'
            });
            w.show();
        }
    },
    /**
     * Reset form function
     */
    onCancelBtnClick: function () {
        this.getViewModel().get("theObject").reject(); // discard changes
        this.getView().fireEventArgs("cancelcreation");
    },
    /**
     * @param {Object} theObject
     * @param {Array} fields
     */
    cleanTheObject: function (theObject, fields) {
        console.log(typeof theObject);
        delete theObject.data._user;
        delete theObject.data._beginDate;
        Ext.Array.each(fields, function (value, index) {
            if (value.cmdbuildtype) {
                if (value.cmdbuildtype.toLowerCase() === CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase()) {
                    theObject.data[value.description] = null;
                } else if (value.cmdbuildtype.toLowerCase() == CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase() && theObject.data[value.description] === 0) {
                    theObject.data[value.description] = null;
                }
            }
        });
        return theObject;
    }
});
