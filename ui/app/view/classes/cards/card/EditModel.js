Ext.define('CMDBuildUI.view.classes.cards.card.EditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.classes-cards-card-edit',

    formulas: {
        updateDescription: {
            bind: {
                description: '{theObject.Description}'
            },
            get: function(data) {
                this.getParent().set("objectDescription", data.description);
            }
        }
    }

});
