Ext.define('CMDBuildUI.view.classes.cards.card.Mixin', {
    mixinId: 'card-mixin',

    config: {
        /**
         * @cfg {String} [objectType] 
         * 
         * The object type.
         */
        objectType: CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,

        /**
         * @cfg {String} [objectTypeName]
         * 
         * Class name
         */
        objectTypeName: null,

        /**
         * @cfg {Numeric} objectTypeId
         */
        objectId: null,

        /**
         * @cfg {Boolean} redirectAfterSave
         * 
         * `true` to open the created card after save action. 
         * Defaults to true.
         */
        redirectAfterSave: true
    },

    publish: [
        'objectType',
        'objectTypeName',
        'objectId'
    ],

    twoWayBindable: [
        'objectType',
        'objectTypeName',
        'objectId'
    ],

    bind: {
        objectType: '{objectType}',
        objectTypeName: '{objectTypeName}',
        objectId: '{objectId}'
    },

    formmode: null,

    /**
     * Add rules for fields visibility
     */
    addConditionalVisibilityRules: function () {
        var fields = [];
        this.getForm().getFields().getRange().forEach(function (f) {
            if (f.updateFieldVisibility !== undefined) {
                // add field to list
                fields.push(f);
            }
        });

        this.getViewModel().bind({
            bindTo: '{theObject}',
            deep: true
        }, function (theObject) {
            fields.forEach(function (f) {
                // apply visibility function
                Ext.callback(f.updateFieldVisibility, f, [theObject]);
            });
        });
    },

    /**
     * Return form fields
     * 
     * @return {Ext.Component[]}
     */
    getDynFormFields: function () {
        var vm = this.getViewModel();
        var defaultValues, overrides;

        // get default values
        if (this.getDefaultValues) {
            defaultValues = this.getDefaultValues();
        }

        // get object overrides
        var obj = vm.get("theObject");
        if (obj) {
            overrides = obj.getOverridesFromPermissions();
        }

        // return dynamic fields
        return CMDBuildUI.util.helper.FormHelper.renderForm(vm.get("objectModel"), {
            mode: this.formmode,
            defaultValues: defaultValues,
            attributesOverrides: overrides,
            visibleAttributes: Object.keys(overrides),
            showAsFieldsets: true
        });
    }
});