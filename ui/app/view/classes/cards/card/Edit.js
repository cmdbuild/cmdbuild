
Ext.define('CMDBuildUI.view.classes.cards.card.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.classes-cards-card-edit',

    requires: [
        'CMDBuildUI.view.classes.cards.card.EditController',
        'CMDBuildUI.view.classes.cards.card.EditModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],

    mixins: [
        'CMDBuildUI.view.classes.cards.card.Mixin'
    ],

    controller: 'classes-cards-card-edit',
    viewModel: {
        type: 'classes-cards-card-edit'
    },

    autoScroll: true,

    formmode: CMDBuildUI.util.helper.FormHelper.formmodes.update,

    bubbleEvents: [
        'itemupdated',
        'cancelupdating'
    ],
    modelValidation: true,

    fieldDefaults: {
        labelAlign: 'top'
    },

    buttons: [{
        text: 'Save',
        formBind: true, //only enabled once the form is valid
        disabled: true,
        ui: 'management-action-small',
        autoEl: {
            'data-testid': 'card-edit-save'
        },
        listeners: {
            click: 'onSaveBtnClick'
        }
    }, {
        text: 'Cancel',
        ui: 'secondary-action-small',
        autoEl: {
            'data-testid': 'card-edit-cancel'
        },
        listeners: {
            click: 'onCancelBtnClick'
        }
    }]
});
