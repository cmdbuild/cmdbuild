Ext.define('CMDBuildUI.view.classes.cards.card.CreateModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.classes-cards-card-create',

    formulas: {
        title: function(get) {
            return this.getView().getObjectTypeName();
        }
    }

});
