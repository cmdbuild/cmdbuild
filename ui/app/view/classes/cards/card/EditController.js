Ext.define('CMDBuildUI.view.classes.cards.card.EditController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.classes-cards-card-edit',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        var me = this;

        /**
         * Promise success callback.
         * @param {CMDBuildUI.model.classes.Card} model 
         */
        function success(model) {
            vm.set("objectModel", model);

            // set instance to ViewModel
            vm.linkTo('theObject', {
                type: model.getName(),
                id: view.getObjectId() || vm.get("objectId")
            });
        }

        // get model 
        CMDBuildUI.util.helper.ModelHelper
            .getModel('class', view.getObjectTypeName() || vm.get("objectTypeName"))
            .then(success);

        // bind theObject to add form
        vm.bind({
            bindTo: {
                theobjecttype: '{theObject._type}',
                objectmodel: '{objectModel}'
            }
        }, function (data) {
            if (data.theobjecttype && data.objectmodel) {
                // add fields
                view.add(view.getDynFormFields());

                // add conditional visibility rules
                view.addConditionalVisibilityRules();
            }
        });
    },

    onSaveBtnClick: function () {
        var vm = this.getViewModel();
        var form = this.getView();
        var me = this;

        if (form.isValid()) {
            vm.getData().theObject.save({
                success: function (record, operation) {
                    form.fireEventArgs("itemupdated", [record]);

                    if (form.getRedirectAfterSave()) {
                        me.redirectTo(Ext.String.format(
                            "classes/{0}/cards/{1}/view",
                            record.get("_type"),
                            record.getId()
                        ));
                    }
                },
                callback: function (record, operation, success) {
                }
            });
        } else {
            var w = Ext.create('Ext.window.Toast', {
                html: 'Error!',
                title: 'Please correct indicted errors',
                iconCls: 'x-fa fa-check-circle',
                align: 'br'
            });
            w.show();
        }
    },

    onCancelBtnClick: function () {
        var vm = this.getViewModel();
        var recordType = vm.getData().theObject.get('_type');
        vm.get("theObject").reject(); // discard changes
        this.getView().fireEventArgs("cancelupdating");
    }

});
