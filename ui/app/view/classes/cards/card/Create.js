
Ext.define('CMDBuildUI.view.classes.cards.card.Create', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.classes.cards.card.CreateController',
        'CMDBuildUI.view.classes.cards.card.CreateModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],

    mixins: [
        'CMDBuildUI.view.classes.cards.card.Mixin'
    ],

    alias: 'widget.classes-cards-card-create',
    controller: 'classes-cards-card-create',
    viewModel: {
        type: 'classes-cards-card-create'
    },

    config: {
        /**
         * @cfg {Object[]} [defaultValues]
         * 
         * Can set default values for any of the attributes. An object can be:
         * `{attribute: 'attribute name', value: 'default value', editable: true|false}` 
         * used for all attributes or
         * `{domain: 'domain name', value: 'default value', editable: true|false}` 
         * used to set default values for references fields.
         */
        defaultValues: null
    },

    publish: [
        'defaultValues'
    ],

    bind: {
        defaultValues: '{defaultValues}'
    },

    modelValidation: true,
    autoScroll: true,
    formmode: CMDBuildUI.util.helper.FormHelper.formmodes.create,
    fieldDefaults: CMDBuildUI.util.helper.FormHelper.fieldDefaults,

    bubbleEvents: [
        'itemcreated',
        'cancelcreation'
    ],

    buttons: [{
        text: CMDBuildUI.locales.Locales.common.actions.save,
        formBind: true, //only enabled once the form is valid
        disabled: true,
        reference: 'savebtn',
        itemId: 'savebtn',
        ui: 'management-action-small',
        autoEl: {
            'data-testid': 'card-create-save'
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.common.actions.save'
        }
    }, {
        text: CMDBuildUI.locales.Locales.common.actions.cancel,
        reference: 'cancelbtn',
        itemId: 'cancelbtn',
        ui: 'secondary-action-small',
        autoEl: {
            'data-testid': 'selection-popup-card-create-cancel'
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.common.actions.cancel'
        }
    }]
});
