
Ext.define('CMDBuildUI.view.classes.cards.TabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.classes-cards-tabpanel',

    requires: [
        'CMDBuildUI.view.classes.cards.TabPanelController',
        'CMDBuildUI.view.classes.cards.TabPanelModel',

        'CMDBuildUI.view.classes.cards.card.View',
        'CMDBuildUI.view.classes.cards.card.Edit'
    ],

    statics: {
        actions: {
            attachments: 'attachments',
            clone: 'clone',
            create: 'new',
            edit: 'edit',
            emails: 'emails',
            history: 'history',
            masterdetail: 'details',
            notes: 'notes',
            relations: 'relations',
            view: 'view'
        }
    },

    controller: 'classes-cards-tabpanel',
    viewModel: {
        type: 'classes-cards-tabpanel'
    },

    ui: 'management',
    border: false,
    tabPosition: 'left',
    tabRotation: 0,

    defaults: {
        textAlign: 'left',
        bodyPadding: 10,
        scrollable: true,
        border: false
    },
    layout: 'fit',

    listeners: {
        // bubbled events are not listened if declared 
        // in control property within ViewController
        itemcreated: 'onItemCreated',
        itemupdated: 'onItemUpdated',
        cancelcreation: 'onCancelCreation',
        cancelupdating: 'onCancelUpdating'

    }
});
