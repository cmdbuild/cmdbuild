Ext.define('CMDBuildUI.view.classes.cards.TabPanelModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.classes-cards-tabpanel',

    data: {
        objectType: CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
        objectTypeName: null,
        objectId: null,
        objectDescription: null,
        storeinfo: {
            autoload: false
        },
        basepermissions: {
            clone: false,
            delete: false,
            edit: false
        },
        disabled: {
            attachments: true,
            card: false,
            email: true,
            history: true,
            masterdetail: true,
            notes: true,
            relations: true
        }
    },

    formulas: {
        //update permissions to able/disable icons
        updatePermissions: {
            bind: {
                action: '{action}',
                objectTypeName: '{objectTypeName}'
            },
            get: function (data) {
                if (
                    data.action !== CMDBuildUI.view.classes.cards.TabPanel.actions.create &&
                    data.action !== CMDBuildUI.view.classes.cards.TabPanel.actions.clone
                ) {
                    this.set("disabled.attachments", false);
                    this.set("disabled.email", false);
                    this.set("disabled.history", false);
                    this.set("disabled.masterdetail", false);
                    this.set("disabled.notes", false);
                    this.set("disabled.relations", false);
                }

                var item = CMDBuildUI.util.helper.ModelHelper.getClassFromName(data.objectTypeName);
                this.set("basepermissions", {
                    clone: item.get(CMDBuildUI.model.base.Base.permissions.clone),
                    delete: item.get(CMDBuildUI.model.base.Base.permissions.delete),
                    edit: item.get(CMDBuildUI.model.base.Base.permissions.edit)
                });
            }
        },

        classDescription: {
            bind: '{objectTypeName}',
            get: function (objectTypeName) {
                // set description for parent view model
                this.getParent().set(
                    "typeDescription",
                    CMDBuildUI.util.helper.ModelHelper.getClassDescription(objectTypeName)
                );
            }
        }
    }
});
