Ext.define('CMDBuildUI.view.classes.cards.grid.GridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.classes-cards-grid-grid',

    mixins: [
        'CMDBuildUI.mixins.grids.GridControllerMixin'
    ],

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            selectedidchanged: 'onSelectedIdChanged',
            selectionchange: 'onSelectionChange',
            reload: 'onReload',
            rowdblclick: 'onRowDblClick'
        },
        tableview: {
            select: 'onSelect'
        } 
    },

    onReload: function (record, action, eOpts) {

        var currentPage;
        var view = this.getView();
        var store = view.getStore();
        var selection = view.getSelection();
        var proxy = store.getProxy();

        if (action === 'edit' || action === 'delete') {

            currentPage = Math.ceil(view.getSelectionModel().getSelection()[0].internalId / store.getConfig().pageSize);
            var selection = view.getSelectionModel().getSelection()[0];
            var index = view.store.indexOf(selection);
            view.getView().deselect(selection);


            view.suspendLayouts();
            store.load({
                params: {
                    limit: store.getConfig().pageSize,
                    page: 1,
                    start: 0
                },
                callback: function (records, operation, success) {

                    view.getView().refresh();
                    view.resumeLayouts();
                },
                scope: this
            });


        } else if (action === 'add') {
            view.suspendLayouts();
            view.getStore().loadPage(1, {
                callback: function (r, o) {
                    view.getView().refresh();


                    view.resumeLayouts();
                }
            });
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.Grid} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view) {
        var vm = this.getViewModel();
        var objectTypeName = view.getObjectTypeName();
        if (!objectTypeName) {
            objectTypeName = vm.get("objectTypeName");
        }
        CMDBuildUI.util.helper.ModelHelper.getModel("class", objectTypeName).then(function (model) {
            view.reconfigure(null, CMDBuildUI.util.helper.GridHelper.getColumns(model.getFields(), {
                allowFilter: view.getAllowFilter(),
                addTypeColumn: CMDBuildUI.util.helper.ModelHelper.getClassFromName(objectTypeName).get("prototype")
            }));
        });

    },

    /**
     * @param {Ext.selection.RowModel} element
     * @param {CMDBuildUI.model.classes.Card} record
     * @param {HTMLElement} rowIndex
     * @param {Event} e
     * @param {Object} eOpts
     */
    onRowDblClick: function (element, record, rowIndex, e, eOpts) {
        var url = Ext.String.format(
            "classes/{0}/cards/{1}/edit",
            record.getRecordType(),
            record.getRecordId()
        );

        this.redirectTo(url, true);
        return false;
    },

    /**
     * @param {Ext.selection.RowModel} selection
     * @param {CMDBuildUI.model.classes.Card[]} selected
     * @param {Object} eOpts
     */
    onSelectionChange: function (selection, selected, eOpts) {
        if (
            this.getView().isMainGrid() &&
            selected.length &&
            selected[0].getId() != this.getViewModel().get("selectedId")
        ) {
            var path = this.getRouteUrl(this.getViewModel().get("objectTypeName"), selected[0].getId());
            Ext.util.History.add(path);
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.Grid} view
     * @param {Numeric|String} newid
     * @param {Numeric|String} oldid
     */
    onSelectedIdChanged: function (view, newid, oldid) {
        var vm = this.getViewModel();

        /**
         * Expands selected row, if selection is present
         * 
         * @param {Integer} index Row index
         * @param {CMDBuildUI.store.classes.Cards} store Cards store
         */
        function expandSelection(index, store) {
            var expander = view.getPlugin("forminrowwidget");
            if (index !== -1) {
                var record = store.getById(newid);
                expander.toggleRow(index, record);
            } else {
                CMDBuildUI.util.Notifier.showWarningMessage(
                    "Card not found"
                );
            }
        }

        /**
         * 
         * @param {CMDBuildUI.store.classes.Cards} store 
         * @param {Ext.data.operation.Read} operation 
         * @param {Object} eOpts 
         */
        function onFirstBeforeLoad(store, operation, eOpts) {
            var extraparams = store.getProxy().getExtraParams();
            extraparams.positionOf = newid;
            extraparams.positionOf_goToPage = false;
        }

        /**
         * 
         * @param {CMDBuildUI.store.classes.Cards} store 
         * @param {CMDBuildUI.model.classes.Card[]} records 
         * @param {Boolean} successful 
         * @param {Ext.data.operation.Read} operation 
         * @param {Object} eOpts 
         */
        function onFirstLoad(store, records, successful, operation, eOpts) {
            var metadata = store.getProxy().getReader().metaData;
            var posinfo = metadata.positions[newid];
            if (!posinfo.pageOffset) {
                expandSelection(posinfo.positionInPage, store);
            } else {
                view.ensureVisible(posinfo.positionInTable, {
                    callback: function() {
                        expandSelection(posinfo.positionInTable, store);
                    }
                });
            }
        }

        // bind cards store to open selected card
        vm.bind({
            bindTo: '{cards}'
        }, function (cards) {
            if (!cards.isLoaded()) {
                cards.on({
                    beforeload: {
                        fn: onFirstBeforeLoad,
                        scope: this,
                        single: true
                    },
                    load: {
                        fn: onFirstLoad,
                        scope: this,
                        single: true
                    }
                });
            }
        });
    },

    /**
     * @param {Ext.selection.RowModel} row
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} eOpts
     */

    onSelect: function (row, record, index, eOpts) {
        var parent = this.getViewModel().getParent();
        parent.set('selectedRow', record);
    },

    onSearchSubmit: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // get value
        var searchTerm = vm.getData().search.value;
        if (searchTerm) {
            // add filter
            var filterCollection = vm.get("cards").getFilters();
            filterCollection.add({
                property: CMDBuildUI.proxy.BaseProxy.filter.query,
                value: searchTerm
            });
        } else {
            this.onSearchClear(field);
        }
    },

    /**
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchClear: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // clear store filter
        var filterCollection = vm.get("cards").getFilters();
        filterCollection.removeByKey(CMDBuildUI.proxy.BaseProxy.filter.query);
        // reset input
        field.reset();
    },

    /**
    * @param {Ext.form.field.Base} field
    * @param {Ext.event.Event} event
    */
    onSearchSpecialKey: function (field, event) {
        if (event.getKey() == event.ENTER) {
            this.onSearchSubmit(field);
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Object} eOpts
     */
    onAddCardButtonBeforeRender: function (button, eOpts) {
        this.updateAddButton(button, "onNewBtnClick", this.getViewModel().get("objectTypeName"));
    },

    /**
     * 
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onNewBtnClick: function (item, event, eOpts) {
        if (this.getView().isMainGrid()) {
            CMDBuildUI.util.helper.SessionHelper.setItem('activeCardIndex', 0);
            var url = 'classes/' + item.objectTypeName + '/cards/new';
            this.redirectTo(url, true);
        } else {
            this.showAddCardFormPopup(item.objectTypeName, item.text);
        }
    },

    /**
     * 
     * @param {String} objectTypeName The name of the Class
     * @param {String} targetTypeDescription The description of the class
     */
    showAddCardFormPopup: function (objectTypeName, targetTypeDescription) {
        var vm = this.getViewModel();
        var me = this;
        CMDBuildUI.util.helper.ModelHelper.getModel('class', objectTypeName).then(function (model) {
            var panel;
            var title = Ext.String.format("New {0}", targetTypeDescription);
            var config = {
                xtype: 'classes-cards-card-create',
                viewModel: {
                    data: {
                        objectTypeName: objectTypeName
                    }
                },
                defaultValues: [{
                    value: objectTypeName,
                    editable: false
                }],
                listeners: [{
                    itemcreated: function (record, eOpts) {
                        panel.destroy();
                        CMDBuildUI.util.Utilities.closePopup('popup-add-relation');
                        CMDBuildUI.util.helper.SessionHelper.setItem('activeCardIndex', 0);

                        me.redirectTo('classes/' + record.getRecordType() + '/cards/' + record.getRecordId());

                    },
                    cancelcreation: function (eOpts) {
                        panel.destroy();
                    }
                }]
            };
            panel = CMDBuildUI.util.Utilities.openPopup('popup-add-class-form', title, config, null);
        }, function () {
        });
    },

    privates: {
        /**
         * @param {String} className
         * @param {Numeric} cardId
         * @param {String} action
         * @return {String}
         */
        getRouteUrl: function (className, cardId, action) {
            var path = 'classes/' + className + '/cards';
            if (cardId) {
                path += '/' + cardId;
            }
            if (action) {
                path += '/' + action;
            }
            return path;
        }
    }

});
