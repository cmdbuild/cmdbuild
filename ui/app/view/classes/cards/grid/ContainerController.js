Ext.define('CMDBuildUI.view.classes.cards.grid.ContainerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.classes-cards-grid-container',

    control: {
        "#": {
            beforerender: "onBeforeRender"
        },
        '#addcard': {
            beforerender: 'onAddCardButtonBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.grid.Container} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var me = this;
        var vm = this.getViewModel();
        me.showContextButton(view);
        CMDBuildUI.util.helper.ModelHelper.getModel("class", view.getObjectTypeName()).then(function (model) {
            vm.set("objectTypeName", view.getObjectTypeName());
            if (vm.getParent().get('activeView') == 'grid-list') {
                view.add([{
                    itemId: me.referenceGridId,
                    reference: me.referenceGridId,
                    xtype: 'classes-cards-grid-grid',
                    maingrid: view.isMainGrid()
                }]);

            } else if (vm.getParent().get('activeView') == 'map') {
                view.add([{
                    itemId: me.referenceMapId,
                    reference: me.referenceMapId,
                    xtype: 'map-container'
                }]);
            }
        });

    },

    addMapView: function () {
        this.view.add({
            itemId: 'map-container-view',
            reference: 'map-container-view',
            xtype: 'map-container'
        });

        //this.MapView = this.view.getReferences()['map-container-view'];
        //this.onShowMapListButtonClick();
    },

    addGridView: function () {
        this.view.add([
            {
                itemId: 'classes-cards-grid-grid-view',
                reference: 'classes-cards-grid-grid-view',
                xtype: 'classes-cards-grid-grid'
            }]);

        //this.GridView = this.view.getReferences()['classes-cards-grid-grid-view'];
        //this.onShowMapListButtonClick();
    },
    /**
   * Filter grid items.
   * @param {Ext.form.field.Text} field
   * @param {Ext.form.trigger.Trigger} trigger
   * @param {Object} eOpts
   */
    onSearchSubmit: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // get value
        var searchTerm = vm.getData().search.value;
        if (searchTerm) {
            // add filter
            var filterCollection = vm.get("cards").getFilters();
            filterCollection.add({
                property: CMDBuildUI.proxy.BaseProxy.filter.query,
                value: searchTerm
            });
        } else {
            this.onSearchClear(field);
        }
    },

    /**
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchClear: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // clear store filter
        var filterCollection = vm.get("cards").getFilters();
        filterCollection.removeByKey(CMDBuildUI.proxy.BaseProxy.filter.query);
        // reset input
        field.reset();
    },

    /**
    * @param {Ext.form.field.Base} field
    * @param {Ext.event.Event} event
    */
    onSearchSpecialKey: function (field, event) {
        if (event.getKey() == event.ENTER) {
            this.onSearchSubmit(field);
        }
    },
    onShowMapListButtonClick: function (event, eOpts) { //TODO: Implement Here\
        var vm = this.getViewModel();
        var activeView = vm.get('activeView');

        /*    if (!this.lookupReference(referenceGridId) || !this.lookupReference(referenceMapId)) {    //workaround for bug
               this.settingUp();
           } */

        if (activeView == 'grid-list') {
            if (!this.lookupReference(this.referenceMapId)) {
                this.addMapView();
            }
            this.lookupReference(this.referenceGridId).hide();
            this.lookupReference(this.referenceMapId).show();
            vm.getParent().set('activeView', 'map');

        } else if (activeView == 'map') {
            if (!this.lookupReference(this.referenceGridId)) {
                this.addGridView();
            }
            this.lookupReference(this.referenceMapId).hide();
            this.lookupReference(this.referenceGridId).show();
            vm.getParent().set('activeView', 'grid-list');
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Object} eOpts
     */
    onAddCardButtonBeforeRender: function (button, eOpts) {
        var vm = this.getViewModel();
        var objectTypeName = this.getView().getObjectTypeName();
        var klass = Ext.getStore('classes.Classes').getById(objectTypeName);

        if (klass.get("prototype")) {
            var menu = [];
            var childrens = klass.getChildren();

            // create menu definition by adding non-prototype classes
            for (var i = 0; i < childrens.length; i++) {
                var cklass = childrens[i];
                if (!childrens[i].get('prototype')) {
                    menu.push({
                        text: cklass.get("description"),
                        iconCls: 'x-fa fa-file-text-o',
                        listeners: {
                            click: 'onNewBtnClick'
                        },
                        objectTypeName: cklass.get("name")
                    });
                }
            }
            // sort menu by description
            Ext.Array.sort(menu, function (a, b) {
                return a.text === b.text ? 0 : (a.text < b.text ? -1 : 1);
            });

            // add menu to button
            button.setMenu(menu);
        } else {
            button.objectTypeName = objectTypeName;
            button.addListener("click", this.onNewBtnClick, this);
        }
    },

    /**
     * 
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onNewBtnClick: function (item, event, eOpts) {
        if (this.getView().isMainGrid()) {
            CMDBuildUI.util.helper.SessionHelper.setItem('activeCardIndex', 0);
            var url = 'classes/' + item.objectTypeName + '/cards/new';
            this.redirectTo(url, true);
        } else {
            this.showAddCardFormPopup(item.objectTypeName, item.text);
        }
    },

    /**
     * 
     * @param {String} objectTypeName The name of the Class
     * @param {String} targetTypeDescription The description of the class
     */
    showAddCardFormPopup: function (objectTypeName, targetTypeDescription) {
        var me = this;
        CMDBuildUI.util.helper.ModelHelper.getModel('class', objectTypeName).then(function (model) {
            var panel;
            var title = Ext.String.format("New {0}", targetTypeDescription);
            var config = {
                xtype: 'classes-cards-card-create',
                viewModel: {
                    data: {
                        objectTypeName: objectTypeName
                    }
                },
                defaultValues: [{
                    value: objectTypeName,
                    editable: false
                }],
                listeners: [{
                    itemcreated: function (record, eOpts) {
                        panel.destroy();
                        CMDBuildUI.util.Utilities.closePopup('popup-add-relation');
                        CMDBuildUI.util.helper.SessionHelper.setItem('activeCardIndex', 0);
                        console.log(['the new record is', record]);

                        me.redirectTo('classes/' + record.getRecordType() + '/cards/' + record.getRecordId());

                    },
                    cancelcreation: function (eOpts) {
                        panel.destroy();
                    }
                }]
            };
            panel = CMDBuildUI.util.Utilities.openPopup('popup-add-class-form', title, config, null);
        }, function () {
            Ext.Msg.alert('Error', 'Class non found!');
        });
    },

    privates: {
        /**
         * @property referenceGridId
         */
        referenceGridId: 'classes-cards-grid-grid-view',

        /**
         * @property referenceMapId
         */
        referenceMapId: 'map-container-view',

        /**
         * show context menu
         * @param {CMDBuildUI.view.classes.cards.grid.Container} view
         */
        showContextButton: function (view) {
            var menu = [];
            var objectTypeName = view.getObjectTypeName();
            var currentKlass = CMDBuildUI.util.helper.ModelHelper.getClassFromName(objectTypeName);
            var contextmenuitems = currentKlass.contextMenuItems().getRange();
            var contextButton = view.lookupReference('cardsMenuButton');
            if (contextmenuitems.length > 0) {
                contextmenuitems.forEach(function (item) {
                    var name = item.get('label');
                    var type = item.get('type');

                    switch (type) {
                        case 'separator':
                            menu.push({
                                xtype: 'menuseparator'
                            });
                            break;
                        case 'custom':
                        case 'component':
                            menu.push({
                                iconCls: 'x-fa fa-angle-double-right',
                                text: name
                            });
                            break;
                    }
                });
                contextButton.setHidden(false);
                contextButton.setMenu(menu);
            }
        }
    }

});
