Ext.define('CMDBuildUI.view.classes.cards.grid.GridModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.classes-cards-grid-grid',

    data: {
        selection: null,
        search: {
            value: null
        }
    },

    formulas: {
        //TODO: should fire an event to make the rowwidget toggle
        /* setSelectedRow: {
            bind: '{selectedRow}',
            get: function(selectionObj){
                //this.getView().setSelection(selectionObj.record);
                this.getView().getView().fireEvent('itemclickmappanel',this, selectionObj.index, selectionObj.record);    
            }
        }, */
        canAdd: function (get) {
            // TODO: check permissions
            return true;
        },
        canFilter: function (get) {
            return this.getView().getAllowFilter();
        },
        newButtonHidden: function () {
            return !this.getView().getShowAddButton();
        }
    }

});
