
Ext.define('CMDBuildUI.view.classes.cards.grid.Container', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.classes.cards.grid.ContainerController',
        'CMDBuildUI.view.classes.cards.grid.ContainerModel'
    ],

    alias: 'widget.classes-cards-grid-container',
    controller: 'classes-cards-grid-container',
    viewModel: {
        type: 'classes-cards-grid-container'
    },
    title: 'cards',

    config: {
        /**
         * @cfg {Boolean} maingrid
         * 
         * Set to true when the grid is added in main content.
         */
        maingrid: false,

        /**
         * @cfg {String} objectTypeName
         * Class name.
         */
        objectTypeName: null,

        /**
         * @cfg {Object} filter
         * Advanced filter definition.
         */
        filter: null
    },

    autoEl: {
        'data-testid': 'cards-card-view-container'
    },

    layout: 'fit',

    bind: {
        title: 'Cards {objectTypeDescription}'
    },

    tbar: [{
        xtype: 'button',
        text: CMDBuildUI.locales.Locales.classes.cards.addcard,
        reference: 'addcard',
        itemId: 'addcard',
        iconCls: 'x-fa fa-plus',
        ui: 'management-action',
        autoEl: {
            'data-testid': 'classes-cards-grid-container-addbtn'
        },
        bind: {
            text: '{addbtn.text}',
            disabled: '{addbtn.disabled}'
        }
    }, {
        xtype: 'textfield',
        name: 'search',
        width: 250,
        emptyText: 'Search...',
        reference: 'searchtext',
        itemId: 'searchtext',
        autoEl: {
            'data-testid': 'classes-cards-grid-container-searchtext'
        },
        bind: {
            value: '{search.value}',
            hidden: '{!canFilter}'
        },
        listeners: {
            specialkey: 'onSearchSpecialKey'
        },
        triggers: {
            search: {
                cls: Ext.baseCSSPrefix + 'form-search-trigger',
                handler: 'onSearchSubmit'
            },
            clear: {
                cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                handler: 'onSearchClear'
            }
        }
    }, {
        xtype: 'filters-launcher',
        storeName: 'cards'
    }, {
        xtype: 'button',
        itemId: 'cardsMenuButton',
        reference: 'cardsMenuButton',
        iconCls: 'x-fa fa-bars',
        arrowVisible: false,
        hidden: true
    }, {
        xtype: 'button',
        reference: 'showMapListButton',
        itemId: 'showMapListButton',
        iconCls: 'x-fa fa-globe',
        hidden: true,
        ui: 'management-action',
        bind: {
            text: '{btnMapText}',
            iconCls: '{btnIconCls}',
            hidden: '{btnMapHidden}'
        },
        listeners: {
            click: 'onShowMapListButtonClick'
        }

    }, {
        xtype: 'tbfill'
    }, {
        xtype: 'bufferedgridcounter',
        padding: '0 20 0 0',
        bind: {
            store: '{cards}'
        }
    }],

    /**
     * Return true if the grid has been added in main container.
     * @return {Boolean}
     */
    isMainGrid: function () {
        return this.maingrid;
    }
});
