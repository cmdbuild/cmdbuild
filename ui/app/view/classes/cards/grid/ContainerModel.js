Ext.define('CMDBuildUI.view.classes.cards.grid.ContainerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.classes-cards-grid-container',

    data: {
        objectType: CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
        objectTypeName: null,
        selectedRow: null,
        addbtn: {},
        storeinfo: {
            autoLoad: false
        }
    },

    formulas: {
        updateData: {
            bind: {
                objectTypeName: '{objectTypeName}'
            },
            get: function (data) {
                if (data.objectTypeName) {
                    // class description
                    var desc = CMDBuildUI.util.helper.ModelHelper.getClassDescription(data.objectTypeName);
                    this.set("objectTypeDescription", desc);

                    // model name
                    var modelName = CMDBuildUI.util.helper.ModelHelper.getModelName(
                        CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
                        data.objectTypeName
                    );
                    this.set("storeinfo.modelname", modelName);

                    var model = Ext.ClassManager.get(modelName);
                    this.set("storeinfo.proxytype", model.getProxy().type);
                    this.set("storeinfo.url", model.getProxy().getUrl());

                    var params = {};
                    if (this.getView().getFilter()) {
                        params.filter = this.getView().getFilter();
                    }
                    this.set("storeinfo.extraparams", params);

                    // sorters
                    var klass = CMDBuildUI.util.helper.ModelHelper.getClassFromName(data.objectTypeName);
                    var sorters = [];
                    if (klass && klass.defaultOrder().getCount()) {
                        klass.defaultOrder().getRange().forEach(function (o) {
                            sorters.push({
                                property: o.get("attribute"),
                                direction: o.get("direction") === "descending" ? "DESC" : 'ASC'
                            });
                        });
                    } else {
                        sorters.push({
                            property: 'Description'
                        });
                    }
                    this.set("storeinfo.sorters", sorters);

                    // auto load
                    this.set("storeinfo.autoload", true);

                    // add button
                    this.set("addbtn.text", CMDBuildUI.locales.Locales.classes.cards.addcard + ' ' + desc);
                    this.set("addbtn.disabled", !klass.get(CMDBuildUI.model.base.Base.permissions.add));
                }
            }
        },
        btnMapHidden: function () {
            return !CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.gis.enabled);
        },
        btnMapText: {
            bind: '{activeView}',
            get: function (activeView) {
                if (activeView === "map") {
                    return "List";
                } else {
                    return "Map";
                }
            }
        },

        btnIconCls: {
            bind: '{activeView}',
            get: function (activeView) {
                if (activeView === "map") {
                    return 'x-fa fa-list';
                } else {
                    return 'x-fa fa-globe';
                }
            }
        }
    },

    stores: {
        cards: {
            type: 'classes-cards',
            model: '{storeinfo.modelname}',
            sorters: '{storeinfo.sorters}',
            autoLoad: '{storeinfo.autoload}',
            proxy: {
                type: '{storeinfo.proxytype}',
                url: '{storeinfo.url}',
                extraParams: '{storeinfo.extraparams}'
            }
        }
    }

});
