Ext.define('CMDBuildUI.view.administration.content.users.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-users-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function(view){
        var vm = this.getViewModel();
        vm.set('title', 'Users');
    }
});
