Ext.define('CMDBuildUI.view.administration.content.users.card.Create', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.users.card.CreateController',
        'CMDBuildUI.view.administration.content.users.card.EditModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],

    alias: 'widget.administration-content-users-card-create',
    controller: 'view-administration-content-users-card-create',
    viewModel: {
        type: 'view-administration-content-users-card-edit'
    },

    config: {
        objectTypeName: null,
        /**
         * @cfg {Object[]}
         * 
         * Can set default values for any of the attributes. An object can be:
         * `{attribute: 'attribute name', value: 'default value', editable: true|false}` 
         * used for all attributes or
         * `{domain: 'domain name', value: 'default value', editable: true|false}` 
         * used to set default values for references fields.
         */
        defaultValues: null
    },
    bind: {
        data: {
            theUser: '{theUser}'
        }

    },
    modelValidation: true,
    autoScroll: true,
    fieldDefaults: {
        labelAlign: 'top'
    },
    bubbleEvents: [
        'itemcreated',
        'cancelcreation'
    ],
    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "General properties", // TODO: translate
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Username',
                name: 'username',
                vtype: 'alphanum',
                enforceMaxLength: true,
                allowBlank: false,
                maxLength: 40,
                bind: {
                    value: '{theUser.username}'
                }
            }, {
                xtype: 'textfield',
                fieldLabel: 'Description',
                name: 'description',
                enforceMaxLength: true,
                maxLength: 40,
                bind: {
                    value: '{theUser.description}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Email',
                name: 'email',
                enforceMaxLength: true,
                maxLength: 320,
                bind: {
                    value: '{theUser.email}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'combo',
                fieldLabel: 'Language',
                labelAlign: 'top',
                name: 'language',
                valueField: 'code',
                displayField: 'description',
                queryMode: 'local',
                typeAhead: true,
                bind: {
                    store: '{languagesStore}',
                    value: '{theUser.language}'
                }
            }, {
                xtype: 'combo',
                fieldLabel: 'Initial page',
                name: 'initialPage',
                valueField: '_id',
                displayField: 'label',
                queryMode: 'local',
                typeAhead: true,
                bind: {
                    value: '{theUser.initialPage}',
                    store: '{getAllPagesStore}'
                },
                triggers: {
                    foo: {
                        cls: 'x-form-clear-trigger',
                        handler: function () {
                            this.clearValue();
                        }
                    }
                },
                tpl: new Ext.XTemplate(
                    '<tpl for=".">',
                    '<tpl for="group" if="this.shouldShowHeader(group)"><div class="group-header">{[this.showHeader(values.group)]}</div></tpl>',
                    '<div class="x-boundlist-item">{label}</div>',
                    '</tpl>', {
                        shouldShowHeader: function (group) {
                            return this.currentGroup !== group;
                        },
                        showHeader: function (group) {
                            this.currentGroup = group;
                            return group;
                        }
                    })
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Service',
                name: 'service',
                bind: {
                    value: '{theUser.service}'
                }
            }, {
                xtype: 'checkbox',
                fieldLabel: 'Privileged',
                name: 'privileged',
                bind: {
                    value: '{theUser.privileged}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                bind: {
                    value: '{theUser.active}'
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Password", // TODO: translate
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                // TODO: Need validation after ws creation
                xtype: 'textfield',
                fieldLabel: 'Password',
                name: 'password',
                inputType: 'password',
                reference: 'password',
                vtype: 'passwordValidation',
                enforceMaxLength: true,
                maxLength: 40,
                triggers: {
                    showPassword: {
                        cls: 'x-fa fa-eye',
                        hideTrigger: false,
                        scope: this,
                        handler: function (field, button, e) {
                            field.lookupReferenceHolder().lookupReference('password').inputEl.el.set({
                                type: 'text'
                            });
                            field.getTrigger('showPassword').setVisible(false);
                            field.getTrigger('hidePassword').setVisible(true);
                        }
                    },
                    hidePassword: {
                        cls: 'x-fa fa-eye-slash',
                        hidden: true,
                        scope: this,
                        handler: function (field, button, e) {
                            // set the element type to text
                            field.lookupReferenceHolder().lookupReference('password').inputEl.el.set({
                                type: 'password'
                            });
                            field.getTrigger('showPassword').setVisible(true);
                            field.getTrigger('hidePassword').setVisible(false);
                        }
                    }
                },
                bind: {
                    value: '{theUser.password}'
                }
            }, {
                xtype: 'textfield',
                fieldLabel: 'Confirm Password',
                name: 'confirmPassword',
                inputType: 'password',
                reference: 'confirmPasswordField',
                vtype: 'passwordMatch',
                enforceMaxLength: true,
                maxLength: 40,
                triggers: {
                    showPassword: {
                        cls: 'x-fa fa-eye',
                        hideTrigger: false,
                        scope: this,
                        handler: function (field, button, e) {
                            // set the element type to text
                            field.lookupReferenceHolder().lookupReference('confirmPasswordField').inputEl.el.set({
                                type: 'text'
                            });
                            field.getTrigger('showPassword').setVisible(false);
                            field.getTrigger('hidePassword').setVisible(true);
                        }
                    },
                    hidePassword: {
                        cls: 'x-fa fa-eye-slash',
                        hidden: true,
                        scope: this,
                        handler: function (field, button, e) {
                            // set the element type to password
                            field.lookupReferenceHolder().lookupReference('confirmPasswordField').inputEl.el.set({
                                type: 'password'
                            });
                            field.getTrigger('showPassword').setVisible(true);
                            field.getTrigger('hidePassword').setVisible(false);
                        }
                    }
                },
                bind: {
                    value: '{theUser.confirmPassword}'
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Groups", // TODO: translate
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'combo',
                labelAlign: 'top',
                flex: '0.5',

                layout: 'anchor',
                fieldLabel: 'Deafult group',
                displayField: 'description',
                valueField: '_id',
                bind: {
                    store: '{getSelectedGroups}',
                    value: '{theUser.defaultUserGroup}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                labelAlign: 'top',
                flex: '0.5',

                layout: 'anchor',
                fieldLabel: 'Multigroup',
                bind: {
                    value: '{theUser.multiGroup}'
                }
            }]
        }, {
            xtype: 'grid',
            bind: {
                store: '{rolesStore}'
            },
            sortable: false,
            sealedColumns: false,
            sortableColumns: false,
            enableColumnHide: false,
            enableColumnMove: false,
            enableColumnResize: false,
            menuDisabled: true,
            stopSelect: true,
            columns: [{
                text: 'Group',
                dataIndex: 'description',
                flex: 1,
                align: 'left'
            }, {
                text: 'active',
                xtype: 'checkcolumn',
                dataIndex: 'active',
                align: 'center',
                listeners: {
                    checkchange: function (check, rowIndex, checked, record, e, eOpts) {
                        var vm = check.up('form').getViewModel();
                        var currentGroups = vm.get('theUser.userGroups');

                        switch (checked) {
                            case true:
                                currentGroups.push({
                                    id:record.get('_id'),
                                    _id: record.get('_id'),
                                    name: record.get('name'),
                                    description: record.get('description')
                                });
                                break;
                            case false:
                                var index = currentGroups.map(function (group) {
                                    return group.get('_id');
                                }).indexOf(record.get('_id'));
                                currentGroups.splice(index, 1);
                                break;
                        }
                        vm.set('userGroups', currentGroups);

                    }
                }
            }]
        }]
    }

],

    buttons: [{
        text: 'Save',
        formBind: true,
        disabled: true,
        ui: 'administration-action-small',
        listeners: {
            click: 'onSaveBtnClick'
        }
    }, {
        text: 'Save and Add',
        formBind: true,
        disabled: true,
        ui: 'administration-action-small',
        listeners: {
            click: 'onSaveAndAddBtnClick'
        }
    }, {
        text: 'Cancel',
        ui: 'administration-secondary-action-small',
        listeners: {
            click: 'onCancelBtnClick'
        }
    }]
});