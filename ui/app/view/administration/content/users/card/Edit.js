Ext.define('CMDBuildUI.view.administration.content.users.card.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.administration-content-users-card-edit',

    requires: [
        'CMDBuildUI.view.administration.content.users.card.EditController',
        'CMDBuildUI.view.administration.content.users.card.EditModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],
    controller: 'view-administration-content-users-card-edit',
    viewModel: {
        type: 'view-administration-content-users-card-edit'
    },
    bubbleEvents: [
        'itemupdated',
        'cancelupdating'
    ],
    modelValidation: true,
    config: {
        theUser: null
    },
    bind: {
        theUser: '{theUser}'
    },
    twoWayBindable: {
        theUser: '{theUser}'
    },
    fieldDefaults: {
        labelAlign: 'top',
        msgTarget: 'under'
    },
    scrollable: true,
    ui: 'administration-formpagination',
    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "General properties", // TODO: translate
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Username',
                name: 'username',
                vtype: 'alphanum',
                enforceMaxLength: true,
                maxLength: 40,
                bind: {
                    value: '{theUser.username}'
                }
            }, {
                xtype: 'textfield',
                fieldLabel: 'Description',
                name: 'description',
                enforceMaxLength: true,
                maxLength: 40,
                bind: {
                    value: '{theUser.description}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Email',
                name: 'email',
                enforceMaxLength: true,
                maxLength: 320,
                bind: {
                    value: '{theUser.email}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'combo',
                fieldLabel: 'Language',
                labelAlign: 'top',
                name: 'language',
                valueField: 'code',
                displayField: 'description',
                queryMode: 'local',
                typeAhead: true,
                bind: {
                    store: '{languagesStore}',
                    value: '{theUser.language}'
                }
            }, {
                xtype: 'combo',
                fieldLabel: 'Initial page',
                name: 'initialPage',
                valueField: '_id',
                displayField: 'label',
                queryMode: 'local',
                typeAhead: true,
                bind: {
                    value: '{theUser.initialPage}',
                    store: '{getAllPagesStore}'
                },
                triggers: {
                    foo: {
                        cls: 'x-form-clear-trigger',
                        handler: function () {
                            this.clearValue();
                        }
                    }
                },
                tpl: new Ext.XTemplate(
                    '<tpl for=".">',
                    '<tpl for="group" if="this.shouldShowHeader(group)"><div class="group-header">{[this.showHeader(values.group)]}</div></tpl>',
                    '<div class="x-boundlist-item">{label}</div>',
                    '</tpl>', {
                        shouldShowHeader: function (group) {
                            return this.currentGroup !== group;
                        },
                        showHeader: function (group) {
                            this.currentGroup = group;
                            return group;
                        }
                    })
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Service',
                name: 'service',
                bind: {
                    value: '{theUser.service}'
                }
            }, {
                xtype: 'checkbox',
                fieldLabel: 'Privileged',
                name: 'privileged',
                bind: {
                    value: '{theUser.privileged}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                bind: {
                    value: '{theUser.active}'
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Password", // TODO: translate
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                // TODO: Need validation after ws creation
                xtype: 'textfield',
                fieldLabel: 'Password',
                name: 'password',
                inputType: 'password',
                reference: 'password',
                vtype: 'passwordValidation',
                enforceMaxLength: true,
                maxLength: 40,
                triggers: {
                    showPassword: {
                        cls: 'x-fa fa-eye',
                        hideTrigger: false,
                        scope: this,
                        handler: function (field, button, e) {
                            field.lookupReferenceHolder().lookupReference('password').inputEl.el.set({
                                type: 'text'
                            });
                            field.getTrigger('showPassword').setVisible(false);
                            field.getTrigger('hidePassword').setVisible(true);
                        }
                    },
                    hidePassword: {
                        cls: 'x-fa fa-eye-slash',
                        hidden: true,
                        scope: this,
                        handler: function (field, button, e) {
                            // set the element type to text
                            field.lookupReferenceHolder().lookupReference('password').inputEl.el.set({
                                type: 'password'
                            });
                            field.getTrigger('showPassword').setVisible(true);
                            field.getTrigger('hidePassword').setVisible(false);
                        }
                    }
                },
                bind: {
                    value: '{theUser.password}'
                }
            }, {
                xtype: 'textfield',
                fieldLabel: 'Confirm Password',
                name: 'confirmPassword',
                inputType: 'password',
                reference: 'confirmPasswordField',
                vtype: 'passwordMatch',
                enforceMaxLength: true,
                maxLength: 40,
                triggers: {
                    showPassword: {
                        cls: 'x-fa fa-eye',
                        hideTrigger: false,
                        scope: this,
                        handler: function (field, button, e) {
                            // set the element type to text
                            field.lookupReferenceHolder().lookupReference('confirmPasswordField').inputEl.el.set({
                                type: 'text'
                            });
                            field.getTrigger('showPassword').setVisible(false);
                            field.getTrigger('hidePassword').setVisible(true);
                        }
                    },
                    hidePassword: {
                        cls: 'x-fa fa-eye-slash',
                        hidden: true,
                        scope: this,
                        handler: function (field, button, e) {
                            // set the element type to password
                            field.lookupReferenceHolder().lookupReference('confirmPasswordField').inputEl.el.set({
                                type: 'password'
                            });
                            field.getTrigger('showPassword').setVisible(true);
                            field.getTrigger('hidePassword').setVisible(false);
                        }
                    }
                },
                bind: {
                    value: '{theUser.confirmPassword}'
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Groups", // TODO: translate
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'combo',
                labelAlign: 'top',
                flex: '0.5',

                layout: 'anchor',
                fieldLabel: 'Deafult group',
                displayField: 'description',
                valueField: '_id',
                bind: {
                    store: '{getSelectedGroups}',
                    value: '{theUser.defaultUserGroup}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                labelAlign: 'top',
                flex: '0.5',

                layout: 'anchor',
                fieldLabel: 'Multigroup',
                bind: {
                    value: '{theUser.multiGroup}'
                }
            }]
        }, {
            xtype: 'grid',
            bind: {
                store: '{rolesStore}'
            },
            sortable: false,
            sealedColumns: false,
            sortableColumns: false,
            enableColumnHide: false,
            enableColumnMove: false,
            enableColumnResize: false,
            menuDisabled: true,
            stopSelect: true,
            columns: [{
                text: 'Group',
                dataIndex: 'description',
                flex: 1,
                align: 'left'
            }, {
                text: 'active',
                xtype: 'checkcolumn',
                dataIndex: 'active',
                align: 'center',
                listeners: {
                    checkchange: function (check, rowIndex, checked, record, e, eOpts) {
                        var vm = check.up('form').getViewModel();
                        var currentGroups = vm.get('theUser.userGroups');

                        switch (checked) {
                            case true:
                                currentGroups.push({
                                    _id: record.get('_id'),
                                    name: record.get('name'),
                                    description: record.get('description')
                                });
                                break;
                            case false:
                                var index = currentGroups.map(function (group) {
                                    return group.get('_id');
                                }).indexOf(record.get('_id'));
                                currentGroups.splice(index, 1);
                                break;
                        }
                        vm.set('userGroups', currentGroups);

                    }
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Tenants", // TODO: translate
        items: [
            /**
             * currently not supported by server
             */
            // {
            //     layout: 'column',
            //     defaults: {
            //         columnWidth: 0.5
            //     },
            //     items: [{
            //         xtype: 'combo',
            //         labelAlign: 'top',
            //         flex: '0.5',

            //         layout: 'anchor',
            //         fieldLabel: 'Deafult tenant',
            //         displayField: 'description',
            //         valueField: '_id',
            //         bind: {
            //             store: '{getSelectedTenants}',
            //             value: '{theUser.defaultTenant}'
            //         }
            //     }]
            // }, 

            /**
             * currently not supported by server
             */
            // {
            //     layout: 'column',
            //     defaults: {
            //         columnWidth: 0.5
            //     },
            //     items: [{
            //         xtype: 'checkbox',
            //         labelAlign: 'top',
            //         flex: '0.5',

            //         layout: 'anchor',
            //         fieldLabel: 'Multitenant',
            //         bind: {
            //             value: '{theUser.multitenant}'
            //         }
            //     }]

            // },
             {
                layout: 'column',
                defaults: {
                    columnWidth: 0.5
                },
                items: [{
                    xtype: 'displayfield',
                    fieldLabel: 'Tenants',
                    bind: {
                        value: '{tenantsHtml}'
                    }
                }]

            },
            /**
             * currently is possible to edit tenants relation only in management
             */
            // {
            //     xtype: 'grid',
            //     bind: {
            //         store: '{tenantsStore}'
            //     },
            //     sortable: false,
            //     sealedColumns: false,
            //     sortableColumns: false,
            //     enableColumnHide: false,
            //     enableColumnMove: false,
            //     enableColumnResize: false,
            //     menuDisabled: true,
            //     stopSelect: true,
            //     columns: [{
            //         text: 'Tenant',
            //         dataIndex: 'description',
            //         flex: 1,
            //         align: 'left'
            //     }, {
            //         text: 'active',
            //         xtype: 'checkcolumn',
            //         dataIndex: 'active',
            //         align: 'center',
            //         listeners: {
            //             checkchange: function (check, rowIndex, checked, record, e, eOpts) {
            //                 var vm = check.up('form').getViewModel();
            //                 var currentTenants = vm.get('theUser.userTenants');
            //                 switch (checked) {
            //                     case true:
            //                     currentTenants.push({
            //                             _id: record.get('_id'),
            //                             name: record.get('name'),
            //                             description: record.get('description'),
            //                             active: true
            //                         });
            //                         break;
            //                     case false:
            //                         var index = currentTenants.map(function (group) {
            //                             return group.get('_id');
            //                         }).indexOf(record.get('_id'));
            //                         currentTenants.splice(index, 1);
            //                         break;
            //                 }

            //                 vm.set('userTenants', currentTenants);

            //             }
            //         }
            //     }]
            // }

        ]
    }],

    buttons: [{
        text: 'Save',
        ui: 'administration-action-small',
        // TODO: formBind not work as expected

        //formBind: true,//only enabled once the form is valid
        //disabled: true,
        listeners: {
            click: 'onSaveBtnClick'
        }
    }, {
        text: 'Cancel',
        ui: 'administration-secondary-action-small',
        listeners: {
            click: 'onCancelBtnClick'
        }
    }]
});