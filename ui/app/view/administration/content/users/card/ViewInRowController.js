Ext.define('CMDBuildUI.view.administration.content.users.card.ViewInRowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-users-card-viewinrow',
    listen: {
        global: {
            selecteduser: 'onUserUpdated',
            userupdated: 'onUserUpdated'
        }
    },
    control: {
        '#': {
            beforerender: 'onBeforeRender',
            itemupdated: 'onUserUpdated',
            tabchange: 'onTabChage'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.classes.tabitems.users.card.ViewInRow} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        this.linkUser(view, vm);
    },

    onUserUpdated: function (v, record) {
        var vm = this.getViewModel();
        var view = this.getView();
        this.linkUser(view, vm);
    },

    onEditBtnClick: function () {
        var view = this.getView();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-users-card-edit',
            viewModel: {
                data: {
                    theUser: view.getViewModel().get('selected'),
                    title: 'Users' + ' - ' + view.getViewModel().get('theUser').get('name'),
                    grid: this.getView().up()
                }
            }
        });
    },

    onDeleteBtnClick: function () {
        var me = this;
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-user');
                    me.getViewModel().getData().theUser.erase({
                        success: function (record, operation) {
                            Ext.ComponentQuery.query('administration-content-users-grid')[0].fireEventArgs('reload', [record, 'delete']);
                        }
                    });
                }
            }, this);
    },


    onOpenBtnClick: function () {
        var view = this.getView();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-users-card-view',

            viewModel: {
                data: {
                    theUser: view.getViewModel().get('selected'),
                    title: 'Users' + ' - ' + view.getViewModel().get('theUser').get('username'),
                    grid: this.getView().up()
                }
            }
        });
    },

    linkUser: function (view, vm) {
        var record = view.up().getSelection()[0];
        // var groupsData = [];

        // var groupsHtml = '<ul>';
        // if(record){
        //     record.get('userGroups').forEach(function(group){
        //         groupsData.push(group.description);
        //         groupsHtml += '<li>'+group.description+'</li>';
        //     });
        // }
        // groupsHtml += '</ul>';
        // vm.set('groupsHtml',groupsHtml);

        // var tenantsHtml = '<ul>';
        // if(record){
        //     record.get('userTenants').forEach(function(tenant){
        //         tenantsHtml += '<li>'+tenant.description+'</li>';
        //     });
        // }
        // tenantsHtml += '</ul>';

        // vm.set('groupsData', groupsData);
        // vm.set('tenantsHtml',tenantsHtml);
        vm.set("theUser", record);
    },

    /**
     * 
     * @param {Ext.tab.Panel} tabPanel 
     * @param {Ext.Component} newCard 
     * @param {Ext.Component} oldCard 
     * @param {Object} eOpts 
     */
    onTabChage: function(tabPanel, newCard, oldCard, eOpts){
        // if(newCard.reference === 'groupsTab'){
        //     var dlength = newCard.up('administration-content-users-grid').getViewModel().get('selected.userGroups').length;
        //     var ml = newCard.down().down().items.items[2].getHeight() + 40 + (dlength * 20);
        //     newCard.down().down().items.items[2].setHeight(ml);
        //     tabPanel.updateLayout();

        // }
    }
});
