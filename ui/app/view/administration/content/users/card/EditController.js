Ext.define('CMDBuildUI.view.administration.content.users.card.EditController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-users-card-edit',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.users.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        if (vm.get('grid').getSelection().length) {
            vm.linkTo("theUser", {
                type: 'CMDBuildUI.model.users.User',
                id: vm.get('grid').getSelection()[0].get('_id')
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var form = this.getView();
        var theUser = this.getViewModel().get('theUser');
        if(theUser){
            Ext.Array.forEach(theUser.get('userGroups'),function(element, index){
                delete theUser.data.userGroups[index].id;
            });
            Ext.Array.forEach(theUser.get('userTenants'),function(element, index){
                delete theUser.data.userTenants[index].id;
            });
        }
        theUser.save({
            success: function (record, operation) {
                Ext.GlobalEvents.fireEventArgs("userupdated", [record]);
                form.up().fireEvent("closed");
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        vm.get("theUser").reject(); // discard changes
        this.getView().up().fireEvent("closed");
    },

    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var theValue = vm.get('theUser');

        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('lookup.{0}.description', theValue.get('_type'))
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-classuser-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-classuser-localization',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px'
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() + 20);
    }

});