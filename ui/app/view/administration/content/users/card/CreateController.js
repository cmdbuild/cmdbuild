Ext.define('CMDBuildUI.view.administration.content.users.card.CreateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-users-card-create',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
    * @param {CMDBuildUI.view.administration.content.classes.tabitems.users.card.EditController} view
    * @param {Object} eOpts
    */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        // set vm varibles
        vm.linkTo("theUser", {
            type: 'CMDBuildUI.model.users.User',
            create: true
        });
    },

    /**
    * @param {Ext.button.Button} button
    * @param {Event} e
    * @param {Object} eOpts
    */
    onSaveBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        var form = this.getView();

        if (form.isValid()) {
            var theUser = vm.getData().theUser;
            theUser.getProxy().setUrl(Ext.String.format('/users'));
            delete theUser.data.inherited;
            delete theUser.data.writable;
            delete theUser.data.hidden;
            
            theUser.save({
                success: function (record, operation) {
                    var w = Ext.create('Ext.window.Toast', {
                        ui: 'administration',
                        title: 'Success!',
                        html: 'User created saved correctly.',
                        iconCls: 'x-fa fa-check-circle',
                        align: 'br'
                    });
                    w.show();
                    Ext.GlobalEvents.fireEventArgs("usercreated", [record]);
                    //form.up().fireEventArgs("usercreated", [vm.get('className'), record.get('name')]);
                    
                    var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
                    container.fireEvent('closed');
                }
            });
            } else {
            var w = Ext.create('Ext.window.Toast', {
                ui: 'administration',
                html: 'Please correct indicted errors',
                title: 'Error!',
                iconCls: 'x-fa fa-check-circle',
                align: 'br'
            });
            w.show();
        }
    },

    /**
    * @param {Ext.button.Button} button
    * @param {Event} e
    * @param {Object} eOpts
    */
    onSaveAndAddBtnClick: function (button, e, eOpts) {
        this.onSaveBtnClick();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-users-card-create',
            viewModel: {
                links:{
                    theUser: {
                        type: 'CMDBuildUI.model.users.User',
                        create: true
                    }
                }
            }
        });
    },
    /**
    * @param {Ext.button.Button} button
    * @param {Event} e
    * @param {Object} eOpts
    */
    onCancelBtnClick: function (button, e, eOpts) {
        this.getViewModel().get("theUser").reject();
        this.getView().up().fireEvent("closed");
    }
});
