Ext.define('CMDBuildUI.view.administration.content.users.card.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-users-card-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.users.card.View} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        if (vm.get('grid').getSelection().length) {
            var record = vm.get('grid').getSelection()[0];
            // var groupsData = [];
    
            // var groupsHtml = '<ul>';
            // if(record){
            //     record.get('userGroups').forEach(function(group){
            //         groupsData.push(group.description);
            //         groupsHtml += '<li>'+group.description+'</li>';
            //     });
            // }
            // groupsHtml += '</ul>';
            // vm.set('groupsHtml',groupsHtml);
    
            // var tenantsHtml = '<ul>';
            // if(record){
            //     record.get('userTenants').forEach(function(tenant){
            //         tenantsHtml += '<li>'+tenant.description+'</li>';
            //     });
            // }
            // tenantsHtml += '</ul>';
    
            // vm.set('groupsData', groupsData);
            // vm.set('tenantsHtml',tenantsHtml);
            vm.set("theUser", record);
        }
    },

    onEditBtnClick: function () {
        var view = this.getView();
        var url = 'administration/classes/' + view.getObjectTypeName() + '/cards/' + view.getObjectId() + '/edit';
        this.redirectTo(url, true);
    },
    onDeleteBtnClick: function () {
        var view = this.getView();
        var store, rowIndex;
        var vm = this.getViewModel();
        if (this.view._rowContext) {
            store = this.view._rowContext.record.store;
            rowIndex = this.view._rowContext.recordIndex;
        }

        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-user');

                    var theObject = vm.getData().theObject;
                    theObject.erase({
                        failure: function (record, operation) {
                            Ext.Msg.alert(
                                'Error', //TODO:translate
                                Ext.JSON.decode(operation.error.response.responseText, true).message  //TODO:translate
                            );
                        },
                        success: function (record, operation) {
                            Ext.ComponentQuery.query('administration-content-classes-tabitems-users-grid')[0].fireEventArgs('reload', [record, 'delete']);
                        }
                    });
                }
            }, this);
    },


    onOpenBtnClick: function () {
        var view = this.getView();
        var url = 'administration/classes/' + view.getObjectTypeName() + '/cards/' + view.getObjectId();
        this.redirectTo(url, true);
    }

});
