Ext.define('CMDBuildUI.view.administration.content.users.card.View', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.users.card.ViewController',
        'CMDBuildUI.view.administration.content.users.card.EditModel',
        'Ext.layout.*'
    ],

    alias: 'widget.administration-content-users-card-view',
    controller: 'administration-content-users-card-view',
    viewModel: {
        type: 'view-administration-content-users-card-edit'
    },

    config: {
        objectTypeName: null,
        objectId: null,
        shownInPopup: false
    },

    scrollable: true,
    // ui: 'administration-formpagination',
    cls: 'administration',
    ui: 'administration-tabandtools',
    fieldDefaults: {
        labelAlign: 'top',
        xtype: 'fieldcontainer',
        flex: '0.5',
        padding: '0 15 0 15',
        layout: 'anchor'
    },

    // items: [{
    //     layout: 'column',
    //     defaults: {
    //         columnWidth: 0.5
    //     },
    //     items: [{
    //         xtype: 'displayfield',
    //         fieldLabel: 'Username',
    //         name: 'username',
    //         bind: {
    //             value: '{theUser.username}'
    //         }
    //     }, {
    //         xtype: 'displayfield',
    //         fieldLabel: 'Description',
    //         name: 'description',
    //         bind: {
    //             value: '{theUser.description}'
    //         }
    //     }]
    // }, {
    //     layout: 'column',
    //     defaults: {
    //         columnWidth: 0.5
    //     },
    //     items: [{
    //         xtype: 'displayfield',
    //         fieldLabel: 'Email',
    //         name: 'email',
    //         bind: {
    //             value: '{theUser.email}'
    //         }
    //     }]
    // }, {
    //     layout: 'column',
    //     defaults: {
    //         columnWidth: 0.5
    //     },
    //     items: [{
    //         xtype: 'displayfield',
    //         fieldLabel: 'Language',
    //         labelAlign: 'top',
    //         name: 'language',
    //         readOnly: true,
    //         bind: {
    //             value: '{theUser.language}'
    //         }
    //     }, {
    //         xtype: 'displayfield',
    //         fieldLabel: 'Initial page',
    //         name: 'initialPage',
    //         readOnly: true,
    //         bind: {
    //             value: '{theUser.initialPage}'
    //         }
    //     }]
    // }, {
    //     layout: 'column',
    //     defaults: {
    //         columnWidth: 0.5
    //     },
    //     items: [{
    //         xtype: 'checkbox',
    //         fieldLabel: 'Service',
    //         name: 'service',
    //         readOnly: true,
    //         bind: {
    //             value: '{theUser.service}'
    //         }
    //     }, {
    //         xtype: 'checkbox',
    //         fieldLabel: 'Privileged',
    //         name: 'privileged',
    //         readOnly: true,
    //         bind: {
    //             value: '{theUser.privileged}'
    //         }
    //     }]
    // }, {
    //     layout: 'column',
    //     defaults: {
    //         columnWidth: 0.5
    //     },
    //     items: [{
    //         xtype: 'checkbox',
    //         fieldLabel: 'Active',
    //         name: 'active',
    //         readOnly: true,
    //         bind: {
    //             value: '{theUser.active}'
    //         }
    //     }]
    // }]

    items: [{
        title: 'General data',
        xtype: "fieldset",
        collapsible: true,
        ui: 'administration-formpagination',
        fieldDefaults: {
            labelAlign: 'top',
            xtype: 'fieldcontainer',
            flex: '0.5',
            padding: '0 15 0 15',
            layout: 'anchor'
        },
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Username',
                name: 'username',
                bind: {
                    value: '{theUser.username}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Description',
                name: 'description',
                bind: {
                    value: '{theUser.description}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Email',
                name: 'email',
                bind: {
                    value: '{theUser.email}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Language',
                labelAlign: 'top',
                name: 'language',
                readOnly: true,
                bind: {
                    value: '{theUser.language}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Initial page',
                name: 'initialPage',
                readOnly: true,
                bind: {
                    value: '{theUser.initialPage}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Service',
                name: 'service',
                readOnly: true,
                bind: {
                    value: '{theUser.service}'
                }
            }, {
                xtype: 'checkbox',
                fieldLabel: 'Privileged',
                name: 'privileged',
                readOnly: true,
                bind: {
                    value: '{theUser.privileged}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                readOnly: true,
                bind: {
                    value: '{theUser.active}'
                }
            }]
        }]
    }, {
        title: 'Groups',
        xtype: "fieldset",
        collapsible: true,
        ui: 'administration-formpagination',
        fieldDefaults: {
            labelAlign: 'top',
            xtype: 'fieldcontainer',
            flex: '0.5',
            padding: '0 15 0 15',
            layout: 'anchor'
        },
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                labelAlign: 'top',
                flex: '0.5',
                padding: '0 15 0 15',
                layout: 'anchor',
                fieldLabel: 'Multigroup',
                readOnly: true,
                bind: {
                    value: '{theUser.multiGroup}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                labelAlign: 'top',
                flex: '0.5',
                padding: '0 15 0 15',
                layout: 'anchor',
                fieldLabel: 'Default group',
                bind: {
                    value: '{theUser.defaultUserGroup}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Groups',
                name: 'username',
                bind: {
                    value: '{groupsHtml}'
                }
            }]
        }]
    }, {
        title: 'Tenants',
        xtype: "fieldset",
        ui: 'administration-formpagination',
        collapsible: true,
        fieldDefaults: {
            labelAlign: 'top',
            xtype: 'fieldcontainer',
            flex: '0.5',
            padding: '0 15 0 15',
            layout: 'anchor'
        },
        items: [
        /**
         * Currently not supperted
         */
        //     {
        //     layout: 'column',
        //     defaults: {
        //         columnWidth: 0.5
        //     },
        //     items: [{
        //         xtype: 'checkbox',
        //         labelAlign: 'top',
        //         flex: '0.5',
        //         padding: '0 15 0 15',
        //         layout: 'anchor',
        //         fieldLabel: 'Multitenant',
        //         readOnly: true,
        //         bind: {
        //             value: '{theUser.multiTenant}'
        //         }
        //     }]
        // }, {
        //     layout: 'column',
        //     defaults: {
        //         columnWidth: 0.5
        //     },
        //     items: [{
        //         xtype: 'displayfield',
        //         labelAlign: 'top',
        //         flex: '0.5',
        //         padding: '0 15 0 15',
        //         layout: 'anchor',
        //         fieldLabel: 'Default tenant',
        //         bind: {
        //             value: '{theUser.defaultUserTenant}'
        //         }
        //     }]
        // },
        
        {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Tenants',
                name: 'username',
                bind: {
                    value: '{tenantsHtml}'
                }
            }]
        }]
    }]
});