Ext.define('CMDBuildUI.view.administration.content.users.card.ViewInRow', {

    extend: 'CMDBuildUI.components.tab.FormPanel',
    requires: [
        'CMDBuildUI.view.administration.content.users.card.ViewInRowController',
        'CMDBuildUI.view.administration.content.users.card.ViewInRowModel',
        'Ext.layout.*'
    ],
    autoDestroy: true,
    alias: 'widget.administration-content-users-card-viewinrow',
    controller: 'administration-content-users-card-viewinrow',
    viewModel: {
        type: 'view-administration-content-users-card-edit'
    },

    cls: 'administration',
    ui: 'administration-tabandtools',
    config: {
        objectTypeName: null,
        objectId: null,
        shownInPopup: false,
        theUser: null
    },
    bind: {
        theUser: '{theUser}',
        hidden: '{!theUser.username}'
    },

    items: [{
        title: 'General data',
        xtype: "form",
        fieldDefaults: {
            labelAlign: 'top',
            xtype: 'fieldcontainer',
            flex: '0.5',
            padding: '0 15 0 15',
            layout: 'anchor'
        },
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Username',
                name: 'username',
                bind: {
                    value: '{theUser.username}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Description',
                name: 'description',
                bind: {
                    value: '{theUser.description}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Email',
                name: 'email',
                bind: {
                    value: '{theUser.email}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Language',
                labelAlign: 'top',
                name: 'language',
                readOnly: true,
                bind: {
                    value: '{theUser.language}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Initial page',
                name: 'initialPage',
                readOnly: true,
                bind: {
                    value: '{theUser.initialPage}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Service',
                name: 'service',
                readOnly: true,
                bind: {
                    value: '{theUser.service}'
                }
            }, {
                xtype: 'checkbox',
                fieldLabel: 'Privileged',
                name: 'privileged',
                readOnly: true,
                bind: {
                    value: '{theUser.privileged}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                readOnly: true,
                bind: {
                    value: '{theUser.active}'
                }
            }]
        }]
    }, {
        title: 'Groups',
        xtype: "form",
        fieldDefaults: {
            labelAlign: 'top',
            xtype: 'fieldcontainer',
            flex: '0.5',
            padding: '0 15 0 15',
            layout: 'anchor'
        },
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                labelAlign: 'top',
                flex: '0.5',
                padding: '0 15 0 15',
                layout: 'anchor',
                fieldLabel: 'Multigroup',
                readOnly: true,
                bind: {
                    value: '{theUser.multiGroup}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                labelAlign: 'top',
                flex: '0.5',
                padding: '0 15 0 15',
                layout: 'anchor',
                fieldLabel: 'Default group',
                bind: {
                    value: '{theUser.defaultUserGroup}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Groups',
                bind: {
                    value: '{groupsHtml}'
                }
            }]
        }]
    }, {
        title: 'Tenants',
        xtype: "form",
        fieldDefaults: {
            labelAlign: 'top',
            xtype: 'fieldcontainer',
            flex: '0.5',
            padding: '0 15 0 15',
            layout: 'anchor'
        },
        items: [{
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'checkbox',
                labelAlign: 'top',
                flex: '0.5',
                padding: '0 15 0 15',
                layout: 'anchor',
                fieldLabel: 'Multitenant',
                readOnly: true,
                bind: {
                    value: '{theUser.multiTenant}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                labelAlign: 'top',
                flex: '0.5',
                padding: '0 15 0 15',
                layout: 'anchor',
                fieldLabel: 'Default tenant',
                bind: {
                    value: '{theUser.defaultUserTenant}'
                }
            }]
        }, {
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items: [{
                xtype: 'displayfield',
                fieldLabel: 'Tenants',
                name: 'username',
                bind: {
                    value: '{tenantsHtml}'
                }
            }]
        }]
    }],
    tools: [{
        xtype: 'tbfill'
    }, {
        xtype: 'tool',
        itemId: 'editBtn',
        glyph: 'f040@FontAwesome', // Pencil icon
        tooltip: 'Edit card',
        callback: 'onEditBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-users-card-view-editBtn'
        },
        bind: {
            hidden: '{!canEdit}'
        }
    }, {
        xtype: 'tool',
        itemId: 'openBtn',
        glyph: 'f08e@FontAwesome', // Open icon
        tooltip: 'Open card',
        callback: 'onOpenBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-users-card-view-openBtn'
        },
        bind: {
            hidden: '{hideOpenBtn}'
        }
    }, {
        xtype: 'tool',
        itemId: 'deleteBtn',
        glyph: 'f014@FontAwesome',
        tooltip: 'Delete card',
        callback: 'onDeleteBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-users-card-view-deleteBtn'
        },
        bind: {
            hidden: '{!canDelete}'
        }
    }]
});