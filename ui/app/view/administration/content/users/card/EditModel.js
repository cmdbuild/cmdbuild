Ext.define('CMDBuildUI.view.administration.content.users.card.EditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.view-administration-content-users-card-edit',
    data: {
        theUser: null,
        rolesData: [],
        tenantsData: []
    },

    formulas: {

        getAllPages: {
            bind: '{theUser}',
            get: function (theUser) {
                var data = [];
                var types = {
                    classes: {
                        label: 'Classes', // TODO: translate
                        childrens: Ext.getStore('classes.Classes').getData().getRange()
                    },
                    processes: {
                        label: 'Processes', // TODO: translate
                        childrens: Ext.getStore('processes.Processes').getData().getRange()
                    },
                    dashboards: {
                        label: 'Dasboards', // TODO: translate
                        childrens: Ext.getStore('Dashboards').getData().getRange()
                    },
                    custompages: {
                        label: 'Custom Pages', // TODO: translate
                        childrens: Ext.getStore('custompages.CustomPages').getData().getRange()
                    },
                    views: {
                        label: 'Views', // TODO: translate
                        childrens: Ext.getStore('views.Views').getData().getRange()
                    }
                };
                Object.keys(types).forEach(function (type, typeIndex) {
                    types[type].childrens.forEach(function (value, index) {
                        var item = {
                            group: type,
                            groupLabel: types[type].label, // TODO: translate
                            _id: value.get('_id'),
                            label: value.get('_description_translation') || value.get('description') // TODO: can be localized?
                        };
                        data.push(item);
                    });
                });
                data.sort(function (a, b) {
                    var aGroup = a.group.toUpperCase();
                    var bGroup = b.group.toUpperCase();
                    var aLabel = a.label.toUpperCase();
                    var bLabel = b.label.toUpperCase();

                    if (aGroup == bGroup) {
                        return (aLabel < bLabel) ? -1 : (aLabel > bLabel) ? 1 : 0;
                    } else {
                        return (aGroup < bGroup) ? -1 : 1;
                    }
                });
                return data;
            }
        },
        groupsHtml: {
            bind: '{theUser.userGroups}',
            get: function (groups) {
                
                if (groups && groups.length) {
                    var groupsHtml = '<ul>';
                    groups.forEach(function (group) {
                        groupsHtml += '<li>' + group.description + '</li>';
                    });
                    groupsHtml += '</ul>';
                    return groupsHtml;
                }
                return '<em>No data</em>'; // TODO: translate
            }
        },
        tenantsHtml: {
            bind: '{theUser.userTenants}',
            get: function (tenants) {
                if (tenants && tenants.length) {
                    var tenantsHtml = '<ul>';
                    tenants.forEach(function (tenant) {
                        tenantsHtml += '<li>' + tenant.description + '</li>';
                    });
                    return tenantsHtml += '</ul>';

                }
                return '<em>No data</em>'; // TODO: translate
            }
        },

        panelTitle: {
            bind: '{theUser.username}',
            get: function (username) {
                var title = Ext.String.format(
                    '{0} - {1}',
                    'User', // TODO: translate
                    username
                );
                this.getParent().set('title', title);
            }
        },

        getRolesData: {
            bind: '{theUser}',
            get: function (theUser) {
                if (theUser) {
                    // var deferred = new Ext.Deferred();
                    var me = this;
                    var store = Ext.create('Ext.data.Store', {
                        proxy: {
                            type: 'baseproxy',
                            url: '/roles'
                        }
                    });
                    store.load({
                        params: {
                            limit: 0
                        },
                        callback: function (items, operation, success) {
                            var roles = [];
                            if (!theUser.get('userGroups')) {
                                theUser.set('userGroups', []);
                            }

                            items.forEach(function (role) {
                                var exist = theUser.get('userGroups').find(function (userGroup) {
                                    return userGroup._id === role.get('_id');
                                });
                                roles.push({
                                    description: role.get('description'),
                                    _id: role.get('_id'),
                                    name: role.get('name'),
                                    active: (exist) ? true : false
                                });
                            });
                            me.set('rolesData', roles);
                            me.set('theUser.groupsLength', roles.length);
                        },
                        scope: this
                    });
                }

            }
        },

        getTenantsData: {
            bind: '{theUser}',
            get: function (theUser) {
                var me = this;
                if (theUser) {
                    CMDBuildUI.util.administration.helper.ConfigHelper.getConfig().then(function (config) {

                        var multitenantConfig = config.filter(function (item) {
                            return 'org__DOT__cmdbuild__DOT__multitenant__DOT__mode' === item._key;
                        })[0];

                        var isMultenantActive = multitenantConfig.hasValue && multitenantConfig.value !== 'DISABLED';
                        if (isMultenantActive) {
                            var store = Ext.create('Ext.data.Store', {
                                proxy: {
                                    type: 'baseproxy',
                                    url: '/tenants'
                                }
                            });
                            store.load({
                                params: {
                                    limit: 0
                                },
                                callback: function (items, operation, success) {
                                    var tenantsActive = [],
                                        tenantsData = [];
                                    if (!theUser.get('userTenants')) {
                                        theUser.set('userTenants', []);
                                    }

                                    items.forEach(function (tenant) {

                                        var exist = theUser.get('userTenants').find(function (userTenant) {
                                            return userTenant._id === tenant.get('_id');
                                        });

                                        var _tenant = {
                                            description: tenant.get('description'),
                                            _id: tenant.get('_id'),
                                            name: tenant.get('name') || tenant.get('code'),
                                            active: (exist) ? true : false
                                        };
                                        tenantsData.push(_tenant);
                                    });
                                    this.set('tenantsData', tenantsData);
                                    this.set('theUsers.userTenants', tenantsData);
                                },
                                scope: me
                            });
                        }
                    });
                }
            }
        },

        userGroups: {
            bind: {
                userGroups: '{theUser.userGroups}',
                groupsLength: '{theUser.groupsLength}'
            },

            get: function (data) {
                if (data.userGroups) {
                    return data.userGroups;
                }
            },
            set: function (value) {
                this.set('theUser.userGroups', value);
                this.set('theUser.groupsLength', this.get('theUser.userGroups').length);
            }
        },
        userTenants: {
            bind: {
                userTenants: '{theUser.userTenants}',
                tenantsLength: '{theUser.tenantsLength}'
            },

            get: function (data) {
                if (data.userTenants) {
                    return data.userTenants;
                }
            },
            set: function (value) {
                var activeItems = value.filter(function (item) {
                    return item.active === true;
                });
                this.set('theUser.userTenants', activeItems);
                this.set('theUser.tenantsLength', activeItems.length);
            }
        }
    },

    stores: {
        rolesStore: {
            data: '{rolesData}',
            autoDestroy: true
        },
        userGroupsStore: {
            proxy: {
                type: 'baseproxy',
                url: '{userGroupProxyUrl}'
            },
            sorters: 'description',
            autoLoad: true,
            autoDestroy: true
        },
        tenantsStore: {
            data: '{tenantsData}',
            proxy: {
                type: 'memory'
            },
            autoDestroy: true
        },
        userTenantsStore: {
            proxy: {
                type: 'baseproxy',
                url: '{userTenantProxyUrl}'
            },
            sorters: 'description',
            autoLoad: true,
            autoDestroy: true
        },
        languagesStore: {
            model: 'CMDBuildUI.model.Language',
            proxy: {
                type: 'baseproxy',
                url: '/languages',
                extraParams: {
                    active: true
                }
            },
            sorters: 'description',
            autoLoad: true,
            autoDestroy: true
        },
        getAllPagesStore: {
            data: '{getAllPages}',
            autoDestroy: true
        },
        getSelectedGroups: {
            data: '{userGroups}',
            proxy: {
                type: 'memory'
            },
            sorters: 'description',
            autoLoad: true,
            autoDestroy: true
        },
        getSelectedTenants: {
            data: '{userTenants}',
            proxy: {
                type: 'memory'
            },
            sorters: 'description',
            autoLoad: true,
            autoDestroy: true,
            filters: [function (item) {
                return item.get('active') === true;
            }]
        }
    }
});