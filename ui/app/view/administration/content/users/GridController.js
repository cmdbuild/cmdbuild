Ext.define('CMDBuildUI.view.administration.content.users.GridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-users-grid',
    listen: {
        global: {
            userupdated: 'onUserUpdated',
            usercreated: 'onUserCreated'
        }
    },

    control: {
        '#': {
            sortchange: 'onSortChange',
            //afterrender: 'onAfterRender'
            beforerender: 'onAfterRender'
        },
        '#adduser': {
            click: 'onNewBtnClick'
        },
        tableview: {
            deselect: 'onDeselect',
            select: 'onSelect'
            // beforedrop: 'onBeforeDrop'
        }
    },

    onAfterRender: function (view) {
        view.getViewModel().get("allUsers").load();
    },

    onIncludeDisabledChange: function (field, newValue, oldValue) {
        var vm = this.getViewModel();

        var filterCollection = vm.get("allUsers").getFilters();

        if (newValue === true) {
            filterCollection.removeByKey('enabledFilter');
        } else {
            filterCollection.add({
                id: 'enabledFilter',
                property: 'enabled',
                value: !newValue
            });

        }
    },

    /**
     * Filter grid items.
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchSubmit: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        var searchValue = vm.getData().search.value;
        var allUserStore = vm.get("allUsers");
        if (searchValue) {
            var filter = {
                "query": searchValue
            };
            allUserStore.getProxy().setExtraParam('filter', Ext.JSON.encode(filter));
            allUserStore.load();
        } else {
            this.onSearchClear(field);
        }
    },

    /**
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchClear: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // clear store filter
        var allUserStore = vm.get("allUsers");
        allUserStore.getProxy().setExtraParam('filter', Ext.JSON.encode([]));
        allUserStore.load();
        // reset input
        field.reset();
    },

    /**
     * @param {Ext.form.field.Base} field
     * @param {Ext.event.Event} event
     */
    onSearchSpecialKey: function (field, event) {
        if (event.getKey() == event.ENTER) {
            this.onSearchSubmit(field);
        }
    },

    /**
     * @param {Ext.selection.RowModel} row
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} eOpts
     */
    onDeselect: function (row, record, index, eOpts) {
        this.getView().getPlugins()[0].enable();
    },

    /**
     * @param {Ext.selection.RowModel} row
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} eOpts
     */
    onSelect: function (row, record, index, eOpts) {
        this.view.setSelection(record);
        Ext.GlobalEvents.fireEventArgs('selecteduser', [record]);
    },

    /**
     * 
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onNewBtnClick: function (item, event, eOpts) {
        var view = this.getView();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-users-card-create',
            viewModel: {
                links: {
                    theUser: {
                        type: 'CMDBuildUI.model.users.User',
                        create: true
                    }
                }
            }
        });
    },

    onUserUpdated: function (record) {
        var view = this.getView();
        view.getView().refresh();
        setTimeout(function () {
            var store = view.getStore();
            var index = store.findExact("_id", record.getId());
            var storeItem = store.getAt(index);

            view.getPlugin('forminrowwidget').view.fireEventArgs('togglerow', [null, storeItem, index]);
            view.getPlugin('forminrowwidget').view.fireEventArgs('togglerow', [null, storeItem, index]);
        }, 0);
    },

    onUserCreated: function (record) {
        var view = this.getView();

        var store = view.getStore();
        store.load();

        store.on('load', function (records, operation, success) {
            view.getView().refresh();
            setTimeout(function () {
                var store = view.getStore();
                var index = store.findExact("_id", record.getId());
                var storeItem = store.getAt(index);

                view.getPlugin('forminrowwidget').view.fireEventArgs('togglerow', [null, storeItem, index]);
            }, 0);
        });
    },

    onSortChange: function () {


        if (this.view.getSelection().length) {
            var store = this.view.getStore();
            var index = store.findExact("_id", this.view.getSelection()[0].get('_id'));
            var record = store.getAt(index);
            this.view.getPlugin('forminrowwidget').view.fireEventArgs('togglerow', [null, record, index]);
            this.view.getPlugin('forminrowwidget').view.fireEventArgs('togglerow', [null, record, index]);
        }
    }

});