Ext.define('CMDBuildUI.view.administration.content.users.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-users-view',
    data: {
        title: 'Users',
        actions: {
            view: true,
            edit: false,
            add: false
        },
        toolbarHiddenButtons: {
            edit: true, // action !== view
            print: true, // action !== view
            disable: true,
            enable: true
        }
    },

    formulas: {
        actionManager: {
            bind: '{action}',
            get: function (action) {
                if (this.get('actions.edit')) {
                    return 'EDIT';
                } else if (this.get('actions.add')) {
                    return 'ADD';
                } else {
                    return 'VIEW';
                }
            },
            set: function (value) {
                this.set('actions.view', value === 'VIEW');
                this.set('actions.edit', value === 'EDIT');
                this.set('actions.add', value === 'ADD');
            }
        },
        getToolbarButtons: {
            bind: '{theUser.active}',
            get: function (get) {
                this.set('toolbarHiddenButtons.edit', !this.get('actions.view'));
                this.set('toolbarHiddenButtons.print', !this.get('actions.view'));
                this.set('toolbarHiddenButtons.disable', true);
                this.set('toolbarHiddenButtons.enable', false);
            }
        },
        updateToolbarButtons: {
            bind: '{theUser.active}',
            get: function (data) {
                if (data) {
                    this.set('toolbarHiddenButtons.disable', false);
                    this.set('toolbarHiddenButtons.enable', true);
                } else {
                    this.set('toolbarHiddenButtons.disable', true);
                    this.set('toolbarHiddenButtons.enable', false);
                }
            }
        }
    }
});
