Ext.define('CMDBuildUI.view.administration.content.users.TopbarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-users-topbar',

    control:{
        '#adduser':{
            click: 'onNewBtnClick'
        }
    },

    /**
     * 
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onNewBtnClick: function (item, event, eOpts) {
        var view = this.getView();
        view.up('administration-content-users-view').getViewModel().set('isGridHidden', false);
        
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-users-card-create',
            viewModel: {
                links: {
                    theUser: {
                        type: 'CMDBuildUI.model.users.User',
                        create: true
                    }
                }
            }
        });
    },
});
