Ext.define('CMDBuildUI.view.administration.content.users.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.users.GridController',
        'CMDBuildUI.view.administration.content.users.GridModel',

        // plugins
        'Ext.grid.filters.Filters',
        'CMDBuildUI.components.grid.plugin.FormInRowWidget'
    ],

    alias: 'widget.administration-content-users-grid',
    controller: 'administration-content-users-grid',
    viewModel: {
        type: 'administration-content-users-grid'
    },
    bind: {
        store: '{allUsers}',
        selection: '{selected}'
    },

    columns: [{
        text: 'Username', // TODO: translate
        dataIndex: 'username',
        align: 'left'
    }, {
        text: 'Description', // TODO: translate
        dataIndex: 'description',
        align: 'left'
    }, {
        text: 'Email', // TODO: translate
        dataIndex: 'email',
        align: 'center'
    }, {
        text: 'Enabled', // TODO: translate
        dataIndex: 'active',
        align: 'center',
        xtype: 'checkcolumn',
        disabled: true,
        disabledCls: '' // or don't add this config if you want the field to look disabled
    }],

    plugins: [{
        ptype: 'forminrowwidget',
        pluginId: 'forminrowwidget',
        expandOnDblClick: true,
        widget: {
            xtype: 'administration-content-users-card-viewinrow',
            autoHeight: true,
            ui: 'administration-tabandtools',
            bind: {
                 theUser: '{theUser}'
            },
            viewModel: {
                theUser: null
            }
        }
    }],

    autoEl: {
        'data-testid': 'administration-content-users-grid'
    },

    forceFit: true,
    loadMask: true,

    selModel: {
        pruneRemoved: false // See https://docs.sencha.com/extjs/6.2.0/classic/Ext.selection.Model.html#cfg-pruneRemoved
    },
    labelWidth: "auto"
});

