Ext.define('CMDBuildUI.view.administration.content.menus.TopbarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-menus-topbar',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#addmenu': {
            click: 'onAddMenuClick'
        }
    },
   
    onBeforeRender: function(view){
        view.up('administration-content').getViewModel().set('title', 'Menus');
    },
    onAddMenuClick: function (view) {
        this.redirectTo('administration/menus', true);
        var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
        vm.set('selected', null);
    }
});
