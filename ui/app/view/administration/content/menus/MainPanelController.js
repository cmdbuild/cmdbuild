Ext.define('CMDBuildUI.view.administration.content.menus.MainPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-menus-mainpanel',

    control: {

        '#cancelBtn': {
            click: 'onCancelBtnClick'
        }
    },

    onAddFolderBtnClick: function () {
        var index;
        var vm = this.getViewModel();
        var newFolderName = vm.get('newFolderName');
        if (!newFolderName) {
            CMDBuildUI.util.Notifier.showMessage(
                "Folder name empty", { // TODO: translate
                    ui: 'administration',
                    icon: CMDBuildUI.util.Notifier.icons.error
                }
            );
            return false;
        }
        var destination = this.lookupReference('menuTreeViewDestination');
        var destinationStore = destination.getStore();
        var selectedDestinationNode = destination.getSelectionModel().getSelected();
        if (selectedDestinationNode.length > 0) {
            index = selectedDestinationNode.items[0].childNodes.length;
        }
        var nodeModel = Ext.create('CMDBuildUI.model.menu.MenuItem', {
            objectdescription: newFolderName,
            objectDescription: newFolderName,
            menutype: CMDBuildUI.model.menu.MenuItem.types.folder,
            menuType: CMDBuildUI.model.menu.MenuItem.types.folder,
            expanded: true,
            children: [],
            index: index
        });
        if (selectedDestinationNode.length === 0) {
            destinationStore.getRoot().appendChild(nodeModel);
        } else {
            if (selectedDestinationNode.items[0].get('leaf')) {
                this.getParentNode(selectedDestinationNode.items[0]).appendChild(nodeModel);
            } else {
                selectedDestinationNode.items[0].appendChild(nodeModel);
            }
        }

        vm.set('newFolderName', '');
    },

    onRemoveItemBtnClick: function () {
        var destination = this.lookupReference('menuTreeViewDestination');
        var store = destination.getStore();
        var selectedDestinationNode = destination.getSelectionModel().getSelection()[0];
        if (!selectedDestinationNode.get('root')) {
            selectedDestinationNode.remove();
            store.sync();
        }
    },

    onSaveBtnClick: function () {
        var me = this;
        var menuTreeView = this.lookupReference('menuTreeViewDestination');
        var store = this.getView().down('administration-content-menus-treepanels-destinationpanel').getStore();
        var root = store.getRoot();
        var rootData = root.childNodes;
        var vm = me.getView().getViewModel();
        var theMenu = vm.get('theMenu');
        var menu = (!theMenu.isModel) ? Ext.create('CMDBuildUI.model.menu.Menu', theMenu) : theMenu;

        //  var menu = vm.get('theMenu');
        var group = menu.get('group');
        if (group.length > 0) {

            var rootRaw = {
                group: group,
                children: [],
                menuType: rootData.menutype || 'root',
                objectDescription: rootData.objectdescription || 'ROOT',
                _id: menu.getId(),
                objectTypeName: rootData.objecttype
            };

            root.childNodes.forEach(function (item, index) {
                rootRaw.children.push({
                    children: me.getRawTree(item.childNodes),
                    menuType: item.get('menutype'),
                    objectDescription: item.get('objectdescription'),
                    _id: item.get('_id'),
                    objectTypeName: item.get('objecttype')
                });
            });

            if (vm.get('actions.add')) {
                Ext.Ajax.request({
                    url: CMDBuildUI.util.Config.baseUrl + '/menu',
                    method: 'POST',
                    jsonData: rootRaw,
                    success: function (transport) {
                        var response = JSON.parse(transport.responseText).data;
                        // TODO: this will be deprecated on #635 fix
                        me.dropCache();
                        var currentView = 'VIEW';
                        vm.getParent().set('action', currentView);
                        
                        var nextUrl = Ext.String.format('administration/menus/{0}', response._id);
                        CMDBuildUI.util.administration.MenuStoreBuilder.initialize(
                            function () {
                                var treestore = Ext.getCmp('administrationNavigationTree');
                                var selected = treestore.getStore().findNode("href", nextUrl);
                                treestore.setSelection(selected);
                            });
                        me.redirectTo(nextUrl, true);
                    }
                });
            } else {
                Ext.Ajax.request({
                    url: CMDBuildUI.util.Config.baseUrl + '/menu/' + menu.getId(),
                    method: 'PUT',
                    jsonData: rootRaw,
                    success: function (transport) {
                        var response = JSON.parse(transport.responseText).data;
                        var currentView = 'VIEW';
                        vm.getParent().set('action', currentView);
                        me.redirectTo(Ext.String.format('administration/menus/{0}', response._id), true);
                    }
                });
            }
        } else {
            // TODO: invalidate group combobox
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        if (this.getViewModel().get('actions.edit')) {
            this.redirectTo(Ext.String.format('administration/menus/{0}', this.getViewModel().get('theMenu._id')), true);
        } else if (this.getViewModel().get('actions.add')) {
            var store = Ext.getStore('administration.MenuAdministration');
            var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
            var currentNode = store.findNode("objecttype", CMDBuildUI.model.administration.MenuItem.types.menu);
            vm.set('selected', currentNode);
            this.redirectTo('administration/menus_empty', true);
        }

    },

    privates: {

        // TODO: this will be deprecated on #635 fix
        dropCache: function () {
            Ext.Ajax.request({
                url: CMDBuildUI.util.Config.baseUrl + '/system/cache/drop',
                method: 'POST',
                success: function (transport) {}
            });
        },
        getParentNode: function (node) {
            if (node.get('leaf')) {
                return this.getParentNode(node.parentNode);
            }
            return node;
        },


        getRawTree: function (childNodes) {
            var me = this;
            var output = [];
            childNodes.forEach(function (item, index) {
                var _item = {
                    children: [],
                    menuType: item.get('menutype'),
                    objectDescription: item.get('objectdescription'),
                    _id: item.get('_id'),
                    objectTypeName: item.get('objecttype')
                };

                if (item.hasChildNodes()) {
                    _item.children = me.getRawTree(item.childNodes);
                }
                output.push(_item);
            });
            return output;
        }

    }
});