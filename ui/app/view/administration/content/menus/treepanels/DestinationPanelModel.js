Ext.define('CMDBuildUI.view.administration.content.menus.treepanels.DestinationPanelModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-menus-treepanels-destinationpanel',
    data: {
        presents: {

        }
    },
    formulas: {

        getCurrentMenuData: {
            bind: '{theMenu}',
            get: function (theMenu) {
                if (theMenu) {
                    var me = this;
                    
                    if ((!theMenu.isModel)) {
                        theMenu.id = undefined;
                        theMenu._id = undefined;
                        theMenu = Ext.create('CMDBuildUI.model.menu.Menu', theMenu);
                    }
                    var record = theMenu.getData();
                    var root = Ext.create('CMDBuildUI.model.menu.MenuItem', {
                        _id: record._id,
                        id: record._id,
                        expanded: true,
                        menutype: record.menuType,
                        index: record.index || 0,
                        objecttype: record.objectTypeName,
                        text: record.description,
                        leaf: false,
                        root: true,
                        children: [] // record.children && record.children.length > 0 ? me.getRecordsAsSubmenu(record.children, record.menuType, record) : []
                    });

                    if (record.children && record.children.length > 0) {
                        root.appendChild(me.getRecordsAsSubmenu(record.children, record.menuType, record));
                    }
                    var presents = me.get('presents');
                    var ostore = Ext.getStore('menuOriginTreeStore');
                    ostore.clearFilter();
                    // REMOVE LOG TABLE

                    var logTable = [];
                    ostore.addFilter(function (item) {
                        var menutype = item.get('menutype');
                        var objecttype = item.get('objecttype');
                        var objecttypeIndex = Ext.Array.indexOf(presents[menutype], objecttype);

                        logTable.push([menutype, objecttype, objecttypeIndex]);
                        return objecttypeIndex === -1;

                    });
                    this.getView().getStore().setRoot(root);
                }
            }
        }
    },

    getRecordsAsSubmenu: function (records, menutype, parent) {
        var output = [];
        var me = this;

        for (var i = 0; i < records.length; i++) {
            var record = records[i];

            if (record.menuType === '_Report') {
                record.menuType = 'reportpdf';
            }

            record.menutype = record.menuType;
            if (record.objectTypeName && record.menutype !== 'folder') {
                var presents = this.get('presents.' + record.menutype) || [];
                presents.push(record.objectTypeName);
                this.set('presents.' + record.menutype, presents);
            }
            if (record.menuType === 'reportpdf') {
                //debugger;
            }
            var menuitem = {
                _id: record._id,
                id: record._id,
                menutype: record.menuType,
                index: i || record.index || 0,
                objecttype: record.objectTypeName,
                text: record.objectDescription,
                objectdescription: record.objectDescription,
                leaf: record.children && record.children.length > 0 ? false : true,
                expanded: record.menuType === 'folder' ? true : false,
                parentId: parent._id,
                children: record.children && record.children.length > 0 ? me.getRecordsAsSubmenu(record.children, menutype, record) : []
            };

            var model = Ext.create('CMDBuildUI.model.menu.MenuItem', menuitem);
            switch (record.menuType) {
                case 'class':
                    var klass = Ext.getStore('classes.Classes').getById(record.objectTypeName);
                    if (klass && klass.get('prototype')) {
                        model.data.iconCls = CMDBuildUI.model.menu.MenuItem.icons.klassparent;
                    }
                    break;
                case 'process':
                    var process = Ext.getStore('processes.Processes').getById(record.objectTypeName);
                    if (process && process.get('prototype')) {
                        model.data.iconCls = CMDBuildUI.model.menu.MenuItem.icons.klassparent;
                    }
                    break;
                default:
                    break;
            }

            output.push(model);
        }
        return output;
    }
});