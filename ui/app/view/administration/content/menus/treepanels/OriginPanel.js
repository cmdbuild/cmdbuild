Ext.define('CMDBuildUI.view.administration.content.menus.treepanels.OriginPanel', {
    extend: 'Ext.tree.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.menus.treepanels.OriginPanelController',
        'CMDBuildUI.view.administration.content.menus.treepanels.OriginPanelModel'
    ],

    alias: 'widget.administration-content-menus-treepanels-originpanel',
    controller: 'administration-content-menus-treepanels-originpanel',
    viewModel: {
        type: 'administration-content-menus-treepanels-originpanel'
    },

    itemId: 'treepanelorigin',
    flex: 1,
    cls: 'tree-noborder',
    ui: 'administration-navigation-tree',
    reference: 'menuTreeViewOrigin',
    align: 'start',
    scrollable: 'y',
    useArrows: true,
    expanded: true,

    store: Ext.create('Ext.data.TreeStore', {
        model: 'CMDBuildUI.model.menu.MenuItem',
        storeId: 'menuOriginTreeStore',
        reference: 'menuOriginTreeStore',
        root: {
            text: 'Root',
            expanded: true,
            children: []
        },
        proxy: {
            type: 'memory'
        },
        sorters: [{
            property: 'index',
            direction: 'ASC'
        }],
        autoLoad: true
    }),

    rootVisible: false,
    columns: [{
        xtype: 'treecolumn',
        dataIndex: 'text',
        flex: 1,
        align: 'start',
        /**
         * @param {String} value
         * @param {Object} metaData
         * @param {Ext.data.Model} record
         * @param {Number} rowIndex
         * @param {Number} colIndex
         * @param {Ext.data.Store} store
         * @param {Ext.view.View} view
         * 
         * @returns {String}
         */
        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
            metaData.align = 'left';
            return value;
        },
        editor: {
            xtype: 'textfield',
            allowBlank: false
        }
    }],

    viewConfig: {
        plugins: {
            id: 'treeviewdragdroporigin',
            // copy: true,
            ptype: 'treeviewdragdrop',
            ddGroup: 'TreeDD',
            nodeHighlightOnRepair: false,
            // ddGroup: 'two-trees-drag-drop',
            enableDrop: false,
            appendOnly: false,
            sortOnDrop: false,
            containerScroll: true,
            allowContainerDrops: false,
            dragZone: {
                animRepair: false
            }
        }
    }
});