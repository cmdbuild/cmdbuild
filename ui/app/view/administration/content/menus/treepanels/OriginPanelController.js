Ext.define('CMDBuildUI.view.administration.content.menus.treepanels.OriginPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-menus-treepanels-originpanel',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function (view) {

        CMDBuildUI.util.MenuStoreBuilder.initialize();
        CMDBuildUI.util.MenuStoreBuilder.allItems.expanded = true;

        var classesItems = Ext.getStore('classes.Classes').getData().getRange();
        var processesItems = Ext.getStore('processes.Processes').getData().getRange();
        var reportsItems = Ext.getStore('Reports').getData().getRange();
        var viewsItems = Ext.getStore('views.Views').getData().getRange();
        var dashboardsItems = Ext.getStore('Dashboards').getData().getRange();
        var custompagesItems = Ext.getStore('custompages.CustomPages').getData().getRange();

        var store = view.getStore();
        var root = store.getRootNode();
        root.removeAll();

        root.appendChild({
            menutype: 'folder',
            index: 0,
            objectdescription: 'Classes', // TODO: translate
            leaf: false,
            allowDrag: false,
            expanded: true,
            children: this.generateChildren(classesItems, CMDBuildUI.model.menu.MenuItem.types.klass)
        });

        root.appendChild({
            menutype: 'folder',
            index: 2,
            objectdescription: 'Processes', // TODO: translate
            leaf: false,
            allowDrag: false,
            expanded: true,
            children: this.generateChildren(processesItems, CMDBuildUI.model.menu.MenuItem.types.process)
        });
        root.appendChild({
            menutype: 'folder',
            index: 3,
            objectdescription: 'Reports', // TODO: translate
            leaf: false,
            allowDrag: false,
            expanded: true,
            children: this.generateChildren(reportsItems, CMDBuildUI.model.menu.MenuItem.types.report)
        });
        root.appendChild({
            menutype: 'folder',
            index: 4,
            objectdescription: 'Views', // TODO: translate
            leaf: false,
            allowDrag: false,
            expanded: true,
            children: this.generateChildren(viewsItems, CMDBuildUI.model.menu.MenuItem.types.view)
        });

        root.appendChild({
            menutype: 'folder',
            index: 5,
            objectdescription: 'Dashboards', // TODO: translate
            leaf: false,
            allowDrag: false,
            expanded: true,
            children: this.generateChildren(dashboardsItems, CMDBuildUI.model.menu.MenuItem.types.dashboard)
        });

        root.appendChild({
            menutype: 'folder',
            index: 5,
            objectdescription: 'Custom pages', // TODO: translate
            leaf: false,
            allowDrag: false,
            expanded: true,
            children: this.generateChildren(custompagesItems, CMDBuildUI.model.menu.MenuItem.types.custompage)
        });
    },
    privates: {
        /**
         * 
         */
        generateChildren: function (items, type) {
            var me = this;
            var _items = [];
            items.forEach(function (element, index) {
                switch (type) {
                    case CMDBuildUI.model.menu.MenuItem.types.klass:
                        _items.push(me.generateClassChildren(type, element, index, 'Class'));
                        break;
                    case CMDBuildUI.model.menu.MenuItem.types.process:
                        _items.push(me.generateClassChildren(type, element, index, 'Process'));
                        break;
                    case CMDBuildUI.model.menu.MenuItem.types.custompage:
                        _items.push(me.generateCustompageChildren(type, element, index, 'Custom page'));
                        break;
                    case CMDBuildUI.model.menu.MenuItem.types.report:
                    case CMDBuildUI.model.menu.MenuItem.types.reportcsv:
                    case CMDBuildUI.model.menu.MenuItem.types.reportodt:
                    case CMDBuildUI.model.menu.MenuItem.types.reportpdf:
                    case CMDBuildUI.model.menu.MenuItem.types.reportrtf:
                        _items.push(me.generateReportChildren(CMDBuildUI.model.menu.MenuItem.types.reportpdf, element, index, 'PDF'));
                        _items.push(me.generateReportChildren(CMDBuildUI.model.menu.MenuItem.types.reportcsv, element, index, 'CSV'));
                        break;

                    case CMDBuildUI.model.menu.MenuItem.types.dashboard:
                        _items.push(me.generateClassChildren(type, element, index, 'Dashboard'));
                        break;

                    case CMDBuildUI.model.menu.MenuItem.types.view:
                        _items.push(me.generateClassChildren(type, element, index, 'View'));
                        break;
                    default:
                        _items.push(me.generateClassChildren(type, element, index));
                        break;
                }
            });
            return _items;
        },
        generateClassChildren: function (type, element, index, qtip) {
            // ok
            var uuid = CMDBuildUI.util.Utilities.generateUUID();
            var leaf = {
                _id: uuid,
                id: uuid,
                menutype: type,
                index: index,
                objecttype: element.get('_id'),
                text: element.get('description'),
                objectdescription: element.get('description'),
                objectDescription: element.get('description'),
                leaf: true,
                qtip: qtip
            };
            leaf = Ext.create('CMDBuildUI.model.menu.MenuItem', leaf);
            if (element.get('prototype')) {
                switch (type) {
                    case CMDBuildUI.model.menu.MenuItem.types.klass:
                        leaf.data.iconCls = CMDBuildUI.model.menu.MenuItem.icons.klassparent;
                        break;
                    case CMDBuildUI.model.menu.MenuItem.types.process:
                        leaf.data.iconCls = CMDBuildUI.model.menu.MenuItem.icons.processparent;
                        break;
                    default:
                        break;
                }
            }
            return leaf;
        },
        generateCustompageChildren: function (type, element, index, qtip) {
            // ok
            var uuid = CMDBuildUI.util.Utilities.generateUUID();
            var leaf = {
                _id: uuid,
                id: uuid,
                menutype: type,
                index: index,
                objecttype: element.get('name'),
                objectid: element.get('_id'),
                text: element.get('description'),
                objectdescription: element.get('description'),
                objectDescription: element.get('description'),
                leaf: true,
                qtip: qtip
            };
            leaf = Ext.create('CMDBuildUI.model.menu.MenuItem', leaf);
            return leaf;
        },
        generateReportChildren: function (type, element, index, qtip) {
            // 

            var uuid = CMDBuildUI.util.Utilities.generateUUID();
            var leaf = {
                _id: uuid,
                id: uuid,
                menutype: type,
                index: index,
                objecttype: element.get('code'),
                objectid: element.get('_id'),
                text: element.get('description'),
                objectdescription: element.get('description'),
                objectDescription: element.get('description'),
                leaf: true,
                qtip: qtip
            };

            leaf = Ext.create('CMDBuildUI.model.menu.MenuItem', leaf);
            return leaf;
        }
    }
});