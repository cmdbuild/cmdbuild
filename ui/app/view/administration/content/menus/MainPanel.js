Ext.define('CMDBuildUI.view.administration.content.menus.MainPanel', {
    extend: 'Ext.container.Container',
    alias: 'widget.administration-content-menus-mainpanel',
    controller: 'administration-content-menus-mainpanel',

    requires: [
        'CMDBuildUI.view.administration.content.menus.MainPanelController',
        'CMDBuildUI.util.MenuStoreBuilder'
    ],

    viewModel: {},
    statics: {
        formmodes: {
            view: 'VIEW',
            edit: 'EDIT',
            create: 'ADD'
        }
    },
    itemId: 'administration-content-menus-mainpanel',
    ui: 'administration-tabandtools',
    layout: 'border',
    items: [{
        region: 'north',
        layout: 'column',
        defaults: {
            padding: '10 0 10 0'
        },

        items: [{
            columnWidth: 0.5,
            flex: 1,
            style: {
                margin: '0 10 0 10'
            },
            xtype: 'combobox',
            reference: 'comboGroup',
            queryMode: 'local',
            displayField: 'description',
            valueField: 'name',
            fieldLabel: 'Group',
            bind: {
                store: '{rolesStore}',
                hidden: '{!actions.add}'
            },
            listeners: {
                change: function (combo, newValue, oldValue, eOpts) {
                    this.up('administration-content-menu-view').getViewModel().set('theMenu.group',newValue);
                },
                expand: function () {
                    this.getStore().load({
                        scope: this,
                        callback: function (items) {
                            this.up('administration-content-menu-view').getViewModel().set('unusedRoles', items);
                        }
                    });
                }
            }
        }, {
            cls: 'administration-inline-label-textfield',
            columnWidth: 0.5,
            xtype: 'displayfield',
            fieldLabel: 'Group', // TODO: translate
            name: 'group',
            bind: {
                value: '{theMenu.name}',
                hidden: '{actions.add}'
            }
        }]
    }, {
        region: 'center',
        xtype: 'container',
        width: "100%",
        height: "100%",
        bodyPadding: 10,
        layout: {
            type: 'hbox',
            align: 'strech'
        },
        items: [{
            xtype: 'panel',
            layout: 'container',
            scrollable: 'y',
            width: "100%",
            height: "100%",
            marginBottom: 20,
            cls: 'container-border-1',
            flex: 1,

            items: [{
                xtype: 'administration-content-menus-treepanels-destinationpanel',
                marginBottom: 20
            }]
        }, {
            xtype: 'panel',
            layout: 'container',
            align: 'middle',
            shrinkWrap: false,
            height: "100%",
            hidden: true,
            bind: {
                hidden: '{actions.view}'
            },
            items: [{
                xtype: 'button',
                margin: '0 0 0 10',
                height: "100%",
                reference: 'removeItemBtn',
                iconCls: 'x-fa fa-arrow-circle-right fa-2x',
                tooltip: 'Remove item', // TODO: translate
                config: {
                    theValue: null
                },
                listeners: {
                    click: 'onRemoveItemBtnClick'
                },
                cls: 'administration-field-container-btn',
                autoEl: {
                    'data-testid': 'administration-lookupvalue-card-edit-translateBtn'
                }
            }]

        }, {
            xtype: 'panel',
            layout: 'container',
            scrollable: 'y',
            shrinkWrap: false,
            width: "100%",
            height: "100%",
            cls: 'container-border-1',
            flex: 1,
            hidden: true,
            bind: {
                hidden: '{actions.view}'
            },
            items: [{
                xtype: 'fieldcontainer',
                margin: '0 0 0 10',
                layout: {
                    type: 'hbox',
                    align: 'end'
                },
                items: [{
                    margin: 10,
                    flex: 1,
                    xtype: 'textfield',
                    fieldLabel: 'New folder', // TODO: translate
                    reference: 'newFolderName',
                    bind: '{newFolderName}',
                    listeners: {
                        change: function (field, value) {
                            field.lookupViewModel().set('canAddNewFolder', field.ownerCt.items.items[0].isDirty());
                        }
                    }
                }, {
                    xtype: 'button',
                    margin: '10 0 10 0',
                    reference: 'addFolderBtn',
                    iconCls: 'x-fa fa-plus-circle fa-2x',
                    tooltip: 'Add folder', // TODO: translate
                    allowBlank: false,
                    config: {
                        theValue: null
                    },
                    bind: {
                        disabled: '{!canAddNewFolder}'
                    },
                    listeners: {
                        click: 'onAddFolderBtnClick'
                    },
                    cls: 'administration-field-container-btn',
                    autoEl: {
                        'data-testid': 'administration-lookupvalue-card-edit-translateBtn'
                    }
                }]
            }, {
                xtype: 'administration-content-menus-treepanels-originpanel'
            }]
        }]
    }, {
        xtype: 'toolbar',
        ui: 'footer',
        region: 'south',
        style: {
            border: '10px solid #fff'

        },
        hidden: true,
        bind: {
            hidden: '{actions.view}'
        },
        items: [{
            xtype: 'component',
            flex: 1
        }, {
            text: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.saveBtn,
            ui: 'administration-action-small',
            listeners: {
                click: 'onSaveBtnClick'
            },
            bind: {
                disabled: '{!theMenu.group}'
            }
        }, {
            xtype: 'button',
            text: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.cancelBtn,
            ui: 'administration-secondary-action-small',
            itemId: 'cancelBtn',
            reference: 'cancelBtn'
        }]
    }]
});