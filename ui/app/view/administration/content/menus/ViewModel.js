Ext.define('CMDBuildUI.view.administration.content.menus.ViewModel', {
    extend: 'Ext.app.ViewModel',
    requires: [
        'CMDBuildUI.util.api.Groups',
        'CMDBuildUI.util.MenuStoreBuilder'
    ],
    alias: 'viewmodel.administration-content-menus-view',

    data: {
        title: 'Menu',
        action: 'VIEW',
        actions: {
            edit: false,
            add: false,
            view: true
        },
        newFolderName: '',
        canAddNewFolder: false,
        unusedRoles: []
    },
    formulas: {
        actionsManager: {
            bind: '{action}',
            get: function (action) {
                this.set('actions.edit', action === 'EDIT');
                this.set('actions.add', action === 'ADD');
                this.set('actions.view', action === 'VIEW');
            }
        }
    },

    stores: {
        
        rolesStore: {
            model: 'CMDBuildUI.model.users.Group',
            proxy: {
                url: CMDBuildUI.util.api.Groups.getRoles(),
                type: 'baseproxy',
                // server response is an empty array if this params are true
                pageParam: false, //to remove param "page"
                startParam: false, //to remove param "start"
                limitParam: false //to remove param "limit"
            },

            filters: [
                function (item) {
                    var menuAdministration = Ext.getStore('administration.MenuAdministration');
                    var menu =  menuAdministration.findNode("objecttype", 'menu');
                    if(menu && menu.childNodes){
                        var nodefilters = menu.childNodes;
                        var unusedItem = true;
                        nodefilters.forEach(function (node) {
                            if (node.get('text') === item.get('name')) {
                                unusedItem = false;
                            }
                        });
                        return unusedItem;
                    }
                    return false;
                }
            ],
            autoLoad: true,
            autoDestroy: true
        }
    },

    setCurrentAction: function (action) {
        this.set('actions.edit', action === 'EDIT');
        this.set('actions.add', action === 'ADD');
        this.set('actions.view', action === 'VIEW');
        this.set('action', action);
    },

    setExistingMenus: function (value) {
        this.set('existingMenu', value);
    }
});