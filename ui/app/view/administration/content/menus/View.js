(function () {
    var elementId = 'CMDBuildAdministrationContentMenusView';
    Ext.define('CMDBuildUI.view.administration.content.menus.View', {
        extend: 'Ext.panel.Panel',

        requires: [
            'CMDBuildUI.view.administration.content.menus.ViewController',
            'CMDBuildUI.view.administration.content.menus.ViewModel'
        ],

        controller: 'administration-content-menus-view',
        viewModel: {
            type: 'administration-content-menus-view'
        },
        alias: 'widget.administration-content-menu-view',

        id: elementId,
        statics: {
            elementId: elementId
        },
        defaults: {
            backgrounColor: '#ffffff',
            //margin: '10 10 0 10',
            textAlign: 'left'
        },
        layout: 'border',
        items: [{
            xtype: 'administration-content-menus-topbar',
            region: 'north',
            viewModel: {}
        }, {
            xtype: 'components-administration-toolbars-formtoolbar',

            cls: 'administration-mainview-tabpanel',
            region: 'north',
            items: [{

                xtype: 'button',

                itemId: 'spacer',
                style: {
                    "visibility": "hidden"
                }
            }, {
                xtype: 'tbfill'
            }, {
                xtype: 'tool',
                align: 'right',
                itemId: 'editBtn',
                cls: 'administration-tool',
                iconCls: 'x-fa fa-pencil',
                tooltip: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.editBtn.tooltip,
                callback: 'onEditBtnClick',
                autoEl: {
                    'data-testid': 'administration-domain-properties-tool-editbtn'
                },
                bind: {
                    hidden: '{!actions.view}'
                }
            }, {

                xtype: 'tool',
                align: 'right',
                itemId: 'deleteBtn',
                cls: 'administration-tool',
                iconCls: 'x-fa fa-trash',
                tooltip: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.deleteBtn.tooltip,
                callback: 'onDeleteBtnClick',
                autoEl: {
                    'data-testid': 'administration-domain-properties-tool-deletebtn'
                },
                bind: {
                    hidden: '{!actions.view}'
                }
            }, {
                xtype: 'button',
                align: 'right',
                itemId: 'copyFrom',
                reference: 'copyfrom',
                iconCls: 'x-fa fa-clone',
                cls: 'administration-tool',
                text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.copyfrom,
                tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.copyfrom,
                localized: {
                    text: 'CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.copyfrom',
                    tooltip: 'CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.copyfrom'
                },
                bind: {
                    hidden: '{actions.view}'
                },
                listeners: {
                    afterrender: 'setCopyButton'
                },
                // disabled: true,
                menu: {
                    items: []
                },
                visible: false,
                autoEl: {
                    'data-testid': 'administration-process-properties-tool-version'
                }
        
            }]
        }, {
            xtype: 'administration-content-menus-mainpanel',
            region: 'center',
            margin: '0 10 10 10',
            viewModel: {}
        }],

        listeners: {
            afterlayout: function (panel) {
                Ext.GlobalEvents.fireEventArgs("showadministrationcontentmask", [false]);
            }
        },
        initComponent: function () {
            var vm = this.getViewModel();
            vm.getParent().set('title', 'Menus'); // TODO: translate
            vm.setCurrentAction(vm.get('action'));
            this.callParent(arguments);
        }
    });
})();