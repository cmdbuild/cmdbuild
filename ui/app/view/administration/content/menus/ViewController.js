Ext.define('CMDBuildUI.view.administration.content.menus.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-menus-view',

    setCopyButton: function (view) {
        var me = this;
        setTimeout(function () {
            var copyFromButton = Ext.ComponentQuery.query('#copyFrom')[0];
            copyFromButton.menu.removeAll();
            var menuAdministration = Ext.getStore('administration.MenuAdministration');
            var menu = menuAdministration.findNode("objecttype", 'menu');
            if (menu && menu.childNodes) {
                Ext.Array.forEach(menu.childNodes, function (element) {
                    if (element.get('text') !== me.getViewModel().get('theMenu.name')) {
                        copyFromButton.menu.add({
                            text: element.get('text'),
                            iconCls: 'x-fa fa-users',
                            listeners: {
                                click: function () {
                                  
                                    me.cloneFrom(element, view);
                                }
                            }
                        });
                    }

                });
            }
        }, 100);


    },
    cloneFrom: function (menu, view, currentGrantsStore) {
        var vm = view.up().up().getViewModel();
        Ext.Ajax.request({
            url: CMDBuildUI.util.Config.baseUrl + '/menu/' + menu.get('objecttype'),
            method: 'GET',
            success: function (transport) {
                var response = JSON.parse(transport.responseText).data;
                var ostore = Ext.getStore('menuDestinationTreeStore');
                vm.set('theMenu', response);
            }
        });
    },
    /**
     * 
     * @param {*} button 
     * @param {*} event 
     * @param {*} eOpts 
     */
    onEditBtnClick: function (button, event, eOpts) {
        var vm = this.getView().getViewModel();
        var action = 'EDIT';
        vm.set('actions.edit', action === 'EDIT');
        vm.set('actions.add', action === 'ADD');
        vm.set('actions.view', action === 'VIEW');
        vm.set('action', action);
    }
});