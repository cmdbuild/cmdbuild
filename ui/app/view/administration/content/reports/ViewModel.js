Ext.define('CMDBuildUI.view.administration.content.reports.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-reports-view',


    data: {
        actions: {
            view: false,
            edit: false,
            add: false
        },
        hideForm: false
    },
    formulas: {
        formtoolbarHidden: {
            bind: {
                isView: '{actions.view}',
                isHiddenForm: '{hideForm}'
            },
            get: function(data){
                if(data.isView && !data.isHiddenForm){
                    return false;
                }
                return true;
            }
        }
    },

    /**
     * Change form mode
     * 
     * @param {String} mode
     */
    setFormMode: function (mode) {
        var me = this;
        switch (mode) {
            case 'view':
                me.set('actions.view', true);
                me.set('actions.edit', false);
                me.set('actions.add', false);
                break;
            case 'add':
                me.set('actions.view', false);
                me.set('actions.edit', false);
                me.set('actions.add', true);
                break;
            case 'edit':
                me.set('actions.view', false);
                me.set('actions.edit', true);
                me.set('actions.add', false);
                break;

        }
    }
});