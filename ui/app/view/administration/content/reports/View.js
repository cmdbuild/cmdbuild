Ext.define('CMDBuildUI.view.administration.content.reports.View', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.reports.ViewController',
        'CMDBuildUI.view.administration.content.reports.ViewModel'
    ],
    alias: 'widget.administration-content-reports-view',
    controller: 'administration-content-reports-view',
    layout: 'border',
    viewModel: {
        type: 'administration-content-reports-view'
    },
    ui: 'administration-tabandtools',
    fieldDefaults: {
        labelAlign: 'top',
        width: '100%'
    },
    items: [{
        xtype: 'panel',
        region: 'north',
        forceFit: true,
        loadMask: true,
        tbar: [{
            xtype: 'button',
            text: 'Add report',
            ui: 'administration-action-small',
            reference: 'addReport',
            itemId: 'addReport',
            iconCls: 'x-fa fa-plus',
            autoEl: {
                'data-testid': 'administration-report-addReportBtn'
            },
            handler: 'onAddReportBtnClick'
        }, {
            xtype: 'textfield',
            name: 'search',
            width: 250,
            emptyText: 'Search report...',
            cls: 'administration-input',
            reference: 'searchtext',
            itemId: 'searchtext',
            bind: {
                value: '{search.value}',
                hidden: '{!canFilter}'
            },
            listeners: {
                specialkey: 'onSearchSpecialKey'
            },
            triggers: {
                search: {
                    cls: Ext.baseCSSPrefix + 'form-search-trigger',
                    handler: 'onSearchSubmit',
                    autoEl: {
                        'data-testid': 'administration-report-form-search-trigger'
                    }
                },
                clear: {
                    cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                    handler: 'onSearchClear',
                    autoEl: {
                        'data-testid': 'administration-report-form-clear-trigger'
                    }
                }
            },
            autoEl: {
                'data-testid': 'administration-report-search-form'
            }
        }, {
            xtype: 'tbfill'
        }, {
            xtype: 'tbtext',
            dock: 'right',
            bind: {
                hidden: '{!theReport.description}',
                html: 'Report' + ': <b data-testid="administration-report-processName">{theReport.description}</b>'
            }
        }]
    }, {
        bind: {
            hidden: '{formtoolbarHidden}'
        },
        xtype: 'components-administration-toolbars-formtoolbar',
        region: 'north',
        borderBottom: 0,
        items: [{

            xtype: 'button',

            itemId: 'spacer',
            style: {
                "visibility": "hidden"
            }
        }, {
            xtype: 'tbfill'
        }, {
            xtype: 'tool',
            itemId: 'editBtn',
            iconCls: 'x-fa fa-pencil',
            tooltip: 'Edit report',
            callback: 'onEditReportBtnClick',
            cls: 'administration-tool',
            autoEl: {
                'data-testid': 'administration-report-editBtn'
            }
        }, {
            xtype: 'tool',
            itemId: 'downloadBtn',
            iconCls: 'x-fa fa-download',
            tooltip: 'Download report package',
            callback: 'onDownloadReportBtnClick',
            cls: 'administration-tool',
            autoEl: {
                'data-testid': 'administration-report-downloadBtn'
            }
        }, {
            xtype: 'tool',
            itemId: 'sqlBtn',
            iconCls: 'x-fa fa-database',
            tooltip: 'View report sql',
            callback: 'onSqlReportBtnClick',
            cls: 'administration-tool',
            autoEl: {
                'data-testid': 'administration-report-sqlBtn'
            }
        }, {
            xtype: 'tool',
            itemId: 'deleteBtn',
            iconCls: 'x-fa fa-trash',
            tooltip: 'Delete report',
            callback: 'onDeleteReportBtnClick',
            cls: 'administration-tool',
            autoEl: {
                'data-testid': 'administration-report-deleteBtn'
            }
        }, {
            xtype: 'tool',
            itemId: 'disableBtn',
            iconCls: 'x-fa fa-ban',
            tooltip: 'Disable report',
            callback: 'onDisableReportBtnClick',
            cls: 'administration-tool',
            bind: {
                hidden: '{!theReport.active}'
            },
            autoEl: {
                'data-testid': 'administration-report-disableBtn'
            }
        }, {

            xtype: 'tool',
            itemId: 'enableBtn',
            hidden: true,
            cls: 'administration-tool',
            iconCls: 'x-fa fa-check-circle-o',
            tooltip: 'Enable report',
            callback: 'onEnableReportBtnClick',
            autoEl: {
                'data-testid': 'administration-report-enableBtn'
            },
            bind: {
                hidden: '{theReport.active}'
            }
        }]
    }, {
        xtype: 'panel',
        region: 'center',
        scrollable: 'y',
        
        items: [{
            ui: 'administration-formpagination',
            bind: {
                hidden: '{hideForm}'
            },
            xtype: "fieldset",
            collapsible: true,
            title: "Generals", // TODO: translate
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        /********************* theReport.code **********************/
                        xtype: 'displayfield',
                        fieldLabel: 'Name',
                        hidden: true,
                        name: 'code',
                        bind: {
                            value: '{theReport.code}',
                            hidden: '{!actions.view}'
                        }
                    }, {
                        /********************* theReport.code **********************/
                        xtype: 'textfield',
                        fieldLabel: 'Name *',
                        hidden: true,
                        allowBlank: false,
                        maxLength: 20,
                        name: 'code',
                        bind: {
                            value: '{theReport.code}',
                            hidden: '{actions.view}',
                            disabled: '{actions.edit}'
                        }
                    }]
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        /********************* theReport.description **********************/
                        xtype: 'displayfield',
                        fieldLabel: 'Description',
                        hidden: true,
                        name: 'description',
                        bind: {
                            value: '{theReport.description}',
                            hidden: '{!actions.view}'
                        }
                    }, 
//                    {
                        /********************* theReport.description **********************/
/*                        xtype: 'textfield',
                        fieldLabel: 'Description *',
                        hidden: true,
                        allowBlank: false,
                        name: 'description',
                        bind: {
                            value: '{theReport.description}',
                            hidden: '{actions.view}'
                        }
                    }*/
                
                    {
                        xtype: 'container',
                        fieldLabel: 'Description',
                        layout: 'hbox',
                        bind: {
                            hidden: '{actions.view}'
                        },
                        items: [{
                            flex: 5,
                            fieldLabel: 'Description',
                            xtype: 'textfield',
                            name: 'description',
                            bind: {
                                value: '{theReport.description}'
                            }
                        }, {
                            xtype: 'fieldcontainer',
                            flex: 1,
                            fieldLabel: '&nbsp;',
                            labelSeparator: '',
                            items: [{
                                xtype: 'button',
                                reference: 'translateBtn',
                                iconCls: 'x-fa fa-flag fa-2x',
                                style: 'padding-left: 5px',
        
                                tooltip: 'Translate',
                                config: {
                                    theSetup: null
                                },
                                viewModel: {
                                    data: {
                                        theReport: null,
                                        vmKey: 'theReport'
                                    }
                                },
                                bind: {
                                    theReport: '{theReport}'
                                },
                                listeners: {
                                    click: 'onTranslateClick'
                                },
                                cls: 'administration-field-container-btn',
                                autoEl: {
                                    'data-testid': 'administration-lookupvalue-card-edit-translateBtn'
                                }
                            }]
        
                        }]
                    }]
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        /********************* theReport.active **********************/
                        xtype: 'checkbox',
                        fieldLabel: 'Enabled',
                        hidden: true,
                        name: 'enabled',
                        bind: {
                            value: '{theReport.active}',
                            readOnly: '{actions.view}',
                            hidden: '{!theReport}'
                        }
                    }]
                }]
            }]
        }, {
            ui: 'administration-formpagination',
            xtype: "fieldset",
            bind: {
                hidden: '{actions.view}'
            },
            collapsible: true,
            title: "File", // TODO: translate
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        /********************* theReport.file **********************/
                        
                        xtype: 'filefield',
                        name: 'file',
                        reference: 'file',
                        fieldLabel: 'Zip file *',
                        allowBlank: false,
                        accept: '.zip',
                        labelWidth: 50,
                        msgTarget: 'side',
                        anchor: '100%',
                        buttonText: 'Select zip file...',
                        buttonConfig: {
                            ui: 'administration-secondary-action-small'
                        }
                    }]
                }]
            }]
        }]
    }],

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        ui: 'footer',
        hidden: true,
        margin: '5 5 5 5',
        bind: {
            hidden: '{actions.view}'
        },
        items: [{
            xtype: 'tbfill'
        }, {
            text: 'Save',
            ui: 'administration-action-small',
            listeners: {
                click: 'onSaveBtnClick'
            }
        }, {
            text: 'Cancel',
            ui: 'administration-secondary-action-small',
            listeners: {
                click: 'onCancelBtnClick'
            }
        }]
    }]
});