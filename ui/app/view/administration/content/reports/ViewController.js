Ext.define('CMDBuildUI.view.administration.content.reports.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-reports-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function (view) {
        view.up('administration-content').getViewModel().set('title', 'Reports');
        this.setFilefieldProperties();
    },
    onAddReportBtnClick: function (button) {
        this.redirectTo('administration/reports_empty/true');
    },

    onDeleteReportBtnClick: function () {
        var me = this;
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-report');
                    me.getViewModel().get('theReport').erase({
                        success: function (record, operation) {
                            // TODO: issue #693
                            console.log('Currently not implemented on server, issue #693');
                        }
                    });
                }
            }, this);
    },

    onDownloadReportBtnClick: function () {
        var vm = this.getView().getViewModel();
        var url = Ext.String.format('{0}/reports/{1}/file?extension=ZIP', CMDBuildUI.util.Config.baseUrl, vm.get('theReport._id'));
        window.open(url, '_blank');
    },
    onEditReportBtnClick: function (button) {
        var vm = this.getView().getViewModel();
        vm.setFormMode('edit');
        this.setFilefieldProperties();
    },

    onCancelBtnClick: function (button) {
        var vm = this.getView().getViewModel();
        if (vm.get('actions.add')) {
            var store = Ext.getStore('administration.MenuAdministration');
            var vmNavigation = Ext.getCmp('administrationNavigationTree').getViewModel();
            var currentNode = store.findNode("objecttype", CMDBuildUI.model.administration.MenuItem.types.report);
            vmNavigation.set('selected', currentNode);
            this.redirectTo('administration/reports_empty', true);
        } else {
            vm.setFormMode('view');
        }
    },

    onDisableReportBtnClick: function (button) {
        var vm = this.getView().getViewModel();
        vm.set('theReport.active', false);
        this.save(vm);
    },

    onEnableReportBtnClick: function (button) {
        var vm = this.getView().getViewModel();
        vm.set('theReport.active', true);
        this.save(vm);
    },
    onSaveBtnClick: function (button) {
        var me = this;
        var form = this.getView().getForm();
        var vm = this.getView().getViewModel();

        if (form.isValid()) {
            this.save(vm,
                function (res) {
                    if (res.success && res.data._id) {
                        me.redirectTo(Ext.String.format('administration/reports/{0}', res.data._id), true);
                    } else {
                        me.redirectTo('administration/reports_empty', true);
                    }
                }
            );
        }
    },

    onSqlReportBtnClick: function () {
        var vm = this.getViewModel();
        var theQuery = vm.get('theReport.query');

        var content = {
            xtype: 'component',
            html: '<div id="reportSql"></div>',
            listeners: {
                afterrender: function (cmp) {
                    var editor = ace.edit('reportSql', {
                        //set autoscroll
                        autoScrollEditorIntoView: true,
                        maxLines: 90, // this will be changed on "change"
                        // set theme
                        theme: "ace/theme/chrome",
                        // show line numbers
                        showLineNumbers: true,
                        // hide print margin
                        showPrintMargin: false
                    });
                    editor.getSession().setMode("ace/mode/pgsql");
                    editor.getSession().setUseWrapMode(true);
                    editor.setValue(theQuery, -1);
                    var heightUpdateFunction = function () {
                        var editorDiv = document.getElementById("reportSql"); // its container
                        var doc = editor.getSession().getDocument(); // a reference to the doc
                        var lineHeight = editor.renderer.lineHeight;
                        editorDiv.style.height = lineHeight * doc.getLength() + "px"; // set new container height 
                        editorDiv.style.width = '100%'; // force container width to 100%
                        editor.setOption('maxLines', Math.ceil((cmp.up().getHeight() - 40) / lineHeight));
                        // its inner structure for adapting to a change in size
                        editor.resize();
                    };

                    // Set initial size to match initial content
                    setTimeout(function () {
                        heightUpdateFunction();
                    }, 10);

                    // Whenever a change happens inside the ACE editor, update
                    // the size again
                    editor.getSession().on('change', heightUpdateFunction);
                    editor.setReadOnly(true);
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-report-query');
            }
        };
        // create panel
        CMDBuildUI.util.Utilities.openPopup(
            'popup-report-query',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel'
            }
        );
    },

    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var theReport = vm.get('theReport');

        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('lookup.{0}.description', theReport.get('_type'))
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-classattribute-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-classattribute-localization',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px'
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() - 250);
    },    

    privates: {
        setFilefieldProperties: function () {
            var vm = this.getViewModel();
            var input = this.lookupReference('file');

            if (vm.get('actions.edit')) {
                input.allowBlank = true;
                input.setFieldLabel('Zip file'); // TODO: translate
            } else if (vm.get('actions.add')) {
                input.allowBlank = false;
                input.setFieldLabel('Zip file *'); // TODO: translate
            }
        },
        save: function (vm, successCb, errorCb) {
            CMDBuildUI.util.Ajax.setActionId('attachment.upload');
            // define method
            var method = vm.get("actions.add") ? "POST" : "PUT";

            var input = this.lookupReference('file').extractFileInput();

            // init formData
            var formData = new FormData();

            // append attachment json data
            var jsonData = Ext.encode(vm.get("theReport").getData());
            var fieldName = 'data';
            try {
                formData.append(fieldName, new Blob([jsonData], {
                    type: "application/json"
                }));
            } catch (err) {
                // TODO: sostituire con logger
                console.log("Unable to create attachment Blob FormData entry with type 'application/json', fallback to 'text/plain': " + err);
                // metadata as 'text/plain' (format compatible with older webviews)
                formData.append(fieldName, jsonData);
            }

            // get url
            var reportsUrl = Ext.String.format('{0}/reports', CMDBuildUI.util.Config.baseUrl);
            var url = vm.get('actions.add') ? reportsUrl : Ext.String.format('{0}/{1}', reportsUrl, vm.get('theReport._id'));

            CMDBuildUI.util.administration.File.upload(method, formData, input, url, {
                success: function (response) {
                    /**
                     * 
                     */
                    vm.setFormMode('view');
                    if (Ext.isFunction(successCb)) {
                        Ext.callback(successCb, null, [response]);
                    }
                },
                failure: function (error) {
                    /**
                     * 
                     */
                    CMDBuildUI.util.Notifier.showErrorMessage(error);
                    if (Ext.isFunction(errorCb)) {
                        Ext.callback(errorCb, null, [error]);
                    }
                }
            });
        }
    }
});