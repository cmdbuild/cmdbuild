
Ext.define('CMDBuildUI.view.administration.content.processes.elements.tabitems.domains.Domains',{
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.processes.elements.tabitems.domains.DomainsController',
        'CMDBuildUI.view.administration.content.processes.elements.tabitems.domains.DomainsModel'
    ],

    alias: 'widget.administration-content-processes-tabitems-domains-domains',
    controller: 'administration-content-processes-tabitems-domains-domains',
    viewModel: {
        type: 'administration-content-processes-tabitems-domains-domains'
    },

    html: 'To be implemented'
});
