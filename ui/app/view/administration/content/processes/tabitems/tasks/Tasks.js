
Ext.define('CMDBuildUI.view.administration.content.processes.elements.tabitems.tasks.Tasks',{
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.processes.elements.tabitems.tasks.TasksController',
        'CMDBuildUI.view.administration.content.processes.elements.tabitems.tasks.TasksModel'
    ],

    alias: 'widget.administration-content-processes-tabitems-tasks-tasks',
    controller: 'administration-content-processes-tabitems-tasks-tasks',
    viewModel: {
        type: 'administration-content-processes-tabitems-tasks-tasks'
    },

    html: 'To be implemented'
});
