Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.grid.GridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-processes-tabitems-attributes-grid-grid',
    listen: {
        global: {
            attributeupdated: 'onAttributeUpdated',
            attributecreated: 'onAttributeCreated'
        }
    },

    control: {
        '#addattribute': {
            click: 'onNewBtnClick'
        },
        tableview: {
            deselect: 'onDeselect',
            select: 'onSelect',
            beforedrop: 'onBeforeDrop'
        }
    },
    onBeforeDrop: function (node, data, overModel, dropPosition, dropHandlers) {
        // Defer the handling
        var vm = this.getViewModel();
        var view = this.getView();

        var filterCollection = vm.get("allAttributes").getFilters();
        view.getView().mask(CMDBuildUI.locales.Locales.administration.common.messages.loading);
        dropHandlers.wait = true;
        // by default allAttributes store have one filter for hide notes and idTenant attributes 
        if (filterCollection.length > 1) {
            var w = Ext.create('Ext.window.Toast', {
                ui: 'administration',
                width: 250,
                title: CMDBuildUI.locales.Locales.administration.common.messages.attention,
                html: CMDBuildUI.locales.Locales.administration.common.messages.cannotsortitems,
                iconCls: 'x-fa fa-exclamation-circle',
                align: 'br'
            });
            w.show();
            dropHandlers.cancelDrop();
            view.getView().unmask();

        } else {

            var moved = data.records[0].getId();
            var reference = overModel.getId();

            var attributes = vm.get('allAttributes').getData().getIndices();
            var sortableAttributes = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    sortableAttributes.push([key, attributes[key]]); // each item is an array in format [key, value]
                }
            }

            // sort items by value
            sortableAttributes.sort(function (a, b) {
                return a[1] - b[1]; // compare numbers
            });

            var jsonData = [];

            Ext.Array.forEach(sortableAttributes, function (val, key) {
                if (moved != val[0]) {
                    if (dropPosition === 'before' && reference === val[0]) {
                        jsonData.push(moved);
                    }
                    jsonData.push(val[0]);
                    if (dropPosition === 'after' && reference === val[0]) {
                        jsonData.push(moved);
                    }
                }
            });

            Ext.Ajax.request({
                url: Ext.String.format(
                    '{0}/processes/{1}/attributes/order',
                    CMDBuildUI.util.Config.baseUrl,
                    view.getViewModel().getParent().get('selected').get('objecttype')
                ),
                method: 'POST',
                jsonData: jsonData,
                success: function (response) {
                    var res = JSON.parse(response.responseText);
                    if (res.success) {
                        view.getView().grid.getStore().load();
                        dropHandlers.processDrop();
                    } else {
                        dropHandlers.cancelDrop();
                    }
                    view.getView().unmask();
                },
                error: function (response) {
                    dropHandlers.cancelDrop();
                    view.getView().unmask();
                }
            });
        }
    },

    onIncludeInheritedChange: function (field, newValue, oldValue) {
        var vm = this.getViewModel();

        var filterCollection = vm.get("allAttributes").getFilters();

        if (newValue === true) {
            filterCollection.removeByKey('inheritedFilter');
        } else {
            filterCollection.add({
                id: 'inheritedFilter',
                property: 'inherited',
                value: newValue
            });

        }
    },

    /**
     * Filter grid items.
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchSubmit: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // get value
        var searchTerm = vm.getData().search.value;
        var filterCollection = vm.get("allAttributes").getFilters();
        if (searchTerm) {
            filterCollection.add([{
                id: 'nameFilter',
                property: 'name',
                operator: 'like',
                value: searchTerm
            }, {
                id: 'descriptionFilter',
                property: 'description',
                operator: 'like',
                value: searchTerm
            }]);
        } else {
            this.onSearchClear(field);
        }
    },

    /**
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchClear: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // clear store filter
        var filterCollection = vm.get("allAttributes").getFilters();
        filterCollection.removeByKey('nameFilter');
        filterCollection.removeByKey('descriptionFilter');

        // reset input
        field.reset();
    },

    /**
     * @param {Ext.form.field.Base} field
     * @param {Ext.event.Event} event
     */
    onSearchSpecialKey: function (field, event) {
        if (event.getKey() == event.ENTER) {
            this.onSearchSubmit(field);
        }
    },

    /**
     * @param {Ext.selection.RowModel} row
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} eOpts
     */
    onDeselect: function (row, record, index, eOpts) {
        this.getView().getView().getPlugins()[0].enable();
    },

    /**
     * @param {Ext.selection.RowModel} row
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} eOpts
     */
    onSelect: function (row, record, index, eOpts) {
        this.view.setSelection(record);
        this.view.viewModel.set('selected', record);
        this.view.getViewModel().set('theAttribute', record);
        var formInRowPlugin = this.getView().getPlugin('forminrowwidget');

        Ext.GlobalEvents.fireEventArgs('selectedprocessattribute', [record]);
        if (formInRowPlugin && !Ext.Object.isEmpty(formInRowPlugin.recordsExpanded)) {
            this.getView().getView().getPlugins()[0].disable();
        } else {
            this.getView().getView().getPlugins()[0].enable();
        }
    },

    /**
     * 
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onNewBtnClick: function (item, event, eOpts) {
        var view = this.getView();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-processes-tabitems-attributes-card-create',
            viewModel: {
                data: {
                    processName: this.getView().getViewModel().get('theProcess.name'),
                    attributes: this.getView().getStore().getRange()
                }
            }
        });
    },

    onAttributeUpdated: function (record) {
        var view = this.getView();
        view.getPlugin('forminrowwidget').view.fireEventArgs('itemupdated', [view, record, this]);
    },

    onAttributeCreated: function (record) {
        var view = this.getView();

        var store = view.getStore();
        store.load();

        store.on('load', function (records, operation, success) {
            view.getView().refresh();
            setTimeout(function () {
                var store = view.getStore();
                var index = store.findExact("_id", record.getId());
                var storeItem = store.getAt(index);

                view.getPlugin('forminrowwidget').view.fireEventArgs('togglerow', [null, storeItem, index]);
            }, 0);
        });
    },

    onEditAttributeBtnClick: function () {
        var view = this.getView();
        var vm = view.getViewModel();

        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-processes-tabitems-attributes-card-edit',
            viewModel: {
                data: {
                    theAttribute: vm.get('selected'),
                    objectTypeName: vm.get('objectTypeName'),
                    attributeName: vm.get('selected').get('name'),
                    attributes: view.getStore().getRange(),
                    grid: this.getView()
                }
            }
        });
    },
    onOpenAttributeBtnClick: function () {
        var view = this.getView();
        var vm = view.getViewModel();

        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-processes-tabitems-attributes-card-view',

            viewModel: {
                data: {
                    theAttribute: vm.get('selected'),
                    objectTypeName: vm.get('objectTypeName'),
                    attributeName: vm.get('selected').get('name'),
                    attributes: view.getStore().getRange(),
                    grid: this.getView()
                }
            }
        });
    },
    
    onDeleteAttributeBtnClick: function () {
        var me = this;
        var view = me.getView();
        var vm = view.getViewModel();
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-attribute');

                    var record = vm.get('theAttribute');
                    record.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
                    record.model.setProxy({
                        type: 'baseproxy',
                        url: Ext.String.format('/processes/{0}/attributes/', vm.get('objectTypeName'))
                    });

                    record.erase({
                        failure: function (record, operation) {
                            view.getPlugin('forminrowwidget').view.fireEventArgs('itemupdated', [view, record, this]);
                        },
                        success: function (record, operation) {
                            CMDBuildUI.util.Notifier.showSuccessMessage(Ext.String.format('{0} attribute was deleted.', record.get('name')), null, 'administration');
                            view.getPlugin('forminrowwidget').view.fireEventArgs('itemdeleted', [view, record, this]);
                        }
                    });
                }
            }, this);
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onToggleBtnClick: function (button, e, eOpts) {
        var view = this.getView();
        var vm = view.getViewModel();
        var theAttribute = vm.get('theAttribute');
        Ext.apply(theAttribute.data, theAttribute.getAssociatedData());
        var value = !theAttribute.get('active');
        CMDBuildUI.util.Ajax.setActionId('toggle-active-attribute');
        theAttribute.set('active', value);
        theAttribute.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
        theAttribute.model.setProxy({
            type: 'baseproxy',
            url: Ext.String.format('/processes/{0}/attributes/', vm.get('objectTypeName'))
        });
        
        theAttribute.save({
            success: function (record, operation) {
                var valueString = record.get('active') ? 'enabled' : 'disabled';
                CMDBuildUI.util.Notifier.showSuccessMessage(Ext.String.format('{0} {1} {2}.',
                    record.get('name'),
                    CMDBuildUI.locales.Locales.administration.common.messages.was,
                    valueString), null, 'administration');
                Ext.GlobalEvents.fireEventArgs("attributeupdated", [record]);

            }
        });
    }
});