Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.EditController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-processes-tabitems-attributes-card-edit',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        this.linkAttribute(view, vm);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var form = this.getView();
        this.getViewModel().get('theAttribute').save({
            success: function (record, operation) {
                Ext.GlobalEvents.fireEventArgs("attributeupdated", [record]);
                form.up().fireEvent("closed");
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        vm.get("theAttribute").reject(); // discard changes
        this.getView().up().fireEvent("closed");
    },

    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var theValue = vm.get('theAttribute');

        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('lookup.{0}.description', theValue.get('_type'))
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-classattribute-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-procesattribute-localization',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px'
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() + 20);
    },

    privates: {
        linkAttribute: function (view, vm) {
            var record = (this.view.getViewModel().get('theAttribute')) ? this.view.getViewModel().get('theAttribute') : null;
            record.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
            record.model.setProxy({
                type: 'baseproxy',
                url: '/processes/' + vm.get('objectTypeName') + '/attributes/'
            });
            vm.set("theAttribute", record);
        }
    }

});