Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-processes-tabitems-attributes-card-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.View} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {

        var vm = this.getViewModel();
        var processName = vm.get('processName');
        var attributeName = vm.get('attributeName');

        if (processName && attributeName && !vm.get('theAttribute')) {
            vm.linkTo("theAttribute", {
                type: 'CMDBuildUI.model.Attribute',
                id: attributeName
            });
        }
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEditAttributeBtnClick: function (button, e, eOpts) {
        var view = this.getView();
        var vm = view.getViewModel();
        var viewConfig = {
            xtype: 'administration-content-classes-tabitems-attributes-card-edit',
            viewModel: {
                data: {
                    objectTypeName: vm.get('objectTypeName'),
                    attributeName: vm.get('attributeName'),
                    attributes: vm.get('attributes'),
                    grid: vm.get('grid')
                }
            }
        };
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add(viewConfig);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onDeleteAttributeBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        var view = this.getView();
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-attribute');
                    var theAttribute = vm.get('theAttribute');
                    theAttribute.getProxy().type = 'baseproxy';
                    theAttribute.getProxy().url = Ext.String.format('/processes/{0}/attributes/', vm.get('objectTypeName'));
                    theAttribute.erase({
                        success: function (record, operation) {
                            Ext.ComponentQuery.query('administration-content-processes-tabitems-attributes-grid-grid')[0].fireEventArgs('reload', [record, 'delete']);
                            view.up().fireEvent("closed");
                        }
                    });
                }
            }, this);
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onToggleBtnClick: function (button, e, eOpts) {
        var view = this.getView();
        var vm = view.getViewModel();
        var theAttribute = vm.get('theAttribute');
        Ext.apply(theAttribute.data, theAttribute.getAssociatedData());
        var value = !theAttribute.get('active');
        theAttribute.set('active', value);
        theAttribute.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
        theAttribute.model.setProxy({
            type: 'baseproxy',
            url: Ext.String.format('/processes/{0}/attributes/', vm.get('objectTypeName'))
        });

        theAttribute.save({
            success: function (record, operation) {
                var valueString = record.get('active') ? 'enabled' : 'disabled';
                CMDBuildUI.util.Notifier.showSuccessMessage(Ext.String.format('{0} {1} {2}.',
                    record.get('name'),
                    'attribute was', // TODO: translate
                    valueString), null, 'administration');
                Ext.GlobalEvents.fireEventArgs("attributeupdated", [record]);

            }
        });
    }
});