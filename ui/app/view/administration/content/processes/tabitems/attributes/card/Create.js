Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.Create', {
    extend: 'CMDBuildUI.view.administration.components.attributes.actionscontainers.Create',

    requires: [
        'CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.CreateController',
        'CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.EditModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],

    alias: 'widget.administration-content-processes-tabitems-attributes-card-create',
    controller: 'view-administration-content-processes-tabitems-attributes-card-create',
    viewModel: {
        type: 'view-administration-content-processes-tabitems-attributes-card-edit'
    }
});