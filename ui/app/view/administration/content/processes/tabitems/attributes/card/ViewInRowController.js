Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.ViewInRowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-processes-tabitems-attributes-card-viewinrow',
    listen: {
        global: {
            selectedclassattribute: 'onSelectedAttribute',
            attributeupdated: 'onAttributeUpdated'
        }
    },

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            itemupdated: 'onAttributeUpdated'

        }
    },

    onSelectedAttribute: function (attribute) {
        this.getViewModel().set('theAttribute', attribute);
    },

    /**
     * @param {CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.ViewInRow} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        this.linkAttribute(view, vm);
    },

    onAttributeUpdated: function (v, record) {
        var vm = this.getViewModel();
        var view = this.getView();
        this.linkAttribute(view, vm);
    },

    onEditBtnClick: function () {
        var view = this.getView();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-domains-tabitems-attributes-card-edit',
            viewModel: {
                data: {
                    theAttribute: view.getViewModel().get('selected'),
                    objectTypeName: view.getViewModel().get('objectTypeName'),
                    attributeName: view.getViewModel().get('theAttribute').get('name'),
                    attributes: this.getView().up().getStore().getRange(),
                    title: view.getViewModel().getParent().get('selected').get('objecttype') + ' - ' + 'Attributes' + ' - ' + view.getViewModel().get('theAttribute').get('name'),
                    grid: this.getView().up()
                }
            }
        });
    },

    onDeleteBtnClick: function () {
        var me = this;
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-attribute-process');
                    me.getViewModel().getData().theAttribute.erase({
                        success: function (record, operation) {
                            Ext.ComponentQuery.query('administration-content-processes-tabitems-attributes-grid')[0].fireEventArgs('reload', [record, 'delete']);
                        }
                    });
                }
            }, this);
    },


    onOpenBtnClick: function () {
        var view = this.getView();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-processes-tabitems-attributes-card-view',

            viewModel: {
                data: {
                    theAttribute: view.getViewModel().get('selected'),
                    processName: view.getViewModel().get('objectTypeName'),
                    attributeName: view.getViewModel().get('theAttribute').get('name'),
                    attributes: this.getView().up().getStore().getRange(),
                    title: view.getViewModel().getParent().get('selected').get('objecttype') + ' - ' + 'Attributes' + ' - ' + view.getViewModel().get('theAttribute').get('name'),
                    grid: this.getView().up()
                }
            }
        });
    },

    linkAttribute: function (view, vm) {
        var objectTypeName = view.getObjectTypeName();
        var objectId = view.getObjectId();
        var config = view.getInitialConfig();
        var theAttributeVM = (this.view.getViewModel().get('theAttribute')) ? this.view.getViewModel().get('theAttribute') : null;
        var theAttributeRecord = config._rowContext.viewModel.get('record');
        if (!objectTypeName && !objectId) {

            if (!Ext.isEmpty(config._rowContext)) {
                var record = theAttributeVM || theAttributeRecord;
                if (record && record.getData()) {

                    var selectedProcess = vm.get('theProcess');
                    if (selectedProcess.getData()) {
                        record.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
                        record.model.setProxy({
                            type: 'baseproxy',
                            url: '/processes/' + vm.get('theProcess').getId() + '/attributes/'
                        });
                        vm.set("theAttribute", record);
                    }
                }
            }
        }
    }
});