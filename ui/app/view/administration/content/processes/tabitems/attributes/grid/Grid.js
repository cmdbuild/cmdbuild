Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.grid.Grid', {
    extend: 'CMDBuildUI.view.administration.components.attributes.grid.Grid',

    requires: [
        'CMDBuildUI.view.administration.content.processes.tabitems.attributes.grid.GridController',
        'CMDBuildUI.view.administration.content.processes.tabitems.attributes.grid.GridModel'
    ],

    alias: 'widget.administration-content-processes-tabitems-attributes-grid-grid',
    controller: 'administration-content-processes-tabitems-attributes-grid-grid',
    viewModel: {
        type: 'administration-content-processes-tabitems-attributes-grid-grid'
    },
    editView: 'administration-content-processes-tabitems-attributes-card-edit'
});