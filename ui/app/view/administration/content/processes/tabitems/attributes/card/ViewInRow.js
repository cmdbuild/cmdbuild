Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.ViewInRow', {
    extend: 'CMDBuildUI.view.administration.components.attributes.actionscontainers.ViewInRow',
    requires: [
        'CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.ViewInRowController',
        'CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.ViewInRowModel',
        'Ext.layout.*'
    ],
    alias: 'widget.administration-content-processes-tabitems-attributes-card-viewinrow',
    controller: 'administration-content-processes-tabitems-attributes-card-viewinrow',
    viewModel: {
        type: 'view-administration-content-processes-tabitems-attributes-card-edit'
    }
});