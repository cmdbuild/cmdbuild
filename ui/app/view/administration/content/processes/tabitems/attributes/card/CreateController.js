Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.CreateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-processes-tabitems-attributes-card-create',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.processes.tabitems.attributes.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        // set vm varibles
        vm.linkTo("theAttribute", {
            type: 'CMDBuildUI.model.Attribute',
            create: true
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        var form = this.getView();

        if (form.isValid()) {
            var theAttribute = vm.getData().theAttribute;
            theAttribute.getProxy().setUrl(Ext.String.format('/processes/{0}/attributes', vm.get('processName')));
            delete theAttribute.data.inherited;
            delete theAttribute.data.writable;
            delete theAttribute.data.hidden;

            theAttribute.save({
                success: function (record, operation) {
                    var w = Ext.create('Ext.window.Toast', {
                        ui: 'administration',
                        title: 'Success!',
                        html: 'Attribute created saved correctly.',
                        iconCls: 'x-fa fa-check-circle',
                        align: 'br'
                    });
                    w.show();
                    Ext.GlobalEvents.fireEventArgs("attributecreated", [record]);
                    form.up().fireEventArgs("attributecreated", [vm.get('processName'), record.get('name')]);
                }
            });
        } else {
            var w = Ext.create('Ext.window.Toast', {
                ui: 'administration',
                html: 'Please correct indicted errors',
                title: 'Error!',
                iconCls: 'x-fa fa-check-circle',
                align: 'br'
            });
            w.show();
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveAndAddBtnClick: function (button, e, eOpts) {
        this.onSaveBtnClick();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-processes-tabitems-attributes-card-create',
            viewModel: {
                data: {
                    processName: container.getViewModel().get('selected').get('objecttype')
                }
            }
        });
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        this.getViewModel().get("theAttribute").reject();
        this.getView().up().fireEvent("closed");
    }
});