
Ext.define('CMDBuildUI.view.administration.content.processes.elements.tabitems.geoattributes.GeoAttributes',{
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.processes.elements.tabitems.geoattributes.GeoAttributesController',
        'CMDBuildUI.view.administration.content.processes.elements.tabitems.geoattributes.GeoAttributesModel'
    ],

    alias: 'widget.administration-content-processes-tabitems-geoattributes-geoattributes',
    controller: 'administration-content-processes-tabitems-geoattributes-geoattributes',
    viewModel: {
        type: 'administration-content-processes-tabitems-geoattributes-geoattributes'
    },

    html: 'To be implemented'
});
