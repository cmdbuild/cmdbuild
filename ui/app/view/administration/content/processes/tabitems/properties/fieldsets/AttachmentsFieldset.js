Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.properties.fieldsets.AttachmentsFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-processes-tabitems-properties-fieldsets-attachmentsfieldset',
    ui: 'administration-formpagination',
    items: [{
        xtype: 'fieldset',
        collapsible: true,
        layout: 'column',
        title: 'Process Attachments',
        ui: 'administration-formpagination',
        items: [{
            columnWidth: 0.5,
            items: [{
                /********************* Category Lookup **********************/
                xtype: 'combobox',
                queryMode: 'local',
                displayField: 'name',
                valueField: '_id',
                fieldLabel: 'Category Lookup',
                name: 'attachmentTypeLookup',
                hidden: true,
                bind: {
                    store: '{attachmentTypeLookupStore}',
                    value: '{theProcess.attachmentTypeLookup}',
                    hidden: '{actions.view}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Category Lookup',
                name: 'attachmentTypeLookup',
                hidden: true,
                bind: {
                    value: '{theProcess.attachmentTypeLookup}',
                    hidden: '{!actions.view}'
                }
            }]
        }, {
            cmdbuildtype: 'column',
            columnWidth: 0.5,
            style: {
                paddingLeft: '15px'
            },
            items: [{
                /********************* Description Mode **********************/
                xtype: 'combobox',
                fieldLabel: 'Description Mode',
                name: 'attachmentDescriptionMode',
                store: Ext.create('Ext.data.Store', {
                    fields: ['value', 'label'],
                    data: [{
                        "value": "hidden",
                        "label": "Hidden"
                    }, {
                        "value": "optional",
                        "label": "Visible optional" // TODO: translate
                    }, {
                        "value": "mandatory",
                        "label": "Visible madatory" // TODO: translate
                    }]
                }),
                queryMode: 'local',
                displayField: 'label',
                valueField: 'value',
                hidden: true,
                bind: {
                    value: '{theProcess.attachmentDescriptionMode}',
                    hidden: '{actions.view}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Description Mode',
                name: 'attachmentDescriptionMode',
                hidden: true,
                bind: {
                    value: '{theProcess.attachmentDescriptionMode}',
                    hidden: '{!actions.view}'
                },
                renderer: function (value) {
                    switch (value) {
                        case 'mandatory':
                            return "Visible madatory";
                        case 'optional':
                            return "Visible optional";
                        case 'hidden':
                            return 'Hidden';
                        default:
                            return value;
                    }
                }
            }]
        }]
    }]
});