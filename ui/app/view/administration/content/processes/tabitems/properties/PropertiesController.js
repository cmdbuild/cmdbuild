Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.properties.PropertiesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-processes-tabitems-properties-properties',


    require: [
        'CMDBuildUI.util.Utilities',
        'CMDBuildUI.util.administration.helper.FormHelper'
    ],

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function (view) {
        var vm = view.up('#CMDBuildAdministrationContentProcessView').getViewModel();
        var versionBtn = view.down('#versionBtn');
        var objectTypeName = vm.get('objectTypeName');

        Ext.create('Ext.data.Store', {
            proxy: {
                type: 'baseproxy',
                url: '/processes/' + objectTypeName + '/versions',
                reader: {
                    type: 'json'
                }
            },
            autoLoad: true,
            sorters: [{
                // Sort by version, in descending order
                sorterFn: function (record1, record2) {
                    var version1 = record1.get('version'),
                        version2 = record2.get('version');

                    return version1 > version2 ? 1 : (version1 === version2) ? 0 : -1;
                },
                direction: 'DESC'
            }],
            listeners: {
                load: function (store, records, success, operation, opts) {
                    var items = [];

                    store.each(function (record) {
                        var item = {
                            text: record.get('version'),
                            planId: record.get('planId'),
                            listeners: {
                                click: 'onVersionMenuItemClick'
                            },
                            cls: 'menu-item-nospace',
                            autoEl: {
                                'data-testid': 'administration-processes-properties-tool-version-' + record.get('version')
                            }
                        };

                        items.push(item);
                        versionBtn.menu.add(item);
                    });
                    if (items.length) {
                        versionBtn.setDisabled(false);
                    }
                }
            }
        });
    },

    onEditBtnClick: function (button) {
        this.getViewModel().getParent().set('action', CMDBuildUI.view.administration.content.processes.TabPanel.formmodes.edit);
    },

    onDeleteBtnClick: function (button) {
        var vm = this.getViewModel();

        Ext.Msg.confirm(
            "Delete process", // TODO: translate
            "Are you sure you want to delete this process?", // TODO: translate
            function (action) {
                if (action === CMDBuildUI.locales.Locales.administration.common.yes) {
                    var theProcess = vm.get('theProcess');
                    var tmpGetAssociated = theProcess.getAssociatedData;
                    theProcess.getAssociatedData = function () {
                        return false;
                    };

                    CMDBuildUI.util.Ajax.setActionId('delete-process');

                    theProcess.erase({
                        error: function () {
                            theProcess.getAssociatedData = tmpGetAssociated;
                        },
                        success: function (record, operation) {
                            var response = operation.getResponse();
                            var w = Ext.create('Ext.window.Toast', {
                                // Ex: "INFO: drop cascades to table "AssetTmpl_history""
                                html: Ext.JSON.decode(response.responseText).message || 'Process successfully deleted.', // TODO: translate
                                title: 'Success', // TODO: translate
                                iconCls: 'x-fa fa-check-circle"',
                                align: 'br',
                                autoClose: true,
                                maxWidth: 300,
                                monitorResize: true,
                                closable: true
                            });
                            w.show();
                            theProcess.commit();
                            theProcess.getAssociatedData = tmpGetAssociated;
                        }
                    });
                }
            }
        );
    },

    onVersionMenuItemClick: function (menuItem) {
        var url,
            objectTypeName = this.getView().up('#CMDBuildAdministrationContentProcessView').getViewModel().get('objectTypeName');

        url = CMDBuildUI.util.api.Processes.getVersionFileUrl(objectTypeName, menuItem.planId);
        window.open(url, '_blank');
    },

    onDisableBtnClick: function () {
        var vm = this.getViewModel();
        var theProcess = vm.get('theProcess');
        Ext.apply(theProcess.data, theProcess.getAssociatedData());
        theProcess.set('active', false);
        theProcess.save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: 'Process deactivated correctly.',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
                vm.getParent().linkTo('theProcess', {
                    type: 'CMDBuildUI.model.processes.Process',
                    id: vm.get('objectTypeName')
                });
                theProcess.commit();
            }
        });
    },

    onEnableBtnClick: function () {
        var vm = this.getViewModel();
        var theProcess = vm.get('theProcess');
        Ext.apply(theProcess.data, theProcess.getAssociatedData());
        theProcess.set('active', true);
        theProcess.save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: 'Process activated correctly.',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
                vm.getParent().linkTo('theProcess', {
                    type: 'CMDBuildUI.model.processes.Process',
                    id: vm.get('objectTypeName')
                });
                //vm.getParent().get('getToolbarButtons');
                theProcess.commit();
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var me = this;
        button.setDisabled(true);
        var vm = this.getViewModel();
        if (!vm.get('theProcess').isValid()) {
            var validatorResult = vm.get('theProcess').validate();
            var errors = validatorResult.items;
            for (var i = 0; i < errors.length; i++) {
                console.log('Key :' + errors[i].field + ' , Message :' + errors[i].msg);
            }
        } else {
            var theProcess = vm.get('theProcess');
            delete theProcess.data.system;
            Ext.apply(theProcess.data, theProcess.getAssociatedData());
            theProcess.save({
                success: function (record, operation) {
                    var objectTypeName = record.getId();
                    var nextUrl = Ext.String.format('administration/processes/{0}', objectTypeName);
                    CMDBuildUI.util.administration.MenuStoreBuilder.initialize(
                        function () {
                            var treestore = Ext.getCmp('administrationNavigationTree');
                            var selected = treestore.getStore().findNode("href", nextUrl);
                            treestore.setSelection(selected);
                        });

                    me.redirectTo(nextUrl, true);
                }
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        if (this.getViewModel().get('actions.edit')) {
            this.redirectTo(Ext.String.format('administration/processes/{0}', this.getViewModel().get('theProcess._id')), true);
        } else if (this.getViewModel().get('actions.add')) {
            var store = Ext.getStore('administration.MenuAdministration');
            console.log(store);
            var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
            var currentNode = store.findNode("objecttype", CMDBuildUI.model.administration.MenuItem.types.process);
            vm.set('selected', currentNode);
            this.redirectTo('administration/processes_empty', true);
        }
    },

    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var theProcess = vm.get('theProcess');

        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('lookup.{0}.description', theProcess.get('_type'))
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-classattribute-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-classattribute-localization',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px'
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() - 100);
    }

});