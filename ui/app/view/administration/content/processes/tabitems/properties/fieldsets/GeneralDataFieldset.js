Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.properties.fieldsets.GeneralDataFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-processes-tabitems-properties-fieldsets-generaldatafieldset',
    viewModel: {},
    ui: 'administration-formpagination',
    items: [{
        xtype: 'fieldset',
        layout: 'column',
        ui: 'administration-formpagination',
        title: 'General Attributes',
        // collapsible: false,
        items: [{
            columnWidth: 0.5,
            items: [{
                /********************* Name **********************/
                // create / edit
                xtype: 'textfield',
                vtype: 'alphanum',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.name.label,
                name: 'name',
                enforceMaxLength: true,
                maxLength: 20,
                hidden: true,
                bind: {
                    value: '{theProcess.name}',
                    hidden: '{!actions.add}'
                }
            }, {
                // view
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.name.label,
                name: 'name',
                hidden: true,
                bind: {
                    value: '{theProcess.name}',
                    hidden: '{actions.add}'
                }
            }, {
                /********************* description **********************/
                // create / edit
                xtype: 'combobox',
                editable: false,
                reference: 'parentField',
                displayField: 'description',
                valueField: 'name',
                name: 'parent',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.parent.label,
                hidden: true,
                bind: {
                    store: '{superclassesStore}',
                    value: '{theProcess.parent}',
                    hidden: '{hideParentCombobox}'
                }
            }, {
                // view
                xtype: 'displayfield',
                name: 'parent',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.parent.label,
                hidden: true,
                bind: {
                    value: '{theProcess.parent}',
                    hidden: '{hideParentDisplayfield}'
                }
            }, {
                /********************* Process type **********************/
                // create
                xtype: 'combobox',
                queryMode: 'local',
                displayField: 'label',
                valueField: 'value',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.parent.label,
                name: 'type',
                hidden: true,
                bind: {
                    store: '{processParentStore}',
                    value: '{theProcess.type}',
                    hidden: '{!actions.add}'
                }
            }, {
                // view
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.parent.label,
                name: 'parent',
                hidden: true,
                bind: {
                    value: '{theProcess.parent}',
                    hidden: '{actions.add}'
                }
            }, {
                /********************* stoppableByUser **********************/
                // create / edit / view
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.stoppableByUser.label,
                name: 'stoppableByUser',
                hidden: true,
                bind: {
                    value: '{theProcess.stoppableByUser}',
                    readOnly: '{actions.view}',
                    hidden: '{!theProcess}'
                }
            }, {
                /********************* Active **********************/
                // create / edit / view
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                hidden: true,
                bind: {
                    value: '{theProcess.active}',
                    readOnly: '{actions.view}',
                    hidden: '{!theProcess}'
                }
            }]
        }, {
            columnWidth: 0.5,
            style: {
                paddingLeft: '15px'
            },
            items: [
                //            {
                /********************* description **********************/
                // create / edit
                /*               xtype: 'textfield',
                                fieldLabel: 'Description',
                                name: 'description',
                                hidden: true,
                                bind: {
                                    value: '{theProcess.description}',
                                    hidden: '{actions.view}'
                                }
                            }*/
                {
                    xtype: 'container',
                    fieldLabel: 'Description',
                    layout: 'hbox',
                    bind: {
                        hidden: '{actions.view}'
                    },
                    items: [{
                        flex: 5,
                        fieldLabel: 'Description',
                        xtype: 'textfield',
                        name: 'description',
                        bind: {
                            value: '{theProcess.description}'
                        }
                    }, {
                        xtype: 'fieldcontainer',
                        flex: 1,
                        fieldLabel: '&nbsp;',
                        labelSeparator: '',
                        items: [{
                            xtype: 'button',
                            reference: 'translateBtn',
                            iconCls: 'x-fa fa-flag fa-2x',
                            style: 'padding-left: 5px',

                            tooltip: 'Translate',
                            config: {
                                theSetup: null
                            },
                            viewModel: {
                                data: {
                                    theProcess: null,
                                    vmKey: 'theProcess'
                                }
                            },
                            bind: {
                                theProcess: '{theProcess}'
                            },
                            listeners: {
                                click: 'onTranslateClick'
                            },
                            cls: 'administration-field-container-btn',
                            autoEl: {
                                'data-testid': 'administration-lookupvalue-card-edit-translateBtn'
                            }
                        }]

                    }]
                }, {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: 'Description',
                    name: 'description',
                    hidden: true,
                    bind: {
                        value: '{theProcess.description}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    /********************* Superclass **********************/
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Superclass',
                    name: 'prototype',
                    bind: {
                        value: '{theProcess.prototype}',
                        readOnly: '{!actions.add}'
                    }
                }, {
                    /********************* enableSaveButton **********************/
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.generalData.inputs.enableSaveButton.label,
                    name: 'enableSaveButton',
                    bind: {
                        value: '{theProcess.enableSaveButton}',
                        readOnly: '{actions.view}'
                    }
                }, {
                    /********************* Multitenant mode **********************/
                    xtype: 'fieldcontainer',
                    flex: 1,
                    fieldLabel: 'Multitenant mode',
                    bind: {
                        hidden: '{!isMultitenantActive}'
                    },
                    items: [{
                        // edit / add
                        xtype: 'combobox',
                        name: 'multitenantMode',
                        displayField: 'label',
                        valueField: 'value',
                        bind: {
                            value: '{theProcess.multitenantMode}',
                            hidden: '{actions.view}',
                            store: '{multitenantModeStore}'
                        }
                    }, {
                        // view
                        xtype: 'displayfield',
                        name: 'multitenantMode',
                        hidden: true,
                        bind: {
                            value: '{theProcess.multitenantMode}',
                            hidden: '{!actions.view}'
                        },
                        renderer: function (value) {
                            return Ext.String.capitalize(value);
                        }
                    }]
                }
            ]
        }]
    }]
});