Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.properties.fieldsets.IconFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-processes-tabitems-properties-fieldsets-iconfieldset',

    items: [{
        xtype: 'fieldset',
        collapsible: true,
        collapsed: false,
        layout: 'column',
        title: 'Icon', // TODO: translate
        ui: 'administration-formpagination',
        items: [{
            columnWidth: 0.5,
            xtype: 'fieldcontainer',
            layout: 'column',
            items: [{
                xtype: 'image',
                columnWidth: 0.2,
                height: 32,
                width: 32,
                alt: 'Precess icon',
                floated: true,
                style: 'margin-top: 30px',
                reference: 'currentIconPreview',
                fieldLabel: 'New icon',
                tooltip: 'Current icon', // TODO: translate
                config: {
                    theValue: null
                },
                viewModel: {
                    data: {
                        theProcess: null,
                        vmKey: 'theProcess'
                    }
                },

                // TODO: url is currently not set by server
                //src: 'http://www.sencha.com/assets/images/sencha-avatar-64x64.png',
                bind: {
                    src: '{theProcess._icon}',
                    al: 'Process icon'
                }
            }, {
                columnWidth: 0.8,
                xtype: 'filefield',
                fieldLabel: 'New icon',
                reference: 'iconFile',
                emptyText: 'Select an .png file',
                accept: '.png',
                buttonConfig: {
                    ui: 'administration-secondary-action-small'
                },
                hidden: true,
                bind: {
                    hidden: '{actions.view}'
                }
            }]
        }]
    }]
});