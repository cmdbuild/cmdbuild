Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.properties.fieldsets.XpdlFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-processes-tabitems-properties-fieldsets-xpdlfieldset',
    ui: 'administration-formpagination',
    bind: {
        hidden: '{!actions.edit}'
    },
    items: [{
        xtype: 'fieldset',
        collapsible: true,
        layout: 'column',
        title: 'XPDL file',
        ui: 'administration-formpagination',
        items: [{
            columnWidth: 0.5,
            items: [{
                /********************* XPDL file **********************/
                xtype: 'filefield',
                fieldLabel: 'File',
                reference: 'xpdlFile',
                emptyText: 'Select an XPDL file',
                accept: '.xpdl',
                buttonConfig: {
                    ui: 'administration-secondary-action-small'
                }
            }]
        }]
    }]
});