Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.properties.fieldsets.EngineFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-processes-tabitems-properties-fieldsets-enginefieldset',
    ui: 'administration-formpagination',
    items: [{
        xtype: 'fieldset',
        collapsible: true,
        layout: 'column',
        title: 'Engine', // TODO: translation
        ui: 'administration-formpagination',
        items: [{
            columnWidth: 0.5,
            items: [{
                /********************* Category Lookup **********************/
                xtype: 'combobox',
                queryMode: 'local',
                displayField: 'label',
                valueField: 'value',
                fieldLabel: 'Engine type *', // TODO: translate
                name: 'engine',
                allowBlank: false,
                hidden: true,
                editable: false,
                bind: {
                    store: '{engineStore}',
                    value: '{theProcess.engine}',
                    hidden: '{actions.view}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Engine type', // TODO: translate
                name: 'engine',
                hidden: true,
                bind: {
                    value: '{currentEngine.label}',
                    hidden: '{!actions.view}'
                }
            }]
        }]
    }]
});