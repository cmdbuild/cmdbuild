
Ext.define('CMDBuildUI.view.administration.content.processes.tabitems.properties.fieldsets.ProcessParamentersFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-processes-tabitems-properties-fieldsets-processparametersfieldset',
    viewModel: {},
    bind: {
        hidden: '{actions.add}'
    },
    items: [{
        xtype: 'fieldset',
        layout: 'column',
        title: 'Process Parameters', // TODO: translate
        ui: 'administration-formpagination',
        collapsible: true,
        items: [{
            columnWidth: 0.5,
            items: [{
                /********************* description **********************/
                // create / edit
                xtype: 'combobox',
                editable: false,
                reference: 'stateAttribute',
                displayField: 'description',
                valueField: '_id',
                name: 'stateAttribute',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.processParameter.inputs.stateAttribute.label,
                hidden: true,
                bind: {
                    store: '{allLookupAttributesStore}',
                    value: '{theProcess.stateAttribute}',
                    hidden: '{actions.view}'
                }
            }, {
                // view
                xtype: 'displayfield',
                name: 'stateAttribute',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.processParameter.inputs.stateAttribute.label,
                hidden: true,
                bind: {
                    value: '{theProcess.stateAttribute}', // TODO: this work when #619 is fixed
                    hidden: '{!actions.view}'
                }
            }, {
                /********************* Default Filter **********************/
                // create / edit
                xtype: 'combobox',
                editable: false,
                reference: 'defaultFilter',
                displayField: 'name',
                valueField: '_id',
                name: 'defaultFilter',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.processParameter.inputs.defaultFilter.label,
                hidden: true,
                bind: {
                    store: '{defaultFilterStore}',
                    value: '{theProcess.defaultFilter}',
                    hidden: '{actions.view}'
                }

            }, {
                // view
                xtype: 'displayfield',
                name: 'defaultFilter',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.processParameter.inputs.defaultFilter.label,
                hidden: true,
                bind: {
                    value: '{theProcess.defaultFilter}', // TODO: this work when #619 is fixed
                    hidden: '{!actions.view}'
                }
            }]
        }, {
            columnWidth: 0.5,
            style: {
                paddingLeft: '15px'
            },
            items: [{
                /********************* description **********************/
                // create / edit
                xtype: 'combobox',
                editable: false,
                reference: 'messageAttribute',
                displayField: 'description',
                valueField: '_id',
                name: 'messageAttribute',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.processParameter.inputs.messageAttribute.label,
                hidden: true,
                bind: {
                    store: '{allAttributesStore}',
                    value: '{theProcess.messageAttribute}',
                    hidden: '{actions.view}'
                }
            }, {
                // view
                xtype: 'displayfield',
                name: 'messageAttribute',
                fieldLabel: CMDBuildUI.locales.Locales.administration.processes.properties.form.fieldsets.processParameter.inputs.messageAttribute.label,
                hidden: true,
                bind: {
                    value: '{theProcess.messageAttribute}',  // TODO: this work when #619 #621 is fixed
                    hidden: '{!actions.view}'
                }
            }]
        }]
    }]
});
