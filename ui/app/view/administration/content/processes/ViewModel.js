Ext.define('CMDBuildUI.view.administration.content.processes.ViewModel', {
    extend: 'Ext.app.ViewModel',
    requires: [
        'CMDBuildUI.model.processes.Process'
    ],
    alias: 'viewmodel.administration-content-processes-view',
    data: {
        activeTab: 0,
        objectTypeName: null,
        theProcess: null,
        action: null,
        actions: {
            view: true,
            edit: false,
            add: false
        },
        disabledTabs: {
            properties: false,
            attributes: true,
            domains: true,
            tasks: true,
            levels: true,
            geoattributes: true
        },
        toolbarHiddenButtons: {
            edit: true, // action !== view
            delete: true, // action !== view
            enable: true, //action !== view && theProcess.active
            disable: true, // action !== view && !theProcess.active
            version: true // action !== view
        },
        checkboxNoteInlineClosed: {
            disabled: true
        },
        currentEngine: {
            label:'',
            value: ''
        },
        isMultitenantActive: false
    },

    formulas: {
        updateCheckboxState: {
            bind: '{theProcess.noteInline}',
            get: function (data) {
                if (data) {
                    this.set('checkboxNoteInlineClosed.disabled', false);
                } else {
                    this.set('checkboxNoteInlineClosed.disabled', true);
                    this.set('theProcess.noteInlineClosed', false);
                }
            }
        },

        action: {
            bind: '{theProcess}',
            get: function (get) {
                if (this.get('actions.edit')) {
                    return 'EDIT';
                } else if (this.get('actions.add')) {
                    return 'ADD';
                } else {
                    return 'VIEW';
                }
            },
            set: function (value) {
                this.set('actions.view', value === 'VIEW');
                this.set('actions.edit', value === 'EDIT');
                this.set('actions.add', value === 'ADD');
                this.configToolbarButtons();
                this.configDisabledTabs();

            }
        },
        getToolbarButtons: {
            bind: {
                actions: '{actions}',
                active: '{theProcess.active}'
            },
            get: function (get) {
                this.set('disabledTabs.properties', false);
                this.set('disabledTabs.attributes', !get.actions.view);
                this.set('disabledTabs.domains', !get.actions.view);
                this.set('disabledTabs.levels', !get.actions.view);
                this.set('disabledTabs.tasks', !get.actions.view);
                this.set('disabledTabs.geoattributes', !get.actions.view);

                this.set('toolbarHiddenButtons.edit', !get.actions.view);
                this.set('toolbarHiddenButtons.delete', !get.actions.view);
                this.set('toolbarHiddenButtons.enable', !get.actions.view || (get.actions.view && get.active));
                this.set('toolbarHiddenButtons.disable', !get.actions.view || (get.actions.view && !get.active));
                this.set('toolbarHiddenButtons.version', !get.actions.view);
            }
        },
        isSuperProcess: {
            bind: '{theProcess.prototype}',
            get: function (prototype) {
                if (prototype) {
                    return prototype;
                }
            }
        },

        isSimpleProcess: {
            bind: '{theProcess.type}',
            get: function (type) {
                if (type) {
                    return type === 'simple';
                }
            }
        },

        hideParentCombobox: {
            bind: '{theProcess.type}',
            get: function (type) {
                if (type) {
                    return (type === 'simple' || this.get('actions.view') === true) ? true : false;
                }
            }
        },

        hideParentDisplayfield: {
            bind: '{theProcess.type}',
            get: function (type) {
                if (type) {
                    return (type === 'simple' ||
                        (this.get('actions.view') === false && type === 'standard')
                    ) ? true : false;
                }
            }
        },

        getDefaultOrderData: {
            bind: '{theProcess.defaultOrder}',
            get: function (defaultOrder) {
                if (defaultOrder && defaultOrder.length) {
                    return defaultOrder;
                }
                return [];
            }
        },

        processModelName: {
            bind: '{theProcess}',
            get: function (theProcess) {
                if (theProcess) {
                    return 'CMDBuildUI.model.processes.Process';
                }
            }
        },

        lookupTypeModelName: {
            bind: '{theProcess}',
            get: function (theProcess) {
                if (theProcess) {
                    return 'CMDBuildUI.model.lookups.LookupType';
                }
            }
        },

        attributeOrderModelName: {
            bind: '{theProcess}',
            get: function (theProcess) {
                if (theProcess) {
                    return 'CMDBuildUI.model.AttributeOrder';
                }
            }
        },
        attributeProxy: {
            bind: '{theProcess.name}',
            get: function (objectTypeName) {
                if (objectTypeName && !this.get('theProcess').phantom) {
                    return {
                        url: Ext.String.format("/processes/{0}/attributes", objectTypeName),
                        type: 'baseproxy'
                    };
                }
            }
        },

        defaultFilterData: {
            bind: '{theProcess}',
            get: function (theProcess) {
                if (theProcess) {
                    this.set('defaultFilterData._id', theProcess.get('_id'));
                    this.set('defaultFilterData.name', theProcess.get('name'));
                }
            }
        },

        unorderedAttributes: function (get) {
            var theProcess = get('theProcess');
            if (theProcess) {
                var defaultOrder = theProcess.getAssociatedData().defaultOrder;
                return [function (item) {

                    var found = false;
                    for (var field in defaultOrder) {
                        found = item.get('name') === 'tenantId' || item.get('name') === 'Notes' || defaultOrder[field].attribute === item.get('name');
                        if (found) {
                            break;
                        }
                    }
                    return !found;
                }];
            }
        },
        processParentStore: {
            bind: '{theProcess}',
            get: function (action) {
                return Ext.getStore('processes.Processes').filterBy(function (process) {
                    return process.get('prototype') === true;
                });
            }
        },

        attributeModelName: {
            bind: '{theProcess}',
            get: function (theProcess) {
                if (theProcess) {
                    return "CMDBuildUI.model.Attribute";
                }
            }
        },


        allAttributeProxy: {
            bind: '{theProcess}',
            get: function (theProcess) {
                if (theProcess.get('name')) {
                    // var enginesStore = this.getStore('engineStore');
                    // var engine = enginesStore.findRecord('value', theProcess.get('engine'));
                    // this.set('currentEngine', engine.getData());

                    return {
                        url: Ext.String.format("/processes/{0}/attributes", theProcess.get('name')),
                        type: 'baseproxy',
                        extraParams: {
                            limit: 0
                        }
                    };
                } else {

                    return {
                        type: 'memory'
                    };
                }
            }
        },

        getCurrentEngine: {
            bind: '{theProcess}',
            get: function (theProcess) {
                if (theProcess) {
                    var enginesStore = this.getStore('engineStore');
                    if(theProcess.get('engine')){
                        var engine = enginesStore.findRecord('value', theProcess.get('engine'));
                        this.set('currentEngine', engine.getData());
                    }else{
                        this.set('currentEngine', null);
                    }

                }
            }
        },
        
        multitenantMode: {
            get: function (get) {
                var me = this;
                CMDBuildUI.util.administration.helper.ConfigHelper.getConfig().then(
                    function (configs) {
                        var multitenantMode = Ext.Array.findBy(configs, function (item, index) {
                            return item._key === 'org__DOT__cmdbuild__DOT__multitenant__DOT__mode';
                        });
                        me.set('isMultitenantActive', multitenantMode.value !== 'DISABLED');
                    }
                );
            }
        }
    },


    stores: {
        processVersionStore: {
            alias: 'store.processVersion',
            proxy: {
                url: '/processes/{theProcess.name}/versions',
                type: 'baseproxy'
            },
            autoDestroy: true
        },

        defaultOrderStoreNew: {
            model: '{attributeOrderModelName}',
            alias: 'store.attribute-default-order',
            autoLoad: true,
            autoDestroy: true,
            proxy: {
                type: 'memory'
            },
            data: Ext.create('CMDBuildUI.model.AttributeOrder')
        },

        defaultOrderStore: {
            model: 'CMDBuildUI.model.AttributeOrder',
            alias: 'store.attribute-default-order',
            proxy: {
                type: 'memory'
            },
            data: '{getDefaultOrderData}',
            autoDestroy: true
        },
        defaultOrderDirectionsStore: {
            autoLoad: true,
            autoDestroy: true,
            fields: ['value', 'label'],
            proxy: {
                type: 'memory'
            },
            data: [{
                    'value': 'ascending',
                    'label': 'Ascending' // TODO: translate
                },
                {
                    'value': 'descending',
                    'label': 'Descending' // TODO: translate
                }
            ]
        },
        attributesStore: {
            model: "CMDBuildUI.model.Attribute",
            proxy: '{attributeProxy}',
            fields: ['name', 'description'],

            autoLoad: true,
            autoDestroy: true,
            remoteFilter: false
        },

        unorderedAttributesStore: {
            source: '{attributesStore}',
            filters: '{unorderedAttributes}',
            autoDestroy: true
        },
        superprocessesStore: {
            model: '{processModelName}',
            autoDestroy: true,
            autoLoad: true,
            sorters: ['description'],
            filters: [
                function (klass) {
                    return klass.get('prototype') === true;
                }
            ],
            pageSize: 0
        },

        attachmentTypeLookupStore: {
            model: '{lookupTypeModelName}',
            type: 'attachments-categories',
            fields: [{
                name: '_id',
                type: 'string'
            }, {
                name: 'name',
                type: 'string'
            }],

            proxy: {
                url: '/lookup_types/',
                type: 'baseproxy'
            },
            autoLoad: true,
            autoDestroy: true,
            pageSize: 0
        },

        //////////////////////////////////////////////////////

        allAttributesStore: {
            model: '{attributeModelName}',
            proxy: '{allAttributeProxy}',
            autoLoad: true,
            autoDestroy: true,
            pageSize: 0,
            sorters: [{
                property: 'index',
                direction: 'ASC'
            }]
        },

        allLookupAttributesStore: {
            source: '{allAttributesStore}',
            filters: [
                function (item) {
                    return item.data.type === 'lookup';
                }
            ],
            autoDestroy: true
        },

        engineStore: {
            type: 'processes-Engines',
            autoDestroy: true
        },
        multitenantModeStore: {
            type: 'multitenant-multitenantmode'
        }
    },

    configToolbarButtons: function () {
        this.set('disabledTabs.properties', false);
        this.set('toolbarHiddenButtons.edit', !this.get('actions.view'));
        this.set('toolbarHiddenButtons.delete', !this.get('actions.view'));
        this.set('toolbarHiddenButtons.enable', !this.get('actions.view') || (this.get('actions.view') && this.data.theProcess.data.active /*this.get('theProcess.active')*/ ));
        this.set('toolbarHiddenButtons.disable', !this.get('actions.view') || (this.get('actions.view') && !this.data.theProcess.data.active /*!this.get('theProcess.active')*/ ));
        this.set('toolbarHiddenButtons.version', !this.get('actions.view'));

        return true;
    },
    configDisabledTabs: function () {
        this.set('disabledTabs.properties', false);
        this.set('disabledTabs.attributes', !this.get('actions.view'));
        this.set('disabledTabs.domains', !this.get('actions.view'));
        this.set('disabledTabs.levels', !this.get('actions.view'));
        this.set('disabledTabs.tasks', !this.get('actions.view'));
        this.set('disabledTabs.geoattributes', !this.get('actions.view'));
    }
});