(function () {

    var elementId = 'CMDBuildAdministrationContentProcessView';
    Ext.define('CMDBuildUI.view.administration.content.processes.View', {
        extend: 'Ext.container.Container',

        requires: [
            'CMDBuildUI.view.administration.content.processes.ViewController',
            'CMDBuildUI.view.administration.content.processes.ViewModel'
        ],

        alias: 'widget.administration-content-processes-view',
        controller: 'administration-content-processes-view',
        viewModel: {
            type: 'administration-content-processes-view'
        },
        statics: {
            elementId: elementId
        },
        id: elementId,
        config: {
            objectTypeName: null,
            allowFilter: true,
            showAddButton: true,
            action: CMDBuildUI.view.administration.content.processes.TabPanel.formmodes.view,
            title: null
        },
        defaults: {
            textAlign: 'left',
            scrollable: true
        },
        layout: 'border',
        style: 'background-color:#fff',
        items: [
            { xtype: 'administration-content-processes-topbar', region: 'north' },
            { xtype: 'administration-content-processes-tabpanel', region: 'center' }
        ],

        initComponent: function () {
            var vm = this.getViewModel();
            vm.getParent().set('title', 'Processes');
            this.callParent(arguments);
        },
        listeners: {
            afterlayout: function (panel) {
                Ext.GlobalEvents.fireEventArgs("showadministrationcontentmask", [false]);
            }
        }
    });
})();