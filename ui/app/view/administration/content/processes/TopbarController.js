Ext.define('CMDBuildUI.view.administration.content.processes.TopbarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-processes-topbar',

    control:{
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#addprocess':{
            click: 'onAddProcessClick'
        }
    },
    
    onBeforeRender: function(view){
        view.up('administration-content').getViewModel().set('title', 'Processes');
    },
    onAddProcessClick: function(){
        this.redirectTo('administration/processes',true);
        var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
        vm.set('selected', null);
    }
});
