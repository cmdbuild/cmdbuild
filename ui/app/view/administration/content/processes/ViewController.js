Ext.define('CMDBuildUI.view.administration.content.processes.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-processes-view',

    control: {
        '#': {
            bererender: 'onBeforeRender'
        }
    },

    onBeforeRender: function (view) {
        var vm = view.getViewModel();
        vm.set('title', 'Processes');
    }

});
