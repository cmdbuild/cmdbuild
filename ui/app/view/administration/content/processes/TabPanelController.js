Ext.define('CMDBuildUI.view.administration.content.processes.TabPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-processes-tabpanel',

    control: {
        '#': {
            beforerender: "onBeforeRender",
            tabchange: 'onTabChage'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.processes.TabPanel} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();

        var currentTabIndex = 0;

        this.addTab(view, "properties", [{
            xtype: 'administration-content-processes-tabitems-properties-properties',
            objectTypeName: vm.get("objectTypeName"),
            objectId: vm.get("objectId"),
            // viewModel: vm,
            autoScroll: true
        }], 0, { disabled: '{disabledTabs.properties}' });
        
        this.addTab(view, "attributes", [{
            xtype: 'administration-content-processes-tabitems-attributes-attributes',
            objectTypeName: vm.get("objectTypeName"),
            objectId: vm.get("objectId")
        }], 1, { disabled: '{disabledTabs.attributes}' });
        
        this.addTab(view, "domains", [{
            xtype: 'administration-content-processes-tabitems-domains-domains'
        }], 2, { disabled: '{disabledTabs.domains}' });

        this.addTab(view, "tasks", [{
            xtype: 'administration-content-processes-tabitems-tasks-tasks'
        }], 3, { disabled: '{disabledTabs.tasks}' });
        
        this.addTab(view, "levels", [{
            xtype: 'administration-content-processes-tabitems-levels-levels'
        }], 4, { disabled: '{disabledTabs.levels}' });
        
        this.addTab(view, "geo-attributes", [{
            xtype: 'administration-content-processes-tabitems-geoattributes-geoattributes'
        }], 5, { disabled: '{disabledTabs.geoattributes}' });

        vm.set("activeTab", currentTabIndex);
    },

    /**
     * @param {CMDBuildUI.view.administration.content.processes.TabPanel} view
     * @param {Ext.Component} newtab
     * @param {Ext.Component} oldtab
     * @param {Object} eOpts
     */
    onTabChage: function (view, newtab, oldtab, eOpts) {
        var vm = this.getViewModel();
        vm.getParent().set("actionDescription", newtab.tabConfig.tooltip);
        vm.set("activeTab", newtab.tabConfig.tabIndex);
        setTimeout(function () {
            view.updateLayout();
        }, 0);

    },


    onItemCreated: function (record, eOpts) {
        // TODO: reload menu tree store
    },

    /**
     * @param {CMDBuildUI.model.classes.Card} record
     * @param {Object} eOpts
     */
    onItemUpdated: function (record, eOpts) {

        Ext.ComponentQuery.query('processes-cards-grid-grid')[0].fireEventArgs('reload', [record, 'update']);
        this.redirectTo('processes/' + record.getRecordType() + '/cards/' + record.getRecordId(), true);
    },

    /**
     * @param {Object} eOpts
     */
    onCancelCreation: function (eOpts) {

        var detailsWindow = Ext.getCmp('CMDBuildManagementDetailsWindow');
        detailsWindow.fireEvent('closed');
    },

    /**
     * @param {Object} eOpts
     */
    onCancelUpdating: function (eOpts) {

        var detailsWindow = Ext.getCmp('CMDBuildManagementDetailsWindow');
        detailsWindow.fireEvent('closed');
    },

    privates: {
        addTab: function (view, name, items, index, bind) {
            return view.add({
                xtype: "panel",
                items: items,
                reference: name,
                layout: 'fit',

                autoScroll: true,
                padding: 0,
                tabConfig: {
                    //maxHeight:20,
                    ui: 'administration-tab-item',
                    tabIndex: index,
                    title: (CMDBuildUI.locales.Locales.administration.processes[name]) ? CMDBuildUI.locales.Locales.administration.processes[name].title : Ext.util.Format.capitalize(name).replace(/([a-z])([A-Z])/g, '$1 $2'),
                    tooltip: (CMDBuildUI.locales.Locales.administration.processes[name]) ? CMDBuildUI.locales.Locales.administration.processes[name].title : Ext.util.Format.capitalize(name).replace(/([a-z])([A-Z])/g, '$1 $2'),
                    autoEl: {
                        'data-testid': Ext.String.format('administration-process-tab-', name)
                    }
                },
                bind: bind
            });
        }
    }
});
