Ext.define('CMDBuildUI.view.administration.content.domains.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-domains-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function(view){
        var vm = this.getViewModel();
        vm.set('title', 'Domains');
    }
});
