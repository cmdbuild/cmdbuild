Ext.define('CMDBuildUI.view.administration.content.domains.TopbarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-domains-topbar',

    control:{
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#adddomain':{
            click: 'onAddDomainClick'
        }
    },
    onBeforeRender: function(view){
        view.up('administration-content').getViewModel().set('title', 'Domains');
    },
    onAddDomainClick: function(){
    //this.getViewModel().set('action','ADD');
       this.redirectTo('administration/domains',true);
       var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
       vm.set('selected', null);
    }
});
