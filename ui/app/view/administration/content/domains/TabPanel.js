
Ext.define('CMDBuildUI.view.administration.content.domains.TabPanel', {
    extend: 'Ext.tab.Panel',

    alias: 'widget.administration-content-domains-tabpanel',
    controller: 'administration-content-domains-tabpanel',

    requires: [
        'CMDBuildUI.view.administration.content.domains.TabPanelController'
    ],

    statics: {
        formmodes: {
            view: 'VIEW',
            edit: 'EDIT',
            create: 'ADD'
        }
    },
 
    tabPosition: 'top',
    tabRotation: 0,
    cls: 'administration-mainview-tabpanel',
    ui: 'administration-tabandtools',
    scrollable: true,
    forceFit: true,
    layout: 'fit',
    bind: {
        activeTab: '{activeTab}'
    },
    
    // defaults: {
    // //     bodyPadding: 10,
    //      height: 25
    // },
    listeners: {

        itemupdated: 'onItemUpdated',
        cancelcreation: 'onCancelCreation',
        cancelupdating: 'onCancelUpdating'

    }
});
