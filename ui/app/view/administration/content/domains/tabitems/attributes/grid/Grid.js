Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.attributes.grid.Grid', {
    extend: 'CMDBuildUI.view.administration.components.attributes.grid.Grid',

    requires: [
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.grid.GridController',
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.grid.GridModel'
    ],

    alias: 'widget.administration-content-domains-tabitems-attributes-grid-grid',
    controller: 'administration-content-domains-tabitems-attributes-grid-grid',
    viewModel: {
        type: 'administration-content-domains-tabitems-attributes-grid-grid'
    },
    editView: 'administration-content-domains-tabitems-attributes-card-edit'
});