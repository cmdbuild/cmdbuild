Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.View', {
    extend: 'CMDBuildUI.view.administration.components.attributes.actionscontainers.View',

    requires: [
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.ViewController',
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.ViewModel',
        'Ext.layout.*'
    ],

    alias: 'widget.administration-content-domains-tabitems-attributes-card-view',
    controller: 'administration-content-domains-tabitems-attributes-card-view',
    viewModel: {
        type: 'view-administration-content-domains-tabitems-attributes-card-edit'
    }
});