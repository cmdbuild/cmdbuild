Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.EditController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-domains-tabitems-attributes-card-edit',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        this.linkAttribute(view, vm);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        var form = this.getView();
        this.getViewModel().get('theAttribute').save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    ui: 'administration',
                    html: 'Attribute saved correctly.',
                    title: 'Success!',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
                Ext.GlobalEvents.fireEventArgs("attributeupdated", [record]);
                form.up().fireEvent("closed");
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        vm.get("theAttribute").reject(); // discard changes
        this.getView().up().fireEvent("closed");
    },

    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('attributedomain.{0}.description', vm.get('objectTypeName'), vm.get('attributeName'))
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-localization',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px',
                resizable: true,
                draggable: true
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() + 20);
    },

    privates: {
        linkAttribute: function (view, vm) {
            Ext.ClassManager.get('CMDBuildUI.model.Attribute').setProxy({
                type: 'baseproxy',
                url: Ext.String.format('/domains/{0}/attributes/', vm.get('objectTypeName'))
            });
            vm.linkTo("theAttribute", {
                type: 'CMDBuildUI.model.Attribute',
                id: vm.get('attributeName')
            });
        }
    }

});