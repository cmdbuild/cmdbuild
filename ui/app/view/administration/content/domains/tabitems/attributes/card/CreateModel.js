Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.CreateModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.view-administration-content-domains-tabitems-attributes-card-create',

    bind: {
        theAttribute: null
    },
    formulas: {
        title: function (get) {
            return this.getView().getObjectTypeName();
        }
    }
});