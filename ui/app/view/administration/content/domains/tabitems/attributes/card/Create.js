Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.Create', {
    extend: 'CMDBuildUI.view.administration.components.attributes.actionscontainers.Create',

    requires: [
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.CreateController',
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.EditModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],

    alias: 'widget.administration-content-domains-tabitems-attributes-card-create',
    controller: 'view-administration-content-domains-tabitems-attributes-card-create',
    viewModel: {
        type: 'view-administration-content-domains-tabitems-attributes-card-edit'
    }
});