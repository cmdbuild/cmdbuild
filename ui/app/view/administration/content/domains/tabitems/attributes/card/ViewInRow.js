Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.ViewInRow', {
    extend: 'CMDBuildUI.view.administration.components.attributes.actionscontainers.ViewInRow',

    requires: [
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.ViewInRowController',
        'CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.ViewInRowModel',
        'Ext.layout.*'
    ],

    alias: 'widget.administration-content-domains-tabitems-attributes-card-viewinrow',
    controller: 'administration-content-domains-tabitems-attributes-card-viewinrow',
    viewModel: {
        type: 'view-administration-content-domains-tabitems-attributes-card-edit'
    }
});