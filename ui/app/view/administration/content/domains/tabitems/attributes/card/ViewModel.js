Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.attributes.card.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-domains-tabitems-attributes-card-view',

    data: {
        theAttribute: null
    },
    stores: {
        lookupStore: {
            model: "CMDBuildUI.model.lookups.Lookup",
            proxy: {
                type: 'baseproxy',
                url: '/lookup_types/'
            },
            autoLoad: true,
            autoDestroy: true,
            fields: ['_id', 'name'],
            pageSize: 0
        }
    },


    formulas: {
        title: function (get) {
            return null;
        },
        panelTitle: function (get) {
            var title = Ext.String.format(
                '{0} - {1} - {2}',
                this.get('objectTypeName'),
                'Attributes', // TODO: translate
                this.get('attributeName')
            );
            this.getParent().set('title', title);
        },
        getDomainValue: {
            bind: '{theAttribute}',
            get: function (theAttribute) {
                if (theAttribute) {
                    return this.get('theAttribute.metadata.cm_domain');
                }
            },
            set: function (domain) {
                this.get('theAttribute.metadata').cm_domain = domain;
            }
        },
        preselectIfUnique: {
            bind: '{theAttribute}',
            get: function (theAttribute) {
                if (theAttribute) {
                    return this.get('theAttribute.metadata.cm_preselectIfUnique');
                }
            },
            set: function (value) {
                this.get('theAttribute.metadata').cm_preselectIfUnique = value;
            }
        }
    }
});