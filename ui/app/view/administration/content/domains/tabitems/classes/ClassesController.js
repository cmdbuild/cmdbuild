Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.domains.DomainsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-domains-tabitems-domains-domains',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },
    onBeforeRender: function (view) {
        var vm = view.getViewModel();
        var theObject = vm.get('theDomain');


        var classStore = Ext.getStore('classes.Classes');
        var processStore = Ext.getStore('processes.Processes');

        // source can be class or process
        var sourceTreeStore = (!theObject.get('sourceProcess')) ?
            classStore.getById(theObject.get('source')) :
            processStore.getById(theObject.get('source'));

        // set source checkbox checked or not
        var sourceTree = sourceTreeStore.getChildrenAsTree(true, function (item) {
            item.set('enabled', theObject.get('disabledSourceDescendants').indexOf(item.get('name')) == -1);
            return item;
        });

        // generate the source tree
        this.getViewModel().get('originStore').setRoot({
            expanded: true,
            text: theObject.get('source'),
            name: theObject.get('name'),
            leaf: !sourceTree.length ? true : false,
            children: sourceTree
        });

        // destination can be domain or process
        var destinationTreeStore = (!theObject.get('destinationProcess')) ?
            classStore.getById(theObject.get('destination')) :
            processStore.getById(theObject.get('destination'));

        // set destination checkbox checked or not
        var destinationTree = destinationTreeStore.getChildrenAsTree(true, function (item) {
            item.set('enabled', theObject.get('disabledDestinationDescendants').indexOf(item.get('name')) == -1);
            return item;
        });

        // generate the destination tree
        this.getViewModel().get('destinationStore').setRoot({
            expanded: true,
            text: theObject.get('destination'),
            name: theObject.get('name'),
            leaf: !destinationTree.length ? true : false,
            children: destinationTree
        });

        vm.set('action', vm.get('action'));
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEditBtnClick: function (button, e, eOpts) {
        this.getViewModel().set('action', CMDBuildUI.view.administration.content.domains.TabPanel.formmodes.edit);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var vm = button.up().up().getViewModel();
        var theObject = vm.get('theDomain');
        theObject.save({
            success: function (batch, options) {}
        });
        this.getViewModel().set('action', CMDBuildUI.view.administration.content.domains.TabPanel.formmodes.view);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        var vm = button.up().up().getViewModel();
        var theObject = vm.get('theDomain');
        theObject.reject();
        this.getViewModel().set('action', CMDBuildUI.view.administration.content.domains.TabPanel.formmodes.view);
    }

});