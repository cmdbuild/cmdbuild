Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.domains.DomainsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-domains-tabitems-domains-domains',

    data: {
        actions: {
            view: false,
            edit: false,
            add: false
        },
        toolbarHiddenButtons: {
            edit: true, // action !== view
            print: true // action !== view
        }
    },

    formulas: {
        action: {
            bind: '{theObject}',
            get: function (get) {
                if (this.get('actions.edit')) {
                    return 'EDIT';
                } else if (this.get('actions.add')) {
                    return 'ADD';
                } else {
                    return 'VIEW';
                }
            },
            set: function (value) {
                this.set('actions.view', value === 'VIEW');
                this.set('actions.edit', value === 'EDIT');
                this.set('actions.add', value === 'ADD');
            }
        },
        getToolbarButtons: {
            bind: '{theObject.active}',
            get: function (get) {
                this.set('toolbarHiddenButtons.edit', !this.get('actions.view'));
                this.set('toolbarHiddenButtons.print', !this.get('actions.view'));
            }
        }
    },

    stores: {
        originStore: {
            type: 'tree',
            proxy: {
                type: 'memory'
            },
            fields: ['description','enabled'],
            root: {
                expanded: true
            },
            autoDestroy: true
        },
        destinationStore: {
            type: 'tree',
            proxy: {
                type: 'memory'
            },
            fields: ['description','enabled'],
            root: {
                expanded: true
            },
            autoDestroy: true
        }
    }
});
