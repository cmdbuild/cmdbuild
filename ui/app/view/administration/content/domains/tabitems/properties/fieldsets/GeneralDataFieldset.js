
Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.properties.fieldsets.GeneralDataFieldset', {
    extend: 'Ext.panel.Panel',
    requires: [
        'CMDBuildUI.view.administration.content.domains.tabitems.properties.fieldsets.GeneralDataFieldsetController',
        'CMDBuildUI.view.administration.content.domains.tabitems.properties.fieldsets.GeneralDataFieldsetModel'
    ],

    alias: 'widget.administration-content-domains-tabitems-properties-fieldsets-generaldatafieldset',

    controller: 'administration-content-domains-tabitems-properties-fieldsets-generaldatafieldset',
    viewModel: {
        type: 'administration-content-domains-tabitems-properties-fieldsets-generaldatafieldset'
    },
    ui: 'administration-formpagination',

    items: [{
        xtype: 'fieldset',
        title: "General Attributes",
        layout: 'anchor',
        
        // collapsible: true,
        ui: 'administration-formpagination',
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* Name **********************/
                items: [{
                    // create / edit
                    xtype: 'textfield',
                    vtype: 'alphanum',
                    reference: 'domainname',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.name.label + ' *',
                    name: 'name',
                    enforceMaxLength: true,
                    hidden: true,
                    alowBlank: false,
                    bind: {
                        value: '{theDomain.name}',
                        hidden: '{!actions.add}'
                    }
                }, {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.name.label,
                    name: 'name',
                    hidden: true,
                    bind: {
                        value: '{theDomain.name}',
                        hidden: '{actions.add}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                /********************* description **********************/
                items: [{
                    // create / edit
                    xtype: 'textfield',
                    reference: 'description',
                    displayField: 'description',
                    valueField: 'name',
                    name: 'parent',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.description.label,
                    hidden: true,
                    bind: {
                        value: '{theDomain.description}',
                        hidden: '{actions.view}'
                    }
                }, {
                    // view
                    xtype: 'displayfield',
                    name: 'description',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.description.label,
                    hidden: true,
                    bind: {
                        value: '{theDomain.description}',
                        hidden: '{!actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* Source **********************/
                items: [{
                    // create
                    xtype: 'combobox',
                    displayField: 'description',
                    queryMode: 'local',
                    forceSelection: true,
                    valueField: 'name',
                    fieldLabel: 'Source *', // TODO: translate
                    allowBlank: false,
                    name: 'source',
                    hidden: true,
                    bind: {
                        store: '{allStandardClassesStore}',
                        value: '{theDomain.source}',
                        hidden: '{!actions.add}'
                    }
                }, {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: 'Source', // TODO: translate
                    name: 'source',
                    hidden: true,
                    bind: {
                        value: '{theDomain.source}',
                        hidden: '{actions.add}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                /********************* destination **********************/
                items: [{
                    // create / edit
                    xtype: 'combobox',
                    displayField: 'description',
                    queryMode: 'local',
                    forceSelection: true,
                    valueField: 'name',
                    fieldLabel: 'Destination *', // TODO: translate
                    name: 'destination',
                    allowBlank: false,
                    hidden: true,
                    bind: {
                        store: '{allStandardClassesStore}',
                        value: '{theDomain.destination}',
                        hidden: '{!actions.add}'
                    }
                }, {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: 'Destination', // TODO: translate
                    name: 'destination',
                    hidden: true,
                    bind: {
                        value: '{theDomain.destination}',
                        hidden: '{actions.add}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* direct description **********************/
                items: [
                
            /*    {
                    // create / edit
                    xtype: 'textfield',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.descriptionDirect.label + '*',
                    name: 'descriptionDirect',
                    allowBlank: false,
                    hidden: true,
                    bind: {
                        value: '{theDomain.descriptionDirect}',
                        hidden: '{actions.view}'
                    }
                }*/
                {
                    xtype: 'container',
                    fieldLabel: 'Description',
                    layout: 'hbox',
                    bind: {
                        hidden: '{actions.view}'
                    },
                    items: [{
                        flex: 5,
                        fieldLabel: 'Direct Description',
                        xtype: 'textfield',
                        name: 'descriptionDirect',
                        bind: {
                            value: '{theDomain.descriptionDirect}'
                        }
                    }, {
                        xtype: 'fieldcontainer',
                        flex: 1,
                        fieldLabel: '&nbsp;',
                        labelSeparator: '',
                        items: [{
                            xtype: 'button',
                            reference: 'translateBtn',
                            iconCls: 'x-fa fa-flag fa-2x',
                            style: 'padding-left: 5px',
    
                            tooltip: 'Translate',
                            config: {
                                theSetup: null
                            },
                            viewModel: {
                                data: {
                                    theDomain: null,
                                    vmKey: 'theDomain'
                                }
                            },
                            bind: {
                                theDomain: '{theDomain}'
                            },
                            listeners: {
                                click: 'onTranslateClickDirect'
                            },
                            cls: 'administration-field-container-btn',
                            autoEl: {
                                'data-testid': 'administration-card-edit-translateBtn'
                            }
                        }]
    
                    }]
                }, {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.descriptionDirect.label,
                    name: 'descriptionDirect',
                    hidden: true,
                    bind: {
                        value: '{theDomain.descriptionDirect}',
                        hidden: '{!actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                /********************* inverse description **********************/
                items: [
/*                    {
                    // create / edit
                    xtype: 'textfield',
                    name: 'descriptionInverse',
                    allowBlank: false,
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.descriptionInverse.label + '*',
                    hidden: true,
                    bind: {
                        value: '{theDomain.descriptionInverse}',
                        hidden: '{actions.view}'
                    }
                }*/
                {
                    xtype: 'container',
                    fieldLabel: 'Description',
                    layout: 'hbox',
                    bind: {
                        hidden: '{actions.view}'
                    },
                    items: [{
                        flex: 5,
                        fieldLabel: 'Invarse Description',
                        xtype: 'textfield',
                        name: 'descriptionInverse',
                        bind: {
                            value: '{theDomain.descriptionInverse}'
                        }
                    }, {
                        xtype: 'fieldcontainer',
                        flex: 1,
                        fieldLabel: '&nbsp;',
                        labelSeparator: '',
                        items: [{
                            xtype: 'button',
                            reference: 'translateBtn',
                            iconCls: 'x-fa fa-flag fa-2x',
                            style: 'padding-left: 5px',
    
                            tooltip: 'Translate',
                            config: {
                                theSetup: null
                            },
                            viewModel: {
                                data: {
                                    theDomain: null,
                                    vmKey: 'theDomain'
                                }
                            },
                            bind: {
                                theDomain: '{theDomain}'
                            },
                            listeners: {
                                click: 'onTranslateClickInverse'
                            },
                            cls: 'administration-field-container-btn',
                            autoEl: {
                                'data-testid': 'administration-lookupvalue-card-edit-translateBtn'
                            }
                        }]
    
                    }]
                }, {
                    // view
                    xtype: 'displayfield',
                    name: 'descriptionInverse',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.domains.properties.form.fieldsets.generalData.inputs.descriptionInverse.label,
                    hidden: true,
                    bind: {
                        value: '{theDomain.descriptionInverse}',
                        hidden: '{!actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* description inverse **********************/
                items: [{
                    // create
                    xtype: 'combobox',
                    queryMode: 'local',
                    forceSelection: true,
                    displayField: 'label',
                    valueField: 'value',
                    fieldLabel: 'Cardinality *', // TODO: translate
                    name: 'cardinality',
                    allowBlank: false,
                    hidden: true,
                    bind: {
                        store: '{cardinalityStore}',
                        value: '{theDomain.cardinality}',
                        hidden: '{!actions.add}'
                    }
                }, {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: 'Cardinality', // TODO: translate
                    name: 'cardinality',
                    hidden: true,
                    bind: {
                        value: '{theDomain.cardinality}',
                        hidden: '{actions.add}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* Master detail **********************/
                items: [{
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Master Detail',
                    name: 'masterDetail',
                    hidden: true,
                    bind: {
                        value: '{theDomain.isMasterDetail}',
                        readOnly: '{actions.view}',
                        hidden: '{!theDomain}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                /********************* Master detail label **********************/
                items: [{
                    // create / edit
                    xtype: 'textfield',
                    name: 'descriptionMasterDetail',
                    fieldLabel: 'Master Detail Label',
                    hidden: true,
                    bind: {
                        value: '{theDomain.descriptionMasterDetail}',
                        hidden: '{descriptionMasterDetailEdit.hidden}'
                    }
                }, {
                    // view
                    xtype: 'displayfield',
                    name: 'descriptionMasterDetail',
                    fieldLabel: 'Master Detail Label',
                    hidden: true,
                    bind: {
                        value: '{theDomain.descriptionMasterDetail}',
                        hidden: '{descriptionMasterDetailDisplay.hidden}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* Inline **********************/
                items: [{
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Inline',
                    name: 'inline',
                    hidden: true,
                    bind: {
                        value: '{theDomain.inline}',
                        readOnly: '{actions.view}',
                        hidden: '{!theDomain}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                /********************* Default closed **********************/
                items: [{
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Default closed ',
                    name: 'defaultClosed',
                    hidden: true,
                    bind: {
                        value: '{theDomain.defaultClosed}',
                        readOnly: '{actions.view}',
                        hidden: '{!theDomain}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* View condition CQL **********************/
                items: [{
                    // create / edit
                    xtype: 'textfield',
                    fieldLabel: 'View Condition (CQL)',
                    name: 'viewConditionCQL',
                    hidden: true,
                    bind: {
                        value: '{theDomain.viewConditionCQL}',
                        hidden: '{actions.view}'
                    }
                }, {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: 'View Condition (CQL)',
                    name: 'viewConditionCQL',
                    hidden: true,
                    bind: {
                        value: '{theDomain.viewConditionCQL}',
                        hidden: '{!actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                /********************* Active **********************/
                items: [{
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Active',
                    name: 'active',
                    hidden: true,
                    bind: {
                        value: '{theDomain.active}',
                        readOnly: '{actions.view}',
                        hidden: '{!theDomain}'
                    }
                }]
            }]
        }]
    }]
});
