Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.properties.fieldsets.GeneralDataFieldsetModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-domains-tabitems-properties-fieldsets-generaldatafieldset',

    stores: {
        cardinalityStore: {
            autoLoad: true,
            autoDestroy: true,
            fields: ['value', 'label'],
            proxy: {
                type: 'memory'
            },
            data: [
                { 'value': '1:1', 'label': '1:1' },
                { 'value': '1:N', 'label': '1:N' },
                { 'value': 'N:1', 'label': 'N:1' },
                { 'value': 'N:N', 'label': 'N:N' }
            ]
        },
        allStandardClassesStore: {
            model: 'CMDBuildUI.model.classes.Class',
            autoDestroy: true,
            autoLoad: true,
            sorters: ['description'],
            filters: [
                function (klass) {                    
                    return klass.get('type') === 'standard' && klass.get('name') !== 'Class';
                }
            ],
            pageSize: 0
        },

        // TODO: check if used
        descriptionMasterDetail: {
            hidden: true
        }
    },

    formulas: {
        updateMasterDetail: {
            bind: {
                isMasterDetail: '{theDomain.isMasterDetail}',
                isView: '{actions.view}'
            },        
            get: function (data) {
                if (data.isMasterDetail) {
                    if (data.isView) {
                        this.set('descriptionMasterDetailEdit.hidden', true);
                        this.set('descriptionMasterDetailDisplay.hidden', false);
                    } else {
                        this.set('descriptionMasterDetailEdit.hidden', false);
                        this.set('descriptionMasterDetailDisplay.hidden', true);
                    }
                } else { 
                    this.set('descriptionMasterDetailEdit.hidden', true);
                    this.set('descriptionMasterDetailDisplay.hidden', true);
                }
            }
        }
    }
});
