Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.properties.Properties', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.domains.tabitems.properties.PropertiesController'
    ],

    alias: 'widget.administration-content-domains-tabitems-properties-properties',
    controller: 'administration-content-domains-tabitems-properties-properties',
    viewModel: {},
    autoScroll: false,
    modelValidation: true,
    fieldDefaults: {
        labelAlign: 'top',
        width: '100%'
    },
    layout: 'border',
    items: [{
            xtype: 'components-administration-toolbars-formtoolbar',
            region: 'north',
            items: [{
                    xtype: 'button',
                    itemId: 'spacer',
                    style: {
                        "visibility": "hidden"
                    }
                }, {
                    xtype: 'tbfill'
                }, {
                    xtype: 'tool',
                    align: 'right',
                    itemId: 'editBtn',
                    cls: 'administration-tool',
                    iconCls: 'x-fa fa-pencil',
                    tooltip: CMDBuildUI.locales.Locales.administration.common.actions.edit,
                    localized: {
                        tooltip: 'CMDBuildUI.locales.Locales.administration.common.actions.edit'
                    },
                    callback: 'onEditBtnClick',
                    hidden: true,
                    autoEl: {
                        'data-testid': 'administration-domain-properties-tool-editbtn'
                    },
                    bind: {
                        hidden: '{!actions.view}'
                    }
                }, {

                    xtype: 'tool',
                    align: 'right',
                    itemId: 'deleteBtn',
                    cls: 'administration-tool',
                    iconCls: 'x-fa fa-trash',
                    tooltip: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.deleteBtn.tooltip,
                    callback: 'onDeleteBtnClick',
                    hidden: true,
                    autoEl: {
                        'data-testid': 'administration-domain-properties-tool-deletebtn'
                    },
                    bind: {
                        hidden: '{toolbarHiddenButtons.delete}'
                    }
                }, {
                    xtype: 'tool',
                    align: 'right',
                    itemId: 'disableBtn',
                    cls: 'administration-tool',
                    iconCls: 'x-fa fa-ban',
                    tooltip: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.disableBtn.tooltip,
                    callback: 'onDisableBtnClick',
                    hidden: true,
                    autoEl: {
                        'data-testid': 'administration-domain-properties-tool-disablebtn'
                    },
                    bind: {
                        hidden: '{toolbarHiddenButtons.disable}'
                    }
                }, {

                    xtype: 'tool',
                    align: 'right',
                    itemId: 'enableBtn',
                    hidden: true,
                    cls: 'administration-tool',
                    iconCls: 'x-fa fa-check-circle-o',
                    tooltip: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.enableBtn.tooltip,
                    callback: 'onEnableBtnClick',
                    autoEl: {
                        'data-testid': 'administration-domain-properties-tool-enablebtn'
                    },
                    bind: {
                        hidden: '{toolbarHiddenButtons.enable}'
                    }
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'center',
            scrollable: 'y',
            items: [{
                xtype: 'administration-content-domains-tabitems-properties-fieldsets-generaldatafieldset'
            }]
        }
    ],
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        ui: 'footer',
        hidden: true,
        bind: {
            hidden: '{actions.view}'
        },
        items: [{
            xtype: 'component',
            flex: 1
        }, {
            text: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.saveBtn,
            formBind: true, //only enabled once the form is valid
            disabled: true,
            ui: 'administration-action-small',
            listeners: {
                click: 'onSaveBtnClick'
            }
        }, {
            text: CMDBuildUI.locales.Locales.administration.domains.properties.toolbar.cancelBtn,
            ui: 'administration-secondary-action-small',
            listeners: {
                click: 'onCancelBtnClick'
            }
        }]
    }]

});