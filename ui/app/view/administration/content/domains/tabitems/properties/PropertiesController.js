Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.properties.PropertiesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-domains-tabitems-properties-properties',
    viewModel: {},

    require: [
        'CMDBuildUI.util.administration.helper.FormHelper'
    ],

    beforeRender: function (view) {
        var vm = view.getViewModel();
        var theDomain = vm.get('theDomain');
        if (!theDomain) {
            vm.linkTo('theDomain', {
                type: 'CMDBuildUI.model.domains.Domain',
                id: vm.get('objectTypeName')
            });
        }
        
    },
    
    onEditBtnClick: function (button) {
        this.getViewModel().set('actionManager', CMDBuildUI.view.administration.content.domains.TabPanel.formmodes.edit);

    },

    onDeleteBtnClick: function (button) {
        var vm = this.getViewModel();
        Ext.Msg.confirm(
            "Delete domain", // TODO: translate
            "Are you sure you want to delete this domain?", // TODO: translate
            function (action) {
                if (action === CMDBuildUI.locales.Locales.administration.common.yes) {
                    var theDomain = vm.get('theDomain');
                    CMDBuildUI.util.Ajax.setActionId('delete-domain');

                    theDomain.erase({
                        success: function (record, operation) {
                            var response = operation.getResponse();
                            var w = Ext.create('Ext.window.Toast', {
                                // Ex: "INFO: drop cascades to table "AssetTmpl_history""
                                html: Ext.JSON.decode(response.responseText).message || 'Domain successfully deleted.', // TODO: translate
                                title: 'Success',// TODO: translate
                                iconCls: 'x-fa fa-check-circle"',
                                align: 'br',
                                autoClose: true,
                                maxWidth: 300,
                                monitorResize: true,
                                closable: true
                            });
                            w.show();
                            theDomain.commit();
                        }
                    });
                }
            } );
    },

    onPrintMenuItemClick: function (menuItem) {
        var url,
            objectTypeName = this.view.getViewModel().get('theDomain').get('name');
        switch (menuItem.fileType) {
            case 'PDF':
                url = CMDBuildUI.util.api.Domains.getDomainReport('PDF', objectTypeName);
                break;
            case 'ODT':
                url = CMDBuildUI.util.api.Domains.getDomainReport('ODT', objectTypeName);
                break;
            default:
                Ext.Msg.alert('Warning', 'File type of report not implemented!');
        }
        window.open(url, '_blank');
    },

    onDisableBtnClick: function () {
        var vm = this.getViewModel();
        var theObj = vm.get('theDomain');
        Ext.apply(theObj.data, theObj.getAssociatedData());
        theObj.set('active', false);
        theObj.save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: 'Domain deactivated correctly.',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();

             
                theObj.commit();
            }
        });
    },

    onEnableBtnClick: function () {
        var vm = this.getViewModel();
        var theObj = vm.get('theDomain');
        Ext.apply(theObj.data, theObj.getAssociatedData());
        theObj.set('active', true);
        theObj.save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: 'Domain activated correctly.',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();                
                theObj.commit();
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var me = this;
        button.setDisabled(true);
        var vm = this.getViewModel();
       
        if (!vm.get('theDomain').isValid()) {
            var validatorResult = vm.get('theDomain').validate();
            var errors = validatorResult.items;
            for (var i = 0; i < errors.length; i++) {
                console.log('Key :' + errors[i].field + ' , Message :' + errors[i].msg);
            }
        } else {
            var theDomain = vm.get('theDomain');
            delete theObj.data.system;
            // save the domain
            theDomain.save({
                success: function (record, operation) {
                    var objectTypeName = record.getId();
                    var nextUrl = Ext.String.format('administration/domains/{0}', objectTypeName);
                    CMDBuildUI.util.administration.MenuStoreBuilder.initialize(
                        function () {
                            var treestore = Ext.getCmp('administrationNavigationTree');
                            var selected = treestore.getStore().findNode("href", nextUrl);
                            treestore.setSelection(selected);
                        });
                    me.redirectTo(nextUrl, true);
                }
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        if(this.getViewModel().get('actions.edit')){
            this.redirectTo(Ext.String.format('administration/domains/{0}', this.getViewModel().get('theDomain._id')), true);
        }else if(this.getViewModel().get('actions.add')){
            var store = Ext.getStore('administration.MenuAdministration');
            var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
            var currentNode = store.findNode("objecttype", CMDBuildUI.model.administration.MenuItem.types.domain);
            vm.set('selected', currentNode);
            this.redirectTo('administration/domains_empty', true);
        }

        
    }
});
