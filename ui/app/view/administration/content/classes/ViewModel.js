Ext.define('CMDBuildUI.view.administration.content.classes.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-classes-view',
    data: {
        activeTab: 0,
        objectTypeName: null,
        theObject: null,
        action: null,
        actions: {
            view: true,
            edit: false,
            add: false
        },
        disabledTabs: {
            properties: false,
            attributes: true,
            domains: true,
            levels: true,
            geoattributes: true
        },
        toolbarHiddenButtons: {
            edit: true, // action !== view
            delete: true, // action !== view
            enable: true, //action !== view && theObject.active
            disable: true, // action !== view && !theObject.active
            print: true // action !== view
        },
        checkboxNoteInlineClosed: {
            disabled: true
        },
        isMultitenantActive: false
    },

    formulas: {
        updateCheckboxState: {
            bind: '{theObject.noteInline}',
            get: function (data) {
                if (data) {
                    this.set('checkboxNoteInlineClosed.disabled', false);
                } else {
                    this.set('checkboxNoteInlineClosed.disabled', true);
                    this.set('theObject.noteInlineClosed', false);
                }
            }
        },

        action: {
            bind: '{theObject}',
            get: function (get) {
                if (this.get('actions.edit')) {
                    // this.set('action', 'EDIT');
                    return 'EDIT';
                } else if (this.get('actions.add')) {
                    // this.set('action', 'ADD');
                    return 'ADD';
                } else {
                    // this.set('action', 'VIEW');
                    return 'VIEW';
                }
            },
            set: function (value) {
                this.set('actions.view', value === 'VIEW');
                this.set('actions.edit', value === 'EDIT');
                this.set('actions.add', value === 'ADD');
                this.configToolbarButtons();
                this.configDisabledTabs();

            }
        },
        getToolbarButtons: {
            bind: {
                actions: '{actions}',
                active: '{theObject.active}'
            },
            get: function (get) {
                this.set('disabledTabs.properties', false);
                this.set('disabledTabs.attributes', !get.actions.view);
                this.set('disabledTabs.domains', !get.actions.view);
                this.set('disabledTabs.levels', !get.actions.view);
                this.set('disabledTabs.geoattributes', !get.actions.view);

                this.set('toolbarHiddenButtons.edit', !get.actions.view);
                this.set('toolbarHiddenButtons.delete', !get.actions.view);
                this.set('toolbarHiddenButtons.enable', !get.actions.view || (get.actions.view && get.active /*this.get('theObject.active')*/ ));
                this.set('toolbarHiddenButtons.disable', !get.actions.view || (get.actions.view && !get.active /*!this.get('theObject.active')*/ ));
                this.set('toolbarHiddenButtons.print', !get.actions.view);
            }
        },
        isSuperClass: {
            bind: '{theObject.prototype}',
            get: function (prototype) {
                if (prototype) {
                    return prototype;
                }
            }
        },
        formTriggerCount: {
            bind: '{theObject.formTriggers}',
            get: function (formTriggers) {
                if (formTriggers) {
                    return formTriggers.data.items.length;
                }
                return 0;
            }
        },
        formWidgetCount: {
            bind: '{theObject.widgets}',
            get: function (widgets) {
                if (widgets) {
                    return widgets.data.items.length;
                }
                return 0;
            }
        },
        contextMenuCount: {
            bind: '{theObject.contextMenuItems}',
            get: function (contextMenuItems) {
                if (contextMenuItems) {
                    return contextMenuItems.data.items.length;
                }
                return 0;
            }
        },
        isSimpleClass: {
            bind: '{theObject.type}',
            get: function (type) {
                if (type) {
                    return type === 'simple';
                }
            }
        },

        isStandardClassAndIsViewAction: {
            bind: '{theObject.type}',
            get: function (type) {
                if (type) {
                    return (type === 'standard' && this.get('actions.view') === true) ? true : false;
                }
            }
        },

        hideParentCombobox: {
            bind: '{theObject.type}',
            get: function (type) {
                if (type) {
                    return (type === 'simple' || this.get('actions.view') === true) ? true : false;
                }
            }
        },
        hideParentDisplayfield: {
            bind: '{theObject.type}',
            get: function (type) {
                if (type) {
                    return (type === 'simple' ||
                        (this.get('actions.view') === false && type === 'standard')
                    ) ? true : false;
                }
            }
        },

        getFormTriggersData: {
            bind: '{theObject}',
            get: function (theObject) {
                return (theObject) ? theObject.getAssociatedData().formTriggers : [];
            }
        },
        getContextMenuItemData: {
            bind: '{theObject}',
            get: function (theObject) {
                return (theObject) ? theObject.getAssociatedData().contextMenuItems : [];
            }
        },
        getFormWidgetData: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    return theObject.getAssociatedData().widgets;
                }
                return [];
            }
        },
        getDefaultOrderData: {
            bind: '{theObject.defaultOrder}',
            get: function (defaultOrder) {
                return (defaultOrder) ? defaultOrder.getData().items : [];
            }
        },
        // TODO: remove if not used

        // getSuperclassesData: {
        //     bind: '{theObject}',
        //     get: function (theObject) {
        //         if (theObject) {
        //             var defaultOrder = theObject.getAssociatedData().defaultOrder;
        //             if (this.get('action') === CMDBuildUI.view.administration.content.classes.TabPanel.formmodes.edit) {
        //                 defaultOrder.push(Ext.create('CMDBuildUI.model.AttributeOrder'));
        //             }
        //             return defaultOrder;
        //         }
        //         return [];
        //     }
        // },
        getClassType: {
            bind: '{theObject.type}',
            get: function (type) {
                if (type) {
                    return Ext.util.Format.capitalize(type);
                }
            }
        },
        storeClassName: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    return 'CMDBuildUI.model.classes.Class';
                }
            }
        },
        lookupTypeModelName: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    return 'CMDBuildUI.model.lookups.LookupType';
                }
            }
        },
        contextMenuItemModelName: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    return 'CMDBuildUI.model.ContextMenuItem';
                }
            }
        },

        formTriggerModelName: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    return 'CMDBuildUI.model.FormTrigger';
                }
            }
        },
        formWidgetModelName: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    return 'CMDBuildUI.model.WidgetDefinition';
                }
            }
        },
        attributeModelName: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    return 'CMDBuildUI.model.AttributeOrder';
                }
            }
        },
        attributeProxy: {
            bind: '{theObject.name}',
            get: function (objectTypeName) {
                if (objectTypeName && !this.get('theObject').phantom) {
                    return {
                        url: Ext.String.format("/classes/{0}/attributes", objectTypeName),
                        type: 'baseproxy'
                    };
                }
            }
        },
        defaultFilterProxy: {
            bind: '{theObject}',
            get: function (theObject) {
                if (theObject) {
                    var objectTypeName = theObject.getId();
                    return {
                        url: Ext.String.format("/classes/{0}/filters", objectTypeName),
                        type: 'baseproxy'
                    };
                }
            }
        },
        defaultFilterData: {
            bind: '{theObject}',
            get: function (objectTypeName) {
                if (objectTypeName) {
                    var datas = objectTypeName.getData();
                    this.set('defaultFilterData._id', datas._id);
                    this.set('defaultFilterData.name', datas.name);
                }
            }
        },

        unorderedAttributes: function (get) {
            var theObject = get('theObject');
            var defaultOrder = theObject.getAssociatedData().defaultOrder;
            return [function (item) {

                var found = false;
                for (var field in defaultOrder) {
                    found = item.get('name') === 'tenantId' || item.get('name') === 'Notes' || defaultOrder[field].attribute === item.get('name');
                    if (found) {
                        break;
                    }
                }
                return !found;
            }];
        },
        /**
          unorderedAttributes: {
                    bind: '{theObject}',
                    get: function (theObject) {
                        if (theObject) {
                            //var theObject = get('theObject');
                            var defaultOrder = theObject.getAssociatedData().defaultOrder;
                            return [function (item) {

                                var found = false;
                                for (var field in defaultOrder) {
                                    found = item.get('name') === 'tenantId' || item.get('name') === 'Notes' || defaultOrder[field].attribute === item.get('name');
                                    if (found) {
                                        break;
                                    }
                                }
                                return !found;
                            }];
                        }
                    }
                },
         */
        getParentDescription: {
            bind: {
                superclassesStore: '{superclassesStore}',
                theObject: '{theObject}'
            },
            get: function (data) {

                var me = this;
                var theObject = data.theObject;
                var superclassesStore = data.superclassesStore;
                if (theObject && superclassesStore) {
                    if (me.getStore('superclassesStore')) {
                        return me.getStore('superclassesStore').load({
                            scope: me,
                            callback: function (data) {
                                data.forEach(function (klass, index) {

                                    // TODO: check this
                                    // Uncaught TypeError: Cannot read property 'getChild' of null
                                    //     at constructor.getStub (ext-all-rtl-debug.js?_dc=1543392876548:104619)
                                    //     at constructor.get (ext-all-rtl-debug.js?_dc=1543392876548:104490)
                                    //     at ViewModel.js?_dc=1543392876541:326
                                    //     at Array.forEach (<anonymous>)
                                    //     at constructor.callback (ViewModel.js?_dc=1543392876541:325)
                                    //     at constructor.triggerCallbacks (ext-all-rtl-debug.js?_dc=1543392876548:81367)
                                    //     at constructor.setCompleted (ext-all-rtl-debug.js?_dc=1543392876548:81328)
                                    //     at constructor.setSuccessful (ext-all-rtl-debug.js?_dc=1543392876548:81341)
                                    //     at constructor.process (ext-all-rtl-debug.js?_dc=1543392876548:81240)
                                    //     at constructor.processResponse (ext-all-rtl-debug.js?_dc=1543392876548:90273)
                                    if (klass.get('name') === me.get('theObject').get('parent')) {
                                        me.set('parentDescription', klass.get('description'));
                                    }
                                });
                                return;
                            }
                        });
                    }
                }
            }
        },

        multitenantMode: {
            get: function (get) {
                var me = this;
                CMDBuildUI.util.administration.helper.ConfigHelper.getConfig().then(
                    function (configs) {
                        var multitenantMode = Ext.Array.findBy(configs, function (item, index) {
                            return item._key === 'org__DOT__cmdbuild__DOT__multitenant__DOT__mode';
                        });
                        me.set('isMultitenantActive', multitenantMode.value !== 'DISABLED');
                    }
                );
            }
        }
    },

    stores: {
        defaultOrderStore: {
            model: 'CMDBuildUI.model.AttributeOrder',
            alias: 'store.attribute-default-order',
            proxy: {
                type: 'memory'
            },
            data: '{getDefaultOrderData}',
            autoDestroy: true
        },
        defaultOrderStoreNew: {
            model: '{attributeModelName}',
            alias: 'store.attribute-default-order',
            autoLoad: true,
            autoDestroy: true,
            proxy: {
                type: 'memory'
            },
            data: Ext.create('CMDBuildUI.model.AttributeOrder')
        },
        defaultOrderDirectionsStore: {
            autoLoad: true,
            autoDestroy: true,
            fields: ['value', 'label'],
            proxy: {
                type: 'memory'
            },
            data: [{
                    'value': 'ascending',
                    'label': 'Ascending'
                }, // TODO: translate
                {
                    'value': 'descending',
                    'label': 'Descending'
                } // TODO: translate
            ]
        },
        attributesStore: {
            model: "CMDBuildUI.model.Attribute",
            proxy: '{attributeProxy}',
            fields: ['name', 'description'],

            autoLoad: true,
            autoDestroy: true,
            remoteFilter: false
        },

        unorderedAttributesStore: {
            source: '{attributesStore}',
            filters: '{unorderedAttributes}',
            autoDestroy: true
        },
        superclassesStore: {
            model: '{storeClassName}',
            alias: 'store.superclassesStore',
            autoDestroy: true,
            autoLoad: true,
            sorters: ['description'],
            filters: [
                function (klass) {
                    if (klass.get('name') === 'Class') {
                        klass.set('description', '-- default --');
                    }
                    return klass.get('prototype') === true;
                }
            ],
            pageSize: 0
        },

        formTriggersStore: {
            model: '{formTriggerModelName}',
            proxy: {
                type: 'memory'
            },
            autoLoad: true,
            autoDestroy: true,
            data: '{getFormTriggersData}'
        },
        formTriggersStoreNew: {
            model: '{formTriggerModelName}',
            proxy: {
                type: 'memory'
            },
            autoLoad: true,
            autoDestroy: true,
            data: Ext.create('CMDBuildUI.model.FormTrigger')
        },
        contextMenuItemsStore: {
            model: '{contextMenuItemModelName}',
            proxy: {
                type: 'memory'
            },
            data: '{getContextMenuItemData}',
            autoDestroy: true
        },
        contextMenuItemTypeStore: {
            autoLoad: true,
            fields: ['value', 'label'],
            proxy: {
                type: 'memory'
            },
            autoDestroy: true,
            data: [{
                    'value': 'component',
                    'label': 'Component'
                }, // TODO: translate
                {
                    'value': 'custom',
                    'label': 'Custom'
                }, // TODO: translate
                {
                    'value': 'separator',
                    'label': 'Separator'
                } // TODO: translate
            ]
        },

        contextMenuApplicabilityStore: {
            autoLoad: true,
            fields: ['value', 'label'],
            proxy: {
                type: 'memory'
            },
            autoDestroy: true,
            data: [{
                    'value': 'one',
                    'label': CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.applicability.values.one.label
                },
                {
                    'value': 'many',
                    'label': CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.applicability.values.many.label
                },
                {
                    'value': 'all',
                    'label': CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.applicability.values.all.label
                }
            ]
        },
        formWidgetsStore: {
            model: '{formWidgetModelName}',
            autoDestroy: true,
            proxy: {
                type: 'memory'
            },
            data: '{getFormWidgetData}'
        },
        formWidgetsStoreNew: {
            model: '{formWidgetModelName}',
            autoDestroy: true,
            proxy: {
                type: 'memory'
            },
            data: Ext.create('CMDBuildUI.model.WidgetDefinition')
        },
        formWidgetTypeStore: {
            autoLoad: true,
            autoDestroy: true,
            fields: ['value', 'label'],
            proxy: {
                type: 'memory'
            },
            data: [{
                    'value': 'calendar',
                    'label': 'Calendar'
                }, // TODO: translate
                {
                    'value': 'createModifyCard',
                    'label': 'Create / Modify Card'
                }, // TODO: translate
                {
                    'value': 'createReport',
                    'label': 'Create Report'
                }, // TODO: translate
                {
                    'value': 'ping',
                    'label': 'Ping'
                },
                {
                    'value': 'startWorkflow',
                    'label': 'Start workflow'
                } // TODO: translate
            ]
        },
        contextMenuItemsStoreNew: {
            model: '{contextMenuItemModelName}',
            proxy: {
                type: 'memory'
            },
            data: CMDBuildUI.model.ContextMenuItem.create({}),
            autoDestroy: true
        },
        attachmentTypeLookupStore: {
            model: '{lookupTypeModelName}',
            type: 'attachments-categories',
            fields: [{
                _name: '_id',
                type: 'string'
            }, {
                name: 'name',
                type: 'string'
            }],

            proxy: {
                url: '/lookup_types/',
                type: 'baseproxy'
            },
            autoLoad: true,
            autoDestroy: true,
            pageSize: 0
        },
        defaultFilterStore: {
            proxy: '{defaultFilterProxy}',
            data: '{defaultFilterData}',
            autoLoad: true,
            autoDestroy: true

        },
        classTypeStore: {
            model: 'CMDBuildUI.model.base.ComboItem',
            fields: ['value', 'label'],
            proxy: {
                type: 'memory'
            },
            data: [{
                    'value': 'standard',
                    'label': 'Standard'
                },
                {
                    'value': 'simple',
                    'label': 'Simple'
                } // TODO: translate
            ],
            autoDestroy: true

        },
        allAttributes: {
            model: "CMDBuildUI.model.Attribute",
            proxy: '{allAttributeProxy}',
            autoLoad: true,
            autoDestroy: true,
            pageSize: 0,
            sorters: [{
                property: 'index',
                direction: 'ASC'
            }],
            filters: [
                function (item) {
                    return item.data.name !== 'Notes' && item.data.name !== 'IdTenant';
                }
            ]
        },

        multitenantModeStore: {
           type: 'multitenant-multitenantmode'
        }
    },

    configToolbarButtons: function () {
        this.set('disabledTabs.properties', false);
        this.set('toolbarHiddenButtons.edit', !this.get('actions.view'));
        this.set('toolbarHiddenButtons.delete', !this.get('actions.view'));
        this.set('toolbarHiddenButtons.enable', !this.get('actions.view') || (this.get('actions.view') && this.data.theObject.data.active /*this.get('theObject.active')*/ ));
        this.set('toolbarHiddenButtons.disable', !this.get('actions.view') || (this.get('actions.view') && !this.data.theObject.data.active /*!this.get('theObject.active')*/ ));
        this.set('toolbarHiddenButtons.print', !this.get('actions.view'));

        return true;
    },
    configDisabledTabs: function () {
        this.set('disabledTabs.properties', false);
        this.set('disabledTabs.attributes', !this.get('actions.view'));
        this.set('disabledTabs.domains', !this.get('actions.view'));
        this.set('disabledTabs.levels', !this.get('actions.view'));
        this.set('disabledTabs.geoattributes', !this.get('actions.view'));
    }
});