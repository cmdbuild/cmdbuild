Ext.define('CMDBuildUI.view.administration.content.classes.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-classes-view',

    control: {
        '#': {
            bererender: 'onBeforeRender'
        }
    },

    onBeforeRender: function (view) {
        var vm = view.getViewModel();
        vm.set('title', 'Classes');
    }

});
