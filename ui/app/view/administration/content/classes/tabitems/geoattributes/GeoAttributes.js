
Ext.define('CMDBuildUI.view.administration.content.elements.tabitems.geoattributes.GeoAttributes',{
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.elements.tabitems.geoattributes.GeoAttributesController',
        'CMDBuildUI.view.administration.content.elements.tabitems.geoattributes.GeoAttributesModel'
    ],

    alias: 'widget.administration-content-classes-tabitems-geoattributes-geoattributes',
    controller: 'administration-content-classes-tabitems-geoattributes-geoattributes',
    viewModel: {
        type: 'administration-content-classes-tabitems-geoattributes-geoattributes'
    },

    html: 'To be implemented'
});
