Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.grid.Grid', {
    extend: 'CMDBuildUI.view.administration.components.attributes.grid.Grid',

    requires: [
        'CMDBuildUI.view.administration.content.classes.tabitems.attributes.grid.GridController',
        'CMDBuildUI.view.administration.content.classes.tabitems.attributes.grid.GridModel'
    ],

    alias: 'widget.administration-content-classes-tabitems-attributes-grid-grid',
    controller: 'administration-content-classes-tabitems-attributes-grid-grid',
    viewModel: {
        type: 'administration-content-classes-tabitems-attributes-grid-grid'
    },
    editView: 'administration-content-classes-tabitems-attributes-card-edit'
});