Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.ViewInRowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-classes-tabitems-attributes-card-viewinrow',
    listen: {
        global: {
            selectedclassattribute: 'onSelectedAttribute',
            attributeupdated: 'onAttributeUpdated'
        }
    },

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            itemupdated: 'onAttributeUpdated'

        }
    },

    onSelectedAttribute: function (attribute) {
        this.getViewModel().set('theAttribute', attribute);
    },

    /**
     * @param {CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.ViewInRow} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        this.linkAttribute(view, vm);
    },

    onAttributeUpdated: function (v, record) {
        var vm = this.getViewModel();
        var view = this.getView();
        this.linkAttribute(view, vm);
    },

    linkAttribute: function (view, vm) {
        var objectTypeName = view.getObjectTypeName();
        var objectId = view.getObjectId();
        var config = view.getInitialConfig();
        var theAttributeVM = (this.view.getViewModel().get('theAttribute')) ? this.view.getViewModel().get('theAttribute') : null;
        var theAttributeRecord = config._rowContext.viewModel.get('record');
        if (!objectTypeName && !objectId) {

            if (!Ext.isEmpty(config._rowContext)) {
                var record = theAttributeVM || theAttributeRecord;
                if (record && record.getData()) {

                    var selectedClass = vm.get('theObject');
                    if (selectedClass.getData()) {
                        record.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
                        record.model.setProxy({
                            type: 'baseproxy',
                            url: '/classes/' + vm.get('theObject').getId() + '/attributes/'
                        });
                        vm.set("theAttribute", record);
                    }
                }
            }
        }
    }
});