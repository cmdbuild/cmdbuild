Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.EditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.view-administration-content-classes-tabitems-attributes-card-edit',
    data: {
        groups: [],
        attributes: [],
        theAttribute: null,
        types: {
            isReference: false,
            isLookup: false,
            isDecimal: false,
            isText: false,
            isString: false,
            isIpAddress: false
        }

    },

    formulas: {
        canDelete: {
            bind: {
                isInherited: '{theAttribute.inherited}'
            },
            get: function (data) {
                return (data.isInherited) ? false : true;
            }
        },
        setCurrentType: {
            bind: '{theAttribute.type}',
            get: function (type) {
                this.set('types.isReference', type === 'reference');
                this.set('types.isLookup', type === 'lookup');
                this.set('types.isDecimal', type === 'decimal');
                this.set('types.isText', type === 'text');
                this.set('types.isString', type === 'string');
                this.set('types.isIpAddress', type === 'ipAddress');
            }
        },

        validationRules: {
            bind: '{theAttribute.metadata}',
            get: function (metadata) {
                return this.get('theAttribute.metadata.cm_validationRules');
            },
            set: function (value) {
                this.get('theAttribute.metadata').cm_validationRules = value;
            }
        },

        actionPostValidation: {
            bind: '{theAttribute.metadata}',
            get: function (metadata) {
                return this.get('theAttribute.metadata.cm_actionPostValidation');
            },
            set: function (value) {
                this.get('theAttribute.metadata').cm_actionPostValidation = value;
            }
        },

        showIf: {
            bind: '{theAttribute.metadata}',
            get: function (metadata) {
                return this.get('theAttribute.metadata.cm_showIf');
            },
            set: function (value) {
                this.get('theAttribute.metadata').cm_showIf = value;
            }
        },

        help: {
            bind: '{theAttribute.metadata}',
            get: function (metadata) {
                return this.get('theAttribute.metadata.cm_help');
            },
            set: function (value) {
                this.get('theAttribute.metadata').cm_help = value;
            }
        },
        panelTitle: function (get) {
            var title = Ext.String.format(
                '{0} - {1} - {2}',
                this.get('objectTypeName'),
                'Attributes', // TODO: translate
                this.get('attributeName')
            );
            this.getParent().set('title', title);
        },

        groups: {
            bind: '{attributes}',
            get: function (attributes) {
                var groups = [],
                    data = [];
                Ext.Array.each(attributes, function (attribute) {
                    if (attribute.get('group') && attribute.get('group').length > 0) {
                        if (!Ext.Array.contains(data, attribute.get('group'))) {
                            Ext.Array.include(data, attribute.get('group'));
                            Ext.Array.include(groups, {
                                label: attribute.get('group'),
                                value: attribute.get('group')
                            });
                        }
                    }
                });
                return groups;
            }
        },

        domainExtraparams: {
            bind: '{objectTypeName}',
            get: function (objectTypeName) {
                var filter = JSON.stringify({
                    "attribute": {
                        "or": [{
                            "and": [{
                                "simple": {
                                    "attribute": "destination",
                                    "operator": "equal",
                                    "value": objectTypeName
                                }
                            }, {
                                "simple": {
                                    "attribute": "cardinality",
                                    "operator": "in",
                                    "value": ["1:N", "1:1"]
                                }
                            }]
                        }, {
                            "simple": {
                                "attribute": "source",
                                "operator": "equal",
                                "value": objectTypeName
                            }
                        }]
                    }
                });
                return filter;
            }
        }
    },

    stores: {
        domainsStore: {
            model: "CMDBuildUI.model.domains.Domain",
            proxy: {
                type: 'baseproxy',
                url: '/domains/',
                extraParams: {
                    filter: '{domainExtraparams}'
                }
            },
            autoLoad: true,
            autoDestroy: true,
            fields: ['_id', 'name'],
            pageSize: 0
        },
        lookupStore: {
            model: "CMDBuildUI.model.lookups.Lookup",
            proxy: {
                type: 'baseproxy',
                url: '/lookup_types/'
            },
            autoLoad: true,
            autoDestroy: true,
            fields: ['_id', 'name'],
            pageSize: 0
        },
        attributeGroupStore: {
            model: "CMDBuildUI.model.Attribute",
            proxy: {
                type: 'memory'
            },
            autoDestroy: true,
            data: '{groups}',
            fields: ['label', 'value']
        },
        attributeModeStore: {
            data: [{
                value: "apm_write",
                label: "Editable" // TODO: translate
            }, {
                value: "apm_read",
                label: "Read only" // TODO: translate
            }, {
                value: "apm_hidden",
                label: "Hidden" // TODO: translate
            }, {
                value: "apm_immutable",
                label: "Immutable" // TODO: translate
            }],
            proxy: {
                type: 'memory'
            },
            autoLoad: true,
            autoDestroy: true,
            fields: ['value', 'label']
        },
        editorTypeStore: {
            data: [{
                value: "PLAIN",
                label: "Plain text" // TODO: translate
            }, {
                value: "HTML",
                label: "Editor HTML" // TODO: translate
            }],
            proxy: {
                type: 'memory'
            },
            autoLoad: true,
            autoDestroy: true,
            fields: ['value', 'label']
        },
        ipTypeStore: {
            data: [{
                value: "ipv4",
                label: "IPV4" // TODO: translate
            }, {
                value: "ipv6",
                label: "IPV6" // TODO: translate
            }, {
                value: "any",
                label: "Any" // TODO: translate
            }],

            proxy: {
                type: 'memory'
            },
            autoLoad: true,
            autoDestroy: true,
            fields: ['value', 'label']
        },
        typeStore: {
            data: [{
                label: 'Boolean',
                value: 'boolean'
            }, {
                label: 'Char',
                value: 'char'
            }, {
                label: 'Date',
                value: 'date'
            }, {
                label: 'Decimal',
                value: 'decimal'
            }, {
                label: 'Double',
                value: 'double'
            }, {
                label: 'Integer',
                value: 'integer'
            }, {
                label: 'Ip address',
                value: 'ipAddress'
            }, {
                label: 'Lookup',
                value: 'lookup'
            }, {
                label: 'Reference',
                value: 'reference'
            }, {
                label: 'String',
                value: 'string'
            }, {
                label: 'Text',
                value: 'text'
            }, {
                label: 'Time',
                value: 'time'
            }, {
                label: 'Timestamp',
                value: 'timestamp'
            }],
            proxy: {
                type: 'memory'
            },
            autoLoad: true,
            autoDestroy: true,
            filds: ['value', 'label']
        }
    }
});