Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.Edit', {
    extend: 'CMDBuildUI.view.administration.components.attributes.actionscontainers.Edit',
    alias: 'widget.administration-content-classes-tabitems-attributes-card-edit',

    requires: [
        'CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.EditController',
        'CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.EditModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],
    controller: 'view-administration-content-classes-tabitems-attributes-card-edit',
    viewModel: {
        type: 'view-administration-content-classes-tabitems-attributes-card-edit'
    }
});