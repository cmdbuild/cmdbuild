Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.grid.GridModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-classes-tabitems-attributes-grid-grid',
    data: {
        search: {
            value: null
        },
        selected: null
    },

    formulas: {
        canDelete: {
            bind: {
                isInherited: '{theAttribute.inherited}'
            },
            get: function (data) {
                return (data.isInherited) ? false : true;
            }
        },
        setCurrentType: {
            bind: '{theAttribute.type}',
            get: function (type) {
                this.set('types.isReference', type === 'reference');
                this.set('types.isLookup', type === 'lookup');
                this.set('types.isDecimal', type === 'decimal');
                this.set('types.isText', type === 'text');
                this.set('types.isString', type === 'string');
                this.set('types.isIpAddress', type === 'ipAddress');
            }
        },
        allAtrributesGorups: function (get) {
            var allAttributesStore = get('allAttributesStore');

            var groups = [],
                data = [];

            Ext.Array.each(allAttributesStore, function (attribute) {
                var attributeData = attribute.getData();
                if (attributeData.group && attributeData.group.length > 0) {
                    if (!Ext.Array.contains(groups, attributeData.group)) {
                        Ext.Array.include(groups, attributeData.group);
                        Ext.Array.include(data, {
                            label: attributeData.group,
                            value: attributeData.group
                        });
                    }
                }
            });

            return data;
        },

        isObjectyTypeNameSet: {
            bind: '{theObject.name}',
            get: function (objectTypeName) {
                if (objectTypeName) {
                    return true;
                }
                return false;
            }
        }
    }

});