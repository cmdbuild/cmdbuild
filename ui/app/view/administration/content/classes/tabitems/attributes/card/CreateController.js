Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.CreateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-classes-tabitems-attributes-card-create',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        // set vm varibles
        if (!vm.get('theAttribute')) {
            vm.linkTo("theAttribute", {
                type: 'CMDBuildUI.model.Attribute',
                create: true
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        var form = this.getView();

        if (form.isValid() && vm.get('objectTypeName')) {
            var theAttribute = vm.getData().theAttribute;
            theAttribute.getProxy().setUrl(Ext.String.format('/classes/{0}/attributes', vm.get('objectTypeName')));
            delete theAttribute.data.inherited;
            delete theAttribute.data.writable;
            delete theAttribute.data.hidden;
            
            theAttribute.save({
                success: function (record, operation) {
                    var w = Ext.create('Ext.window.Toast', {
                        ui: 'administration',
                        title: CMDBuildUI.locales.Locales.administration.common.messages.success,
                        html: CMDBuildUI.locales.Locales.administration.common.messages.itemwascreated,
                        iconCls: 'x-fa fa-check-circle',
                        align: 'br'
                    });
                    w.show();
                    Ext.GlobalEvents.fireEventArgs("attributecreated", [record]);
                    
                }
            });
        } else {
            var w = Ext.create('Ext.window.Toast', {
                ui: 'administration',
                html: CMDBuildUI.locales.Locales.administration.common.messages.correctformerrors,
                title: CMDBuildUI.locales.Locales.administration.common.messages.error,
                iconCls: 'x-fa fa-check-circle',
                align: 'br'
            });
            w.show();
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveAndAddBtnClick: function (button, e, eOpts) {
        var me = this;
        var objectTypeName = me.getViewModel().get('objectTypeName');
        
        me.onSaveBtnClick();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-classes-tabitems-attributes-card-create',
            viewModel: {
                data: {
                    objectTypeName: objectTypeName
                }
            }
        });
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        this.getViewModel().get("theAttribute").reject();
        this.getView().up().fireEvent("closed");
    }
});