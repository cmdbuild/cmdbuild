Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.grid.GridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-classes-tabitems-attributes-grid-grid',
    listen: {
        global: {
            attributeupdated: 'onAttributeUpdated',
            attributecreated: 'onAttributeCreated',
            editattributebtnclick: 'onEditAttributeBtnClick'
        }
    },

    control: {
        '#addattribute': {
            click: 'onNewBtnClick'
        },
        tableview: {
            deselect: 'onDeselect',
            select: 'onSelect',
            beforedrop: 'onBeforeDrop'
        }
    },

    onBeforeDrop: function (node, data, overModel, dropPosition, dropHandlers) {
        // Defer the handling
        var vm = this.getViewModel();
        var view = this.getView();

        var filterCollection = vm.get("allAttributes").getFilters();
        view.getView().mask(CMDBuildUI.locales.Locales.administration.common.messages.loading);
        dropHandlers.wait = true;
        // by default allAttributes store have one filter for hide notes and idTenant attributes 
        if (filterCollection.length > 1) {
            var w = Ext.create('Ext.window.Toast', {
                ui: 'administration',
                width: 250,
                title: CMDBuildUI.locales.Locales.administration.common.messages.attention,
                html: CMDBuildUI.locales.Locales.administration.common.messages.cannotsortitems,
                iconCls: 'x-fa fa-exclamation-circle',
                align: 'br'
            });
            w.show();
            dropHandlers.cancelDrop();
            view.getView().unmask();

        } else {

            var moved = data.records[0].getId();
            var reference = overModel.getId();

            var attributes = vm.get('allAttributes').getData().getIndices();
            var sortableAttributes = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    sortableAttributes.push([key, attributes[key]]); // each item is an array in format [key, value]
                }
            }

            // sort items by value
            sortableAttributes.sort(function (a, b) {
                return a[1] - b[1]; // compare numbers
            });

            var jsonData = [];

            Ext.Array.forEach(sortableAttributes, function (val, key) {
                if (moved != val[0]) {
                    if (dropPosition === 'before' && reference === val[0]) {
                        jsonData.push(moved);
                    }
                    jsonData.push(val[0]);
                    if (dropPosition === 'after' && reference === val[0]) {
                        jsonData.push(moved);
                    }
                }
            });

            Ext.Ajax.request({
                url: Ext.String.format(
                    '{0}/classes/{1}/attributes/order',
                    CMDBuildUI.util.Config.baseUrl,
                    view.getViewModel().get('theObject').get('name')
                ),
                method: 'POST',
                jsonData: jsonData,
                success: function (response) {
                    var res = JSON.parse(response.responseText);
                    if (res.success) {
                        view.getView().grid.getStore().load();
                        dropHandlers.processDrop();
                    } else {
                        dropHandlers.cancelDrop();
                    }
                    view.getView().unmask();
                },
                error: function (response) {
                    dropHandlers.cancelDrop();
                    view.getView().unmask();
                }
            });
        }

    },

    /**
     * @event change
     * Fires when the value of a field is changed. The value of a field is 
     * checked for changes when the field's setValue method 
     * is called and when any of the events listed in 
     * checkChangeEvents are fired.
     * @param {Ext.form.field.Field} field
     * @param {Boolean} newValue The new value
     * @param {Boolean} oldValue The original value
     */
    onIncludeInheritedChange: function (field, newValue, oldValue) {
        var vm = this.getViewModel();

        var filterCollection = vm.get("allAttributes").getFilters();

        if (newValue === true) {
            filterCollection.removeByKey('inheritedFilter');
        } else {
            filterCollection.add({
                id: 'inheritedFilter',
                property: 'inherited',
                value: newValue
            });

        }
    },

    /**
     * Filter grid items.
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchSubmit: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // get value

        var searchTerm = vm.getData().search.value;
        var filterCollection = vm.get("allAttributes").getFilters();
        if (searchTerm) {
            filterCollection.add([{
                id: 'nameFilter',
                property: 'name',
                operator: 'like',
                value: searchTerm
            }, {
                id: 'descriptionFilter',
                property: 'description',
                operator: 'like',
                value: searchTerm
            }]);
        } else {
            this.onSearchClear(field);
        }
    },

    /**
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchClear: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // clear store filter
        var filterCollection = vm.get("allAttributes").getFilters();
        filterCollection.removeByKey('nameFilter');
        filterCollection.removeByKey('descriptionFilter');

        // reset input
        field.reset();
    },

    /**
     * @param {Ext.form.field.Base} field
     * @param {Ext.event.Event} event
     */
    onSearchSpecialKey: function (field, event) {
        if (event.getKey() == event.ENTER) {
            this.onSearchSubmit(field);
        }
    },

    /**
     * @param {Ext.selection.RowModel} row
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} eOpts
     */
    onDeselect: function (row, record, index, eOpts) {
        this.getView().getView().getPlugins()[0].enable();
    },

    /**
     * @param {Ext.selection.RowModel} row
     * @param {Ext.data.Model} record
     * @param {Number} index
     * @param {Object} eOpts
     */
    onSelect: function (row, record, index, eOpts) {
        this.view.setSelection(record);
        this.view.getViewModel().set('selected', record);
        this.view.getViewModel().set('theAttribute', record);
        var formInRowPlugin = this.getView().getPlugin('forminrowwidget');

        Ext.GlobalEvents.fireEventArgs('selectedclassattribute', [record]);
        if (formInRowPlugin &&
            !Ext.Object.isEmpty(formInRowPlugin.recordsExpanded)) {
            this.getView().getView().getPlugins()[0].disable();
        } else {
            this.getView().getView().getPlugins()[0].enable();
        }
    },

    /**
     * 
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onNewBtnClick: function (item, event, eOpts) {
        var view = this.getView();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-classes-tabitems-attributes-card-create',
            viewModel: {
                data: {
                    objectTypeName: view.getViewModel().get('objectTypeName'),
                    attributes: this.getView().getStore().getRange()
                }
            }
        });
    },

    /**
     * 
     * @param {CMDBuildUI.model.Attribute} record 
     */
    onAttributeUpdated: function (record) {
        var view = this.getView();
        view.getPlugin('forminrowwidget').view.fireEventArgs('itemupdated', [view, record, this]);
    },

    /**
     * 
     * @param {CMDBuildUI.model.Attribute} record 
     */
    onAttributeCreated: function (record) {
        var view = this.getView();

        var store = view.getStore();
        store.load();

        store.on('load', function (records, operation, success) {
            view.getView().refresh();
            setTimeout(function () {
                var store = view.getStore();
                var index = store.findExact("_id", record.getId());
                var storeItem = store.getAt(index);

                view.getPlugin('forminrowwidget').view.fireEventArgs('togglerow', [null, storeItem, index]);
            }, 0);
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEditAttributeBtnClick: function (button, e, eOpts) {
        var view = this.getView();
        var vm = view.getViewModel();

        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-classes-tabitems-attributes-card-edit',
            viewModel: {
                data: {
                    objectTypeName: vm.get('objectTypeName'),
                    attributeName: vm.get('selected').get('name'),
                    attributes: view.getStore().getRange(),
                    // title: vm.get('objectTypeName') + ' - ' + 'Attributes' + ' - ' + vm.get('selected').get('name'),
                    grid: view
                }
            }
        });
    },

    /**
     * Triggered on clone tool click.
     * 
     * @param {CMDBuildUI.components.tab.FormPanel} formpanel
     * @param {Ext.panel.Tool} tool
     * @param {Object} eOpts
     */
    /*    onCloneBtnClick: function () {
            var view = this.getView();
            var vm = view.getViewModel();
            var theAttribute = vm.get('theAttribute');
            var newAttribute = theAttribute.copy();

            var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
            container.removeAll();

            container.add({
                xtype: 'administration-content-classes-tabitems-attributes-card-create',
                viewModel: {
                    data: {
                        objectTypeName: vm.get('objectTypeName'),
                        attributeName: vm.get('selected').get('name'),
                        attributes: view.getStore().getRange(),
                        theAttribute: newAttribute,
                        title: vm.get('objectTypeName') + ' - ' + 'Attributes' + ' - ' + vm.get('selected').get('name'),
                        grid: view
                    }
                }
            });
        },
    */
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onToggleBtnClick: function (button, e, eOpts) {
        var view = this.getView();
        var vm = view.getViewModel();
        var theAttribute = vm.get('theAttribute');
        Ext.apply(theAttribute.data, theAttribute.getAssociatedData());
        var value = !theAttribute.get('active');
        CMDBuildUI.util.Ajax.setActionId('toggle-active-attribute');
        theAttribute.set('active', value);
        theAttribute.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
        theAttribute.model.setProxy({
            type: 'baseproxy',
            url: Ext.String.format('/classes/{0}/attributes/', vm.get('objectTypeName'))
        });

        theAttribute.save({
            success: function (record, operation) {
                var valueString = record.get('active') ? CMDBuildUI.locales.Locales.administration.common.messages.enabled : CMDBuildUI.locales.Locales.administration.common.messages.disabled;
                CMDBuildUI.util.Notifier.showSuccessMessage(Ext.String.format('{0} {1} {2}.',
                    record.get('name'),
                    CMDBuildUI.locales.Locales.administration.common.messages.was,
                    valueString), null, 'administration');
                Ext.GlobalEvents.fireEventArgs("attributeupdated", [record]);
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onOpenAttributeBtnClick: function (button, e, eOpts) {
        var view = this.getView();
        var vm = view.getViewModel();

        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();

        container.add({
            xtype: 'administration-content-classes-tabitems-attributes-card-view',

            viewModel: {
                data: {
                    theAttribute: vm.get('selected'),
                    objectTypeName: vm.get('objectTypeName'),
                    attributeName: vm.get('selected').get('name'),
                    attributes: view.getStore().getRange(),
                    title: Ext.String.format('{0} - {1} - {2}',
                        vm.get('objectTypeName'),
                        CMDBuildUI.locales.Locales.administration.attributes.attributes,
                        vm.get('selected').get('name')),
                    grid: this.getView()
                }
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onDeleteAttributeBtnClick: function (button, e, eOpts) {
        var me = this;
        var view = me.getView();
        var vm = view.getViewModel();
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowerCase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowerCase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-attribute-class');

                    var record = vm.get('theAttribute');
                    var store = record.store;
                    record.model = Ext.ClassManager.get('CMDBuildUI.model.Attribute');
                    record.model.setProxy({
                        type: 'baseproxy',
                        url: Ext.String.format('/classes/{0}/attributes/', vm.get('objectTypeName'))
                    });
                    record.erase({
                        failure: function (record, operation) {
                            view.getPlugin('forminrowwidget').view.fireEventArgs('itemupdated', [view, record, this]);
                        },
                        success: function (record, operation) {
                            CMDBuildUI.util.Notifier.showSuccessMessage(Ext.String.format('{0} {1}',
                                record.get('name'),
                                CMDBuildUI.locales.Locales.administration.common.messages.wasdeleted
                            ), null, 'administration');
                            view.getPlugin('forminrowwidget').view.fireEventArgs('itemdeleted', [view, record, this]);
                        }
                    });
                }
            }, this);
    }

});