Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.View', {
    extend: 'CMDBuildUI.view.administration.components.attributes.actionscontainers.View',

    requires: [
        'CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.ViewController',
        'CMDBuildUI.view.administration.content.classes.tabitems.attributes.card.EditModel',
        'Ext.layout.*'
    ],

    alias: 'widget.administration-content-classes-tabitems-attributes-card-view',
    controller: 'administration-content-classes-tabitems-attributes-card-view',
    viewModel: {
        type: 'view-administration-content-classes-tabitems-attributes-card-edit'
    }
});