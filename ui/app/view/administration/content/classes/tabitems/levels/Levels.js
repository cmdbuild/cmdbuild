
Ext.define('CMDBuildUI.view.administration.content.elements.tabitems.levels.Levels',{
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.elements.tabitems.levels.LevelsController',
        'CMDBuildUI.view.administration.content.elements.tabitems.levels.LevelsModel'
    ],

    alias: 'widget.administration-content-classes-tabitems-levels-levels',
    controller: 'administration-content-classes-tabitems-levels-levels',
    viewModel: {
        type: 'administration-content-classes-tabitems-levels-levels'
    },

    html: 'To be implemented'
});
