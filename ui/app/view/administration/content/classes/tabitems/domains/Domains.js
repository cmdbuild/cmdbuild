Ext.define('CMDBuildUI.view.administration.content.elements.tabitems.domains.Domains', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.elements.tabitems.domains.DomainsController',
        'CMDBuildUI.view.administration.content.elements.tabitems.domains.DomainsModel'
    ],

    alias: 'widget.administration-content-classes-tabitems-domains-domains',
    controller: 'administration-content-classes-tabitems-domains-domains',
    viewModel: {
        type: 'administration-content-classes-tabitems-domains-domains'
    },

    html: 'To be implemented'
});
