Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.TriggersFieldsetController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-classes-tabitems-properties-fieldsets-triggersfieldset',

    control: {
        '#saveTriggerBtn': {
            click: 'onSaveBtnClick'
        }
    },

    onAddNewTriggerBtn: function (grid, rowIndex, colIndex) {

        var vm = this.getViewModel();
        var triggers = vm.get('theObject.formTriggers');
        var newTriggerStore = vm.get('formTriggersStoreNew');
        var newTrigger = newTriggerStore.getData().items[0];


        Ext.suspendLayouts();
        triggers.add(newTrigger);
        newTriggerStore.removeAt(rowIndex);
        newTriggerStore.add(Ext.create('CMDBuildUI.model.FormTrigger', { script: '' }));
        grid.refresh();
        this.lookupReference('triggersGrid').view.grid.getView().refresh();
        Ext.ComponentQuery.query('[name="formTriggerBeforeView"]')[0].setValue(false);
        Ext.ComponentQuery.query('[name="formTriggerBeforeInsert"]')[0].setValue(false);
        Ext.ComponentQuery.query('[name="formTriggerAfterInsert"]')[0].setValue(false);
        Ext.ComponentQuery.query('[name="formTriggerBeforeEdit"]')[0].setValue(false);
        Ext.ComponentQuery.query('[name="formTriggerAfterEdit"]')[0].setValue(false);
        Ext.ComponentQuery.query('[name="formTriggerBeforeClone"]')[0].setValue(false);
        Ext.ComponentQuery.query('[name="formTriggerAfterClone"]')[0].setValue(false);
        Ext.ComponentQuery.query('[name="formTriggerActive"]')[0].setValue(true);
        window.newTriggerScript.getSession().setValue('');
        Ext.resumeLayouts();

    },
    onSaveBtnClick: function () {
        
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {

    },

    formTriggerCheckChange: function (element, newValue, oldValue, eOpts) {
        this.getViewModel().getParent().get('formTriggersStoreNew').getData().items[0].set(element.recordKey, newValue);
    }, 

    onEditBtn: function (view, rowIndex, colIndex) {
        var vm = this.getViewModel();
        var grid = this.lookupReference('triggersGrid');
        var theTrigger = grid.getStore().getAt(rowIndex);
        vm.set('theTrigger', theTrigger);
        var content = {
            xtype: 'form',
            layout: 'column',
            ui: 'administration-formpagination',
            fieldDefaults: {
                labelAlign: 'top'
            },

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',

                items: [{ xtype: 'component', flex: 1 },
                {
                    text: CMDBuildUI.locales.Locales.administration.classes.properties.toolbar.saveBtn,
                    formBind: true, //only enabled once the form is valid
                    disabled: true,
                    ui: 'administration-action-small',
                    reference: 'saveTriggerBtn',
                    itemId: 'saveTriggerBtn',
                    listeners: {
                        click: function () {
                            Ext.suspendLayouts();
                            theTrigger.commit();
                            grid.getView().refresh();
                            Ext.resumeLayouts();
                            popUp.fireEvent('close');

                        }
                    }
                },
                {
                    text: CMDBuildUI.locales.Locales.administration.classes.properties.toolbar.cancelBtn,
                    ui: 'administration-secondary-action-small',
                    listeners: {
                        click: function () {
                            theTrigger.reject();
                            popUp.fireEvent('close');
                        }
                    }
                }]
            }],

            items: [{
                columnWidth: 0.8,
                items: [
                    /********************* Name **********************/
                    {
                        xtype: 'aceeditortextarea',
                        reference: 'editTriggerScriptField',
                        inputField: 'script',
                        vmObjectName: 'theTrigger',
                        fieldLabel: 'Code',
                        config: {
                            options: {
                                autoScrollEditorIntoView: true,
                                maxLines: 40,
                                minLines: 40,
                                readOnly: false
                            }
                        },
                        value: view.grid.getStore().getAt(rowIndex).get('script'),
                        bind: {
                            value: '{theTrigger.script}'
                        },
                        onChange: function () {
                            this.lastValue = this.value;
                            this.value = this.getAceEditor().getSession().getValue();
                            vm.get('theTrigger').set('script', this.value);
                            this.isCodeValid();
                            this.fireEvent('change', this, this.value, this.lastValue);
                        },
                        width: '100%',
                        height: '100%'
                    },
                    {
                        xtype: 'checkbox', // textfield | combo | radio
                        value: view.grid.getStore().getAt(rowIndex).get('active'),
                        bind: '{theTrigger.active}',
                        boxLabel: 'Active', // TODO: translate
                        readOnly: false
                    }
                ]
            }, {
                columnWidth: 0.2,
                items: [{
                    /********************* Name **********************/
                    xtype: 'checkboxgroup',
                    userCls: 'hideCellCheboxes',
                    fieldLabel: 'Execute on',
                    columns: 1,
                    vertical: true,
                    items: [
                        { boxLabel: 'Before view', recordKey: 'beforeView', uncheckedValue: false, inputValue: true, cls: 'checkbox0', value: view.grid.getStore().getAt(rowIndex).get('beforeView'), bind: '{theTrigger.beforeView}' },// TODO:translate
                        { boxLabel: 'Before insert', recordKey: 'beforeInsert', inputValue: true, cls: 'checkbox1', value: view.grid.getStore().getAt(rowIndex).get('beforeInsert'), bind: '{theTrigger.beforeInsert}' },// TODO:translate
                        { boxLabel: 'After insert', recordKey: 'afterInsert', inputValue: true, cls: 'checkbox3', value: view.grid.getStore().getAt(rowIndex).get('afterInsert'), bind: '{theTrigger.afterInsert}' },// TODO:translate
                        { boxLabel: 'Before edit', recordKey: 'beforeEdit', inputValue: true, cls: 'checkbox4', value: view.grid.getStore().getAt(rowIndex).get('beforeEdit'), bind: '{theTrigger.beforeEdit}' },// TODO:translate
                        { boxLabel: 'After edit', recordKey: 'afterEdit', inputValue: true, cls: 'checkbox6', value: view.grid.getStore().getAt(rowIndex).get('afterEdit'), bind: '{theTrigger.afterEdit}' },// TODO:translate
                        { boxLabel: 'Before clone', recordKey: 'beforeClone', inputValue: true, cls: 'checkbox7', value: view.grid.getStore().getAt(rowIndex).get('beforeClone'), bind: '{theTrigger.beforeClone}' },// TODO:translate
                        { boxLabel: 'After clone', recordKey: 'afterClone', inputValue: true, cls: 'checkbox9', value: view.grid.getStore().getAt(rowIndex).get('afterClone'), bind: '{theTrigger.afterClone}' }// TODO:translate
                    ]
                }]
            }]
        };

        // custom panel listeners
        var listeners = {
            closed: function () {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-formtrigger');
            },
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-formtrigger');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-formtrigger',
            'Edit trigger',
            content,
            listeners,
            {
                ui: 'administration', // TODO: administratiion 
                width: '800px',
                height: '700px',
                viewModel: {}
            }
        );
    }

});
