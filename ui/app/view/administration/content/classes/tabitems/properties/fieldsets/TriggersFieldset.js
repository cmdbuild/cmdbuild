Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.TriggersFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-classes-tabitems-properties-fieldsets-triggersfieldset',

    controller: 'administration-content-classes-tabitems-properties-fieldsets-triggersfieldset',

    viewModel: {
        type: 'administration-content-classes-tabitems-properties-fieldsets-triggersfieldset'
    },

    items: [{
        xtype: 'fieldset',
        collapsible: true,
        collapsed: true,
        layout: 'column',
        bind: {
            title: 'Form Triggers ({formTriggerCount})'
        },

        ui: 'administration-formpagination',

        items: [{
            columnWidth: 1,
            items: [{
                xtype: 'components-grid-reorder-grid',
                bind: {
                    store: '{theObject.formTriggers}'
                },

                columnWidth: 0.5,
                reference: 'triggersGrid',
                flex: 1,
                columns: [{
                        flex: 2,
                        text: 'Javascript Script',
                        xtype: 'widgetcolumn',
                        align: 'left',
                        bind: {
                            data: '{record}'
                        },
                        widget: {
                            xtype: 'aceeditortextarea',
                            config: {
                                options: {
                                    readOnly: true
                                }
                            },
                            inputField: 'script',
                            vmObjectName: 'record',
                            bind: {
                                value: '{record.script}'
                            }
                        }
                    },

                    {
                        flex: 2,
                        text: 'Events',
                        align: 'left',
                        renderer: function (value, meta, record) {
                            return record.getSelectedTriggers();
                        }
                    }, {
                        flex: 1,
                        text: 'Status',
                        xtype: 'widgetcolumn',
                        align: 'left',
                        widget: {
                            xtype: 'checkbox', // textfield | combo | radio
                            bind: {
                                value: '{record.active}',
                                readOnly: '{!actions.edit}'
                            },
                            boxLabel: 'Active' // TODO: translate

                        }
                    },
                    {
                        xtype: 'actioncolumn',
                        minWidth: 100,
                        maxWidth: 200,
                        bind: {
                            hidden: '{!actions.edit}'
                        },
                        align: 'center',
                        items: [{
                            iconCls: 'x-fa fa-pencil',
                            tooltip: 'Edit trigger',
                            handler: 'onEditBtn'
                        }, {
                            iconCls: 'x-fa fa-arrow-up',
                            tooltip: 'Move up',
                            handler: function (grid, rowIndex, colIndex) {
                                Ext.suspendLayouts();
                                var store = grid.getStore();
                                var record = store.getAt(rowIndex);
                                rowIndex--;
                                if (!record || rowIndex < 0) {
                                    return;
                                }

                                store.remove(record);
                                store.insert(rowIndex, record);

                                this.getView().refresh();
                                Ext.resumeLayouts();
                            },

                            isDisabled: function (view, rowIndex, colIndex, item, record) {
                                return rowIndex == 0;
                            }
                        }, {
                            iconCls: 'x-fa fa-arrow-down',
                            tooltip: 'Move down',
                            handler: function (grid, rowIndex, colIndex) {
                                Ext.suspendLayouts();
                                var store = grid.getStore();
                                var record = store.getAt(rowIndex);
                                rowIndex++;
                                if (!record || rowIndex >= store.getCount()) {
                                    return;
                                }
                                store.remove(record);
                                store.insert(rowIndex, record);

                                this.getView().refresh();
                                Ext.resumeLayouts();
                            },
                            isDisabled: function (view, rowIndex, colIndex, item, record) {
                                return rowIndex >= view.store.getCount() - 1;
                            }
                        }, {

                            iconCls: 'x-fa fa-times',
                            tooltip: 'Delete trigger',
                            handler: function (grid, rowIndex, colIndex) {
                                //Ext.suspendLayouts();
                                var store = grid.getStore();

                                var record = store.getAt(rowIndex);
                                //grid.getStore().removeAt(rowIndex, 1);
                                store.remove(record);
                                //this.getView().refresh();
                                //this.lookupReference('triggersGrid').view.grid.getView().refresh();
                                //Ext.resumeLayouts();
                                this.getView().updateLayout();

                            }
                        }]
                    }
                ]
            }, {
                xtype: 'components-grid-reorder-grid',
                bind: {
                    store: '{formTriggersStoreNew}',
                    hidden: '{!actions.edit}'
                },
                scrollable: 'y',
                //forceFit: true,
                columnWidth: 0.5,
                flex: 1,
                columns: [{
                    flex: 2,
                    text: 'Create new form trigger',
                    xtype: 'widgetcolumn',
                    align: 'left',
                    cellWrap: true ,
                    variableRowHeight: true,
                    widget: {
                        xtype: 'component',
                        html: '<div id="newTriggerScript" style="min-height:100px;height:100%;min-width:20px; width:100%"></div>',
                        inputField: 'script',
                        listeners: {
                            afterrender: function (cmp) {
                                var me = this;
                                var editor = window.newTriggerScript = ace.edit('newTriggerScript', {
                                    //set autoscroll
                                    autoScrollEditorIntoView: true,
                                    // set maxLines
                                    maxLines: 30,
                                    //set the mode
                                    mode: "ace/mode/javascript",
                                    // set theme
                                    theme: "ace/theme/chrome",
                                    // show line numbers
                                    showLineNumbers: true,
                                    // hide print margin
                                    showPrintMargin: false
                                });

                                editor.setValue('');

                                editor.getSession().on('change', function (event, _editor) {
                                    var rows = _editor.getScreenLength();
                                    if (rows > 8 && rows < editor.getOption('maxLines')) {
                                        me.up().setHeight(rows * editor.getFontSize()+25);
                                    }
                                    var vm = me.up('administration-content-classes-tabitems-properties-fieldsets-triggersfieldset').getViewModel().getParent();
                                    vm.get('formTriggersStoreNew').getData().items[0].set(cmp.inputField, _editor.getValue());
                                });
                            }
                        }
                    }
                }, {
                    flex: 2,
                    text: '',
                    xtype: 'widgetcolumn',
                    align: 'left',
                    widget: {
                        xtype: 'checkboxgroup',
                        userCls: 'hideCellCheboxes',
                        columns: 3,
                        vertical: true,
                        /**
                         * onInsert, onEdit and onClone was removed since prototype #23
                         */
                        items: [{
                            boxLabel: 'Before view',
                            recordKey: 'beforeView',
                            uncheckedValue: false,
                            inputValue: true,
                            cls: 'checkbox0',
                            name: 'formTriggerBeforeView',
                            listeners: {
                                change: 'formTriggerCheckChange'
                            }
                        }, {
                            boxLabel: 'Before insert', // TODO:translate
                            recordKey: 'beforeInsert',
                            name: 'formTriggerBeforeInsert',
                            inputValue: true,
                            cls: 'checkbox1',
                            listeners: {
                                change: 'formTriggerCheckChange'
                            }
                        }, {
                            boxLabel: 'After insert', // TODO: translate
                            recordKey: 'afterInsert',
                            name: 'formTriggerAfterInsert',
                            inputValue: true,
                            cls: 'checkbox3',
                            listeners: {
                                change: 'formTriggerCheckChange'
                            }
                        }, {
                            boxLabel: 'Before edit', // TODO: translate
                            recordKey: 'beforeEdit',
                            name: 'formTriggerBeforeEdit',
                            inputValue: true,
                            cls: 'checkbox4',
                            listeners: {
                                change: 'formTriggerCheckChange'
                            }
                        }, {
                            boxLabel: 'After edit', // TODO:translate
                            recordKey: 'afterEdit',
                            name: 'formTriggerAfterEdit',
                            inputValue: true,
                            cls: 'checkbox6',
                            listeners: {
                                change: 'formTriggerCheckChange'
                            }
                        }, {
                            boxLabel: 'Before clone', // TODO:translate
                            recordKey: 'beforeClone',
                            name: 'formTriggerBeforeClone',
                            inputValue: true,
                            cls: 'checkbox7',
                            listeners: {
                                change: 'formTriggerCheckChange'
                            }
                        }, {
                            boxLabel: 'After clone', // TODO:translate
                            recordKey: 'afterClone',
                            name: 'formTriggerAfterClone',
                            inputValue: true,
                            cls: 'checkbox9',
                            listeners: {
                                change: 'formTriggerCheckChange'
                            }
                        }]
                    }
                }, {
                    flex: 1,
                    text: '',
                    xtype: 'widgetcolumn',
                    align: 'left',
                    widget: {
                        xtype: 'checkbox', // textfield | combo | radio
                        recordKey: 'active',
                        name: 'formTriggerActive',
                        boxLabel: 'Active', // TODO: translate
                        readOnly: false,
                        listeners: {
                            change: function (element, newValue, oldValue, eOpts) {
                                element.up('administration-content-classes-tabitems-properties-fieldsets-triggersfieldset').getViewModel().getParent().get('formTriggersStoreNew').getData().items[0].set(element.recordKey, element.checked);
                            }
                        }
                    }
                }, {
                    xtype: 'actioncolumn',
                    minWidth: 100,
                    maxWidth: 200,
                    bind: {
                        hidden: '{!actions.edit}'
                    },
                    align: 'center',
                    items: [{
                        iconCls: 'x-fa fa-ellipsis-h',
                        disabled: true
                    }, {
                        iconCls: 'x-fa fa-ellipsis-h',
                        disabled: true
                    }, {
                        iconCls: 'x-fa fa-ellipsis-h',
                        disabled: true
                    }, {
                        iconCls: 'x-fa fa-plus',
                        tooltip: 'Add new trigger', // TODO: translate
                        handler: 'onAddNewTriggerBtn'
                    }]
                }]
            }]
        }]
    }]
});