Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.GeneralDataFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-classes-tabitems-properties-fieldsets-generaldatafieldset',

    viewModel: {

    },
    ui: 'administration-formpagination',

    items: [{
        xtype: 'fieldset',
        layout: 'column',
        title: 'General Attributes',
        ui: 'administration-formpagination',
        items: [{
            columnWidth: 0.5,
            items: [{
                /********************* Name **********************/
                // create / edit
                xtype: 'textfield',
                vtype: 'alphanum',
                fieldLabel: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.generalData.inputs.name.label,
                name: 'name',
                enforceMaxLength: true,
                maxLength: 20,
                hidden: true,
                bind: {
                    value: '{theObject.name}',
                    hidden: '{!actions.add}'
                }
            }, {
                // view
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.generalData.inputs.name.label,
                name: 'name',
                hidden: true,
                bind: {
                    value: '{theObject.name}',
                    hidden: '{actions.add}'
                }
            }, {
                /********************* description **********************/
                // create / edit
                xtype: 'combobox',
                editable: false,
                reference: 'parentField',
                displayField: 'description',
                valueField: 'name',
                name: 'parent',
                fieldLabel: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.generalData.inputs.parent.label,
                hidden: true,
                bind: {
                    store: '{superclassesStore}',
                    value: '{theObject.parent}',
                    hidden: '{hideParentCombobox}'
                }
            }, {
                // view
                xtype: 'displayfield',
                name: 'parent',
                fieldLabel: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.generalData.inputs.parent.label,
                hidden: true,
                bind: {
                    value: '{parentDescription}',
                    hidden: '{hideParentDisplayfield}'
                }
            }, {
                /********************* Class type **********************/
                // create
                xtype: 'combobox',
                queryMode: 'local',
                displayField: 'label',
                valueField: 'value',
                fieldLabel: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.generalData.inputs.classType.label,
                name: 'type',
                hidden: true,
                bind: {
                    store: '{classTypeStore}',
                    value: '{theObject.type}',
                    hidden: '{!actions.add}'
                },
                listeners: {
                    change: function (combobox, newValue, oldValue, eOpts) {
                        var vm = combobox.up('administration-content-classes-tabitems-properties-fieldsets-generaldatafieldset').getViewModel().getParent();
                        if (oldValue !== null) {
                            if (newValue === 'standard') {
                                vm.set('theObject.parent', 'Class');
                            } else {
                                vm.set('theObject.parent', '');
                            }
                        }
                    }
                }
            }, {
                // view
                xtype: 'displayfield',
                fieldLabel: 'Class type',
                name: 'type',
                hidden: true,
                bind: {
                    value: '{theObject.type}',
                    hidden: '{actions.add}'
                },
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    switch (value) {
                        case 'standard':
                            return 'Standard'; // TODO: translate
                        case 'simple':
                            return 'Simple';
                    }
                }
            }, {
                /********************* Active **********************/
                // create / edit / view
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                hidden: true,
                bind: {
                    value: '{theObject.active}',
                    readOnly: '{actions.view}',
                    hidden: '{!theObject}'
                }
            }]
        }, {
            columnWidth: 0.5,
            style: {
                paddingLeft: '15px'
            },
            items: [
                /********************* description **********************/
                /*                {
                                    // create / edit
                                    xtype: 'textfield',
                                    fieldLabel: 'Description',
                                    name: 'description',
                                    hidden: true,
                                    bind: {
                                        value: '{theObject.description}',
                                        hidden: '{actions.view}'
                                    }
                                },*/
                {
                    // view
                    xtype: 'displayfield',
                    fieldLabel: 'Description',
                    name: 'description',
                    hidden: true,
                    bind: {
                        value: '{theObject.description}',
                        hidden: '{!actions.view}'
                    }
                },
                {
                    xtype: 'container',
                    fieldLabel: 'Description',
                    layout: 'hbox',
                    bind: {
                        hidden: '{actions.view}'
                    },
                    items: [{
                        flex: 5,
                        fieldLabel: 'Description',
                        xtype: 'textfield',
                        name: 'description',
                        bind: {
                            value: '{theObject.description}'
                        }
                    }, {
                        xtype: 'fieldcontainer',
                        flex: 1,
                        fieldLabel: '&nbsp;',
                        labelSeparator: '',
                        items: [{
                            xtype: 'button',
                            reference: 'translateBtn',
                            iconCls: 'x-fa fa-flag fa-2x',
                            style: 'padding-left: 5px',

                            tooltip: 'Translate',
                            config: {
                                theSetup: null
                            },
                            viewModel: {
                                data: {
                                    theObject: null,
                                    vmKey: 'theObject'
                                }
                            },
                            bind: {
                                theObject: '{theObject}'
                            },
                            listeners: {
                                click: 'onTranslateClick'
                            },
                            cls: 'administration-field-container-btn',
                            autoEl: {
                                'data-testid': 'administration-card-edit-translateBtn'
                            }
                        }]

                    }]
                }, {
                    /********************* Superclass **********************/
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Superclass',
                    name: 'prototype',
                    hidden: true,
                    bind: {
                        value: '{theObject.prototype}',
                        readOnly: '{!actions.add}',
                        hidden: '{isSimpleClass}'
                    }
                }, {
                    /********************* Multitenant mode **********************/
                    xtype: 'fieldcontainer',
                    flex: 1,
                    fieldLabel: 'Multitenant mode',
                    bind: {
                        hidden: '{!isMultitenantActive}'
                    },
                    items: [{
                        // edit / add
                        xtype: 'combobox',
                        name: 'multitenantMode',
                        displayField: 'label',
                        valueField: 'value',
                        bind: {
                            value: '{theObject.multitenantMode}',
                            hidden: '{actions.view}',
                            store: '{multitenantModeStore}'
                        }
                    }, {
                        // view
                        xtype: 'displayfield',
                        name: 'multitenantMode',
                        hidden: true,
                        bind: {
                            value: '{theObject.multitenantMode}',
                            hidden: '{!actions.view}'
                        },
                        renderer: function (value) {
                            return Ext.String.capitalize(value);
                        }
                    }]
                }
            ]
        }]
    }]
});