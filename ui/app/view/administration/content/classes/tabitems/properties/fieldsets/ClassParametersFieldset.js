
Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.ClassParamentersFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-classes-tabitems-properties-fieldsets-classparametersfieldset',

    items: [{
        xtype: 'fieldset',
        layout: 'column',
        title: 'Class Parameters',
        ui: 'administration-formpagination',
        collapsible: true,
        items: [
            {
                columnWidth: 1,
                items: [{
                    width: '50%',
                    items: [{
                        // create / edit
                        xtype: 'combobox',
                        queryMode: 'local',
                        displayField: 'name',
                        valueField: '_id',
                        name: 'dafaultfilter',
                        fieldLabel: 'Default filter',
                        allowBlank: true,
                        hidden: true,
                        bind: {
                            store: '{defaultFilterStore}',
                            value: '{theObject.dafaultFilter}',
                            hidden: '{actions.view}'
                        }
                    },
                    {
                        // view
                        xtype: 'displayfield',
                        name: 'dafaultfilter',
                        fieldLabel: 'Default filter',
                        hidden: true,
                        bind: {
                            value: '{theObject.dafaultfilter}',
                            hidden: '{!actions.view}'
                        }
                    }]
                }]
            }, {
                columnWidth: 0.5,
                items: [{
                    /********************* Note inline closed **********************/
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Note inline',
                    name: 'noteinline',
                    hidden: true,
                    bind: {
                        value: '{theObject.noteInline}',
                        readOnly: '{actions.view}',
                        hidden: '{!theObject}'
                    },
                    listeners: {
                        change: function () {

                        }
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    /********************* Note inline closed **********************/
                    // create / edit / view
                    xtype: 'checkbox',
                    fieldLabel: 'Note inline closed',
                    name: 'noteinlineclosed',
                    hidden: true,
                    bind: {
                        value: '{theObject.noteInlineClosed}',
                        readOnly: '{actions.view}',
                        hidden: '{!theObject}',
                        disabled: '{checkboxNoteInlineClosed.disabled}'
                    }
                }]
            }
        ]
    }]
});
