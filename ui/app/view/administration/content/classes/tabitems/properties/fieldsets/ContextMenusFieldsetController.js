Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.ContextMenusFieldsetController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-classes-tabitems-properties-fieldsets-contextmenusfieldset',


    onAddNewContextMenuBtn: function (grid, rowIndex, colIndex) {

        var vm = this.getViewModel();
        var contexstMenus = vm.get('theObject.contextMenuItems');
        var newContextMenuStore = vm.get('contextMenuItemsStoreNew');
        var newContextMenu = newContextMenuStore.getData().items[0];


        Ext.suspendLayouts();
        contexstMenus.add(newContextMenu);
        newContextMenuStore.removeAt(rowIndex);
        newContextMenuStore.add(Ext.create('CMDBuildUI.model.ContextMenuItem', { script: '' }));
        grid.refresh();
        this.lookupReference('contextMenuGrid').view.grid.getView().refresh();
        window.newContextMenuScriptField.getSession().setValue('');
        //this.lookupReference('newContextMenuScriptField').getAceEditor().getSession().setValue('');
        Ext.resumeLayouts();

    },
    onEditBtn: function (view, rowIndex, colIndex) {
        var vm = this.getViewModel();
        var grid = this.lookupReference('contextMenuGrid');
        var theContext = grid.getStore().getAt(rowIndex);
        vm.set('theContext', theContext);
        
        var content = {
            xtype: 'form',
            ui: 'administration-formpagination',

            fieldDefaults: {
                labelAlign: 'top'
            },
            config: {
                theContext: null
            },
            bind: {
                theContext: 'theContext'
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',

                items: [{ xtype: 'component', flex: 1 },
                {
                    text: CMDBuildUI.locales.Locales.administration.classes.properties.toolbar.saveBtn,
                    formBind: true, //only enabled once the form is valid
                    disabled: true,
                    ui: 'administration-action-small',
                    reference: 'saveTriggerBtn',
                    itemId: 'saveTriggerBtn',
                    listeners: {
                        click: function () {
                            Ext.suspendLayouts();
                            grid.getView().remove(theContext);
                            grid.getView().refresh();
                            Ext.resumeLayouts();
                            popUp.fireEvent('close');

                        }
                    }
                },
                {
                    text: CMDBuildUI.locales.Locales.administration.classes.properties.toolbar.cancelBtn,
                    ui: 'administration-secondary-action-small',
                    listeners: {
                        click: function () {
                            theContext.reject();
                            popUp.fireEvent('close');
                        }
                    }
                }]
            }],
            layout: {
                type: 'vbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [{
                // our top panel
                xtype: 'container',
                flex: 1,             // take remaining available vert space
                
                anchor:   '100%',
                autoScroll: true,
                items: [{
                    xtype: 'aceeditortextarea',
                    reference: 'editTriggerScriptField',
                    inputField: 'script',
                    vmObjectName: 'theContext',
                    fieldLabel: 'Code',
                    //layout: 'fit',
                    
                    anchor:   '100%',
                    config: {
                        options: {
                            //autoScrollEditorIntoView: false,
                            maxLines: 39,
                            minLines: 39
                        }
                    },
                    value: view.grid.getStore().getAt(rowIndex).get('script'),
                    bind: {
                        value: '{theContext.script}'
                    },
                    onChange: function () {
                        this.lastValue = this.value;
                        this.value = this.getAceEditor().getSession().getValue();
                        vm.get('theContext').set('script', this.value);
                        this.isCodeValid();
                        this.fireEvent('change', this, this.value, this.lastValue);

                    }
                },
                {
                    xtype: 'combobox',
                    inputField: 'visibility',
                    width: '100%',
                    //style: "padding-top:0px",
                    editable: false,
                    forceSelection: true,
                    allowBlank: false,
                    fieldLabel: 'Applicability',
                    displayField: 'label',
                    valueField: 'value',
                    value: view.grid.getStore().getAt(rowIndex).get('applicability'),
                    bind: {
                        value: '{theContext.visibility}',
                        store: '{contextMenuApplicabilityStore}'
                    }
                },
                {
                    /********************* Active **********************/
                    xtype: 'checkbox',
                    bind: '{theContext.active}',
                    value: view.grid.getStore().getAt(rowIndex).get('active'),
                    boxLabel: 'Active' // TODO: translate
                }]
            }]
        };
        // custom panel listeners
        var listeners = {
            closed: function () {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-formtrigger');
            },
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-formtrigger');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-contextmenu',
            'Edit trigger',
            content,
            listeners,
            {
                ui: 'administration', // TODO: administratiion 
                width: '800px',
                height: '700px',
                viewModel: {

                }
            }
        );
    }

});
