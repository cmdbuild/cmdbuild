Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.DefaultOrdersFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-classes-tabitems-properties-fieldsets-defaultordersfieldset',
    viewModel: {},
    items: [{
        xtype: 'fieldset',
        layout: 'column',
        bind: {
            hidden: '{actions.add}'
        },
        title: 'Data cards sorting',
        collapsible: true,
        ui: 'administration-formpagination',
        items: [{
            columnWidth: 0.5,
            items: [{
                xtype: 'components-grid-reorder-grid',
                reference: 'defaultOrderGrid',
                bind: {
                    //    data: '{getDefaultOrderData}', 
                    store: '{theObject.defaultOrder}'
                },
                columns: [{
                    flex: 2,
                    text: 'Attribute',
                    dataIndex: 'attribute',
                    align: 'left'
                }, {
                    flex: 1,
                    text: 'Direction',
                    dataIndex: 'direction',
                    align: 'left',
                    renderer: function (value) {
                        return Ext.String.capitalize(value);
                    }
                }, {
                    xtype: 'actioncolumn',
                    minWidth: 100,
                    maxWidth: 200,
                    bind: {
                        hidden: '{!actions.edit}'
                    },
                    align: 'center',
                    items: [{
                        iconCls: 'x-fa fa-pencil',
                        tooltip: 'Edit trigger',
                        handler: function (grid, rowIndex, colIndex) {
                            console.log('edit'); // TODO: enable edit button
                        }
                    }, {
                        iconCls: 'x-fa fa-arrow-up',
                        tooltip: 'Move up',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.suspendLayouts();
                            var store = grid.getStore();
                            var record = store.getAt(rowIndex);
                            rowIndex--;
                            if (!record || rowIndex < 0) {
                                return;
                            }

                            store.remove(record);
                            store.insert(rowIndex, record);

                            this.getView().refresh();
                            Ext.resumeLayouts(true);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                            return rowIndex == 0;
                        }
                    }, {
                        iconCls: 'x-fa fa-arrow-down',
                        tooltip: 'Move Down',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.suspendLayouts();
                            var store = grid.getStore();
                            var record = store.getAt(rowIndex);
                            rowIndex++;
                            if (!record || rowIndex >= store.getCount()) {
                                return;
                            }
                            store.remove(record);
                            store.insert(rowIndex, record);


                            this.getView().refresh();
                            Ext.resumeLayouts(true);
                        },
                        isDisabled: function (view, rowIndex, colIndex, item, record) {
                            return rowIndex >= view.store.getCount() - 1;
                        }
                    }, {
                        iconCls: 'x-fa fa-times',
                        tooltip: 'Remove',
                        handler: function (grid, rowIndex, colIndex) {
                            Ext.suspendLayouts();
                            var store = grid.getStore();
                            var record = store.getAt(rowIndex);

                            var attribute = this.lookupReferenceHolder().lookupReference("defaultOrderAttribute");
                            store.remove(record);

                            var newStore = Ext.create('Ext.data.Store', {
                                model: "CMDBuildUI.model.Attribute",
                                proxy: {
                                    url: Ext.String.format("/classes/{0}/attributes", attribute.getViewModel().get('theObject').getId()),
                                    type: 'baseproxy'
                                },
                                fields: ['name', 'description'],

                                autoLoad: true,
                                autoDestroy: true,
                                remoteFilter: false,
                                filters: [
                                    function (item) {
                                        var defaultOrder = attribute.getViewModel().get('theObject').getAssociatedData().defaultOrder;
                                        var found = false;
                                        for (var field in defaultOrder) {
                                            found = item.get('name') === 'tenantId' || item.get('name') === 'Notes' || defaultOrder[field].attribute === item.get('name');
                                            if (found) {
                                                break;
                                            }
                                        }
                                        return !found;
                                    }
                                ]
                            });



                            attribute.bindStore(newStore);
                            //newStore.load();
                            this.getView().refresh();

                            Ext.resumeLayouts(true);
                        }
                    }]
                }]
            }, {
                items: [{
                    xtype: 'components-grid-reorder-grid',
                    bind: {
                        store: '{defaultOrderStoreNew}',
                        hidden: '{!actions.edit}'
                    },
                    columnWidth: 0.5,
                    flex: 1,
                    columns: [{
                        flex: 2,
                        text: '',
                        xtype: 'widgetcolumn',
                        align: 'left',
                        dataIndex: 'attribute',
                        widget: {
                            xtype: 'combobox',
                            inputField: 'attribute',
                            reference: 'defaultOrderAttribute',
                            editable: false,
                            forceSelection: false,
                            allowBlank: true,
                            displayField: 'description',
                            valueField: 'name',
                            value: '',
                            queryMode: 'local',
                            bind: {
                                value: '{record.attribute}',
                                store: '{unorderedAttributesStore}'
                            }
                        }
                    }, {
                        flex: 1,
                        text: '',
                        xtype: 'widgetcolumn',
                        align: 'left',
                        dataIndex: '_type',
                        widget: {
                            xtype: 'combobox',
                            inputField: 'direction',
                            reference: 'defaultOrderDirection',
                            editable: false,
                            forceSelection: true,
                            // allowBlank: false,
                            displayField: 'label',
                            valueField: 'value',
                            bind: {
                                value: '{record.direction}',
                                store: '{defaultOrderDirectionsStore}'
                            }
                        }
                    }, {
                        xtype: 'actioncolumn',
                        minWidth: 100,
                        maxWidth: 200,

                        align: 'center',
                        items: [{
                            iconCls: 'x-fa fa-ellipsis-h',
                            disabled: true
                        }, {
                            iconCls: 'x-fa fa-ellipsis-h',
                            disabled: true
                        }, {
                            iconCls: 'x-fa fa-ellipsis-h',
                            disabled: true
                        }, {
                            iconCls: 'x-fa fa-plus',
                            tooltip: 'Add',
                            handler: function (grid, rowIndex, colIndex) {
                                var attribute = this.lookupReferenceHolder().lookupReference("defaultOrderAttribute");
                                var direction = this.lookupReferenceHolder().lookupReference("defaultOrderDirection");
                                var orderGrid = this.lookupReferenceHolder().lookupReference("defaultOrderGrid");
                                var orderStore = orderGrid.getStore();
                                var newRecordStore = grid.getStore();
                                if (attribute.getValue() && direction.getValue()) {
                                    Ext.suspendLayouts();
                                    orderStore.add(CMDBuildUI.model.AttributeOrder.create({
                                        attribute: attribute.getValue(),
                                        direction: direction.getValue()
                                    }));
                                    attribute.getStore().remove(attribute.selection);
                                    newRecordStore.getAt(0).set('attribute', null);
                                    newRecordStore.getAt(0).set('direction', null);
                                    orderGrid.getView().refresh();

                                    Ext.resumeLayouts();
                                }
                            }
                        }]
                    }]
                }]
            }]
        }]
    }]
});