Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.PropertiesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-classes-tabitems-properties-properties',

    require: [
        'CMDBuildUI.util.administration.helper.FormHelper'
    ],
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEditBtnClick: function (button, e, eOpts) {
        this.getViewModel().set('action', CMDBuildUI.view.administration.content.classes.TabPanel.formmodes.edit);
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onDeleteBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();

        Ext.Msg.confirm(
            "Delete class", // TODO: translate
            "Are you sure you want to delete this class?", // TODO: translate
            function (action) {
                if (action === CMDBuildUI.locales.Locales.administration.common.yes) {
                    var theObject = vm.get('theObject');
                    var tmpGetAssociated = theObject.getAssociatedData;
                    theObject.getAssociatedData = function () {
                        return false;
                    };

                    CMDBuildUI.util.Ajax.setActionId('delete-class');

                    theObject.erase({
                        error: function () {
                            theObject.getAssociatedData = tmpGetAssociated;
                        },
                        success: function (record, operation) {
                            var response = operation.getResponse();
                            var w = Ext.create('Ext.window.Toast', {
                                // Ex: "INFO: drop cascades to table "AssetTmpl_history""
                                html: Ext.JSON.decode(response.responseText).message || 'Class successfully deleted.', // TODO: translate
                                title: 'Success', // TODO: translate
                                iconCls: 'x-fa fa-check-circle"',
                                align: 'br',
                                autoClose: true,
                                maxWidth: 300,
                                monitorResize: true,
                                closable: true
                            });
                            w.show();
                            theObject.commit();
                            theObject.getAssociatedData = tmpGetAssociated;
                        }
                    });
                }
            }
        );
    },
    /**
     * @param {Ext.menu.Item} menuItem
     * @param {Event} e
     * @param {Object} eOpts
     */
    onPrintMenuItemClick: function (menuItem) {
        var url,
            objectTypeName = this.view.getViewModel().get('theObject').get('name');
        switch (menuItem.fileType) {
            case 'PDF':
                url = CMDBuildUI.util.api.Classes.getClassReport('PDF', objectTypeName);
                break;
            case 'ODT':
                url = CMDBuildUI.util.api.Classes.getClassReport('ODT', objectTypeName);
                break;
            default:
                Ext.Msg.alert('Warning', 'File type of report not implemented!');
        }
        window.open(url, '_blank');
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onDisableBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        var theObject = vm.get('theObject');
        Ext.apply(theObject.data, theObject.getAssociatedData());
        theObject.set('active', false);
        theObject.save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: 'Class deactivated correctly.',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
                vm.getParent().linkTo('theObject', {
                    type: 'CMDBuildUI.model.classes.Class',
                    id: vm.get('objectTypeName')
                });
                theObject.commit();
            }
        });
    },
    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEnableBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        var theObject = vm.get('theObject');
        Ext.apply(theObject.data, theObject.getAssociatedData());
        theObject.set('active', true);
        theObject.save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: 'Class activated correctly.',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
                vm.getParent().linkTo('theObject', {
                    type: 'CMDBuildUI.model.classes.Class',
                    id: vm.get('objectTypeName')
                });
                theObject.commit();
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var me = this;
        button.setDisabled(true);
        var vm = this.getViewModel();
        if (vm.get('theObject').isValid()) {

            var theObject = vm.get('theObject');
            delete theObject.data.system;
            Ext.apply(theObject.data, theObject.getAssociatedData());
            // delete all id / _id in associatedData
            theObject.data.formTriggers.forEach(function (record, index) {
                delete theObject.data.formTriggers[index].id;
                delete theObject.data.formTriggers[index]._id;
            });
            theObject.data.contextMenuItems.forEach(function (record, index) {
                delete theObject.data.contextMenuItems[index].id;
                delete theObject.data.contextMenuItems[index]._id;
            });
            theObject.data.widgets.forEach(function (record, index) {
                delete theObject.data.widgets[index].id;
                delete theObject.data.widgets[index]._id;
            });
            // save the class
            theObject.save({
                success: function (record, operation) {

                    var objectTypeName = record.getId();
                    var nextUrl = Ext.String.format('administration/classes/{0}', objectTypeName);
                    CMDBuildUI.util.administration.MenuStoreBuilder.initialize(
                        function () {
                            var treestore = Ext.getCmp('administrationNavigationTree');
                            var selected = treestore.getStore().findNode("href", nextUrl);
                            treestore.setSelection(selected);
                        });
                    me.redirectTo(nextUrl, true);
                }
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        if (this.getViewModel().get('actions.edit')) {
            this.redirectTo(Ext.String.format('administration/classes/{0}', this.getViewModel().get('theObject._id')), true);
        } else if (this.getViewModel().get('actions.add')) {
            var store = Ext.getStore('administration.MenuAdministration');
            var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
            var currentNode = store.findNode("objecttype", CMDBuildUI.model.administration.MenuItem.types.klass);
            vm.set('selected', currentNode);
            this.redirectTo('administration/classes_empty', true);
        }
    },


    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('class.{0}.description', vm.get('objectTypeName'))
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-localization',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px',
                draggable: true
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() - 100);
    }
});