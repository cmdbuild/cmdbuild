Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.ContextMenusFieldset', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administration-content-classes-tabitems-properties-fieldsets-contextmenusfieldset',
    controller: 'administration-content-classes-tabitems-properties-fieldsets-contextmenusfieldset',

    viewModel: {
        type: 'administration-content-classes-tabitems-properties-fieldsets-contextmenusfieldset'
    },

    items: [{
        xtype: 'fieldset',
        collapsible: true,
        bind: {
            title: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.title + ' ({contextMenuCount})'
        },
        collapsed: true,
        layout: 'column',

        ui: 'administration-formpagination',
        items: [{
            columnWidth: 1,
            items: [{
                    xtype: 'components-grid-reorder-grid',
                    bind: {
                        store: '{theObject.contextMenuItems}'
                    },
                    columnWidth: 0.5,
                    reference: 'contextMenuGrid',
                    flex: 1,
                    columns: [{
                            flex: 1,
                            text: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.menuItemName.label,
                            width: "10%",
                            align: 'left',
                            dataIndex: 'label',
                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                switch (record.get('type')) {
                                    case 'separator':
                                        return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.menuItemName.values.separator.label;
                                    default:
                                        return record.get('label');
                                }
                            }
                        }, {
                            flex: 1,
                            text: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.typeOrGuiCustom.label,
                            width: "15%",
                            align: 'left',
                            dataIndex: 'type',
                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                switch (value) {
                                    case 'custom':
                                        return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.typeOrGuiCustom.values.custom.label;
                                    case 'component':
                                        record.set('script', record.get('config'));
                                        return Ext.String.htmlDecode(
                                            Ext.String.format(
                                                '{0}<br>{1}',
                                                CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.typeOrGuiCustom.values.component.label,
                                                record.get('componentId')
                                            )
                                        );
                                    case 'separator':
                                        return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.typeOrGuiCustom.values.separator.label; //'[---------]';
                                }
                            }
                        }, {
                            flex: 2,
                            text: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.javascriptScript.label,
                            width: "35%",
                            xtype: 'widgetcolumn',
                            align: 'left',
                            widget: {
                                xtype: 'aceeditortextarea',
                                inputField: 'script',
                                vmObjectName: 'record',
                                config: {
                                    options: {
                                        readOnly: '{!actions.edit}'
                                    }
                                },
                                bind: {
                                    value: '{record.script}'
                                }
                            }
                        },
                        {
                            //Applicability
                            flex: 1,
                            text: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.applicability.label,
                            width: "20%",
                            align: 'left',
                            dataIndex: 'visibility',
                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                switch (value) {
                                    case 'one':
                                        return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.applicability.values.one.label;
                                    case 'many':
                                        return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.applicability.values.many.label;
                                    case 'all':
                                        return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.applicability.values.all.label;
                                }
                            }
                        },
                        {
                            flex: 1,
                            text: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.status.label,
                            width: "10%",
                            xtype: 'widgetcolumn',
                            align: 'left',
                            dataIndex: 'active',
                            widget: {
                                xtype: 'checkbox', // textfield | combo | radio
                                bind: '{record.active}',
                                boxLabel: CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.inputs.status.values.active.label, // TODO: translate
                                readOnly: true
                            }
                        },
                        {
                            xtype: 'actioncolumn',
                            minWidth: 100,
                            maxWidth: 200,
                            width: "10%",
                            bind: {
                                hidden: '{!actions.edit}'
                            },
                            align: 'center',
                            items: [{
                                iconCls: 'x-fa fa-pencil',
                                handler: 'onEditBtn',
                                getTip: function (value, metadata, record, rowIndex, colIndex, store) {
                                    return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.actions.edit.tooltip;
                                }

                            }, {
                                iconCls: 'x-fa fa-arrow-up',
                                handler: function (grid, rowIndex, colIndex) {


                                    Ext.suspendLayouts();
                                    var store = grid.getStore();
                                    var record = store.getAt(rowIndex);
                                    rowIndex--;
                                    if (!record || rowIndex >= store.getCount()) {
                                        return;
                                    }
                                    store.remove(record);
                                    store.insert(rowIndex, record);


                                    this.getView().refresh();
                                    Ext.resumeLayouts(true);
                                },

                                getTip: function (value, metadata, record, rowIndex, colIndex, store) {

                                    return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.actions.moveUp.tooltip;
                                },
                                isDisabled: function (view, rowIndex, colIndex, item, record) {
                                    return rowIndex == 0;
                                }
                            }, {
                                iconCls: 'x-fa fa-arrow-down',
                                bind: {
                                    hidden: '{!actions.edit}'
                                },
                                handler: function (grid, rowIndex, colIndex) {
                                    Ext.suspendLayouts();
                                    var store = grid.getStore();
                                    var record = store.getAt(rowIndex);
                                    rowIndex++;
                                    if (!record || rowIndex >= store.getCount()) {
                                        return;
                                    }
                                    store.remove(record);
                                    store.insert(rowIndex, record);


                                    this.getView().refresh();
                                    Ext.resumeLayouts(true);
                                },
                                getTip: function (value, metadata, record, rowIndex, colIndex, store) {
                                    return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.actions.moveDown.tooltip;
                                },
                                isDisabled: function (view, rowIndex, colIndex, item, record) {
                                    return rowIndex >= view.store.getCount() - 1;
                                }
                            }, {
                                iconCls: 'x-fa fa-times',
                                bind: {
                                    hidden: '{!actions.edit}',
                                    disabled: '{isLastDisabledAddButton}'
                                },
                                handler: function (grid, rowIndex, colIndex, me, event, record, row) {
                                    var store = grid.getStore();
                                    store.remove(record);
                                    grid.updateLayout();
                                },

                                getTip: function (value, metadata, record, rowIndex, colIndex, store) {
                                    return CMDBuildUI.locales.Locales.administration.classes.properties.form.fieldsets.contextMenus.actions.delete.tooltip;
                                }
                            }]
                        }
                    ]
                },
                // FORM
                // TODO: translate
                {
                    columnWidth: 1,

                    items: [{
                        xtype: 'components-grid-reorder-grid',
                        bind: {
                            store: '{contextMenuItemsStoreNew}',
                            hidden: '{!actions.edit}'
                        },
                        columnWidth: 0.5,
                        flex: 1,

                        columns: [{
                                flex: 1,
                                text: 'Create new context action', // TODO: translate
                                xtype: 'widgetcolumn',
                                style: 'padding:0;',
                                align: 'left',
                                widget: {
                                    xtype: 'panel',
                                    align: 'left',
                                    bind: {
                                        data: '{record}'
                                    },
                                    items: [{
                                        width: '100%',
                                        xtype: 'textfield',
                                        inputField: 'label',
                                        bind: {
                                            value: '{record.label}'
                                        }
                                    }]
                                }
                            }, {
                                flex: 1,
                                xtype: 'widgetcolumn',
                                style: 'padding:0',
                                widget: {
                                    xtype: 'panel',
                                    align: 'left',
                                    bind: {
                                        data: '{record}'
                                    },
                                    dataIndex: 'type',
                                    items: [{
                                        xtype: 'combobox',
                                        inputField: 'type',
                                        width: '100%',
                                        style: "padding-top:0px",
                                        editable: false,
                                        forceSelection: true,
                                        allowBlank: false,
                                        displayField: 'label',
                                        valueField: 'value',

                                        bind: {
                                            value: '{record.type}',
                                            store: '{contextMenuItemTypeStore}'
                                        },
                                        listeners: {
                                            select: function (ele, rec, idx) {
                                                var isComponent = ele.getValue() === 'component';
                                                this.up().getWidgetRecord().data._isComponent = isComponent;
                                            }
                                        }

                                    }, {
                                        xtype: 'combobox',
                                        inputField: 'componentId',
                                        width: '100%',
                                        editable: false,
                                        forceSelection: true,
                                        allowBlank: false,
                                        displayField: 'label',
                                        valueField: 'value',
                                        hidden: true,
                                        bind: {
                                            value: '{record.componentId}',
                                            store: '{contextMenuComponentStore}',
                                            hidden: '{!record._isComponent}'
                                        }
                                    }]
                                }
                            }, {
                                flex: 2,
                                //text: 'Javascript script / custom GUI paramenters',
                                width: "35%",
                                xtype: 'widgetcolumn',
                                align: 'left',

                                bind: {
                                    hidden: '{actions.view}'
                                },
                                // widget: {
                                //     xtype: 'aceeditortextarea',
                                //     inputField: 'script',
                                //     elementId: 'newContextMenuScriptField',
                                //     reference: 'newContextMenuScriptField',
                                //     vmObjectName: 'record',
                                //     options: {
                                //         readOnly: false
                                //     },
                                //     bind: {
                                //         value: '{record.script}',
                                //         config: {
                                //             options: {
                                //                 editorId: 'newContextMenuScriptField',
                                //             },
                                //             readOnly: '{!actions.edit}'
                                //         }
                                //     }
                                // }
                                widget: {
                                    xtype: 'component',
                                    html: '<div id="newContextMenuScriptField" style="min-height:58px;height:100%;min-width:20px; width:100%"></div>',
                                    listeners: {
                                        afterrender: function (cmp) {
                                            var me = this;
                                            var editor = window.newContextMenuScriptField = ace.edit('newContextMenuScriptField');
            
                                            //set the theme
                                            //
                                            editor.setTheme('ace/theme/chrome');
            
                                            //set the mode
                                            //
                                            editor.getSession().setMode('ace/mode/javascript');
            
                                            //set some options
                                            //
                                            editor.setOptions({
                                                showLineNumbers: true,
                                                showPrintMargin: false
                                            });
            
                                            //set a value
                                            //
                                            editor.setValue('');
            
                                            editor.getSession().on('change', function (event, _editor) {
                                                var vm = me.up('administration-content-classes-tabitems-properties-fieldsets-contextmenusfieldset').getViewModel().getParent();
                                                vm.get('contextMenuItemsStoreNew').getData().items[0].set('script', _editor.getValue());
                                            });
            
            
                                        }
                                    }
                                }
                            },
                            {
                                flex: 1,
                                xtype: 'widgetcolumn',
                                style: 'padding:0',
                                widget: {
                                    xtype: 'panel',
                                    align: 'left',
                                    dataIndex: 'visibility',

                                    items: [{
                                        xtype: 'combobox',
                                        inputField: 'visibility',
                                        width: '100%',
                                        style: "padding-top:0px",
                                        editable: false,
                                        forceSelection: true,
                                        allowBlank: false,
                                        displayField: 'label',
                                        valueField: 'value',
                                        bind: {
                                            value: '{record.visibility}',
                                            store: '{contextMenuApplicabilityStore}'
                                        }
                                    }]
                                }
                            },
                            {
                                flex: 1,
                                //text: 'Status',
                                width: "10%",
                                xtype: 'widgetcolumn',
                                align: 'left',
                                dataIndex: 'active',
                                widget: {
                                    xtype: 'checkbox', // textfield | combo | radio
                                    bind: '{record.active}',
                                    boxLabel: 'Active' // TODO: translate
                                },
                                onWidgetAttach: function (column, widget, record) {
                                    widget.setVisible(record.get('type') !== 'separator');
                                }
                            },
                            {
                                xtype: 'actioncolumn',
                                minWidth: 100,
                                maxWidth: 200,
                                width: "10%",
                                align: 'center',
                                items: [{
                                    iconCls: 'x-fa fa-ellipsis-h',
                                    disabled: true

                                }, {
                                    iconCls: 'x-fa fa-ellipsis-h',
                                    disabled: true
                                }, {
                                    iconCls: 'x-fa fa-ellipsis-h',
                                    disabled: true
                                }, {
                                    iconCls: 'x-fa fa-plus',
                                    tooltip: 'Add',
                                    handler: 'onAddNewContextMenuBtn'
                                }]
                            }
                        ]
                    }]
                }
            ]
        }]
    }]
});