Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.FormWidgetFieldset', {
    extend: 'Ext.panel.Panel',
    controller: 'administration-content-classes-tabitems-properties-fieldsets-formwidgetfieldset',

    alias: 'widget.administration-content-classes-tabitems-properties-fieldsets-formwidgetfieldset',
    viewModel: {
        type: 'administration-content-classes-tabitems-properties-fieldsets-formwidgetfieldset'
    },
    items: [{
        xtype: 'fieldset',
        collapsible: true,
        collapsed: true,
        layout: 'column',
        bind: {
            title: 'Form Widgets ({formWidgetCount})'
        },
        ui: 'administration-formpagination',
        items: [{
            columnWidth: 1,
            items: [{
                    xtype: 'components-grid-reorder-grid',
                    bind: {
                        store: '{theObject.widgets}'
                    },
                    columnWidth: 0.5,
                    reference: 'formWidgetGrid',
                    flex: 1,
                    columns: [{
                            flex: 1,
                            text: 'Widget Name',
                            align: 'left',
                            dataIndex: '_label'
                        }, {
                            flex: 1,
                            text: 'GUI custom',
                            align: 'left',
                            dataIndex: '_type'
                        },
                        {
                            flex: 2,
                            text: 'GUI custom parameters',
                            xtype: 'widgetcolumn',
                            align: 'left',
                            widget: {
                                xtype: 'aceeditortextarea',
                                inputField: '_config',
                                vmObjectName: 'record',

                                config: {
                                    readOnly: true
                                },
                                bind: {
                                    value: '{record._config}',
                                    config: {
                                        readOnly: '{!actions.edit}'
                                    }
                                }
                            }
                        },
                        {
                            flex: 1,
                            text: 'Status',
                            xtype: 'widgetcolumn',
                            align: 'left',
                            dataIndex: '_active',
                            widget: {
                                xtype: 'checkbox', // textfield | combo | radio
                                bind: '{record._active}',
                                boxLabel: 'Active', // TODO: translate
                                readOnly: true
                            }
                        },
                        {
                            xtype: 'actioncolumn',
                            minWidth: 100,
                            maxWidth: 200,
                            bind: {
                                hidden: '{!actions.edit}'
                            },
                            align: 'center',
                            items: [{
                                iconCls: 'x-fa fa-pencil',
                                tooltip: 'Edit trigger',
                                handler: function (grid, rowIndex, colIndex) {
                                    console.log('edit'); // TODO: enable edit button
                                }
                            }, {
                                iconCls: 'x-fa fa-arrow-up',
                                tooltip: 'Move up',
                                handler: function (grid, rowIndex, colIndex) {
                                    Ext.suspendLayouts();
                                    var store = grid.getStore();
                                    var record = store.getAt(rowIndex);
                                    rowIndex--;
                                    if (!record || rowIndex < 0) {
                                        return;
                                    }

                                    store.remove(record);
                                    store.insert(rowIndex, record);

                                    this.getView().refresh();

                                },
                                isDisabled: function (view, rowIndex, colIndex, item, record) {
                                    return rowIndex == 0;
                                }
                            }, {
                                iconCls: 'x-fa fa-arrow-down',
                                tooltip: 'Move Down',
                                handler: function (grid, rowIndex, colIndex) {
                                    Ext.suspendLayouts();
                                    var store = grid.getStore();
                                    var record = store.getAt(rowIndex);
                                    rowIndex++;
                                    if (!record || rowIndex >= store.getCount()) {
                                        return;
                                    }
                                    store.remove(record);
                                    store.insert(rowIndex, record);


                                    this.getView().refresh();
                                },
                                isDisabled: function (view, rowIndex, colIndex, item, record) {
                                    return rowIndex >= view.store.getCount() - 1;
                                }
                            }, {
                                iconCls: 'x-fa fa-times',
                                tooltip: 'Remove',
                                handler: function (grid, rowIndex, colIndex) {
                                    Ext.suspendLayouts();
                                    var store = grid.getStore();

                                    var record = store.getAt(rowIndex);
                                    store.remove(record);
                                    this.getView().refresh();
                                }
                            }]
                        }
                    ]
                },
                {
                    columnWidth: 1,
                    items: [{

                        xtype: 'components-grid-reorder-grid',
                        bind: {
                            store: '{formWidgetsStoreNew}',
                            hidden: '{!actions.edit}'
                        },
                        columnWidth: 0.5,
                        flex: 1,
                        columns: [{
                                flex: 1,
                                text: 'Create new widget',
                                xtype: 'widgetcolumn',
                                align: 'left',
                                dataIndex: '_label',
                                widget: {
                                    xtype: 'textfield',
                                    label: '',
                                    inputField: '_label',
                                    bind: {
                                        value: '{record._label}'
                                    },
                                    listeners: {
                                        change: function (element, newValue, oldValue) {
                                            element.up('administration-content-classes-tabitems-properties-fieldsets-formwidgetfieldset').getViewModel().getParent().get('formWidgetsStoreNew').getData().items[0].set('_label', newValue);

                                        }
                                    }
                                }
                            }, {
                                flex: 1,
                                text: '',
                                xtype: 'widgetcolumn',
                                align: 'left',
                                dataIndex: '_type',
                                widget: {
                                    xtype: 'combobox',
                                    inputField: '_type',
                                    editable: false,
                                    forceSelection: true,
                                    allowBlank: false,
                                    displayField: 'label',
                                    valueField: 'value',
                                    bind: {
                                        value: '{record._type}',
                                        store: '{formWidgetTypeStore}'
                                    },
                                    listeners: {
                                        change: function (element, newValue, oldValue) {
                                            element.up('administration-content-classes-tabitems-properties-fieldsets-formwidgetfieldset').getViewModel().getParent().get('formWidgetsStoreNew').getData().items[0].set('_type', newValue);
                                        }
                                    }
                                }
                            },
                            {
                                flex: 2,
                                text: '',
                                xtype: 'widgetcolumn',
                                align: 'left',
                                // widget: {
                                //     xtype: 'aceeditortextarea',
                                //     inputField: '_config',
                                //     vmObjectName: 'record',
                                //     // reference: 'newFormWidgetScriptField',
                                //     config: {
                                //         editorId: 'newFormWidgetScriptField'
                                //     },
                                //     itemId: 'newFormWidgetScriptField',
                                //     bind: {
                                //         value: '{record._config}',
                                //     }
                                // }
                                widget: {
                                    xtype: 'component',
                                    html: '<div id="newFormWidgetScriptField" style="min-height:58px;height:100%;min-width:20px; width:100%"></div>',
                                    listeners: {
                                        afterrender: function (cmp) {
                                            var me = this;
                                            var editor = window.newFormWidgetScriptField = ace.edit('newFormWidgetScriptField');

                                            //set the theme
                                            //
                                            editor.setTheme('ace/theme/chrome');

                                            //set the mode
                                            //
                                            editor.getSession().setMode('ace/mode/javascript');

                                            //set some options
                                            //
                                            editor.setOptions({
                                                showLineNumbers: true,
                                                showPrintMargin: false
                                            });

                                            //set a value
                                            //
                                            editor.setValue('');

                                            editor.getSession().on('change', function (event, _editor) {
                                                var vm = me.up('administration-content-classes-tabitems-properties-fieldsets-formwidgetfieldset').getViewModel().getParent();
                                                vm.get('formWidgetsStoreNew').getData().items[0].set('_config', _editor.getValue());
                                            });
                                        }
                                    }
                                }
                            },
                            {
                                flex: 1,
                                text: '',
                                xtype: 'widgetcolumn',
                                align: 'left',
                                dataIndex: '_active',
                                widget: {
                                    xtype: 'checkbox', // textfield | combo | radio
                                    bind: '{record._active}',
                                    boxLabel: 'Active' // TODO: translate
                                }
                            },
                            {
                                xtype: 'actioncolumn',
                                minWidth: 100,
                                maxWidth: 200,

                                align: 'center',
                                items: [{
                                    iconCls: 'x-fa fa-ellipsis-h',
                                    disabled: true

                                }, {
                                    iconCls: 'x-fa fa-ellipsis-h',
                                    disabled: true
                                }, {
                                    iconCls: 'x-fa fa-ellipsis-h',
                                    disabled: true
                                }, {
                                    iconCls: 'x-fa fa-plus',
                                    tooltip: 'Add',
                                    handler: 'onAddNewWidgetMenuBtn'
                                    // handler: function (grid, rowIndex, colIndex) {
                                    //     console.log('form row widget add button');
                                    //     // TODO: complete action
                                    // }
                                }]
                            }
                        ]
                    }]
                }
            ]
        }]
    }]
});