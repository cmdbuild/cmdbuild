Ext.define('CMDBuildUI.view.administration.content.classes.tabitems.properties.fieldsets.FormWidgetFieldsetController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-classes-tabitems-properties-fieldsets-formwidgetfieldset',


    onAddNewWidgetMenuBtn: function (grid, rowIndex, colIndex) {

        var vm = this.getViewModel();
        var widgets = vm.get('theObject.widgets');
        var newWidgetStore = vm.get('formWidgetsStoreNew');
        var newWidget = newWidgetStore.getData().items[0];


        Ext.suspendLayouts();
        widgets.add(newWidget);
        newWidgetStore.removeAt(rowIndex);
        newWidgetStore.add(Ext.create('CMDBuildUI.model.WidgetDefinition', { script: '' }));
        grid.refresh();
        this.lookupReference('formWidgetGrid').view.grid.getView().refresh();
        //this.lookupReference('newFormWidgetScriptField').getAceEditor().getSession().setValue('');
        window.newFormWidgetScriptField.getSession().setValue('');

        Ext.resumeLayouts();

    }

});
