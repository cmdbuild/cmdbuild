Ext.define('CMDBuildUI.view.administration.content.setup.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-setup-view',

    data: {
        classList: [],
        functionList: [],
        isClassMode: false,
        isEditBtnHidden: false,
        isFunctionMode: false,
        isMultitenantEnabled: false,
        theSetup: {},
        actions: {
            view: true,
            edit: false
        }
    },
    formulas: {

        allPagesData: {
            get: function (get) {
                var data = [];
                var types = {
                    classes: {
                        label: 'Classes', // TODO: translate
                        childrens: Ext.getStore('classes.Classes').getData().getRange()
                    },
                    processes: {
                        label: 'Processes', // TODO: translate
                        childrens: Ext.getStore('processes.Processes').getData().getRange()
                    },
                    dashboards: {
                        label: 'Dasboards', // TODO: translate
                        childrens: Ext.getStore('Dashboards').getData().getRange()
                    },
                    custompages: {
                        label: 'Custom Pages', // TODO: translate
                        childrens: Ext.getStore('custompages.CustomPages').getData().getRange()
                    },
                    views: {
                        label: 'Views', // TODO: translate
                        childrens: Ext.getStore('views.Views').getData().getRange()
                    }
                };
                Object.keys(types).forEach(function (type, typeIndex) {
                    types[type].childrens.forEach(function (value, index) {
                        var item = {
                            group: type,
                            groupLabel: types[type].label, // TODO: translate
                            _id: value.get('_id'),
                            label: value.get('_description_translation') || value.get('description') // TODO: can be localized?
                        };
                        data.push(item);
                    });
                });
                data.sort(function (a, b) {
                    var aGroup = a.group.toUpperCase();
                    var bGroup = b.group.toUpperCase();
                    var aLabel = a.label.toUpperCase();
                    var bLabel = b.label.toUpperCase();

                    if (aGroup == bGroup) {
                        return (aLabel < bLabel) ? -1 : (aLabel > bLabel) ? 1 : 0;
                    } else {
                        return (aGroup < bGroup) ? -1 : 1;
                    }
                });
                return data;
            }
        },
        editBtnManager: {
            bind: {
                //isEditBtnHidden: '{isEditBtnHidden}',
                multiTenantEnabled: '{multiTenantEnabled}',
                currentPage: '{currentPage}',
                isEdit: '{actions.edit}'
            },
            get: function (data) {
                if (data.isEdit || (data.multiTenantEnabled && data.currentPage === 'multitenant') || data.currentPage === 'servermanagement') {
                    this.set('isEditBtnHidden', true);
                }
            }
        },

        /**
         * Multitenant configuration
         */

        multiTenantEnabled: {
            bind: '{theSetup.org__DOT__cmdbuild__DOT__multitenant__DOT__mode}',
            get: function (mode) {
                if (mode) {
                    switch (mode) {
                        case 'DISABLED':
                        case '':
                            this.set('isClassMode', false);
                            this.set('isFunctionMode', false);
                            return false;
                        case 'CMDBUILD_CLASS':
                            this.set('isClassMode', true);
                            this.set('isFunctionMode', false);
                            return true;
                        case 'DB_FUNCTION':
                            this.set('isClassMode', false);
                            this.set('isFunctionMode', true);
                            return true;
                        default:
                            return false;
                    }
                }
            },
            set: function (value) {
                switch (value) {
                    case true:
                        this.set('theSetup.org__DOT__cmdbuild__DOT__multitenant__DOT__mode', 'CMDBUILD_CLASS');
                        break;
                    default:
                        this.set('theSetup.org__DOT__cmdbuild__DOT__multitenant__DOT__mode', 'DISABLED');
                        break;
                }
            }
        },
        defaultProvider: {
            bind: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider}',
            get: function (mode) {
                if (mode) {
                    switch (mode) {
                        case 'shark':
                            this.set('isSharkDefault', true);
                            this.set('isRiverDefault', false);
                            break;
                        case 'river':
                            this.set('isSharkDefault', false);
                            this.set('isRiverDefault', true);
                            break;
                    }
                }
            },
            set: function (value) {

            }
        },
        sharkDefaultFieldDisabled: {
            bind: {
                sharkEnabled: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__shark__DOT__enabled}',
                riverEnabled: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__river__DOT__enabled}'
            },
            get: function (data) {
                if (data.sharkEnabled.toString() !== 'true' || data.riverEnabled.toString() !== 'true') {
                    return true;
                }
                return false;
            }
        },
        riverDefaultFieldDisabled: {
            bind: {
                sharkEnabled: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__shark__DOT__enabled}',
                riverEnabled: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__river__DOT__enabled}'
            },
            get: function (data) {
                if (data.sharkEnabled.toString() !== 'true' || data.riverEnabled.toString() !== 'true') {
                    return true;
                }
                return false;
            }
        }
    },

    stores: {
        getAllPagesStore: {
            data: '{allPagesData}',
            autoDestroy: true
        },
        getConfigurationModeStore: {
            data: [{
                value: 'CMDBUILD_CLASS',
                label: 'Connected to a class' // TODO: translate
            }, {
                value: 'DB_FUNCTION',
                label: 'Connected to a function'
            }],
            autoDestroy: true
        },
        getFilteredClasses: {
            model: 'CMDBuildUI.model.classes.Class',
            autoLoad: true,
            autoDestroy: true,
            sorters: ['description'],
            pageSize: 0,
            filters: [function (item) {
                return item.data.name !== 'Class';
            }]
        }
    },

    /**
     * Change form mode
     * 
     * @param {String} mode
     */
    setFormMode: function (mode) {
        var me = this;
        if(me.get('actions.edit') && mode === 'view'){
            this.set('isEditBtnHidden', false);
        }

        switch (mode) {
            case 'edit':
                me.set('actions.view', false);
                me.set('actions.edit', true);
                break;

            default:
                me.set('actions.view', true);
                me.set('actions.edit', false);
                break;
        }
    }
});