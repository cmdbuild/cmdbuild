Ext.define('CMDBuildUI.view.administration.content.setup.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-setup-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.setup.View} view
     */
    onBeforeRender: function (view) {
        var vm = this.getView().getViewModel();
        vm.setFormMode('view');
        this.loadData();
        view.down('panel').add({
            xtype: Ext.String.format('administration-content-setup-elements-{0}', vm.get('currentPage'))
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEditSetupBtnClick: function (button, e, eOpts) {
        this.getView().getViewModel().setFormMode('edit');
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        this.getView().getViewModel().setFormMode('view');
        this.loadData();

    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        button.disable();
        if (button.up().up().getViewModel().get('currentPage') === 'multitenant') {
            this.saveMultitenantData(button);
        } else {
            this.saveData(button);
        }
    },

    privates: {
        /**
         * Load data from server, format keys and set vm data for binding
         * @private
         */
        loadData: function () {
            var vm = this.getView().getViewModel();
            CMDBuildUI.util.administration.helper.ConfigHelper.getConfig().then(
                function (configs) {
                    configs.forEach(function (key) {
                        vm.set(Ext.String.format('theSetup.{0}', key._key), (key.hasValue) ? key.value : key.default);
                    });
                }
            );
        },
        /**
         * Save the configuration
         * 
         * @param {Ext.button.Button} button 
         */
        saveData: function (button) {
            var vm = this.getView().getViewModel();
            CMDBuildUI.util.administration.helper.ConfigHelper.setConfig(vm.get('theSetup')).then(
                function (transport) {
                    vm.setFormMode('view');
                    button.enable();
                }
            );
        },

        /**
         * Save the configuration of multitenant
         * 
         * @param {Ext.button.Button} button 
         */
        saveMultitenantData: function (button) {
            var vm = this.getView().getViewModel();
            CMDBuildUI.util.administration.helper.ConfigHelper.setMultinantData(vm.get('theSetup')).then(
                function (transport) {
                    vm.setFormMode('view');
                    button.enable();
                }
            );
        }
    }
});