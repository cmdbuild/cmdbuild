
Ext.define('CMDBuildUI.view.administration.content.setup.elements.Gis',{
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.setup.elements.GisController',
        'CMDBuildUI.view.administration.content.setup.elements.GisModel'
    ],
    alias: 'widget.administration-content-setup-elements-gis',
    controller: 'administration-content-setup-elements-gis',
    viewModel: {
        type: 'administration-content-setup-elements-gis'
    },

    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Generals", // TODO: translate
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    /********************* org.cmdbuild.gis.enabled **********************/
                    xtype: 'checkbox',
                    fieldLabel: 'Enabled',
                    name: 'isEnabled',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__gis__DOT__enabled}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    /********************* org.cmdbuild.gis.initialLatitude **********************/
                    xtype: 'displayfield',
                    // TODO: add nord to label?
                    fieldLabel: 'Initial latitude',
                    name: 'gisInitialLatitude',
                    hidden: true,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__gis__DOT__initialLatitude}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    // TODO: add nord to label?
                    fieldLabel: 'Initial latitude',
                    name: 'gisInitialLatitude',
                    hidden: true,
                    allowExponential: false,
                    allowDecimal: true,
                    decimalPrecision: 8,
                    decimalSeparator:'.',
                    hideTrigger: true,
                    minValue: -90,
                    maxValue: 90,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__gis__DOT__initialLatitude}',
                        hidden: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    /********************* org.cmdbuild.gis.initialLongitude **********************/
                    xtype: 'displayfield',
                    // TODO: add east to label?
                    fieldLabel: 'Initial longitude',
                    name: 'gisInitialLongitude',
                    hidden: true,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__gis__DOT__initialLongitude}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    // TODO: add east to label?
                    fieldLabel: 'Initial longitude',
                    name: 'gisInitialLongitude',
                    hideTrigger: true,
                    hidden: true,
                    allowExponential: false,
                    allowDecimal: true,
                    decimalPrecision: 8,
                    decimalSeparator:'.',
                    minValue: -180,
                    maxValue: 180,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__gis__DOT__initialLongitude}',
                        hidden: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    /********************* org.cmdbuild.gis.initialZoom **********************/
                    xtype: 'displayfield',
                    fieldLabel: 'Initial zoom',
                    name: 'gisInitialZoom',
                    hidden: true,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__gis__DOT__initialZoom}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    allowExponential: false,
                    allowDecimal: false,
                    minValue: 0,
                    maxValue: 25,
                    step: 1,
                    fieldLabel: 'Initial zoom',
                    name: 'gisInitialZoom',
                    hidden: true,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__gis__DOT__initialZoom}',
                        hidden: '{actions.view}'
                    }
                }]
            }]
        }]
    }]
});
