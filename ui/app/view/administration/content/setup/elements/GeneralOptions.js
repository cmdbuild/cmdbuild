Ext.define('CMDBuildUI.view.administration.content.setup.elements.GeneralOptions', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.setup.elements.GeneralOptionsController',
        'CMDBuildUI.view.administration.content.setup.elements.GeneralOptionsModel'
    ],

    alias: 'widget.administration-content-setup-elements-generaloptions',
    controller: 'administration-content-setup-elements-generaloptions',
    viewModel: {
        type: 'administration-content-setup-elements-generaloptions'
    },

    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Generals", // TODO: translate
        items: [{
            layout: 'hbox',
            items: [{
                flex: 1,
                marginRight: 20,
                items: [{
                    xtype: 'displayfield',
                    name: 'instanceName',
                    fieldLabel: 'Instance name',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__instance_name}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'container',
                    fieldLabel: 'Instance name',
                    layout: 'hbox',
                    bind: {
                        hidden: '{actions.view}'
                    },
                    items: [{
                        flex: 5,
                        fieldLabel: 'Instance name',
                        xtype: 'textfield',
                        name: 'instanceName',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__instance_name}'
                        }
                    }, {
                        xtype: 'fieldcontainer',
                        flex: 1,
                        fieldLabel: '&nbsp;',
                        labelSeparator: '',
                        items: [{
                            xtype: 'button',
                            reference: 'translateBtn',
                            iconCls: 'x-fa fa-flag fa-2x',
                            style: 'padding-left: 5px',

                            tooltip: 'Translate',
                            config: {
                                theSetup: null
                            },
                            viewModel: {
                                data: {
                                    theSetup: null,
                                    vmKey: 'theSetup'
                                }
                            },
                            bind: {
                                theSetup: '{theSetup}'
                            },
                            listeners: {
                                click: 'onTranslateClick'
                            },
                            cls: 'administration-field-container-btn',
                            autoEl: {
                                'data-testid': 'administration-lookupvalue-card-edit-translateBtn'
                            }
                        }]

                    }]
                }]
            }, {
                //columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                flex: 1,
                items: [{
                    xtype: 'displayfield',
                    name: 'initialPage',
                    fieldLabel: 'Default page',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__startingclass}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'combo',
                    fieldLabel: 'Default page',
                    name: 'initialPage',
                    valueField: '_id',
                    displayField: 'label',
                    queryMode: 'local',
                    typeAhead: true,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__startingclass}',
                        store: '{getAllPagesStore}',
                        hidden: '{actions.view}'
                    },
                    triggers: {
                        clearField: {
                            cls: 'x-form-clear-trigger',
                            handler: function () {
                                this.clearValue();
                            }
                        }
                    },
                    tpl: new Ext.XTemplate(
                        '<tpl for=".">',
                        '<tpl for="group" if="this.shouldShowHeader(group)"><div class="group-header">{[this.showHeader(values.group)]}</div></tpl>',
                        '<div class="x-boundlist-item">{label}</div>',
                        '</tpl>', {
                            shouldShowHeader: function (group) {
                                return this.currentGroup !== group;
                            },
                            showHeader: function (group) {
                                this.currentGroup = group;
                                return group.replace(/\b\w/g, function (l) {
                                    return l.toUpperCase();
                                });
                            }
                        }
                    )
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'displayfield',
                    name: 'relationLimit',
                    fieldLabel: 'Relation Limit',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__relationlimit}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    name: 'relationLimit',
                    fieldLabel: 'Relation limit',
                    minValue: 0, //prevents negative numbers
                    step: 10,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__relationlimit}',
                        hidden: '{actions.view}'
                    }
                }]

            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    xtype: 'displayfield',
                    name: 'referenceComboLimit',
                    fieldLabel: 'Reference combobox limit',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__referencecombolimit}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    name: 'referenceComboLimit',
                    fieldLabel: 'Reference combobox limit',
                    minValue: 0, //prevents negative numbers
                    step: 100,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__referencecombolimit}',
                        hidden: '{actions.view}'
                    }
                }]
            }]

        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'displayfield',
                    name: 'sessionTimeout',
                    fieldLabel: 'Session timeout',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__session__DOT__timeout}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    name: 'sessionTimeout',
                    fieldLabel: 'Session timeout',
                    minValue: 0, //prevents negative numbers
                    step: 60,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__session__DOT__timeout}',
                        hidden: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Note inline',
                    name: 'noteInline',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__noteInline}',
                        readOnly: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Note inline default closed',
                    name: 'noteInlineClosed',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__noteInlineClosed}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Lock management", // TODO: translate

        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Enabled',
                    name: 'lockcardenabled',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__lockcardenabled}',
                        readOnly: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Shows the name of the user who blocked the card',
                    name: 'lockcarduservisible',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__lockcarduservisible}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'displayfield',
                    name: 'lockcardtimeout',
                    fieldLabel: 'Maximum lock time (seconds)',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__lockcardtimeout}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    name: 'lockcardtimeout',
                    fieldLabel: 'Maximum lock time (seconds)',
                    minValue: 0, //prevents negative numbers
                    step: 10,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__lockcardtimeout}',
                        hidden: '{actions.view}'
                    }
                }]
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Grid autorefresh", // TODO: translate
        hidden: true, // TODO: Temporarily disabled (Fabio 5/11/18) 
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Enabled',
                    name: 'lockcardenabled',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__gridAutorefresh}',
                        readOnly: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    xtype: 'displayfield',
                    name: 'lockcardtimeout',
                    fieldLabel: 'Frequency (seconds)',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__gridAutorefreshFrequency}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'numberfield',
                    name: 'lockcardtimeout',
                    fieldLabel: 'Frequency (seconds)',
                    minValue: 0, //prevents negative numbers
                    step: 10,
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__core__DOT__gridAutorefreshFrequency}',
                        hidden: '{actions.view}'
                    }
                }]
            }]
        }]
    }]
});