Ext.define('CMDBuildUI.view.administration.content.setup.elements.ServerManagement', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.setup.elements.ServerManagementController',
        'CMDBuildUI.view.administration.content.setup.elements.ServerManagementModel'
    ],
    alias: 'widget.administration-content-setup-elements-servermanagement',
    controller: 'administration-content-setup-elements-servermanagement',
    viewModel: {
        type: 'administration-content-setup-elements-servermanagement'
    },

    items: [{
        //xtype: 'tabpanel',
        xtype: 'container',

        // tabPosition: 'top',
        // tabRotation: 0,
        // activeTab: 0,
        //ui: 'administration-tabandtools',
        ui: 'administration-formpagination',
        //style: 'border-top:0',
        scrollable: true,
        forceFit: true,
        // borderTop: 0,
        //title: "Generals", // TODO: translate

        // items: [{
        //     padding: 10,
        //     items: [{
        items: [{
            style: 'margin-top:15px',
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'button',
                    ui: 'administration-action-small',
                    text: 'Drop cache',
                    iconCls: 'x-fa fa-wrench',
                    handler: 'onDropCacheBtnClick'
                }]
            }]
        }, {
            style: 'margin-top:15px',
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'button',
                    ui: 'administration-action-small',
                    text: 'Synchronize services',
                    iconCls: 'x-fa fa-wrench',
                    handler: 'onSyncServicesBtnClick'
                }]
            }]
        }, {
            style: 'margin-top:15px',
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'button',
                    ui: 'administration-action-small',
                    text: 'Unlock all cards',
                    iconCls: 'x-fa fa-wrench',
                    handler: 'onSyncServicesBtnClick'
                }]
            }]
        }]
    }]
    //     }]
    // }]
});