Ext.define('CMDBuildUI.view.administration.content.setup.elements.DocumentManagementSystem', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.setup.elements.DocumentManagementSystemController',
        'CMDBuildUI.view.administration.content.setup.elements.DocumentManagementSystemModel'
    ],

    alias: 'widget.administration-content-setup-elements-documentmanagementsystem',
    controller: 'administration-content-setup-elements-documentmanagementsystem',
    viewModel: {
        type: 'administration-content-setup-elements-documentmanagementsystem'
    },

    items: [{
            ui: 'administration-formpagination',
            xtype: "fieldset",
            collapsible: true,
            title: "Generals", // TODO: translate
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        xtype: 'checkbox',
                        fieldLabel: 'Enabled',
                        name: 'isEnabled',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__enabled}',
                            readOnly: '{actions.view}'
                        }
                    }]
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        xtype: 'displayfield',
                        fieldLabel: 'Category Lookup',
                        name: 'attachmentTypeLookup',
                        hidden: true,
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__category__DOT__lookup}',
                            hidden: '{!actions.view}'
                        }
                    }, {
                        /********************* Category Lookup **********************/
                        xtype: 'combobox',
                        queryMode: 'local',
                        forceSelection: true,
                        displayField: 'name',
                        valueField: '_id',
                        fieldLabel: 'CMDBuild category (default lookup)',
                        name: 'attachmentTypeLookup',
                        hidden: true,
                        bind: {
                            store: '{attachmentTypeLookupStore}',
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__category__DOT__lookup}',
                            hidden: '{actions.view}'
                        }
                    }]
                }, {
                    cmdbuildtype: 'column',
                    columnWidth: 0.5,
                    style: {
                        paddingLeft: '15px'
                    },
                    items: [{
                        /********************* Description Mode **********************/
                        xtype: 'combobox',
                        fieldLabel: 'Description Mode',
                        name: 'attachmentDescriptionMode',
                        editable: false,
                        store: Ext.create('Ext.data.Store', {
                            fields: ['value', 'label'],
                            data: [{
                                "value": "hidden",
                                "label": "Hidden"
                            }, {
                                "value": "optional",
                                "label": "Visible optional" // TODO: translate
                            }, {
                                "value": "mandatory",
                                "label": "Visible madatory" // TODO: translate
                            }]
                        }),
                        queryMode: 'local',
                        displayField: 'label',
                        valueField: 'value',
                        hidden: true,
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__category__DOT__lookupDescriptionMode}',
                            hidden: '{actions.view}'
                        }
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'Description Mode',
                        name: 'attachmentDescriptionMode',
                        hidden: true,
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__category__DOT__lookupDescriptionMode}',
                            hidden: '{!actions.view}'
                        },
                        renderer: function (value) {
                            switch (value) {
                                case 'mandatory':
                                    return "Visible madatory";
                                case 'optional':
                                    return "Visible optional";
                                case 'hidden':
                                    return 'Hidden';
                                default:
                                    return value;
                            }
                        }
                    }]
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        /********************* Service Type **********************/
                        xtype: 'displayfield',
                        fieldLabel: 'Service type',
                        name: 'dmsServiceType',
                        hidden: true,
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__type}',
                            hidden: '{!actions.view}'
                        },
                        renderer: function (value) {
                            switch (value) {
                                case 'cmis':
                                    return "CMIS";
                                case 'alfresco':
                                    return "Alfresco";
                                case 'postgres':
                                    return 'Postgres';
                                default:
                                    return value;
                            }
                        }
                    }, {
                        xtype: 'combobox',
                        fieldLabel: 'Service type',
                        name: 'dmsServiceType',
                        editable: false,
                        store: Ext.create('Ext.data.Store', {
                            fields: ['value', 'label'],
                            data: [
                                // {
                                //     "value": "alfresco",
                                //     "label": "Afresco (v. 3.4 or lower)"
                                // }, 
                                {
                                    "value": "cmis",
                                    "label": "CMIS" // TODO: translate
                                }
                                // {
                                //     "value": "postgres",
                                //     "label": "Postgres" // sperimental
                                // }
                            ]
                        }),
                        queryMode: 'local',
                        displayField: 'label',
                        valueField: 'value',
                        hidden: true,
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__type}',
                            hidden: '{actions.view}'
                        }
                    }]
                }]
            }]
        }, {
            ui: 'administration-formpagination',
            xtype: "fieldset",
            collapsible: true,
            title: "CMIS",
            hidden: true,
            bind: {
                hidden: '{!isCmis}'
            },
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        xtype: 'displayfield',
                        name: 'cmisHost',
                        fieldLabel: 'Host',
                        bind: {
                            fieldLabel: 'is cmis: {isCmis}',
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__url}',
                            hidden: '{!actions.view}'
                        }
                    }, {
                        xtype: 'textfield',
                        name: 'cmisHost',
                        fieldLabel: 'Host',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__url}',
                            hidden: '{actions.view}'
                        }
                    }]
                }, {
                    columnWidth: 0.5,
                    style: {
                        paddingLeft: '15px'
                    },
                    items: [{
                        xtype: 'displayfield',
                        name: 'webServicePath',
                        fieldLabel: 'Host',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__path}',
                            hidden: '{!actions.view}'
                        }
                    }, {
                        xtype: 'textfield',
                        name: 'webServicePath',
                        fieldLabel: 'Webservice path',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__path}',
                            hidden: '{actions.view}'
                        }
                    }]
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        xtype: 'displayfield',
                        name: 'username',
                        fieldLabel: 'Username',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__user}',
                            hidden: '{!actions.view}'
                        }
                    }, {
                        xtype: 'textfield',
                        name: 'username',
                        fieldLabel: 'Username',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__user}',
                            hidden: '{actions.view}'
                        }
                    }]
                }, {
                    columnWidth: 0.5,
                    style: {
                        paddingLeft: '15px'
                    },
                    items: [{
                        xtype: 'displayfield',
                        name: 'password',
                        fieldLabel: 'Password',
                        value: '****',
                        bind: {
                            hidden: '{!actions.view}'
                        }
                    }, {
                        xtype: 'textfield',
                        inputType: 'password',
                        name: 'password',
                        fieldLabel: 'Password',

                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__password}',
                            hidden: '{actions.view}'
                        },
                        triggers: {
                            showPassword: {
                                cls: 'x-fa fa-eye',
                                hideTrigger: false,
                                scope: this,
                                handler: function (field, button, e) {
                                    // set the element type to text
                                    field.inputEl.el.set({
                                        type: 'text'
                                    });
                                    field.getTrigger('showPassword').setVisible(false);
                                    field.getTrigger('hidePassword').setVisible(true);
                                }
                            },
                            hidePassword: {
                                cls: 'x-fa fa-eye-slash',
                                hidden: true,
                                scope: this,
                                handler: function (field, button, e) {
                                    // set the element type to password
                                    field.inputEl.el.set({
                                        type: 'password'
                                    });
                                    field.getTrigger('showPassword').setVisible(true);
                                    field.getTrigger('hidePassword').setVisible(false);
                                }
                            }
                        }
                    }]
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    items: [{
                        xtype: 'displayfield',
                        name: 'cmisPresets',
                        fieldLabel: 'Preset',
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__model}',
                            hidden: '{!actions.view}'
                        },
                        renderer: function (value) {
                            switch (value) {
                                case 'alfresco':
                                    return "Alfresco";
                                default:
                                    return value;
                            }
                        }
                    }, {
                        xtype: 'combobox',
                        fieldLabel: 'Preset',
                        name: 'cmisPresets',
                        editable: false,
                        store: Ext.create('Ext.data.Store', { // TODO: remove static data after #683 issue resolution
                            fields: ['value', 'label'],
                            data: [{
                                "value": "alfresco",
                                "label": "Afresco"
                            }]
                        }),
                        queryMode: 'local',
                        displayField: 'label',
                        valueField: 'value',
                        hidden: true,
                        bind: {
                            value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__model}',
                            hidden: '{actions.view}'
                        }
                    }]
                }]
            }]
        }
        // , {
        //     ui: 'administration-formpagination',
        //     xtype: "fieldset",
        //     collapsible: true,
        //     title: "Credentials", // TODO: translate
        //     hidden: true,
        //     bind:  {
        //         hidden: '{!isAlfresco}'
        //     },
        //     items: [{
        //         layout: 'column',
        //         items: [{
        //             columnWidth: 0.5,
        //             items: [{
        //                 xtype: 'displayfield',
        //                 name: 'username',
        //                 fieldLabel: 'Username',
        //                 bind: {
        //                     value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__user}',
        //                     hidden: '{!actions.view}'
        //                 }
        //             }, {
        //                 xtype: 'textfield',
        //                 name: 'username',
        //                 fieldLabel: 'Username',
        //                 bind: {
        //                     value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__user}',
        //                     hidden: '{actions.view}'
        //                 }
        //             }]
        //         }, {
        //             columnWidth: 0.5,
        //             style: {
        //                 paddingLeft: '15px'
        //             },
        //             items: [{
        //                 xtype: 'displayfield',
        //                 name: 'password',
        //                 fieldLabel: 'Password',
        //                 value: '****',
        //                 bind: {
        //                     hidden: '{!actions.view}'
        //                 }
        //             }, {
        //                 xtype: 'textfield',
        //                 inputType: 'password',
        //                 name: 'password',
        //                 fieldLabel: 'Password',

        //                 bind: {
        //                     value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__service__DOT__cmis__DOT__password}',
        //                     hidden: '{actions.view}'
        //                 },
        //                 triggers: {
        //                     showPassword: {
        //                         cls: 'x-fa fa-eye',
        //                         hideTrigger: false,
        //                         scope: this,
        //                         handler: function (field, button, e) {
        //                             // set the element type to text
        //                             field.inputEl.el.set({
        //                                 type: 'text'
        //                             });
        //                             field.getTrigger('showPassword').setVisible(false);
        //                             field.getTrigger('hidePassword').setVisible(true);
        //                         }
        //                     },
        //                     hidePassword: {
        //                         cls: 'x-fa fa-eye-slash',
        //                         hidden: true,
        //                         scope: this,
        //                         handler: function (field, button, e) {
        //                             // set the element type to password
        //                             field.inputEl.el.set({
        //                                 type: 'password'
        //                             });
        //                             field.getTrigger('showPassword').setVisible(true);
        //                             field.getTrigger('hidePassword').setVisible(false);
        //                         }
        //                     }
        //                 }
        //             }]
        //         }]
        //     }]
        // }, {
        //     ui: 'administration-formpagination',
        //     xtype: "fieldset",
        //     collapsible: true,
        //     title: "FTP", // TODO: translate
        //     hidden: true,
        //     bind:  {
        //         hidden: '{!isAlfresco}'
        //     },
        //     items: [{
        //         layout: 'column',
        //         items: [{
        //             columnWidth: 0.5,
        //             // org.cmdbuild.dms.fileserver.url
        //             items: [{
        //                 xtype: 'displayfield',
        //                 name: 'ftpHost',
        //                 fieldLabel: 'Host',
        //                 bind: {
        //                     value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__fileserver__DOT__url}',
        //                     hidden: '{!actions.view}'
        //                 }
        //             }, {
        //                 xtype: 'numberfield',
        //                 name: 'ftpHost',
        //                 fieldLabel: 'Host',
        //                 bind: {
        //                     value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__fileserver__DOT__url}',
        //                     hidden: '{!actions.view}'
        //                 }
        //             }]
        //         }, {
        //             columnWidth: 0.5,
        //             style: {
        //                 paddingLeft: '15px'
        //             },
        //             items: [{
        //                 xtype: 'displayfield',
        //                 name: 'ftpPort',
        //                 fieldLabel: 'Port',
        //                 bind: {
        //                     value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__fileserver__DOT__port}',
        //                     hidden: '{!actions.view}'
        //                 }
        //             }, {
        //                 xtype: 'numberfield',
        //                 name: 'ftpPort',
        //                 fieldLabel: 'Port',

        //                 bind: {
        //                     value: '{theSetup.org__DOT__cmdbuild__DOT__dms__DOT__fileserver__DOT__port}',
        //                     hidden: '{actions.view}'
        //                 }
        //             }]
        //         }]
        //     }]
        // }
    ]
});