Ext.define('CMDBuildUI.view.administration.content.setup.MultiTenant', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.setup.elements.MultiTenantController',
        'CMDBuildUI.view.administration.content.setup.elements.MultiTenantModel'
    ],
    alias: 'widget.administration-content-setup-elements-multitenant',
    controller: 'administration-content-setup-elements-multitenant',
    viewModel: {
        type: 'administration-content-setup-elements-multitenant'
    },

    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Generals", // TODO: translate
        items:[{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Enabled',
                    name: 'enabled',
                    bind: {
                        value: '{multiTenantEnabled}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'combo',
                    fieldLabel: 'Configuration mode',
                    allowBlank: false,
                    displayField: 'label',
                    valueField: 'value',
                    bind: {
                        store: '{getConfigurationModeStore}',
                        value: '{theSetup.org__DOT__cmdbuild__DOT__multitenant__DOT__mode}',
                        hidden: '{!multiTenantEnabled}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'combo',
                    fieldLabel: 'Class',
                    allowBlank: false,
                    editable: false,
                    displayField: 'description',
                    valueField: 'name',
                    bind: {
                        store: '{getFilteredClasses}',
                        value: '{theSetup.org__DOT__cmdbuild__DOT__multitenant__DOT__tenantClass}',
                        hidden: '{!isClassMode}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'displayfield',
                    fieldLabel: 'Function',
                    value: '_cm3_multitenant_get_tenants_for_user',
                    bind: {
                        hidden: '{!isFunctionMode}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }]
    }]
});