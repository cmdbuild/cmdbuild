Ext.define('CMDBuildUI.view.administration.content.setup.elements.ServerManagementController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-setup-elements-servermanagement',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function (view) {
        view.up('administration-content-setup-view').getViewModel().setFormMode('view');
        view.up('administration-content').getViewModel().set('title', 'Server management');
    },

    onDropCacheBtnClick: function (button, e, eOpts) {
        Ext.Ajax.request({
            url: CMDBuildUI.util.Config.baseUrl + '/system/cache/drop',
            method: 'POST',
            success: function (transport) {
                CMDBuildUI.util.Notifier.showSuccessMessage('The server cache memory has been emptied', null, 'administration');
            }
        });
    },

    onSyncServicesBtnClick: function (button, e, eOpts) {
        // TODO: make ajax request when #686 was closed
        // show fake message
        setTimeout(function () {
            CMDBuildUI.util.Notifier.showSuccessMessage('The services are now synchronized', null, 'administration');
        }, 1000);
    },

    onUnlockAllCardsBtnClick: function (button, e, eOpts) {
        // TODO: make ajax request when #686 was closed
        // show fake message
        setTimeout(function () {
            CMDBuildUI.util.Notifier.showSuccessMessage('All the cards have been unlocked', null, 'administration');
        }, 700);
    }

});