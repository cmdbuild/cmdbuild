Ext.define('CMDBuildUI.view.administration.content.setup.elements.DocumentManagementSystemController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-setup-elements-documentmanagementsystem',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function(view){
        view.up('administration-content').getViewModel().set('title', 'DMS');
        view.up('administration-content-setup-view').getViewModel().setFormMode('view');
    }
    
    
});
