Ext.define('CMDBuildUI.view.administration.content.setup.elements.Workflow', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.setup.elements.WorkflowController',
        'CMDBuildUI.view.administration.content.setup.elements.WorkflowModel'
    ],
    alias: 'widget.administration-content-setup-elements-workflow',
    controller: 'administration-content-setup-elements-workflow',
    viewModel: {
        type: 'administration-content-setup-elements-workflow'
    },

    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: "Generals", // TODO: translate
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Enabled',
                    name: 'enabled',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__enabled}',
                        readOnly: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: []
            }]

        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Enable "Add attachment" to closed activities',
                    name: 'enableAddAttachmentOnClosedActivities',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__enableAddAttachmentOnClosedActivities}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'User can disable',
                    name: 'userCanDisable',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__userCanDisable}',
                        readOnly: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Hide "Save" button',
                    name: 'hideSaveButton',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__hideSaveButton}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        collapsed: false,
        title: "Tecnoteca River", // TODO: translate
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Enabled',
                    name: 'riverEnabled',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__river__DOT__enabled}',
                        readOnly: '{actions.view}'
                    },
                    listeners: {
                        change: function (checkbox, newValue, oldValue) {
                            var formView = this.up('administration-content-setup-elements-workflow');
                            var vm = formView.getViewModel().getParent();
                            switch (newValue) {
                                case true:
                                    if (vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider') === "shark" &&
                                        vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__shark__DOT__enabled').toString() === 'false'
                                    ) {
                                        vm.set('isSharkDefault', false);
                                        vm.set('isRiverDefault', true);
                                        vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'river');
                                        formView.lookupReference('riverDefault').setValue(true);
                                    }
                                    break;
                                case false:
                                    if (vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider') === "river" &&
                                        vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__shark__DOT__enabled').toString() == 'true') {
                                        vm.set('isRiverDefault', false);
                                        vm.set('isSharkDefault', true);
                                        vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'shark');
                                        formView.lookupReference('sharkDefault').setValue(true);
                                    }
                                    break;
                            }
                        }
                    }
                }]
            }, {
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    style: {
                        paddingLeft: '15px'
                    },
                    fieldLabel: 'Default',
                    name: 'riverDefault',
                    reference: 'riverDefault',
                    bind: {
                        value: '{isRiverDefault}',
                        readOnly: '{actions.view}',
                        disabled: '{riverDefaultFieldDisabled}'
                    },
                    listeners: {
                        change: function (checkbox, newValue, oldValue) {
                            var formView = this.up('administration-content-setup-elements-workflow');
                            var vm = formView.getViewModel().getParent();
                            switch (newValue) {
                                case true:
                                    // vm.set('isSharkDefault', false);
                                    // vm.set('isRiverDefault', true);
                                    vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'river');
                                    formView.lookupReference('sharkDefault').setValue(false);
                                    formView.lookupReference('riverDefault').setValue(true);
                                    break;
                                case false:
                                    if (vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__shark__DOT__enabled').toString() === "true") {
                                        // vm.set('isSharkDefault', true);
                                        // vm.set('isRiverDefault', false);
                                        vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'shark');
                                        formView.lookupReference('sharkDefault').setValue(true);
                                        formView.lookupReference('riverDefault').setValue(false);
                                    } else {
                                        // vm.set('isSharkDefault', false);
                                        // vm.set('isRiverDefault', true);

                                        // formView.lookupReference('riverDefault').setValue(true);
                                        // formView.lookupReference('sharkDefault').setValue(false);
                                    }
                                    break;
                            }
                        }
                    }
                }]
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        collapsed: true,
        title: "Enhydra Shark", // TODO: translate
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'displayfield',
                    name: 'username',
                    fieldLabel: 'Username',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__user}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'textfield',
                    name: 'username',
                    fieldLabel: 'Username',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__user}',
                        hidden: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    xtype: 'displayfield',
                    name: 'password',
                    fieldLabel: 'Password',
                    value: '****',
                    bind: {
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'textfield',
                    inputType: 'password',
                    name: 'password',
                    fieldLabel: 'Password',

                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__password}',
                        hidden: '{actions.view}'
                    },
                    triggers: {
                        showPassword: {
                            cls: 'x-fa fa-eye',
                            hideTrigger: false,
                            scope: this,
                            handler: function (field, button, e) {
                                // set the element type to text
                                field.inputEl.el.set({
                                    type: 'text'
                                });
                                field.getTrigger('showPassword').setVisible(false);
                                field.getTrigger('hidePassword').setVisible(true);
                            }
                        },
                        hidePassword: {
                            cls: 'x-fa fa-eye-slash',
                            hidden: true,
                            scope: this,
                            handler: function (field, button, e) {
                                // set the element type to password
                                field.inputEl.el.set({
                                    type: 'password'
                                });
                                field.getTrigger('showPassword').setVisible(true);
                                field.getTrigger('hidePassword').setVisible(false);
                            }
                        }
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'displayfield',
                    name: 'serviceUrl',
                    fieldLabel: 'Service URL',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__endpoint}',
                        hidden: '{!actions.view}'
                    }
                }, {
                    xtype: 'textfield',
                    name: 'serviceUrl',
                    fieldLabel: 'Service URL',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__endpoint}',
                        hidden: '{actions.view}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                style: {
                    paddingLeft: '15px'
                },
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Disable synchronization missing variables',
                    name: 'disableSynchronizationOfMissingVariables',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__disableSynchronizationOfMissingVariables}',
                        readOnly: '{actions.view}'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    fieldLabel: 'Enabled',
                    name: 'sharkEnabled',
                    bind: {
                        value: '{theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__shark__DOT__enabled}',
                        readOnly: '{actions.view}'
                    },
                    listeners: {
                        change: function (checkbox, newValue, oldValue) {
                            var formView = this.up('administration-content-setup-elements-workflow');
                            var vm = formView.getViewModel().getParent();
                            switch (newValue) {
                                case true:
                                    if (vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider') === "river" &&
                                        vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__river__DOT__enabled').toString() === "false"
                                    ) {
                                        formView.lookupReference('sharkDefault').setValue(true);
                                        formView.lookupReference('riverDefault').setValue(false);
                                        //vm.set('isSharkDefault', true);
                                        // vm.set('isRiverDefault', false);
                                        vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'shark');
                                    } else {

                                    }
                                    break;
                                case false:
                                    if (vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__river__DOT__enabled').toString() === "true") {
                                        formView.lookupReference('riverDefault').setValue(true);
                                        formView.lookupReference('sharkDefault').setValue(false);
                                        // vm.set('isSharkDefault', false);
                                        //vm.set('isRiverDefault', true);
                                        vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'river');
                                    }
                                    break;
                            }
                        }
                    }
                }]
            }, {
                columnWidth: 0.5,
                items: [{
                    xtype: 'checkbox',
                    style: {
                        paddingLeft: '15px'
                    },
                    fieldLabel: 'Default',
                    name: 'sharkDefault',
                    reference: 'sharkDefault',
                    bind: {
                        value: '{isSharkDefault}',
                        readOnly: '{actions.view}',
                        disabled: '{sharkDefaultFieldDisabled}'
                    },
                    listeners: {
                        change: function (checkbox, newValue, oldValue) {
                            var formView = this.up('administration-content-setup-elements-workflow');
                            var vm = formView.getViewModel().getParent();
                            switch (newValue) {
                                case true:
                                    // vm.set('isSharkDefault', true);
                                    // vm.set('isRiverDefault', false);
                                    vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'shark');
                                    formView.lookupReference('sharkDefault').setValue(true);
                                    formView.lookupReference('riverDefault').setValue(false);
                                    break;
                                case false:
                                    if (vm.get('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__river__DOT__enabled').toString() === "true") {
                                        // vm.set('isSharkDefault', false);
                                        // vm.set('isRiverDefault', true);
                                        vm.set('theSetup.org__DOT__cmdbuild__DOT__workflow__DOT__provider', 'river');
                                        formView.lookupReference('riverDefault').setValue(true);
                                        formView.lookupReference('sharkDefault').setValue(false);
                                    } else {
                                        // vm.set('isSharkDefault', true);
                                        // vm.set('isRiverDefault', false);

                                        // formView.lookupReference('sharkDefault').setValue(true);
                                        // formView.lookupReference('riverDefault').setValue(false);
                                    }
                                    break;
                            }
                        }
                    }
                }]
            }]
        }]
    }]
});