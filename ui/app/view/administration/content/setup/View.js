Ext.define('CMDBuildUI.view.administration.content.setup.View', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.setup.ViewController',
        'CMDBuildUI.view.administration.content.setup.ViewModel'
    ],
    alias: 'widget.administration-content-setup-view',
    controller: 'administration-content-setup-view',
    viewModel: {
        type: 'administration-content-setup-view'
    },

    
    cls: 'administration-mainview-tabpanel tab-hidden',
    ui: 'administration-tabandtools',
    layout: 'border',
    fieldDefaults:{
        labelAlign: 'top',
        width: '100%'
    },
    items: [{
        xtype: 'components-administration-toolbars-formtoolbar',
        region: 'north',
        borderBottom: 1,
        items: [{

            xtype: 'button',
            itemId: 'spacer',
            style: {
                "visibility": "hidden"
            }
        }, {
            xtype: 'tbfill'
        }, {
            xtype: 'tool',
            itemId: 'editAttributeBtn',
            iconCls: 'x-fa fa-pencil',
            tooltip: 'Edit card',
            callback: 'onEditSetupBtnClick',
            cls: 'administration-tool',
            bind: {
                hidden: '{isEditBtnHidden}'
            },
            autoEl: {
                'data-testid': 'administration-setup-view-editAttributeBtn'
            }
        }]
    }, {
        xtype: 'panel',
        region: 'center',
        scrollable: 'y',
        items: []
    }],

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        ui: 'footer',
        hidden: true,
        //margin: '5 5 5 5',
        bind: {
            hidden: '{actions.view}'
        },
        items: [{
            xtype: 'tbfill'
        }, {
            text: 'Save',
            ui: 'administration-action-small',
            listeners: {
                click: 'onSaveBtnClick'
            }
        }, {
            text: 'Cancel',
            ui: 'administration-secondary-action-small',
            listeners: {
                click: 'onCancelBtnClick'
            }
        }]
    }]
});