(function () {

    var elementId = 'CMDBuildAdministrationContentLookupTypesView';
    Ext.define('CMDBuildUI.view.administration.content.lookuptypes.View', {
        extend: 'Ext.container.Container',

        requires: [
            'CMDBuildUI.view.administration.content.lookuptypes.ViewController',
            'CMDBuildUI.view.administration.content.lookuptypes.ViewModel'
        ],

        alias: 'widget.administration-content-lookuptypes-view',
        controller: 'administration-content-lookuptypes-view',
        viewModel: {
            type: 'administration-content-lookuptypes-view'
        },
        title: 'Lookup',
        itemId: elementId,
        reference: elementId,
        id: elementId,
        config: {
            objectTypeName: null,
            allowFilter: true,
            showAddButton: true,
            action: CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.view,
            title: null
        },
        loadMask: true,
        defaults: {
            textAlign: 'left',
            scrollable: true
        },
        layout: 'border',
        style: 'background-color:#fff',
        items: [{
            xtype: 'administration-content-lookuptypes-topbar',
            region: 'north'
        }, {
            xtype: 'administration-content-lookuptypes-tabpanel',
            region: 'center'
        }],
        listeners: {
            afterlayout: function (panel) {
                Ext.GlobalEvents.fireEventArgs("showadministrationcontentmask", [false]);
            }
        }
    });
})();