Ext.define('CMDBuildUI.view.administration.content.lookuptypes.ViewModel', {
    imports:[
        'CMDBuildUI.util.Utilities'
    ],
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-lookuptypes-view',
    data: {
        activeTab: 0,
        objectTypeName: null,
        theObject: null,
        action: null,
        actions: {
            view: true,
            edit: false,
            add: false
        },
        disabledTabs: {
            list: false,
            values: true
        },
        allLookupTypeProxy: {
            type: 'memory'
        }
    },

    formulas: {
        tabManager: {
            bind: {
                action: '{action}',
                theLookupType: '{theLookupType}'
            },
            get: function (bind) {
                this.set('actions.view', bind.action === CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.view);
                this.set('actions.edit', bind.action === CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.edit);
                this.set('actions.add', bind.action === CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.create);

                if (bind.action !== CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.view ) {
                    this.set('allLookupTypeProxy', {
                        url: "/lookup_types",
                        type: 'baseproxy'
                    });
                } else {
                    this.set('allLookupTypeProxy', { type: 'memory' });
                }

                if (bind.theLookupType.getId() === 'new' && bind.action !== CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.create) {
                    this.set('action', CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.create);
                }

                if (bind.action === CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.create || bind.action === CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.view) {
                    this.set('disabledTabs', {
                        list: false,
                        values: true
                    });
                } else {
                    this.set('disabledTabs', {
                        list: false,
                        values: false
                    });
                }
            }
        },

        lookupValuesProxy: {
            bind: '{theLookupType.name}',
            get: function (objectTypeName) {
                if (objectTypeName && !this.get('theLookupType').phantom) {
                    return {
                        url: Ext.String.format("/lookup_types/{0}/values", CMDBuildUI.util.Utilities.stringToHex(objectTypeName)),
                        type: 'baseproxy'
                    };
                }
            }
        }
    }
});
