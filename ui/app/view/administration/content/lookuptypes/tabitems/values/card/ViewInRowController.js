Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewInRowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-lookuptypes-tabitems-values-card-viewinrow',

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            itemupdated: 'onLookupValueUpdated'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewInRow} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        this.linkLookupValue();
    },
    onLookupValueUpdated: function (v, record) {
        this.linkLookupValue();
    },
    onEditBtnClick: function () {
        var view = this.getView();
        var vm = this.getViewModel();
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        var objecttype = vm.get('theValue').get('_type'); // TODO: check

        container.add({
            xtype: 'administration-content-lookuptypes-tabitems-values-card-edit',
            viewModel: {
                data: {
                    lookupTypeName: objecttype,
                    valueId: vm.get('theValue').getId(),
                    values: view.up().getStore().getRange(),
                    title: objecttype + ' - ' + 'Values' + ' - ' + vm.get('theValue').get('name'),
                    grid: view.up()
                }
            }
        });
    },

    onDeleteBtnClick: function () {
        var vm = this.getViewModel();
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {

                    CMDBuildUI.util.Ajax.setActionId('delete-attribute-card');

                    var theObject = vm.getData().theAttribute;
                    theObject.erase({
                        // failure: function (record, operation) {
                        //     Ext.Msg.alert(
                        //         'Error', //TODO:translate
                        //         Ext.JSON.decode(operation.error.response.responseText, true).message //TODO:translate
                        //     );
                        // },
                        success: function (record, operation) {
                            Ext.ComponentQuery.query('administration-content-lookuptypes-tabitems-values-grid')[0].fireEventArgs('reload', [record, 'delete']);
                        }
                    });
                }
            },
            this
        );
    },

    /**
     * 
     */
    onOpenBtnClick: function () {
        var view = this.getView();
        var vm = this.getViewModel();
        var objecttype = vm.get('selected').get('objecttype');
        var container = Ext.getCmp(CMDBuildUI.view.administration.DetailsWindow.elementId) || Ext.create(CMDBuildUI.view.administration.DetailsWindow);
        container.removeAll();
        container.add({
            xtype: 'administration-content-lookuptypes-tabitems-values-card-view',
            viewModel: {
                data: {
                    lookupTypeName: objecttype,
                    valueId: vm.get('theValue').getId(),
                    values: view.up().getStore().getRange(),
                    title: objecttype + ' - ' + 'Values' + ' - ' + vm.get('theValue').get('name'),
                    grid: view.up()
                }
            }
        });
    },

    privates: {
        /**
         * Link the lookup value  
         */
        linkLookupValue: function () {
            var view = this.getView();
            var vm = this.getViewModel();
            var config = view.getInitialConfig();
            var record = config._rowContext.ownerGrid.selection || config._rowContext.record; // get atttribute record
            var lookupType = Ext.getCmp('CMDBuildAdministrationContentLookupTypesView').getViewModel().get('objectTypeName');

            if (record && record.getData()) {
                if (lookupType) {
                    record.getProxy().setUrl('/lookup_types/' + CMDBuildUI.util.Utilities.stringToHex(lookupType) + '/values/');
                    vm.set("theValue", record);
                }
            }
        }
    }
});