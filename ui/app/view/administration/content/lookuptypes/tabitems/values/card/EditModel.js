Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.EditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.view-administration-content-lookuptypes-tabitems-values-card-edit',

    data: {
        valueIconType: {
            isImageOrNone: false,
            isFontOrNone: false,
            isNone: false
        },
        isIconFileRequired: false
    },
    formulas: {

        iconTypeManager: {
            bind: {
                iconType: '{theValue.icon_type}',
                iconImage: '{theValue.icon_image}'
            },
            get: function (data) {
                this.set('valueIconType.isImageOrNone', data.iconType && (data.iconType === 'image' || data.iconType === 'none'));
                this.set('valueIconType.isFontOrNone', data.iconType && (data.iconType === 'font' || data.iconType === 'none'));
                this.set('valueIconType.isNone', data.iconType && data.iconType === 'none');
                this.set('isIconFileRequired', data.iconType && data.iconType === 'image' && !data.iconImage.length);

                switch (data.iconType) {
                    case "none":
                        Ext.ComponentQuery.query('#lookupValueIconFont')[0].allowBlank = true;
                        Ext.ComponentQuery.query('#lookupValueIconColor')[0].allowBlank = true;
                        Ext.ComponentQuery.query('#lookupValueImage')[0].allowBlank = true;
                        break;
                    case "image":
                        Ext.ComponentQuery.query('#lookupValueIconFont')[0].allowBlank = true;
                        Ext.ComponentQuery.query('#lookupValueIconColor')[0].allowBlank = true;
                        Ext.ComponentQuery.query('#lookupValueImage')[0].allowBlank = false;
                        break;
                    case "font":
                        Ext.ComponentQuery.query('#lookupValueIconFont')[0].allowBlank = false;
                        Ext.ComponentQuery.query('#lookupValueIconColor')[0].allowBlank = false;
                        Ext.ComponentQuery.query('#lookupValueImage')[0].allowBlank = true;
                        break;
                }
            }
        },

        parentLookupValuesProxy: {
            bind: '{theValue.parent_type}',
            get: function (parentType) {
                if (parentType) {
                    return {
                        url: Ext.String.format("/lookup_types/{0}/values", CMDBuildUI.util.Utilities.stringToHex(parentType)),
                        type: 'baseproxy'
                    };
                }
            }
        },

        panelTitle: {
            bind: '{theValue.description}',
            get: function (description) {
                if (description) {
                    var title = Ext.String.format(
                        '{0} - {1}',
                        this.get('lookupTypeName'),
                        this.getData().theValue.get('description')
                    );
                    this.getParent().set('title', title);
                }
            }
        }
    }

});