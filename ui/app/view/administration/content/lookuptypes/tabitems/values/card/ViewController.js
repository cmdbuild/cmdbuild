Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-lookuptypes-tabitems-values-card-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.View} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        this.vm = this.getViewModel();
        var lookupTypeName = this.vm.get('lookupTypeName');
        var valueId = this.vm.get('valueId');

        if (lookupTypeName && valueId) {
            this.vm.linkTo("theValue", {
                type: 'CMDBuildUI.model.lookups.Lookup',
                id: encodeURI(valueId)
            });
        }
    },

    onEditBtnClick: function () {
        var view = this.getView();
        var url = 'administration/classes/' + view.getObjectTypeName() + '/cards/' + view.getObjectId() + '/edit';
        this.redirectTo(url, true);
    },
    onDeleteBtnClick: function () {
        Ext.Msg.confirm(
            CMDBuildUI.locales.Locales.administration.common.messages.attention,
            CMDBuildUI.locales.Locales.administration.common.messages.areyousuredeleteitem,
            function (btnText) {
                if (btnText.toLowercase() === CMDBuildUI.locales.Locales.administration.common.actions.yes.toLowercase()) {
                    CMDBuildUI.util.Ajax.setActionId('delete-attribute-card');

                    vm.get("theObject").erase({
                        failure: function (record, operation) {
                            Ext.Msg.alert(
                                'Error', //TODO: translate
                                Ext.JSON.decode(operation.error.response.responseText, true).message //TODO:translate
                            );
                        },
                        success: function (record, operation) {
                            Ext.ComponentQuery.query('administration-content-classes-tabitems-attributes-grid')[0].fireEventArgs('reload', [record, 'delete']);
                        }
                    });
                }
            },
            this
        );
    },

    onOpenBtnClick: function () {
        var view = this.getView();
        var url = 'administration/classes/' + view.getObjectTypeName() + '/cards/' + view.getObjectId();
        this.redirectTo(url, true);
    }
});