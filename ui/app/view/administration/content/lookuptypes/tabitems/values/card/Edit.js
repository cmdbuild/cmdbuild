Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.administration-content-lookuptypes-tabitems-values-card-edit',

    requires: [
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.EditController',
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.EditModel',

        'CMDBuildUI.util.helper.FormHelper'
    ],
    controller: 'view-administration-content-lookuptypes-tabitems-values-card-edit',
    viewModel: {
        type: 'view-administration-content-lookuptypes-tabitems-values-card-edit'
    },
    bubbleEvents: [
        'itemupdated',
        'cancelupdating'
    ],
    modelValidation: true,
    config: {
        theValue: null
    },
    fieldDefaults: {
        labelAlign: 'top',
        msgTarget: 'under'
    },
    scrollable: true,
    ui: 'administration-formpagination',
    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Code', // TODO: translate
                name: 'code',
                bind: {
                    value: '{theValue.code}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Description',
                layout: 'column',

                items: [{
                    columnWidth: 0.8,
                    xtype: 'textfield',

                    name: 'description',
                    bind: {
                        value: '{theValue.description}'
                    }
                }, {
                    xtype: 'button',
                    columnWidth: 0.2,
                    reference: 'translateBtn',
                    iconCls: 'x-fa fa-flag fa-2x',
                    tooltip: 'Translate',
                    config: {
                        theValue: null
                    },
                    viewModel: {
                        data: {
                            theValue: null,
                            vmKey: 'theValue'
                        }
                    },
                    bind: {
                        theValue: '{theValue}'
                    },
                    listeners: {
                        click: 'onTranslateClick'
                    },
                    cls: 'administration-field-container-btn',
                    autoEl: {
                        'tag': 'div',
                        'data-testid': 'administration-lookupvalue-card-edit-translateBtn'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'combo',
                fieldLabel: 'Parent description', // TODO: translate
                name: 'parentDescription',
                allowBlank: true,
                displayField: 'description',
                valueField: '_id',
                bind: {
                    value: '{theValue.parent_id}',
                    disabled: '{!theValue.parent_type}',
                    store: {
                        model: "CMDBuildUI.model.lookups.LookupValue",
                        proxy: '{parentLookupValuesProxy}',

                        pageSize: 0, // disable pagination
                        fields: ['_id', 'description'],
                        autoLoad: true,
                        sorters: [
                            'description'
                        ]
                    }
                }
            }, {
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Text color',
                layout: 'column',

                items: [{
                    columnWidth: 0.8,
                    xtype: 'cmdbuild-colorpicker',
                    value: '#30373D',
                    bind: {
                        value: '{theValue.text_color}'
                    }
                }, {
                    xtype: 'image',
                    autoEl: 'div',
                    alt: 'Color preview', // TODO: translate
                    columnWidth: 0.2,
                    cls: 'fa-2x x-fa fa-square',
                    style: {
                        lineHeight: '32px'
                    },
                    bind: {
                        style: {
                            color: '{theValue.text_color}'
                        }
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 1,
                xtype: 'textarea',
                fieldLabel: 'Note', // TODO: translate
                name: 'note',
                bind: {
                    value: '{theValue.note}'
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                bind: {
                    value: '{theValue.active}'
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        title: "Icon", // TODO: translate
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'combo',
                fieldLabel: 'Type',
                reference: 'iconType',
                name: 'iconType',
                allowBlank: true,
                queryMode: 'local',
                displayField: 'label',
                valueField: 'value',
                bind: {
                    value: '{theValue.icon_type}',
                    store: {
                        model: 'CMDBuildUI.model.base.ComboItem',
                        fields: ['value', 'label'],
                        autoLoad: true,
                        proxy: {
                            type: 'memory'
                        },
                        data: [{
                            'value': 'none',
                            'label': 'None' // TODO: translate
                        }, {
                            'value': 'font',
                            'label': 'Font' // TODO: translate
                        }, {
                            'value': 'image',
                            'label': 'Image' // TODO: translate
                        }]
                    }
                }
            }]
        }, {
            layout: 'column',
            bind: {
                hidden: '{valueIconType.isFontOrNone}'
            },
            items: [{
                columnWidth: 0.5,
                xtype: 'filefield',
                itemId: 'lookupValueImage',
                ui: 'administration-action-small',
                name: 'iconImage',
                fieldLabel: 'Image',
                labelCls: Ext.baseCSSPrefix + 'form-item-label-default',
                reference: 'iconImage',
                allowBlank: false,
                buttonText: 'Select Image...',
                accept: 'image/png',
                validator: 'isInputFileRequired',
                listeners: {
                    change: 'onFileChange' // no scope given here    
                }
            }]
        }, {
            layout: 'column',
            liquidLayout: true,
            userCls: 'img-container',
            width: 32,
            height: 32,
            border: false,
            bind: {
                hidden: '{valueIconType.isFontOrNone}'
            },
            style: {
                borderWidth: 0
            },
            items: [{
                xtype: 'image',
                reference: 'lookupValueImage',
                alt: 'Icon image',
                src: null,
                bind: {
                    src: '{theValue.icon_image}'
                },
                style: {
                    marginTop: '5px',
                    borderWidth: 0
                },
                liquidLayout: true,
                border: false,
                width: 256,
                heigth: 256
            }]
        }, {
            layout: 'column',
            bind: {
                hidden: '{valueIconType.isImageOrNone}'
            },
            items: [{
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Icon',
                layout: 'column',

                items: [{
                    columnWidth: 0.8,
                    xtype: 'cmdbuild-fapicker',
                    itemId: 'lookupValueIconFont',
                    allowBlank: false,
                    bind: {
                        value: '{theValue.icon_font}'
                    }
                }, {
                    xtype: 'image',
                    autoEl: 'div',
                    alt: 'Icon preview',
                    columnWidth: 0.2,
                    cls: 'fa-2x',
                    style: {
                        lineHeight: '32px'
                    },
                    bind: {
                        userCls: '{theValue.icon_font}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Icon color',
                layout: 'column',
                items: [{
                    columnWidth: 0.8,
                    xtype: 'cmdbuild-colorpicker',
                    itemId: 'lookupValueIconColor',
                    value: '#30373D',
                    bind: {
                        value: '{theValue.icon_color}'
                    }
                }, {
                    xtype: 'image',
                    autoEl: 'div',
                    alt: 'Color preview',
                    columnWidth: 0.2,
                    cls: 'fa-2x x-fa fa-square',
                    style: {
                        lineHeight: '32px'
                    },
                    bind: {
                        style: {
                            color: '{theValue.icon_color}'
                        }
                    }
                }]
            }]
        }]
    }],

    buttons: [{
        text: 'Save',
        formBind: true,
        disabled: true,
        ui: 'administration-action-small',
        listeners: {
            click: 'onSaveBtnClick'
        }
    }, {
        text: 'Cancel',
        ui: 'administration-secondary-action-small',
        listeners: {
            click: 'onCancelBtnClick'
        }
    }]
});