Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.CreateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-lookuptypes-tabitems-values-card-create',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },
    /**
     * @param {CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        // set vm varibles
        var vm = this.getViewModel();
        vm.linkTo("theValue", {
            type: 'CMDBuildUI.model.lookups.Lookup',
            create: true
        });
    },
    /**
     * Save function
     */
    onSaveBtnClick: function () {
        var me = this;
        var vm = this.getViewModel();
        var form = this.getView();

        if (form.isValid()) {
            var theValue = vm.get('theValue');
            theValue.getProxy().setUrl(Ext.String.format('/lookup_types/{0}/values', vm.get('lookupTypeName')));
            delete theValue.data.inherited;
            delete theValue.data.writable;
            delete theValue.data.hidden;

            theValue.save({
                success: function (record, operation) {                    
                    var w = Ext.create('Ext.window.Toast', {
                        ui:'administration',
                        title: 'Success!',
                        html: 'Attribute created saved correctly.', // TODO: translate
                        iconCls: 'x-fa fa-check-circle',
                        align: 'br'
                    });
                    w.show();
                    Ext.GlobalEvents.fireEventArgs("lookupvalueupdated", [record]);
                    me.getView().up().fireEvent("closed");
                }
            });
        } else {
            var w = Ext.create('Ext.window.Toast', {
                ui:'administration',
                html: 'Please correct indicted errors', // TODO: translate
                title: 'Error!',
                iconCls: 'x-fa fa-check-circle',
                align: 'br'
            });
            w.show();
        }
    },
    /**
     * Reset form function
     */
    onCancelBtnClick: function () {
        this.getViewModel().get("theValue").reject();
        this.getView().fireEventArgs("cancelcreation");
    }
});
