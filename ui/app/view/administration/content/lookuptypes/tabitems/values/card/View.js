Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.View', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewController',
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewModel',
        'Ext.layout.*'
    ],
    alias: 'widget.administration-content-lookuptypes-tabitems-values-card-view',
    controller: 'administration-content-lookuptypes-tabitems-values-card-view',
    viewModel: {
        type: 'administration-content-lookuptypes-tabitems-values-card-view'
    },

    config: {
        objectTypeName: null,
        objectId: null,
        shownInPopup: false
    },

    scrollable: true,
    ui: 'administration-formpagination',
    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Code', // TODO: translate
                name: 'code',
                bind: {
                    value: '{theValue.code}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Description', // TODO: translate
                layout: 'column',

                items: [{
                    columnWidth: 0.9,
                    xtype: 'displayfield',

                    name: 'description',
                    bind: {
                        value: '{theValue.description}'
                    }
                }, {
                    xtype: 'button',
                    columnWidth: 0.1,
                    reference: 'translateBtn',
                    iconCls: 'x-fa fa-flag fa-2x',
                    tooltip: 'Translate',
                    handler: function (view, event) {
                        var content = {
                            xtype: 'administration-localization-localizecontent',
                            scrollable: 'y',
                            viewModel: {
                                data: {
                                    action: 'edit'
                                }
                            }
                        };

                        // custom panel listeners
                        var listeners = {
                            /**
                             * 
                             */
                            onBeforeRender: function () {

                            },
                            /**
                             * @param {Ext.panel.Panel} panel
                             * @param {Object} eOpts
                             */
                            close: function (panel, eOpts) {
                                CMDBuildUI.util.Utilities.closePopup('popup-edit-attribute-localization');
                            }
                        };
                        // create panel
                        var popUp = CMDBuildUI.util.Utilities.openPopup(
                            'popup-edit-attribute-localization',
                            'Localization text',
                            content,
                            listeners, {
                                ui: 'administration-actionpanel', // TODO: administratiion 
                                width: '450px',
                                height: '450px'
                            }
                        );

                        popUp.setPagePosition(event.getX() - 450, event.getY() + 20);
                    },
                    cls: 'administration-field-container-btn',
                    autoEl: {
                        'data-testid': 'administration-lookuptype-card-view-translateBtn'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Parent description', // TODO: translate
                name: 'parentDescription',
                bind: {
                    value: '{theValue.parent_id}'
                },
                renderer: function (value) {
                    if (value === 0) {
                        return '';
                    }
                    return value;
                }
            }, {
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Text color', // TODO: translate
                layout: 'column',

                items: [{
                    xtype: 'image',
                    autoEl: 'div',
                    alt: 'Color preview', // TODO: translate
                    columnWidth: 0.1,
                    cls: 'fa-2x x-fa fa-square',
                    style: {
                        lineHeight: '32px'
                    },
                    bind: {
                        style: {
                            color: '{theValue.text_color}'
                        }
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 1,
                xtype: 'textarea',
                fieldLabel: 'Note', // TODO: translate
                readOnly: true,
                name: 'note',
                bind: {
                    value: '{theValue.note}'
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                readOnly: true,
                bind: {
                    value: '{theValue.active}'
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        title: "Icon", // TODO: translate
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Icon type', // TODO: translate
                name: 'iconType',
                bind: {
                    value: '{theValue.icon_type}'
                },
                renderer: function (value) {
                    if (value) {
                        switch (value) {
                            case 'none':
                                return 'None';
                            case 'font':
                                return 'Font';
                            case 'image':
                                return 'Image';
                        }
                    }
                }
            }]
        }, {
            layout: 'column',
            liquidLayout: true,
            userCls: 'img-container',
            width: 36,
            height: 36,
            border: false,
            bind: {
                hidden: '{!iconTypeIsImage}'
            },
            style: {
                borderWidth: 0
            },
            items: [{
                xtype: 'image',
                reference: 'lookupValueImage',
                alt: 'Icon image', // TODO: translate
                src: null,
                bind: {
                    src: '{theValue.icon_image}'
                },
                style: {
                    marginTop: '5px',
                    borderWidth: 0
                },
                liquidLayout: true,
                border: false,
                width: 32,
                heigth: 32
            }]
        }, {
            layout: 'column',
            bind: {
                hidden: '{iconTypeIsImage}'
            },
            items: [{
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Icon', // TODO: translate
                layout: 'column',

                items: [{
                    xtype: 'image',
                    autoEl: 'div',
                    alt: 'Icon preview', // TODO: translate
                    columnWidth: 0.1,
                    cls: 'fa-2x',
                    style: {
                        lineHeight: '32px'
                    },
                    bind: {
                        userCls: '{theValue.icon_font}'
                    }
                }]
            }, {
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: 'Icon color', // TODO: translate
                layout: 'column',

                items: [{
                    xtype: 'image',
                    autoEl: 'div',
                    alt: 'Color preview', // TODO: translate
                    columnWidth: 0.1,
                    cls: 'fa-2x x-fa fa-square',
                    style: {
                        lineHeight: '32px'
                    },
                    bind: {
                        style: {
                            color: '{theValue.icon_color}'
                        }
                    }
                }]
            }]
        }]
    }],

    fieldDefaults: {
        labelAlign: 'top'
    }
});