Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewInRow', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewInRowController',
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.ViewInRowModel',
        'Ext.layout.*'
    ],
    autoDestroy: true,
    alias: 'widget.administration-content-lookuptypes-tabitems-values-card-viewinrow',
    controller: 'administration-content-lookuptypes-tabitems-values-card-viewinrow',
    viewModel: {
        type: 'administration-content-lookuptypes-tabitems-values-card-viewinrow'
    },
    cls: 'administration',
    config: {
        objectTypeName: null,
        objectId: null,
        shownInPopup: false
    },

    items: [{
        ui: 'administration-formpagination',
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Code',
                name: 'code',
                bind: {
                    value: '{theValue.code}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Description',
                name: 'description',
                bind: {
                    value: '{theValue.description}'
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Text color',
                name: 'textColor',
                bind: {
                    value: '<i class="fa fa-square" style="color:{theValue.text_color}"></i>'
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: 'Parent description',
                name: 'vumericValue',
                bind: {
                    value: '{theValue.parent_description}'
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 1,
                xtype: 'displayfield',
                fieldLabel: 'Note',
                name: 'note',
                bind: {
                    value: '{theValue.note}'
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: 'Active',
                name: 'active',
                disabled: true,
                bind: {
                    value: '{theValue.active}'
                }
            }]
        }]
    }],

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        ui: 'administration-formtoolbar',
        items: [{
            xtype: 'tbfill'
        }, {
            xtype: 'tool',
            itemId: 'editBtn',
            glyph: 'f040@FontAwesome', // Pencil icon
            tooltip: 'Edit lookup value', // TODO: translate
            callback: 'onEditBtnClick',
            cls: 'administration-tool',
            autoEl: {
                'data-testid': 'administration-attributes-card-view-editBtn'
            },
            bind: {
                hidden: '{!actions.view}'
            }
        }, {
            xtype: 'tool',
            itemId: 'openBtn',
            glyph: 'f08e@FontAwesome', // Open icon
            tooltip: 'Open lookup value', // TODO: translate
            callback: 'onOpenBtnClick',
            // disabled: true,
            cls: 'administration-tool',
            autoEl: {
                'data-testid': 'administration-attributes-card-view-openBtn'
            },
            bind: {
                hidden: '{hideOpenBtn}'
            }
        }, {
            xtype: 'tool',
            itemId: 'deleteBtn',
            glyph: 'f014@FontAwesome',
            tooltip: 'Delete lookup value', // TODO: translate
            callback: 'onDeleteBtnClick',
            // disabled: true,
            cls: 'administration-tool',
            autoEl: {
                'data-testid': 'administration-attributes-card-view-deleteBtn'
            },
            bind: {
                hidden: '{!canDelete}'
            }
        }]
    }],

    fieldDefaults: {
        labelAlign: 'top'
    }
});