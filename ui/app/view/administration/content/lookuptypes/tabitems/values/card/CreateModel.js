Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.CreateModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.view-administration-content-lookuptypes-tabitems-values-card-create',

    bind:{
        theAttribute: null
    },
    formulas: {
        title: function(get) {
            return this.getView().getObjectTypeName();
        }
    }
});
