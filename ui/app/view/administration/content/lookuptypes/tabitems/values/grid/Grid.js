Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.grid.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.grid.GridController',
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.grid.GridModel',

        // plugins
        'Ext.grid.filters.Filters',
        'CMDBuildUI.components.grid.plugin.FormInRowWidget'
    ],

    alias: 'widget.administration-content-lookuptypes-tabitems-values-grid-grid',
    controller: 'administration-content-lookuptypes-tabitems-values-grid-grid',
    viewModel: {
        type: 'administration-content-lookuptypes-tabitems-values-grid-grid'
    },
    bind: {
        store: '{allValues}',
        selection: '{selected}'
    },
    reference: 'lookupValuesGrid',

    columns: [{
        text: 'Code', // TODO: translate
        dataIndex: 'code',
        align: 'left'
    }, {
        text: 'Description', // TODO: translate
        dataIndex: 'description',
        align: 'left'
    }, {
        text: 'Text color', // TODO: translate
        dataIndex: 'text_color',
        align: 'left',
        renderer: function (value) {
            if (!value.length) {
                value = '#30373D'; // TODO: set global var??
            } else {
                if (!value.startsWith('#')) {
                    value = '#' + value;
                }
            }
            return '<i class="fa fa-square" style="color:' + value + '"></i>';
        }
    }, {
        text: 'Active', // TODO: translate
        dataIndex: 'active',
        align: 'center',
        xtype: 'checkcolumn',
        disabled: true,
        disabledCls: '' // or don't add this config if you want the field to look disabled
    }, {
        text: 'Parent description', // TODO: translate
        dataIndex: 'parent',
        align: 'left'
    }],


    viewConfig: {
        plugins: [{
            ptype: 'gridviewdragdrop',
            dragText: 'Drag and drop to reorganize',
            containerScroll: true,
            pluginId: 'gridviewdragdrop'
        }]
    },

    plugins: [{
        ptype: 'forminrowwidget',
        pluginId: 'forminrowwidget',
        expandOnDblClick: true,
        // TODO: need fix for reload the correct data
        widget: {
            xtype: 'administration-content-lookuptypes-tabitems-values-card-viewinrow',
            bind: {
                data: '{lookupValuesGrid.selection}'
            },
            viewModel: {
                data: {
                    theValue: null
                }
            }
        }
    }],

    autoEl: {
        'data-testid': 'administration-content-lookuptypes-tabitems-values-grid'
    },

    config: {
        objectTypeName: null,
        allowFilter: true,
        showAddButton: true,
        selected: null
    },

    forceFit: true,
    loadMask: true,

    selModel: {
        pruneRemoved: false // See https://docs.sencha.com/extjs/6.2.0/classic/Ext.selection.Model.html#cfg-pruneRemoved
    },
    labelWidth: "auto",
    tbar: [{
        xtype: 'button',
        text: 'Add value', // TODO: translate
        reference: 'addlookupvalue',
        itemId: 'addlookupvalue',
        iconCls: 'x-fa fa-plus',
        ui: 'administration-action',

        bind: {
            disabled: '{!canAdd}',
            hidden: '{newButtonHidden}'
        }
    }, {
        xtype: 'textfield',
        name: 'search',
        width: 250,
        emptyText: 'Search...', // TODO: translate
        reference: 'searchtext',
        itemId: 'searchtext',
        cls: 'administration-input',
        bind: {
            value: '{search.value}',
            hidden: '{!canFilter}'
        },
        listeners: {
            specialkey: 'onSearchSpecialKey'
        },
        triggers: {
            search: {
                cls: Ext.baseCSSPrefix + 'form-search-trigger',
                handler: 'onSearchSubmit'
            },
            clear: {
                cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                handler: 'onSearchClear'
            }
        }
    }, {
        xtype: 'tbfill'
    }]
});