Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.EditController', {
    imports: [
        'CMDBuildUI.util.Utilities'
    ],

    extend: 'Ext.app.ViewController',
    alias: 'controller.view-administration-content-lookuptypes-tabitems-values-card-edit',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.card.EditController} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();
        var lookupTypeName = vm.get('lookupTypeName');
        var valueId = vm.get('valueId');
        if (lookupTypeName && valueId) {
            vm.linkTo("theValue", {
                type: 'CMDBuildUI.model.lookups.Lookup',
                id: valueId
            });
        }
    },

    /**
     * @param {Ext.form.field.File} input
     * @param {Object} value
     * @param {Object} eOpts
     */
    onFileChange: function (input, value, eOpt) {
        var vm = this.getViewModel();
        var file = input.fileInputEl.dom.files[0];
        var reader = new FileReader();

        reader.addEventListener("load", function () {
            vm.getData().theValue.set('icon_image', reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    },

    /**
    * On cancel button click
    * @param {Ext.button.Button} button
    * @param {Event} e
    * @param {Object} eOpts
    */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var theValue = vm.get('theValue');

        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('lookup.{0}.{1}.description', theValue.get('_type'), theValue.get('code'))
                }
            }
        };

        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-lookupvalue-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-lookupvalue-localization',
            'Localization text',
            content,
            listeners,
            {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px'
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() + 20);
    },

    /**
    * On save button click
    * @param {Ext.button.Button} button
    * @param {Event} e
    * @param {Object} eOpts
    */
    onSaveBtnClick: function (button, event, eOpts) {
        var me = this;
        var vm = me.getViewModel();

        vm.get('theValue').save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    ui: 'administration',
                    html: 'Lookup value saved correctly.',
                    title: 'Success!',
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
                Ext.GlobalEvents.fireEventArgs("lookupvalueupdated", [record]);
               
                me.getView().up().fireEvent("closed");
            }
        });
    },
    /**
    * On cancel button click
    * @param {Ext.button.Button} button
    * @param {Event} e
    * @param {Object} eOpts
    */
    onCancelBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel();
        vm.get("theValue").reject(); // discard changes
        this.getView().up().fireEvent("closed");
    }

});
