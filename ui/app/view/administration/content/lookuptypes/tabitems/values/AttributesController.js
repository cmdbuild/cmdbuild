Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.AttributesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-lookuptypes-tabitems-values-attributes',
    control: {
        "#": {
            beforerender: "onBeforeRender",
            itemcreated: "onItemCreated"
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.content.lookuptypes.tabitems.values.Attributes} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        //view.up('administration-content').getViewModel().set('title', 'Lookup Types');
    },

    onItemCreated: function (record, eOpts) {
       // TODO: reload menu tree store
    }
});
