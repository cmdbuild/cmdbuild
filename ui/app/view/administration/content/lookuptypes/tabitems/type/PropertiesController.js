Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.type.PropertiesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-lookuptypes-tabitems-type-properties',
    require: [
        'CMDBuildUI.util.administration.helper.FormHelper'
    ],

    onEditBtnClick: function (button) {

        this.getViewModel().set('action', CMDBuildUI.view.administration.content.lookuptypes.TabPanel.formmodes.edit);
    },

    onDeleteBtnClick: function (button) {
        var vm = this.getViewModel();

        Ext.Msg.confirm(
            "Delete lookup", // TODO: translate
            "Are you sure you want to delete this lookup?", // TODO: translate
            function (action) {
                if (action === CMDBuildUI.locales.Locales.administration.common.yes) {
                    var theObject = vm.get('theLookupType');
                    CMDBuildUI.util.Ajax.setActionId('delete-lookuptype');

                    theObject.erase({
                        failure: function (record, operation) {

                        },
                        success: function (record, operation) {

                            var response = operation.getResponse();
                            var w = Ext.create('Ext.window.Toast', {
                                // Ex: "INFO: drop cascades to table "AssetTmpl_history""
                                html: Ext.JSON.decode(response.responseText).message || 'Lookup type successfully deleted.', // TODO: translate
                                title: 'Success',// TODO: translate
                                iconCls: 'x-fa fa-check-circle"',
                                align: 'br',
                                autoClose: true,
                                maxWidth: 300,
                                monitorResize: true,
                                closable: true
                            });
                            w.show();
                            theObject.commit();
                        }
                    });
                }
            }
        );
    },



    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var me = this;
        button.setDisabled(true);
        var vm = this.getViewModel();
        if (!vm.get('theLookupType').isValid()) {
            var validatorResult = vm.get('theLookupType').validate();
            var errors = validatorResult.items;
            for (var i = 0; i < errors.length; i++) {
                console.log('Key :' + errors[i].field + ' , Message :' + errors[i].msg);
            }
        } else {
            var theObj = vm.get('theLookupType');

            theObj.save({
                failure: function (record, operation) {

                    button.setDisabled(false);
                },
                success: function (record, operation) {
                    var w = Ext.create('Ext.window.Toast', {
                        title: 'Success!',
                        html: 'Lookup type saved correctly.',
                        iconCls: 'x-fa fa-check-circle',
                        align: 'br'
                    });
                    w.show();
                    theObj.commit();
                    CMDBuildUI.util.administration.MenuStoreBuilder.initialize(
                        function () {
                            var treestore = Ext.getCmp('administrationNavigationTree');
                            var oldselected = treestore.getStore().findNode("href", Ext.History.getToken());

                            treestore.setSelection(oldselected);
                        });
                    Ext.GlobalEvents.fireEventArgs("reloadadministrationmenu", [record]);
                    me.redirectTo('administration/lookup_types/' + record.getId(), true);
                },
                callback: function (record, operation, success) {
                }
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        var store = Ext.getStore('administration.MenuAdministration');
        var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
        var currentNode = store.findNode("objecttype", CMDBuildUI.model.administration.MenuItem.types.lookuptype);
        vm.set('selected', currentNode);
        this.redirectTo('administration/lookup_types_empty', true);
    }
});
