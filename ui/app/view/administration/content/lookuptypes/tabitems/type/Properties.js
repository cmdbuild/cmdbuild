Ext.define('CMDBuildUI.view.administration.content.lookuptypes.tabitems.type.Properties', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.lookuptypes.tabitems.type.PropertiesController'
    ],

    alias: 'widget.administration-content-lookuptypes-tabitems-type-properties',
    controller: 'administration-content-lookuptypes-tabitems-type-properties',
    autoScroll: false,
    modelValidation: true,
    fieldDefaults: {
        labelAlign: 'top',
        width: '100%'
    },
    layout: 'border',
    items: [{
        xtype: 'components-administration-toolbars-formtoolbar',
        region: 'north',
        //cls: 'with-margin',
        items: [{

            xtype: 'button',

            itemId: 'spacer',
            style: {
                "visibility": "hidden"
            }
        }, {
            // move all buttons on right side
            xtype: 'tbfill'
        }, {
            // edit button
            xtype: 'tool',
            align: 'right',
            itemId: 'editBtn',
            cls: 'administration-tool',
            iconCls: 'x-fa fa-pencil',
            tooltip: CMDBuildUI.locales.Locales.administration.lookuptypes.type.toolbar.editBtn.tooltip,
            callback: 'onEditBtnClick',
            hidden: true,
            autoEl: {
                'data-testid': 'administration-lookuptypes-type-tool-editbtn'
            },
            bind: {
                hidden: '{!actions.view}'
            }
        }, {
            // delete button
            xtype: 'tool',
            align: 'right',
            itemId: 'deleteBtn',
            cls: 'administration-tool',
            iconCls: 'x-fa fa-trash',
            tooltip: CMDBuildUI.locales.Locales.administration.lookuptypes.type.toolbar.deleteBtn.tooltip,
            callback: 'onDeleteBtnClick',
            hidden: true,
            autoEl: {
                'data-testid': 'administration-lookuptypes-type-tool-deletebtn'
            },
            bind: {
                hidden: '{!actions.view}'
            }
        }, {
            // prevent toolbar height change on edit/add mode
            xtype: 'tool',
            align: 'right',
            itemId: 'placeholder',
            cls: 'administration-tool',
            iconCls: '',
            hidden: true,
            autoEl: {
                'data-testid': 'administration-lookuptypes-type-tool-placeholder'
            },
            bind: {
                hidden: '{actions.view}'
            }
        }]
    }, {
        xtype: 'panel',
        region: 'center',
        scrollable: 'y',
        items: [{
            xtype: 'administration-content-lookuptypes-tabitems-type-fieldsets-generaldatafieldset'
        }]
    }],
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        ui: 'footer',
        hidden: true,
        bind: {
            hidden: '{actions.view}'
        },
        items: [{
            xtype: 'component',
            flex: 1
        }, {
            text: CMDBuildUI.locales.Locales.administration.classes.properties.toolbar.saveBtn,
            formBind: true, //only enabled once the form is valid
            disabled: true,
            ui: 'administration-action-small',
            listeners: {
                click: 'onSaveBtnClick'
            }
        }, {
            text: CMDBuildUI.locales.Locales.administration.classes.properties.toolbar.cancelBtn,
            ui: 'administration-secondary-action-small',
            listeners: {
                click: 'onCancelBtnClick'
            }
        }]
    }]
});