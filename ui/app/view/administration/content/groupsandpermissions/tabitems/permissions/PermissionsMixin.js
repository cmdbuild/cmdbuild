Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.PermissionsMixin', {
    mixinId: 'administration-permissions-tab-mixin',

    /**
     * Filter grid items.
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchChange: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // get value
        var searchTerm = field.value;
        // debugger;
        var filterCollection = this.getView().down('grid').getStore().getFilters();
        if (searchTerm) {
            filterCollection.add([{
                id: 'objectTypeNameFilter',
                property: '_object_description',
                operator: 'like',
                value: searchTerm
            }]);
        } else {
            this.onSearchClear(field);
        }
    },

    /**
     * Reset search field
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchClear: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // clear store filter
        var filterCollection = this.getView().down('grid').getStore().getFilters();
        filterCollection.removeByKey('objectTypeNameFilter');

        // reset input
        field.reset();
    },

    /**
     * Function for enable/disable sub tabs of "Permission" permission tab panel
     * @param {Number} index 
     */
    toggleEnablePermissionsTabs: function (index) {
        var vm = Ext.getCmp('CMDBuildAdministrationPermissions').getViewModel();
        vm.toggleEnableTabs(index);
        console.log('toggleEnablePermissionsTabs', index);
    },

    /**
     * Function for enable/disable tabs of 
     * "CMDBuildUI.view.administration.content.groupsandpermissions.View" tab 
     * panel
     * @param {Number} index 
     */
    toggleEnableTabs: function (index) {
        var vm = Ext.getCmp('CMDBuildAdministrationContentGroupView').getViewModel();
        vm.toggleEnableTabs(index);
    },

    /**
     * 
     * @param {Ext.data.Store} store 
     * @param {Ext.data.Model[]} items 
     * @param {Boolean} successful 
     * @param {Ext.data.operation.Read} operation 
     * @param {Object} eOpts 
     */
    onGrantStoreLoad: function (store, items, successful, operation, eOpts) {

        // if (store.config && store.config.roleId && store.config.relatedStore && store.config.objectType) {
        //     var roleId = store.config.roleId;
        //     var relatedStore = Ext.getStore(store.config.relatedStore);

        //     if (!relatedStore) {
        //         return;
        //     }
        //     if (relatedStore && !relatedStore.isLoaded()) {

        //         relatedStore.on('load', function (store, items) {

        //             var relatedItems = relatedStore.getRange();
        //             Ext.Array.forEach(relatedItems, function (relatedItem) {

        //                 var found = Ext.Array.findBy(items, function (element, index) {

        //                     return element.get('objectType') === store.config.objectType && element.get('objectTypeName') === relatedItem.get('name');
        //                 }, this);

        //                 if (!found) {

        //                     var grant = CMDBuildUI.model.users.Grant.create({
        //                         objectType: store.config.objectType,
        //                         objectTypeName: relatedItem.get('name'),
        //                         objectTypeDescription: relatedItem.get('description'),
        //                         role: roleId
        //                     });
        //                     grant.phantom = false;
        //                     grant.crudState = 'R';

        //                     store.add(grant);
        //                 }

        //             });

        //             store.sort('objectTypeName', 'ASC');

        //         });
        //     } else {
        //         var relatedItems = relatedStore.getRange();
        //         Ext.Array.forEach(relatedItems, function (relatedItem) {

        //             var found = Ext.Array.findBy(items, function (element, index) {
        //                 return element.get('objectType') === store.config.objectType && (element.get('objectTypeName') === relatedItem.get('name') || element.get('objectTypeName') === relatedItem.get('_id'));
        //             }, this);
        //             var grant;
        //             if (!found) {

        //                 grant = CMDBuildUI.model.users.Grant.create({
        //                     objectType: store.config.objectType,
        //                     objectTypeName: relatedItem.get('_id'),
        //                     objectTypeDescription: relatedItem.get('description'),
        //                     role: roleId
        //                 });
        //                 grant.phantom = false;
        //                 grant.crudState = 'R';

        //                 store.add(grant);
        //             } else {
        //                 // add 
        //                 var objectTypeDescription = (relatedItem.get('description') && relatedItem.get('description').length) ? relatedItem.get('description') : relatedItem.get('_id');
        //                 found.set('objectTypeDescription', objectTypeDescription);
        //                 found.phantom = false;
        //                 found.crudState = 'R';
        //                 store.add(found);
        //             }

        //         });

        //         store.sort('objectTypeName', 'ASC');
        //     }


        // }
    }
});