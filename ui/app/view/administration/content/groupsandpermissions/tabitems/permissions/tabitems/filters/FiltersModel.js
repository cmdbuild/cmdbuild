Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.tabitems.filters.FiltersModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-groupsandpermissions-tabitems-permissions-tabitems-filters-filters',
    
    stores:{
        objectTypeGrantsStore: {
            source: '{grantsStore}',
            filters: [{
                filterFn: function(rec){
                    if(rec.get('objectType') === 'filter'){
                        return true;
                    }
                    return false;
                }
            }],
            autoLoad: true,
            autoDestroy: true
        }
    }
});
