Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.tabitems.dashboards.DashboardsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-groupsandpermissions-tabitems-permissions-tabitems-dashboards-dashboards',
   
    stores:{
        objectTypeGrantsStore: {
            source: '{grantsStore}',
            filters: [{
                filterFn: function(rec){
                    if(rec.get('objectType') === 'dashboard'){
                        return true;
                    }
                    return false;
                }
            }],
            autoLoad: true,
            autoDestroy: true
        }
    }
});
