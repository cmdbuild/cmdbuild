Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.PermissionsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-groupsandpermissions-tabitems-permissions-permissions',

    data: {
        activeTab: 0,
        currentObjectType: null,

        disabledTabs: {
            classes_tab: false,
            processes_tab: true,
            views_tab: false,
            filters_tab: false,
            dashboards_tab: false,
            reports_tab: false,
            custompages_tab: false
        }
    },

    formulas: {
        hiddenColumns: {
            bind: '{objectType}',
            get: function (objectType) {
                switch (objectType) {
                    case 'classes':
                        return {
                            modeTypeNone: false,
                            modeTypeRead: false,
                            modeTypeDefault: true,
                            modeTypeDefaultRead: true,
                            modeTypeWrite: false,
                            actionFilter: false,
                            actionResetFilter: false,
                            actionActionDisabled: false
                        };
                    case 'processes':
                        return {
                            modeTypeNone: false,
                            modeTypeRead: true,
                            modeTypeDefault: false,
                            modeTypeDefaultRead: false,
                            modeTypeWrite: true,
                            actionFilter: false,
                            actionResetFilter: false,
                            actionActionDisabled: true
                        };
                    case 'views':
                    case 'searchFilters':
                    case 'dashboards':
                    case 'reports':
                    case 'custompages':
                        return {
                            modeTypeNone: false,
                            modeTypeRead: false,
                            modeTypeDefault: true,
                            modeTypeDefaultRead: true,
                            modeTypeWrite: true,
                            actionFilter: true,
                            actionResetFilter: true,
                            actionActionDisabled: true
                        };
                    default:
                        break;
                }
            }
        }
    },

    stores: {
        grantsChainedStore: {
            type: 'chained',
            source: 'groups.Grants',
            autoLoad: true,
            autoDestroy: true,
            pageSize: 0, // disable pagination
            config: {
                // relatedStore: newtab.config.relatedStore,
                // objectType: newtab.config.objectType,
                // roleId:'{theGroup._id}'
            },
            // filters: [{
            //     filterFn: function (rec) {
            //         if (rec.get('objectType') === newtab.config.objectType) {
            //             return true;
            //         }
            //         return false;
            //     }
            // }],
            // listeners: {
            //     load: me.onGrantStoreLoad
            // }
        }
    },

    /**
     * Enable/disable one or multiple tabs of 
     * "CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.Permissions" tab panel
     * @param {Number} currrentTabIndex 
     */
    toggleEnableTabs: function (currrentTabIndex) {
        var me = this;
        var view = me.getView();
        var tabs = view.items.items;
        var mainTabs = view.up('administration-content-groupsandpermissions-tabpanel').items.items;

        if (typeof currrentTabIndex == 'undefined') {
            tabs.forEach(function (tab) {
                tab.enable();
            });
            mainTabs.forEach(function (tab) {
                tab.enable();
            });
        } else {
            tabs.forEach(function (tab) {
                if (tab.tabConfig.tabIndex !== currrentTabIndex) {
                    tab.disable();
                }else{
                    tab.enable();
                }
            });
            mainTabs.forEach(function (tab) {
                if(tab.tabConfig.tabIndex !== 1){
                    tab.disable(); 
                }else{
                    tab.enable();
                }
            });
        }
    },

    /**
     * Change form mode
     * 
     * @param {String} mode
     */
    setFormMode: function (mode) {
        var me = this;
        switch (mode) {
            case 'view':
                me.set('actions.view', true);
                me.set('actions.edit', false);
                break;
            case 'edit':
                me.set('actions.view', false);
                me.set('actions.edit', true);
                break;
        }
    }
});