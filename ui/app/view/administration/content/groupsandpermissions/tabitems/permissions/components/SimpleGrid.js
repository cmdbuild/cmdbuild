Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.components.SimpleGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.administration-content-groupsandpermissions-tabitems-permissions-components-simplegrid',
    viewModel: {},

    width: '100%',
    layout: 'fit',

    viewConfig: {
        markDirty: false
    },

    sealedColumns: false,
    sortableColumns: false,
    enableColumnHide: false,
    enableColumnMove: false,
    enableColumnResize: false,
    menuDisabled: true,

    columns: [{
        flex: 10,
        text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.description,
        localize: {
            text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.description
        },
        dataIndex: '_object_description',
        align: 'left',
        renderer: function(value, metaData,record, rowIndex, colIndex, store){
            if(!value.length){
                return record.get('objectTypeName');
            }
            return value;
        }
    }, {
        flex: 1,
        text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.none,
        localize: {
            text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.none
        },
        dataIndex: 'modeTypeNone',
        align: 'center',
        xtype: 'checkcolumn',
        headerCheckbox: true,
        disabled: true,
        hideable: false,
        hidden: true,
        disabledCls: '', // or don't add this config if you want the field to look disabled,
        bind: {
            hidden: '{hiddenColumns.modeTypeNone}',
            disabled: '{!actions.edit}'
        },
        listeners: {
            headercheckchange: function (columnHeader, checked, e, eOpts) {
                this.getView().getStore().each(function (record) {
                    record.changeMode('-');
                });
            },
            checkchange: function (check, rowIndex, checked, record, e, eOpts) {
                record.changeMode('-');
            }
        }
    }, {
        flex: 1,
        text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.read,
        localize: {
            text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.read
        },
        dataIndex: 'modeTypeRead',
        align: 'center',
        xtype: 'checkcolumn',
        headerCheckbox: true,
        disabled: true,
        hideable: false,
        hidden: true,
        disabledCls: '', // or don't add this config if you want the field to look disabled
        bind: {
            hidden: '{hiddenColumns.modeTypeRead}',
            disabled: '{!actions.edit}'
        },
        listeners: {
            headercheckchange: function (columnHeader, checked, e, eOpts) {
                this.getView().getStore().each(function (record) {
                    record.changeMode('r');
                });
            },
            checkchange: function (check, rowIndex, checked, record, e, eOpts) {
                record.changeMode('r');
            }
        }
    }, {
        flex: 1,
        text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.default,
        localize: {
            text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.default
        },
        dataIndex: 'modeTypeDefault',
        align: 'center',
        xtype: 'checkcolumn',
        headerCheckbox: true,
        disabled: true,
        hideable: false,
        hidden: true,
        disabledCls: '', // or don't add this config if you want the field to look disabled
        bind: {
            hidden: '{hiddenColumns.modeTypeDefault}',
            disabled: '{!actions.edit}'
        },
        listeners: {
            headercheckchange: function (columnHeader, checked, e, eOpts) {
                this.getView().getStore().each(function (record) {
                    record.changeMode('w');
                });
            },
            checkchange: function (check, rowIndex, checked, record, e, eOpts) {
                record.changeMode('w');
            }
        }
    }, {
        flex: 1,
        text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.defaultread,
        localize: {
            text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.defaultread
        },
        dataIndex: 'modeTypeDefaultRead',
        align: 'center',
        xtype: 'checkcolumn',
        headerCheckbox: true,
        disabled: true,
        hideable: false,
        hidden: true,
        disabledCls: '', // or don't add this config if you want the field to look disabled
        bind: {
            hidden: '{hiddenColumns.modeTypeDefaultRead}',
            disabled: '{!actions.edit}'
        },
        listeners: {
            headercheckchange: function (columnHeader, checked, e, eOpts) {
                this.getView().getStore().each(function (record) {
                    record.changeMode('r');
                });
            },
            checkchange: function (check, rowIndex, checked, record, e, eOpts) {
                record.changeMode('r');
            }
        }
    }, {
        flex: 1,
        text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.write,
        localize: {
            text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.write
        },
        dataIndex: 'modeTypeWrite',
        align: 'center',
        xtype: 'checkcolumn',
        headerCheckbox: true,
        disabled: true,
        hideable: false,
        hidden: true,
        disabledCls: '', // or don't add this config if you want the field to look disabled
        bind: {
            hidden: '{hiddenColumns.modeTypeWrite}',
            disabled: '{!actions.edit}'
        },
        listeners: {
            headercheckchange: function (columnHeader, checked, e, eOpts) {
                this.getView().getStore().each(function (record) {
                    record.changeMode('w');
                });
            },
            checkchange: function (check, rowIndex, checked, record, e, eOpts) {
                record.changeMode('w');
            }
        }
    }, {
        xtype: 'actioncolumn',
        minWidth: 80,
        maxWidth: 80,
        text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.filters,
        localize: {
            text: CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.filters
        },
        hideable: false,
        disabled: true,
        hidden: true,
        border: 0,
        align: 'center',
        bind: {
            hidden: '{hiddenColumns.actionFilter}'
        },
        items: [{
            viewModel: {},
            bind: {
                disabled: '{!actions.edit}'
            },
            iconCls: 'cmdbuildicon-filter',
            tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.filters,
            localize: {
                tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.filters
            },
            getClass: function (v, meta, row, rowIndex, colIndex, store) {
                return 'cmdbuildicon-filter margin-right5';
            },
            handler: function (grid, rowIndex, colIndex) {
                var record = grid.getStore().getAt(rowIndex);
                grid.fireEvent("actionclonerowclick", grid, record, rowIndex, colIndex);
            },
            autoEl: {
                'data-testid': 'administration-permissions-grid-row-filter'
            }
        }, {
            viewModel: {},
            iconCls: 'cmdbuildicon-filter-remove',
            tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.removefilters,
            localize: {
                tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.removefilters
            },
            isDisabled: function (view, rowIndex, colIndex, item, record) {
                if (view.up().getViewModel().get('actions.view') || (record.get('filter') || record.get('filter').length)) {
                    return true;
                }
                return false;
            },
            getClass: function (v, meta, row, rowIndex, colIndex, store) {
                return 'cmdbuildicon-filter-remove margin-right5';
            },
            handler: function (grid, rowIndex, colIndex) {
                var record = grid.getStore().getAt(rowIndex);
                grid.fireEvent("actionclonerowclick", grid, record, rowIndex, colIndex);
            },
            autoEl: {
                'data-testid': 'administration-permissions-grid-row-filter'
            }
        }]

    }, {
        xtype: 'actioncolumn',
        minWidth: 80,
        maxWidth: 80,
        text: 'Actions',
        hideable: false,
        disabled: true,
        hidden: true,
        border: 0,
        align: 'center',
        bind: {
            hidden: '{hiddenColumns.actionActionDisabled}'
        },
        items: [{
            viewModel: {},
            bind: {
                disabled: '{!actions.edit}'
            },
            iconCls: 'cmdbuildicon-list',
            tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.disabledactions,
            localize: {
                tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.disabledactions
            },
            getClass: function (v, meta, row, rowIndex, colIndex, store) {
                return 'cmdbuildicon-list margin-right5';
            },
            // handler: function (grid, rowIndex, colIndex) {
            //     var record = grid.getStore().getAt(rowIndex);
            //     debugger;
            //     grid.fireEvent("actionclonerowclick", grid, record, rowIndex, colIndex);
            // },
             handler: 'onDisabledActionClick',
            autoEl: {
                'data-testid': 'administration-permissions-grid-row-list'
            }

        }, {
            viewModel: {},
            bind: {
                disabled: '{!actions.edit}'
            },
            iconCls: 'cmdbuildicon-list-alt-remove',
            tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.removedisabledactions,
            localize: {
                tooltip: CMDBuildUI.locales.Locales.administration.groupandpermissions.tooltips.removedisabledactions
            },
            getClass: function (v, meta, row, rowIndex, colIndex, store) {
                return 'cmdbuildicon-list-alt-remove margin-left5';
            },

            listeners: {
                click: 'onDisabledActionClick'
            },
            autoEl: {
                'data-testid': 'administration-permissions-grid-row-list'
            }
        }]

    }]
});