Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.tabitems.classes.ClassesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-groupsandpermissions-tabitems-permissions-tabitems-classes-classes',

    mixins: [
        'CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.PermissionsMixin'
    ],

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    onBeforeRender: function () {
        var vm = this.getView().up('administration-content-groupsandpermissions-tabitems-permissions-permissions').getViewModel();
        vm.setFormMode('view');
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEditBtnClick: function (button, e, eOpts) {
        var vm = this.getView().up('administration-content-groupsandpermissions-tabitems-permissions-permissions').getViewModel();
        vm.setFormMode('edit');
        this.toggleEnablePermissionsTabs(0);
        this.toggleEnableTabs(1);
    },


    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onDisabledActionClick: function (grid, rowIndex, colIndex, button, event, record) {
        grid.setSelection(record);
        var formMode = grid.grid.getViewModel().get('actions');
        var fbar;
        var me = this;

        switch (formMode.edit) {
            case true:
                fbar = [{
                    text: CMDBuildUI.locales.Locales.administration.common.actions.save,
                    localized: {
                        text: CMDBuildUI.locales.Locales.administration.common.actions.save
                    },
                    reference: 'savebutton',
                    itemId: 'savebutton',
                    viewModel: {},
                    listeners: {
                        click: function (button, event, eOpts) {
                            var vm = button.getViewModel();

                            // vm.get('record').reject();
                            var record = vm.get('record');
                            var fields = button.up('form').form.getFields().items;

                            Ext.Array.forEach(fields, function (element) {
                                var modelField = element.config.field;
                                var privilege = element.config.privilege;
                                var value = element.checked;

                                // record.get('customPrivileges')[privilege] = !value;
                                record.set(modelField, value);
                            });

                            record.crudState = 'R';
                            record.crudStateWas = 'R';
                            record.modified = {};
                            record.previousValues = {};
                            button.up('#popup-disabled-actions').fireEvent('close');

                        }
                    },
                    ui: 'administration-action'
                }, {
                    text: CMDBuildUI.locales.Locales.administration.common.actions.cancel,
                    localized: {
                        text: CMDBuildUI.locales.Locales.administration.common.actions.cancel
                    },
                    reference: 'cancelbutton',
                    ui: 'administration-secondary-action',
                    viewModel: {},
                    listeners: {
                        click: function (button, event, eOpts) {
                            var vm = this.getViewModel();

                            var record = vm.get('record');
                            for (var customPrivilege in record.previousValues) {
                                record.set(customPrivilege, record.previousValues[customPrivilege]);
                            }
                            record.previousValues = {};

                            button.up('#popup-disabled-actions').fireEvent('close');
                        }
                    }
                }];
                break;

            default:
                fbar = [{
                    text: CMDBuildUI.locales.Locales.administration.common.actions.close,
                    localized: {
                        text: CMDBuildUI.locales.Locales.administration.common.actions.close
                    },
                    reference: 'closebutton',
                    ui: 'administration-secondary-action',
                    handler: function (button) {
                        button.up('#popup-disabled-actions').fireEvent('close');
                    }
                }];
                break;
        }
        var content = {
            xtype: 'form',
            scrollable: 'y',
            padding: 10,
            reference: 'customPrivilegesChecks',
            bind: {
                actions: '{actions}',
                record: '{record}'
            },
            config: {
                selection: grid.getSelection(),
                record: grid.getSelection()
            },
            viewModel: {
                data: {
                    index: rowIndex
                }
            },
            fieldDefaults: {
                labelAlign: 'left',
                labelWidth: 150
            },
            items: [{
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.clone,
                localized: {
                    fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.clone
                },
                config: {
                    field: '_card_clone_disabled',
                    privilege: 'clone'
                },
                value: record.get('_card_clone_disabled'),
                // bind: {
                //     value: '{record._card_clone_disabled}'
                // },

                viewModel: {},
                readOnly: formMode.view,
                listeners: {
                    change: me.onDisabledActionsCheckChange
                }

            }, {
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.create,
                localized: {
                    fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.create
                },
                config: {
                    field: '_card_create_disabled',
                    privilege: 'create'
                },
                value: record.get('_card_create_disabled'),
                // bind: {
                //     value: '{record._card_create_disabled}'
                // },
                viewModel: {},
                readOnly: formMode.view,
                listeners: {
                    change: me.onDisabledActionsCheckChange
                }
            }, {
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.delete,
                localized: {
                    fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.delete
                },
                config: {
                    field: '_card_delete_disabled',
                    privilege: 'delete'
                },
                value: record.get('_card_delete_disabled'),
                // bind: {
                //     value: '{record._card_delete_disabled}'
                // },
                readOnly: formMode.view,
                listeners: {
                    change: me.onDisabledActionsCheckChange
                }
            }, {
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.update,
                localized: {
                    fieldLabel: CMDBuildUI.locales.Locales.administration.common.actions.update
                },
                config: {
                    field: '_card_update_disabled',
                    privilege: 'update'
                },
                value: record.get('_card_update_disabled'),
                // bind: {
                //     value: '{record._card_update_disabled}'
                // },
                readOnly: formMode.view,
                listeners: {
                    change: me.onDisabledActionsCheckChange
                }
            }],

            buttonAlign: 'center',
            fbar: fbar
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-disabled-actions');
            }
        };

        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-disabled-actions',
            CMDBuildUI.locales.Locales.administration.groupandpermissions.titles.disabledactions,
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '200px',
                height: '250px',
                reference: 'popup-disabled-actions',
                viewModel: {
                    data: {
                        index: rowIndex,
                        grid: grid,
                        record: record
                    }
                }
            }
        );
    },

    onDisabledActionsCheckChange: function (check, newValue, oldValue) {
        var field = check.config.field;
        var record = check.up().up().getViewModel().get('record');
        var grid = check.up().up().getViewModel().get('grid');
        var gridStore = grid.getStore();
        // debugger;

        gridStore.getById(record.getId()).set(field, newValue);
        check.up().up().getViewModel().set('record.' + field, newValue);

    }
});