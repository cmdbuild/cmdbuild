Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.tabitems.views.ViewsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-groupsandpermissions-tabitems-permissions-tabitems-views-views',
    
    stores:{
        objectTypeGrantsStore: {
            source: '{grantsStore}',
            filters: [{
                filterFn: function(rec){
                    if(rec.get('objectType') === 'view'){
                        return true;
                    }
                    return false;
                }
            }],
            autoLoad: true,
            autoDestroy: true
        }
    }

});
