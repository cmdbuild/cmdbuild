(function () {

    var elementId = 'CMDBuildAdministrationPermissions';
    Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.Permissions', {
        extend: 'Ext.tab.Panel',

        requires: [
            'CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.PermissionsController',
            'CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.permissions.PermissionsModel'
        ],
        alias: 'widget.administration-content-groupsandpermissions-tabitems-permissions-permissions',
        reference: 'administration-content-groupsandpermissions-tabitems-permissions-permissions',
        controller: 'administration-content-groupsandpermissions-tabitems-permissions-permissions',
        viewModel: {
            type: 'administration-content-groupsandpermissions-tabitems-permissions-permissions'
        },
        id: elementId,
        statics: {
            formmodes: {
                view: 'VIEW',
                edit: 'EDIT',
                create: 'ADD'
            }
        },
        tabPosition: 'top',
        tabRotation: 0,
        cls: 'administration-mainview-subtabpanel',
        ui: 'administration-tabandtools',
        scrollable: true,
        forceFit: true,
        layout: 'fit',
        config: {
            disabledTabs: {},
            theGroup: {}
        },
        bind: {
            //disabledTabs: '{disabledTabs}',
            theGroup: '{theGroup}'
        },

        defaults: {
            height: 25
        },

        dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            hidden: true,
            bind: {
                hidden: '{actions.view}'
            },
            items: [{
                xtype: 'component',
                flex: 1
            }, {
                text: CMDBuildUI.locales.Locales.administration.common.actions.save,
                localized: {
                    text: CMDBuildUI.locales.Locales.administration.common.actions.save
                },
                ui: 'administration-action-small',
                listeners: {
                    click: 'onSaveBtnClick'
                }
            }, {
                text: CMDBuildUI.locales.Locales.administration.common.actions.cancel,
                localized: {
                    text: CMDBuildUI.locales.Locales.administration.common.actions.cancel
                },
                ui: 'administration-secondary-action-small',
                listeners: {
                    click: 'onCancelBtnClick'
                }
            }]
        }]
    });
})();