Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.defaultfilters.DefaultFiltersModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content-groupsandpermissions-tabitems-defaultfilters-defaultfilters',

    stores: {
        defaultFiltersTreeStore: {
            type: 'tree',
            model: 'CMDBuildUI.model.menu.MenuItem',
            root: {
                text: 'Root',
                expanded: true,
                children: []
            },
            proxy: {
                type: 'memory'
            },
            sorters: [{
                property: 'index',
                direction: 'ASC'
            }],
            autoDestroy: true
        }
    }
});