Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.defaultfilters.DefaultFiltersController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-groupsandpermissions-tabitems-defaultfilters-defaultfilters',


    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onEditBtnClick: function (button, e, eOpts) {
        var vm = this.getViewModel('administration-content-groupsandpermissions-view');
        vm.set('actions.view', false);
        vm.set('actions.edit', true);
        vm.set('actions.add', false);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var me = this;
        button.setDisabled(true);
        var vm = this.getViewModel();
        if (!vm.get('theGroup').isValid()) {
            var validatorResult = vm.get('theGroup').validate();
            var errors = validatorResult.items;
            for (var i = 0; i < errors.length; i++) {
                console.log('Key :' + errors[i].field + ' , Message :' + errors[i].msg);
            }
        } else {
            var theGroup = vm.get('theGroup');
            delete theGroup.data.system;
            Ext.apply(theGroup.data, theGroup.getAssociatedData());
            theGroup.save({
                success: function (record, operation) {
                    var objectTypeName = record.getId();
                    var nextUrl = Ext.String.format('administration/groupsandpermissions/{0}', objectTypeName);
                    CMDBuildUI.util.administration.MenuStoreBuilder.initialize(
                        function () {
                            var treestore = Ext.getCmp('administrationNavigationTree');
                            var selected = treestore.getStore().findNode("href", nextUrl);
                            treestore.setSelection(selected);
                        });

                    me.redirectTo(nextUrl, true);
                }
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        var vm = this.getView().up('administration-content-groupsandpermissions-view').getViewModel();
        vm.get('theGroup').reject();
        vm.set('actions.view', true);
        vm.set('actions.edit', false);
        vm.set('actions.add', false);
        vm.toggleEnableTabs();
    },

    privates: {
        addTab: function (view, name, items, index, bind) {
            return view.add({
                xtype: "panel",
                items: items,
                reference: name,
                layout: 'fit',
                viewModel: {},
                autoScroll: true,
                padding: 0,
                tabConfig: {
                    //maxHeight:20,
                    ui: 'administration-tab-item',
                    tabIndex: index,
                    title: Ext.util.Format.capitalize(name).replace(/([a-z])([A-Z])/g, '$1 $2'),
                    tooltip: Ext.util.Format.capitalize(name).replace(/([a-z])([A-Z])/g, '$1 $2'),
                    autoEl: {
                        'data-testid': Ext.String.format('administration-groupandpermission-tab-', name)
                    }
                },
                bind: bind
            });
        }
    }
});