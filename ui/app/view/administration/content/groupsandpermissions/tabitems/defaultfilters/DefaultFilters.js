Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.defaultfilters.DefaultFilters', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.defaultfilters.DefaultFiltersController',
        'CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.defaultfilters.DefaultFiltersModel'
    ],
    alias: 'widget.administration-content-groupsandpermissions-tabitems-defaultfilters-defaultfilters',
    controller: 'administration-content-groupsandpermissions-tabitems-defaultfilters-defaultfilters',
    viewModel: {
        type: 'administration-content-groupsandpermissions-tabitems-defaultfilters-defaultfilters'
    },

    autoScroll: false,
    modelValidation: true,
    fieldDefaults: {
        labelAlign: 'top',
        width: '100%'
    },
    
    layout: 'border',
    items: [{
        xtype: 'components-administration-toolbars-formtoolbar',
        region: 'north',
        bind: {
            hidden: '{actions.view}'
        },
        items: [{
            xtype: 'button',
            itemId: 'spacer',
            style: {
                "visibility": "hidden"
            }
        }]
    }, {
        xtype: 'components-administration-toolbars-formtoolbar',
        region: 'north',
        bind: {
            hidden: '{!actions.view}'
        },
        items: [{
            xtype: 'button',
            itemId: 'spacer',
            style: {
                "visibility": "hidden"
            }
        }, {
            xtype: 'tbfill'
        }, {
            xtype: 'tool',
            align: 'right',
            itemId: 'editBtn',
            cls: 'administration-tool',
            iconCls: 'x-fa fa-pencil',
            tooltip: CMDBuildUI.locales.Locales.administration.common.actions.edit,
            localized: {
                tooltip: 'CMDBuildUI.locales.Locales.administration.common.actions.edit'
            },
            callback: 'onEditBtnClick',
            hidden: true,
            autoEl: {
                'data-testid': 'administration-groupandpermission-users-tool-editbtn'
            },
            bind: {
                hidden: '{!actions.view}'
            }
        }]
    }, {
        xtype: 'treepanel',
        viewModel: {},
        region: 'center',
        scrollable: 'y',
        useArrows: true,
        rootVisible: false,
        bind: {
            store: '{defaultFiltersTreeStore}'
        },
        multiSelect: true,
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            text:  CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.class,
            localized: {
                text: 'CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.class'
            },
            width: '50%',
            sortable: false,
            dataIndex: 'name'
        }, {
            //we must use the templateheader component so we can use a custom tpl
            xtype: 'treecolumn',
            text:  CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.defaultfilter,
            localized: {
                text: 'CMDBuildUI.locales.Locales.administration.groupandpermissions.texts.defaultfilter'
            },
            width: '50%',
            sortable: false,
            dataIndex: 'filter'
        }]
    }],

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        ui: 'footer',
        hidden: true,
        bind: {
            hidden: '{actions.view}'
        },
        items: [{
            xtype: 'component',
            flex: 1
        }, {
            text: CMDBuildUI.locales.Locales.administration.common.actions.save,
            localized: {
                text: 'CMDBuildUI.locales.Locales.administration.common.actions.save'
            },
            formBind: true, //only enabled once the form is valid
            disabled: true,
            ui: 'administration-action-small',
            listeners: {
                click: 'onSaveBtnClick'
            }
        }, {
            text: CMDBuildUI.locales.Locales.administration.common.actions.cancel,
            localized: {
                text: 'CMDBuildUI.locales.Locales.administration.common.actions.cancel'
            },
            ui: 'administration-secondary-action-small',
            listeners: {
                click: 'onCancelBtnClick'
            }
        }]
    }]
});