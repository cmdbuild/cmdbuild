Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.group.PropertiesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-groupsandpermissions-tabitems-group-properties',

    require: [
        'CMDBuildUI.util.Utilities',
        'CMDBuildUI.util.administration.helper.FormHelper'
    ],

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * 
     * @param {CMDBuildUI.view.administration.content.groupsandpermissions.tabitems.group.Properties} view 
     */
    onBeforeRender: function (view) {
        var vm = this.getView().up('administration-content-groupsandpermissions-view').getViewModel();
        Ext.asap(function () {
            var theGroup = vm.get('theGroup');
            var isPhantom = theGroup.phantom;
            if (isPhantom) {
                vm.toggleEnableTabs(0);
            }
        }, this);
    },

    onEditBtnClick: function (button) {
        var vm = this.getViewModel('administration-content-groupsandpermissions-view');
        vm.set('actions.view', false);
        vm.set('actions.edit', true);
        vm.set('actions.add', false);
    },

    onToggleEnableBtnClick: function () {
        var vm = this.getViewModel('administration-content-groupsandpermissions-view');

        var value = !vm.get('theGroup.active');
        vm.set('theGroup.active', value);
        vm.get('theGroup').save({
            success: function (record, operation) {
                var w = Ext.create('Ext.window.Toast', {
                    title: 'Success!',
                    html: Ext.String.format('Group was {0} correctly.', (value) ? 'activated' : 'deactivated'), // todo: translate
                    iconCls: 'x-fa fa-check-circle',
                    align: 'br'
                });
                w.show();
            }
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var me = this;
        button.setDisabled(true);
        var vm = this.getViewModel();
        if (!vm.get('theGroup').isValid()) {
            var validatorResult = vm.get('theGroup').validate();
            var errors = validatorResult.items;
            for (var i = 0; i < errors.length; i++) {
                console.log('Key :' + errors[i].field + ' , Message :' + errors[i].msg);
            }
        } else {
            var theGroup = vm.get('theGroup');
            delete theGroup.data.system;
            Ext.apply(theGroup.data, theGroup.getAssociatedData());
            theGroup.save({
                success: function (record, operation) {
                    var objectTypeName = record.getId();
                    var nextUrl = Ext.String.format('administration/groupsandpermissions/{0}', objectTypeName);
                    CMDBuildUI.util.administration.MenuStoreBuilder.initialize(
                        function () {
                            var treestore = Ext.getCmp('administrationNavigationTree');
                            var selected = treestore.getStore().findNode("href", nextUrl);
                            treestore.setSelection(selected);
                        });

                    me.redirectTo(nextUrl, true);
                }
            });
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelBtnClick: function (button, e, eOpts) {
        var vm = this.getView().up('administration-content-groupsandpermissions-view').getViewModel();
        vm.get('theGroup').reject();
        vm.set('actions.view', true);
        vm.set('actions.edit', false);
        vm.set('actions.add', false);
        vm.toggleEnableTabs();
    },

    /**
     * On translate button click
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onTranslateClick: function (button, e, eOpts) {

        var vm = this.getViewModel();
        var theGroup = vm.get('theGroup');

        var content = {
            xtype: 'administration-localization-localizecontent',
            scrollable: 'y',
            viewModel: {
                data: {
                    action: 'edit',
                    translationCode: Ext.String.format('lookup.{0}.description', theGroup.get('_type'))
                }
            }
        };
        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                CMDBuildUI.util.Utilities.closePopup('popup-edit-group-localization');
            }
        };
        // create panel
        var popUp = CMDBuildUI.util.Utilities.openPopup(
            'popup-edit-group-localization',
            'Localization text',
            content,
            listeners, {
                ui: 'administration-actionpanel',
                width: '450px',
                height: '450px'
            }
        );

        popUp.setPagePosition(e.getX() - 450, e.getY() - 100);
    }

});