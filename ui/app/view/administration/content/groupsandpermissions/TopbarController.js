Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.TopbarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content-groupsandpermissions-topbar',

    control: {
        '#addgroup': {
            click: 'onAddGroupClick'
        }
    },

    /**
     * 
     * @param {Ext.button} button 
     * @param {*} event 
     * @param {*} eOpts 
     */
    onAddGroupClick: function (button, event, eOpt) {
        this.redirectTo('administration/groupsandpermissions_empty', true);
        var vm = Ext.getCmp('administrationNavigationTree').getViewModel();
        vm.set('selected', null);
    }
});