(function () {

    var elementId = 'CMDBuildAdministrationContentGroupView';
    Ext.define('CMDBuildUI.view.administration.content.groupsandpermissions.View', {
        extend: 'Ext.container.Container',

        requires: [
            'CMDBuildUI.view.administration.content.groupsandpermissions.ViewController',
            'CMDBuildUI.view.administration.content.groupsandpermissions.ViewModel'
        ],
        bind: {

        },
        alias: 'widget.administration-content-groupsandpermissions-view',
        controller: 'administration-content-groupsandpermissions-view',
        viewModel: {
            type: 'administration-content-groupsandpermissions-view'
        },
        statics: {
            elementId: elementId
        },

        id: elementId,
        config: {
            objectTypeName: null,
            allowFilter: true,
            showAddButton: true,
            action: 'VIEW',
            title: null
        },
        defaults: {
            textAlign: 'left',
            scrollable: true
        },
        layout: 'border',
        style: 'background-color:#fff',
        items: [{
            xtype: 'administration-content-groupsandpermissions-topbar',
            region: 'north',
            viewModel: {}
        }, {
            xtype: 'administration-content-groupsandpermissions-tabpanel',
            region: 'center',
            viewModel: { }
        }],

        initComponent: function () {
            var vm = this.getViewModel();
            vm.getParent().set('title', 'Groups and permissions'); // TODO: translate
            this.callParent(arguments);
        },
        listeners: {
            afterlayout: function (panel) {
                Ext.GlobalEvents.fireEventArgs("showadministrationcontentmask", [false]);
            }
        }
    });
})();