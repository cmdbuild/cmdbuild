Ext.define('CMDBuildUI.view.administration.ContentModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-content',
    data: {
        title: CMDBuildUI.locales.Locales.administration.title
    }

});
