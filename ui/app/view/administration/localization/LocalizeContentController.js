Ext.define('CMDBuildUI.view.administration.localization.LocalizeContentController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-localization-localizecontent',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },
    /**
     * @param {CMDBuildUI.view.administration.localization.LocalizeContent} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        
        var vm = view.getViewModel();
        var action = vm.get('action');
        vm.linkTo("theTranslation", {
            type: 'CMDBuildUI.model.Translation',
            id: vm.get('translationCode')
        });
        var languagesStore = vm.getStore('languagesStore');
        var localizationStore = vm.getStore('localizationStore');
        var languages = [];

        function generateLocaleInput(language, locale) {
            var fieldContainer = {
                xtype: (action === 'edit') ? 'textfield' : 'displayfield',
                fieldLabel: language.get('description'),
                beforeLabelTpl: '<img width="20px" src="resources/images/flags/' + language.get('code') + '.png" alt="' + language.get('code') + ' flag"></img>',
                style: {
                    tableLayout: 'unset',
                    width: '100%'
                },
                bind: {
                    value: '{theTranslation.' + language.get('code') + '}'
                }
            };
            return fieldContainer;
        }
        localizationStore.getProxy().setUrl('/translations/' + vm.get('translationCode'));

        languagesStore.load(function (records, operation, success) {
            if (success) {
                localizationStore.load(function (localization, operation, success) {
                    localization = localization[0];
                    for (var i = 0; i < records.length; i++) {
                        var lang = records[i];
                        if (localization && !localization.get(lang.get('code'))) {
                            localization.set(lang.get('code'), '');
                        }
                        vm.set('localization', localization);
                        var translation = localization.get(lang.get('code')) || '';
                        languages.push(generateLocaleInput(lang, translation));
                    }
                    view.add({
                        margin: '10px',
                        items: languages
                    });
                });
            }
        });
    },

    /**
    *             
    */
    onCancelBtnClick: function () {
        this.getView().up().fireEvent('close');

    },

    /**
     * onSaveBtnClick
     */
    onSaveBtnClick: function () {
        var me = this;
        this.getViewModel().get('theTranslation').save({
            success: function (record, operation) {
                // var w = Ext.create('Ext.window.Toast', {
                //     ui: 'administration',
                //     html: 'Localization correctly.',
                //     title: 'Success!',
                //     iconCls: 'x-fa fa-check-circle',
                //     align: 'br'
                // });
                // w.show();
                me.onCancelBtnClick();
            }
        });
    }
});
