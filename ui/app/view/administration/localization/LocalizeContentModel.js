Ext.define('CMDBuildUI.view.administration.localization.LocalizeContentModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-localization-localizecontent',
    data: {
        action: null,
        actions: {
            edit: false,
            add: false,
            view: false
        },
        languages: []
    },
    formulas: {
        actionManager: {
            bind: '{action}',
            get: function (action) {
                if (action) {
                    this.set('actions.add', action === 'ADD');
                    this.set('actions.edit', action === 'EDIT');
                    this.set('actions.view', action === 'VIEW');
                }
            }
        }
    },
    stores: {
        languagesStore: {
            model: 'CMDBuildUI.model.Language',
            proxy: {
                type: 'baseproxy',
                url: '/languages',
                extraParams: {
                    active: true
                }
            },
            sorters: 'description',
            autoLoad: false,
            autoDestroy: true
        },
        localizationStore: {
            model: 'CMDBuildUI.model.Translation',
            proxy:{
                type: 'baseproxy',
                url: '/translations'
            },
            autoload: false,
            autoDestroy: true
        }
    }
});
