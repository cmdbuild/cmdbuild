Ext.define('CMDBuildUI.view.administration.DetailsWindowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-detailswindow',

    control: {
        '#': {
            boxready: 'onBoxReady',
            attributecreated: 'onAttributeCreated'
        }
    },

    /**
     * @param {CMDBuildUI.view.administration.DetailsWindow} window
     * @param {Number} width
     * @param {Number} height
     * @param {Object} eOpts
     */
    onBoxReady: function (window, width, height, eOpts) {
        this.getView().anchorTo(Ext.getBody(), 'br-br', {
            x: -30,
            y: 0
        });
    },

    onClosed: function (recordType, eOpts) {

        this.destroyDetailsWindow();
        this.fireEvent('close');
    },
    onClose: function (recordType, eOpts) {

    },
    onAttributeCreated: function (objectTypeName, attributeName, grid) {
        var view = this.getView();
        view.removeAll();

        view.add({
            xtype: 'administration-content-classes-tabitems-attributes-card-view',
            viewModel: {
                data: {
                    objectTypeName: objectTypeName,
                    attributeName: attributeName
                }
            }
        });
    },

    /**
     * @private
     */
    destroyDetailsWindow: function () {
        var view = this.getView();
        if (view) {
            view.close();
        }
    }
});