Ext.define('CMDBuildUI.view.administration.navigation.ContainerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-navigation-container',

    control: {
        '#': {
            afterrender: 'onAfterRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.management.navigation.Container} view
     * @param {Object} eOpts
     */
    onAfterRender: function (view, eOpts) {
        var me = this;
        var startingNode;
        var store = Ext.getStore('administration.MenuAdministration');
        store.on('datachanged', function (store) {

            var currentNode = store.findNode("href", Ext.History.getToken());

            if (!startingNode) {
                startingNode = me.getFirstSelectableMenuItem(store.getRootNode().childNodes);
            }

            if (view.items) {
                var vm = view.items.items[0].getViewModel();
                if (currentNode) {
                    vm.set('selected', currentNode);
                }
                if (!vm.get('selected')) {
                    vm.set('selected', startingNode);
                }
            }
        });
        view.add({
            xtype: 'administration-navigation-tree',
            data: {
                selected: startingNode
            },
            viewModel: {
                stores: {
                    menuItems: store
                }
            }
        });
    },
    privates: {
        /**
         * @param {CMDBuildUI.model.menu.MenuItem[]} items
         * @return {CMDBuildUI.model.menu.MenuItem} First selectable menu item
         */
        getFirstSelectableMenuItem: function (items) {
            var item;
            var i = 0;
            while (!item && i < items.length) {
                var node = items[i];
                if (node.get("menutype") !== CMDBuildUI.model.menu.MenuItem.types.folder) {
                    item = node;
                } else {
                    item = this.getFirstSelectableMenuItem(node.childNodes);
                }
                i++;
            }
            return item;
        }
    }
});
