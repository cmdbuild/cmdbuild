Ext.define('CMDBuildUI.view.administration.navigation.TreeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-navigation-tree',

    control: {
        '#': {
            selectionchange: "onSelectionChange"
        }
    },

    /**
     * @param {Ext.list.Tree} view
     * @param {Ext.data.TreeModel} record
     * @param {Object} eOpts
     */
    onSelectionChange: function (view, record, eOpts) {

        // redirect to selected
        if (!record) {
            return;
        }

        var data = record.getData();
        var node = view.getItem(record);
        this.expandNodeHierarchy(node);
        var menutype = data.menutype;
        var url;
        if (menutype !== CMDBuildUI.model.menu.MenuItem.types.folder) {
            switch (menutype) {
                case CMDBuildUI.model.menu.MenuItem.types.klass:

                    // expand nodes     
                    url = 'administration/classes/' + data.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.lookuptype:
                    url = 'administration/lookup_types/' + data.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.domain:
                    url = 'administration/domains/' + data.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.menu:
                    url = 'administration/menus/' + data.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.process:
                    url = 'administration/processes/' + data.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.user:
                    url = 'administration/users';
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.groupsandpermissions:
                    url = 'administration/groupsandpermissions/' + data.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.setup:
                    url = data.href;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.report:
                    url = data.href;
                    break;
                default:
                    Ext.Msg.alert('Warning', 'Menu type not implemented!');
            }
        } else {
            //
            switch (data.objecttype.toLowerCase()) {
                case CMDBuildUI.model.administration.MenuItem.types.klass:
                    url = 'administration/classes_empty';
                    break;
                case 'simples':
                    url = 'administration/classes_empty';
                    break;
                case 'standard':
                    // expand nodes     
                    //this.expandNodeHierarchy(view.getItem(record));

                    url = 'administration/classes_empty';
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.process:
                    url = 'administration/processes_empty';
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.lookuptype:

                    // expand nodes     
                    //this.expandNodeHierarchy(view.getItem(record));

                    url = 'administration/lookup_types_empty';
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.domain:
                    url = 'administration/domains_empty';
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.report:
                    url = 'administration/reports_empty';
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.menu:
                    url = 'administration/menus_empty';
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.user:
                    url = 'administration/users_empty';
                    break;
                default:
                    this.expandNodeHierarchy(view.getItem(record));
                    break; //Ext.Msg.alert('Warning', 'Menu type not implemented!');
            }
        }

        if (url !== undefined) {
            this.redirectTo(url);
        }
    },

    privates: {
        expandNodeHierarchy: function (node) {
            node.expand();
            if (node.getParentItem()) {
                this.expandNodeHierarchy(node.getParentItem());
            }
        }
    }


});