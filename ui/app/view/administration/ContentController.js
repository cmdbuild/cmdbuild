Ext.define('CMDBuildUI.view.administration.ContentController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.administration-content',
    listen: {
        global: {
            showadministrationcontentmask: 'onShowAdministrationContentMask'
        }
    },

    config: {
        maskCount: 0
    },
    onShowAdministrationContentMask: function (active) {
        var me = this;
        switch (active) {
            case true:
                me.view.mask(CMDBuildUI.locales.Locales.administration.common.messages.loading);
                me.setMaskCount(me.getMaskCount() + 1);
                break;
            default:
                me.setMaskCount(me.getMaskCount() - 1);
                if (me.getMaskCount() <= 0) {
                    me.setMaskCount(0);
                    setTimeout(function () {
                        if (me.view) {
                            me.view.unmask();
                        }
                    }, 250);
                }
                break;
        }
    }

});