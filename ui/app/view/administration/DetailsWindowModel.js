Ext.define('CMDBuildUI.view.administration.DetailsWindowModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.administration-detailswindow',

    data: {
        actionDescription: null,
        typeDescription: null,
        itemDescription: null,
        title: null
    }

});
