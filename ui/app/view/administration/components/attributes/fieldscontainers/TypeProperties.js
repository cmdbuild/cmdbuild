Ext.define('CMDBuildUI.view.administration.components.attributes.fieldscontainers.TypeProperties', {
    extend: 'Ext.form.Panel',

    alias: 'widget.administration-components-attributes-fieldscontainers-typeproperties',

    fieldDefaults: {
        labelAlign: 'top',
        xtype: 'fieldcontainer',
        flex: '0.5',
        //padding: '0 15 0 15',
        layout: 'anchor'
    },

    items: [{
        layout: 'column',
        items: [{
            columnWidth: 0.5,
            xtype: 'displayfield',
            fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.type,
            localized: {
                fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.type'
            }, 
            name: 'type',
            bind: {
                value: '{theAttribute.type}'
            },
            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (value) {
                    return Ext.String.capitalize(value.toLowerCase());
                }
            }
        }]
    }, {
        // If type is reference
        bind: {
            hidden: '{!types.isReference}'
        },
        hidden: true,
        items: [{
            layout: 'column',

            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.domain,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.domain'
                }, 
                name: 'domain',
                bind: {
                    value: '{theAttribute.domain}'
                },
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    if (value) {
                        return Ext.String.capitalize(value.toLowerCase()); // TODO: translate
                    }
                }
            }, {
                columnWidth: 0.5,
                xtype: 'textarea',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.filter,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.filter'
                }, 
                name: 'filter',
                readOnly: true,
                bind: {
                    value: "{theAttribute.filter}"
                }
            }]
        }, {
            layout: 'column',

            items: [{
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.preselectifunique,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.preselectifunique'
                }, 
                name: 'preselectIfUnique',
                readOnly: true,
                bind: {
                    value: '{preselectIfUnique}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'button',
                text: CMDBuildUI.locales.Locales.administration.attributes.texts.viewmetadata,
                localized: {
                    text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.viewmetadata'
                }, 
                reference: 'viewmetadata',
                itemId: 'viewmetadata',
                iconCls: 'x-fa fa-external-link',
                ui: 'administration-action-small',
                style: 'margin-top:25px',
                handler: function () {
                    console.log('view metadata'); // TODO: open popup for metadata;
                }
            }]
        }]
    }, {
        // If type is decimal
        bind: {
            hidden: '{!types.isDecimal}'
        },
        hidden: true,
        items: [{
            layout: 'column',

            items: [{
                columnWidth: 0.5,
                xtype: 'numberfield',
                itemId: 'precisionAttributeField',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision'
                }, 
                name: 'precision',
                step: 1,
                bind: {
                    value: '{theAttribute.precision}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'numberfield',
                itemId: 'scaleAttributeField',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.name.scale,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision'
                }, 
                name: 'scale',
                step: 1,
                bind: {
                    value: "{theAttribute.scale}"
                }
            }]
        }]
    }, {
        // If type is lookup
        bind: {
            hidden: '{!types.isLookup}'
        },
        hidden: true,
        items: [{
            layout: 'column',

            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.lookup,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision'
                }, 
                name: 'lookup',
                bind: {
                    value: '{theAttribute.lookupType}'
                }
            }]
        }]
    }, {
        // If type is string
        bind: {
            hidden: '{!types.isString}'
        },
        hidden: true,
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.maxlength,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision'
                }, 
                name: 'maxLength',
                bind: {
                    value: '{theAttribute.maxLength}'
                }
            }]
        }]
    }, {
        // If type is text
        bind: {
            hidden: '{!types.isText}'
        },
        hidden: true,
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.editortype,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.editortype'
                },
                name: 'editorType',
                bind: {
                    value: '{theAttribute.editorType}'
                },
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    if (value) {
                        switch (value) {
                            case 'HTML':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.editorhtml;
                            case 'PLAIN':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.plaintext;
                        }
                    }
                }
            }]
        }]
    }, {
        // If type is ip address
        bind: {
            hidden: '{!types.isIpAddress}'
        },
        hidden: true,
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.iptype,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.iptype'
                }, 
                name: 'ipType',
                bind: {
                    value: '{theAttribute.ipType}'
                },
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    if (value) {
                        switch (value) {
                            case 'ipv4':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.ipv4;
                            case 'ipv6':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.ipv6;
                            case 'any':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.any;
                        }
                    }
                }
            }]
        }]
    }, {
        hidden: true,
        bind: {
            hidden: '{!theAttribute}'
        },
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'textarea',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.help,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.help'
                }, 
                name: 'cm_help',
                readOnly: true,
                bind: {
                    value: '{theAttribute.help}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'textarea',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showif,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showif'
                }, 
                name: 'cm_showIf',
                readOnly: true,
                bind: {
                    value: '{theAttribute.showIf}'
                }
            }]
        }]
    }, {
        hidden: true,
        bind: {
            hidden: '{!theAttribute}'
        },
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'textarea',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.validationrules,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.validationrules'
                }, 
                name: 'cm_validadationRules',
                readOnly: true,
                bind: {
                    value: '{theAttribute.validationRules}'
                }
            }, {
                columnWidth: 0.5,
                hidden: true, // TODO: not supported until 3.x 
                xtype: 'textarea',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.actionpostvalidation,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.actionpostvalidation'
                }, 
                name: 'cm_actionsPostValidation',
                readOnly: true,
                bind: {
                    value: '{theAttribute.actionPostValidation}'
                }
            }]
        }]
    }]
});