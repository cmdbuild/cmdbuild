Ext.define('CMDBuildUI.view.administration.components.attributes.actionscontainers.View', {
    // extend: 'Ext.form.Panel',
    extend: 'CMDBuildUI.components.tab.FormPanel',
    requires: [
        'CMDBuildUI.view.administration.components.attributes.fieldscontainers.GeneralProperties',
        'CMDBuildUI.view.administration.components.attributes.fieldscontainers.TypeProperties'
    ],
    alias: 'widget.administration-components-attributes-actionscontainers-view',

    config: {
        objectTypeName: null,
        objectId: null,
        shownInPopup: false
    },

    scrollable: true,
    cls: 'administration tab-hidden',
    ui: 'administration-tabandtools',
    items: [{
        xtype: 'container',
        items: [{
            ui: 'administration-formpagination',
            xtype: "fieldset",
            collapsible: true,
            title: CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties,
            localized: {
                title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties'
            },
            items: [{
                xtype: 'administration-components-attributes-fieldscontainers-generalproperties'
            }]
        }, {
            ui: 'administration-formpagination',
            xtype: "fieldset",
            collapsible: true,
            title: CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties,
            localized: {
                title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties'
            },
            items: [{
                xtype: 'administration-components-attributes-fieldscontainers-typeproperties'
            }]
        }]
    }],

    tools: [{
        xtype: 'tbfill'
    }, {
        xtype: 'tool',
        itemId: 'editAttributeBtn',
        glyph: 'f040@FontAwesome', // Pencil icon
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.editattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.editattribute'
        },
        callback: 'onEditAttributeBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-attributes-tool-editbtn'
        },
        bind: {
            hidden: '{!canEdit}'
        }
    }, {
        xtype: 'tool',
        itemId: 'deleteAttributeBtn',
        glyph: 'f014@FontAwesome',
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.deleteattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.deleteattribute'
        },
        callback: 'onDeleteAttributeBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-attributes-tool-deletebtn'
        },
        bind: {
            disabled: '{!canDelete}'
        }
    }, {
        xtype: 'tool',
        itemId: 'disableBtn',
        cls: 'administration-tool',
        iconCls: 'x-fa fa-ban',
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.disableattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.disableattribute'
        },
        callback: 'onToggleBtnClick',
        hidden: true,
        autoEl: {
            'data-testid': 'administration-attributes-tool-disablebtn'
        },
        bind: {
            hidden: '{!theAttribute.active}'
        }
    }, {
        xtype: 'tool',
        itemId: 'enableBtn',
        hidden: true,
        cls: 'administration-tool',
        iconCls: 'x-fa fa-check-circle-o',
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.enableattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.enableattribute'
        },
        callback: 'onToggleBtnClick',
        autoEl: {
            'data-testid': 'administration-attributes-tool-enablebtn'
        },
        bind: {
            hidden: '{theAttribute.active}'
        }
    }]
});