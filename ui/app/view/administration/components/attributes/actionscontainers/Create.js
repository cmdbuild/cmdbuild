Ext.define('CMDBuildUI.view.administration.components.attributes.actionscontainers.Create', {
    extend: 'Ext.form.Panel',
    config: {
        objectTypeName: null,
        /**
         * @cfg {Object[]}
         * 
         * Can set default values for any of the attributes. An object can be:
         * `{attribute: 'attribute name', value: 'default value', editable: true|false}` 
         * used for all attributes or
         * `{domain: 'domain name', value: 'default value', editable: true|false}` 
         * used to set default values for references fields.
         */
        defaultValues: null
    },

    modelValidation: true,
    autoScroll: true,
    fieldDefaults: {
        labelAlign: 'top'
    },
    bubbleEvents: [
        'itemcreated',
        'cancelcreation'
    ],
    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties,
        localized: {
            title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties'
        },
        items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'textfield',
                    maxLength: 20,
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.name,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.name'
                    },
                    name: 'name',
                    bind: {
                        value: '{theAttribute.name}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'fieldcontainer',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.description,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.description'
                    },
                    layout: 'column',

                    items: [{
                        columnWidth: 0.9,
                        xtype: 'textfield',

                        name: 'description',
                        bind: {
                            value: '{theAttribute.description}'
                        }
                    }, {
                        xtype: 'button',
                        columnWidth: 0.1,
                        itemId: 'translateBtn',
                        iconCls: 'x-fa fa-flag fa-2x',
                        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.translate,
                        localized: {
                            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.translate'
                        },
                        handler: function (grid, rowIndex, colIndex) {
                            console.log('translate here'); // TODO: translate
                        },
                        cls: 'administration-field-container-btn',
                        autoEl: {
                            'data-testid': 'administration-attributes-card-edit-translateBtn'
                        }
                    }]
                }]
            },
            {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'combo',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.group,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.group'
                    },
                    name: 'group',
                    allowBlank: true,
                    displayField: 'label',
                    valueField: 'value',
                    bind: {
                        value: '{theAttribute.group}',
                        store: '{attributeGroupStore}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'combo',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mode,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mode'
                    },
                    name: 'mode',
                    clearFilterOnBlur: false,
                    anyMatch: true,
                    autoSelect: true,
                    forceSelection: true,
                    allowBlank: false,
                    displayField: 'label',
                    valueField: 'value',
                    bind: {
                        value: '{theAttribute.mode}',
                        store: '{attributeModeStore}'
                    },
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        switch (value) {
                            case 'apm_write':
                            case 'write':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.editable;
                            case 'apm_read':
                            case 'read':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.readonly;
                            case 'apm_hidden':
                            case 'hidden':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.hidden;
                            case 'apm_immutable':
                            case 'immutable':
                                return CMDBuildUI.locales.Locales.administration.attributes.strings.immutable;
                        }
                    }
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showingrid,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showingrid'
                    },
                    name: 'showInGrid',
                    bind: {
                        value: '{theAttribute.showInGrid}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showinreducedgrid,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showinreducedgrid'
                    },
                    name: 'unique',
                    bind: {
                        value: '{theAttribute.showInReducedGrid}'
                    }
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.unique,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.unique'
                    },
                    name: 'unique',
                    bind: {
                        value: '{theAttribute.unique}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mandatory,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mandatory'
                    },
                    name: 'mandatory',
                    bind: {
                        value: '{theAttribute.mandatory}'
                    }
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.active,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.active'
                    },
                    name: 'active',
                    bind: {
                        value: '{theAttribute.active}'
                    }
                }]
            }
        ]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties,
        localized: {
            title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties'
        },
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'combo',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.type,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.type'
                },
                name: 'type',
                allowBlank: false,
                displayField: 'label',
                valueField: 'value',
                bind: {
                    value: '{theAttribute.type}',
                    store: '{typeStore}'
                },
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    if (value) {
                        return Ext.String.capitalize(value.toLowerCase());
                    }
                }
            }]
        }, {
            // If type is reference
            bind: {
                hidden: '{!types.isReference}'
            },
            hidden:false,
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'combobox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.domain,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.domain'
                    },
                    name: 'domain',
                    clearFilterOnBlur: true,
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'name',
                    bind: {
                        value: '{theAttribute.domain}',
                        store: '{domainsStore}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.filter,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.filter'
                    },
                    name: 'filter',
                    bind: {
                        value: "{theAttribute.filter}"
                    }
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.preselectifunique,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.preselectifunique'
                    },
                    name: 'preselectIfUnique',
                    bind: {
                        value: '{preselectIfUnique}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'button',
                    text: CMDBuildUI.locales.Locales.administration.attributes.texts.viewmetadata,
                    localized: {
                        text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.viewmetadata'
                    },
                    reference: 'viewmetadata',
                    itemId: 'viewmetadata',
                    iconCls: 'x-fa fa-pencil',
                    ui: 'administration-action-small',
                    style: 'margin-top:25px',
                    handler: function () {
                        console.log('view metadata');
                        // TODO: open popup for metadata
                    }
                }]
            }]
        }, {
            // If type is decimal
            bind: {
                hidden: '{!types.isDecimal}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    columnWidth: 0.5,
                    xtype: 'numberfield',
                    itemId: 'precisionAttributeField',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision'
                    },
                    name: 'type',
                    step: 1,
                    bind: {
                        value: '{theAttribute.precision}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'numberfield',
                    itemId: 'scaleAttributeField',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.scale,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.scale'
                    },
                    name: 'scale',
                    step: 1,
                    bind: {
                        value: "{theAttribute.scale}"
                    }
                }]
            }]
        }, {
            // If type is lookup
            bind: {
                hidden: '{!types.isLookup}'
            },
            hidden: true,
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'combo',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.lookup,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.lookup'
                    },
                    name: 'lookup',
                    clearFilterOnBlur: false,
                    anyMatch: true,
                    autoSelect: true,
                    forceSelection: true,
                    displayField: 'name',
                    valueField: '_id',
                    bind: {
                        value: '{theAttribute.lookupType}',
                        store: '{lookupStore}'
                    },
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        if (value) {
                            return Ext.String.capitalize(value.toLowerCase()); // TODO: translate
                        }
                    }
                }]
            }]
        }, {
            // If type is string
            bind: {
                hidden: '{!types.isString}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    columnWidth: 0.5,
                    xtype: 'numberfield',
                    step: 1,
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.maxlength,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.maxlength'
                    },
                    name: 'maxLength',
                    bind: {
                        value: '{theAttribute.maxLength}'
                    }
                }]
            }]
        }, {
            // If type is text
            bind: {
                hidden: '{!types.isText}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    columnWidth: 0.5,
                    xtype: 'combo',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.editortype,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.editortype'
                    },
                    name: 'editorType',
                    clearFilterOnBlur: false,
                    anyMatch: true,
                    autoSelect: true,
                    forceSelection: true,
                    displayField: 'label',
                    valueField: 'value',
                    bind: {
                        value: '{theAttribute.editorType}',
                        store: '{editorTypeStore}'
                    },
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        if (value) {
                            switch (value) {
                                case 'HTML':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.editorhtml;
                                case 'PLAIN':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.plaintext;
                            }
                        }
                    }
                }]
            }]
        }, {
            // If type is ip address
            bind: {
                hidden: '{!types.isIpAddress}'
            },
            hidden: true,
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'combo',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.iptype,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.iptype'
                    },
                    name: 'ipType',
                    clearFilterOnBlur: false,
                    anyMatch: true,
                    autoSelect: true,
                    forceSelection: true,
                    displayField: 'label',
                    valueField: 'value',
                    bind: {
                        value: '{theAttribute.ipType}',
                        store: '{ipTypeStore}'
                    },
                    renderer: function (value) {
                        if (value) {
                            switch (value) {
                                case 'ipv4':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.ipv4;
                                case 'ipv6':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.ipv6;
                                case 'any':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.any;
                            }
                        }
                    }
                }]
            }]
        }, {
            hidden: true,
            bind: {
                hidden: '{!theAttribute}'
            },
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.help,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.help'
                    },
                    name: 'help',
                    bind: {
                        value: '{help}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showif,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showif'
                    },
                    name: 'showIf',
                    bind: {
                        value: '{showIf}'
                    }
                }]
            }]
        }, {

            hidden: true,
            bind: {
                hidden: '{!theAttribute}'
            },
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.validationrules,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.validationrules'
                    },
                    name: 'validadationRules',
                    bind: {
                        value: '{validationRules}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.actionpostvalidation,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.actionpostvalidation'
                    },
                    name: 'actionsPostValidation',
                    bind: {
                        value: '{actionPostValidation}'
                    }
                }]
            }]
        }]
    }],

    buttons: [{
        text: CMDBuildUI.locales.Locales.administration.attributes.texts.save,
        localized: {
            text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.save'
        },
        formBind: true,
        disabled: true,
        ui: 'administration-action-small',
        listeners: {
            click: 'onSaveBtnClick'
        }
    }, {
        text: CMDBuildUI.locales.Locales.administration.attributes.texts.saveandadd,
        localized: {
            text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.saveandadd'
        },
        formBind: true,
        disabled: true,
        ui: 'administration-action-small',
        listeners: {
            click: 'onSaveAndAddBtnClick'
        }
    }, {
        text: CMDBuildUI.locales.Locales.administration.attributes.texts.cancel,
        localized: {
            text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.cancel'
        },
        ui: 'administration-secondary-action-small',
        listeners: {
            click: 'onCancelBtnClick'
        }
    }]
});