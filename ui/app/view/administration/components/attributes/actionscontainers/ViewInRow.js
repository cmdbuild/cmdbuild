Ext.define('CMDBuildUI.view.administration.components.attributes.actionscontainers.ViewInRow', {
    extend: 'CMDBuildUI.components.tab.FormPanel',
    alias: 'widget.administration-components-attributes-actionscontainers-viewinrow',

    requires: [
        'CMDBuildUI.view.administration.components.attributes.fieldscontainers.GeneralProperties',
        'CMDBuildUI.view.administration.components.attributes.fieldscontainers.TypeProperties'
    ],
    cls: 'administration',
    ui: 'administration-tabandtools',
    config: {
        objectTypeName: null,
        objectId: null,
        shownInPopup: false,
        theAttribute: null
    },

    items: [{
        title: CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties,
        localized: {
            title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties'
        },
        xtype: "form",
        fieldDefaults: {
            padding: '0 15 0 15'
        },
        items: [{
            xtype: 'administration-components-attributes-fieldscontainers-generalproperties',
            viewModel: {
                data: {
                    action: 'VIEW'
                }
            }
        }]
    }, {
        xtype: "form",
        title: CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties,
        localized: {
            title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties'
        },
        fieldDefaults: {
            padding: '0 15 0 15'
        },
        items: [{
            xtype: 'administration-components-attributes-fieldscontainers-typeproperties',
            viewModel: {
                data: {
                    action: 'VIEW'
                }
            }
        }]
    }],

    tools: [{
        xtype: 'tbfill'
    }, {
        xtype: 'tool',
        itemId: 'editAttributeBtn',
        glyph: 'f040@FontAwesome', // Pencil icon
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.editattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.editattribute'
        },
        callback: 'onEditAttributeBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-attributes-tool-editbtn'
        },
        bind: {
            hidden: '{!canEdit}'
        }
    }, {
        xtype: 'tool',
        itemId: 'openAttributeBtn',
        glyph: 'f08e@FontAwesome', // Open icon
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.openattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.openattribute'
        },
        callback: 'onOpenAttributeBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-attributes-tool-openbtn'
        },
        bind: {
            hidden: '{hideOpenBtn}'
        }
    }, {
        xtype: 'tool',
        itemId: 'deleteAttributeBtn',
        glyph: 'f014@FontAwesome',
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.deleteattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.deleteattribute'
        },
        callback: 'onDeleteAttributeBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-attributes-tool-deletebtn'
        },
        bind: {
            disabled: '{!canDelete}'
        }
    },  /*{
        xtype: 'tool',
        itemId: 'cloneBtn',
        glyph: 'f24d@FontAwesome', // Clone icon
        tooltip: 'Clone card',
        callback: 'onCloneBtnClick',
        cls: 'administration-tool',
        autoEl: {
            'data-testid': 'administration-attributes-card-view-cloneBtn'
        },
        bind: {
            hidden: '{!canClone}'
        }
    },*/{
        xtype: 'tool',
        itemId: 'disableBtn',
        cls: 'administration-tool',
        iconCls: 'x-fa fa-ban',
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.disableattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.disableattribute'
        },
        callback: 'onToggleBtnClick',
        hidden: true,
        autoEl: {
            'data-testid': 'administration-attributes-tool-disablebtn'
        },
        bind: {
            hidden: '{!theAttribute.active}'
        }
    },{
        xtype: 'tool',
        itemId: 'enableBtn',
        hidden: true,
        cls: 'administration-tool',
        iconCls: 'x-fa fa-check-circle-o',
        tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.enableattribute,
        localized: {
            tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.enableattribute'
        },
        callback: 'onToggleBtnClick',
        autoEl: {
            'data-testid': 'administration-properties-tool-enablebtn'
        },
        bind: {
            hidden: '{theAttribute.active}'
        }
    }]
});