Ext.define('CMDBuildUI.view.administration.components.attributes.actionscontainers.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.administration-components-attributes-actionscontainers-edit',

    bubbleEvents: [
        'itemupdated',
        'cancelupdating'
    ],
    modelValidation: true,
    config: {
        objectTypeName: null,
        objectId: null,
        shownInPopup: false,
        theAttribute: null
    },

    fieldDefaults: {
        labelAlign: 'top',
        msgTarget: 'under'
    },
    scrollable: true,
    ui: 'administration-formpagination',
    items: [{
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties, 
        localized: {
            title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.generalproperties'
        },
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.name, 
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.name'
                },
                name: 'name',
                bind: {
                    value: '{theAttribute.name}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'fieldcontainer',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.description, 
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.description'
                },
                layout: 'column',

                items: [{
                    columnWidth: 0.9,
                    xtype: 'textfield',
                    name: 'description',
                    bind: {
                        value: '{theAttribute.description}'
                    }
                }, {
                    xtype: 'button',
                    columnWidth: 0.1,
                    reference: 'translateBtn',
                    iconCls: 'x-fa fa-flag fa-2x',
                    tooltip: CMDBuildUI.locales.Locales.administration.attributes.tooltips.translate,
                    localized: {
                        tooltip: 'CMDBuildUI.locales.Locales.administration.attributes.tooltips.translate'
                    },
                    config: {
                        theValue: null
                    },
                    viewModel: {
                        data: {
                            theValue: null,
                            vmKey: 'theAttribute'
                        }
                    },
                    bind: {
                        theAttribute: '{theAttribute}'
                    },
                    listeners: {
                        click: 'onTranslateClick'
                    },
                    cls: 'administration-field-container-btn',
                    autoEl: {
                        'data-testid': 'administration-card-edit-translateBtn'
                    }
                }]
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'combo',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.group,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.group'
                },
                name: 'group',
                allowBlank: true,
                displayField: 'label',
                valueField: 'value',
                bind: {
                    value: '{theAttribute.group}',
                    store: '{attributeGroupStore}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'combo',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mode,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mode'
                },
                name: 'mode',
                clearFilterOnBlur: false,
                anyMatch: true,
                autoSelect: true,
                forceSelection: true,
                allowBlank: false,
                displayField: 'label',
                valueField: 'value',
                msgTarget: 'qtip',
                bind: {
                    value: '{theAttribute.mode}',
                    store: '{attributeModeStore}'

                },
                listeners: {
                    change: function () {
                        var fields = this.up('form').getForm().getFields().items;
                        fields.forEach(function (field, index) {
                            field.validate();
                        });
                    }
                },
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    switch (value) {
                        case 'apm_write':
                        case 'write':
                            return CMDBuildUI.locales.Locales.administration.attributes.strings.editable;
                        case 'apm_read':
                        case 'read':
                            return CMDBuildUI.locales.Locales.administration.attributes.strings.readonly;
                        case 'apm_hidden':
                        case 'hidden':
                            return CMDBuildUI.locales.Locales.administration.attributes.strings.hidden;
                        case 'apm_immutable':
                        case 'immutable':
                            return CMDBuildUI.locales.Locales.administration.attributes.strings.immutable;
                    }
                },
                /**
                 * Returns whether or not the widget value is currently valid by {@link #getErrors validating} the
                 * {@link #processRawValue processed raw value} of the widget. **Note**: {@link #disabled} buttons are
                 * always treated as valid.
                 *
                 * @return {Boolean} True if the value is valid, else false
                 */
                isValid: function () {
                    return this.validateValue(this.getValue());
                },

                /**
                 * Uses {@link #getErrors} to build an array of validation errors. If any errors are found, they are passed to
                 * {@link #markInvalid} and false is returned, otherwise true is returned.
                 * 
                 * @param {Object} value The value to validate
                 * @return {Boolean} True if all validations passed, false if one or more failed
                 */
                validateValue: function (value) {
                    var errors = this.getErrors(value),
                        isValid = Ext.isEmpty(errors);

                    if (isValid) {
                        this.clearInvalid();
                    } else {
                        this.markInvalid(errors);
                    }
                    return isValid;
                },

                /**
                 * @param {Object} value The value to validate. The processed raw value will be used if nothing is passed.
                 * @return {String[]} Array of any validation errors
                 */
                getErrors: function (value) {
                    var messages = [];

                    // we have to validate only if value is 'hidden' 
                    var form = this.up('form');
                    if (value == 'hidden') {
                        // get theAttribute from viewModel
                        var theAttribute = form.getViewModel().get('theAttribute');
                        // if field showInGrid === true return error
                        if (theAttribute.get('showInGrid')) {
                            messages.push(CMDBuildUI.locales.Locales.administration.attributes.strings.thefieldshowingridcantbechecked); 
                        }
                        // if field showInReducedGrid === true return error
                        if (theAttribute.get('showInReducedGrid')) {
                            messages.push(CMDBuildUI.locales.Locales.administration.attributes.strings.thefieldshowinreducedgridcantbechecked);
                        }
                        // if field mandatory === true return error
                        if (theAttribute.get('mandatory')) {
                            messages.push(CMDBuildUI.locales.Locales.administration.attributes.strings.thefieldmandatorycantbechecked);
                        }
                    }
                    return messages;
                },

                /**
                 * Returns whether or not the widget value is currently valid by {@link #getErrors validating} the field's current
                 * value, and fires the {@link #validitychange} event if the field's validity has changed since the last validation.
                 * **Note**: {@link #disabled} fields are always treated as valid.
                 *
                 * Custom implementations of this method are allowed to have side-effects such as triggering error message display.
                 * To validate without side-effects, use {@link #isValid}.
                 *
                 * @return {Boolean} True if the value is valid, else false
                 */
                validate: function () {
                    return this.checkValidityChange(this.isValid());
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showingrid,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showingrid'
                },
                name: 'showInGrid',
                bind: {
                    value: '{theAttribute.showInGrid}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showinreducedgrid,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showinreducedgrid'
                },
                name: 'showInReducedGrid',
                bind: {
                    value: '{theAttribute.showInReducedGrid}'
                },
                /**
                 * @param {Object} value The value to validate. The processed raw value will be used if nothing is passed.
                 * @return {String[]} Array of any validation errors
                 */
                getErrors: function (value) {
                    var messages = [];
                    var form = this.up('form');
                    // get theAttribute from viewModel
                    var theAttribute = form.getViewModel().get('theAttribute');
                    // we have to validate only if the value is true and the "mode" field is "hidden"
                    if (value == true && theAttribute.get('mode') === 'hidden') {
                        messages.push(CMDBuildUI.locales.Locales.administration.attributes.strings.thefieldmodeishidden);
                    }

                    return messages;
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.unique,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.unique'
                },
                name: 'unique',
                bind: {
                    value: '{theAttribute.unique}'
                }
            }, {
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mandatory,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.mandatory'
                },
                name: 'mandatory',
                bind: {
                    value: '{theAttribute.mandatory}'
                },
                /**
                 * @param {Object} value The value to validate. The processed raw value will be used if nothing is passed.
                 * @return {String[]} Array of any validation errors
                 */
                getErrors: function (value) {
                    var messages = [];
                    var form = this.up('form');
                    // get theAttribute from viewModel
                    var theAttribute = form.getViewModel().get('theAttribute');
                    // we have to validate only if the value is true and the "mode" field is "hidden"
                    if (value == true && theAttribute.get('mode') === 'hidden') {
                        messages.push(CMDBuildUI.locales.Locales.administration.attributes.strings.thefieldmodeishidden);
                    }

                    return messages;
                }
            }]
        }, {
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'checkbox',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.active,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.active'
                },
                name: 'active',
                bind: {
                    value: '{theAttribute.active}'
                }
            }]
        }]
    }, {
        ui: 'administration-formpagination',
        xtype: "fieldset",
        collapsible: true,
        title: CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties,
        localized: {
            title: 'CMDBuildUI.locales.Locales.administration.attributes.titles.typeproperties'
        },
        items: [{
            layout: 'column',
            items: [{
                columnWidth: 0.5,
                xtype: 'displayfield',
                fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.type,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.type'
                },
                name: 'type',
                bind: {
                    value: '{theAttribute.type}'
                },
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    if (value) {
                        return Ext.String.capitalize(value.toLowerCase());
                    }
                }
            }]
        }, {
            // If type is reference
            bind: {
                hidden: '{!types.isReference}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    columnWidth: 0.5,
                    xtype: 'displayfield',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.domain,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.domain'
                    },
                    bind: {
                        value: '{theAttribute.domain}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.filter,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.filter'
                    },
                    name: 'filter',
                    bind: {
                        value: "{theAttribute.filter}"
                    }
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'checkbox',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.preselectifunique,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.preselectifunique'
                    },
                    name: 'preselectIfUnique',
                    bind: {
                        value: '{preselectIfUnique}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'button',
                    text: CMDBuildUI.locales.Locales.administration.attributes.texts.editmetadata, 
                    localized: {
                        text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.editmetadata'
                    },
                    reference: 'viewmetadata',
                    itemId: 'viewmetadata',
                    iconCls: 'x-fa fa-pencil',
                    ui: 'administration-action-small',
                    style: 'margin-top:25px',
                    handler: function () {
                        // TODO: open popup whith custom metadata
                        console.log('open edit metadata'); // TODO: open popup with metadata
                    }
                }]
            }]
        }, {
            // If type is decimal
            bind: {
                hidden: '{!types.isDecimal}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    // if attribute is inherited this value can't be edited
                    columnWidth: 0.5,
                    xtype: 'displayfield',
                    itemId: 'precisionAttributeField',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision'
                    },
                    name: 'precision',
                    step: 1,
                    bind: {
                        value: '{theAttribute.precision}',
                        hidden: '{!theAttribute.inherited}'
                    }
                }, {
                    // if attribute is inherited this value can't be edited
                    columnWidth: 0.5,
                    xtype: 'displayfield',
                    itemId: 'scaleAttributeField',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.scale,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.scale'
                    },
                    name: 'scale',
                    step: 1,
                    bind: {
                        value: "{theAttribute.scale}",
                        hidden: '{!theAttribute.inherited}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'numberfield',
                    itemId: 'precisionAttributeField',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.precision'
                    },
                    name: 'precision',
                    step: 1,
                    bind: {
                        value: '{theAttribute.precision}',
                        hidden: '{theAttribute.inherited}'
                    },
                    validator: function (field) {
                        var form = this.up('form');
                        var precisionAttributeField = form.down('#scaleAttributeField');
                        if (typeof this.getValue !== undefined && this.getValue() <= precisionAttributeField.getValue()) {
                            return CMDBuildUI.locales.Locales.administration.attributes.strings.precisionmustbebiggerthanscale;
                        }
                        return true;
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'numberfield',
                    itemId: 'scaleAttributeField',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.scale,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.scale'
                    },
                    name: 'scale',
                    step: 1,
                    bind: {
                        value: "{theAttribute.scale}",
                        hidden: '{theAttribute.inherited}'
                    },
                    validator: function (field) {
                        var form = this.up('form');
                        var precisionAttributeField = form.down('#precisionAttributeField');
                        if (typeof this.getValue !== undefined && this.getValue() >= precisionAttributeField.getValue()) {
                            return CMDBuildUI.locales.Locales.administration.attributes.strings.scalemustbesmallerthanprecision;
                        }
                        return true;
                    }
                }]
            }]
        }, {
            // If type is lookup
            bind: {
                hidden: '{!types.isLookup}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    columnWidth: 0.5,
                    xtype: 'displayfield',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.lookup,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.lookup'
                    },
                    bind: {
                        value: '{theAttribute.lookupType}'
                    }
                }]
            }]
        }, {
            // If type is string
            bind: {
                hidden: '{!types.isString}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    // if attribute is inherited this value can't be edited
                    columnWidth: 0.5,
                    xtype: 'displayfield',
                    step: 1,
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.maxlength,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.maxlength'
                    },
                    name: 'maxLength',
                    bind: {
                        value: '{theAttribute.maxLength}',
                        hidden: '{!theAttribute.inherited}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'numberfield',
                    step: 1,
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.maxlength,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.maxlength'
                    },
                    name: 'maxLength',
                    bind: {
                        value: '{theAttribute.maxLength}',
                        hidden: '{theAttribute.inherited}'
                    }
                }]
            }]
        }, {
            // If type is text
            bind: {
                hidden: '{!types.isText}'
            },
            hidden: true,
            items: [{
                layout: 'column',

                items: [{
                    columnWidth: 0.5,
                    xtype: 'combo',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.editortype,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.editortype'
                    },
                    name: 'editorType',
                    clearFilterOnBlur: false,
                    anyMatch: true,
                    autoSelect: true,
                    forceSelection: true,
                    displayField: 'label',
                    valueField: 'value',
                    bind: {
                        value: '{theAttribute.editorType}',
                        store: '{editorTypeStore}'
                    },
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        if (value) {
                            switch (value) {
                                case 'HTML':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.editorhtml;
                                case 'PLAIN':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.plaintext;
                            }
                        }
                    }
                }]
            }]
        }, {
            // If type is ip address
            bind: {
                hidden: '{!types.isIpAddress}'
            },
            hidden: true,
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'combo',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.iptype,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.iptype'
                    },
                    displayField: 'label',
                    valueField: 'value',
                    name: 'ipType',
                    bind: {
                        value: '{theAttribute.ipType}',
                        store: '{ipTypeStore}'
                    },
                    renderer: function (value) {
                        if (value) {
                            switch (value) {
                                case 'ipv4':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.ipv4; 
                                case 'ipv6':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.ipv6;
                                case 'any':
                                    return CMDBuildUI.locales.Locales.administration.attributes.strings.any;
                            }
                        }
                    }
                }]
            }]
        }, {
            hidden: true,
            bind: {
                hidden: '{!theAttribute}'
            },
            items: [{
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.help,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.help'
                    },
                    name: 'cm_help',
                    bind: {
                        value: '{help}'
                    }
                }, {
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showif,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.showif'
                    },
                    name: 'cm_showIf',
                    bind: {
                        value: '{showIf}'
                    }
                }]
            }, {
                layout: 'column',
                items: [{
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.validationrules,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.validationrules'
                    },
                    name: 'cm_validationRules',
                    bind: {
                        value: '{validationRules}'
                    }
                }, {
                    hidden: true, // TODO: activate on 3.x milestone
                    columnWidth: 0.5,
                    xtype: 'textarea',
                    fieldLabel: CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.actionpostvalidation,
                    localized: {
                        fieldLabel: 'CMDBuildUI.locales.Locales.administration.attributes.fieldlabels.actionpostvalidation'
                    },
                    name: 'cm_actionPostValidation',
                    bind: {
                        value: '{actionPostValidation}'
                    }
                }]
            }]
        }]
    }],

    buttons: [{
        text: CMDBuildUI.locales.Locales.administration.attributes.texts.save,
        localized: {
            text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.save'
        },
        ui: 'administration-action-small',
        formBind: true, //only enabled once the form is valid
        disabled: true,
        listeners: {
            click: 'onSaveBtnClick'
        }
    }, {
        text: CMDBuildUI.locales.Locales.administration.attributes.texts.cancel,
        localized: {
            text: 'CMDBuildUI.locales.Locales.administration.attributes.texts.cancel'
        },
        ui: 'administration-secondary-action-small',
        listeners: {
            click: 'onCancelBtnClick'
        }
    }]
});