
Ext.define('CMDBuildUI.view.patches.Panel', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.patches.PanelController',
        'CMDBuildUI.view.patches.PanelModel'
    ],

    alias: 'widget.patches-panel',
    controller: 'patches-panel',
    viewModel: {
        type: 'patches-panel'
    },

    forceFit: true,
    title: 'Patches',
    sortableColumns: false,
    enableColumnHide: false,

    columns: [{
        text: 'Name',
        dataIndex: 'name'
    }, {
        text: 'Description',
        dataIndex: 'description'
    }, {
        text: 'Category',
        dataIndex: 'category'
    }],

    bind: {
        store: '{patches}'
    },

    buttons: [{
        text: 'Apply patches',
        itemId: 'btnApply',
        ui: 'management-action',
        autoEl: {
            'data-testid': 'patcches-btnapply'
        }
    }]
});
