Ext.define('CMDBuildUI.view.reports.ContainerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.reports-container',
    data: {
        name: 'CMDBuildUI'
    },

    formulas: {
        title: function (get) {
            var title = '';
            var report = Ext.getStore('Reports').findRecord('code', this.getView().objectTypeId);
            title += report.get('_description_translation') ? report.get('_description_translation') : report.get('description');
            return title;
        }
    }
});
