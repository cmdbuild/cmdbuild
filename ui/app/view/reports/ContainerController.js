Ext.define('CMDBuildUI.view.reports.ContainerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reports-container',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    /************************************************************************************************************** 
     *                                                                  
     *                                                  REPORTS
     *
     * EVENTS:
     *  onBeforeRender              (view, eOpts)                           --> render view with selected object
     * 
     * METHODS:
     *  getReportAttributes         (me, ext, report, title, reportId)      --> gets the attributes for the requested report                 
     *  addRelationAttibutes        (attributes, ext, title, reportId)      --> add attributes for the current report
     *  addItems                    (attributes, extension)                 --> return formfields from attributes as array of viewitems   
     *  fileOpener                  (extension, url, view)                  --> opens the file in the requested format
     *  createUrl                   (urlParts)                              --> Util: creates and returns the url to call
     *  isEmptyCustom               (object)                                --> Util: returns true if js Object is empty
     *                                                                  
     * ************************************************************************************************************/

    /**
    * @param {CMDBuildUI.view.reports.ContainerController} view
    * @param {Object} eOpts
    */
    onBeforeRender: function (view, eOpts) {
        var me = this;
        var ext = view.extension;
        var report = Ext.getStore('Reports').findRecord('code', me.getView().objectTypeId);
        var title = report.get('_description_translation') ? report.get('_description_translation') : report.get('description');
        var reportId = report.id;
        this.getReportAttributes(me, ext, report, title, reportId);
    },

    /**
    * Get the attributes for report 
    * @param {CMDBuildUI.view.reports.ContainerController} view
    * @param {string} ext
    * @param {Object} eOpts
    */

    getReportAttributes: function (me, ext, report, title, reportId) {
        if (report) {
            var attributes = report.attributes();
            if (attributes.isLoaded()) {
                me.addRelationAttibutes(attributes.getRange(), ext, title, reportId);
            } else {
                attributes.getProxy().setUrl(report.getProxy().getUrl() + report.getId() + "/attributes");
                attributes.load({
                    callback: function (records, operation, success) {
                        me.addRelationAttibutes(records, ext, title, reportId);
                    }
                });
            }
        }
    },

    /**
    * add attributes for the current report
    * @param {Object} attributes
    * @param {string} ext
    * @param {string} title
    * @param {numeric} reportId
    */

    addRelationAttibutes: function (attributes, ext, title, reportId) {
        var me = this;
        var parameters = {};
        var urlParts = [];
        var extension = ext;
        var objTypeId = reportId;
        var url = '';

        urlParts.push(CMDBuildUI.util.Config.baseUrl + '/reports/' + objTypeId + '/file/');

        /**
         * ext is 0 when need a format.
         * defined in app/mixins/routes/management.js
         */

        if (extension !== 0) {
            urlParts.push('?extension=' + extension);
        }

        var items = me.addItems(attributes, extension);
        var config = {
            width: '50%',
            height: '75%'
        }

        var form = Ext.create("Ext.form.Panel", {
            fieldDefaults: {
                labelAlign: 'top',
                margin: '0 15'
            },
            scrollable: true,
            items: items,
            buttons: [
                {
                    xtype: 'button',
                    ui: 'management-action',
                    text: 'Print',
                    reference: 'printbutton',
                    itemId: 'printbutton',
                    formBind: true,
                    disabled: true,
                    handler: function (view, eOpts) {
                        var encodedParams = '';
                        var fielditems = form.getForm().getFields().items;
                        if (!Ext.isEmpty(attributes) || fielditems.length > 0) {
                            fielditems.forEach(function (fielditem) {

                                if (fielditem.getId() === 'reportFileFormat') {
                                    extension = fielditem.getValue();
                                    urlParts.push('?extension=' + extension);
                                } else {
                                    if (!fielditem.isFieldContainer) {
                                        var value = fielditem.getValue() ? fielditem.getValue() : "";
                                        parameters[fielditem.getName()] = value;
                                    }
                                }
                            });
                            encodedParams = Ext.util.Format.uri(Ext.encode(parameters));

                            if (!me.isEmptyCustom(parameters)) {
                                urlParts.push('&parameters=' + encodedParams);
                            }
                        }
                        url = me.createUrl(urlParts);
                        me.fileOpener(extension, url);
                        popup.close();
                    }
                },
                {
                    xtype: 'button',
                    ui: 'secondary-action',
                    text: 'Cancel',
                    reference: 'cancelbutton',
                    itemId: 'cancelutton',
                    listeners: {
                        click: function (view) {
                            popup.close();
                        }
                    }
                }
            ]
        });

        if (extension !== 0 && Ext.isEmpty(attributes)) {
            url = me.createUrl(urlParts);
            me.fileOpener(extension, url);
        } else {
            var popup = CMDBuildUI.util.Utilities.openPopup(null, title, form, {}, config);
        }
    },


    /**
    * return field from attributes as view items
    * @param {Object} attributes
    * @param {string} extension
    */

    addItems: function (attributes, extension) {
        var items = [];
        if (extension === 0) {
            var customitem = {
                anchor: '100%',
                xtype: 'combobox',
                margin: '0 15 0 15',
                fieldLabel: 'File Format *',
                name: 'reportFileFormat',
                id: 'reportFileFormat',
                store: Ext.create('Ext.data.Store', {
                    fields: ['value', 'label'],
                    data: [
                        { "value": "pdf", "label": "PDF" },
                        { "value": "rtf", "label": "RTF" },
                        { "value": "odt", "label": "ODT" },
                        { "value": "csv", "label": "CSV" },
                    ]
                }),
                queryMode: 'local',
                displayField: 'label',
                valueField: 'value',
                allowBlank: false
            }

            items.push(customitem);
        }

        if (!Ext.isEmpty(attributes)) {
            attributes.forEach(function (attribute) {
                var field = CMDBuildUI.util.helper.ModelHelper.getModelFieldFromAttribute(attribute);
                field.name = attribute.get('name'); //al field.name vengono tolti gli spazi, se l'attribute li aveva la query da errore, quindi mi riprendo il nome
                var item = CMDBuildUI.util.helper.FormHelper.getFormField(field, {
                    mode: field.mode,
                    linkName: null,
                    overrides: {
                        mandatory: field.mandatory
                    }
                });
                items.push(item);
            });
        }

        return items;
    },

    /**
    * opens the file in the requested format
    * @param {string} extension
    * @param {string} url
    * @param {CMDBuildUI.view.reports.ContainerController} view
    */

    fileOpener: function (extension, url, view) {
        var container = view ? view : this.getView();
        var html = '';
        switch (extension) {
            case 'pdf':
                html = '<object style="width:100%;height:100%;" data="' + url + '" type="application/pdf"><embed src="' + url + '" type="application/pdf" /></object>';
                container.setHtml(html);
                break;
            case 'csv':

                Ext.Ajax.request({
                    url: url,
                    dataType: "text",
                    callback: function (opts, success, response) {
                        var allTextLines = response.responseText;
                        var str = allTextLines.replace(/(?:\r\n|\r|\n)/g, '<br>');
                        var html = '<div style="width:100%;height:100%; padding: 15px 15px; font-weight: 400;" type="text/csv">' + str + '</div>';
                        container.setHtml(html);
                        container.setScrollable(true);
                    }

                });
                break;
            default:
                window.open(url);
        }
    },

    /**
    * Util: creates and returns the url to call
    * @param {array} urlParts
    */

    createUrl: function (urlParts) {
        var url = '';
        urlParts.forEach(function (urlpart) {
            url += urlpart;
        });
        return url;
    },


    /**
    * Util: returns true if js Object is empty
    * @param {object} object
    */

    isEmptyCustom: function (object) {
        for (var element in object) {
            if (object.hasOwnProperty(element)) {
                return false;
            }
        }
        return true;
    },
});
