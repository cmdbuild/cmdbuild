
Ext.define('CMDBuildUI.view.reports.Container', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.reports.ContainerController',
        'CMDBuildUI.view.reports.ContainerModel'
    ],

    alias: 'widget.reports-container',
    controller: 'reports-container',
    viewModel: {
        type: 'reports-container'
    },
    bind: {
        title: '{title}'
    }      
});
