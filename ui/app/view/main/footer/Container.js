
Ext.define('CMDBuildUI.view.main.footer.Container',{
    extend: 'Ext.container.Container',

    requires: [
        'CMDBuildUI.view.main.footer.ContainerController',
        'CMDBuildUI.view.main.footer.ContainerModel'
    ],

    xtype: 'main-footer-container',
    controller: 'main-footer-container',
    viewModel: {
        type: 'main-footer-container'
    },

    dock: 'bottom',
    padding: 10,

    // add data-testid attribute to element
    autoEl: {
        'data-testid' : 'main-footer-container'
    },

    style: {
        textAlign: 'center'
    },

    html: '&copy; CMDBuild 3.0.0-alpha'
});
