/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('CMDBuildUI.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    mixins: {
        managementroutes: 'CMDBuildUI.mixins.routes.Management',
        adminroutes: 'CMDBuildUI.mixins.routes.Administration'
    },

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        }
    },

    routes: {
        '': {
            action: 'showManagement',
            before: 'onBeforeShowManagement'
        },
        'patches': {
            action: 'showPatches'
        },
        'login': {
            action: 'showLogin',
            before: 'onBeforeShowLogin'
        },
        'logout': {
            action: 'doLogout'
        },
        'management': {
            action: 'showManagement',
            before: 'onBeforeShowManagement'
        },
        'administration': {
            action: 'showAdministration',
            before: 'onBeforeShowAdministration'
        },
        'administration/classes': {
            action: 'showClassAdministrationAdd'
        },
        'administration/classes_empty': {
            action: 'showClassAdministration_empty'
        },
        'administration/classes/:className': {
            action: 'showClassAdministrationView'
        },
        'administration/classes/:className/attribute/:attributeName': {
            action: 'showClassAttributeAdministrationView'
        },
        'administration/classes/:className/attribute/:attributeName/edit': {
            action: 'showClassAttributeAdministrationEdit'
        },
        'administration/lookup_types': {
            action: 'showLookupTypeAdministrationAdd'
        },
        'administration/lookup_types_empty': {
            action: 'showLookupTypeAdministration_empty'
        },
        'administration/lookup_types/:lookupName': {
            action: 'showLookupTypeAdministrationView'
        },
        'administration/domains': {
            action: 'showDomainAdministrationCreate'
        },
        'administration/domains_empty': {
            action: 'showDomainAdministration_empty'
        },
        'administration/domains/:domain': {
            action: 'showDomainAdministrationView'
        },
        'administration/menus': {
            action: 'showMenuAdministrationAdd'
        },
        'administration/menus/:menu': {
            action: 'showMenuAdministrationView'
        },
        'administration/menus_empty': {
            action: 'showMenuAdministration_empty'
        },
        'administration/processes': {
            action: 'showProcessesAdministrationAdd'
        },
        'administration/processes/:process': {
            action: 'showProcessAdministrationView'
        },
        'administration/reports_empty/:showForm': {
            action: 'showReportAdministration_empty'
        },
        'administration/reports_empty': {
            action: 'showReportAdministration_empty'
        },
        'administration/reports/:reportId': {
            action: 'showReportAdministrationView'
        },
        'administration/groupsandpermissions_empty': {
            action: 'showGroupsandpermissionsAdministration_empty'
        },
        'administration/groupsandpermissions/:roleId': {
            action: 'showGroupsandpermissionsAdministrationView'
        },
        'administration/users': {
            action: 'showUsersAdministrationView'
        },
        'administration/users_empty': {
            action: 'showUsersAdministrationView_empty'
        },
        'administration/processes_empty': {
            action: 'showProcessAdministration_empty'
        },
        'administration/setup/:setupPage': {
            action: 'showSetupAdministrationView'
        },
        'classes/:className/cards': {
            action: 'showCardsGrid',
            before: 'onBeforeShowCardsGrid'
        },
        'classes/:className/cards/:idCard': {
            action: 'showCard',
            before: 'onBeforeShowCard'
        },
        'classes/:className/cards/new': {
            action: 'showCardCreate',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/view': {
            action: 'showCardView',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/clone': {
            action: 'showCardClone',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/edit': {
            action: 'showCardEdit',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/details': {
            action: 'showCardDetails',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/notes': {
            action: 'showCardNotes',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/relations': {
            action: 'showCardRelations',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/history': {
            action: 'showCardHistory',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/emails': {
            action: 'showCardEmails',
            before: 'onBeforeShowCardWindow'
        },
        'classes/:className/cards/:idCard/attachments': {
            action: 'showCardAttachments',
            before: 'onBeforeShowCardWindow'
        },
        'processes/:processName/instances': {
            action: 'showProcessInstancesGrid',
            before: 'onBeforeShowProcessInstancesGrid'
        },
        'processes/:processName/instances/new': {
            action: 'showProcessInstanceCreate',
            before: 'onBeforeShowProcessInstanceCreate'
        },
        'processes/:processName/instances/:idInstance': {
            action: 'showProcessInstanceView',
            before: 'onBeforeShowProcessInstanceView'
        },
        'processes/:processName/instances/:idInstance/activities/:activityId': {
            action: 'showProcessInstanceView',
            before: 'onBeforeShowProcessInstanceView'
        },
        'processes/:processName/instances/:idInstance/activities/:activityId/edit': {
            action: 'showProcessInstanceEdit',
            before: 'onBeforeShowProcessInstanceEdit'
        },
        'custompages/:pageName': {
            action: 'showCustomPage',
            before: 'onBeforeShowCustomPage'
        },
        'reports/:idReport': {
            action: 'showReport',
            before: 'onBeforeShowReport'
        },
        'reports/:idReport/:extension': {
            action: 'showReportExtension',
            before: 'onBeforeShowReportExtension'
        },
        'views/:viewName/items': {
            action: 'showView',
            before: 'onBeforeShowView'
        }
    },

    onBeforeRender: function() {
        var me = this;

        CMDBuildUI.util.helper.SessionHelper.updateStartingUrlWithCurrentUrl();
        me.redirectTo('login', true);

        // // redirect to login if tocken is empty
        // function redirectToLogin() {
        //     if (Ext.isEmpty(Ext.History.getToken())) {
        //         me.redirectTo('login', true);
        //     }
        // }

        // // check boot status
        // Ext.Ajax.request({
        //     url: CMDBuildUI.util.Config.baseUrl + CMDBuildUI.util.api.Common.getBootStatusUrl(),
        //     method: 'GET'
        // }).then(function (response, opts) {
        //     if (response.status === 200 && response.responseText) {
        //         var jsonresponse = Ext.JSON.decode(response.responseText);
        //         if (jsonresponse.status === 'WAITING_FOR_USER') {
        //             me.getViewModel().set("patchesNeeded", true);
        //         }
        //     }
        //     redirectToLogin();
        // }, function () {
        //     redirectToLogin();
        // });
    }
});
