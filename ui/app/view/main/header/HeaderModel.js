Ext.define('CMDBuildUI.view.main.header.HeaderModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.main-header-header',

    formulas: {
        companylogo: function (get) {
            // TODO: replace with ws data
            return Ext.getResourcePath('images/company-logo.png', 'shared');
        },
        isAuthenticated: {
            bind: '{theSession.username}',
            get: function (username) {
                if (username) {
                    return true;
                }
                return false;
            }
        },
        isAdministrator: {
            bind: {
                provileges: '{theSession.rolePrivileges}',
                isAdministrationModule: '{isAdministrationModule}'
            },
            get: function (data) {
                return data.provileges.admin_access && !data.isAdministrationModule;
            }
        }
    }

});
