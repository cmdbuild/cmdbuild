Ext.define('CMDBuildUI.view.login.FormPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login-formpanel',

    control: {
        'textfield': {
            specialkey: 'onSpecialKey'
        },
        '#loginbtn': {
            click: 'onLoginBtnClick'
        },
        '#logoutbtn': {
            click: 'onLogoutBtnClick'
        }
    },

    /**
     * @param {Ext.form.field.Text} textfield
     * @param {Event} e The click event
     */
    onSpecialKey: function (textfield, e) {
        if (e.getKey() == e.ENTER) {
            this.onLoginBtnClick(this.lookupReference('loginbtn'));
        }
    },
    /**
     * @param {Ext.button.Button} btn
     * @param {Event} e The click event
     */
    onLoginBtnClick: function (btn, e) {
        var me = this;
        var vm = this.getViewModel();
        var form = this.getView();

        if (form.isValid()) {
            btn.mask();
            vm.get("theSession").set("password", vm.get("password"));
            // set action id
            CMDBuildUI.util.Ajax.setActionId('login');
            // save session
            vm.get("theSession").save({
                failure: function (record, operation) {
                    if (record.getId() === CMDBuildUI.model.users.Session.temporary_id) {
                        // TODO: use cmdbuild error message Error message
                        var w = Ext.create('Ext.window.Toast', {
                            html: 'Wrong credentials',
                            title: 'Error',
                            iconCls: 'x-fa fa-times-circle',
                            align: 'br'
                        });
                        w.show();
                    }
                },
                success: function (record, operation) {
                    CMDBuildUI.util.helper.SessionHelper.setToken(record.getId());

                    // add binding fo tenants field
                    form.lookupReference('activeTenantsField').setBind({
                        value: '{theSession.activeTenants}'
                    });

                    if (record.get("role") && (!record.get('availableTenants') || record.get('activeTenants'))) {
                        // show logged in messages
                        var w = Ext.create('Ext.window.Toast', {
                            html: 'Welcome back ' + record.get('userDescription'),
                            title: 'Logged in',
                            iconCls: 'x-fa fa-check-circle',
                            align: 'br'
                        });
                        w.show();

                        // redirect to management
                        me.redirectTo('management');
                        return;
                    }

                    vm.set('loggedIn', true);
                },
                callback: function (record, operation, success) {
                    btn.unmask();
                    // hide error message
                    vm.set('showErrorMessage', false);
                }
            });
        } else {
            vm.set('showErrorMessage', true);
        }
    },

    /**
     * @param {Ext.button.Button} btn
     * @param {Event} e The click event
     */
    onLogoutBtnClick: function (btn, e) {
        var vm = this.getViewModel();
        // set action id
        CMDBuildUI.util.Ajax.setActionId('logout');
        // erase session
        vm.getData().theSession.erase({
            success: function (record, operation) {
                // blank session token
                CMDBuildUI.util.helper.SessionHelper.setToken(null);
                CMDBuildUI.util.Utilities.redirectTo("login", true);
            }
        });
    }
});
