
Ext.define('CMDBuildUI.view.processes.instances.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.processes.instances.GridController',
        'CMDBuildUI.view.processes.instances.GridModel',

        // plugins
        'Ext.grid.filters.Filters',
        'CMDBuildUI.components.grid.plugin.FormInRowWidget'
    ],

    alias: 'widget.processes-instances-grid',
    controller: 'processes-instances-grid',
    viewModel: {
        type: 'processes-instances-grid'
    },

    title: CMDBuildUI.locales.Locales.processes.workflow,

    bind: {
        title: '{title}',
        store: '{instances}',
        selection: '{selected}'
    },

    plugins: [
        'gridfilters', {
            ptype: 'forminrowwidget',
            expandOnDblClick: true,
            widget: {
                xtype: 'processes-instances-instance-view',
                viewModel: {} // do not remove otherwise the viewmodel will not be initialized
            }
        }
    ],

    config: {
        allowFilter: true,
        showAddButton: true
    },

    forceFit: true,
    loadMask: true,

    selModel: {
        pruneRemoved: false // See https://docs.sencha.com/extjs/6.2.0/classic/Ext.selection.Model.html#cfg-pruneRemoved
    },

    tbar: [{
        xtype: 'button',
        text: CMDBuildUI.locales.Locales.processes.startworkflow,
        iconCls: 'x-fa fa-play',
        reference: 'addbtn',
        itemId: 'addbtn',
        ui: 'management-action',
        disabled: true,
        hidden: true,
        bind: {
            text: '{addbtn.text}',
            disabled: '{addbtn.disabled}',
            hidden: '{addbtn.hidden}'
        },
        autoEl: {
            'data-testid': 'processes-instances-grid-addbtn'
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.processes.startworkflow'
        }
    }, {
        xtype: 'textfield',
        name: 'search',
        width: 250,
        emptyText: 'Search...',
        reference: 'searchtext',
        itemId: 'searchtext',
        cls: 'management-input',
        bind: {
            value: '{search.value}',
            hidden: '{!allowfilter}'
        },
        listeners: {
            specialkey: 'onSearchSpecialKey'
        },
        triggers: {
            search: {
                cls: Ext.baseCSSPrefix + 'form-search-trigger',
                handler: 'onSearchSubmit'
            },
            clear: {
                cls: Ext.baseCSSPrefix + 'form-clear-trigger',
                handler: 'onSearchClear'
            }
        }
    }, {
        xtype: 'tbfill'
    }, {
        xtype: 'bufferedgridcounter',
        padding: '0 20 0 0',
        bind: {
            store: '{instances}'
        }
    }]
});
