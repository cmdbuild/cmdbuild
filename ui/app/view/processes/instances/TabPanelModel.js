Ext.define('CMDBuildUI.view.processes.instances.TabPanelModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.processes-instances-tabpanel',

    data: {
        objectType: CMDBuildUI.util.helper.ModelHelper.objecttypes.process,
        objectTypeName: null,
        objectId: null,
        activityId: null,
        activeTab: 0,
        storeinfo: {
            autoload: false
        },
        disabled: {
            activity: false,
            attachments: true,
            email: true,
            history: true,
            notes: true,
            relations: true
        },
        basepermissions: {
            delete: false,
            edit: false
        }

    },

    formulas: {
        //update permissions to able/disable icons
        updatePermissions: {
            bind: {
                action: '{action}'
            },
            get: function (data) {
                if (
                    data.action !== CMDBuildUI.view.processes.instances.TabPanel.actions.create &&
                    data.action !== CMDBuildUI.view.processes.instances.TabPanel.actions.clone
                ) {
                    this.set("disabled.attachments", false);
                    this.set("disabled.email", false);
                    this.set("disabled.history", false);
                    this.set("disabled.notes", false);
                    this.set("disabled.relations", false);
                }
            }
        }
    }
});
