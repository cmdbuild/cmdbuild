Ext.define('CMDBuildUI.view.processes.instances.GridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.processes-instances-grid',

    listen: {
        global: {
            processinstanceupdated: 'onProcessInstanceUpdated'
        }
    },

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            rowdblclick: 'onRowDblClick'
        },
        '#addbtn': {
            beforerender: 'onAddBtnBeforeRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.Grid} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view) {
        var vm = this.getViewModel();
        var objectTypeName = vm.get("objectTypeName");
        CMDBuildUI.util.helper.ModelHelper.getModel(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.process,
            objectTypeName
        ).then(function (model) {
            // get columns definition
            var columns = CMDBuildUI.util.helper.GridHelper.getColumns(model.getFields(), {
                allowFilter: view.getAllowFilter()
            });

            // get Code to change its renderer to show Activity description
            var column = Ext.Array.findBy(columns, function (c, i) {
                return c.dataIndex === "Code";
            });
            column.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get("_activity_description");
            };

            // reconfigure columns
            view.reconfigure(null, columns);
        });
    },

    /**
     * @param {Ext.selection.RowModel} element
     * @param {CMDBuildUI.model.classes.Card[]} record
     * @param {HTMLElement} rowIndex
     * @param {Event} e
     * @param {Object} eOpts
     */

    onRowDblClick: function (element, record, rowIndex, e, eOpts) {

        var url = Ext.String.format(
            "processes/{0}/instances/{1}/activities/{2}/edit",
            record.getRecordType(),
            record.getRecordId(),
            record.get("_activity_id")
        );

        this.redirectTo(url, true);
        return false;
    },

    /**
     * Filter grid items.
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchSubmit: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // get value
        var searchTerm = vm.get("search").value;
        if (searchTerm) {
            CMDBuildUI.util.Ajax.setActionId("proc.inst.search");
            // add filter
            var filterCollection = vm.get("instances").getFilters();
            filterCollection.add({
                property: CMDBuildUI.proxy.BaseProxy.filter.query,
                value: searchTerm
            });
        } else {
            CMDBuildUI.util.Ajax.setActionId("proc.inst.clearsearch");
            this.onSearchClear(field);
        }
    },

    /**
     * @param {Ext.form.field.Text} field
     * @param {Ext.form.trigger.Trigger} trigger
     * @param {Object} eOpts
     */
    onSearchClear: function (field, trigger, eOpts) {
        var vm = this.getViewModel();
        // clear store filter
        var filterCollection = vm.get("instances").getFilters();
        filterCollection.removeByKey(CMDBuildUI.proxy.BaseProxy.filter.query);
        // reset input
        field.reset();
    },

    /**
    * @param {Ext.form.field.Base} field
    * @param {Ext.event.Event} event
    */
    onSearchSpecialKey: function (field, event) {
        if (event.getKey() == event.ENTER) {
            this.onSearchSubmit(field);
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Object} eOpts
     */
    onAddBtnBeforeRender: function (button, eOpts) {
        var objectTypeName = this.getViewModel().get("objectTypeName");
        var process = CMDBuildUI.util.helper.ModelHelper.getProcessFromName(objectTypeName);

        if (process && process.get("prototype")) {
            var menu = [];

            process.getChildren(true).forEach(function (child) {
                menu.push({
                    text: child.getTranslatedDescription(),
                    iconCls: CMDBuildUI.model.menu.MenuItem.icons.process,
                    listeners: {
                        click: 'onAddBtnClick'
                    },
                    objectTypeName: child.get("name")
                });
            });

            // sort menu by description
            Ext.Array.sort(menu, function (a, b) {
                return a.text === b.text ? 0 : (a.text < b.text ? -1 : 1);
            });

            // add menu to button
            button.setMenu(menu);
        } else {
            button.objectTypeName = objectTypeName;
            button.addListener("click", this.onAddBtnClick, this);
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Object} eOpts
     */
    onAddBtnClick: function (button, eOpts) {
        CMDBuildUI.util.Ajax.setActionId("proc.inst.add");
        var url = 'processes/' + button.objectTypeName + '/instances/new';
        this.redirectTo(url, true);
    },

    /**
     * @param {CMDBuildUI.model.processes.Instance} instance
     * @param {Boolean} forcereload Default `false`
     */
    onProcessInstanceUpdated: function (instance, forcereload) {
        var store = this.getView().getStore();
        var vm = this.getViewModel();
        var view = this.getView();
        if (!store.getById(instance.getId()) || forcereload) {
            store.load({
                callback: function () {
                    vm.set("selected", instance);
                }
            });
        }
    }

});
