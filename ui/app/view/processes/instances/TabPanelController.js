Ext.define('CMDBuildUI.view.processes.instances.TabPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.processes-instances-tabpanel',

    control: {
        '#': {
            beforerender: "onBeforeRender"
        }
    },

    /**
     * @param {CMDBuildUI.view.processes.instances.TabPanel} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        // set view model variables
        var vm = this.getViewModel();
        var action = vm.get("action");
        var privileges = CMDBuildUI.util.helper.SessionHelper.getCurrentSession().get("rolePrivileges");

        // update url on window close
        var detailsWindow = Ext.getCmp('CMDBuildManagementDetailsWindow');
        view.mon(detailsWindow, 'beforeclose', this.closingManagedEvent, this);

        // init tabs
        var configAttachments = CMDBuildUI.util.helper.Configurations.get("cm_system_dms_enabled");
        var activity, tabactivity, tabmasterdetail, tabnotes, tabrelations, tabhistory, tabemails, tabattachments;

        if (action === CMDBuildUI.view.processes.instances.TabPanel.actions.edit) {
            activity = {
                xtype: 'processes-instances-instance-edit',
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.edit
            };
        } else if (action === CMDBuildUI.view.processes.instances.TabPanel.actions.create) {
            activity = {
                xtype: 'processes-instances-instance-create',
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.create
            };
        } else {
            activity = {
                xtype: 'processes-instances-instance-view',
                shownInPopup: true,
                autoScroll: true,
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.view
            };
        }

        // Activity tab
        tabactivity = view.add({
            xtype: "panel",
            iconCls: 'x-fa fa-file-text',
            items: [activity],
            reference: "activity",
            layout: 'fit',
            autoScroll: true,
            padding: 0,
            tabAction: activity.tabAction,
            tabConfig: {
                tabIndex: 0,
                title: null,
                tooltip: CMDBuildUI.locales.Locales.common.tabs.activity
            },
            bind: {
                disabled: '{disabled.activity}'
            }
        });

        // Notes tab
        if (privileges.flow_tab_note_access) {
            tabnotes = view.add({
                xtype: 'notes-panel',
                iconCls: 'x-fa fa-sticky-note',
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.notes,
                tabConfig: {
                    tabIndex: 1,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.notes
                },
                bind: {
                    disabled: '{disabled.notes}'
                }
            });
        }

        // Relations tab
        if (privileges.flow_tab_relation_access) {
            tabrelations = view.add({
                xtype: 'relations-list-container',
                iconCls: 'x-fa fa-link',
                reference: 'relations',
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.relations,
                tabConfig: {
                    tabIndex: 2,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.relations
                },
                autoScroll: true,
                bind: {
                    disabled: '{disabled.relations}'
                }
            });
        }

        // History tab
        if (privileges.flow_tab_history_access) {
            tabhistory = view.add({
                xtype: "panel",
                iconCls: 'x-fa fa-history',
                items: [{
                    xtype: 'history-grid',
                    viewModel: { // TODO: modify when className in view model become name
                        data: {
                            targetType: 'process',
                            targetTypeName: vm.get("objectTypeName"),
                            targetId: vm.get("objectId")
                        }
                    },
                    autoScroll: true
                }],
                reference: 'history',
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.history,
                tabConfig: {
                    tabIndex: 3,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.history
                },
                layout: 'fit',
                autoScroll: true,
                padding: 0,
                bind: {
                    disabled: '{disabled.history}'
                }
            });
        }

        // Email tab
        if (privileges.card_tab_email_access) {
            tabemails = view.add({
                iconCls: 'x-fa fa-envelope',
                disabled: true,
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.emails,
                tabConfig: {
                    tabIndex: 4,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.emails
                }
            });
        }

        // Attachments tab
        if (configAttachments && privileges.flow_tab_attachment_access) {
            tabattachments = view.add({
                xtype: "attachments-container",
                iconCls: 'x-fa fa-paperclip',
                reference: 'attachments',
                autoScroll: true,
                tabAction: CMDBuildUI.view.processes.instances.TabPanel.actions.attachments,
                tabConfig: {
                    tabIndex: 5,
                    title: null,
                    tooltip: CMDBuildUI.locales.Locales.common.tabs.attachments
                },
                padding: 0,
                viewModel: {
                    data: {
                        targetType: 'process',
                        targetTypeName: vm.get("objectTypeName"),
                        targetId: vm.get("objectId")
                    }
                },
                bind: {
                    disabled: '{disabled.attachments}'
                }
            });
        }

        // set view active tab
        var activetab;
        switch (action) {
            case CMDBuildUI.view.processes.instances.TabPanel.actions.notes:
                activetab = tabnotes;
                break;
            case CMDBuildUI.view.processes.instances.TabPanel.actions.relations:
                activetab = tabrelations;
                break;
            case CMDBuildUI.view.processes.instances.TabPanel.actions.history:
                activetab = tabhistory;
                break;
            case CMDBuildUI.view.processes.instances.TabPanel.actions.emails:
                activetab = tabemails;
                break;
            case CMDBuildUI.view.processes.instances.TabPanel.actions.attachments:
                activetab = tabattachments;
                break;
        }
        if (!activetab) {
            activetab = tabactivity;
        }
        view.setActiveTab(activetab);
    },

    closingManagedEvent: function () {
        var me = this;
        // CMDBuildUI.util.Navigation.updateCurrentManagementContextAction(undefined);
        // CMDBuildUI.util.Utilities.redirectTo(me.getBasePath(), true);
    },
});
