Ext.define('CMDBuildUI.view.processes.instances.GridModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.processes-instances-grid',

    data: {
        objectType: CMDBuildUI.util.helper.ModelHelper.objecttypes.process,
        objectTypeName: null,
        search: {
            value: null
        },
        selected: null,
        addbtn: {
            disabled: true,
            hidden: true
        }
    },

    formulas: {

        /**
         * Updata view model data
         */
        updateData: {
            bind: {
                typename: '{objectTypeName}'
            },
            get: function(data) {
                // update translations
                if (data.typename) {
                    var process = CMDBuildUI.util.helper.ModelHelper.getProcessFromName(data.typename);

                    // set model name
                    this.set("storedata.modelname", CMDBuildUI.util.helper.ModelHelper.getModelName(
                        CMDBuildUI.util.helper.ModelHelper.objecttypes.process, 
                        data.typename
                        ));

                    this.set("title", CMDBuildUI.locales.Locales.processes.workflow + ' ' + process.getTranslatedDescription());

                    // set proxy url
                    this.set("storedata.proxyurl", CMDBuildUI.util.api.Processes.getAllInstancesActivitiesUrl(data.typename));

                    // add button
                    this.set("addbtn.text", CMDBuildUI.locales.Locales.processes.startworkflow + ' ' + process.getTranslatedDescription());
                    this.set("addbtn.disabled", !process.get(CMDBuildUI.model.base.Base.permissions.add));
                    this.set("addbtn.hidden", !this.getView().getShowAddButton());

                    // filters
                    this.set("allowfilter", this.getView().getAllowFilter());
                }
            }
        }
    }, 

    stores: {
        instances: {
            type: 'processes-instances',
            model: '{storedata.modelname}',
            proxy: {
                type: 'baseproxy',
                url : '{storedata.proxyurl}'
            },
            autoLoad: true
        }
    }

});
