Ext.define('CMDBuildUI.view.processes.instances.instance.Mixin', {
    mixinId: 'processinstance-mixin',

    showForm: Ext.emptyFn,

    config: {
        buttons: [{
            reference: 'saveBtn',
            itemId: 'saveBtn',
            text: CMDBuildUI.locales.Locales.common.actions.save,
            ui: 'management-action-small',
            bind: {
                hidden: '{!showSaveButton}'
            },
            autoEl: {
                'data-testid': 'processinstance-save'
            },
            localized: {
                text: 'CMDBuildUI.locales.Locales.common.actions.save'
            }
        }, {
            reference: 'executeBtn',
            itemId: 'executeBtn',
            text: CMDBuildUI.locales.Locales.common.actions.execute,
            ui: 'management-action-small',
            formBind: true, //only enabled once the form is valid
            disabled: true,
            autoEl: {
                'data-testid': 'processinstance-execute'
            },
            localized: {
                text: 'CMDBuildUI.locales.Locales.common.actions.execute'
            }
        }, {
            reference: 'cancelBtn',
            itemId: 'cancelBtn',
            ui: 'secondary-action-small',
            text: CMDBuildUI.locales.Locales.common.actions.cancel,
            autoEl: {
                'data-testid': 'processinstance-cancel'
            },
            localized: {
                text: 'CMDBuildUI.locales.Locales.common.actions.cancel'
            }
        }]
    },

    /**
     * Load activity data and display form
     */
    loadActivity: function () {
        var vm = this.getViewModel();

        // get model and set proxy url
        var activityModel = Ext.ClassManager.get("CMDBuildUI.model.processes.Activity");
        activityModel.getProxy().setUrl(
            CMDBuildUI.util.api.Processes.getInstanceActivitiesUrl(
                vm.get("objectTypeName"),
                vm.get("objectId")
            )
        );

        // load activity and save variables in ViewModel
        activityModel.load(vm.get("activityId"), {
            success: function (record, operation) {
                vm.set("theActivity", record);

                // get the process definition
                vm.set("theProcess", CMDBuildUI.util.helper.ModelHelper.getProcessFromName(vm.get("objectTypeName")));

                // render form
                this.showForm();
            },
            scope: this
        });
    },

    /**
     * 
     * @return {Object} 
     */
    getProcessStatusBar: function () {
        var config;
        var vm = this.getViewModel();
        var model = vm.get("objectModel");

        // get flow status attribute configuration
        var flowStatusAttr = vm.get("theProcess").get("flowStatusAttr");
        if (flowStatusAttr) {
            var attr = model.getField(flowStatusAttr);
            config = {
                type: attr.metadata.lookupType,
                field: flowStatusAttr
            };
        } else {
            // return default values
            config = {
                type: 'FlowStatus',
                field: '_status'
            };
        }
        return {
            xtype: 'statuses-progress-bar',
            lookuptype: config.type,
            bind: {
                lookupvalue: '{theObject.' + config.field + '}'
            }
        };
    },

    /**
     * Get attributes configuration from activity
     * @return {Object} containing visibleAttributes and overrides
     */
    getAttributesConfigFromActivity: function () {
        var visibleAttributes = [], overrides = {};
        var vm = this.getViewModel();

        var messageAttr = this.getMessageAttribute();
        // iterate on activity attributes
        Ext.Array.each(vm.get("theActivity").attributes().getRange(), function (attr, index) {
            if (attr.getId() !== messageAttr) {
                // TODO: check permissions
                visibleAttributes.push(attr.getId());
                overrides[attr.getId()] = attr.getData();
            }
        });
        return {
            visibleAttributes: visibleAttributes,
            overrides: overrides
        };
    },

    /**
     * Get action field
     * 
     * @return {Ext.form.field.ComboBox}
     */
    getActionField: function () {
        var vm = this.getViewModel();
        var bindvalue;

        var model = vm.get("objectModel");

        // iterate on activity attributes
        var action = Ext.Array.findBy(
            vm.get("theActivity").attributes().getRange(),
            function (attr, index) {
                return attr.get("action");
            });

        // create store and add it to viewmodel
        var store = {
            model: 'CMDBuildUI.model.lookups.Lookup'
        };
        if (action) {
            vm.set("activity_action.fieldname", action.getId());

            // get action attribute lookup definition
            var attr = model.getField(vm.get("activity_action.fieldname"));
            Ext.apply(store, {
                proxy: {
                    url: CMDBuildUI.util.api.Lookups.getLookupValues(attr.metadata.lookupType),
                    type: 'baseproxy'
                },
                autoLoad: true
            });
            bindvalue = '{theObject.' + vm.get("activity_action.fieldname") + '}';
        } else {
            // set defalt value
            vm.set("activity_action.value", 'advance_action');

            // create store with only one item
            Ext.apply(store, {
                data: {
                    data: [{
                        _id: vm.get("activity_action.value"),
                        code: 'advance',
                        description: CMDBuildUI.locales.Locales.processes.action.advance,
                        number: 1
                    }]
                },
                proxy: {
                    type: 'memory',
                    reader: {
                        type: 'json',
                        rootProperty: 'data'
                    }
                }
            });
            bindvalue = '{activity_action.value}';
        }
        // add store to viewmodel
        var stores = vm.storeInfo || {};
        stores._activity_action_store = store;
        vm.setStores(stores);

        // create combobox
        return {
            xtype: 'combobox',
            name: '_action',
            fieldLabel: CMDBuildUI.locales.Locales.processes.action.label + ' *',
            labelAlign: 'left',
            valueField: '_id',
            displayField: 'text',
            forceSelection: true,
            editable: false,
            allowBlank: false,
            width: "50%",
            cls: Ext.baseCSSPrefix + 'process-action-field',
            bind: {
                readOnly: '{_activity_action_store.data.length <= 1}',
                value: bindvalue,
                store: '{_activity_action_store}'
            }
        };
    },

    /**
     * 
     * @param {Object[]} items 
     * @return {Object}
     */
    getMainPanelForm: function (items) {
        return {
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'  //stretch vertically to parent
            },
            items: [{
                items: items,
                flex: 1
            }, {
                xtype: 'widgets-launchers',
                formMode: this.formmode,
                bind: {
                    widgets: '{theActivity.widgets}'
                }
            }]
        };
    },

    /**
     * Get message box
     * 
     * @return {Object} Message box configuration
     */
    getMessageBox: function () {
        var vm = this.getViewModel();
        var conf;
        var model = vm.get("objectModel");
        var messageAttr = this.getMessageAttribute();
        if (messageAttr) {
            var hasmessage = vm.get("theActivity").attributes().find("_id", messageAttr) !== -1;
            if (hasmessage) {
                var field = model.getField(messageAttr);
                if (
                    field.cmdbuildtype.toLowerCase() === CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase() ||
                    field.cmdbuildtype.toLowerCase() === CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase()
                ) {
                    messageAttr = Ext.String.format("_{0}_description", messageAttr);
                }
                return {
                    xtype: 'container',
                    ui: 'messageinfo',
                    messageAttr: field.name,
                    bind: {
                        html: Ext.String.format("{theObject.{0}}", messageAttr)
                    }
                };
            }
        }
        return conf;
    },

    /**
     * @param {Object} callback
     * @param {Function} callback.success
     * @param {Function} callback.failure
     * @param {Function} callback.callback
     */
    executeProcess: function (callback) {
        var vm = this.getViewModel();

        if (this.isValid()) {
            var theObject = vm.get("theObject");
            // set advance attribute
            theObject.set("_advance", true);
            theObject.set("_activity", vm.get("activityId"));
            // set type if empty
            if (!theObject.get("_type")) {
                theObject.set("_type", vm.get("objectTypeName"));
            }

            CMDBuildUI.util.Ajax.setActionId("proc.inst.execute");
            this.loadMask = CMDBuildUI.util.Utilities.addLoadMask(this);

            // save and advance
            theObject.save(callback);
        } else {
            var w = Ext.create('Ext.window.Toast', {
                html: 'Error!',
                title: 'Please correct indicted errors',
                iconCls: 'x-fa fa-check-circle',
                align: 'br'
            });
            w.show();
        }
    },

    /**
     * @param {Object} callback
     * @param {Function} callback.success
     * @param {Function} callback.failure
     * @param {Function} callback.callback
     */
    saveProcess: function (callback) {
        var vm = this.getViewModel();

        var theObject = vm.get("theObject");
        // set advance attribute
        theObject.set("_advance", false);
        theObject.set("_activity", vm.get("activityId"));
        // set type if empty
        if (!theObject.get("_type")) {
            theObject.set("_type", vm.get("objectTypeName"));
        }

        CMDBuildUI.util.Ajax.setActionId("proc.inst.save");
        this.loadMask = CMDBuildUI.util.Utilities.addLoadMask(this);

        // save and advance
        theObject.save(callback);
    },

    /**
     * Close detail window
     */
    closeDetailWindow: function() {
        CMDBuildUI.util.Navigation.removeManagementDetailsWindow();
    },

    privates: {
        /**
         * Return message attribute name
         */
        getMessageAttribute: function () {
            return this.getViewModel().get("theProcess").get("messageAttr");
        }
    }
});