
Ext.define('CMDBuildUI.view.processes.instances.instance.Edit', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.processes.instances.instance.EditController',
        'CMDBuildUI.view.processes.instances.instance.EditModel'
    ],

    mixins: [
        'CMDBuildUI.view.processes.instances.instance.Mixin'
    ],

    alias: 'widget.processes-instances-instance-edit',
    controller: 'processes-instances-instance-edit',
    viewModel: {
        type: 'processes-instances-instance-edit'
    },

    modelValidation: true,
    autoScroll: true,

    fieldDefaults: CMDBuildUI.util.helper.FormHelper.fieldDefaults,
    formmode: CMDBuildUI.util.helper.FormHelper.formmodes.update,

    /**
     * Render form fields
     * 
     * @param {CMDBuildUI.model.processes.Instance} model
     */
    showForm: function () {
        var vm = this.getViewModel();

        // attributes configuration from activity
        var attrsConf = this.getAttributesConfigFromActivity();

        // message panel
        var message_panel = this.getMessageBox();

        // action combobox
        var action_field = this.getActionField();

        if (vm.get("activity_action.fieldname")) {
            Ext.Array.remove(attrsConf.visibleAttributes, vm.get("activity_action.fieldname"));
        }

        // get form fields as fieldsets
        var formitems = CMDBuildUI.util.helper.FormHelper.renderForm(vm.get("objectModel"), {
            mode: this.formmode,
            showAsFieldsets: true,
            attributesOverrides: attrsConf.overrides,
            visibleAttributes: attrsConf.visibleAttributes
        });

        // add action_field as first element in form items
        Ext.Array.insert(formitems, 0, [message_panel, action_field]);
        // create items
        var items = [
            this.getProcessStatusBar(), 
            this.getMainPanelForm(formitems)
        ];

        this.add(items);
    }
});
