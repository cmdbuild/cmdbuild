
Ext.define('CMDBuildUI.view.processes.instances.instance.View', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.processes.instances.instance.ViewController',
        'CMDBuildUI.view.processes.instances.instance.ViewModel'
    ],

    mixins: [
        'CMDBuildUI.view.processes.instances.instance.Mixin'
    ],

    alias: 'widget.processes-instances-instance-view',
    controller: 'processes-instances-instance-view',
    viewModel: {
        type: 'processes-instances-instance-view'
    },

    config: {
        buttons: null,
        objectTypeName: null,
        objectId: null,
        activityId: null,
        shownInPopup: false
    },

    fieldDefaults: CMDBuildUI.util.helper.FormHelper.fieldDefaults,
    formmode: CMDBuildUI.util.helper.FormHelper.formmodes.read,

    bind: {
        title: '{title}'
    },


    tabpaneltools: [{
        xtype: 'tool',
        itemId: 'editBtn',
        glyph: 'f040@FontAwesome', // Pencil icon
        tooltip: 'Edit card',
        callback: 'onEditBtnClick',
        cls: 'management-tool',
        disabled: true,
        autoEl: {
            'data-testid': 'cards-card-view-editBtn'
        },
        bind: {
            disabled: '{!basepermissions.edit}'
        }
    }, {
        xtype: 'tool',
        itemId: 'openBtn',
        glyph: 'f08e@FontAwesome', // Open icon
        tooltip: 'Open card',
        callback: 'onOpenBtnClick',
        cls: 'management-tool',
        autoEl: {
            'data-testid': 'cards-card-view-openBtn'
        },
        bind: {
            hidden: '{hiddenbtns.open}'
        }
    }, {
        xtype: 'tool',
        itemId: 'deleteBtn',
        glyph: 'f014@FontAwesome', // Trash icon
        tooltip: 'Delete card',
        callback: 'onDeleteBtnClick',
        cls: 'management-tool',
        disabled: true,
        autoEl: {
            'data-testid': 'cards-card-view-deleteBtn'
        },
        bind: {
            disabled: '{!basepermissions.delete}'
        }
    }],

    /**
     * Render form fields
     */
    showForm: function () {
        var vm = this.getViewModel();

        // attributes configuration from activity
        var attrsConf = this.getAttributesConfigFromActivity();

        // generate tabs/fieldsets and fields
        var items = [];
        if (this.getShownInPopup()) {
            // get form fields as fieldsets
            var formitems = CMDBuildUI.util.helper.FormHelper.renderForm(vm.get("objectModel"), {
                mode: this.formmode,
                attributesOverrides: attrsConf.overrides,
                visibleAttributes: attrsConf.visibleAttributes
            });

            // create items
            items = [
                this.getProcessStatusBar(),
                {
                    xtype: 'toolbar',
                    cls: 'fieldset-toolbar',
                    items: Ext.Array.merge([{ xtype: 'tbfill' }], this.tabpaneltools)
                },
                this.getMainPanelForm(formitems)
            ];
        } else {
            // get form fields as tab panel
            var panel = CMDBuildUI.util.helper.FormHelper.renderForm(vm.get("objectModel"), {
                mode: this.formmode,
                showAsFieldsets: false,
                attributesOverrides: attrsConf.overrides,
                visibleAttributes: attrsConf.visibleAttributes
            });
            Ext.apply(panel, {
                tools: this.tabpaneltools
            });
            items.push(panel);
        }
        this.add(items);

        if (this.loadmask) {
            CMDBuildUI.util.Utilities.removeLoadMask(this.loadmask);
        }
    }
});
