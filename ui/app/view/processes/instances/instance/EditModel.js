Ext.define('CMDBuildUI.view.processes.instances.instance.EditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.processes-instances-instance-edit',

    data: {
        activity_action: {
            fieldname: null,
            value: null
        }
    },

    formulas: {
        showSaveButton: {
            bind: '{theProcess}',
            get: function (theProcess) {
                return theProcess && theProcess.get("enableSaveButton");
            }
        },
        popupTitle: {
            bind: {
                process: '{theProcess.description}',
                instance: '{theObject.Description}',
                activity: '{theActivity.description}'
            },
            get: function (data) {
                CMDBuildUI.util.Navigation.updateTitleOfManagementDetailsWindow(
                    Ext.String.format(
                        "Edit workflow {0} &mdash; {1} &mdash; {2}", 
                        data.process, 
                        data.instance, 
                        data.activity
                    )
                );
            }
        }
    }

});
