Ext.define('CMDBuildUI.view.processes.instances.instance.ViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.processes-instances-instance-view',
    
    data: {
        hiddenbtns: {
            open: true
        }
    },

    formulas: {
        title: function (get) {
            return null; // return null to hide header
        },
        popupTitle: {
            bind: {
                process: '{theProcess.description}',
                instance: '{theObject.Description}',
                activity: '{theActivity.description}'
            },
            get: function (data) {
                if (this.getView().getShownInPopup()) {
                    CMDBuildUI.util.Navigation.updateTitleOfManagementDetailsWindow(
                        Ext.String.format(
                            "Workflow {0} &mdash; {1} &mdash; {2}", 
                            data.process, 
                            data.instance, 
                            data.activity
                        )
                    );
                }
            }
        },
        updatePermissions: {
            bind: {
                activity: '{theActivity}'
            },
            get: function(data) {
                if (data.activity && data.activity.get("writable")) {
                    this.getParent().set("basepermissions.delete", true);
                    this.getParent().set("basepermissions.edit", true);
                }
                this.set("hiddenbtns.open", this.getView().getShownInPopup());
            }
        }
    }

});
