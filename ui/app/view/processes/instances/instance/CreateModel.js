Ext.define('CMDBuildUI.view.processes.instances.instance.CreateModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.processes-instances-instance-create',
    
    data: {
        activity_action: {
            fieldname: null,
            value: null
        }
    },

    formulas: {
        showSaveButton: {
            bind: '{theProcess}',
            get: function (theProcess) {
                return theProcess && theProcess.get("enableSaveButton");
            }
        },
        popupTitle: {
            bind: {
                process: '{theProcess.description}',
                activity: '{theActivity.description}'
            },
            get: function (data) {
                CMDBuildUI.util.Navigation.updateTitleOfManagementDetailsWindow(
                    Ext.String.format(
                        "Start workflow {0} &mdash; {1}", 
                        data.process, 
                        data.activity
                    )
                );
            }
        }
    }

});
