Ext.define('CMDBuildUI.view.processes.instances.instance.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.processes-instances-instance-view',

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            afterrender: 'onAfterRender'
        }
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.card.View} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();

        // read instance parameters from row configuration
        // when view is rendered inside grid row
        if (!view.getShownInPopup()) {
            var config = view.getInitialConfig();
            if (!Ext.isEmpty(config._rowContext)) {
                // get widget record
                var record = config._rowContext.record;
                if (record && record.getData()) {
                    // set view model variable
                    vm.set("objectId", record.getId());
                    vm.set("objectTypeName", record.get("_type"));
                    vm.set("activityId", record.get("_activity_id"));
                }
            }
        }

        // get instance model
        CMDBuildUI.util.helper.ModelHelper.getModel(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.process,
            vm.get("objectTypeName")
        ).then(function (model) {
            vm.set("objectModel", model);

            // load process instance
            vm.linkTo("theObject", {
                type: model.getName(),
                id: vm.get("objectId")
            });

            // load activity
            view.loadActivity();
        }, function () {
            Ext.Msg.alert('Error', 'Process non found!');
        });
    },

    /**
     * @param {CMDBuildUI.view.classes.cards.card.View} view
     * @param {Object} eOpts
     */
    onAfterRender: function (view, eOpts) {
        view.loadmask = null;
        if (!view.getShownInPopup()) {
            view.setMinHeight(100);
            view.loadmask = CMDBuildUI.util.Utilities.addLoadMask(view);
        }
    },

    onEditBtnClick: function () {
        CMDBuildUI.util.Ajax.setActionId("proc.inst.edit");
        var vm = this.getViewModel();
        var url = Ext.String.format(
            "processes/{0}/instances/{1}/activities/{2}/edit",
            vm.get("objectTypeName"),
            vm.get("objectId"),
            vm.get("activityId")
        );
        this.redirectTo(url, true);
    },

    onDeleteBtnClick: function () {
        CMDBuildUI.util.Ajax.setActionId("proc.inst.delete");
        Ext.Msg.alert('Warning', 'Action "Delete process instance" not implemented!');
    },

    onOpenBtnClick: function () {
        CMDBuildUI.util.Ajax.setActionId("proc.inst.view");
        var vm = this.getViewModel();
        var url = Ext.String.format(
            "processes/{0}/instances/{1}/activities/{2}",
            vm.get("objectTypeName"),
            vm.get("objectId"),
            vm.get("activityId")
        );
        this.redirectTo(url, true);
    }

});
