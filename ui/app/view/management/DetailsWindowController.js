Ext.define('CMDBuildUI.view.management.DetailsWindowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.management-detailswindow',

    control: {
        '#': {
            boxready: 'onBoxReady'
        }
    },

    /**
     * @param {CMDBuildUI.view.management.DetailsWindow} window
     * @param {Number} width
     * @param {Number} height
     * @param {Object} eOpts
     */
    onBoxReady: function (window, width, height, eOpts) {
        this.getView().anchorTo(Ext.getBody(), 'br-br', { x: -30, y: 0 });
        var token = Ext.util.History.getToken();
        history.pushState(null,null,'#'+token);
    },

    onClosed: function (recordType,eOpts) {
        this.destroyDetailsWindow();
        this.fireEvent('close');
    },
    onClose: function (recordType,eOpts) {
        // var vm = this.getView().items.items[0].getViewModel();
        // var recordType = vm.get('className') ||  vm.get('objectTypeName');
        // history.replaceState({}, null, '#classes/' + recordType + '/cards');
    },

    /**
     * @private
     */
    destroyDetailsWindow: function () {
        var view = this.getView()
        if (view) {
            view.close();
        }
    }
});
