(function () {
    var elementId = 'CMDBuildManagementDetailsWindow';

    Ext.define('CMDBuildUI.view.management.DetailsWindow', {
        extend: 'Ext.window.Window',
        statics: {
            elementId: elementId
        },

        requires: [
            'CMDBuildUI.view.management.DetailsWindowController',
            'CMDBuildUI.view.management.DetailsWindowModel'
        ],

        controller: 'management-detailswindow',
        viewModel: {
            type: 'management-detailswindow'
        },

        bind: {
            title: '{actionDescription} {typeDescription} {itemDescription}'
        },

        id: elementId,
        autoEl: {
            'data-testid': 'cards-card-detailsWindow'
        },
        ui: 'management',
        layout: 'fit',
        autoShow: true,
        resizable: true,
        draggable: true,
        minimizable: true,
        maximizable: true,
        monitorResize: true,
        defaultAlign: 'br-br',
        
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },

        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        listeners: {
            // bubbled events are not listened if declared 
            // in control properti within ViewControlle
            closed: 'onClosed',
            close: 'onClose',
            minimize: function(win,obj) {
                win.minimize( )
                }
        },
        initComponent: function () {       
            var viewportSize = Ext.getBody().getViewSize();
            Ext.apply(this, {
                height: viewportSize.height * 0.95,
                width: viewportSize.width * 0.75,
                tools: [

                ]
            });

            this.callParent(arguments);
        }
    });
})();