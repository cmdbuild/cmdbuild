
Ext.define('CMDBuildUI.view.views.items.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.views.items.GridController',
        'CMDBuildUI.view.views.items.GridModel',

        // plugins
        'Ext.grid.filters.Filters',
        'CMDBuildUI.components.grid.plugin.FormInRowWidget',
        'CMDBuildUI.view.views.items.View',
        'CMDBuildUI.util.helper.SessionHelper'
    ],

    alias: 'widget.views-items-grid',
    controller: 'views-items-grid',
    viewModel: {
        type: 'views-items-grid'
    },

    config: {
        /**
         * @cfg {String} [objectTypeName]
         */
        objectTypeName: null,

        allowFilter: true,

        /**
         * @cfg {Numeric} [selectedId]
         * Selected card id.
         */
        selectedId: null
    },

    publish: [
        'objectTypeName',
        'selectedId'
    ],

    bind: {
        title: '{title}',
        objectTypeName: '{objectTypeName}',
        store: '{items}',
        selectedId: '{selectedId}',
        selection: '{selection}'
    },

    forceFit: true,
    loadMask: true,

    selModel: {
        pruneRemoved: false // See https://docs.sencha.com/extjs/6.2.0/classic/Ext.selection.Model.html#cfg-pruneRemoved
    },

    plugins: [
        'gridfilters', {
            pluginId: 'forminrowwidget',
            ptype: 'forminrowwidget',
            id: 'forminrowwidget',
            widget: {
                xtype: 'views-items-view',
                bind: {}, // do not remove otherwise the view will break
                viewModel: {} // do not remove otherwise the viewmodel will not be initialized
            }
        }
    ],
    autoEl: {
        'data-testid': 'views-items-grid'
    },
});
