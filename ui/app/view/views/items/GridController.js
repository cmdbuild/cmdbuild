Ext.define('CMDBuildUI.view.views.items.GridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.views-items-grid',

    mixins: [
        'CMDBuildUI.mixins.grids.GridControllerMixin'
    ],

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            }
        },

    /**
     * @param {CMDBuildUI.view.views.items.Grid} view
     */
    onBeforeRender: function (view) {
        var vm = this.getViewModel();
        CMDBuildUI.util.helper.ModelHelper.getModel(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.view,
            vm.get("objectTypeName")
        ).then(function (model) {
            // get columns definition
            var columns = CMDBuildUI.util.helper.GridHelper.getColumns(model.getFields(), {
                allowFilter: view.getAllowFilter()
            });
            columns.forEach(function (column) {
                column.hidden = false;
                column.text = column.dataIndex;
            });
            view.reconfigure(null, columns);
        });
    },
});
