
Ext.define('CMDBuildUI.view.relations.list.add.GridContainer', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.relations.list.add.GridContainerController',
        'CMDBuildUI.view.relations.list.add.GridContainerModel'
    ],

    alias: 'widget.relations-list-add-gridcontainer',
    controller: 'relations-list-add-gridcontainer',
    viewModel: {
        type: 'relations-list-add-gridcontainer'
    },

    layout: 'fit',
    config: {
        originTypeName: null,
        originId: null,
        mode :null // create|edit
    },

    fbar: [{
        text: 'Save',
        disabled: true,
        reference: 'savebutton',
        itemId: 'savebutton',
        ui: 'management-action',
        bind: {
            disabled: '{!relselection.length}'
        }
    }, {
        text: 'Cancel',
        reference: 'cancelbutton',
        ui: 'secondary-action',
        itemId: 'cancelbutton'
    }]
});
