Ext.define('CMDBuildUI.view.relations.list.add.GridContainerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.relations-list-add-gridcontainer',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#cardgrid': {
            selectionchange: 'onGridSelectionChange',
        },
        'tableview': {
            selectioncheckrendered: 'onSelectionCheckRendered'
        },
        '#savebutton': {
            click: 'onSaveButtonClick'
        },
        '#cancelbutton': {
            click: 'onCancelButtonClick'
        }
    },

    onBeforeRender: function (view, e, c) {
        var mode = view.getViewModel().get('selection') ? "SINGLE" : "MULTI";
        view.add({
            xtype: 'classes-cards-grid-grid',
            reference: 'cardgrid',
            itemId: 'cardgrid',
            selModel: {
                type: 'cmdbuildcheckboxmodel',
                pruneRemoved: false,
                checkOnly: true,
                mode: mode
            },
            plugins: null
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveButtonClick: function (button, e, eOpts) {
        var me = this;
        var selection = me.getReferences().cardgrid.getSelection();
        var view = me.getView();
        var store = Ext.create(Ext.data.Store, {
            model: 'CMDBuildUI.model.domains.Relation',
            proxy: {
                type: 'baseproxy',

                //TODO: when the server support this URL 
                //url: Ext.String.format('/classes/{0}/cards/{1}/relations', view.getOriginTypeName(), view.getOriginId())
                url: Ext.String.format('/domains/{0}/relations', view.getViewModel().get("theDomain").get("name"))
            }
        });

        for (var i = 0; i < selection.length; i++) {
            store.add({
                _type: view.getViewModel().get("theDomain").get("name"),
                _destinationId: selection[i].get("_id"),
                _destinationType: selection[i].get("_type"),
                _sourceType: view.getOriginTypeName(),
                _sourceId: view.getOriginId()
            });
        }

        store.sync({
            success: function (batch, options) {
                Ext.ComponentQuery.query('relations-list-grid')[0].getStore().reload();
                view.fireEvent("popupclose");
            }
        });


    },

    /**
     * @param {String|Numeric} value
     * @param {Object} cell
     * @param {Ext.data.Model} record
     * @param {Numeric} rowIndex
     * @param {Numeric} colIndex
     * @param {Ext.data.Store} store
     * @param {Ext.grid.column.Check} check
     * @param {Ext.view.Table}
     */
    onSelectionCheckRendered: function (value, cell, record, rowIndex, colIndex, store, check, tableview) {
        var domainName = this.getViewModel().get("theDomain").get("name");

        // open the first tree where the class is present
        if (tableview.grid.getViewModel().get('cardgrid').selection && tableview.grid.getViewModel().get('cardgrid').selection.get('_destinationId')) {

            if (record && record.getId() == tableview.grid.getViewModel().get('cardgrid').selection.get('_destinationId')) {
                //need async for layout 
                setTimeout(function () {
                    tableview.grid.getSelectionModel().select(rowIndex, true);
                }, 0);
            }
            if (!record.get("_" + domainName + "_available")) {
                check.disable();
            } else {
                check.enable();
            }
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCancelButtonClick: function (button, e, eOpts) {
        this.getView().fireEvent("popupclose");
    },
    /**
     * @param {Ext.selection.RowModel} selection
     * @param {CMDBuildUI.model.classes.Card[]} selected
     * @param {Object} eOpts
     */
    onGridSelectionChange: function(selection, selected, eOpts) {
        this.getViewModel().set("relselection", selected);
    }

});
