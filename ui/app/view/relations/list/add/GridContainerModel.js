Ext.define('CMDBuildUI.view.relations.list.add.GridContainerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.relations-list-add-gridcontainer',

    data: {
        objectType: 'class',
        objectTypeName: null,
        relationDirection: null,
        relselection: []
    },

    formulas: {
        objectTypeDescription: {
            bind: {
                objectType: '{objectType}',
                objectTypeName: '{objectTypeName}'
            },
            get: function(data) {
                if (data.objectType === CMDBuildUI.util.helper.ModelHelper.objecttypes.klass) {
                    return CMDBuildUI.util.helper.ModelHelper.getClassDescription(data.objectTypeName);
                } else if (data.objectType === CMDBuildUI.util.helper.ModelHelper.objecttypes.process) {
                    return CMDBuildUI.util.helper.ModelHelper.getProcessDescription(data.objectTypeName);
                }
            }
        },
        storeModelName: {
            bind: {
                objectType: '{objectType}',
                objectTypeName: '{objectTypeName}'
            },
            get: function(data) {
                if (data.objectTypeName) {
                    return CMDBuildUI.util.helper.ModelHelper.getModelName(data.objectType, data.objectTypeName);
                }
            }
        },
        storeProxy: {
            bind: {
                theDomain: '{theDomain}',
                modelName: '{storeModelName}',
                direction: '{relationDirection}'
            },
            get: function(data) {
                var proxy = Ext.clone(Ext.ClassManager.get(data.modelName).getProxy());
                proxy.setExtraParam("forDomain_name", data.theDomain.get("name"));
                proxy.setExtraParam("forDomain_direction", data.direction);
                proxy.setExtraParam("forDomain_originId", this.getView().getOriginId());
                return proxy;
            }
        }
    }, 

    stores: {
        cards: {
            type: 'classes-cards',
            model: '{storeModelName}',
            autoLoad: true,
            proxy: '{storeProxy}'
        }
    }

});
