Ext.define('CMDBuildUI.view.relations.masterdetail.TabPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.relations-masterdetail-tabpanel',

    control: {
        '#': {
            added: 'onAdded',
            beforerender: 'onBeforeRender'
        }
    },

    /**
     * 
     * @param {CMDBuildUI.view.relations.masterdetail.TabPanel} view 
     * @param {Ext.container.Container} container 
     * @param {Number} position 
     * @param {Object} eOpts 
     */
    onAdded: function(view, container, position, eOpts) {
        var vm = this.getViewModel();
        var item = CMDBuildUI.util.helper.ModelHelper.getObjectFromName(vm.get("objectTypeName"), vm.get("objectType"));
        var mddomains = [];
        item.getDomains().then(function (domains) {
            domains.each(function (d) {
                if (d.get("isMasterDetail") &&
                    (d.get("cardinality") === "1:N" && Ext.Array.contains(item.getHierarchy(), d.get("source"))) ||
                    (d.get("cardinality") === "N:1" && Ext.Array.contains(item.getHierarchy(), d.get("destination")))
                ) {
                    mddomains.push(d);
                }
            });
            vm.set("mddomains", mddomains);
        });
    },

    /**
     * @param {CMDBuildUI.view.relations.masterdetail.TabPanel} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var vm = this.getViewModel();

        vm.bind({
            bindTo: '{mddomains.length}'
        }, function(length) {
            vm.get("mddomains").forEach(function(d) {
                var targetTypeName, isProcess;
                if (d.get("cardinality") === '1:N') {
                    targetTypeName = d.get("destination");
                    isProcess = d.get("destinationProcess");
                } else if (d.get("cardinality") === 'N:1') {
                    targetTypeName = d.get("source");
                    isProcess = d.get("soucreProcess");
                }
                // add tab panel
                view.add({
                    xtype: 'relations-masterdetail-tab',
                    viewModel: {
                        data: {
                            domain: d,
                            targetTypeName: targetTypeName
                        }
                    },
                    isProcess: isProcess
                });
            });
            view.setActiveTab(0);
        });
    }

});
