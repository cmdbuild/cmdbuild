
Ext.define('CMDBuildUI.view.relations.masterdetail.Tab', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.relations.masterdetail.TabController',
        'CMDBuildUI.view.relations.masterdetail.TabModel'
    ],

    alias: 'widget.relations-masterdetail-tab',
    controller: 'relations-masterdetail-tab',
    viewModel: {
        type: 'relations-masterdetail-tab'
    },

    config: {
        /**
         * @cfg {String} targetTypeName
         */
        targetTypeName: null,

        /**
         * @cfg {CMDBuildUI.model.domains.Domain} domain
         */
        domain: null,

        /**
         * @cfg {Boolean} isProcess
         */
        isProcess: null
    },

    /**
     * @cfg {CMDBuildUI.model.classes.Class|CMDBuildUI.model.processes.Process} targetTypeName
     */
    targetTypeObject: null,

    /**
     * @event targetdataupdated
     * Fires when target data has been updated.
     * @param {CMDBuildUI.view.relations.masterdetail.Tab} view
     * @param {CMDBuildUI.model.classes.Class|CMDBuildUI.model.processes.Process} targetTypeObject
     * @param {CMDBuildUI.model.classes.Card} targetTypeModel
     * @param {CMDBuildUI.model.domains.Domain} domain
     * @param {Object} eOpts
     */

    publishes: [
        'targetTypeName',
        'domain'
    ],

    layout: 'fit',

    bind: {
        title: '{mddescription} <br /> <small>({cards.totalCount} {mditems})</small>',
        targetTypeName: '{targetTypeName}',
        domain: '{domain}'
    },

    tabConfig: {
        width: 150,
        textAlign: 'left'
    },

    tbar: [{
        xtype: 'button',
        text: CMDBuildUI.locales.Locales.relations.adddetail,
        reference: 'adddetailbtn',
        itemId: 'adddetailbtn',
        iconCls: 'x-fa fa-plus',
        ui: 'management-action-small',
        bind: {
            text: '{addbutton.text}',
            disabled: '{addbutton.disabled}'
        },
        autoEl: {
            'data-testid': 'relations-masterdetail-tab-addbtn'
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.relations.adddetail'
        }
    }],

    /**
     * This method is called when targetTypeName is changed.
     * @param {String} newValue
     * @param {String} oldValue
     */
    updateTargetTypeName: function (newValue, oldValue) {
        this.targetTypeObject = CMDBuildUI.util.helper.ModelHelper.getClassFromName(newValue);
    },

    /**
     * @return {CMDBuildUI.model.classes.Class|CMDBuildUI.model.processes.Process}
     */
    getTargetTypeObject: function () {
        return this.targetTypeObject;
    }

});
