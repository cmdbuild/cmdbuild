Ext.define('CMDBuildUI.view.relations.masterdetail.TabModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.relations-masterdetail-tab',

    data: {
        domain: null,
        targetTypeName: null,
        searchtext: null,
        storeInfo: {
            autoLoad: false
        },
        addbutton: {
            text: null,
            disabled: true
        }
    },

    formulas: {
        /**
         * Updates store configuration depending on type name and domain.
         */
        updateStoreInfo: {
            bind: {
                targettypename: '{targetTypeName}',
                domain: '{domain}'
            },
            get: function (data) {
                if (data.targettypename && data.domain) {
                    var me = this;

                    // update tab title
                    this.set("mddescription", data.domain.getTranslatedDescriptionMasterDetail());
                    this.set("mditems", CMDBuildUI.locales.Locales.relations.mditems);

                    CMDBuildUI.util.helper.ModelHelper.getModel(
                        CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
                        data.targettypename
                    ).then(function (model) {
                        // set model name
                        me.set("storeInfo.model", model.getName());

                        // set store type
                        var proxytype = me.getView().getIsProcess() ? undefined : 'cards';
                        me.set("storeInfo.type", proxytype);

                        // set store proxy
                        var source, destination, direction;
                        if (data.domain.get("cardinality") === '1:N') {
                            source = data.domain.get("destination");
                            destination = data.domain.get("source");
                            direction = "_2";
                        } else {
                            source = data.domain.get("source");
                            destination = data.domain.get("destination");
                            direction = "_1";
                        }
                        var filter = {
                            relation: [{
                                domain: data.domain.getId(),
                                type: "oneof",
                                destination: destination,
                                source: source,
                                direction: direction,
                                cards: [{
                                    className: me.get("objectTypeName"),
                                    id: me.get("objectId")
                                }]
                            }]
                        };
                        me.set("storeInfo.proxyurl", model.getProxy().getUrl());
                        me.set("storeInfo.proxyextraparams", { filter: Ext.JSON.encode(filter) });

                        // set autoload
                        me.set("storeInfo.autoload", true);

                        var item = CMDBuildUI.util.helper.ModelHelper.getClassFromName(data.targettypename);
                        // update target type description
                        var addbuttontext = Ext.String.format(
                            '{0} {1}', 
                            CMDBuildUI.locales.Locales.relations.adddetail, 
                            item.getTranslatedDescription()
                        );
                        me.set("addbutton.text", addbuttontext);
                        me.set("addbutton.disabled", !(me.get("basepermissions.edit") && item.get(CMDBuildUI.model.base.Base.permissions.edit)));

                        // fire data changed event
                        var view = me.getView();
                        view.fireEvent("targetdataupdated", view, view.getTargetTypeObject(), model, data.domain);
                    });
                }
            }
        }
    },

    stores: {
        cards: {
            type: 'classes-cards',
            model: '{storeInfo.model}',
            autoLoad: '{storeInfo.autoload}',
            proxy: {
                type: 'baseproxy',
                url: '{storeInfo.proxyurl}',
                extraParams: '{storeInfo.proxyextraparams}'
            }
        }
    }

});
