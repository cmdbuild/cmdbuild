Ext.define('CMDBuildUI.view.relations.masterdetail.TabController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.relations-masterdetail-tab',

    control: {
        '#': {
            targetdataupdated: 'onTargetDataUpdated'
        },
        '#searchtext': {
            change: 'onSearchTextChange'
        },
        '#adddetailbtn': {
            click: 'onAddDetailButtonClick'
        }
    },

    /**
     * @param {CMDBuildUI.view.relations.masterdetail.Tab} view
     * @param {CMDBuildUI.model.classes.Class|CMDBuildUI.model.processes.Process} targetTypeObject
     * @param {CMDBuildUI.model.classes.Card} targetTypeModel
     * @param {CMDBuildUI.model.domains.Domain} domain
     * @param {Object} eOpts
     */
    onTargetDataUpdated: function (view, targetTypeObject, targetTypeModel, domain, eOpts) {
        var me = this;
        // add grid
        view.add({
            xtype: 'classes-cards-grid-grid',
            showAddButton: false,
            allowFilter: false,
            plugins: [{
                ptype: 'forminrowwidget',
                expandOnDblClick: true,
                widget: {
                    xtype: 'classes-cards-card-view',
                    viewModel: {}, // do not remove otherwise the viewmodel will not be initialized
                    tabpaneltools: [{
                        xtype: 'tool',
                        itemId: 'editBtn',
                        glyph: 'f040@FontAwesome', // Pencil icon
                        cls: 'management-tool',
                        disabled: true,
                        tooltip: CMDBuildUI.locales.Locales.relations.editdetail,
                        callback: function (panel, tool, event) {
                            me.onViewCardEditButtonClick(panel.up());
                        },
                        autoEl: {
                            'data-testid': 'cards-card-view-editBtn'
                        },
                        bind: {
                            disabled: '{!basepermissions.edit && !permissions.edit}'
                        }
                    }, {
                        xtype: 'tool',
                        itemId: 'openBtn',
                        glyph: 'f08e@FontAwesome', // Open icon
                        cls: 'management-tool',
                        hidden: false,
                        tooltip: CMDBuildUI.locales.Locales.relations.opendetail,
                        callback: function (panel, tool, event) {
                            panel.fireEvent("openbtnclick", panel, tool, event);
                        },
                        autoEl: {
                            'data-testid': 'cards-card-view-openBtn'
                        }
                    }, {
                        xtype: 'tool',
                        itemId: 'deleteBtn',
                        glyph: 'f014@FontAwesome', // Trash icon
                        cls: 'management-tool',
                        disabled: true,
                        tooltip: CMDBuildUI.locales.Locales.relations.deletedetail,
                        callback: function (panel, tool, event) {
                            me.onViewCardDeleteButtonClick(panel.up());
                        },
                        autoEl: {
                            'data-testid': 'cards-card-view-deleteBtn'
                        },
                        bind: {
                            disabled: '{!basepermissions.delete && !permissions.delete}'
                        }
                    }]
                }
            }],
            viewModel: {
                data: {
                    objectTypeName: view.getTargetTypeName()
                }
            }
        });

        // update button
        if (targetTypeObject.get("prototype")) {
            // if is superclass add menu with all children leafs
            var children = targetTypeObject.getChildren(true);
            var menu = [];
            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                menu.push({
                    text: child.getTranslatedDescription(),
                    iconCls: 'x-fa fa-file-text-o',
                    listeners: {
                        click: 'onAddDetailMenuItemClick'
                    },
                    disabled: !child.get(CMDBuildUI.model.base.Base.permissions.edit),
                    type: child.get("name")
                });
            }
            this.lookupReference("adddetailbtn").setMenu(Ext.Array.sort(menu, function (a, b) {
                return a.text === b.text ? 0 : (a.text < b.text ? -1 : 1);
            }));
        }
    },

    /**
     * @param {Ext.form.field.Field} field
     * @param {Object} newValue
     * @param {Object} oldValue
     * @param {Object} eOpts
     */
    onSearchTextChange: function (field, newValue, oldValue, eOpts) {
        this.getViewModel().set("searchtext", newValue);
    },

    /**
     * @param {Ext.button.Button} button Add detail botton
     * @param {Event} e
     * @param {Object} eOpts
     */
    onAddDetailButtonClick: function (button, e, eOpts) {
        var view = this.getView();
        var targetTypeName = view.getTargetTypeName();
        var targetTypeObject = view.getTargetTypeObject();
        if (!targetTypeObject.get("prototype")) {
            // if element is not prototype
            this.showAddDetailForm(targetTypeName, targetTypeObject.get("description"));
        }
    },

    /**
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onAddDetailMenuItemClick: function (item, event, eOpts) {
        this.showAddDetailForm(item.type, item.text);
    },

    /**
     * Show add detail form
     * @param {String} targetTypeName
     * @param {String} targetTypeDescription
     */
    showAddDetailForm: function (targetTypeName, targetTypeDescription) {
        var vm = this.getViewModel();
        var view = this.getView();
        CMDBuildUI.util.helper.ModelHelper.getModel(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
            targetTypeName
        ).then(function (model) {
            var panel;
            var title = Ext.String.format("New {0}", targetTypeDescription);
            var config = {
                xtype: 'classes-cards-card-create',
                viewModel: {
                    data: {
                        objectTypeName: targetTypeName
                    }
                },
                defaultValues: [{
                    domain: view.getDomain().get("name"),
                    value: vm.get("objectId"),
                    valuedescription: vm.get("objectDescription"),
                    editable: false
                }],
                redirectAfterSave: false,
                listeners: [{
                    itemcreated: function (record, eOpts) {
                        panel.destroy();
                        vm.get("cards").load(); // TODO: reload grid
                    },
                    cancelcreation: function (eOpts) {
                        panel.destroy();
                    }
                }]
            };
            panel = CMDBuildUI.util.Utilities.openPopup('popup-add-detail', title, config, null);
        }, function () {
        });
    },

    /**
     * Called when card is shown
     * @param {CMDBuildUI.view.classes.cards.card.View} cardpanel 
     */
    onViewCardEditButtonClick: function (cardpanel) {
        var popup;
        var me = this;
        // open popup
        var title = cardpanel.getTitle();
        var cvm = cardpanel.getViewModel();
        var config = {
            xtype: 'classes-cards-card-edit',
            objectTypeName: cvm.get("objectTypeName"),
            objectId: cvm.get("objectId"),
            redirectAfterSave: false,
            listeners: {
                itemupdated: function () {
                    popup.close();
                    me.getViewModel().get("cards").load();
                },
                cancelupdating: function () {
                    popup.close();
                }
            }
        };
        popup = CMDBuildUI.util.Utilities.openPopup('popup-edit-card', title, config);
    },

    /**
     * Called when card is shown
     * @param {CMDBuildUI.view.classes.cards.card.View} cardpanel 
     */
    onViewCardDeleteButtonClick: function (cardpanel) {
        var me = this;
        Ext.Msg.confirm(
            "Attention",    //TODO:translate
            "Are you sure you want to delete this card?",   //TODO:translate
            function (btnText) {
                if (btnText === "yes") {
                    CMDBuildUI.util.Ajax.setActionId('relation.delete');
                    cardpanel.getViewModel().get("theObject").erase({
                        success: function (record, operation) {
                            me.getViewModel().get("cards").load();
                        }
                    });
                }
            },
            this
        );
    }
});

