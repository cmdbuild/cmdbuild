Ext.define('CMDBuildUI.view.history.GridModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.history-grid',

    data: {
        targetType: null,
        targetTypeName: null,
        targetId: null
    },

    formulas: {
        objectsStoreModelName: {
            bind: {
                category: '{targetType}',
                type: '{targetTypeName}'
            },
            get: function(data) {
                if (data.category && data.type) {
                    var model = CMDBuildUI.util.helper.ModelHelper.getHistoryModel(data.category, data.type);
                    return model.getName();
                }
            }
        },
        objectsStoreAutoLoad: {
            bind: '{objectsStoreModelName}',
            get: function(modelname) {
                return modelname ? true : false;
            }
        },
        objectsStoreProxyUrl: {
            bind: {
                category: '{targetType}',
                type: '{targetTypeName}',
                id: '{targetId}'
            },
            get: function(data) {
                if (data.category && data.type && data.id) {
                    // get parent model returns the url
                    var parentModel = Ext.ClassManager.get(CMDBuildUI.util.helper.ModelHelper.getModelName(data.category, data.type));
                    return Ext.String.format(
                        "{0}/{1}/history",
                        parentModel.getProxy().getUrl(),
                        data.id
                    );
                }
            }
        }
    },

    stores: {
        objects: {
            type: 'history',
            model: '{objectsStoreModelName}',
            autoLoad: '{objectsStoreAutoLoad}',
            proxy: {
                url: '{objectsStoreProxyUrl}',
                type: 'baseproxy'
            }
        }
    }
});
