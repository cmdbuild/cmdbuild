Ext.define('CMDBuildUI.view.history.Grid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.history.GridController',
        'CMDBuildUI.view.history.GridModel'
    ],

    alias: 'widget.history-grid',
    controller: 'history-grid',
    viewModel: {
        type: 'history-grid'
    },

    plugins: [{
        ptype: 'forminrowwidget',
        expandOnDblClick: true,
        widget: {
            xtype: 'history-item',
            viewModel: {} // do not remove otherwise the viewmodel will not be initialized
        }
    }],

    config: {
        className: null,
        allowFilter: false,
        showAddButton: false
    },

    forceFit: true,
    loadMask: true,

    columns: [{
        text: CMDBuildUI.locales.Locales.history.begindate,
        dataIndex: '_beginDate',
        align: 'left',
        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
            return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.datetime);
        },
        hidden: false,
        localized: {
            text: 'CMDBuildUI.locales.Locales.history.begindate'
        }
    }, {
        text: CMDBuildUI.locales.Locales.history.enddate,
        dataIndex: '_endDate',
        align: 'left',
        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
            return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.datetime);
        },
        hidden: false,
        localized: {
            text: 'CMDBuildUI.locales.Locales.history.enddate'
        }
    }, {
        text: CMDBuildUI.locales.Locales.history.user,
        dataIndex: '_user',
        align: 'left',
        hidden: false,
        localized: {
            text: 'CMDBuildUI.locales.Locales.history.user'
        }
    }],

    selModel: {
        pruneRemoved: false // See https://docs.sencha.com/extjs/6.2.0/classic/Ext.selection.Model.html#cfg-pruneRemoved
    },

    bind: {
        store: '{objects}'
    }
});
