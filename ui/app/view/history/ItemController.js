Ext.define('CMDBuildUI.view.history.ItemController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.history-item',

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        'field': {
            change: 'onFieldChange'
        }
    },

    /**
     * @param {CMDBuildUI.view.history.Item} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        // get element id
        var config = view.getInitialConfig();
        if (!Ext.isEmpty(config._rowContext)) {
            var record = config._rowContext.record; // get widget record
            if (record && record.getData()) {
                view.setObjectId(record.get("_id"));
            }
        }

        var vm = this.getViewModel();
        var targetType = vm.get("targetType");
        var targetTypeName = vm.get("targetTypeName");

        var model = CMDBuildUI.util.helper.ModelHelper.getHistoryModel(targetType, targetTypeName);
        model.setProxy({
            url: vm.get("objectsStoreProxyUrl"),
            type: 'baseproxy'
        });
        vm.linkTo("theObject", {
            type: model.getName(),
            id: view.getObjectId()
        });

        // get form fields
        var tabpanel = CMDBuildUI.util.helper.FormHelper.renderForm(model, {
            mode: CMDBuildUI.util.helper.FormHelper.formmodes.read,
            showNotes: true
        });

        Ext.apply(tabpanel, {
            tools: view.tabpaneltools
        });
        view.add(tabpanel);
    },

    /**
     * @param {Ext.form.field.Base} field
     * @param {Object} newValue
     * @param {Object} oldValue
     * @param {Object} eOpts
     */
    onFieldChange: function(field, eOpts) {
        var object = this.getViewModel().get("theObject");
        var fieldname = Ext.String.format("_{0}_changed", field.getName());
        if (object && object.get(fieldname) == true) {
            field.addCls("highlight-field");
        }
    }
});
