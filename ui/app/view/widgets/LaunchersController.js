Ext.define('CMDBuildUI.view.widgets.LaunchersController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-launchers',

    control: {
        '#': {
            widgetschanged: 'onWidgetsChanged',
            widgetbuttonclick: 'onWidgetButtonClick'
        }
    },

    /**
     * @param {CMDBuildUI.view.widgets.Launchers} view
     * @param {Ext.data.Store} newvalue
     * @param {Ext.data.Store} oldvalue
     */
    onWidgetsChanged: function (view, newvalue, oldvalue) {
        if (newvalue && newvalue.getData().length) {
            Ext.Array.each(newvalue.getRange(), function (widget, index) {
                if (widget.get("_active")) {
                    // add value binding
                    var binding;
                    if (widget.get("_output")) {
                        binding = {
                            value: '{theObject.' + widget.get("_output") + '}'
                        };
                    }

                    // add widget button
                    view.add({
                        xtype: 'widgets-button',
                        text: widget.get("_label") + (widget.get("_required") ? ' *' : ''),
                        disabled: view.getFormMode() === CMDBuildUI.view.widgets.Launchers.formmodes.view && !widget.get("_alwaysenabled"),
                        required: widget.get("_required"),
                        bind: binding,
                        handler: function (button, e) {
                            view.fireEvent("widgetbuttonclick", view, button, widget, e);
                        }
                    });
                }
            });

            // show panel
            this.getViewModel().set("hideLaunchersPanel", false);
        }
    },

    /**
     * @param {Ext.Component} view
     * @param {Ext.button.Button} button
     * @param {CMDBuildUI.model.WidgetDefinition} widget
     * @param {Event} e
     * @param {Object} eOpts
     */
    onWidgetButtonClick: function (view, button, widget, e, eOpts) {
        // update ajax action id
        CMDBuildUI.util.Ajax.setActionId(Ext.String.format(
            'widget.open.{0}.{1}',
            widget.get("_type"),
            widget.getId()
        ));

        var configAttachments = CMDBuildUI.util.helper.Configurations.get("cm_system_dms_enabled");
        var popup;
        var widgettype = widget.get("_type");
        var xtype = CMDBuildUI.util.Config.widgets[widgettype];
        if (!xtype) {
            Ext.Msg.alert(
                'Warning',
                Ext.String.format('Widget <strong>{0}</strong> not implemented!', widgettype) // TODO: translate
            );
            return;
        }

        if (widgettype === 'openAttachment' && !configAttachments){
            Ext.Msg.alert(
                'Warning',
                Ext.String.format('Attachments disabled!') 
            );
            return;
        }

        // create widget configuration
        var config = {
            xtype: xtype,
            widgetId: widget.getId(),
            output: widget.get("_output"),
            _widgetOwner: view,
            viewModel: {
                data: {
                    theWidget: widget,
                    theTarget: this.getViewModel().get("theObject")
                }
            },
            bind: {
                target: '{theTarget}'
            },
            listeners: {
                /**
                 * Custom event to close popup directly from widget
                 */
                popupclose: function (eOpts) {
                    popup.close();
                }
            }
        };

        // custom panel listeners
        var listeners = {
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            beforeclose: function (panel, eOpts) {
                panel.removeAll(true);
            },
            /**
             * @param {Ext.panel.Panel} panel
             * @param {Object} eOpts
             */
            close: function (panel, eOpts) {
                button.fireEvent('validitychange', button, button.isValid());
            }
        };
        // open popup
        popup = CMDBuildUI.util.Utilities.openPopup(
            'popup-show-widget',
            widget.get("_label"),
            config,
            listeners
        );


    }
});
