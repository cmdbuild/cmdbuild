
Ext.define('CMDBuildUI.view.widgets.linkcards.Panel', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.widgets.linkcards.PanelController',
        'CMDBuildUI.view.widgets.linkcards.PanelModel'
    ],

    mixins: [
        'CMDBuildUI.view.widgets.Mixin'
    ],

    alias: 'widget.widgets-linkcards-panel',
    controller: 'widgets-linkcards-panel',
    viewModel: {
        type: 'widgets-linkcards-panel'
    },

    /**
     * @cfg {String} theWidget.ClassName
     * Class or Process name
     */

    /**
     * @cfg {Object} theWidget._Filter_ecql
     * eCQL filter definition.
     */

    /**
     * @cfg {String} theWidget._DefaultSelection_ecql
     * Default selection defined as eCQL filter.
     */

    /**
     * @cfg {Number} theWidget.NoSelect
     * If equals to 1 disable the selection.
     */

    /**
     * @cfg {Number} theWidget.SingleSelect
     * If equals to 1 enable the selection of only one item.
     */

    /**
     * @cfg {*} theWidget.AllowCardEditing
     * If present and different to false, allows the user to modify
     * the row item.
     */

    /**
     * @cfg {Boolean} theWidget.DisableGridFilterToggler
     * If true disable filter toggle button.
     */

    layout: "fit",

    tbar: [{
        xtype: 'button',
        enableToggle: true,
        ui: 'management-action',
        disabled: true,
        reference: 'togglefilter',
        itemId: 'togglefilter',
        text: CMDBuildUI.locales.Locales.widgets.linkcards.togglefilterenabled,
        iconCls: 'x-fa fa-filter',
        bind: {
            text: '{textTogglefilter}',
            disabled: '{disableTogglefilter}',
            pressed: '{disablegridfilter}'
        }
    }, {
        xtype: 'button',
        ui: 'management-action',
        disabled: true,
        reference: 'refreshselection',
        itemId: 'refreshselection',
        text: CMDBuildUI.locales.Locales.widgets.linkcards.refreshselection,
        iconCls: 'x-fa fa-refresh',
        bind: {
            text: '{textRefreshselection}',
            disabled: '{disableRefreshselection}'
        }
    }],

    fbar: [{
        xtype: 'button',
        ui: 'secondary-action',
        reference: 'closebtn',
        itemId: 'closebtn',
        text: CMDBuildUI.locales.Locales.common.actions.close,
        iconCls: 'x-fa fa-check',
        bind: {
            text: '{textClosebtn}'
        }
    }]
});
