Ext.define('CMDBuildUI.view.widgets.linkcards.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-linkcards-panel',

    control: {
        "#": {
            beforerender: "onBeforeRender"
        },
        grid: {
            selectionchange: "onSelectionChange"
        },
        tableview: {
            actionviewobject: "onActionViewObject",
            actioneditobject: "onActionEditObject"
        },
        '#togglefilter': {
            toggle: 'onToggleFilterToggle'
        },
        '#refreshselection': {
            click: 'onRefreshSelectionClick'
        },
        '#closebtn': {
            click: 'onCloseBtnClick'
        }
    },

    /**
     * @param {CMDBuildUI.view.widgets.linkcards.Panel} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        var objectTypeName;
        var me = this;
        var vm = this.getViewModel();
        var widget = vm.get("theWidget");

        if (widget.get("_Filter_ecql")) {
            objectTypeName = widget.get("_Filter_ecql").from;
        } else {
            objectTypeName = widget.get("ClassName");
        }

        // get object type from type name
        vm.set("objectType", CMDBuildUI.util.helper.ModelHelper.getObjectTypeByName(objectTypeName));

        // get the model for objtect type name
        CMDBuildUI.util.helper.ModelHelper.getModel(
            vm.get("objectType"),
            objectTypeName
        ).then(
            function (model) {
                // set model variable into view model
                vm.set("model", model);

                // get columns from model
                var columns = CMDBuildUI.util.helper.GridHelper.getColumns(model.getFields(), {
                    allowFilter: false
                });

                // add view object action columns
                columns.push({
                    xtype: 'actioncolumn',
                    minWidth: 30,
                    maxWidth: 30,
                    hideable: false,
                    disabled: true,
                    align: 'center',
                    bind: {
                        disabled: '{disableViewAction}'
                    },
                    iconCls: 'x-fa fa-external-link',
                    tooltip: CMDBuildUI.locales.Locales.widgets.linkcards.opencard,
                    handler: function (grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        grid.fireEvent("actionviewobject", grid, record, rowIndex, colIndex);
                    }
                });
                // add edit object action columns
                columns.push({
                    xtype: 'actioncolumn',
                    minWidth: 30,
                    maxWidth: 30,
                    hideable: false,
                    disabled: true,
                    align: 'center',
                    bind: {
                        disabled: '{disableEditAction}'
                    },
                    iconCls: 'x-fa fa-pencil',
                    tooltip: CMDBuildUI.locales.Locales.widgets.linkcards.editcard,
                    handler: function (grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        grid.fireEvent("actioneditobject", grid, record, rowIndex, colIndex);
                    }
                });

                // define selection model
                var selModel = {
                    selType: 'checkboxmodel',
                    checkOnly: true
                };
                if (vm.get("theWidget").get("NoSelect")) {
                    selModel = null;
                }
                if (vm.get("theWidget").get("SingleSelect")) {
                    selModel = "SINGLE";
                }

                // add grid
                view.add({
                    xtype: 'grid',
                    columns: columns,
                    forceFit: true,
                    loadMask: true,
                    itemId: 'grid',
                    reference: 'grid',
                    selModel: selModel,
                    bind: {
                        store: '{gridrows}',
                        selection: '{selection}'
                    }
                });
            });
    },

    /**
     * @param {Ext.grid.Panel} view
     * @param {Ext.data.Model[]} selected
     * @param {Object} eOpts
     */
    onSelectionChange: function (grid, selected, eOpts) {
        var view = this.getView();
        if (view.getOutput()) {
            var sel = [];
            for (var i = 0; i < selected.length; i++) {
                sel.push({
                    _id: selected[i].getId()
                });
            }
            view.getTarget().set(view.getOutput(), sel);
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Boolean} selected
     * @param {Object} eOpts
     */
    onToggleFilterToggle: function (button, selected, eOpts) {
        var params = {
            filter: null
        };

        // get the filter if toggle is not selected 
        if (!selected) {
            var vm = this.getViewModel();
            var filter = vm.get("theWidget").get("_Filter_ecql");
            var target = vm.get("theTarget");

            if (filter) {
                // calculate ecql
                var ecql = CMDBuildUI.util.ecql.Resolver.resolve(
                    filter,
                    target
                );
                if (ecql) {
                    params.filter = Ext.JSON.encode({
                        ecql: ecql
                    });
                }
            }
        }

        this.getView().lookupReference("grid").getStore().load({
            params: params
        });
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onRefreshSelectionClick: function (button, e, eOpts) {
        var store = this.getViewModel().get("selectedrows");
        this.getViewModel().set("selection", null);
        // TODO: remove parameters when sencha give us the fix.
        if (!store.getData().length) {
            this.onSelectedRowsLoaded(store, [], true);
        } else {
            this.onSelectedRowsLoaded(store, store.getRange(0, store.getTotalCount()), true); 
        }
    },

    /**
     * @param {Ext.data.Store} store
     * @param {Ext.data.Model[]} records
     * @param {Boolean} successful
     * @param {Ext.data.operation.Read} operation
     * @param {Object} eOpts
     */
    onSelectedRowsLoaded: function (store, records, successful, operation, eOpts) {
        var vm = this.getViewModel();
        if (successful && vm.get("selection") === null) {
            this.getView().lookupReference("grid").getSelectionModel().deselectAll();
            var gridstore = vm.get("gridrows");
            var selection = [];
            for (var i = 0; i < records.length; i++) {
                var r = gridstore.getById(records[i].getId());
                if (r) {
                    selection.push(r);
                }
            }
            vm.set("selection", selection);
        }
    },

    /**
    * @param {CMDBuildUI.view.attachments.Grid} grid
    * @param {Ext.data.Model} record
    * @param {Number} rowIndex
    * @param {Number} colIndex
    */
    onActionViewObject: function (grid, record, rowIndex, colIndex) {
        var title, config;
        var vm = this.getViewModel();
        if (vm.get("objectType") === CMDBuildUI.util.helper.ModelHelper.objecttypes.klass) {
            title = CMDBuildUI.util.helper.ModelHelper.getClassDescription(record.get("_type"));
            config = {
                xtype: 'classes-cards-card-view',
                objectTypeName: record.get("_type"),
                objectId: record.getId(),
                shownInPopup: true,
                hideTools: true
            };
        }
        CMDBuildUI.util.Utilities.openPopup('popup-open-card', title, config);
    },

    /**
    * @param {CMDBuildUI.view.attachments.Grid} grid
    * @param {Ext.data.Model} record
    * @param {Number} rowIndex
    * @param {Number} colIndex
    */
    onActionEditObject: function (grid, record, rowIndex, colIndex) {
        var title, config, popup;
        var me = this;
        var vm = this.getViewModel();
        if (vm.get("objectType") === CMDBuildUI.util.helper.ModelHelper.objecttypes.klass) {
            title = CMDBuildUI.util.helper.ModelHelper.getClassDescription(record.get("_type"));
            config = {
                xtype: 'classes-cards-card-edit',
                objectTypeName: record.get("_type"),
                objectId: record.getId(),
                listeners: {
                    itemupdated: function () {
                        popup.close();
                        me.onToggleFilterToggle(null, vm.get("disablegridfilter"), null);
                    },
                    cancelupdating: function () {
                        popup.close();
                    }
                }
            };
        }
        popup = CMDBuildUI.util.Utilities.openPopup('popup-edit-card', title, config);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCloseBtnClick: function (button, e, eOpts) {
        this.getView().fireEvent("popupclose");
    }

});
