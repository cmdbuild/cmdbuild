Ext.define('CMDBuildUI.view.widgets.customform.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-customform-panel',

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            beforedestroy: 'onBeforeDestroy'
        },
        tableview: {
            actionclonerowclick: 'onActionCloneRowClick',
            actioneditrowclick: 'onActionEditRowClick',
            actionremoverowclick: 'onActionRemoveRowClick'
        },
        '#closebtn': {
            click: 'onCloseBtnClick'
        }
    },

    /**
     * @param {CMDBuildUI.view.widgets.customform.Panel} view
     * @param {Object} eOpts
     */
    onBeforeRender: function (view, eOpts) {
        if (!Ext.ClassManager.isCreated(view.getModelName())) {
            // generate model
            var theWidget = this.getViewModel().get("theWidget");
            if (theWidget.get("ModelType").toLowerCase() === 'form') {
                this.createModelFromWidgetDefAttributes();
            } else if (theWidget.get("ModelType").toLowerCase() === 'class') {
                this.createModelFromClassAttributes();
            } else if (theWidget.get("ModelType").toLowerCase() === 'function') {
                // TODO: Show error message - not yet implemented
            } else {
                // TODO: Show error message
            }
        } else {
            // render widget
            this.renderWidget();
        }
    },

    /**
     * On popup close handler.
     */
    onBeforeDestroy: function () {
        // update output variable in target object
        var view = this.getView();
        var store = this.getViewModel().get("dataStore");
        if (store) {
            var serializationconfig = this.getSerializationConfig();
            var rows = [];

            var modelAttributes = [];
            this.getModelAttributes().forEach(function(attr) {
                modelAttributes.push(attr.get("name"));
            });

            if (serializationconfig.type === "json") {
                store.each(function (row) {
                    var r = {};
                    // save only model attributes
                    Ext.Object.each(row.getData(), function(k, v) {
                        if (modelAttributes.indexOf(k) !== -1) {
                            r[k] = v;
                        }
                    });
                    rows.push(r);
                });
            } else {
                store.each(function (row) {
                    var rdata = row.getData();
                    var attributes = [];
                    for (var k in rdata) {
                        // save only model attributes
                        if (modelAttributes.indexOf(k) !== -1) {
                            attributes.push(Ext.String.format("{0}{1}{2}", k, serializationconfig.keyseparator, rdata[k]));
                        }
                    }
                    rows.push(attributes.join(serializationconfig.attributeseparator));
                });
            }   
            view.getTarget().set(view.getOutput(), rows.join(serializationconfig.rowseparator));
        }
    },

    /**
     * Render widget content
     */
    renderWidget: function () {
        var conf;
        var theWidget = this.getViewModel().get("theWidget");
        if (theWidget.get("Layout").toLowerCase() === 'form') {
            conf = this.getFormConfig();
        } else if (theWidget.get("Layout").toLowerCase() === 'grid') {
            conf = this.getGridConfig();
            this.getViewModel().setStores({
                dataStore: {
                    model: this.getView().getModelName(),
                    proxy: 'memory'
                }
            });
        } else {
            // TODO: Show error message
        }
        this.getView().removeAll(true);
        this.getView().add(conf);
        this.laodData();
    },

    /**
     * Load data
     * @param {Boolean} force If `true` data is always readed from the server or from the configuration.
     * Data saved in output variable in target object will be ignored.
     */
    laodData: function (force) {
        var vm = this.getViewModel();
        var theWidget = vm.get("theWidget");

        // clear store data
        vm.get("dataStore").removeAll();

        // check refresh behaviour
        if (theWidget.get("RefreshBehaviour").toLowerCase() === 'everytime') {
            force = true;
        }

        // get data from output
        var outputdata = vm.get("theTarget").get(this.getView().getOutput());
        if (!force && outputdata !== undefined) {
            var serializationconfig = this.getSerializationConfig();
            var data = [];
            if (serializationconfig.type === "json") {
                data = Ext.JSON.decode(outputdata);
            } else {
                outputdata.split(serializationconfig.rowseparator).forEach(function (srow) {
                    var row = {};
                    srow.split(serializationconfig.attributeseparator).forEach(function (sattribute) {
                        var splitted_attr = sattribute.split(serializationconfig.keyseparator);
                        if (splitted_attr.length === 2) {
                            row[splitted_attr[0]] = splitted_attr[1];
                        } else if (splitted_attr.length === 1) {
                            row[splitted_attr[0]] = null;
                        }
                    });
                    data.push(row);
                });
            }
            vm.get("dataStore").add(data);
            return;
        }

        // get data from configuration or server
        if (theWidget.get("DataType").toLowerCase() === 'raw' || theWidget.get("DataType").toLowerCase() === 'raw_json') {
            // TODO: Show warning message - not implemented yet
        } else if (theWidget.get("DataType").toLowerCase() === 'raw_text') {
            // TODO: Show warning message - not implemented yet
        } else if (theWidget.get("DataType").toLowerCase() === 'function') {
            this.loadDataFromFunction();
        } else {
            // TODO: Show error message
        }
    },

    /**
     * @param {Ext.view.Table} tableview
     * @param {CMDBuildUI.model.base.Base} record
     * @param {Integer} rowIndex
     * @param {Integer} colIndex
     */
    onActionRemoveRowClick: function (tableview, record, rowIndex, colIndex) {
        // erase record
        record.erase();
    },

    /**
     * @param {Ext.view.Table} tableview
     * @param {CMDBuildUI.model.base.Base} record
     * @param {Integer} rowIndex
     * @param {Integer} colIndex
     */
    onActionEditRowClick: function (tableview, record, rowIndex, colIndex) {
        var config = this.getFormConfig();
        var popup;
        var theRow = record.clone();

        Ext.apply(config, {
            viewModel: {
                data: {
                    theRow: theRow
                }
            },
            buttons: [{
                reference: 'saveBtn',
                itemId: 'saveBtn',
                ui: 'management-action-small',
                text: CMDBuildUI.locales.Locales.common.actions.save,
                autoEl: {
                    'data-testid': 'widgets-customform-form-save'
                },
                handler: function () {
                    var changes = theRow.getChanges();
                    for (var c in changes) {
                        record.set(c, theRow.get(c));
                    }
                    popup.close();
                }
            }, {
                reference: 'cancelBtn',
                itemId: 'cancelBtn',
                ui: 'secondary-action-small',
                text: CMDBuildUI.locales.Locales.common.actions.cancel,
                autoEl: {
                    'data-testid': 'widgets-customform-form-cancel'
                },
                handler: function () {
                    popup.close();
                }
            }]
        });

        popup = CMDBuildUI.util.Utilities.openPopup(
            null,
            Ext.String.format(
                "{0} - {1}",
                this.getViewModel().get("theWidget").get("_label"),
                CMDBuildUI.locales.Locales.widgets.customform.editrow
            ),
            config, {
                /**
                 * @param {Ext.panel.Panel} panel
                 * @param {Object} eOpts
                 */
                beforeclose: function (panel, eOpts) {
                    panel.removeAll(true);
                }
            }
        );
    },

    /**
     * @param {Ext.view.Table} tableview
     * @param {CMDBuildUI.model.base.Base} record
     * @param {Integer} rowIndex
     * @param {Integer} colIndex
     */
    onActionCloneRowClick: function (tableview, record, rowIndex, colIndex) {
        var recordData = record.getData();
        delete recordData._id;
        this.getViewModel().get("dataStore").add([recordData]);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCloseBtnClick: function (button, e, eOpts) {
        this.getView().fireEvent("popupclose");
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Ext.event.Event} e
     */
    onRefreshBtnClick: function (button, e) {
        // update ajax action id
        CMDBuildUI.util.Ajax.setActionId('widget.customform.refreshdata');
        // load data
        this.laodData(true);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Ext.event.Event} e
     */
    onAddRowBtnClick: function (button, e) {
        var config = this.getFormConfig();
        var popup;
        var theRow = Ext.create(this.getView().getModelName());
        var vm = this.getViewModel();

        Ext.apply(config, {
            viewModel: {
                data: {
                    theRow: theRow
                }
            },
            buttons: [{
                reference: 'saveBtn',
                itemId: 'saveBtn',
                ui: 'management-action-small',
                text: CMDBuildUI.locales.Locales.common.actions.save,
                autoEl: {
                    'data-testid': 'widgets-customform-form-save'
                },
                handler: function () {
                    vm.get("dataStore").add(theRow);
                    popup.close();
                }
            }, {
                reference: 'cancelBtn',
                itemId: 'cancelBtn',
                ui: 'secondary-action-small',
                text: CMDBuildUI.locales.Locales.common.actions.cancel,
                autoEl: {
                    'data-testid': 'widgets-customform-form-cancel'
                },
                handler: function () {
                    popup.close();
                }
            }]
        });

        popup = CMDBuildUI.util.Utilities.openPopup(
            null,
            Ext.String.format(
                "{0} - {1}",
                this.getViewModel().get("theWidget").get("_label"),
                CMDBuildUI.locales.Locales.widgets.customform.addrow
            ),
            config, {
                /**
                 * @param {Ext.panel.Panel} panel
                 * @param {Object} eOpts
                 */
                beforeclose: function (panel, eOpts) {
                    panel.removeAll(true);
                }
            }
        );
    },

    privates: {
        _model_attributes: [],

        /**
         * @return {CMDBuildUI.model.Attribute[]}
         */
        getModelAttributes: function () {
            return this._model_attributes;
        },

        /**
         * @param {CMDBuildUI.model.Attribute[]} attributes
         */
        setModelAttributes: function (attributes) {
            this._model_attributes = attributes;
        },

        /**
         * @param {CMDBuildUI.model.Attribute} attribute
         */
        addModelAttribute: function (attribute) {
            this._model_attributes.push(attribute);
        },

        /**
         * Get serialization configs.
         * @return {Object} An object containing `type`, `keyseparator`, `attributeseparator` and `rowseparator`.
         */
        getSerializationConfig: function () {
            var configs = this.getViewModel().get("theWidget");
            return {
                type: configs.get("SerializationType") || 'text',
                keyseparator: configs.get("KeyValueSeparator") || '=',
                attributeseparator: configs.get("AttributesSeparator") || ',',
                rowseparator: configs.get("RowsSeparator") || '\n'
            };
        },

        /**
         * Read attributes from class definition and create model.
         */
        createModelFromClassAttributes: function () {
            var me = this;
            var theWidget = this.getViewModel().get("theWidget");
            var targetTypeName = theWidget.get("ClassModel");
            var allowedattributes;
            if (!Ext.isEmpty(theWidget.get("ClassAttributes"))) {
                allowedattributes = theWidget.get("ClassAttributes").split(",");
            }

            var baseurl = CMDBuildUI.util.helper.ModelHelper.getBaseUrl(
                CMDBuildUI.util.helper.ModelHelper.objecttypes.klass,
                targetTypeName
            );

            // create attributes store and load class attributes
            var attributesStore = Ext.create('Ext.data.Store', {
                model: 'CMDBuildUI.model.Attribute',

                proxy: {
                    url: baseurl + '/attributes',
                    type: 'cmdbuildattributesproxy'
                },
                autoDestroy: true,
                pageSize: 0 // disable pagination
            });

            // load attributes
            attributesStore.load(function (records, operation, success) {
                if (success) {
                    records.forEach(function (record, i) {
                        if ((Ext.isEmpty(allowedattributes) || allowedattributes.indexOf(record.get("name")) !== -1) &&
                            record.get("active") &&
                            !Ext.Array.contains(CMDBuildUI.util.helper.ModelHelper.ignoredFields, record.get("name"))
                        ) {
                            me.addModelAttribute(record);
                        }
                    });
                    me.createModel();
                }
            });
        },

        /**
         * Create model from attributes in widget definition. 
         */
        createModelFromWidgetDefAttributes: function () {
            var theWidget = this.getViewModel().get("theWidget");
            var str_attributes = theWidget.get("FormModel");
            var attributes_def = Ext.JSON.decode(str_attributes, true);
            if (attributes_def) {
                attributes_def.forEach(function (attribute_def, i) {
                    // TODO: check after fields adjustments
                    var attr_def = Ext.applyIf(attribute_def, {
                        index: i,
                        showInGrid: attribute_def.showColumn !== undefined ? attribute_def.showColumn == "true" : true,
                        metadata: {
                            lookupType: attribute_def.lookupType,
                            targetType: attribute_def.target && attribute_def.target.type,
                            targetClass: attribute_def.target && attribute_def.target.name
                        }
                    });
                    this.addModelAttribute(Ext.create("CMDBuildUI.model.Attribute", attr_def));
                }, this);
                this.createModel();
            }
        },

        /**
         * Create model using given attributes.
         */
        createModel: function () {
            var attributes = this.getModelAttributes();
            // sort attributes
            attributes.sort(function (a, b) {
                var ai = a.data.index || 0, bi = b.data.index;
                return ai === bi ? 0 : (ai < bi ? -1 : 1);
            });

            // get fields
            var fields = [];
            for (var i = 0; i < attributes.length; i++) {
                var field = CMDBuildUI.util.helper.ModelHelper.getModelFieldFromAttribute(attributes[i]);
                if (field) {
                    fields.push(field);
                }
            }

            // create model
            Ext.define(this.getView().getModelName(), {
                extend: 'CMDBuildUI.model.base.Base',
                fields: fields,
                proxy: 'memory'
            });

            // render widget
            this.renderWidget();
        },

        /**
         * Get grid configuration
         * 
         * @return {Object} grid configuration
         */
        getGridConfig: function () {
            var model = Ext.ClassManager.get(this.getView().getModelName());
            var columns = [];
            var fields = model.getFields();

            // define columns 
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                var column = CMDBuildUI.util.helper.GridHelper.getColumn(field, {
                    allowFilter: false
                });
                if (field.writable) {
                    column.editor = CMDBuildUI.util.helper.FormHelper.getEditorForField(field);
                    if (
                        field.cmdbuildtype.toLowerCase() === CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase() ||
                        field.cmdbuildtype.toLowerCase() === CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase()
                    ) {
                        // override editor listeners to update field description
                        column.editor.listeners = {
                            change: function (field, newvalue, oldvalue, eOpts) {
                                var object = field.getRefOwner().context.record;
                                if (object) {
                                    object.set(Ext.String.format("_{0}_description", field.name), field.getDisplayValue());
                                }
                            }
                        };
                    }
                }
                if (column) {
                    columns.push(column);
                }
            }

            // add view row action columns
            columns.push({
                xtype: 'actioncolumn',
                minWidth: 30,
                maxWidth: 30,
                hideable: false,
                disabled: true,
                align: 'center',
                bind: {
                    disabled: '{!permissions.clone}'
                },
                iconCls: 'x-fa fa-copy',
                tooltip: CMDBuildUI.locales.Locales.widgets.customform.clonerow,
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    grid.fireEvent("actionclonerowclick", grid, record, rowIndex, colIndex);
                },
                autoEl: {
                    'data-testid': 'widgets-customform-grid-row-clone'
                }
            });
            // add edit row action columns
            columns.push({
                xtype: 'actioncolumn',
                minWidth: 30,
                maxWidth: 30,
                hideable: false,
                disabled: true,
                align: 'center',
                bind: {
                    disabled: '{!permissions.modify}'
                },
                iconCls: 'x-fa fa-pencil',
                tooltip: CMDBuildUI.locales.Locales.widgets.customform.editrow,
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    grid.fireEvent("actioneditrowclick", grid, record, rowIndex, colIndex);
                },
                autoEl: {
                    'data-testid': 'widgets-customform-grid-row-modify'
                }
            });
            // add delete row action columns
            columns.push({
                xtype: 'actioncolumn',
                minWidth: 30,
                maxWidth: 30,
                hideable: false,
                disabled: true,
                align: 'center',
                bind: {
                    disabled: '{!permissions.delete}'
                },
                iconCls: 'x-fa fa-remove',
                tooltip: CMDBuildUI.locales.Locales.widgets.customform.deleterow,
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    grid.fireEvent("actionremoverowclick", grid, record, rowIndex, colIndex);
                },
                autoEl: {
                    'data-testid': 'widgets-customform-grid-row-delete'
                }
            });

            return {
                xtype: 'grid',
                forceFit: true,
                loadMask: true,
                columns: columns,
                bind: {
                    store: '{dataStore}'
                },

                plugins: {
                    ptype: 'cellediting',
                    clicksToEdit: 1
                },

                tbar: [{
                    xtype: 'button',
                    text: CMDBuildUI.locales.Locales.widgets.customform.addrow,
                    iconCls: 'x-fa fa-plus',
                    ui: 'management-action',
                    reference: 'addrowbtn',
                    itemid: 'addrowbtn',
                    handler: 'onAddRowBtnClick',
                    bind: {
                        disabled: '{!permissions.add}'
                    },
                    autoEl: {
                        'data-testid': 'widgets-customform-grid-refresh'
                    }
                }, {
                    xtype: 'button',
                    text: CMDBuildUI.locales.Locales.widgets.customform.import,
                    iconCls: 'x-fa fa-upload',
                    ui: 'management-action',
                    reference: 'importbtn',
                    itemid: 'importbtn',
                    bind: {
                        disabled: '{!permissions.import}'
                    },
                    autoEl: {
                        'data-testid': 'widgets-customform-grid-refresh'
                    }
                }, {
                    xtype: 'button',
                    text: CMDBuildUI.locales.Locales.widgets.customform.export,
                    iconCls: 'x-fa fa-download',
                    ui: 'management-action',
                    reference: 'exportbtn',
                    itemid: 'exportbtn',
                    bind: {
                        disabled: '{!permissions.export}'
                    },
                    autoEl: {
                        'data-testid': 'widgets-customform-grid-refresh'
                    }
                }, {
                    xtype: 'button',
                    text: CMDBuildUI.locales.Locales.widgets.customform.refresh,
                    iconCls: 'x-fa fa-refresh',
                    ui: 'management-action',
                    reference: 'refreshbtn',
                    itemid: 'refreshbtn',
                    handler: 'onRefreshBtnClick',
                    autoEl: {
                        'data-testid': 'widgets-customform-grid-refresh'
                    }
                }]
            };
        },

        /**
         * Get form configuration
         * 
         * @return {Object} form configuration
         */
        getFormConfig: function () {
            var model = Ext.ClassManager.get(this.getView().getModelName());

            // get form fields
            var items = CMDBuildUI.util.helper.FormHelper.renderForm(model, {
                mode: CMDBuildUI.util.helper.FormHelper.formmodes.update,
                showAsFieldsets: true,
                linkName: 'theRow'
            });

            return {
                xtype: "form",
                modelValidation: true,
                autoScroll: true,

                fieldDefaults: {
                    labelAlign: 'top'
                },

                items: items
            };
        },

        /**
         * Load data from function
         */
        loadDataFromFunction: function () {
            var me = this;
            var theWidget = this.getViewModel().get("theWidget");
            var fnName = theWidget.get("FunctionData");
            if (!fnName) {
                // TODO: return error - bad configuration
                return;
            }
            Ext.Ajax.request({
                url: CMDBuildUI.util.Config.baseUrl + CMDBuildUI.util.api.Functions.getFunctionByNameUrl(fnName),
                method: "GET",
                callback: function (opitons, success, response) {
                    if (response.status < 400) { // has no errors
                        var responseJson = Ext.JSON.decode(response.responseText, true);
                        var fn = Ext.create("CMDBuildUI.model.Function", responseJson.data);

                        // check for paramenters
                        var fn_parameters = {};
                        if (fn.get("parameters") && fn.get("parameters").length) {
                            var parameters = fn.get("parameters");
                            parameters.forEach(function (parameter) {
                                fn_parameters[parameter.name] = me.extractVariableFromString(theWidget.get(parameter.name));
                            });
                        }

                        // model configuration
                        var fn_model = { output: [] };
                        me.getModelAttributes().forEach(function (attribute) {
                            switch (attribute.get("type").toLowerCase()) {
                                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase():
                                    fn_model.output.push({
                                        name: attribute.get("name"),
                                        type: 'lookup',
                                        lookupType: attribute.get("metadata").lookupType
                                    });
                                    break;
                                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase():
                                    fn_model.output.push({
                                        name: attribute.get("name"),
                                        type: 'foreignkey',
                                        fkTarget: attribute.get("metadata").targetClass
                                    });
                                    break;
                            }
                        });

                        // load function results
                        Ext.Ajax.request({
                            url: CMDBuildUI.util.Config.baseUrl + CMDBuildUI.util.api.Functions.getFunctionOutputsByNameUrl(fnName),
                            method: "GET",
                            params: {
                                parameters: Ext.JSON.encode(fn_parameters),
                                model: fn_model.output.length > 0 ? Ext.JSON.encode(fn_model) : null
                            },
                            callback: function (fopitons, fsuccess, fresponse) {
                                if (fresponse.status < 400) { // has no errors
                                    var fresponseJson = Ext.JSON.decode(fresponse.responseText, true);
                                    me.getViewModel().get("dataStore").add(fresponseJson.data);
                                }
                            }
                        });
                    }
                }
            });
        },

        /**
         * Resolve variable.
         * @param {String} variable
         * @return {*} The variable resolved.
         */
        extractVariableFromString: function (variable) {
            variable = variable.replace("{", "").replace("}", "");
            var s_variable = variable.split(":");
            if (s_variable[0] === "server") {
                return CMDBuildUI.util.ecql.Resolver.resolveServerVariables([s_variable[1]], this.getView().getTarget())[s_variable[1]];
            } else if (s_variable[0] === "client") {
                return CMDBuildUI.util.ecql.Resolver.resolveClientVariables([s_variable[1]], this.getView().getTarget())[s_variable[1]];
            } else {
                return variable;
            }
        }


    }

});
