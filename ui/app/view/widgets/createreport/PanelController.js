Ext.define('CMDBuildUI.view.widgets.createreport.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-createreport-panel',
    mixins: [
        'CMDBuildUI.mixins.grids.GridControllerMixin'

    ],

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#closebtn': {
            click: 'onCloseBtnClick'
        },
        '#printbtn': {
            click: 'onPrintBtnClick'
        },
    },

    /**
    * @param {CMDBuildUI.view.widgets.createmodifycard.PanelController} view
    * @param {Object} eOpts
    */

    onBeforeRender: function (view, eOpts) {
        var me = this;
        var vm = me.getViewModel();
        var theWidget = vm.get('theWidget');
        var theReport = Ext.getStore("Reports").findRecord('code', theWidget.get('ReportCode'));
        var reportId = theReport.id;
        var title = theReport.get('_description_translation') ? theReport.get('_description_translation') : theReport.get('description');
        var ext = theWidget.get('ForcePDF') == 1 ? 'pdf' : theWidget.get('ForceCSV') == 1 ? 'csv' : 0;
        vm.set('extension', ext);
        CMDBuildUI.view.reports.ContainerController.prototype.getReportAttributes(me, ext, theReport, title, reportId);
    },

    /**
    * add attributes for the current report
    * @param {Object} attributes
    * @param {string} ext
    * @param {string} title
    * @param {numeric} reportId
    */

    addRelationAttibutes: function (attributes, extension, title, reportId) {
        var me = this;
        var vm = me.getViewModel();
        var view = this.getView();
        var theWidget = this.getViewModel().get('theWidget');
        var readOnlyAttributes = theWidget.get('ReadOnlyAttributes') ? theWidget.get('ReadOnlyAttributes').split(',') : '';

        var urlParts = [];
        var url = '';
        urlParts.push(CMDBuildUI.util.Config.baseUrl + '/reports/' + reportId + '/file/');
        if (extension !== 0) {
            urlParts.push('?extension=' + extension);
        }
        vm.set('urlParts', urlParts);
        var items = [];

        if (!Ext.isEmpty(attributes)) {
            attributes.forEach(function (attribute) {
                var field = CMDBuildUI.util.helper.ModelHelper.getModelFieldFromAttribute(attribute);
                field.name = attribute.get('name');  //al field.name vengono tolti gli spazi, se l'attribute li aveva la query da errore, quindi mi riprendo il nome          
                var itemmode = readOnlyAttributes.includes(field.name);     
                var item = CMDBuildUI.util.helper.FormHelper.getFormField(field, {
                    mode: field.mode,      //TODO: migliorare comportamento: verificare perchè mode:'read' torna errore su reference e lookup
                    linkName: null,
                    overrides: {
                        mandatory: field.mandatory
                    }
                });
                item.disabled = itemmode;
                if (theWidget.get(field.name) ){
                    var tgt = me.extractVariableFromString(theWidget.get(field.name), this.getViewModel().get('theTarget'));
                    item.value = tgt;
                }
                items.push(item);
            }, this);
        }
        if (extension !== 0 && Ext.isEmpty(attributes)) {
            url = CMDBuildUI.view.reports.ContainerController.prototype.createUrl(urlParts);
            CMDBuildUI.view.reports.ContainerController.prototype.fileOpener(extension, url, view);
        } else {
            me.getView().add(items);
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCloseBtnClick: function (button, e, eOpts) {
        this.getView().fireEvent("popupclose");
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Object} eOpts
     */
    onPrintBtnClick: function (buttonView, eOpts) {
        var encodedParams = '';
        var view = this.getView();
        var vm = this.getViewModel();
        var form = view.getForm();
        var parameters = {};
        var urlParts = vm.get('urlParts');
        var extension = vm.get('extension');
        var fielditems = form.getFields().items;
        if (fielditems.length > 0) { 
            fielditems.forEach(function (fielditem) {

                if (fielditem.getId() === 'reportFileFormat') {
                    extension = fielditem.getValue();
                    urlParts.push('?extension=' + extension);
                } else {
                    if (!fielditem.isFieldContainer) {
                        var value = fielditem.getValue() ? fielditem.getValue() : "";
                        parameters[fielditem.getName()] = value;
                    }
                }
            });
            encodedParams = Ext.util.Format.uri(Ext.encode(parameters));

            if (!CMDBuildUI.view.reports.ContainerController.prototype.isEmptyCustom(parameters)) {
                urlParts.push('&parameters=' + encodedParams);
            }
        }
        view.removeAll();
        buttonView.setHidden(true);
        var url = CMDBuildUI.view.reports.ContainerController.prototype.createUrl(urlParts);
        CMDBuildUI.view.reports.ContainerController.prototype.fileOpener(extension, url, view);
    },


    /**
     * Resolve variable.
     * @param {String} variable
     * @param {CMDBuildUI.model.base.Base} theTarget 
     * @return {*} The variable resolved.
     */    
    extractVariableFromString: function (variable, theTarget) {
        variable = variable.replace("{", "").replace("}", "");
        var s_variable = variable.split(":");
        var result;
        if (s_variable[0] === "server") {
            result =  CMDBuildUI.util.ecql.Resolver.resolveServerVariables([s_variable[1]], theTarget);
            return result[s_variable[1]];
        } else if (s_variable[0] === "client") {
            result =  CMDBuildUI.util.ecql.Resolver.resolveClientVariables([s_variable[1]], theTarget);
            return result[s_variable[1]];
        } else {
            return variable;
        }
    }
});
