
Ext.define('CMDBuildUI.view.widgets.createreport.Panel', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.widgets.createreport.PanelController',
        'CMDBuildUI.view.widgets.createreport.PanelModel',

        'CMDBuildUI.view.reports.ContainerController'
    ],
    mixins: [
        'CMDBuildUI.view.widgets.Mixin'
    ],

    alias: 'widget.widgets-createreport-panel',
    controller: 'widgets-createreport-panel',
    viewModel: {
        type: 'widgets-createreport-panel'
    },

    fieldDefaults: {
        labelAlign: 'top',
        margin: '0 15'
    },
    buttons: [
        {
            xtype: 'button',
            ui: 'management-action',
            text: 'Print',
            reference: 'printbtn',
            itemId: 'printbtn',
            autoEl: {
                'data-testid': 'widgets-createmodifycard-save'
            },
            formBind: true,
            disabled: true,

        },
        {
            xtype: 'button',
            ui: 'secondary-action',
            reference: 'closebtn',
            itemId: 'closebtn',
            text: CMDBuildUI.locales.Locales.common.actions.close,
            autoEl: {
                'data-testid': 'widgets-createreport-close'
            }
        }
    ]
});
