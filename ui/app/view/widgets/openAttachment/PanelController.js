Ext.define('CMDBuildUI.view.widgets.openAttachment.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-openattachment-panel',

    control: {
        "#": {
            afterrender: "onAfterRender"
        }
    },

    /**
    * @param {CMDBuildUI.view.widgets.createmodifycard.PanelController} view
    * @param {Object} eOpts
    */

    onAfterRender: function (view, eOpts) {
        var popupView = view.up();
        var widgetOwner = view._widgetOwner;
        var tabpanel = widgetOwner.up('tabpanel');
        tabpanel.setActiveTab(5);
        popupView.close();
    }
});
