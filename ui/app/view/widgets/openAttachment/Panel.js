
Ext.define('CMDBuildUI.view.widgets.openAttachment.Panel',{
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.widgets.openAttachment.PanelController',
        'CMDBuildUI.view.widgets.openAttachment.PanelModel'
    ],

    mixins: [
        'CMDBuildUI.view.widgets.Mixin'
    ],

    alias: 'widget.widgets-openattachment-panel',

    controller: 'widgets-openattachment-panel',
    viewModel: {
        type: 'widgets-openattachment-panel'
    },
    fieldDefaults: {
        labelAlign: 'top',
        margin: '0 15'
    }, 

    buttons: [
        {
            xtype: 'button',
            ui: 'secondary-action',
            reference: 'closebtn',
            itemId: 'closebtn',
            text: CMDBuildUI.locales.Locales.common.actions.close,
            autoEl: {
                'data-testid': 'widgets-createreport-close'
            }
        }
    ]
});
