
Ext.define('CMDBuildUI.view.widgets.createmodifycard.Panel', {
    extend: 'Ext.form.Panel',

    requires: [
        'CMDBuildUI.view.widgets.createmodifycard.PanelController',
        'CMDBuildUI.view.widgets.createmodifycard.PanelModel'
    ],

    mixins: [
        'CMDBuildUI.view.widgets.Mixin'
    ],

    alias: 'widget.widgets-createmodifycard-panel',
    controller: 'widgets-createmodifycard-panel',
    viewModel: {
        type: 'widgets-createmodifycard-panel'
    },


    /**
     * @cfg {String} ButtonLabel  
     * Label for the widgetButton
     */

    /**
     * @cfg {Boolean} theWidget.ReadOnly
     * If true disables create/modify
     */

    /**
     * @cfg {String} theWidget.ClassName
     * ClassName to select the card preset from
     */

    /**
     * @cfg {String} ObjId 
     * Card Id to modify
     */

    /**
     * @cfg {String} Reference
     * Attribute name reference of the card to modify 
     */

    fieldDefaults: CMDBuildUI.util.helper.FormHelper.fieldDefaults,

    modelValidation: true,
    autoScroll: true,

    tbar: [{
        xtype: 'button',
        text: CMDBuildUI.locales.Locales.classes.cards.addcard,
        reference: 'addcardbtn',
        itemId: 'addcardbtn',
        iconCls: 'x-fa fa-plus',
        ui: 'management-action',
        autoEl: {
            'data-testid': 'selection-popup-addcardbtn'
        },
        bind: {
            text: '{addbtn.text}',
            disabled: '{addbtn.disabled}',
            hidden: '{addbtn.hidden}'
        }
    }],

    fbar: [{
        xtype: 'button',
        ui: 'management-action',
        reference: 'savebtn',
        itemId: 'savebtn',
        text: CMDBuildUI.locales.Locales.common.actions.save,
        autoEl: {
            'data-testid': 'widgets-createmodifycard-save'
        },
        formBind: true,
        bind: {
            hidden: '{savebtn.hidden}'
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.common.actions.save'
        }
    }, {
        xtype: 'button',
        ui: 'secondary-action',
        reference: 'closebtn',
        itemId: 'closebtn',
        text: CMDBuildUI.locales.Locales.common.actions.close,
        autoEl: {
            'data-testid': 'widgets-createmodifycard-close'
        },
        localized: {
            text: 'CMDBuildUI.locales.Locales.common.actions.close'
        }
    }]
});
