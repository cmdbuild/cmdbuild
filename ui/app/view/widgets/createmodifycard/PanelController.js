Ext.define('CMDBuildUI.view.widgets.createmodifycard.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-createmodifycard-panel',
    mixins: [
        'CMDBuildUI.mixins.grids.GridControllerMixin'

    ],

    control: {
        '#': {
            beforerender: 'onBeforeRender'
        },
        '#addcardbtn': {
            beforerender: 'onAddCardBtnBeforeRender'
        },
        '#closebtn': {
            click: 'onCloseBtnClick'
        },
        '#savebtn': {
            click: 'onSaveBtnClick'
        }
    },


    /************************************************************************************************************** 
     *                                                                  
     *                                          WIDGET: CreateModifyCard
     *
     *
     * EVENTS:
     *  onBeforeRender              (view, eOpts)                          --> render view with selected object
     *  onAddCardBtnBeforeRender    (button, eOpts)                        --> manage addcard button with subclasses
     *  onCloseBtnClick             (button, e, eOpts)                     --> close popup
     *  onAddCardBtnClick           (item, event, eOpts)                   --> render view with new object
     *  onSaveBtnClick              (button, e, eOpts)                     --> save object if form is valid
     *                                                                  
     * UTILS:
     *  extractVariableFromString   (variable, theTarget)                 --> serialize parameteres
     *  fetchDataToView: function   (classname, creation,                 --> fetch the object to render
     *                              idhardware, vm, view, mode)
     *                                                                  
     * ************************************************************************************************************/


    /**
    * @param {CMDBuildUI.view.widgets.createmodifycard.PanelController} view
    * @param {Object} eOpts
    */
    onBeforeRender: function (view, eOpts) {
        var me = this;
        var vm = this.getViewModel();
        var theWidget = vm.get('theWidget');
        var theTarget = vm.get('theTarget');
        var readonly = theWidget.get("readonly") !== undefined ? theWidget.get("readonly") : false;
        var mode;
        if (readonly) {
            mode = CMDBuildUI.util.helper.FormHelper.formmodes.read;
        } else {
            mode = CMDBuildUI.util.helper.FormHelper.formmodes.update;
        }
        var objectId, objectTypeName;
        if (theWidget.get("ObjId") && theWidget.get("ClassName")) {
            var objId = theWidget.get("ObjId");
            var theIdObj = me.extractVariableFromString(objId, theTarget);
            objectId = Object.values(theIdObj).pop();
            objectTypeName = theWidget.get("ClassName");
        } else if (theWidget.get("Reference")) {
            var targetFieldName = me.getView().getOutput(); // TODO: will become theWidget.get("Reference")
            var refDefinition = theTarget.getField(targetFieldName);
            objectId = theTarget.get(targetFieldName);
            objectTypeName = refDefinition.metadata.targetClass;
        }
        if (objectId) {
            Ext.Ajax.request({
                url: Ext.String.format(
                    '{0}/classes/{1}/cards/{2}',
                    CMDBuildUI.util.Config.baseUrl,
                    objectTypeName,
                    objectId
                ),
                callback: function (options, success, response) {
                    var res = JSON.parse(response.responseText);
                    me.fetchDataToView(res.data._type, false, res.data._id, mode);
                }
            });
        }
    },

    /**
    * method to fetch data to the view
    * @param {CMDBuildUI.app.model.modelname} classname
    * @param {boolean} creation
    * @param {CMDBuildUI.app.model.id} idInstance
    * @param {CMDBuildUI.util.helper.FormHelper.formmodes} mode
    */

    fetchDataToView: function (classname, creation, idInstance, mode) {
        var vm = this.getViewModel();
        var view = this.getView();
        CMDBuildUI.util.helper.ModelHelper.getModel(
            CMDBuildUI.util.helper.ModelHelper.objecttypes.klass, // 'class'
            classname
        ).then(function (model) {
            if (!creation) {
                vm.linkTo("theObject", {
                    type: model.getName(),
                    id: idInstance
                });
            } else {
                vm.linkTo("theObject", {
                    type: model.getName(),
                    create: true
                });
            }
            var items = CMDBuildUI.util.helper.FormHelper.renderForm(model, {
                mode: mode,
                showAsFieldsets: true
            });
            view.add(items);
        });
    },

    /**
     * Resolve variable.
     * @param {String} variable
     * @param {CMDBuildUI.model.base.Base} theTarget 
     * @return {*} The variable resolved.
     */
    extractVariableFromString: function (variable, theTarget) {
        variable = variable.replace("{", "").replace("}", "");
        var s_variable = variable.split(":");
        if (s_variable[0] === "server") {
            return CMDBuildUI.util.ecql.Resolver.resolveServerVariables([s_variable[1]], theTarget);
        } else if (s_variable[0] === "client") {
            return CMDBuildUI.util.ecql.Resolver.resolveClientVariables([s_variable[1]], theTarget);
        } else {
            return variable;
        }
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Object} eOpts
     */
    onAddCardBtnBeforeRender: function (button, eOpts) {
        var vm = this.getViewModel();
        var theWidget = vm.get('theWidget');
        var classname = theWidget.get("ClassName");
        this.updateAddButton(button, "onAddCardBtnClick", classname);
    },

    /**
     * 
     * @param {Ext.menu.Item} item
     * @param {Ext.event.Event} event
     * @param {Object} eOpts
     */
    onAddCardBtnClick: function (item, event, eOpts) {
        var me = this;
        var vm = this.getViewModel();
        var theWidget = vm.get('theWidget');
        var readonly = theWidget.get("readonly") !== undefined ? theWidget.get("readonly") : false;
        var mode;
        if (readonly) {
            mode = CMDBuildUI.util.helper.FormHelper.formmodes.read;
        } else {
            mode = CMDBuildUI.util.helper.FormHelper.formmodes.create;
        }
        var view = this.getView();
        view.removeAll();
        var objectTypeName = item.config.objectTypeName;
        me.fetchDataToView(objectTypeName, true, null, mode);
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onCloseBtnClick: function (button, e, eOpts) {
        this.getViewModel().get("theObject").reject();
        this.getView().fireEvent("popupclose");
    },

    /**
     * @param {Ext.button.Button} button
     * @param {Event} e
     * @param {Object} eOpts
     */
    onSaveBtnClick: function (button, e, eOpts) {
        var form = this.getView();
        var vm = this.getViewModel();
        if (form.isValid()) {
            var theObject = vm.get('theObject');
            theObject.save({
                success: function (record, operation) {
                    var w = Ext.create('Ext.window.Toast', {
                        ui: 'administration',
                        title: 'Success!',
                        html: 'Object saved correctly.',
                        iconCls: 'x-fa fa-check-circle',
                        align: 'br'
                    });
                    w.show();
                }
            });
            this.getView().fireEvent("popupclose");
        } else {
            var validatorResult = vm.get('theObject').validate();
            var errors = validatorResult.items;
            for (var i = 0; i < errors.length; i++) {
                console.log('Key :' + errors[i].field + ' , Message :' + errors[i].msg);
            }
        }
    }
});
