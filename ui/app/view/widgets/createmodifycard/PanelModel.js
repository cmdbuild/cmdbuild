Ext.define('CMDBuildUI.view.widgets.createmodifycard.PanelModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-createmodifycard-panel',

    data: {
        savebtn: {
            hidden: true
        }
    },

    formulas: {
        updateSaveBtn: {
            bind: {
                theWidget: '{theWidget}'
            },
            get: function (data) {
                var readonly = data.theWidget.get("readonly") !== undefined ? data.theWidget.get("readonly") : false;
                this.set('savebtn.hidden', readonly);
            }
        }
    }

});
