Ext.define('CMDBuildUI.view.graph.GraphContainerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.graph-graphcontainer',

    control: {
        '#': {
            afterrender: 'onAfterRender',
            beforedestroy: 'onBeforeDestroy'
        },
        //view.graph.topMenu.TopMenu
        // '#refresh': {
        //     click: ''
        // }

        //graph-canvas-bottommenu-canvasmenu
        '#sliderLevel': {
            changecomplete: 'onSliderChangeComplete',
            change: 'onSliderChange',
            beforerender: 'onSliderBeforeRender'
        },
        '#sliderValue': {
            beforerender: 'onSliderValueBeforeRender'
        }
    },
    listen: {
        global: {
            sceneselectednode: 'onSceneSelectedNode',
            doubleclicknode: 'onDoubleClickNode'
        },
        store: {
            '#relationStore': {
                clear: 'onRelationStoreClear'
            }
        }
    },

    /**
     * Adds the canvas with the menu in the relation graph popup
     * @param container
     * @param eOpts
     */
    onAfterRender: function (container, eOpts) {
        var vm = container.getViewModel();
        vm.set('node', { //NOTE: change name
            _type: container._type, //class name
            _id: container._id, //card id
            _type_name: container._type_name //domain name
        });
    },

    /**
     * @param {Ext.Component} container
     * @param {Object} eOpts
     */
    onBeforeDestroy: function (container, eOpts) {
        CMDBuildUI.graph.threejs.SceneUtils.destroyScene();
    },

    /**
     * @param id
     * @param type
     * @param callback the callback function 
     * @returns the records related to that card
     */
    proxyNodeRelation: function (id, type, callback) {
        var me = this;
        var newStore = Ext.create('Ext.data.Store', {
            model: "CMDBuildUI.model.domains.Relation",
            proxy: {
                url: Ext.String.format("/classes/{0}/cards/{1}/relations", type, id),
                type: 'baseproxy'
            },
            autoLoad: true,
            autoDestroy: true,
        });
        newStore.load({
            callback: function (records, operation, success) {
                callback.call(me, id, type, records, operation, success);
            }
        });
    },

    /**
     * @param {[NodeDataStructure]} nodes
     * @param {Object} callback Function to launch when the loading of all the nodes is complete
     * callback.fn {Function}
     * callback.scope {Object}
     */
    nodeRelations: function (nodes, callback) {
        var l = nodes.length;

        /**
         * 
         * @param {*} id 
         * @param {*} type 
         * @param {[Ext.data.Model]} records 
         * @param {*} operation 
         * @param {*} success 
         */
        function ProxyNodeRelationCallback(id, type, records, operation, success) {
            --l;

            if (this.ignoreCallbackResponse == false) {
                this.fillCytoscape(id, type, records);
                this.fillMemoryNodeDataStructure(id, records);
            }

            if (l === 0) {
                console.log('loadingEnd');
                this.waitingCallbackResponse = false;
                if (this.ignoreCallbackResponse == true) {
                    this.ignoreCallbackResponse = false;
                    return;
                }
                callback.fn.call(callback.scope);
                this.fireEvent('acquisitionend', this.getView());
            }
        }

        nodes.forEach(function (node) {
            this.waitingCallbackResponse = true;
            this.proxyNodeRelation(node.getId(), node.getType(), ProxyNodeRelationCallback);
        }, this);
    },


    /**
     * @param {String} mode The defined mode
     * @param {Object} config
     * @type {[String]} config.nodes
     * @type {Number} config.depth
     * @type {object} config.callback
     *  config.callback: {
     *      fn: @type {Function},
     *      scope: @type {Object}
     *  }
     */
    graphRelation: function (mode, config) {
        var me = this;

        config = config || {};
        Ext.applyIf(config, {
            scope: me
        });

        var memoryNodeDataStructure = this.getViewModel().get('memoryNodeDataStructure');
        switch (mode) {
            // case 'childOfDepth':
            //     this.childOdDepth(config.depth, {
            //         fn: function () {
            //             var cy = this.getViewModel().get('cy');
            //             CMDBuildUI.graph.threejs.SceneUtils.fillScene(cy);

            //             /** */
            //             if (config.callback) {
            //                 config.callback.fn.call(config.callback.scope || this);
            //             }
            //         },
            //         scope: this
            //     });
            //     break;
            case 'childOfMultiDepth':
                //Ideal for the slider changes

                function recCallback() {
                    var lastDepth = memoryNodeDataStructure.getLastDepth();
                    if (lastDepth < config.depth && !memoryNodeDataStructure.isComplete()) {
                        this.childOfDepth(lastDepth, {
                            fn: recCallback,
                            scope: this
                        });
                    } else {
                        var cy = this.getViewModel().get('cy');
                        CMDBuildUI.graph.threejs.SceneUtils.fillScene(cy);

                        /**
                         * callback call of graphRelation
                         */
                        if (config.callback) {
                            config.callback.fn.call(
                                config.callback.scope || this,
                                config.callback.arguments.ids || null
                            );
                        }
                    }
                }

                var lastDepth = memoryNodeDataStructure.getLastDepth();
                if (lastDepth < config.depth) {
                    this.childOfDepth(lastDepth, {
                        fn: recCallback,
                        scope: this
                    });
                }
                break;
            case 'relationOf':
                //ideal for multiple nodes at unknow depth
                var tmpNodes = [];
                config.nodes.forEach(function (id) {
                    tmpNodes.push(memoryNodeDataStructure.getNode(id));
                }, this);

                this.nodeRelations(tmpNodes,
                    {
                        fn: function () {
                            var cy = this.getViewModel().get('cy');
                            CMDBuildUI.graph.threejs.SceneUtils.fillScene(cy);

                            /**
                             * callback call of graphRelation
                             */
                            if (config.callback) {
                                config.callback.fn.call(config.callback.scope || this, config.callback.arguments || null);
                            }
                        },
                        scope: this
                    });
                break;

            default:
                console.error('No mode found');
        }

    },

    /**
     * @param {Number} depth
     * @param {Object} callback
     * callback.fn {Function}
     * callback.scope {Object}
     */
    childOfDepth: function (depth, callback) {
        var memoryNodeDataStructure = this.getViewModel().get('memoryNodeDataStructure');
        var nodes = memoryNodeDataStructure.getNodesAtdepth(depth);
        this.nodeRelations(nodes, callback);
    },

    /**
     * @param {Number} id node id
     * @param {String} type node type
     * @param {Ext.data.Mode} records the node childs
     */
    fillCytoscape: function (id, type, records) {
        var cy = this.getViewModel().get('cy');
        var relationStore = this.getStore('relationStore');
        var cId, cType; //child id, child type

        records.forEach(function (record) {
            cId = record.get('_destinationId');
            cType = record.get('_destinationType');

            if (!this.isLoaded(cId, cType)) {
                /**
                 * Fill cytoscape noden and edges
                 */
                cy.add(this.cytoNode(record));
                cy.add(this.cytoEdge(record, id));

                /**
                 * fill the memory store;
                 */
                relationStore.insert(0, record);
            } else {
                if (cy.$('#' + record.get('_id')).length === 0) {
                    /** 
                     * Fill cytoscape edge
                    */
                    cy.add(this.cytoEdge(record, id));

                    /**
                    * fill the memory store;
                    */
                    relationStore.insert(0, record);
                }
            }
        }, this);
    },

    /**
     * This function fills the memoryNodedataStructure
     * @param {String} id
     * @param {Ext.data.Mode} records
     */
    fillMemoryNodeDataStructure: function (id, records) {
        var memoryNodeDataStructure = this.getViewModel().get('memoryNodeDataStructure');
        var parentNode = memoryNodeDataStructure.getNode(id);
        var childs = [];

        if (parentNode) {
            records.forEach(function (record) {
                childs.push({
                    id: record.get('destinationId'),
                    type: record.get('destinationType')
                });
            }, this);

            memoryNodeDataStructure.addChildren(parentNode, childs);
        }
    },

    isLoaded: function (id, type) {
        var cy = this.getViewModel().get('cy');
        var nd = cy.nodes('#' + id);

        switch (nd.length) {
            case 0:
                return false;
            case 1:
                return true;
            default:
                console.error("Can't be a different number of elements");
        }
    },

    /**
     * creates the node structure for the cytoscape node
     * @param {Ext.record} record the source of info to extract
     * @returns an addable node in the cytoscape graph
     */
    cytoNode: function (record) {
        return {
            group: 'nodes',
            data: this.mapNode_2_nodes(record)
        };
    },

    /**
     * @param {Ext.data.Model} record the source of info
     * @param {Number} id the parent data in the listNodeDataStructure
     * @returns an addable edge to the cytoscape graph
     */
    cytoEdge: function (record, id) {
        return {
            group: 'edges',
            data: this.mapNode_2_edges(record, id)
        };
    },

    /**
     * this function takes a record and transforms its'data. Used for cytoscape node transformation
     * @param {Object} record
     * @returns the .data field of an a cytoscape node 
     */
    mapNode_2_nodes: function (record) {
        return {
            id: record.get('_destinationId'),
            type: record.get('_destinationType')
        };
    },

    /**
     * this function takes a record and it's parent and transforms its'data. Used for cytoscape edges creation
     * @param {Ext.data.Model} record
     * @param {Number} parent
     * @returns the .data field of a cytoscape edge
     */
    mapNode_2_edges: function (record, id) {
        return {
            id: record.get('_id'),
            source: id,
            target: record.get('_destinationId')
        };
    },

    /**
     * @param {[Number]} ids an array of card id. The corrisponding cards are selected;
     */
    onSceneSelectedNode: function (ids) {
        this.getViewModel().set('selectedNode', null);

        var store = this.getViewModel().get('relationStore');
        var selectedNodes = [];

        ids.forEach(function (id) {
            var record = store.findRecord('_destinationId', id);
            var targetClass = record.get('_destinationType');
            selectedNodes.push({
                id: id,
                type: targetClass
            });
        }, this);

        /**
         * fill card tab TODO: Use the this.updateSelectionNode method
         */
        this.getViewModel().set('selectedNode', selectedNodes);
    },

    /**
     * Updates the information related to the bind card 
     * @param {[Object]}
     */
    updateSelectionNode: function (nodes) {
        nodes = nodes || this.getViewModel().get('selectedNode');
        this.getViewModel().set('selectedNode', null);
        this.getViewModel().set('selectedNode', nodes);
    },
    /**
     * @param {[Number]} ids contains the ids of the double clicked nodes
     */
    onDoubleClickNode: function (ids) {
        this.graphRelation('relationOf', {
            nodes: ids,
            callback: {
                fn: function () {
                    console.log('callback');
                    this.updateSelectionNode();
                },
                scope: this
            }
        });

    },
    //<-- Remove Node Part START-->
    /**
     *
     */
    resetEnvironment: function () {
        var selNode = this.getViewModel().get('selectedNode');
        if (selNode.length != 0) { //FIXME: vedere come gestire gli stati del selNode
            this.tmpSelectedNode = selNode[0];
        }

        var vm = this.getViewModel();
        var relationStore = vm.get('relationStore');
        relationStore.removeAll();


    },
    //<-- Remove Node Part END-->

    /**
     * @param {Ext.data.Store} relationStore
     * @param {Object} eOpts 
     */
    onRelationStoreClear: function (relationStore, eOpts) {
        //createsw the new momoryNodeDataStructure
        this.eraseNodeDataStructure();
        this.eraseCytoscape();
        this.eraseThreeJsScene();
        //empty NodeDataStructure


        this.getViewModel().set('node', {
            _type: this.tmpSelectedNode.type,
            _id: this.tmpSelectedNode.id,
            _type_name: null
        });
    },

    /**
     * 
     */
    eraseNodeDataStructure: function () {
        //deletes the old memoryNodeDataStructure
        this.getViewModel().set('memoryNodeDataStructure', null);
    },

    /**
     * 
     */
    eraseCytoscape: function () {
        this.getViewModel().set('cy', null);
    },

    /**
     * 
     */
    eraseThreeJsScene: function () {
        CMDBuildUI.graph.threejs.SceneUtils.destroyScene();
        var canvasView = this.getView().down('graph-canvas-canvaspanel');

        canvasView.setHtml('<div id="cy"></div>');
        var div = document.getElementById('cy');
        div.style.height = 'inherit';


    },

    //---------------------------------------------------------------------//
    //  graph-canvas-bottommenu-canvasmenu 

    /**
     * @param {Ext.Component} slider
     * @param {Object} eOpts
     */
    onSliderBeforeRender: function (slider, eOtps) {
        var value = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.relgraph.baselevel);
        var maxValue = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.relgraph.clusteringThreshold);
        //set slider values
        slider.setValue(value);
        slider.setMaxValue(maxValue);
    },

    /**
     * @param {Ext.slider.Multi} slider 
     * @param {Number} newValue
     * @param {Ext.slider.Thumb} thumb
     * @param {object} eOpts
     */
    onSliderChange: function (slider, newValue, thumb, eOpts) {
        var tbtext = this.getComponent('#sliderValue');
        tbtext.setHtml(newValue);
    },

    /**
     * @param {Ext.slider.Multi} slider 
     * @param {Number} newValue
     * @param {Ext.slider.Thumb} thumb
     * @param {object} eOpts
     */
    onSliderChangeComplete: function (slider, newValue, thumb, eOpts) {
        if (this.waitingCallbackResponse === true) {
            this.ignoreCallbackResponse = true;
        }
        this.resetEnvironment();
    },

    /**
     * @param {Ext.Component} tbtext
     * @param {Object} eOpts
     */
    onSliderValueBeforeRender: function (tbtext, eOpts) {
        var value = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.relgraph.baselevel);
        tbtext.setHtml(value);
    },

    /**
     * 
     * NodeDataStructure type
     * @param {String} id 
     * @param {String} type
     * @param {Object} _config
     */
    NodeDataStructure: function (id, type, _config) {
        this.id = id;
        this.type = type;
        this.children = [];
        /**
         * This field tells if this node has passed through the operation of child (relation) acquisition
         */
        this.childLoaded = false;
        /**
         * This field indicates at which depth this element is found
         */
        this.depth = null;

        this.init = function (conf) {
            if (typeof (conf.depth) === 'number') {
                this.depth = conf.depth;
            }

            if (conf.childLoaded === true) {
                this.childLoaded = true;
            }
        };

        this.getId = function () {
            return this.id;
        };

        this.getType = function () {
            return this.type;
        };

        this.getDepth = function () {
            return this.depth;
        };

        /**
         * Sets the node depth. If yet inizializied doesn't change the value
         * @param {Number} depth 
         */
        this.setDepth = function (depth) {
            if (typeof (this.depth) != 'number') {
                this.depth = depth;
            }
        };

        /**
         * This function changes the internal state of the value childLoaded 
         * settting that to true
         */
        this._setChildLoaded = function () {
            this.childLoaded = true;
        };

        /**
         * @returns {Boolean} the value of this.childLoaded
         */
        this._gtChildLoaded = function () {
            return this.childLoaded;
        };
        /**
         * Adds children to the curren NodeDataStructure
         * Sets depth and 
         * @param {[NodeDataStructure]} nodes 
         * @param {Object} config Configuration object
         */
        this.addChildren = function (nodes, config) {
            nodes.forEach(function (node) {
                this.children.push(node);

                node.setDepth(this.depth + 1);

                /**
                 * Fires the related afterNodeAdd function
                 * NOTE: This event could be fire only once saving the nodes in an array: tmpNodes.push(node) and handle an array of objects instead of the single node
                 */
                if (config.afterNodeAdd) {
                    config.afterNodeAdd.fn.call(config.afterNodeAdd.scope || this, node);
                }
            }, this);

            this._setChildLoaded();
        };

        /** */
        if (_config) {
            this.init(_config);
        }
    },

    /**
     * ListNodeDataStructure
     * @param {Object} _config the global scope 
     */

    ListNodeDataStructure: function (_config) {
        this.nodes = {};

        /**
         * This field have this style:
         * {
         *  0: {
         *      _meta: {
         *          complete: true    
         *      },
         *      nodes: {
         *          id1: NodeDataStructure,
         *          ...,
         *          idn: NodeDataStructure
         *      }
         *  },
         *  1: {...},
         *  ...,
         *  n: {...}
         * } 
         * 
         * _meta: Contains information about the set of nodes
         *  _meta.complete: Indicates if all the possible element that can be found at level n are in the set
         *  ES: 
         *      at level 0 all the elements that can be found in that level are present by default so depth[0]._meta.complete = true
         *      at level 1 can be found all the childs (suppose to be 2 elements) of the root so depth[1]._meta.complete = true
         *      at level 2 if i load only one element of the level 1 the depth[2]._meta.complete = false. After loading the other element yet not loaded and adding his child i can set depth[2]._meta.complete = true
         * 
         */
        this.depth = {};
        this.me = null;

        this._init = function (conf) {
            if (!conf.scope) {
                console.error('Define scope');
            } else {
                this.me = conf.scope;
            }

            if (!conf.root) {
                console.error('Define Root');
            } else {
                var tmpNodeDataStructure = new this.me.NodeDataStructure(conf.root._id, conf.root._type, { depth: 0 });
                //add the node in this.nodes
                this._saveNode(tmpNodeDataStructure);
                //sync the this.depth
                this.depth[0] = {
                    _meta: {
                        complete: true
                    },
                    nodes: {}
                };
                this.depth[0].nodes[conf.root._id] = tmpNodeDataStructure;
            }
        };

        /**
         * 
         * @param {[String]} ids array of ids
         * @returns {[NodeDataStructure]}
         */
        this.getNodes = function (ids) {
            var tmpNodes = [];
            var tmpNode;

            ids.forEach(function (id) {
                tmpNode = this.nodes[id];
                if (tmpNode) {
                    tmpNodes.push(tmpNode);
                } else {
                    console.error('cant find that node');
                }
            }, this);

            return tmpNodes;
        };

        /**
         * 
         * @param {Number} id 
         * @returns {NodeDataStructure} the node with that id
         */
        this.getNode = function (id) {
            var node = this.nodes[id];

            if (node) {
                return node;
            } else {
                return null;
            }
        };

        /**
         * Adds nodes in the this.nodes object
         * @param {NodeDataStructure} node 
         */
        this._saveNode = function (node) {
            var id = node.getId();
            if (!this.nodes[id]) {
                this.nodes[id] = node;
            } else {
                console.error('Duplicated Id');
            }
        };

        /**
         * 
         * @param {NodeDataStructure} parentNode 
         * @param {Object} childNodes 
         * childNodes.id
         * childNodes.type
         */
        this.addChildren = function (parentNode, childNodes) {
            var tmpAdds = [];
            var tmpNode;

            childNodes.forEach(function (childNode) {
                tmpNode = this.getNode(childNode.id);
                if (!tmpNode) {
                    tmpNode = new this.me.NodeDataStructure(childNode.id, childNode.type);
                    this._saveNode(tmpNode);
                }
                tmpAdds.push(tmpNode);
            }, this);


            parentNode.addChildren(tmpAdds, {
                /**
                 * This function modifies the this.depth list
                 * @param {DataNodeStructure} node
                 */
                afterNodeAdd: {
                    fn: function (node) {
                        var depth = node.getDepth();

                        if (!this.depth[depth]) {
                            this.depth[depth] = {
                                _meta: {
                                    complete: false
                                },
                                nodes: {}
                            };
                        }

                        this.depth[depth].nodes[node.getId()] = node;
                    },
                    scope: this
                }
            });
        };

        /**
         * @param {Number} depth
         * @returns {[NodeDataStructure]}
         */
        this.getNodesAtdepth = function (depth) {
            if (!this.depth[depth]) {
                console.error('No elements at this depth');
            }

            var tmpIds = [];
            for (var id in this.depth[depth].nodes) {
                tmpIds.push(id);
            }
            return this.getNodes(tmpIds);
        };


        /**
         * This function returns the last depth at wich the _meta.complete = true
         * Each time this function is called the values are recalculated in a efficient way
         * @returns {Number} the last complete depth load
         */
        this.getLastDepth = function () {
            var maxDepth = this._getMaxDepth();
            var lastComplete = maxDepth;

            while (this.depth[lastComplete]._meta.complete !== true) {
                lastComplete--;
            }

            /**
             * lastComplete have always the .complete field = true
             */
            while (lastComplete != maxDepth) {
                if (this._areAllNodesLoaded(lastComplete)) {
                    lastComplete++;
                    this.depth[lastComplete]._meta.complete = true;
                } else {
                    return lastComplete;
                }
            }

            return lastComplete;
        };

        /**
         * @param {Number} depth
         * @returns {Boolean} true if all the nodes in that depth have NodeDateStructure.childLoaded = true; False otherwise
         */
        this._areAllNodesLoaded = function (depth) {
            var tmpNode;

            for (var id in this.depth[depth].nodes) {
                tmpNode = this.depth[depth].nodes[id];
                if (tmpNode._gtChildLoaded() === false) {
                    return false;
                }
            }

            return true;
        };

        /**
         * @returns {Number} the maximum depth reached
         */
        this._getMaxDepth = function () {
            var maxDepth = 0;

            for (var dh in this.depth) {
                if (parseInt(dh) > parseInt(maxDepth)) {
                    maxDepth = dh;
                }
            }

            return maxDepth;
        };

        this.isComplete = function () {
            var maxDepth = this._getMaxDepth();
            if (this._areAllNodesLoaded(maxDepth)) {
                return true;
            }

            return false;
        };

        /**
         * Start configuration
         */
        this._init(_config);
    },
    privates: {

        /**
         * @type {Boolean}
         * This field invalidates the incoming callback response
         */
        ignoreCallbackResponse: false,

        /**
         * @type {Boolean}
         * This field indicates if we are waiting for a callback response
         */
        waitingCallbackResponse: false,

        /**
         * @param {String} name
         */
        getComponent: function (name) {
            var view = this.getView();
            var elem = view.down(name);
            return elem;
        }
    }
});