Ext.define('CMDBuildUI.view.graph.topMenu.TopMenuController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.graph-topmenu-topmenu',
    listen: {
        component: {
            '#refresh': {
                click: 'onRefreshButtonClick'
            }
        }
    },

    // TODO: move to GraphContainerController
    /**
     * @param {Ext.panel.Tool} tool
     * @param {Ext.event.Event} e
     * @param {Ext.Component} owner
     * @param {Object} eOpts
     */
    onRefreshButtonClick: function (tool, e, owner, eOpts) {
        var selNode = this.getViewModel().get('selectedNode');
        var tmpSelectedNode = {};
        Ext.apply(tmpSelectedNode, selNode);

        this.graphResetEnvironment(tmpSelectedNode);
    },

    /**
     * @param {[Object]} tmpSelectedNode 
     * {    id: id,
     *      type: targetClass
     * }
     */
    graphResetEnvironment: function (tmpSelectedNode) {
        var graphView = this.getView().up('graph-graphcontainer');
        var graphController = graphView.getController();

        graphController.resetEnvironment(tmpSelectedNode);
    }

});
