
Ext.define('CMDBuildUI.view.graph.topMenu.TopMenu', {
    extend: 'Ext.toolbar.Toolbar',

    requires: [
        'CMDBuildUI.view.graph.topMenu.TopMenuController',
        'CMDBuildUI.view.graph.topMenu.TopMenuModel'
    ],

    controller: 'graph-topmenu-topmenu',
    viewModel: {
        type: 'graph-topmenu-topmenu'
    },
    alias: 'widget.view.graph.topMenu.TopMenu',

    items: [{
        iconCls: 'x-fa fa-refresh',
        tooltip: CMDBuildUI.locales.Locales.relationGraph.refresh,
        localize: {
            tooltip: 'CMDBuildUI.locales.Locales.relationGraph.refresh'
        },
        cls: 'management-tool',
        xtype: 'tool',
        itemId: 'refresh'
    }]
});
