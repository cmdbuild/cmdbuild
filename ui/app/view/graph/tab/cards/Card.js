
Ext.define('CMDBuildUI.view.graph.tab.cards.Card', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.graph.tab.cards.CardController',
        'CMDBuildUI.view.graph.tab.cards.CardModel'
    ],

    alias: 'widget.graph-tab-cards-card',
    controller: 'graph-tab-cards-card',
    viewModel: {
        type: 'graph-tab-cards-card'
    },
    autoScroll: true,
    items: [{
        xtype: 'classes-cards-card-view',
        reference: 'classes-cards-card-view',
        shownInPopup: true,
        hideTools: true,
        bind: {
            objectTypeName: '{name}',
            objectId: '{id}'
        }
    }]

});
