Ext.define('CMDBuildUI.view.graph.tab.cards.ListCardController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.graph-tab-cards-listcard',
    listen: {
        component: {
            '#': {
                selectionchange: 'onSelectionChange'
            }
        },
        store: {
            '#relationStore': {
                add: 'onRelationStoreAdd',
                clear: 'onRelationStoreClear'
            }
        }
    },

    /**
     * This function handles the selection from GRID -> CANVAS
     * @param {Ext.selection.Model} selectionModel
     * @param {[Ext.data.Model]} selected
     * @param {Object} eOpts
     */
    onSelectionChange: function (selectionModel, selected, eOpts) {
        var ids = [];
        selected.forEach(function (select) {
            ids.push(select.get('_destinationId'));
        }, this);

        CMDBuildUI.graph.threejs.SceneUtils.setSelectedNode(ids, false);
    },

    /**
     * This function handles this store
     * @param {Ext.data.store} relationStore 
     * @param {Ext.data.Model} records 
     * @param {Number} index
     * @param {Object} eOpts 
     */
    onRelationStoreAdd: function (store, records, index, eOpt) {
        var st = this.getViewModel().get('listCardStore');
        var foundRecord;

        records.forEach(function (record) {
            foundRecord = st.findRecord('_destinationId', record.get('_destinationId'));
            if(!foundRecord) {
                st.insert(0,record);
            }
        }, this);

    },

    /**
     * @param {Ext.data.Store} relationStore
     * @param {Object} eOpts 
     */
    onRelationStoreClear: function (relationStore, eOpts) {
        var vm = this.getViewModel();
        var store = vm.get('listCardStore');

        store.removeAll();
    }
});
