
Ext.define('CMDBuildUI.view.graph.tab.cards.ListClass', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.graph.tab.cards.ListClassController',
        'CMDBuildUI.view.graph.tab.cards.ListClassModel'
    ],
    alias: 'widget.graph-tab-cards-listclass',
    controller: 'graph-tab-cards-listclass',
    viewModel: {
        type: 'graph-tab-cards-listclass'
    },
    columns: [{
        text: CMDBuildUI.locales.Locales.relationGraph.class,
        localize: {
            text: 'CMDBuildUI.locales.Locales.relationGraph.class'
        },
        dataIndex: 'classTarget',
        align: 'left',
        flex: 1
    }, {
        text: CMDBuildUI.locales.Locales.relationGraph.qt,
        localize: {
            text: 'CMDBuildUI.locales.Locales.relationGraph.qt'
        },
        dataIndex: 'qt',
        align: 'left',
        flex: 1
    }],

    bind: {
        store: '{listClassStore}'
    }
});
