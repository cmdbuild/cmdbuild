
Ext.define('CMDBuildUI.view.graph.tab.cards.Relations', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.graph.tab.cards.RelationsController',
        'CMDBuildUI.view.graph.tab.cards.RelationsModel'
    ],
    alias: 'widget.graph-tab-cards-relations',
    controller: 'graph-tab-cards-relations',
    viewModel: {
        type: 'graph-tab-cards-relations'
    },
    layout: 'fit',
    columns: [{
        text: CMDBuildUI.locales.Locales.relationGraph.relation,
        localize: {
            text: 'CMDBuildUI.locales.Locales.relationGraph.relation'
        },
        dataIndex: '_destinationType',
        align: 'left',
        flex: 1
    }, {
        text: CMDBuildUI.locales.Locales.relationGraph.card,
        localize: {
            text: 'CMDBuildUI.locales.Locales.relationGraph.card'
        },
        dataIndex: '_destinationDescription',
        align: 'left',
        flex: 1
    }],

    bind: {
        store: '{edgesRelationStore}'
    }
});
