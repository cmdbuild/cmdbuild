Ext.define('CMDBuildUI.view.graph.tab.cards.RelationsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.graph-tab-cards-relations',
    listen: {
        store: {
            '#relationStore': {
                clear: 'onRelationStoreClear'
            }
        }
    },

    /**
     * @param {Ext.data.Store} relationStore
     * @param {Object} eOpts 
     */
    onRelationStoreClear: function (relationStore, eOpts) {
        var vm = this.getViewModel();
        var store = vm.get('edgesRelationStore');
        store.removeAll();

        this.setTitle();
        this.disableCard();
    },

    /**
     * sets the tab title using the bind
     */
    setTitle: function () {
        var containerViewModel = this.getView().up('graph-tab-tabpanel').getViewModel();
        containerViewModel.set('relationLenghtValue', 0);
    },

    /**
     * disable relation tab
     */
    disableCard: function () {
        var containerViewModel = this.getView().up('graph-tab-tabpanel');
        var relationTab = containerViewModel.lookupReference('graph-tab-cards-relations');

        relationTab.setDisabled(true);
    }
});
