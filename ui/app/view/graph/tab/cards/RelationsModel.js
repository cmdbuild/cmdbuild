Ext.define('CMDBuildUI.view.graph.tab.cards.RelationsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.graph-tab-cards-relations',
    data: {
        name: 'CMDBuildUI'
    },
    formulas: {
        updateStore: {
            bind: {
                nodeId: '{selectedNode}'
            },
            get: function (data) {
                var nodeId = data.nodeId;
                if (!nodeId || nodeId.length === 0) return;
                nodeId = nodeId[0].id;

                var cy = this.get('cy');
                var gridStore = this.getView().getStore();
                gridStore.removeAll();

                var store = this.get('relationStore');
                var edges = cy.$('#' + nodeId).connectedEdges().toArray();

                edges.forEach(function (edge) {
                    var record = store.findRecord('_id', edge.id());
                    if (record) {
                        /**
                         * fills the firs element in relation store
                         */
                        var newRecord = Ext.create('CMDBuildUI.model.domains.Relation', {
                            _destinationType: record.get('_type'), //class
                            _destinationDescription: record.get('_destinationDescription'),
                            //_type: node._type_name//domain
                        });
                        gridStore.insert(0, newRecord);
                    }
                }, this);
                console.log(nodeId);

                var containerViewModel = this.getView().up('graph-tab-tabpanel').getViewModel();
                containerViewModel.set('relationLenghtValue', edges.length);
            }
        }
    },
    stores: {
        edgesRelationStore: {
            model: 'CMDBuildUI.model.domains.Relation',
            autoDestroy: true,
            proxy: {
                type: 'memory'
            },
            // autoLoad: true
        }
    }
});
