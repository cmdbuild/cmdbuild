
Ext.define('CMDBuildUI.view.graph.tab.cards.ListCard', {
    extend: 'Ext.grid.Panel',

    requires: [
        'CMDBuildUI.view.graph.tab.cards.ListCardController',
        'CMDBuildUI.view.graph.tab.cards.ListCardModel'
    ],

    alias: 'widget.graph-tab-cards-listcard',
    controller: 'graph-tab-cards-listcard',
    viewModel: {
        type: 'graph-tab-cards-listcard'
    },
    layout: 'fit',
    multiSelect: true,
    bind: {
        store: '{listCardStore}'
    },
    columns: [{
        text: CMDBuildUI.locales.Locales.relationGraph.card,
        localize: {
            text: 'CMDBuildUI.locales.Locales.relationGraph.card'
        },
        dataIndex: '_destinationDescription',
        align: 'left',
        flex: 1

    }, {
        text: CMDBuildUI.locales.Locales.relationGraph.class,
        localize: {
            text: 'CMDBuildUI.locales.Locales.relationGraph.class'
        },
        dataIndex: '_destinationType',
        align: 'left',
        flex: 1
    }]
});
