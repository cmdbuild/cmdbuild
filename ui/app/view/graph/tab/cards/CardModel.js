Ext.define('CMDBuildUI.view.graph.tab.cards.CardModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.graph-tab-cards-card',
    data: {
        name: 'CMDBuildUI'
    },
    formulas: {
        updateCardBind: {
            bind: {
                selectedNode: '{selectedNode}'
            },
            get: function (data) {
                var selectedNode = data.selectedNode;
                if (!selectedNode || selectedNode.length === 0) return;

                this.set('id', selectedNode[0].id);
                this.set('name', selectedNode[0].type);

            }
        }
    }

});
