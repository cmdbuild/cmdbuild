Ext.define('CMDBuildUI.view.map.ContainerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.map-container',

    data: {
        checkLayers: {},
        checkNavigationTree: {},
        toRemove: [],
        toAdd: null,
        bbox: null,
        bboxExpanded: [0, 0, 0, 0],
        zoomDisabled: false,

        // selected feature
        selectedFeature: null,
        selectedFeatureNotFound: null,

        //geoAttributes of selected row
        geoDataRow: undefined,
        // stores config
        geoattributesStoreConfig: {
            autoload: false
        },
        externalLayerStoreConfig: {
            autoload: false
        }
    },

    formulas: {
        /**
         * Update stores configuration after objectTypeName updates
         */
        onUpdateObjectTypeName: {
            bind: '{objectTypeName}',
            get: function (objectTypeName) {
                if (objectTypeName) {
                    // set geoattributes store config
                    this.set("geoattributesStoreConfig.proxyurl", CMDBuildUI.util.api.Classes.getGeoAttributes(objectTypeName));
                    this.set("geoattributesStoreConfig.autoload", true);
                    // set external layers store config
                    this.set("externalLayerStoreConfig.proxyurl", CMDBuildUI.util.api.Classes.getExternalGeoAttributes(objectTypeName));
                    this.set("externalLayerStoreConfig.autoload", true);
                }
            }
        },

        /**
         * Update externalLayerStore data based on map.ExternalLayerExtends store data.
         */
        extentDataExternalLayerStore: {
            bind: {
                externalLayerStoreCount: '{externalLayerStore.totalCount}'
            },
            get: function (data) {
                if (data.externalLayerStoreCount) {
                    var records = Ext.getStore("map.ExternalLayerExtends").getRange();
                    var externalLayerStore = this.get("externalLayerStore");

                    for (var i = 0; i < records.length; i++) {
                        var name = records[i].get('Name');
                        var lrecord = externalLayerStore.findRecord('name', name);
                        if (lrecord != null) {
                            lrecord.set('extent', [records[i].get('minx'), records[i].get('miny'), records[i].get('maxx'), records[i].get('maxy')]);
                            lrecord.set('CRS', records[i].get('CRS'));
                        }
                    }
                }
            }
        },

        /**
         * Update selected feature
         */
        selectedRowChange: {
            bind: {
                selectedRow: '{selectedRow}'
            },

            get: function (data) {

                if (data.selectedRow == null) {
                    return;
                } else /* if (ft == null  || ft.get('data')._owner_id != data.selectedRow.get('_id') ) */ {
                    Ext.GlobalEvents.fireEventArgs('selectedrowchanged',data.selectedRow);
                    Ext.Ajax.request({
                        url: Ext.String.format(CMDBuildUI.util.Config.baseUrl + '/classes/{0}/cards/{1}/geovalues', this.get('objectTypeName'), data.selectedRow.get('_id')),
                        method: 'GET',

                        success: function (response, opts) {
                            response = JSON.parse(response.responseText);
                            var map = this.getView().lookupReference("map");
                            if (response.data.length == 0) { //TODO: verify if the response can be null or even an empty array
                                this.set('selectedFeature', null);
                                this.set('selectedFeatureNotFound', null);
                                this.set('geoDataRow', null);
                                map.fireEvent("clearfeaturesselection", map);
                            } else {
                                this.set('geoDataRow', response.data);
                                map.fireEvent("clearfeaturesselection", map);
                                map.fireEvent("newrowwithfeatureselected", map, response.data);
                            }
                        },
                        failure: function (response, opts) {
                            CMDBuildUI.util.Logger.log('server-side failure with status code ' + response.status, 'error');
                        },
                        scope: this
                    });
                }
            }
        },

        bboxChanges: {
            bind: {
                bbox: '{bbox}'
            },
            get: function (data) {
                if (data.bbox != null) {
                    var navTreeView = this.getView().lookupReference('tab-panel').lookupReference('map-tab-cards-navigationtree');
                    if (navTreeView) {
                        navTreeView.fireEvent('bboxchanged', data.bbox);
                    }
                }
            }
        }
    },


    stores: {
        /**
         * Geo attributes store
         */
        geoattributes: {
            model: 'CMDBuildUI.model.map.GeoAttribute',
            proxy: {
                type: 'baseproxy',
                url: '{geoattributesStoreConfig.proxyurl}',
                extraParams: {
                    visible: true,
                    limit: 9999
                }
            },
            autoLoad: '{geoattributesStoreConfig.autoload}'
        },
        /**
         * External layers store
         */
        externalLayerStore: {
            model: 'CMDBuildUI.model.map.GeoExternalLayer',
            proxy: {
                type: 'baseproxy',
                url: '{externalLayerStoreConfig.proxyurl}',
                extraParams: {
                    visible: true
                }
            },
            autoLoad: '{externalLayerStoreConfig.autoload}'
        }
    },

    links: {
        theTree: {
            type: 'CMDBuildUI.model.map.navigation.NavigationTreeDescription',
            id: 'gisnavigation'
        }
    }
});
