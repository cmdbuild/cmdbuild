Ext.define('CMDBuildUI.view.map.ContainerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.map-container',
    control: {
        '#': {
            afterrender: 'onAfterRender'
        }
    },

    onAfterRender: function () {
        /**
         * Geoserver
         */
        var store = Ext.getStore("map.ExternalLayerExtends");
        var geoserverEnabled = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.gis.geoserverEnabled);
        if (geoserverEnabled && !store.isLoaded()) {
            store.load(/* {
                callback : function(){
                    this.getViewModel().set('ExternalLayerExtendsLoaded',true);
                },
                scope: this
            } */);
        }

        /**
         * Bim Projects
         */
        var bimStore = Ext.getStore("bim.Projects");
        var bimEnabled = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.bim.enabled);

        if (bimEnabled) {
            bimStore.load({
                callback: function () {
                    
                }
            });
        }
    }

});
