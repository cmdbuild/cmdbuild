
Ext.define('CMDBuildUI.view.map.Container', {
    extend: 'Ext.panel.Panel',

    requires: [
        'CMDBuildUI.view.map.ContainerController',
        'CMDBuildUI.view.map.ContainerModel'
    ],

    alias: 'widget.map-container',
    controller: 'map-container',
    viewModel: {
        type: 'map-container'
    },

    layout: 'border',

    padding: '0',

    items: [
        {
            xtype: 'map-map',
            region: 'center',
            split: true,
            reference: 'map'
        }, {
            xtype: 'map-tab-tabpanel',
            title: 'Cards Menu',
            region: 'east',
            split: true,
            reference: 'tab-panel',
            width: '30%',
            collapsible: true,
            layout: 'container'
        }
    ]

    /**
     * This event is fired when the selected row changes.
     * @event selectedrowchanged 
     * @param selectedRow the new selected row
     */

});
