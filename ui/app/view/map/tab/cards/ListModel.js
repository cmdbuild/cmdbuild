Ext.define('CMDBuildUI.view.map.tab.cards.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.map-tab-cards-list',


    formulas: {
        setSelectedRow: {
            bind: '{selectedRow}',
            get: function (selectedRow) {
                this.getView().setSelection(selectedRow);
            }
        },
        selectedFeatureChanged: {
            bind: '{selectedFeature}',
            get: function (selectedFeature) {
                this.getView().fireEvent('selectedfeaturechange', selectedFeature);
            }
        }
    }

});
