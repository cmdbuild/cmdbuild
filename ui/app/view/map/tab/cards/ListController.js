Ext.define('CMDBuildUI.view.map.tab.cards.ListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.map-tab-cards-list',

    control: {
        '#': {
            beforerender: 'onBeforeRender',
            select: 'onSelect',
            selectedfeaturechange: 'findRowFromFeature'
        }
    },

    /**
      * @param {CMDBuildUI.view.classes.cards.Grid} view
      * @param {Object} eOpts
      */
    onBeforeRender: function (view,eOpts) {
        var vm = this.getViewModel();
        var objectTypeName = view.getObjectTypeName();
        if (!objectTypeName) {
            objectTypeName = vm.get("objectTypeName");
        }
        CMDBuildUI.util.helper.ModelHelper.getModel("class", objectTypeName).then(function (model) {
            var columns = [];
            view.reconfigure(null, CMDBuildUI.util.helper.GridHelper.getColumns(model.getFields(), {
                allowFilter: view.getAllowFilter()
            }));
        });
    },
    /**
     * @param r Ext.selection.RowModel
     * @param record Ext.data.Model The selected record
     * @param index the index of the row
     * @param eOpts
     */
    onSelect: function (r, record, index, eOpts) {
        var vm = this.getViewModel();
        vm.getParent().getParent().getParent().set('selectedRow', record);
        //http://localhost:8a90400080/cmdbuild/services/rest/v3/classes/Building/geocards/281879/

    },

    /**
     * This method sets the the selection in the grid tabs of the selected feature
     * @param {array} featrure the feature clicked in maps
     */
    findRowFromFeature: function (feature) {
        if (feature != null) {
            var vm = this.getViewModel();
            var store = vm.get('cards');
            var _id = feature.get('data')._owner_id;
            var record = store.findRecord('_id', _id);

            if (record == null) {
                Ext.Ajax.request({
                    url: Ext.String.format(CMDBuildUI.util.Config.baseUrl + '/classes/{0}/cards', feature.get('data')._owner_type),
                    method: 'GET',
                    params: {
                        positionOf: feature.get('data')._owner_id
                    },

                    success: function (response, opts) {
                        response = JSON.parse(response.responseText);
                        var id = response.data[0]._id;
                        var position = response.meta.positions.id;
                        var page = Math.ceil(position / store.getPageSize());
                        store.prefetchPage(page, {
                            callback: function () {
                                var record = store.findRecord('_id', _id);
                                vm.getParent().getParent().getParent().set('selectedRow', record);
                            },
                            scope: this
                        });
                    },

                    failure: function (response, opts) {
                        CMDBuildUI.util.Logger.log('server-side failure','error',response.status);
                    },
                    scope: this
                });
            } else {
                vm.getParent().getParent().getParent().set('selectedRow', record);
            }
        }
    }
});
