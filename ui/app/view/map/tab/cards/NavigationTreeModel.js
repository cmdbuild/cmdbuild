Ext.define('CMDBuildUI.view.map.tab.cards.NavigationTreeModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.map-tab-cards-navigationtree',

    formulas: {
        openNavigationTreePath: {
            bind: {
                row: '{selectedRow}'
            },
            get: function (data) {
                var store = this.getView().getStore();

                //close the old selection
                var selection = this.getView().getSelection()[0];
                while (selection != null && selection.get('_type') != '_root') {

                    selection.collapse();
                    selection = selection.parentNode;
                }

                if (data.row == null) return;
                
                this.getView().fireEvent('opennavigationtrepath', data.row);

                //set selection in navigation tree TODO: dont put this part of code here. because we still wait for callback when enter here

            }
        },
        loadedNodesInitialize: {
            bind: {
                class: '{objectTypeName}'
            },
            get: function (data) {
                this.getView().getController()._loadedNodes = {
                    root: {
                        text: 'Root',
                        checked: 'true',
                        children: [],
                        expanded: true,
                        _type: '_root',
                        _id: '_root_id',
                        id: 'root'
                    }
                };
            }
        }
    },
    stores: {
        navigationTreeProxyStore: {
            model: 'CMDBuildUI.model.map.navigation.NavigationTree',
            proxy: {
                url: '/classes/_ANY/cards/_ANY/geovalues',
                type: 'baseproxy',
                extraParams: {
                    attach_nav_tree: true
                },
                reader: {
                    type: 'json',
                    rootProperty: 'meta.nav_tree_items'
                }
            }
        }
    }
});
