/**
 * When records are loaded in the navigation tree:
 *  1- when layerlistchanged is fired
 *  2- when expand button of a record is clicked and there are childs available
 *  3- when the selectedRow changes and the tree is filled
 *  4- when a feature on map is clicked and we expand the path to that record representing the feature  
 * 
 */
Ext.define('CMDBuildUI.view.map.tab.cards.NavigationTreeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.map-tab-cards-navigationtree',
    control: {
        '#': {
            render: 'onBeforeRender',
            treedomainsloading: 'onDomainTreeLoaded',
            checkchange: 'onCheckChange',
            opennavigationtrepath: 'openNavigationTreePath',
            layerlistchanged: 'onLayerListChanged',
            bboxchanged: 'onBboxChange'

        }
    },
    listen: {
        store: {
            '#navigationTreeStore': {
                nodeexpand: 'onExpandNode'
            }
        }
    },
    /**
     * @param view the view
     * @param eOpts
     */
    onBeforeRender: function (view, eOpts) {
        _this = this;
        var vm = this.getViewModel();

        vm.bind({
            bindTo: '{theTree}'
        }, function () {
            this.getView().fireEvent('treedomainsloading', this, vm.get("theTree"));
        }, this);

    },
    /**
     * This function is used to fill the entire root livel
     * @param tree the treeDomain store loades
     */
    onDomainTreeLoaded: function (tree) {
        this._loading = true;
        this._firsExpand.firsExpandWasMade = true;
        this.onExpandNode(
            this.getView().getStore().getRoot()
        );
    },

    /**
     * This function loads the navigation tree from the server
     * @param bbox the coordinates [minx, miny, maxx,maxy] representing the coordinates of the view
     */
    onBboxChange: function (bbox) {
        if (!this._loading) {
            var bboxExpanded = this.getViewModel().get('bboxExpanded');

            if (ol.extent.containsExtent(bboxExpanded, bbox)) {
                return;
            }

            bboxExpanded = this.calculateExpandedBbox(bbox);
            this.getViewModel().getParent().getParent().set('bboxExpanded', bboxExpanded);

            var layerList = this.getViewModel().getParent().getParent().getView().lookupReference('map').getLayerList();


            var bool = this.appearInDomainTree(layerList);
            if (bool) {
                this.buildNavigationTree(layerList, bboxExpanded);
            } else {
                console.log("Thers no layer available here");
            }
        }
    },
    /**
     * this function arranges the input and calls buildNavigationTree
     * @param layerList the list of layers saved on map
     * @param olMap the openlayer Map
     */
    onLayerListChanged: function (layerList, olMap) {
        if (!this._loading) {
            var bbox = olMap.getView().calculateExtent(olMap.getSize());
            var bboxExpanded = this.getViewModel().get('bboxExpanded', bboxExpanded);

            if (!ol.extent.containsExtent(bboxExpanded, bbox)) {
                bboxExpanded = this.calculateExpandedBbox(bbox);
                this.getViewModel().getParent().getParent().set('bboxExpanded', bboxExpanded);
            }

            var bool = this.appearInDomainTree(layerList);

            if (bool) {
                this.buildNavigationTree(layerList, bboxExpanded);
            }
        }
    },
    /**
     * this function tells if thers an element in array that appears in a second array
     * @param {[ol.vector]} layerList the array of elements to look 
     * @returns true if an element in layersList appears in DomainTree
     */
    appearInDomainTree: function (layerList) {
        var bool = false;
        for (var el in layerList) { //PERFORMANCE: Exit from the for loop as soon as bool becomes true;
            var tmpEl = layerList[el];
            if (this.isInNavTree(tmpEl.get('owner_type'))) {
                bool = true;
            }
        }
        return bool;
    },

    /**
     * This function is fired when the navigation tree needs to be recalculated
     * due to new layers added on the map
     * NOTE:
     * This function is critical in terms of time. The aproach used is very fast. The use of appendChild() slows all the process.
     * @param layerList the list of the layer on the olMap either visible or not (enabled or disabled by the tab-layer checkBox)
     * @param bbox the openlayer map
     */
    buildNavigationTree: function (layerList, bbox) {
        var validLayers = [];

        for (var els in layerList) {
            var el = layerList[els];
            if (el.type === 'VECTOR') {
                validLayers.push(els);
            }
        }

        //get's the store
        var store = this.getViewModel().get('navigationTreeProxyStore');

        //loads the store
        this._loading = true;

        store.load({
            params: {
                attribute: validLayers,
                area: this.stringfyBbox(bbox)
            },
            callback: function (records, operation, success) {
                if (!records) {
                    CMDBuildUI.util.Logger.log('unable to read elements from the store', 'error');
                    return;
                }

                var checkNavigationTree = this.getViewModel().get('checkNavigationTree');
                var navStore = this.getView().getStore();
                var bool = false;
                var resetRoot = false;

                records.forEach(function (record) {
                    if (this._loadedNodes[record.id] == null) {
                        resetRoot = true;
                        //this._loadedNodes[rec]
                        var nodeAppending;
                        // look for his parent record
                        var parentId = record.get('parentid');
                        if (parentId == 0) { //the parent is the root
                            nodeAppending = this._loadedNodes._root_id;;
                        } else { //the parent is not the root
                            nodeAppending = this._loadedNodes[parentId]; //? this._loadedNodes[parentId] : navStore.getNodeById(parentId);
                            /** assertion. The parent id is yet in the store **/
                            if (!nodeAppending) {
                                CMDBuildUI.util.Logger.log('Huge error. Parent should ever be in the store before the child', 'error');
                            }

                        }
                        //this._loadedNodes[record.id] = nodeAppending.appendChild(this.fromRecordToChildNode(record, { parentRecord: nodeAppending }), true);

                        this._loadedNodes[record.id] = this.fromRecordToChildNodeVersion2(record, checkNavigationTree, nodeAppending);

                        if (!nodeAppending.children) {
                            nodeAppending.children = [];
                        }

                        nodeAppending.children.push(this._loadedNodes[record.id]);

                        if (this._loadedNodes[record.id].checked == false) {
                            this.manageChecknavigationTreeModification(record, false, checkNavigationTree);
                            bool = true;
                        }
                    }
                }, this);

                // if (resetRoot) { //TODO: 
                //     this.preserveCheckState(this._loadedNodes, checkNavigationTree);
                //     navStore.setRoot(Ext.clone(this._loadedNodes.root)); //FIXME:save the value of checks
                // }

                this._loading = false;
                if (bool) {
                    this.getViewModel().getParent().getParent().set('checkNavigationTree', Ext.clone(checkNavigationTree));
                }

            },
            scope: this
        });
    },
    /**
     * This function copy the check state for each node in the checkNavigatioTree on the loadedNodes
     * @param {object} loadedNodes object containing all the records in the nevigation tree
     * @param {object} checkNavigationTree object containing informations about che checkstate of the records in checknavigationtree
     */
    preserveCheckState: function (loadedNodes, checkNavigationTree) {
        for (var el in loadedNodes) {
            var node = loadedNodes[el];
            var _type = node._type;
            var _id = node._id;
            if (checkNavigationTree[_type] && checkNavigationTree[_type][_id]) {
                node.checked = checkNavigationTree[_type][_id].visible;
            }
        }
    },
    /**
     * This function is similar to fromRecordToChildNode whit the difference of major accuracy for the check cmp
     * @param {Object} record the actual record returned from the serve
     * @param {Object} checkNavigationTree the checknavigationTree in the viewModel
     * @param {Object} nodeAppending the parent node present in this._loadedNodes
     */
    fromRecordToChildNodeVersion2: function (record, checkNavigationTree, nodeAppending) {
        var checked = true;

        var _type = nodeAppending._type;
        var _id = nodeAppending._id;
        if (checkNavigationTree[_type] && checkNavigationTree[_type][_id]) {
            checked = checkNavigationTree[_type][_id].visible;
        }
        return {
            text: record.get('description') || record.get('Description'),
            id: record.get('_id'),
            leaf: this.nodeFormatter({
                field: 'leaf',
                className: record.get('_type')
            }),
            checked: checked,
            _type: record.get('_type'),
            _id: record.get('_id'),
            expanded: false
        };
    },

    //-------------------//

    /**
     * this function transforms the given input to anoter containing the same info but orzanized differenty
     * @param {Ext.data.record} record the input record
     * @param {Object} data contains information to allow the function works
     * @param {Ext.data.record} data.parentRecord the parent of the actual record
     * @returns {Object} the formatted output
     */
    fromRecordToChildNode: function (record, data) {
        return {
            text: record.get('description') || record.get('Description'),
            id: record.get('_id'),
            leaf: this.nodeFormatter({
                field: 'leaf',
                className: record.get('_type')
            }),
            checked: this.nodeFormatter({
                field: 'check',
                parentNode: data.parentRecord
            }), //TODO: add a check to the parent state
            _type: record.get('_type'),
            _id: record.get('_id'),
            expanded: false
        };
    },
    /**
     * This function handles the incremental loading of the navigation tree
     * @param {Object} node the node to expand. It is already in the three; 
     */
    onExpandNode: function (node) {

        var className = node.get('_type');

        var child = null;
        var parentNode = null;
        var filter = null;
        var formatNodes = null;
        var cardId = node.get('id');

        if (className == '_root') {
            child = this.getRoot();
        } else {
            child = this.getChildNode(className);
            parentNode = this.getNode(className);
            filter = this.makeFilter(parentNode, child, parentNode.metadata.targetClass, cardId);
        }

        if (this._loadedNodes[cardId] && this._loadedNodes[cardId].hasBeenExpanded) {
            return; //if the node has been jet expanded previously
        }

        /**
         * @param {object[]} records the response of the proxy call 
         */
        var callback = function (records) {

            var checkNavigationTree = this.getViewModel().get('checkNavigationTree');
            var children = [];

            for (var i = 0; i < records.length; i++) {
                if (!this._loadedNodes[records[i].data._id]) {
                    var foundNode = this.fromRecordToChildNodeVersion2(records[i], checkNavigationTree, node);
                    this._loadedNodes[records[i].data._id] = foundNode;
                    //children.push(foundNode);

                    this.manageChecknavigationTreeModification(records[i], node.get('checked'), checkNavigationTree);
                    node.appendChild(foundNode); //PERFORMANCE: make n appendChild or only one out of the for cicle
                }
            }
            this._loadedNodes[cardId].hasBeenExpanded = true;
            this._loading = false;

            /**
             * handles the callback for the opennavigationthreepath
             */
            if (this._firsExpand.HavetoOpenPath) {
                this._firsExpand.HavetoOpenPath = false;
                var row = this.getViewModel().get('selectedRow');
                this.openNavigationTreePath(row);
            }
            //node.appendChild(children);
        };

        this.proxyCall(child.metadata.targetClass, filter, callback, this);


    },
    /**
     * This function creates the filter in order to make a proxy call for the domainsTree relations
     * @param parentNode the parent node domains information
     * @param childNode the child node domains information
     * @param {string} className the class Name for the cards attribute if advanced filter
     * @param {number} cardId the card id for the query
     * @param {string} direction the direction for the relation
     * @returns the formatted filter
     */
    makeFilter: function (parentNode, childNode, className, cardId, direction) { //TODO: Set standard rules to find direction. rename params direction and source
        if (!direction) direction = '_2';

        filter = {
            relation: [{
                domain: childNode.metadata.domain,
                type: 'oneof',
                destination: parentNode.metadata.targetClass,
                source: childNode.metadata.targetClass,
                direction: direction,
                cards: [{
                    className: className,
                    id: cardId
                }]
            }]
        };

        return filter;
    },
    onCheckChange: function (node, checked) {
        var checkNavigationTree = this.getViewModel().get('checkNavigationTree');

        this.recursiveChangeCheck(node, checked, checkNavigationTree);
        //Now the bind on checkNavigationTree is fired because the object changes (it means is a new objact, not only changing it's internal field wich wouldn't fire the bind)
        //checkNavigationTreeClone = JSON.parse(JSON.stringify(checkNavigationTree)); 

        this.getViewModel().getParent().getParent().set('checkNavigationTree', Ext.clone(checkNavigationTree));
    },
    /**
     * This function loads the child in the navigation tree of the given node
     * @param {String} targetClass the child className of parentNode
     * @param {Object} filter the filter passed to the proxy
     * @param {function} callback the function to execute after the server responses    
     * @param {object} scope define the scope
     */

    proxyCall: function (targetClass, filter, callback, scope) {
        CMDBuildUI.util.helper.ModelHelper.getModel('class', targetClass).then(function (model) {
            var store = Ext.create(Ext.data.Store, {
                model: model.getName(),
                proxy: {
                    type: 'baseproxy',
                    //TODO: when the server support this URL 
                    //url: Ext.String.format('/classes/{0}/cards/{1}/relations', view.getOriginTypeName(), view.getOriginId())
                    url: Ext.String.format('/classes/{0}/cards', targetClass)
                }
            });

            if (filter != null) {
                store.getProxy().setExtraParam("filter", Ext.JSON.encode(filter));
            }

            store.load({
                params: { //HACK: load more in navigation tree
                    limit: 9999
                },
                callback: function (records) {
                    callback.call(this, records); //see javascript Function.prototype.call 
                },
                scope: scope
            });
        }, null, null, scope); // specify the scope
    },

    /**
     * This function expands the path from a node to the root. The node must be found first
     * @param {object} row selectedRow 
     */
    openNavigationTreePath: function (row) {
        /**
         * this variable is used to avoid this function being executed before onExpandNode();
         */
        if(!this._firsExpand.firsExpandWasMade) {
            this._firsExpand.HavetoOpenPath = true;
            return;
        }

        var rowType = row.get('_type');

        /**
         * 
         * @param {Ext.data.model} rw -The row of the store in the view
         */
        var recursiveExpand = function (rw) {
            while (rw != null) {
                rw.expand();
                rw = rw.parentNode;
            }
        };
        /**
         * This function alows to view the selected feature in the middle of the view
         * @param rw the selected row in the view
         */
        var viewSelectedRowInCenter = function (rw) {
            //var height = this.getView().el.getHeight();
            var row = this.getView().getView().getNode(rw);

            if (row) {//TODO: make it possible either if the navigation tree is not loaded
                var viewEl = this.getView().getView().el;
                var middle = (viewEl.getHeight() - Ext.fly(row).getHeight()) * 0.3;
                viewEl.scrollBy(0, -(middle + viewEl.getOffsetsTo(row)[1]));
            }
        };

        /**
         * 
         */
        var callingOrder = function (rw) {
            this.getView().setSelection(rw);
            recursiveExpand(rw);
            viewSelectedRowInCenter.call(this, rw);
        };

        if (this.isInNavTree(rowType)) { //The row can be found in the navigation tree
            var store = this.getView().getStore();
            var tmpRow = store.findNode('_id', row.get('_id')) || store.findNode('id', row.get('_id')); //FIXME: set univoque pat for the id or _id

            if (!tmpRow) { //if the selected feature is not loaden in the navigation tree

                this._loading = true;
                this.recursiveBottomUpTreeFill(row.get('_type'), row.get('_id'), null, function () {

                    tmpRow = store.findNode('_id', row.get('_id'));
                    callingOrder.call(this, tmpRow);
                    /* this.getView().setSelection(tmpRow);
                    recursiveExpand(tmpRow);
                    viewSelectedRowInCenter() */

                }, this);

            } else {

                //tmpRow = tmpRow.parentNode;
                callingOrder.call(this, tmpRow);
                /* this.getView().setSelection(tmpRow);
                recursiveExpand(tmpRow);
                viewSelectedRowInCenter(); */
            }
        }
        else {
            //The element doesnt't belong to the navigation tree
        }
    },

    /**
     * This function loads recursively the card, his parent and the siblings in the gisNavigationTree 
     * @param {String} className
     * @param {number} id
     * @param {object} pathChain the path build bottom Up
     * @param {function} callback the function to call after the recursion ends
     * @param {object} scope the scope of the f=unction callback
     * 
     */
    recursiveBottomUpTreeFill: function (className, id, pathChain, callback, scope) {
        var store = this.getView().getStore();
        var tmpRow = store.findNode('_id', id);

        if (tmpRow != null) {
            tmpRow.appendChild(pathChain.children);
            this.downRecursiveChangeCheck(tmpRow, tmpRow.get('checked'), this.getViewModel().get('checkNavigationTree'));
            callback.call(scope); //see javascript Function.prototype.call 

            this._loading = false;
            return;

        } else {
            var childNode = this.getNode(className);
            var parentNode = this.getParentNode(childNode.metadata.targetClass); //could write className

            /**
             * Make a function that calculates exactly this situation, is not a standard solution
             */
            var filter = this.makeFilter(parentNode, childNode, childNode.metadata.targetClass, id, '_1');

            this.proxyCall(parentNode.metadata.targetClass, filter, function (results) {
                var _id = results[0].data._id;
                var foundNode;

                //save in this._loadedNodes
                if (this._loadedNodes[_id] != null) { //this case will never happend
                    foundNode = this._loadedNodes[_id];
                } else {
                    foundNode = this.fromRecordToChildNodeVersion2(results[0], this.getViewModel().get('checkNavigationTree'), { _type: null });
                    this._loadedNodes[_id] = foundNode;
                }

                var filter_two = this.makeFilter(parentNode, childNode, parentNode.metadata.targetClass, results[0].data._id, '_2');

                this.proxyCall(childNode.metadata.targetClass, filter_two, function (results_two) {
                    //i can't use nodeFormatter here
                    for (var i = 0; i < results_two.length; i++) {
                        var tmpNode = this.fromRecordToChildNodeVersion2(results_two[i], this.getViewModel().get('checkNavigationTree'), foundNode);

                        if (tmpNode._id == id) {
                            if (pathChain != null) {
                                tmpNode = pathChain;
                                tmpNode.expanded = true;
                            }
                        }

                        this._loadedNodes[results_two[i].data._id] = this.fromRecordToChildNodeVersion2(results_two[i], this.getViewModel().get('checkNavigationTree'), { _type: null });

                        if (foundNode.children == null) {
                            foundNode.children = [];
                        }

                        foundNode.children.push(tmpNode);
                    }
                    this.recursiveBottomUpTreeFill(foundNode._type, foundNode._id, foundNode, callback, scope);
                }, this);
            }, this);
        }
    },

    /*
    Example of this._loadedNodes
    _loadedNodes: {
        root: {
            text: 'root',
            checked: 'true',
            children: [],
            expanded: true,
            _type: '_root',
            _id: '_root',
            hasBeenExpanded: false //value that tells if a node need to load on a onExpandeNode function
        }
    }, */

    /**
     *semaphor strategy, only one method can operate checking this._loading.  
     */
    _loading: false,

    /**
     * is used to set the order for openNavigationTreePath and onExpandNode() (The one called on onBeforeRender)
     */
    _firsExpand: {
        firsExpandWasMade: false,
        HavetoOpenPath: false
    },

    privates: {



        /**
         * This method calculates the fields to set in a node according to the field and to the data 
         * @param {Object} data: an object wit different data used according to the field
         * @param {String} data.field: specify the field to calculate
         */
        nodeFormatter: function (data) {
            //calculates the leaf field
            switch (data.field) {
                /**
                 * calculates the check of the node in base on the check of his parent
                 * @param {Ext.record } data.parentNode the parent node on wich controll the check status 
                 */
                case 'check':   //TODO: handle the floor example
                    return data.parentNode.get('checked');
                /**
                 * calculates the leaf value for the current node
                 * @param {String} data.className the className of the parent
                 */
                case 'leaf':
                    return this.isLastChild(data.className);
            }
        },
        /**
         * This function chenges in a recursive manner the check of the navigationTree euther up and down
         * @param node the actual node
         * @param checked the information of the value of the check
         * @param checkNavigationTree an array we pass in each call, we save here the data
         */
        recursiveChangeCheck: function (node, checked, checkNavigationTree) {
            this.downRecursiveChangeCheck(node, checked, checkNavigationTree);
            this.upNonRecursiveChangeCheck(node, checked, checkNavigationTree);
        },

        /**
         * This function chenges in a recursive manner the check of the navigationTree
         * @param node the actual node
         * @param checked the information of the value of the check
         * @param checkNavigationTree an array we pass in each call, we save here the data
         */
        downRecursiveChangeCheck: function (node, checked, checkNavigationTree) {
            node.set('checked', checked);
            var childNodes = node.childNodes;
            this._loadedNodes[node.get('id')].checked = checked;

            this.manageChecknavigationTreeModification(node, checked, checkNavigationTree);

            for (var i = 0; i < childNodes.length; i++) {
                this.downRecursiveChangeCheck(childNodes[i], checked, checkNavigationTree);
            }
        },
        /**
         * This function chenges in a recursive manner the check of the navigationTree of the up
         * @param node the actual node
         * @param checked the information of the value of the check
         * @param checkNavigationTree an array we pass in each call, we save here the data
         */
        upNonRecursiveChangeCheck: function (node, checked, checkNavigationTree) {

            var tmpNode = node.parentNode;
            while (tmpNode != null && !tmpNode.get('checked')) {
                tmpNode.set('checked', true);
                //tmpNode.set('visible', true);
                this.manageChecknavigationTreeModification(tmpNode, checked, checkNavigationTree);
                tmpNode = tmpNode.parentNode;

            }
        },

        /**
         * This function modifies the array rappresenting checkNavicationTree in the viewModel
         * @param node the interested node
         * @param checked the value of the check
         * @param checkNavigationTree an array we pass in each call, we save here the data
         */
        manageChecknavigationTreeModification: function (node, checked, checkNavigationTree) {

            var type = node.get('_type'); //TODO: normalize this aspect
            var id = node.get('_id');

            //to avoid the warn is possible to add a field data.type and data.id in the model or handle it in 
            //the previous calls
            if (!type || !id) {
                CMDBuildUI.util.Logger.log("ATTENCTION: only root node should happend here", 'warn');
                return;
            }

            if (checkNavigationTree[type] == null) {
                checkNavigationTree[type] = {};
            }

            if (checkNavigationTree[type][id] == null) {
                checkNavigationTree[type][id] = {};
            }

            checkNavigationTree[type][id].visible = checked;
        },

        /**
         * This function returns the root node of the navigation description tree
         * @return the root of the tree. The node who doesn't have parent
         */
        getRoot: function () {
            var theTree = this.getViewModel().get("theTree");
            if (!theTree) return;
            var nodes = theTree.get('nodes');
            var i = 0;
            // TODO:sostituire con find
            while (i < nodes.length && nodes[i].parent != null) {
                i++;
            }
            return nodes[i];
        },

        /**
         * This function returns the parent of the given Class in the navigation tree
         * 
         * @param {String or Number} param The name or the _id of the Class in the navigation Tree
         * @returns {Object} An object containing information of the parent of param in the navigation tree
         * @returns Object.id : the id of the parent
         * @returns Object.name : the name of the parent 
         */
        getParentNode: function (param) {
            if (typeof (param) == 'string') {
                path = 'element.metadata.targetClass';
            } else {
                path = 'element._id';
            }
            returned = {
                '_id': undefined,
                'name': undefined
            };

            var tree = this.getViewModel().get("theTree").get('nodes');
            Ext.each(tree, function (element) {
                if (eval(path) == param) {
                    returned._id = element.parent;
                    return;
                }
            });
            Ext.each(tree, function (element) {
                if (element._id == returned._id) {
                    returned.name = element.metadata.targetClass;
                    returned.metadata = element.metadata;
                    return;
                }
            });
            return returned;
        },

        /**
         * This method search in the 'domain Tree' the child of the given node
         * @param {String} parentClassName the initial node
         * @return {Object} The child of node. Return null if doesn't have childs;
         */
        getChildNode: function (parentClassName) {
            var nodes = this.getViewModel().get("theTree").get('nodes');

            var i = 0;
            while (i < nodes.length && nodes[i].metadata.targetClass != parentClassName) {
                i++;
            }
            if (i == nodes.length) {
                CMDBuildUI.util.Logger.log('This case shuold never append. The last child should not have the expand operation', 'warn');
                return null;
            }

            var _id = nodes[i]._id;
            i = 0;
            while (i < nodes.length && nodes[i].parent != _id) {
                i++;
            }
            if (i < nodes.length) {
                return nodes[i];
            } else {
                return null;
            }
        },

        /**
         * This procedures tells if a node in the 'domains Tree' have childs or is the last element
         * @param {String} nodeName the node to look children for
         * 
         */
        isLastChild: function (nodeName) {
            if (this.getChildNode(nodeName) == null) {
                return true;
            }
            return false;
        },
        /**
         * This function tells if a class is in the navigationTree
         * @param className the class to look for in the domanin tree
         */
        isInNavTree: function (className) {
            var tmpNode = this.getRoot();

            while (tmpNode != null) {
                tmpNodeClassName = tmpNode.metadata.targetClass;
                if (tmpNodeClassName == className) {
                    return true;
                }
                tmpNode = this.getChildNode(tmpNodeClassName);
            }
            return false;
        },
        /**
         * This function returns the node of thTree store having the targhetClass = nodeName
         * @param {string} nodename Teh node to look for in the theTree store
         */
        getNode: function (nodeName) {
            var nodes = this.getViewModel().get("theTree").get('nodes');

            for (var node in nodes) {
                if (nodes[node].metadata.targetClass == nodeName) {
                    return nodes[node];
                }
            }
            return null;
        },
        /**
         * this function transforms the array of numbers in a string of same numbers separated by comma
         * @param {number[]} bbox
         * @returns {String} the stringfied extent
         */
        stringfyBbox: function (bbox) {
            string = bbox[0];
            for (var i = 1; i < bbox.length; i++) {
                string = string + ',' + bbox[i];
            }
            return string;
        },

        /**
         * this function provides to augment in percentage the height and width of the bbox
         * @param bbox the input boox
         * @return the augmented bbox
         */
        calculateExpandedBbox: function (bbox) {
            //the value in percentage to augment 
            var percent = 0;

            size = ol.extent.getSize(bbox);

            var width = size[0];
            var height = size[1];

            var percentX = width * percent;
            var percentY = height * percent;

            return [bbox[0] - percentX, bbox[1] - percentY, bbox[2] + percentX, bbox[3] + percentY];
        }
    }
});
