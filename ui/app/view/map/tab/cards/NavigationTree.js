
Ext.define('CMDBuildUI.view.map.tab.cards.NavigationTree', {
    extend: 'Ext.tree.Panel',

    requires: [
        'CMDBuildUI.view.map.tab.cards.NavigationTreeController',
        'CMDBuildUI.view.map.tab.cards.NavigationTreeModel',

        'CMDBuildUI.store.map.Tree'
    ],
    alias: 'widget.map-tab-cards-navigationtree',
    controller: 'map-tab-cards-navigationtree',
    viewModel: {
        type: 'map-tab-cards-navigationtree'
    },

    cls: 'noicontree',

    store: {
        type: 'map-tree',
        reference: 'navigationTreeStore',
        storeId: 'navigationTreeStore',
        autoSync: true
    },

    hideHeaders: true,
    me: this,
    columns: [{
        xtype: 'treecolumn',
        dataIndex: 'text',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: '100',
        flex: 1,
        items: [{
            iconCls: 'x-fa fa-arrow-circle-right',
            handler: function (grid, rowIndex, colIndex, column, e, record, row) {
                var url = 'classes/' + record.get('_type') + '/cards';
                CMDBuildUI.util.Utilities.redirectTo(url, true);
            }
        }]
    }]

    /**
     * @event opennavigationtrepath
     * Event fired on row selection change
     * @param {object} row the selected row
     */

    /**
     * @event layerlistchanged
     * this function is fired after all the layers are added on map 
     * @param layerList the list of the layer on the olMap either visible or not (enabled or disabled by the tab-layer checkBox)
     * @param olMap the openlayer ma
     */

    /**
     * @event bboxchanged
     * This event is fired when the bbox of the map changes
     * @param bbox the map bbox
     */

     /**
      * @event treedomainsloading
      * this function is fired when the treedomain is loaded
      * @param tree the treeDomain store loades
      */
});
