Ext.define('CMDBuildUI.view.map.tab.cards.LayersModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.map-tab-cards-layers',
    data: {
        oldRange: []
    },
    formulas: {
        geoAttributesByZoom: {
            bind: {
                zoomDisabled: '{zoomDisabled}',
                actualZoom: '{actualZoom}',
                layerStore: '{geoAttributeStoreLoad}',
                externalLayerStore: '{externalLayerStoreLoad}'
            },
            get: function (d) {
                if (d.actualZoom != null && d.layerStore.value != null && d.externalLayerStore.value != null && !d.zoomDisabled) {
                    this.getView().fireEvent('onmapzoomchanged',d.actualZoom, d.layerStore.store, d.externalLayerStore.store);
                }
            }
        },

        geoAttributeStoreLoad: {
            bind: '{geoattributes}',
            get: function (store) {
                return {
                    store: store,
                    value: true
                };
            }
        },
        externalLayerStoreLoad: {
            bind: '{externalLayerStore}',
            get: function (store) {
                return {
                    store: store,
                    value: true
                };
            }
        }
    },

    stores: {
        layerTree: {
            type: 'map-layers',
            root: {
                text: 'Root',
                id: 'root',
                expanded: true,
                checked: true,
                children: [{
                    id: 'geoAttributesRoot',
                    text: CMDBuildUI.locales.Locales.gis.geographicalAttributes,
                    expanded: true,
                    checked: true,
                    leaf: false,
                    children: []
                }, {
                    text: CMDBuildUI.locales.Locales.gis.externalServices, //TODO:
                    id: 'externalServicesRoot',
                    expanded: true,
                    checked: true,
                    leaf: false,
                    children: [{
                        id: 'externalLayersNode',
                        text: CMDBuildUI.locales.Locales.gis.geoserverLayers,
                        expanded: true,
                        checked: true,
                        leaf: false
                    }, {
                        id: 'mapservices',
                        text: CMDBuildUI.locales.Locales.gis.mapServices,
                        expanded: true,
                        checked: true,
                        leaf: false
                    }]
        
                }]
            }
        }
    }
});
