Ext.define('CMDBuildUI.view.map.tab.tabPanelModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.map-tab-tabpanel',
    data: {

    },
    formulas: {
        geoAttributesBase: {
            bind: {
                geoAttributes: '{geoattributes}'
            },
            get: function (data) {
                var view = this.getView().lookupReference('map-geoattributes-grid');
                var store = view.getStore();

                if (data.geoAttributes != null) {
                    var objectTypeName = this.getView().getViewModel().get('objectTypeName');
                    data.geoAttributes.load({
                        callback: function (records, operation, success) {
                            var els = [];
                            records.forEach(function (record) {
                                if (record.get('owner_type') == objectTypeName) {
                                    els.push([
                                        record.get('name'),
                                        record.get('owner_type'),
                                        record.get('zoomDef'),
                                        record.get('zoomMin'),
                                        record.get('zoomMax'),
                                        record.get('type'),
                                        record.get('subtype'),
                                        record.get('_id')
                                    ]);
                                }
                            }, this);
                            store.setData(els);
                        },
                        scope: this
                    });
                }
                return;
            }
        },
        geoAttributesResponse: {
            bind: {
                geoValues: '{geoDataRow}'
            },
            get: function (data) {

                getFirstCoordinates = function (feature, type) {
                    switch (type) {
                        case 'POINT':
                            return [feature.x, feature.y];
                        case 'LINESTRING':
                            return [feature.points[0].x, feature.points[0].y];
                        case 'POLYGON':
                            return [feature.points[0].x, feature.points[0].y];
                    }
                }

                var view = this.getView().lookupReference('map-geoattributes-grid');
                var store = view.getStore();
                var range = store.getRange(); //the ones in the grid view - geoAttrributes

                range.forEach(function (record) {
                    record.set('add', false);
                    record.set('edit', true);
                    record.set('remove', true);
                    record.set('view', true);
                    record.set('_id', null);
                    record.set('coordinates',null);
                }, this);

                if (data.geoValues != null) {
                    data.geoValues.forEach(function (feature) {   //from geovalues call
                        var i = 0;
                        while (i < range.length && feature._attr != range[i].get('name')) {
                            i++;
                        }
                        if (i < range.length) {
                            range[i].set('add', true);
                            range[i].set('edit', false);
                            range[i].set('remove', false);
                            range[i].set('view', false);
                            range[i].set('_id', feature._id);
                            range[i].set('coordinates',getFirstCoordinates(
                                    feature,
                                    feature._type.toUpperCase())
                                );


                            console.log('currency found');//set disabled for the found column
                        }
                    });
                }
            }
        }

    }
});
