Ext.define('CMDBuildUI.view.map.MapController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.map-map',

    listen: {
        global: {
            recursivechechcontrol: 'onCheckChangeLayers'
        }
    },
    control: {
        '#': {
            afterrender: "onAfterRender",
            resize: "onResize",
            clearfeaturesselection: 'onClearFeaturesSelection',
            newrowwithfeatureselected: 'onNewRowWithFeaturesSelected',
            toaddlayer: 'addLayerGis',
            toremovelayer: 'removeLayerGis',
            featurebynavigationtreechanged: 'removeFeaturesBynavigationTree'
        }
    },

    onResize: function (extCmp, width, height) {
        var view = this.getView();
        var map = view.getOlMap();
        map.setSize([width, height]);
    },
    /**
     * @param view
     * @param eOpts
     */
    onAfterRender: function (view, eOpts) {
        // sets and generates the html div id
        view.setDivMapId("map-" + Ext.id());
        view.setHtml(
            Ext.String.format(
                '<div id="{0}"></div>',
                view.getDivMapId()
            )
        );
        // Configure map components
        var olView = new ol.View({
            projection: 'EPSG:3857',
            center: ol.proj.fromLonLat([4.8, 47.75]),
            zoom: 5

        });
        var baseLayer = new ol.layer.Tile({
            source: new ol.source.OSM()
        });
        // add map to container
        // TODO: Resolve the probleme on the opening of map, if i resize the window (of chrome) all works otherwise is wrong 
        view.setOlMap(new ol.Map({
            target: view.getDivMapId(),
            view: olView,
            layers: [
                baseLayer
            ]
        }));
        this.createControls();
    },

    /**
     * @param {Object} a -contains information about the event fired
     */
    onMoveEnd: function (a) {
        var map = a.map;
        var mapView = map.getView();
        var extent = mapView.calculateExtent(map.getSize());
        var zoom = map.getView().getZoom();

        if (zoom != this.getViewModel().get('actualZoom')) {
            this.getViewModel().getParent().getParent().set('actualZoom', zoom);
        } else {
            this.getViewModel().getParent().set('bbox', [extent[0], extent[1], extent[2], extent[3]]);
        }
    },
    /**
     * 
     */
    onCheckChangeLayers: function () {
        var vm = this.getViewModel();
        var checkLayers = vm.get('checkLayers');
        var layerList = this.getView().getLayerList();

        var bool = (vm.get('selectedFeature') != null) ? true : false;
        var bool2 = false;


        for (var el in layerList) {
            if (checkLayers[el] && checkLayers[el].checked == false) {
                layerList[el].setVisible(false);
                bool ? this.interactionSelectionVisibility(layerList[el]) : null;   //handle the case in wich the selectedFeature is on a layer wich is now not visible and mus operate on that feature.
            } else {
                layerList[el].setVisible(true);
                bool2 = true;
            }
        }

        var ft = vm.get('selectedFeatureNotFound'); //Handles the case in wich the selectedFeature is on a layer wich is now visible and must operate to make that feature visible
        if (bool2 && ft != null) {
            this.findFeatureFromRow(vm.get('selectedFeatureNotFound'), false);
        }
    },
    /**
     * remove Layers from the map, provide to delate the selected feature too from the map
     * @param {Array[Objects]} list the list of record representing the layers to remove
     */
    removeLayerGis: function (list) {
        var layerList = this.getView().getLayerList();
        var map = this.getView().getOlMap();
        var vm = this.getViewModel();

        var selectedFeature = vm.get('selectedFeature');

        list.forEach(function (el) {
            var _id = el.get('_id');

            if (this.selectedFeatureBelongslayer(selectedFeature, el)) {
                this.udateNotFoundSelectedFeature([selectedFeature.get('data')]);
                this.onClearFeaturesSelection(this.getView());   //clear the features from the selectInteraction
            }

            var layer = layerList[_id];
            map.removeLayer(layer);

            delete (layerList[_id]);
        }, this);
    },
    /**
     * Puts new layers on map
     * @param list  the list of layer. Has information about style
     */
    addLayerGis: function (list) {
        var vm = this.getViewModel();
        var selectedFeature = this.getViewModel().get('selectedFeature');

        list.forEach(function (el) {
            var _id = el.get('_id');
            //TODO: differentiate between gis layer and GeoExternal Layer
            switch ((el.get('type')).toLowerCase()) {
                case 'geometry':
                    var gisLayer = me.buildGisLayer(el);
                    this.saveLayer(_id, gisLayer);
                    this.handleLayerVisibility(_id, gisLayer);
                    if (this.selectedFeatureBelongslayer(selectedFeature, el)) {
                        var selectInteraction = this.getMapSelectInteraction();
                        selectInteraction.getFeatures().push(selectedFeature);
                    }
                    break;
                case 'shape':
                    var geoLayer = this.buildGeoLayer(el);
                    this.saveLayer(_id, geoLayer);
                    this.handleLayerVisibility(_id, geoLayer);
                    break;
                default:
                    CMDBuildUI.util.Logger.log('Geometry not found', 'error');
            }
        }, this);

        var suspendedFeatures = this.getViewModel().get('selectedFeatureNotFound');

        if (suspendedFeatures != null) {
            this.findFeatureFromRow(suspendedFeatures, false);
        }
    },
    /**
     * This function creates an hash table wich helps to find ol.layers by '_id'
     * @param id The id of the cmdbuild layer
     * @param gisLayer The ol.layer to save
     */
    saveLayer: function (id, gisLayer) {
        var layerList = this.getView().getLayerList();
        layerList[id] = gisLayer;
    },
    /**
     * This function adds the layer to the map and checks if it must be visible or not loocking at checkLayers
     * @param {String} id
     * @param gisLayer TODO: set better commet
     */
    handleLayerVisibility: function (id, gisLayer) {
        var checkLayers = this.getViewModel().get('checkLayers');
        var map = this.getView().getOlMap();

        map.addLayer(gisLayer);

        /* var checkLayers = this.getViewModel().get('checkLayers');
            var nodeParent = this.getViewModel().get('layerTree').getNodeById(nodeName);
            if (checkLayers[id] && checkLayers[id].checked == false || nodeParent.get('checked') == false ) { */
        var parentTreeLayer = this.getParentLayerTree(gisLayer.get('type'), 1);
        var layerTreeStore = this.getViewModel().getParent().getView().lookupReference('tab-panel').lookupReference('map-tab-cards-layer').items.items[0].getStore();
        var node = layerTreeStore.findNode('id', parentTreeLayer);
        var check = node.get('checked');

        if (checkLayers[id] && checkLayers[id].checked == false || !check) {
            gisLayer.setVisible(false);
        } else {
            gisLayer.setVisible(true);
        }
    },
    /**
     * this function creates the layer using the geoserver WMS services
     * @param el the information about the node to add
     */
    buildGeoLayer: function (el) {

        if (!el.get('extent')) {
            CMDBuildUI.util.Logger.log(el.get('name') + " should have a specificated little extent to improve performance " + el.get('extent'), 'warn');
        }

        var geoLayer = new ol.layer.Tile({
            extent: el.get('extent')/* (el.get('extent')) ? (ol.proj.transformExtent(el.get('extent'),'EPSG:4326','EPSG:3857')) : undefined */,
            source: new ol.source.TileWMS({
                url: Ext.String.format('{0}/wms/', CMDBuildUI.util.Config.geoserverBaseUrl),
                params: {
                    'LAYERS': el.get('_id'), //TODO: change here
                    'TILED': true
                },
                loader: function () {
                    return false;
                },
                serverType: 'geoserver'
            })
        });
        geoLayer.set('type', el.data.type);
        return geoLayer;
    },
    /**
     * Constructs the gis layer taking care of all the aspects of the layer
     * @param {Object} layer Is a record. Describes the layer
     * @returns the ol.Layer type
     */
    buildGisLayer: function (layer) {
        me = this;
        var map = this.getView().getOlMap();
        var vmP = this.getViewModel().getParent();
        //this.createControls(map);
        var loader = function (extent, resolution, projection) {

            var params = {
                _id: layer.get('_id'),
                zoomMin: layer.get('zoomMin'),
                zoomMax: layer.get('zoomMax')
            };
            var actualZoom = me.getView().getOlMap().getView().getZoom();

            if (vmP.get('zoomDisabled') == true) { // made for the edit Mode.
                return false;
            } else if (actualZoom >= params.zoomMin && actualZoom <= params.zoomMax) {
                //this.clear();
                me.getGeoElements(params._id, this, extent);
            } else {
                return false;
            }
        };

        var vectorSource = new ol.source.Vector({
            loader: loader,
            strategy: ol.loadingstrategy.bbox
        });

        var olLayer = new ol.layer.Vector({
            source: vectorSource
        });
        this.calculateStyle(layer.get('subtype'), layer.get('style'), olLayer);

        olLayer.set('_attr', layer.get('description'));
        olLayer.set('owner_type', layer.get('owner_type'));
        olLayer.set('_id', layer.get('_id'));
        olLayer.set('type', layer.get('type'));
        return olLayer;
    },
    /**
     * This function creates the click controls for the features
     */
    createControls: function () {
        layerList = this.getView().getLayerList();
        objectType = this.getViewModel().get('objectTypeName');
        this.select = new ol.interaction.Select({
            /* toggleCondition: function(mbe){
                return (mbe.type == 'singleclick');
            }, */
            addCondition: function (mbe) {
                return (mbe.type == 'singleclick');
            },
            style: this.findStyle,
            /*  new ol.style.Style({
                image: new ol.style.Circle({
                    fill: new ol.style.Fill({
                        color: 'green'
                    }),
                    stroke: new ol.style.Stroke({
                        width: 1,
                        color: 'white'
                    }),
                    radius: 5

                }),
                fill: new ol.style.Fill({
                    color: 'green'
                }),
                stroke: new ol.style.Stroke({
                    color: '#white',
                    width: 3
                })

            }), */
            layers: function (layer) {
                var _owner_type = layer.get('owner_type');
                if (_owner_type == objectType) { //HACK: change here to make cliccable all points on map
                    return true;
                }
                return false;
            }
        });
        me = this;
        var selectedFeaturesClick = this.select.getFeatures();

        this.select.on('select', function (event) {
            if (event.selected.length != 0) {
                selectedFeaturesClick.clear();
                selectedFeaturesClick.push(event.selected[0]);
                me.udateFoundSelectedFeature(event.selected[0]);
            }
        });
        var map = this.getView().getOlMap();
        map.addInteraction(this.select);

        this.addBimInteraction();
    },
    /**
     * This function constructs us the correct ol style type
     * @param type the type of style to get
     * @param style Specify style informations
     * @param olLayer the ol.layer.Vector on wich to set the style
     */
    calculateStyle: function (type, style, olLayer) { //TODO: handle icons in style TODO: handle dash Styles
        var me = this;
        var fillColor = this.hexToRgbA(style.fillColor);
        if (fillColor != null) fillColor.push(style.fillOpacity);

        var strokeWidth = style.strokeWidth;
        var strokeColor = this.hexToRgbA(style.strokeColor);

        if (strokeColor != null) strokeColor.push(style.strokeOpacity);

        var pointRadius = style.pointRadius;

        switch (type) {
            case 'POINT': {
                var image;
                var noIconPointStyle = function () {
                    olLayer.setStyle(function (feature) {
                        var basicStyle = [new ol.style.Style({ //point without bin and no icon
                            image: new ol.style.Circle({
                                fill: new ol.style.Fill({
                                    color: fillColor
                                }),
                                stroke: new ol.style.Stroke({
                                    width: strokeWidth,
                                    color: strokeColor
                                }),
                                radius: pointRadius

                            })
                        })];

                        if (feature != null && feature.get('data').hasBim && feature.get('data').bimActive == true) {

                            basicStyle = [me.getBimStyle('point-noIcon', { //point with bim and no icon
                                fillColor: fillColor,
                                strokeColor: strokeColor,
                                strokeWidth: strokeWidth,
                                pointRadius: pointRadius
                            })];
                        }
                        return basicStyle;
                    });
                };
                if (style._icon == null) {  //point icon handler
                    noIconPointStyle();
                } else {
                    var img = new Image();
                    var url = Ext.String.format('{0}/uploads/{1}/download', CMDBuildUI.util.Config.baseUrl, style._icon);

                    img.onerror = function () {
                        noIconPointStyle();
                    };

                    img.onload = function () {

                        var width = img.width;
                        var height = img.height;
                        var coeff;

                        if (width > height) {
                            coeff = width / height;
                            width = pointRadius * 2;
                            height = width / coeff;
                        } else {
                            coeff = height / width;
                            height = pointRadius * 2;
                            width = height / coeff;
                        }

                        var canvas = document.createElement('canvas');
                        canvas.width = width;
                        canvas.height = height;
                        canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);

                        olLayer.setStyle(function (feature) { //set the stile for the icon images
                            var point_icon = [new ol.style.Style({
                                image: new ol.style.Icon({
                                    img: canvas,
                                    imgSize: [width, height]
                                })
                            })];

                            if (feature!= null && feature.get('data').hasBim && feature.get('data').bimActive == true) {
                                point_icon = [me.getBimStyle("point-icon", {
                                    img: canvas,
                                    imgSize: [width, height]
                                })];
                            }
                            return point_icon;
                        });

                    };

                    img.src = url;
                }
                break;
            }
            case 'POLYGON': {
                olLayer.setStyle(new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: style.strokeColor,
                        width: style.strokeWidth,
                        lineDash: undefined
                    }),
                    fill: new ol.style.Fill({
                        color: style.fillColor
                    })
                }));
                break;
            }
            case 'LINESTRING': {
                olLayer.setStyle(new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: style.strokeColor
                    })
                }));
                break;
            }
        }
    },

    /**
     * This function creates a style function for the bim elements
     * @param data the object describing style of the feature
     */
    getBimStyle: function (type, data) {
        //var style = feature.getStyle();
        switch (type) {
            case 'point-noIcon': {
                return new ol.style.Style({
                    image: new ol.style.RegularShape({
                        fill: new ol.style.Fill({
                            color: data.fillColor
                        }),
                        stroke: new ol.style.Stroke({
                            width: data.strokeWidth,
                            color: data.strokeColor
                        }),
                        radius: data.pointRadius,
                        points: 4,
                        angle: Math.PI / 4
                    })
                });
            }
            case 'point-icon': {
                data.image.style.border = "2px double red"; //HACK: chenge style of icon with bim
                new ol.style.Style({
                    image: new ol.style.Icon({
                        img: data.image,
                        imgSize: data.imgSize
                    })
                });
            }
        }

    },
    /**
     * This function is used to create the style of the current selected feature by adding an extra 
     * style
     * @param {ol.feature.Feature} feature
     * @returns {ol.style.Style[]} the merged styles
     */
    findStyle: function (feature) {
        /**
         * Note: is not handled the case in wich the feature have is own style
         */
        var layerList = me.getView().getLayerList();
        var owner_type = feature.get('data')._owner_type;
        var layerStyle;

        for (var el in layerList) {
            var layer = layerList[el];
            if (layer.get('owner_type') == owner_type) {
                layerStyle = layer.getStyleFunction()();
                break;
            }
        }
        var selectionStyle = new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: [0, 255, 0, 0.5]
                }),
                stroke: new ol.style.Stroke({
                    width: 1,
                    color: 'white'
                }),
                radius: 10

            }),
            fill: new ol.style.Fill({
                color: 'green'
            }),
            stroke: new ol.style.Stroke({
                color: '#white',
                width: 3
            })

        });
        var result = [];

        if (layerStyle.length) { //handle the case in wich the the layer have an array of styles
            layerStyle.forEach(function (el) {
                result.push(el);
            }, me);

            result.push(selectionStyle);
        } else {  // handle the case in wich the layers have only one style, an ol.style.Style
            result = [layerStyle, selectionStyle];
        }

        return result;

    },
    /**
     * This function gets the geoElements and put them on the ol layer
     * @param _id the id of the cmdbuild Layer
     * @param vectorSource the ol.layer
     * @param extent The area where to search, if null is calculated at the moment 
     */
    getGeoElements: function (_id, vectorSource, extent) {
        var vm = this.getViewModel();
        var bimProjectStore = Ext.getStore("bim.Projects");

        if (extent == null) {
            extent = this.stringfyBbox(vm.get('bbox'));
        } else {
            extent = this.stringfyBbox(extent);
        }
        var store = Ext.create('Ext.data.Store', {
            model: 'CMDBuildUI.model.map.GeoElement',

            proxy: {
                url: Ext.String.format('/classes/_ANY/cards/_ANY/geovalues', vm.get('objectTypeName')),
                type: 'baseproxy'
            }
        });
        var list = this.getViewModel().get('checkNavigationTree');
        store.load({
            params: {
                attribute: _id,
                area: extent
            }, callback: function (records) {
                if(!records) {
                    CMDBuildUI.util.Logger.log('Something is wrong', 'error');
                    return;
                }
                records.forEach(function (record) {
                    var type = record.get('_type');
                    if (type == 'point') {
                        var point = new ol.geom.Point(
                            ol.proj.transform([record.get('x'), record.get('y')], 'EPSG:3857', 'EPSG:3857')
                        );
                        var feature = new ol.Feature({
                            type: 'Point',
                            geometry: point,
                            data: record.getData()
                        });
                        feature.setId(record.get('_id'));
                        this.setBimProperty(feature, bimProjectStore);
                        this.insertFeature(vectorSource, feature, list);
                    } else if (type == 'polygon') {
                        var points = record.get('points');
                        var coords = [];
                        for (var i = 0; i < points.length; i++) {
                            coords.push(ol.proj.transform([points[i].x, points[i].y], 'EPSG:3857', 'EPSG:3857'));
                        }
                        var featurePoly = new ol.Feature({
                            type: 'Polygon',
                            geometry: new ol.geom.Polygon([coords]),
                            data: record.getData()
                        });
                        featurePoly.setId(record.get('_id'));
                        this.insertFeature(vectorSource, featurePoly, list);
                    } else if (type == 'linestring') {
                        var pointsL = record.get('points');
                        var coordsL = [];
                        for (var k = 0; k < pointsL.length; k++) {
                            coordsL.push(ol.proj.transform([pointsL[k].x, pointsL[k].y], 'EPSG:3857', 'EPSG:3857'));
                        }
                        var featureLine = new ol.Feature({
                            type: 'LineString',
                            geometry: new ol.geom.LineString(coordsL),
                            data: record.getData()
                        });
                        featureLine.setId(record.get('_id'));
                        this.insertFeature(vectorSource, featureLine, list);
                    }
                }, this);
            },
            scope: this
        });


    },
    /**
     * This feature handle the process to add a a feature in the vectorSource with the correct style
     * @param {ol.VectorSource} vectorSource the openlayer vector source
     * @param {ol.Feature} feature the openlayyer feature 
     * @param checkNavigationTree the checkNavigationTree contained in the modelView
     */
    insertFeature: function (vectorSource, feature, checkNavigationTree) {
        if (!this.isVisibleFeature(feature, checkNavigationTree)) {
            feature.setStyle(
                [new ol.style.Style({
                    visibility: 'hidden'
                })
                ]);
        }
        vectorSource.addFeature(feature);
    },

    /**
     * This function tells if a feature is visible or not
     * @param {ol.Feature} feature the theature
     * @param checkNavigationTree the checkNavigationTree contained in the modelView
     * MODIFY this function modifies the checkNavigationTree 
     */
    isVisibleFeature: function (feature, checkNavigationTree) {
        var type = feature.get('data')._owner_type;

        if (checkNavigationTree[type] != null) {
            var id = feature.get('data')._owner_id;
            if (checkNavigationTree[type][id] != null) {
                if (checkNavigationTree[type][id].visible == false) {
                    return false;
                } else {
                    delete checkNavigationTree[type][id];
                }
            }
        }
        return true;
    },

    /**
     * This function removes from the ol map the features individuated by the data in the list
     * @param {Object} list the checkNavigationTree contained in the modelView
     * @param {Object} list.checkListNT the actual Object where data are listed
     */
    removeFeaturesBynavigationTree: function (list) { //TODO: add the overlay functionality to save the selected point
        list = list.checkListNT;
        var map = this.getView().getOlMap();
        var layers = map.getLayers().getArray(); //should be the same as this.getView().getLayerList();

        var tmpList = {};

        layers.forEach(function (el) {
            var type = el.get('owner_type');
            if (list[type] != null) {
                tmpList[type] = {};
                this.manageFeatures(el, list[type], tmpList[type]);
            }
        }, this);

        this.updateCheckListNT(list, tmpList);
    },

    /**
     * This method change the visibility of the fature listed in ObjFeatures on the olLayer
     * assert the olLayer is the owner of those features
     * @param olLayer the openlayer layer
     * @param {Object} ObjFeatures contains description of the features
     * @param {boolean} ObjFeatures.visible Tells if the object must be visible or hidden 
     * @param {object} tmpList Saves all the cards being made visible again (same structure as checkListNT)
     */
    manageFeatures: function (olLayer, ObjFeatures, tmpList) {
        var olFeat = olLayer.getSource().getFeatures();

        olFeat.forEach(function (el) {
            var id = el.get('data')._owner_id;
            if (ObjFeatures[id] != null) {
                var tmp = ObjFeatures[id];
                var style;
                if (tmp.visible == true) {
                    style = undefined; //NOTE: this style the returned value is the previous. If not working chenge here
                    this.privateListManage(id, ObjFeatures, tmpList);
                } else {
                    style = new ol.style.Style({
                        display: 'none'
                    });
                }
                el.setStyle(style);
            }
        }, this);
    },
    /**
     * This function decides if remove an object from checkNavigationTree or not
     * @param id a class owner id 
     * @param ObjFeature the 
     * @param {object} tmpList saves all the elements being made visible again
     */
    privateListManage: function (id, ObjFeature, tmpList) {
        tmpList[id] = ObjFeature[id];
    },
    /**
     * this function removes the elements in list that appear in tmpList
     * NB: list and tmpList have the same structure. Elements in 'tmpList' must appear in 'list'
     * @param list the first list
     * @param tmpList the elements to remove from lis
     * 
     */
    updateCheckListNT: function (list, tmpList) {
        for (var type in tmpList) {
            for (var id in tmpList[type]) {
                delete list[type][id];
            }
        }
    },
    /**
     * @param {CMDBuildUI.view.map.Map} view
     * @param {Object[]} data
     */
    onNewRowWithFeaturesSelected: function (view, data) {
        this.findFeatureFromRow(data, true);
    },

    /**
     * This function manages the modify of the map in base on the selection of the user in the tab->list panel
     * @param data The list of features present with the relative card
     */
    manageFeaturelistUserSelection: function (data, centerSet) {
        if (centerSet == true) {
            var map = this.getView().getOlMap();
            var mapView = map.getView();
            var center = this.getCenter(data);
            mapView.setCenter(center);
        }
    },

    /**
     * this function handles the visibility of the interactionSelection
     * @param layer the ol.Layer
     */
    interactionSelectionVisibility: function (layer) {
        var vm = this.getViewModel();
        var feature = vm.get('selectedFeature');
        if (this.selectedFeatureBelongslayer(feature, layer)) {
            var ft = this.getMapSelectInteraction().getFeatures().getArray()[0];

            this.udateNotFoundSelectedFeature(this.modifyFeature(ft));

            this.onClearFeaturesSelection(this.getView());
        }
    },

    /**
     * Clear features selection
     * @param {CMDBuildUI.view.map.Map} view
     */
    onClearFeaturesSelection: function (view) {
        this.getMapSelectInteraction().getFeatures().clear();
    },

    /**
     * This function sets hover and styling for the features with bim
     * Add's an event handler for the map pointer move
     */
    addBimInteraction: function () {
        var bimEnabled = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.bim.enabled);
        if (!bimEnabled) {
            return;
        }

        this.addBimPopup();
        this.pointermoveMapAssign();
        
    },

    /**
     * Binds a function to pointermove event
     */
    pointermoveMapAssign: function () {
        var map = this.getView().getOlMap();
        map.on('pointermove', this.pointerMoveFunction, this);
    },

    /**
     * 
     */
    pointermoveMapUnassign: function () {
        var map = this.getView().getOlMap();
        map.un('pointermove', this.pointerMoveFunction, this);
    },
    /**
     * This is the function fired on 'pointermove' ol event
     * Is used to kwno when open the popup for the bim elements
     * @param event
     */
    pointerMoveFunction: function (event) {
        var map = this.getView().getOlMap();
        var features = map.getFeaturesAtPixel(event.pixel);

        if (features) {
            var feature = features[0];
            var featureId = feature.id_;

            if (feature.get('data')._type == 'point') { //HACK: enable popup only for point type

                if (feature.get('data').hasBim && feature.get('data').bimActive && featureId != this._popupBim.lastFeatureId && !me._popupBimEvents.popupHover) {
                    this._popupBim.lastFeatureId = featureId;
                    this._popupBim.lastProjectId = feature.get('data').projectId;
                    this._popupBimEvents.featureHover = true;

                    var position = feature.getGeometry().getCoordinates();
                    this._popupBim.overlay.setPosition(position);
                }
            }
        }
        else {
            this._popupBimEvents.featureHover = false;
        }

        if (this._popupBimEvents.featureHover == false && this._popupBimEvents.popupHover == false) {
            this._popupBim.overlay.setPosition(undefined);
            this._popupBim.lastFeatureId = null;
        }
    },

    /**
     * creates the DOM element,the ol.Overlay element and adds it to the map
     */
    addBimPopup: function () {
        /**
         * Creates the DOM element
         */

        var extEl = new Ext.button.Button({
            text: 'Show Bim',
            renderTo: Ext.getBody(),
            handler: function () {
                CMDBuildUI.util.Utilities.openPopup('bimPopup', "Bim Viewer", { //FUTURE: create a configuration for passing the poid and ifctype
                    xtype: 'bim-container',
                    projectId: me._popupBim.lastProjectId
                });
            }
        });

        var element = extEl.getEl().dom;

        /**
         * Add controls to the dom elemnt
         */

        element.onmouseenter = function (mouseEvt) {
            me._popupBimEvents.popupHover = true;
        };

        element.onmouseleave = function (mouseEvt) {
            me._popupBimEvents.popupHover = false;

            if (me._popupBimEvents.featureHover == false && me._popupBimEvents.popupHover == false) {
                me._popupBim.overlay.setPosition(undefined);
                me._popupBim.lastFeatureId = null;
            }
        };


        /**
         * creates ol.Overlay
         */
        this._popupBim = {
            overlay: new ol.Overlay({
                element: element,
                id: 'bimMapPopup',
                offset: [0, -25],
                stopEvent: false
            }),
            element: element
        };
        this._popupBim.overlay.setPosition(undefined);

        var map = this.getView().getOlMap(); // this can be passed as argument of the function avoiding calling the view
        map.addOverlay(this._popupBim.overlay);
    },


    /**
     * @param  {ol.Feature} feature the interested feature 
     * @param  {Ext.data.Store} bimProjectStore the bim.Projects store. NOTE: is passed as argument to avoid calling Ext.getStore('bim.Projects') for each function call
     */
    setBimProperty: function (feature, bimProjectStore) {
        var me = this;
        var bimEnabled = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.bim.enabled);

        if (!bimEnabled) {
            return;
        }

        /**
         * this function sets some values in the feature if ther is a match in the bim.Projects store 
         * @param _feature the feature being analized
         */
        setFeatureBim = function (_feature) {
            var _owner_id = _feature.get('data')._owner_id;
            var _owner_class = _feature.get('data')._owner_type;
            //vedere nei range dello store se trovo una corrispondenza tra owner id e owner type. se si assegno has bim alla feature
            var range = bimProjectStore.getRange();
            var bool = false;
            var i;

            for (i = 0; i < range.length && !bool; i++) {
                if (range[i].get('ownerClass') == _owner_class && range[i].get('ownerCard') == _owner_id) {
                    bool = true;
                }
            }


            if (bool) { //HACK: chose here the extra bim values to add to the feature 
                i--; //must do, set the i to the correct value. Because the for loop increases import {  } from "module";

                var data = _feature.get('data');
                data.hasBim = true;
                data.bimActive = range[i].get('active');
                data.projectId = range[i].get('projectId');

                //me.setBimStyle(_feature);
            }

        };

        if (bimProjectStore.isLoaded()) { //handles the case in wich the store is already loaded
            setFeatureBim(feature);
        } else {    //handle the case in wich the store is not yet loaded and used the callback function
            bimprojectStore.load({
                callback: setFeatureBim(feature),
                scope: this
            });
        }
    },
    privates: {

        /**
         * {object} object containing the ol.Overlay and the htmlElement used fo it
         */
        _popupBim: {
            /**
             * saves the information about the last feature wich opened the popup 
             */
            lastFeatureId: null,
            /**
             * Contains the html element used as popup
             */
            element: null,
            /**
             * contains the ol.Overlay object
             */
            overlay: null,
            /**
             * The id of the last project clicked
             */
            lastProjectId: null
        },

        _popupBimEvents: {
            popupHover: null,
            featureHover: null
        },

        /**
         * gets an olFeature and transform it in his essential components
         * @param feature TODO: set better commet here
         */
        modifyFeature: function (feature) {
            ret = feature.get('data');
            return [ret];
        },

        /**
         * this functon sets a feature as selected. The feature is found in the list
         * @param {ol.Feature} feature the feature
         */
        makeSelection: function (feature) {
            var selectInteraction = this.getMapSelectInteraction();
            selectInteraction.getFeatures().clear();
            selectInteraction.getFeatures().push(feature);
        },

        /**
         * this function finds a feature saved in the map
         * @param id information to find the feature
         * @returns the feature if is find, null if the feature is not in any layer
         */
        getFeatureFromId: function (id) { //TODO: this method could be upgraded by accessing the layer directli without searching it in the layerList
            var layerList = this.getView().getLayerList();
            for (var el in layerList) {
                var source = layerList[el].getSource();
                var feature = source.getFeatureById(id);
                if (feature != null) {
                    return feature;
                }
            }
            return null;
        },

        /**
         * this function calculates the center of the map of a given cmdBuild feature
         * @param data the cmdbuild feature
         */
        getCenter: function (data) {
            var type = data._type;
            if (type == 'point') {
                return [data.x, data.y];
            } else if (type == 'polygon') {
                return [data.points[0].x, data.points[0].y];
            } else if (type == 'linestring') {
                return [data.points[0].x, data.points[0].y];
                //TODO: implements linestring points center take
            } else {
                CMDBuildUI.util.Logger.log('Is an error here', 'error');
            }
        },

        /**
         * This function sets some data specified in the viewModel and modify it
         * @param {Object[]} obj contains (key - value) pairs
         */
        manualParametersBinding: function (obj) { //TODO: modify this function
            var vm = this.getViewModel().getParent();
            obj.forEach(function (element) {
                var key = element.key;
                var value = element.value;
                vm.set(key, value);
            });
        },

        /**
         * @param list TODO: set better comment here
         */
        stringfyAttribute: function (list) {
            var string = '';
            for (var k in list) {
                if (k != 'lenght') {
                    if (string.length > 0) {
                        string = string + ',' + k;
                    } else {
                        string = k;
                    }
                }
            }
            return string;
        },

        /**
         * this function modifyes the input array of for numbers in a string format
         * NOTE: this format is used to make proxy calls
         * @param {number[]} bbox an array of 4 numbers
         * @returns {String} a string representing the array
         */
        stringfyBbox: function (bbox) {
            string = bbox[0];
            for (var i = 1; i < bbox.length; i++) {
                string = string + ',' + bbox[i];
            }
            return string;
        },

        /**
         * @returns the select interaction of the map
         */
        getMapSelectInteraction: function () {
            var map = this.getView().getOlMap();
            var interactionArray = map.getInteractions().getArray();
            var i = interactionArray.length - 1;

            while (i >= 0) {
                if (interactionArray[i] instanceof ol.interaction.Select) {
                    return interactionArray[i];
                }
                i--;
            }
        },

        /**
        * This data tells if a feature belong to a layer
        * @param {object} feateure the feature saved in the view model as selectedFeature
        * @param {ol.layer} layer the layer
        * @return true if belongs, false otherwose
        */
        selectedFeatureBelongslayer: function (feature, layer) {
            var owner = layer.get('owner_type');
            var name = layer.get('name') ? layer.get('name') : layer.get('_attr');

            if (feature instanceof ol.Feature) {
                //var name = layer.get('name');
                if (feature != null && feature.get('data')._owner_type == owner && feature.get('data')._attr == name) { //i can use the layer.getSource().getfeatureById(feature._id) 
                    return true;
                }
                return false;
            } else {
                //var name = layer.get('_attr');
                if (feature != null && feature._attr == name && feature._owner_type == owner) {
                    return true;
                }
                return false;
            }
        },

        /**
         * This function saves in the viewModel the data reguarding the actual selected feature, the one clicked with the mouse
         */
        saveSelectedFeature: function () {
            var selectInteraction = this.getMapSelectInteraction();
            var features = selectInteraction.getFeatures().getArray();

            if (features.length > 0) {
                var feature = features[0];
                var selectedFeature = {
                    id_: feature.getId(),
                    owner_type: feature.get('data')._owner_type,
                    name: feature.get('data')._attr,
                    data: feature

                };
                this.getViewModel().getParent().getParent().set('selectedFeature', selectedFeature);
            } else {
                this.getViewModel().getParent().getParent().set('selectedFeature', null);
            }
        },
        /**
         * this function generates a DOM element
         * @return the dom element
         */
        generateEl: function () {
            /**Create the dom element with this style */
            var el = document.createElement('div');
            el.id = 'overlayId';
            el.style.width = '10px';
            el.style.height = '10px';
            el.style.borderRadius = '100%';
            el.style.backgroundColor = 'orange';
            el.style.marginLeft = '-50%';
            el.style.marginTop = '-50%';

            return el;
        },

        /**
         * This function changes the format of color so it can be readable from openlayer
         * @param {String} hex the input hexadecimal
         * @return an array rappresenting the hex color
         */
        hexToRgbA: function (hex) {
            if (hex == null) return;
            var c;
            if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
                c = hex.substring(1).split('');
                if (c.length == 3) {
                    c = [c[0], c[0], c[1], c[1], c[2], c[2]];
                }
                c = '0x' + c.join('');
                return [(c >> 16) & 255, (c >> 8) & 255, c & 255]; // to set the transparency value add the alpha parameter to the returned ones
            }
            throw new Error('Bad Hex');
        },

        /**
         * Update found selected feature
         * @param {ol.Feature} feature
         */
        udateFoundSelectedFeature: function (feature) {
            this.getViewModel().getParent().set('selectedFeature', feature);
            this.getViewModel().getParent().set('selectedFeatureNotFound', null);
        },

        /**
         * Update not found selected feature
         * @param {Object} feature
         */
        udateNotFoundSelectedFeature: function (feature) {
            this.getViewModel().getParent().set('selectedFeatureNotFound', feature);
            this.getViewModel().getParent().set('selectedFeature', null);
        },

        /**
         * This function finds a feature in the actual layers present in the layerList. 
         * When I enter this function i know somewhere exists an olFeature. it might 
         * not be loaded so cant be but in interactionSelect
         * @param {Object[]} featrueData
         * @param {boolean} centerSet boolean value wich tells if change map center after finding feature
         */
        findFeatureFromRow: function (featureData, centerSet) {    //the feature exists
            var layerList = this.getView().getLayerList();
            this.manageFeaturelistUserSelection(featureData[0], centerSet);

            for (var el in layerList) {

                if (layerList[el].getVisible() && this.selectedFeatureBelongslayer(featureData[0], layerList[el])) {//we know a layer in layerList is the owner of that feature
                    layerList[el].un('addfeature');
                    var source = layerList[el].getSource();
                    var ft = source.getFeatureById(featureData[0]._id);
                    if (ft != null) {   //the layer exists and the feature is loaded on that layer
                        this.udateFoundSelectedFeature(ft);
                        this.makeSelection(ft);
                    } else {
                        this.udateNotFoundSelectedFeature(featureData);
                        layerList[el].getSource().on('addfeature', function (featureEvt) { // The layer exist but the feature is not loaded on that layer
                            if (featureData[0]._id == featureEvt.feature.getId()) {
                                delete featureEvt.target.listeners_.addfeature;
                                //featureEvt.target.un('addfeature');
                                this.udateFoundSelectedFeature(featureEvt.feature);
                                this.makeSelection(featureEvt.feature);
                                return;
                            } else if (featureData[0]._owner_id == featureEvt.feature.get('data')._owner_id) {
                                console.log("i've found a feature that has this owner id");
                            }
                        }, this);
                    }
                    return;
                }
            }
            this.udateNotFoundSelectedFeature(featureData);
        }
    },
    /**
     * This function gets the name of parent nodes based on type
     * @param {string} type geometry or shpe
     * @param {number} depth how many parents to return, if null gives the first one
     */
    getParentLayerTree: function (type, depth) {
        switch (type) {
            case 'geometry':
                return 'geoAttributesRoot';
            case 'SHAPE':
                return 'externalLayersNode';
            default:
                console.error('SHAPE NAME IS NOT CORRECT!');
                return null;
        }
    }
});
