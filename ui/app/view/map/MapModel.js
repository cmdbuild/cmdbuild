Ext.define('CMDBuildUI.view.map.MapModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.map-map',

    data: {

    },
    formulas: {
        toAddLayer: {
            bind: '{toAdd}',
            get: function (list) {
                if (list == null) return;
                else if (list.length > 0) {
                    this.getView().fireEvent('toaddlayer', list);
                    var navTreeView = this.getView().getViewModel().getParent().getView().lookupReference('tab-panel').lookupReference('map-tab-cards-navigationtree');
                    if (navTreeView) {
                        navTreeView.fireEvent('layerlistchanged', this.getView().getLayerList(), this.getView().getOlMap());
                    }
                } else {
                    var map = this.getView().getOlMap();
                    var extent = map.getView().calculateExtent(map.getSize());
                    this.getView().getViewModel().getParent().set('bbox', [extent[0], extent[1], extent[2], extent[3]]);
                }

            }
        },
        toRemoveLayer: {
            bind: '{toRemove}',
            get: function (list) {
                if (list.length > 0) {
                    this.getView().fireEvent('toremovelayer', list);
                }
            }
        },
        navigationTreeCheckChange: {
            bind: {
                checkListNT: '{checkNavigationTree}'
            },
            get: function (checkListNT) {
                this.getView().fireEvent('featurebynavigationtreechanged', checkListNT);
            }
        },
        overlaySelection: {
            bind: {
                notFound: '{selectedFeatureNotFound}'
            },
            get: function (data) { //TODO: put on View
                var map = this.getView().getOlMap();
                var controller = this.getView().getController();
                var el;
                if (data.notFound != null) {
                    el = controller.generateEl();
                    var position = controller.getCenter(data.notFound[0]);
                    var ovLay = map.getOverlayById('overlayId');

                    if (ovLay == null) {
                        ovLay = new ol.Overlay({
                            id: 'overlayId',
                            position: position,
                            element: el,
                            stopEvent: false
                        });
                        map.addOverlay(ovLay);

                    } else {
                        ovLay.setPosition(position);
                    }
                } else {
                    map.removeOverlay(map.getOverlayById('overlayId'));
                    el = document.getElementById('overlayId');

                    if (el != null) {
                        el.remove();
                    }
                }
            }
        }
    },
    stores: {
        geoElements: {
            model: 'CMDBuildUI.model.map.GeoElement',
            proxy: {
                url: Ext.String.format('/classes/_ANY/cards/_ANY/geovalues', '{objectTypeName}'),
                type: 'baseproxy'
            }
        }
    }
});