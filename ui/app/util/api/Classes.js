Ext.define('CMDBuildUI.util.api.Classes', {
    singleton: true,

    /**
     * Get class geo attributes
     * 
     * @param {String} className
     * @return {String} The url for api resources
     */
    getGeoAttributes: function (className) {
        return Ext.String.format(
            "/classes/{0}/geoattributes",
            className
        );
    },
    /**
     * This function returns the url for the geolayer service
     * @param className the class name
     */
    getExternalGeoAttributes: function (className) {
        return Ext.String.format('/classes/{0}/geolayers', className);
    },

    /**
     * Get cards url by className
     * @param {String} className
     * @return {String}
     */
    getCardsUrl: function (className) {
        return Ext.String.format(
            "/classes/{0}/cards",
            className
        );
    },

    /**
     * Get class report for give type
     * @param {String} extension The file extension (PDF|ODT)
     * @param {String} className
     */
    getClassReport: function (extension, className) {
        var uri = Ext.String.format(
            '{0}/reports/classschema/report_classschema.pdf?extension={1}&parameters={"class":"{2}"}',
            CMDBuildUI.util.Config.baseUrl,
            extension,
            className);
        return encodeURI(uri);
    },

    /**
     * 
     * @param {String} className 
     * @param {Number} cardId 
     */
    getCardBimUrl: function (className, cardId) {
        return Ext.String.format(
            '{0}/classes/{1}/cards/{2}/bimvalue?if_exists=true',
            CMDBuildUI.util.Config.baseUrl,
            className,
            cardId
        );
    },

    /**
     * 
     * @param {String} className 
     * @param {Number} cardId 
     */
    getCardRelations: function (className, cardId) {
        return Ext.String.format("/classes/{0}/cards/{1}/relations", className, cardId);
    }
});