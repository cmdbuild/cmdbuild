Ext.define('CMDBuildUI.util.api.Client', {
    singleton: true,

    /**
     * @cfg {Ext.data.Model} record
     */
    record: null,

    /**
     * @param {String} attribute
     * @return {Strin|Numeric|Object}
     */
    getValue: function(attribute) {
        if (this.record) {
            return this.record.get(attribute);
        } else {
            CMDBuildUI.util.Logger.log(
                "Record is not evaluated",
                CMDBuildUI.util.Logger.levels.warn
            );
        }
    },

    /**
     * @param {String} attribute
     * @return {Strin}
     */
    getLookupCode: function(attribute) {
        if (this.record) {
            return this.record.get("_" + attribute + "_code");
        } else {
            CMDBuildUI.util.Logger.log(
                "Record is not evaluated",
                CMDBuildUI.util.Logger.levels.warn
            );
        }
    },

    /**
     * @param {String} attribute
     * @return {Strin}
     */
    getLookupDescription: function(attribute) {
        if (this.record) {
            return this.record.get("_" + attribute + "_description_translation");
        } else {
            CMDBuildUI.util.Logger.log(
                "Record is not evaluated",
                CMDBuildUI.util.Logger.levels.warn
            );
        }
    },

    /**
     * @param {String} attribute
     * @return {Strin}
     */
    getReferenceDescription: function(attribute) {
        if (this.record) {
            return this.record.get("_" + attribute + "_description");
        } else {
            CMDBuildUI.util.Logger.log(
                "Record is not evaluated",
                CMDBuildUI.util.Logger.levels.warn
            );
        }
    },

    /**
     * @param {RegExp} regex
     * @param {String} value
     * @return {Boolean}
     */
    testRegExp: function(regexp, value) {
        if (!regexp || !(regexp instanceof RegExp)) {
            CMDBuildUI.util.Logger.log(
                "RegExp not valid!",
                CMDBuildUI.util.Logger.levels.error,
                null, {
                    regexp: regexp
                }
            );
        }
        return regexp.test(value);
    }
});