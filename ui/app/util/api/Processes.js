Ext.define('CMDBuildUI.util.api.Processes', {
    singleton: true,

    /**
     * Get all instances activities url
     * 
     * @param {String} processName
     * @return {String} The url for api resources
     */
    getAllInstancesActivitiesUrl: function(processName) {
        return Ext.String.format(
            "/processes/{0}/instance_activities",
            processName
        );
    },

    /**
     * Get the url for process start activities
     * 
     * @param {String} processName
     * @param {Numeric} instanceId
     * @return {String} The url for api resources
     */
    getStartActivitiesUrl: function(processName) {
        return Ext.String.format(
            "/processes/{0}/start_activities",
            processName
        );
    },

    /**
     * Get the url for process instances
     * 
     * @param {String} processName
     * @return {String} The url for api resources
     */
    getInstancesUrl: function(processName) {
        return Ext.String.format(
            "/processes/{0}/instances/{1}",
            processName
        );
    },

    /**
     * Get the url for available activities for an instance
     * 
     * @param {String} processName
     * @param {Numeric} instanceId
     * @return {String} The url for api resources
     */
    getInstanceActivitiesUrl: function(processName, instanceId) {
        return Ext.String.format(
            "/processes/{0}/instances/{1}/activities",
            processName,
            instanceId
        );
    },

    /**
     * Get class report for give type
     * @param {String} extension The file extension (PDF|ODT)
     * @param {String} className
     */
    getVersionFileUrl: function (processName, versionId) {
        var uri = Ext.String.format(
            '{0}/processes/{1}/versions/{2}/file',
            CMDBuildUI.util.Config.baseUrl,
            processName,
            versionId);
        return encodeURI(uri);

    },

    /**
     * 
     * @param {String} processName 
     * @param {Number} instanceId 
     */
    getProcessInstanceRelations: function (processName, instanceId) {
        return Ext.String.format("/processes/{0}/instances/{1}/relations", processName, instanceId);
    }
});