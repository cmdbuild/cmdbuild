Ext.define('CMDBuildUI.util.bim.Viewer', {
    singleton: true,

    /**
     * @event ifctreeloaded
     * Fired when the ifctree is loaded
    */

    /**
     * @param  {} point
     * @param  {} distance
     * @param  {} limits
     */
    zoomLookAtPoint: function (point, distance, limits) {
        CMDBuildUI.util.bim.SceneTree.zoomLookAtPoint(point, distance, limits);
    },
    /**
     * @param {Number} oid
     * @param {boolean} isLeaf 
     */
    select: function (oid, isLeaf) {
        var clickSelect = CMDBuildUI.util.bim.SceneTree.getViewer().getControl("BIMSURFER.Control.ClickSelect");
        var orbit = CMDBuildUI.util.bim.SceneTree.getViewer().getControl("BIMSURFER.Control.PickFlyOrbit");
        this.globalCurrentOid = oid;
        var gid = CMDBuildUI.util.bim.SceneTree.getGid(oid);
        if (!isLeaf && !gid) {
            this.showTransparentRecusively(CMDBuildUI.util.bim.IfcTree.getRoot(), this.globalCurrentOid, this.transparenceValue);
        }
        if (!gid) {
            return;
        }

        clickSelect.pick({
            nodeId: gid
        });
        var sceneObject = CMDBuildUI.util.bim.SceneTree.findNode(gid);
        var matrix = sceneObject.nodes[0].nodes[0];
        var worldMatrix = matrix.getWorldMatrix();
        var geometryNode = matrix.nodes[0];
        var color = geometryNode._core.arrays.colors;
        var boundary = geometryNode.getBoundary();
        /*
         * Object {xmin: -150, ymin: -150, zmin: 0, xmax: 3950, ymax: 150,
         * zmax: 2800}
         * 
         */
        var center = {
            x: (boundary.xmax - boundary.xmin) / 2,
            y: (boundary.ymax - boundary.ymin) / 2,
            z: (boundary.zmax - boundary.zmin) / 2
        }
        var centerTransformed = SceneJS_math_transformVector4(worldMatrix, [center.x, center.y, center.z, 1]);
        centerTransformed = {
            x: centerTransformed[0],
            y: centerTransformed[1],
            z: centerTransformed[2]
        }
        this.zoomLookAtPoint(centerTransformed, 10000);
        orbit.pick({
            nodeId: CMDBuildUI.util.bim.SceneTree.getGid(oid),
            worldPos: [centerTransformed.x, centerTransformed.y, centerTransformed.z]
        });
    },
    /**
     * this function calls a recursive function and sets the values for the call
     * @param {Number} value a transparence value to set as default
     */
    changeTransparence: function (value) {
        /*  if (!this.globalCurrentOid) { //LOOK: this is not useful now
             this.globalCurrentOid = IfcTree.getRoot().ifcObject.object.oid;
         } 
        */
        var root = CMDBuildUI.util.bim.IfcTree.getIfcRoot();
        for (var i = 0; i < root.length; i++) {
            this.GlobalCurrentOid = root[i].ifcObject.object.oid;
            this.showTransparentRecusively(root[i],/* this.globalCurrentOid, */ value);// oid);
        }

    },

    /**
     * @param {Object} node the node we are operating on
     * @param {Number} value the default value
     */
    showTransparentRecusively: function (node,/*  oid, */ value) {

        value = this.getAlphaValueFromIfcType(node.ifcObject.getType(), value);

        var currentOid = node.ifcObject.object.oid;
        var gid = CMDBuildUI.util.bim.SceneTree.getGid(currentOid);
        if (gid) {
            var mode = this.transparentLayers[node.ifcObject.object._t];
            if (mode) {
                if (mode == 1) {
                    value = this.getAlphaValueFromIfcType(node.ifcObject.getType(), 1) / 2;
                } else {
                    value = 0;
                }
            }
            CMDBuildUI.util.bim.SceneTree.showTransparent(currentOid, gid, value);
        }
        for (var i = 0; i < node.children.length; i++) {
            this.showTransparentRecusively(node.children[i], this.GlobalCurrentOid, value);
        }
    },
    /**
     *  updates the value for the layer with full opacity
     * @param {String} layername the ifc layer name 
    */
    showLayer: function (layerName) {
        delete this.transparentLayers[layerName];
    },

    /**
     *  updates the value for the layer with half transparence
     * @param {String} layername the ifc layer name 
    */
    semiHideLayer: function (layerName) {
        this.transparentLayers[layerName] = 1;
    },

    /**
     *  updates the value for the layer not visible
     * @param {String} layername the ifc layer name 
    */
    hideLayer: function (layerName) {
        this.transparentLayers[layerName] = 2;
    },
    /**
     * @param  {Number} poid the identifier for the bim project
     * @param  {String} type Ifc4 or Ifc2x3tc1
     * @param  {} observer
    */

    /**
     * 
     */
    defaultView: function () {
        CMDBuildUI.util.bim.SceneTree.viewFromDefault();
    },
    /**
     * 
     */
    sideView: function () {
        CMDBuildUI.util.bim.SceneTree.viewFromSide();
    },
    /**
     * 
     */
    topView: function () {
        CMDBuildUI.util.bim.SceneTree.viewFromTop();
    },
    /**
     * 
     */
    frontView: function () {
        CMDBuildUI.util.bim.SceneTree.viewFromFront();
    },
    /**
        * @param {string} IfcType describes the type of the ifc Object
        * @param {int} defaultValue the default value to return if the type is not stored in memory as costant object 
        * @return {int} a value between 0 and 1 representing the alpha (trasparency) of the IfcObject. if not found return the defaultValue 
        */
    getAlphaValueFromIfcType: function (IfcType, defaultValue) {
        if (IfcType) {
            type = BIMSURFER.Constants.materials[IfcType];
            if (type) {
                return type.a;
            }
        }
        return defaultValue;
    },
    show: function (poid, type, observer) {
        _this = this;
        var email = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.bim.user);
        var password = CMDBuildUI.util.helper.Configurations.get(CMDBuildUI.model.Configuration.bim.password);

        this._init();

        this.bimServerClient.init(function () {
            console.log(arguments, "Bimserver.Init Function");
            loginBimServer.call(_this);
        });

        /**
         * 
         */
        function loginBimServer() {

            this.bimServerClient.login(email, password,
                /**
                 * 
                 */
                function () {

                    console.log(arguments, "Login _this.bimserverClient");
                    CMDBuildUI.util.bim.ProjectLoader._init(_this.bimServerClient);
                    CMDBuildUI.util.bim.SceneTree._init(_this.bimServerClient, 'divBim3DView', _this);
                    resize();

                    CMDBuildUI.util.bim.SceneTree.setupViewer();

                    var restoreRoot = true;
                    CMDBuildUI.util.bim.ProjectLoader.showProject(poid, type,
                        function (project, model) {
                            CMDBuildUI.util.bim.IfcTree._init(project, restoreRoot); //when true the ifcTree.ifcTreeRoot is reinitialized, is made for multiple projects
                            restoreRoot = false;
                            Ext.GlobalEvents.fireEventArgs("ifctreeready", null);
                            //FUTURE: CMDBuildUI.util.bim.SceneTree.getViewer().SYSTEM.events.register('mouseDown', this.mouseDown, this);
                            //FUTURE: CMDBuildUI.util.bim.SceneTree.getViewer().SYSTEM.events.register('mouseUp', this.mouseUp, this);
                            //CMDBuildUI.util.bim.SceneTree.getViewer().SYSTEM.events.register('pick', this.pick, this);


                            displayInWebGl(model);//TODO:continut here
                        }, _this);

                });
        }
        /**
         * 
         * @param {*} model 
         */
        function displayInWebGl(model) {
            var ifcOidsArray = [];
            var ifcObjectArray = CMDBuildUI.util.bim.IfcTree.getIfcObjectArray();
            ifcObjectArray.forEach(function (ifcObject) {
                ifcOidsArray.push(ifcObject.oid);
            }, this);
            var objectArray = [];
            var oldModeArray = [];

            ifcOidsArray.forEach(function (oid) {
                model.get(oid, function (object) {
                    if (object != null) {
                        var oldMode = object.trans.mode;
                        var tmpMode = _this.transparentLayers[object.object._t];
                        tmpMode ? object.trans.mode = tmpMode : object.trans.mode = 0;
                        objectArray.push(object);
                        oldModeArray.push(oldMode);
                    }
                });
            }, this);

            CMDBuildUI.util.bim.SceneTree.render3D(objectArray);
        }

        /**
         * 
         */
        function resize() {
            CMDBuildUI.util.bim.SceneTree.resize($('div#divBim3DView').width(), $('div#divBim3DView').height());
        }
    },

    _init: function (callback) {
        this.reset();
        var bimServerBaseUrl = CMDBuildUI.util.Config.bimserverBaseUrl;
        this.bimServerClient = new BimServerClient(bimServerBaseUrl, null);
    },

    /**
     * reset the values of the local variables
     */
    reset: function () {
        // HACK: For loading a a project and setting the transparency mode 0 --> default , 1 --> half transparency, 2 --> make object invisible 
        this.bimServerClient = null;
        this.transparentLayers = {
            IfcSpace: 2
        };
    },

    /**
     * 
     */
    mouseDown: function (hit) {
        var timeInMs = Date.now();
        this.mouseDown = {
            time: timeInMs,
            screenX: hit.screenX,
            screenY: hit.screenY
        }
    },

    /**
     * 
     */
    mouseUp: function (hit) {
        var me = this;
        if (equalsHits(hit, this.mouseDown) && (Date.now() - this.mouseDown.time) > 1000) {
            CMDBuild.bim.proxy.Bim.fetchCardFromViewewId({
                params: {
                    revisionId: me.delegate.getCurrentRoid(),
                    objectId: me.pickedOid
                },

                success: function (fp, request, response) {
                    if (response.card) {
                        me.delegate.openCardDataWindow(response.card);
                    }
                    else {
                        //console.log("! found");
                    }
                }
            });

        }
    },

    // /**
    //  * 
    //  */
    // pick: function (hit) {
    //     // Some plugins wrap things in this name to
    //     // avoid them being picked, such as skyboxes
    //     if (hit.name == "__SceneJS_dontPickMe") {
    //         return;
    //     }
    //     if (this.delegate) {
    //         this.pickedOid = CMDBuildUI.util.bim.SceneTree.getOid(hit.nodeId)
    //         this.delegate.selectGraphicNode(this.pickedOid);
    //     }
    // },

    privates: {
        /**
         * BimServerClient instance
        */
        bimServerClient: null,

        /**
         * Set the default values sono layers. see this.reset() function
         * 
        */
        transparentLayers: {}

        // /**
        //  * the string containing informatin about the bimserver url
        //  */
        // bimServerBaseUrl: null
    }
});