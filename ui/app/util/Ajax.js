Ext.define('CMDBuildUI.util.Ajax', {
    singleton: true,
    /**
     * Pending counter for automated test
     */
    currentPendingCount: 0,
    currentPendingDebug: false,

    /**
     * Initialize singleton
     */
    init: function () {
        Ext.Ajax.setTimeout(CMDBuildUI.util.Config.ajaxTimeout || 15000);
        // TODO: add debug logger to log setTimeout operation

        this.initBeforeRequest();
        this.initRequestComplete();
        this.initRequestException();
    },

    /**
     * Initialize beforerequest event handler
     */
    initBeforeRequest: function () {
        /**
         * Fired before a network request is made to retrieve a data object.
         * 
         * @param {Ext.data.Connection} conn
         * @param {Object} options
         * @param {Object} eOpts
         */
        Ext.Ajax.on('beforerequest', function (conn, options, eOpts) {
            this.currentPending('add');
            var isSilentRequest = this.isSilentRequest(options);
            if (!isSilentRequest) {
                // TODO: do mask
                // console.debug("Mask request");
            }

            // set headers
            var headers = {
                'Content-Type': "application/json",
                'CMDBuild-ActionId': CMDBuildUI.util.Ajax.getActionId(),
                'CMDBuild-RequestId': CMDBuildUI.util.Utilities.generateUUID()
            };

            // add authentication token
            var token = CMDBuildUI.util.helper.SessionHelper.getToken();
            if (token) {
                headers['CMDBuild-Authorization'] = token;
            }

            // add localization
            var localization = CMDBuildUI.util.helper.SessionHelper.getLanguage();
            if (localization) {
                headers['CMDBuild-Localized'] = true;
                headers['CMDBuild-Localization'] = localization;
            }

            //remove _id from PUT request
            if (options.method === 'PUT') {
                Ext.Array.each(options.records, function (value, index) {
                    if (options.jsonData && options.jsonData.hasOwnProperty('_id')) {
                        delete options.jsonData._id;
                    }
                });
            }

            // merge options with custom headers
            Ext.merge(options, {
                headers: headers
            });
        }, this);
    },

    /**
     * Initialize requestcomplete event handler
     */
    initRequestComplete: function () {
        /**
         * Fired if the request was successfully completed.
         * 
         * @param {Ext.data.Connection} conn
         * @param {Object} response
         * @param {Object} options
         * @param {Object} eOpts
         */
        Ext.Ajax.on('requestcomplete', function (conn, response, options, eOpts) {
            var isSilentRequest = this.isSilentRequest(options);
            if (!isSilentRequest) {
                // TODO: do unmask
                // console.debug("Unmask request");
            }
            this.currentPending('sub');
        }, this);
    },

    /**
     * Initialize requestexception event handler
     */
    initRequestException: function () {
        /**
         * Fired if an error HTTP status was returned from the server.
         * 
         * @param {Ext.data.Connection} conn
         * @param {Object} response
         * @param {Object} options
         * @param {Object} eOpts
         */
        Ext.Ajax.on('requestexception', function (conn, response, options, eOpts) {
            var isSilentRequest = this.isSilentRequest(options);
            if (!isSilentRequest) {
                // TODO: do unmask
                // console.debug("Unmask request");
            }

            if (response.status === 401) {
                // Cmdb.Logger.debug("Got unauthorized (401) status from server, check session...");
                CMDBuildUI.util.helper.SessionHelper.checkSessionValidity().then(function (token) {
                    CMDBuildUI.util.Logger.log("You cannot access this resource.", CMDBuildUI.util.Logger.levels.warn, 401);
                }, function (err) {
                    if (!CMDBuildUI.util.helper.SessionHelper.getStartingUrl()) {
                        CMDBuildUI.util.helper.SessionHelper.updateStartingUrlWithCurrentUrl();
                    }
                    CMDBuildUI.util.Utilities.redirectTo("login", true);
                });
            } else {
                var errormessage = CMDBuildUI.util.Ajax.getErrorMessage(response, options);
                if (options.hideErrorNotification) {
                    CMDBuildUI.util.Logger.log(errormessage.message, CMDBuildUI.util.Logger.error, errormessage.code);
                } else if (response.status !== -1) {
                    CMDBuildUI.util.Notifier.showErrorMessage(errormessage.usermessage, errormessage.code, undefined, errormessage.message);
                }
            }
        }, this);
    },

    /**
     * A silent request is a request without mask.
     * 
     * @param {Object} options
     * @param {Boolean} [options.silenRequest]
     * @param {Boolean} [options.operation.config.silentRequest]
     * @return {Boolean}
     * @private
     */
    isSilentRequest: function (options) {
        // try {
        //     return options.silentRequest || options.operation.config.silentRequest;
        // } catch (err) {
        return false;
        // }
    },

    /** 
     * 
     * Get Javascript global variable tracking the number of pending http requests from client (issue #710)
     * 
     * @global CMDBuildUI.util.Ajax.currentPending(null|add|sub|subtract|enable-debug|disable-debug|reset); // return [0-9]
     * 
     * @param {String} operation 
     * @returns {Number} currentPendingCount
     */
    currentPending: function (operation) {
        switch (operation) {
            case 'add':
                this.currentPendingCount++;
                break;
            case 'sub':
            case 'subtract':
                this.currentPendingCount--;
                break;
            case 'enable-debug':
                this.currentPendingDebug = true;
                break;
            case 'disable-debug':
                this.currentPendingDebug = false;
                break;
            case 'reset':
                this.currentPendingCount = 0;
                break;

        }
        if (this.currentPendingDebug) {
            CMDBuildUI.util.Logger.log(Ext.String.format('Pending ajax: {0}',this.currentPendingCount));
        }
        return this.currentPendingCount;
    },

    /**
     * @return {String}
     */
    getActionId: function () {
        return this._actionid;
    },

    /**
     * @param {String} actionid
     */
    setActionId: function (actionid) {
        this._actionid = actionid;
    },

    privates: {
        _actionid: null,

        /**
         * @param {Object} response
         * @param {Object} options
         * @return {Object} An object conaining error message and error code.
         */
        getErrorMessage: function (response, options) {
            var oresponse = Ext.JSON.decode(response.responseText, true);
            var errors = {
                usermessage: CMDBuildUI.locales.Locales.notifier.genericerror,
                message: response.statusText
            };
            if (oresponse && oresponse.messages) {
                var usermessages = [];
                var messages = [];
                oresponse.messages.forEach(function (m) {
                    if (m.show_user) {
                        usermessages.push(m.message);
                    } else {
                        messages.push(m.message);
                    }
                });
                if (usermessages.length) {
                    errors.usermessage = usermessages.join("<br />");
                }

                if (messages.length) {
                    errors.message = messages.join("<br />");
                }
            }
            return errors;
        }
    }
});