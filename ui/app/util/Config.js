Ext.define('CMDBuildUI.util.Config', {
    singleton: true,

    baseUrl: 'http://localhost:8080/cmdbuild/services/rest/v3',
    geoserverBaseUrl: 'http://localhost:8080/geoserver',
    bimserverBaseUrl: 'http://bimserver3.cmdbuild.org/bimserver',
    ajaxTimeout: 15000, // milliseconds
    uiBaseUrl: window.location.origin + window.location.pathname,

    ui: {
        relations: {
            collapsedlimit: 10
        }
    },

    widgets: {
        customForm: 'widgets-customform-panel',
        linkCards: 'widgets-linkcards-panel',
        createModifyCard: 'widgets-createmodifycard-panel',
        createReport: 'widgets-createreport-panel',
        openAttachment: 'widgets-openattachment-panel'
    }
});
