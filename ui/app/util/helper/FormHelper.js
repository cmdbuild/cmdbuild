Ext.define('CMDBuildUI.util.helper.FormHelper', {
    singleton: true,

    formmodes: {
        create: 'create',
        update: 'update',
        read: 'read'
    },

    fieldmodes: {
        hidden: 'hidden',
        immutable: 'immutable',
        read: 'read',
        write: 'write'
    },

    properties: {
        padding: '0 15 0 15'
    },
    fieldDefaults: {
        labelAlign: 'top',
        labelPad: 2,
        labelSeparator: '',
        anchor: '100%'
    },

    /**
     * @param {String} fieldname
     * @return {String} The store id
     * @private
     */
    getStoreId: function (fieldname) {
        return fieldname + "Store";
    },

    /**
     * Get form fields
     * @param {Ext.data.Model} model
     * @param {Object} config
     * @param {Boolean} config.mode
     * @param {Object[]} config.defaultValues An array of objects containing default values.
     * @param {String} config.linkName The name of the object linked within the ViewModel.
     * @param {Object} config.attributesOverrides An object containing properties to override for attributes
     * @param {Boolean} config.attributesOverrides.attributename.writable Override writable property
     * @param {Boolean} config.attributesOverrides.attributename.mandatory Override mandatory property
     * @param {Boolean} config.attributesOverrides.attributename.hidden Override writable property
     * @param {Numeric} config.attributesOverrides.attributename.index Override index property
     * @return {Object[]} the list of form fields
     */
    getFormFields: function (model, config) {
        var items = [];
        config = config || {};
        var me = this;

        // set default configuration
        Ext.applyIf(config, {
            readonly: false,
            attributesOverrides: {},
            mode: config.readonly ? this.formmodes.read : this.formmodes.update
        });

        Ext.Array.each(model.getFields(), function (modelField, index) {
            var field = Ext.apply({}, modelField);
            if (!field.name.startsWith("_")) {
                var defaultValue;
                // check and use default values
                if (!Ext.isEmpty(config.defaultValues)) {
                    defaultValue = Ext.Array.findBy(config.defaultValues, function (item, index) {
                        if (item.value) {
                            if (field.cmdbuildtype.toLowerCase() === CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase()) {
                                return (item.attribute && item.attribute === field.name) ||
                                    (item.domain && item.domain === field.metadata.domain);
                            } else {
                                return item.attribute && item.attribute === field.name;
                            }
                        }
                    });
                }

                var overrides = config.attributesOverrides[field.name];
                if (overrides) {
                    if (!Ext.isEmpty(overrides.index)) {
                        field.metadata.index = overrides.index;
                    }
                    if (!Ext.isEmpty(overrides.mandatory)) {
                        field.mandatory = overrides.mandatory;
                    }
                    if (!Ext.isEmpty(overrides.writable)) {
                        field.writable = overrides.writable;
                    }
                    if (!Ext.isEmpty(overrides.hidden)) {
                        field.hidden = overrides.hidden;
                    }
                }

                var formfield = me.getFormField(field, {
                    mode: config.mode,
                    defaultValue: defaultValue,
                    linkName: config.linkName,
                    // overrides: config.attributesOverrides[field.name]
                });

                items.push(formfield);
            }
        });

        // sort attributes on index property
        return items.sort(function (a, b) {
            return a.metadata.index - b.metadata.index;
        });
    },

    /**
     * Get form fields
     * 
     * @param {Ext.data.field.Field} field
     * @param {String} config.mode One of `read`, `create` or `update`.
     * @param {Object} config.defaultValue An object containing default value.
     * @param {String} config.linkName The name of the object linked within the ViewModel.
     * @return {Object} An `Ext.form.field.Field` definition.
     */
    getFormField: function (field, config) {
        var fieldsettings;

        config = Ext.applyIf(config, {
            linkName: this._default_link_name,
            mode: this.formmodes.read
        });

        // append asterisk to label for mandatory fields
        var label = field.metadata.description_localized || field.description;
        if (field.mandatory === true) {
            label += ' *';
        }

        var bind = {};
        if (config.linkName) {
            bind = {
                value: Ext.String.format('{{0}.{1}}', config.linkName, field.name)
            };
        }

        // base field information
        var formfield = {
            fieldLabel: label,
            labelPad: CMDBuildUI.util.helper.FormHelper.properties.labelPad,
            labelSeparator: CMDBuildUI.util.helper.FormHelper.properties.labelSeparator,
            name: field.name,
            hidden: field.hidden,
            anchor: '100%',
            metadata: field.metadata,
            formmode: config.mode,
            bind: bind
        };

        if (config.defaultValue) {
            // add listener to set value when field is added to form
            // to apdate theObject within viewmodel.
            formfield.listeners = {
                beforerender: function (f) {
                    var vm = f.lookupViewModel(true); // get form view model
                    vm.get(config.linkName).set(field.name, config.defaultValue.value);
                    if (config.defaultValue.valuedescription) {
                        vm.get(config.linkName).set(
                            Ext.String.format("_{0}_description", field.name),
                            config.defaultValue.valuedescription
                        );
                    }
                }
            };
            if (!Ext.isEmpty(config.defaultValue.editable)) {
                field.writable = config.defaultValue.editable;
            }
        }

        if (
            config.mode !== this.formmodes.read &&
            field.writable &&
            (field.mode != this.fieldmodes.immutable || config.mode === this.formmodes.create)
        ) {
            fieldsettings = this.getEditorForField(
                field, {
                    linkName: config.linkName
                }
            );
        }

        if (!fieldsettings) {
            fieldsettings = this.getReadOnlyField(field, config.linkName);
        }

        // override mandatory behaviour
        if (field.mandatory && !field.hidden) {
            fieldsettings.allowBlank = false;
        }
        Ext.merge(formfield, fieldsettings);

        return formfield;
    },

    /**
     * Returns the editor definition for given field
     * @param {Ext.data.field.Field} field
     * @param {Object} config
     * @param {String} config.linkName
     * @return {Object}
     */
    getEditorForField: function (field, config) {
        var editor;
        config = config || {};

        config = Ext.applyIf(config, {
            linkName: this._default_link_name
        });

        // field configuration
        switch (field.cmdbuildtype.toLowerCase()) {
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.boolean.toLowerCase():
                editor = {
                    xtype: 'checkboxfield'
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.char.toLowerCase():
                editor = {
                    xtype: 'textfield',
                    enforceMaxLength: true,
                    maxLength: 1
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.date.toLowerCase():
                //TODO: check localization setting
                editor = {
                    xtype: 'datefield',
                    format: CMDBuildUI.locales.Locales.common.dates.date
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.double.toLowerCase():
                //TODO: check localization setting
                editor = {
                    xtype: 'numberfield'
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.time.toLowerCase():
                //TODO: check localization setting
                //TODO: check increment setting
                editor = {
                    xtype: 'timefield',
                    format: CMDBuildUI.locales.Locales.common.dates.time,
                    formatText: CMDBuildUI.locales.Locales.common.dates.time,
                    renderer: function (value, field) {
                        return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.time);
                    },
                    increment: 30
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.datetime.toLowerCase():
                //TODO: check localization setting
                editor = {
                    xtype: 'datefield',
                    format: CMDBuildUI.locales.Locales.common.dates.datetime
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.decimal.toLowerCase():
                // TODO: add scale and precision
                editor = {
                    xtype: 'numberfield'
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.integer.toLowerCase():
                editor = {
                    xtype: 'numberfield',
                    hideTrigger: true
                    // keyNavEnabled: false,
                    // mouseWhellEnabled: false
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.ipaddress.toLowerCase():
                // TODO: add ip mask
                editor = {
                    xtype: 'textfield'
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase():
                editor = {
                    xtype: 'lookupfield',
                    recordLinkName: config.linkName
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase():
                if (CMDBuildUI.util.helper.ModelHelper.getObjectFromName(field.metadata.targetClass, field.metadata.targetType)) {
                    editor = {
                        xtype: 'referencefield',
                        recordLinkName: config.linkName
                    };
                }
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.foreignkey.toLowerCase():
                editor = {
                    xtype: 'referencefield',
                    recordLinkName: config.linkName
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.string.toLowerCase():
                editor = {
                    xtype: 'textfield',
                    enforceMaxLength: true,
                    maxLength: field.metadata.maxLength
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.text.toLowerCase():
                if (field.metadata.editorType === "HTML") {
                    editor = {
                        xtype: 'htmleditor',
                        enableAlignments: true,
                        enableColors: false,
                        enableFont: false,
                        enableFontSize: true,
                        enableFormat: true,
                        enableLinks: true,
                        enableLists: true,
                        enableSourceEdit: true
                    };
                } else {
                    editor = {
                        xtype: 'textareafield',
                        resizable: true
                    };
                }
                break;
            default:
                CMDBuildUI.util.Logger.log("Missing field for " + field.name, CMDBuildUI.util.Logger.levels.warn);
                break;
        }

        // Add help tooltip
        if (editor && field.metadata && field.metadata.cm_help) {
            var converter = new showdown.Converter();
            var help = converter.makeHtml(field.metadata.cm_help);
            editor.ariaHelp = help;
            editor.afterLabelTextTpl = Ext.String.format(
                '<span class="x-fa fa-question-circle x-form-item-label-help" data-qtip="{0}" data-hide="user" data-qalign="t-b"></span>',
                Ext.String.htmlEncode(help)
            );
        }

        // add updateVisibility function
        this.addUpdateVisibilityToField(editor, field.metadata, config.linkname);

        // add custom validator
        this.addCustomValidator(editor, field.metadata, config.linkName);

        // append metadata to editor configuration
        if (Ext.isObject(editor) && !Ext.Object.isEmpty(editor)) {
            editor.metadata = field.metadata;
        }

        return editor;
    },

    /**
     * 
     * @param {Ext.data.field.Field} field
     * @param {String} linkName 
     * @return {Object}
     */
    getReadOnlyField: function (field, linkName) {
        // setup readonly fields
        var fieldsettings = {
            xtype: 'displayfield',
            fieldLabel: field.metadata.description_localized || field.description // override label for mandatory fields
        };

        switch (field.cmdbuildtype.toLowerCase()) {
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase():
                fieldsettings.bind = {
                    value: Ext.String.format('{{0}._{1}_description_translation}', linkName, field.name)
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase():
                fieldsettings.bind = {
                    value: Ext.String.format('{{0}._{1}_description}', linkName, field.name)
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.foreignkey.toLowerCase():
                fieldsettings.bind = {
                    value: Ext.String.format('{{0}._{1}_description}', linkName, field.name)
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.date.toLowerCase():
                fieldsettings.renderer = function (value, field) {
                    return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.date);
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.time.toLowerCase():
                fieldsettings.renderer = function (value, field) {
                    return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.time);
                };
                break;
            case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.datetime.toLowerCase():
                fieldsettings.renderer = function (value, field) {
                    return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.datetime);
                };
                break;
        }

        // add updateVisibility function
        this.addUpdateVisibilityToField(fieldsettings, field.metadata, linkName);

        return fieldsettings;
    },

    /**
     * Returns store definition for LookUp store.
     * @param {String} type
     * @return {Object} Ext.data.Store definition
     */
    getLookupStore: function (type) {
        return {
            model: 'CMDBuildUI.model.lookups.Lookup',
            proxy: {
                url: CMDBuildUI.util.api.Lookups.getLookupValues(type),
                type: 'baseproxy'
            },
            autoLoad: true
        };
    },

    /**
     * Returns store definition for Reference store.
     * @param {String} type Target type.
     * @param {String} name Target name.
     * @return {Object} Ext.data.Store definition
     */
    getReferenceStore: function (type, name) {
        if (type === 'class') {
            return {
                model: 'CMDBuildUI.model.domains.Reference',
                proxy: {
                    url: '/classes/' + name + '/cards/',
                    type: 'baseproxy'
                },
                autoLoad: true
            };
        }
    },

    /**
     * Return the base form for given model
     * @param {Ext.Model} model
     * @param {Object} config
     * @param {Boolean} config.mode Default value is true.
     * @param {Object[]} config.defaultValues An array of objects containing default values.
     * @param {String} config.linkName The name of the object linked within the ViewModel.
     * @param {Boolean} config.showNotes Show notes as new tab.
     * @param {Boolean} config.showAsFieldsets Set to true for display fieldsets instead of tabs.
     * @param {Object} config.attributesOverrides An object containing properties to override for attributes
     * @param {Boolean} config.attributesOverrides.attributename.writable Override writable property
     * @param {Boolean} config.attributesOverrides.attributename.mandatory Override mandatory property
     * @param {Numeric} config.attributesOverrides.attributename.index
     * @param {String[]} config.visibleAttributes An array containing the names of visible attributes
     * @return {CMDBuildUI.components.tab.FormPanel|CMDBuildUI.components.tab.FieldSet[]}
     */
    renderForm: function (model, config) {
        // set default configuration
        Ext.applyIf(config || {}, {
            readonly: true,
            defaultValues: [],
            linkName: this._default_link_name,
            showNotes: false,
            showAsFieldsets: false,
            attributesOverrides: {},
            visibleAttributes: [],
            mode: config.readonly == undefined || config.readonly ? this.formmodes.read : this.formmodes.update
        });

        // get form fields
        var fields = CMDBuildUI.util.helper.FormHelper.getFormFields(model, {
            readonly: config.readonly,
            defaultValues: config.defaultValues,
            linkName: config.linkName,
            attributesOverrides: config.attributesOverrides,
            mode: config.mode
        });

        var tabs = [];
        Ext.Array.each(fields, function (field, index) {
            // add only non-excluded fields
            if (config.visibleAttributes.length === 0 || Ext.Array.indexOf(config.visibleAttributes, field.name) !== -1) {
                var tab = Ext.Array.findBy(tabs, function (item, index) {
                    return item.reference === field.metadata.group.name;
                });
                // create tab key if not exists
                if (!tab) {
                    tab = {
                        reference: field.metadata.group.name,
                        description: field.metadata.group.label,
                        position: tabs.length,
                        fields: []
                    };
                    tabs.push(tab);
                }
                // add item
                tab.fields.push(field);
            }
        });

        // add tenant field
        if (CMDBuildUI.util.helper.Configurations.get("cm_system_multitenant_enabled")) {
            var objectdefinition = CMDBuildUI.util.helper.ModelHelper.getObjectFromName(model.objectTypeName, model.objectType);
            var multitenantMode = objectdefinition.get("multitenantMode");
            if (
                multitenantMode === CMDBuildUI.model.users.Tenant.tenantmodes.always ||
                multitenantMode === CMDBuildUI.model.users.Tenant.tenantmodes.mixed
            ) {
                var tenantfield = this.getTenantField(config.mode, multitenantMode);
                if (tenantfield) {
                    tabs[0].fields = Ext.Array.merge([tenantfield], tabs[0].fields);
                }
            }
        }

        // create base pagination
        var items = [];
        var tabindex = 1;
        Ext.Array.each(tabs, function (tab, index) {
            var left = [], right = [], counter = 0;
            tab.fields.forEach(function (f, fi) {
                if (!f.hidden) {
                    f.tabIndex = tabindex++;
                    if (counter % 2) {
                        right.push(f);
                    } else {
                        left.push(f);
                    }
                    counter++;
                }
            });
            items.push({
                title: tab.description,
                layout: 'column',
                // layout: 'vbox',
                defaults: {
                    xtype: 'fieldcontainer',
                    columnWidth: 0.5,
                    flex: '0.5',
                    padding: CMDBuildUI.util.helper.FormHelper.properties.padding,
                    layout: 'anchor'
                },
                items: [{
                    items: left
                }, {
                    items: right
                }]
            });
        });

        // add notes in a new page
        if (config && config.showNotes) {
            items.push({
                title: 'Notes', // TODO: translate
                reference: "_notes",
                items: [{
                    xtype: 'displayfield',
                    name: 'Notes',
                    anchor: '100%',
                    padding: CMDBuildUI.util.helper.FormHelper.properties.padding,
                    bind: {
                        value: Ext.String.format("{{0}.Notes}", config.linkName)
                    }
                }]
            });
        }

        // return as fieldsets
        if (config.showAsFieldsets) {
            // set fieldset configurations
            for (var i = 0; i < items.length; i++) {
                Ext.apply(items[i], {
                    xtype: 'formpaginationfieldset',
                    collapsible: items.length > 1
                });
            }
            return items;
        }

        // hide tab in tabbar if there is only one tab
        if (items.length === 1) {
            items[0].tabConfig = {
                cls: 'hidden-tab'
            };
        }
        // return tab panel
        return {
            xtype: 'formtabpanel',
            items: items
        };
    },

    privates: {
        _default_link_name: 'theObject',

        /**
         * @param {String} formmode
         * @param {String} multitenantmode
         * @return {Object}
         */
        getTenantField: function (formmode, multitenantmode) {
            var tenants = CMDBuildUI.util.helper.SessionHelper.getActiveTenants();
            var writable = formmode === this.formmodes.update || formmode === this.formmodes.create;
            if (writable) {
                // add combobox
                return {
                    xtype: 'combobox',
                    fieldLabel: 'Tenant',
                    displayField: 'description',
                    valueField: 'code',
                    queryMode: 'local',
                    anchor: '100%',
                    forceSelection: true,
                    allowBlank: multitenantmode !== CMDBuildUI.model.users.Tenant.tenantmodes.always,
                    bind: {
                        value: '{theObject._tenant}'
                    },
                    store: {
                        data: tenants
                    },
                    triggers: {
                        clear: {
                            cls: 'x-form-clear-trigger',
                            handler: function (combo, trigger, eOpts) {
                                combo.clearValue();
                            }
                        }
                    }
                };
            } else {
                if (tenants.length) {
                    return {
                        xtype: 'displayfield',
                        fieldLabel: 'Tenant',
                        bind: {
                            value: '{theObject._tenant}'
                        },
                        renderer: function (value, field) {
                            var t = Ext.Array.findBy(tenants, function (i) {
                                return i.code == value;
                            });
                            if (t) {
                                return t.description;
                            }
                            return "";
                        }
                    };
                }
            }
            return;
        },

        /**
         * Add update visibility function to field
         * @param {Object} config Ext.form.Field configuration
         * @param {Object} fieldMeta Field metadata
         * @param {Object} fieldMeta.cm_showIf Show if code
         * @param {String} linkname
         */
        addUpdateVisibilityToField: function (config, fieldMeta, linkname) {
            linkname = linkname || this._default_link_name;
            // Add show if control
            if (config && fieldMeta && !Ext.isEmpty(fieldMeta.cm_showIf)) {
                /* jshint ignore:start */
                var jsfn = Ext.String.format(
                    'function executeShowIf(api) {{0}}',
                    fieldMeta.cm_showIf
                );
                eval(jsfn);
                /* jshint ignore:end */
                config.updateFieldVisibility = function () {
                    var form = this.up("form");
                    var record = form && form.getViewModel() ? form.getViewModel().get(linkname) : null;
                    var api = {
                        record: record,
                        getValue: CMDBuildUI.util.api.Client.getValue,
                        getLookupCode: CMDBuildUI.util.api.Client.getLookupCode,
                        getLookupDescription: CMDBuildUI.util.api.Client.getLookupDescription,
                        getReferenceDescription: CMDBuildUI.util.api.Client.getReferenceDescription,
                        testRegExp: CMDBuildUI.util.api.Client.testRegExp
                    };

                    // use try / catch to manage errors
                    try {
                        if (executeShowIf(api)) {
                            this.setHidden(false);
                        } else {
                            this.setHidden(true);
                        }
                    } catch (e) {
                        CMDBuildUI.util.Logger.log(
                            "Error on showIf configuration for field " + this.getFieldLabel(),
                            CMDBuildUI.util.Logger.levels.error,
                            null,
                            e
                        );
                    }
                };
            }
        },

        /**
         * Add update visibility function to field
         * @param {Object} config Ext.form.Field configuration
         * @param {Object} fieldMeta Field metadata
         * @param {Object} fieldMeta.cm_validationRules Validation rules code
         * @param {String} linkname
         */
        addCustomValidator: function (config, fieldMeta, linkname) {
            linkname = linkname || this._default_link_name;
            if (config && fieldMeta && !Ext.isEmpty(fieldMeta.cm_validationRules)) {
                /* jshint ignore:start */
                var jsfn = Ext.String.format(
                    'function executeValidationRules(value, api) {{0}}',
                    fieldMeta.cm_validationRules
                );
                eval(jsfn);
                /* jshint ignore:end */

                config.getValidation = function () {
                    var value = this.getValue();
                    if (this.validation !== null && this.validation !== true) {
                        return this.validation;
                    }

                    var form = this.up("form");
                    var record = form && form.getViewModel() ? form.getViewModel().get(linkname) : null;
                    var api = {
                        record: record,
                        getValue: CMDBuildUI.util.api.Client.getValue,
                        getLookupCode: CMDBuildUI.util.api.Client.getLookupCode,
                        getLookupDescription: CMDBuildUI.util.api.Client.getLookupDescription,
                        getReferenceDescription: CMDBuildUI.util.api.Client.getReferenceDescription,
                        testRegExp: CMDBuildUI.util.api.Client.testRegExp
                    };

                    var errors = null;
                    // use try / catch to manage errors
                    try {
                        errors = executeValidationRules(value, api);
                        if (Ext.isBoolean(errors) && errors === false) {
                            errors = "Error"; // TODO: translate
                        }
                    } catch (e) {
                        CMDBuildUI.util.Logger.log(
                            "Error on validationRules configuration for field " + this.getFieldLabel(),
                            CMDBuildUI.util.Logger.levels.error,
                            null,
                            e
                        );
                    }

                    return errors;
                };
            }
        }
    }

});