Ext.define('CMDBuildUI.util.helper.UserPreferences', {
    singleton: true,

    /**
     * Load user preferences
     * 
     * @return {Ext.promise.Promise} Resolve method has as argument an 
     *      instance of {CMDBuildUI.store.users.Preferences}. 
     *      Reject method has as argument a {String} containing error message.
     */
    load: function () {
        // create deferred instance
        var deferred = new Ext.Deferred();
        var store = Ext.getStore("users.Preferences");

        // set store proxy
        var url = Ext.String.format("/sessions/{0}/preferences", CMDBuildUI.util.helper.SessionHelper.getToken());
        store.setProxy({
            url: url,
            type: 'baseproxy'
        });

        // load preferences
        store.load(function (records, operation, success) {
            if (success) {
                deferred.resolve(store.getAt(0));
            } else {
                deferred.reject(operation);
            }
        });

        // returns promise
        return deferred.promise;
    },

    /**
     * @return {CMDBuildUI.store.users.Preferences} Returns null if has not preferences.
     */
    getPreferences: function() {
        return Ext.getStore("users.Preferences").getAt(0);
    }
});