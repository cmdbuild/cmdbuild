Ext.define('CMDBuildUI.util.helper.SessionHelper', {
    singleton: true,

    authorization: 'CMDBuild-Authorization',
    localization: 'CMDBuild-Localization',

    /**
     * @return {String}
     */
    getToken: function () {
        return Ext.util.Cookies.get(CMDBuildUI.util.helper.SessionHelper.authorization);
    },

    /**
     * @param {String} token
     */
    setToken: function (token) {
        if (token) {
            Ext.util.Cookies.set(CMDBuildUI.util.helper.SessionHelper.authorization, token);
        } else {
            CMDBuildUI.util.helper.SessionHelper.clearToken();
        }
    },

    /**
     * 
     */
    clearToken: function () {
        Ext.util.Cookies.clear(CMDBuildUI.util.helper.SessionHelper.authorization);
    },

    /**
     * @return {String}
     */
    getLanguage: function () {
        return Ext.util.Cookies.get(CMDBuildUI.util.helper.SessionHelper.localization);
    },

    /**
     * @param {String} lang
     */
    setLanguage: function (lang) {
        if (lang) {
            Ext.util.Cookies.set(CMDBuildUI.util.helper.SessionHelper.localization, lang);
            this.loadLocale(lang);
        } else {
            CMDBuildUI.util.helper.SessionHelper.clearLanguage();
        }
    },

    /**
     * 
     */
    clearLanguage: function () {
        Ext.util.Cookies.clear(CMDBuildUI.util.helper.SessionHelper.localization);
    },

    /**
     * Check the validity of the token.
     * 
     * @return {Ext.promise.Promise}
     */
    checkSessionValidity: function () {
        var deferred = new Ext.Deferred();
        // get saved token
        var token = CMDBuildUI.util.helper.SessionHelper.getToken();
        
        if (token) {
            // check the validity of the saved token.
            CMDBuildUI.model.users.Session.load(token, {
                hideErrorNotification: true,
                success: function (session, operation) {
                    if (session.get("role") && (Ext.isEmpty(session.get("availableTenants")) || !Ext.isEmpty(session.get("activeTenants")) || session.get("ignoreTenants"))) {
                        CMDBuildUI.util.helper.SessionHelper.setSessionIntoViewport(session);
                        deferred.resolve(token);
                    } else {
                        var err = "Group or tenant not selected.";
                        CMDBuildUI.util.Logger.log(err, CMDBuildUI.util.Logger.levels.debug, 401);
                        deferred.reject(err);
                    }
                },
                failure: function (action) {
                    CMDBuildUI.util.helper.SessionHelper.setToken(null);
                    var err = "Session token expired.";
                    CMDBuildUI.util.Logger.log(err, CMDBuildUI.util.Logger.levels.debug, 401);
                    deferred.reject(err);
                }
            });
        } else {
            var err = "Session token not found.";
            CMDBuildUI.util.Logger.log(err, CMDBuildUI.util.Logger.levels.debug, 401);
            deferred.reject(err);
        }
        return deferred.promise;
    },

    /**
     * Set session object into Viewport
     * 
     * @param {CMDBuildUI.model.users.Session} session
     */
    setSessionIntoViewport: function (session) {
        this.getViewportVM().set("theSession", session);
    },

    /**
     * Get current session.
     * 
     * @return {CMDBuildUI.model.users.Session} Current session
     */
    getCurrentSession: function () {
        return this.getViewportVM().get("theSession");
    },

    /**
     * Get current session.
     * 
     * @param {String} instancename
     */
    updateInstanceName: function (instancename) {
        return this.getViewportVM().set("instancename", instancename);
    },

    /**
     * Implementation of window.sessionStorage.setItem()
     * 
     * @param {String} key The key.
     * @param {*} value The new associated value for `key`.
     * 
     */
    setItem: function (key, value) {
        if(!this.localSessionStorage.id){
            this.localSessionStorage = new Ext.util.LocalStorage({
                id: this.LOCAL_STORAGE_ID,
                session: true
            })
        }
        this.localSessionStorage.setItem(key, Ext.JSON.encode(value));
    },

    /**
     * Implementation of window.sessionStorage.getItem()
     * 
     * @param {String|Number} key The key.
     * @param {*} [defaultValue=null] The default associated value for `key`.
     * @returns {*}
     */
    getItem: function (key, defaultValue) {
        if (this.localSessionStorage.id) {
            return Ext.JSON.decode(this.localSessionStorage.getItem(key)) || defaultValue;
        }
        return defaultValue;
    },

    /**
     * Implementation of window.sessionStorage.removeItem()
     * 
     * @param {String|Number} key The key.
     */
    removeItem: function (key) {
        if(this.localSessionStorage.id){
            this.localSessionStorage.removeItem(key);
        } 
    },

    /**
     * Load localization file.
     * @param {String} lang
     */
    loadLocale: function(lang) {
        if (!lang) {
            lang = this.getLanguage();
        }
        if (lang && lang !== "en") {
            Ext.require([
                Ext.String.format("CMDBuildUI.locales.{0}.LocalesAdministration", lang),
                Ext.String.format("CMDBuildUI.locales.{0}.Locales", lang)
            ]);
        } else if (lang === "en") {
            Ext.require(
                Ext.String.format("CMDBuildUI.locales.Locales", lang)
            );
        }
    },

    /**
     * @param {String} url
     */
    setStartingUrl: function(url) {
        this._startingurl = url;
    },

    /**
     * @return {String} 
     */
    getStartingUrl: function() {
        return this._startingurl;
    },

    /**
     * Sets current url as starting url.
     */
    updateStartingUrlWithCurrentUrl: function() {
        var currentUrl = Ext.History.getToken();
        if (currentUrl.length > 1) {
            CMDBuildUI.util.helper.SessionHelper.setStartingUrl(currentUrl);
        }
    },

    /**
     * Clear starting url.
     */
    clearStartingUrl: function() {
        CMDBuildUI.util.helper.SessionHelper.setStartingUrl(null);
    },

    /**
     * 
     */
    getActiveTenants: function() {
        var session = this.getCurrentSession();
        var activetenants = session.get("activeTenants");
        var availabletenants = session.get('availableTenantsExtendedData');
        var ignoretenants = session.get("ignoreTenants");
        function activeTenantsFilter(value) {
            return ignoretenants || Ext.Array.contains(activetenants, value.code);
        }
        return availabletenants.filter(activeTenantsFilter);
    },

    /**
     * 
     * @param {String[]} tenants 
     */
    updateActiveTenants: function(tenants) {
        this.getCurrentSession().set("activeTenants", tenants);
    },

    privates: {
        /**
         * An Object contains new Ext.util.LocalStorage
         * @type {Ext.util.LocalStorage}
         */
        localSessionStorage: {},
        /**
         * The id param used in new Ext.util.LocalStorage
         * @type {String}
         */
        LOCAL_STORAGE_ID: 'CMDBUILD-SESSION',
        
        /**
         * @property {String} _startingurl
         * The starting url
         */
        _startingurl: null,
        
        /**
         * Get Viewport ViewModel
         * 
         * @return {CMDBuildUI.view.main.MainModel}
         */
        getViewportVM: function () {
            var viewports = Ext.ComponentQuery.query('viewport');
            if (viewports.length) {
                return viewports[0].getViewModel();
            }
        }
    }
});