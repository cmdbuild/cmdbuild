Ext.define('CMDBuildUI.util.helper.GridHelper', {
    singleton: true,

    /**
     * Returns columns definition for grids.
     * 
     * @param {Ext.data.field.Field[]} fields
     * @param {Object} config
     * @param {String[]/Boolean} config.allowFilter An array of columns on which enable filters or true to enable filter for each column.
     * @param {Boolean} config.addTypeColumn If `true` a new column is added as first item with object type.
     * @return {Object[]} An array of Ext.grid.column.Column definitions.
     */
    getColumns: function (fields, config) {
        var columns = [];
        var me = this;

        config = Ext.applyIf(config || {}, {
            allowFilter: false,
            addTypeColumn: false
        });

        if (config.addTypeColumn) {
            columns.push({
                text: CMDBuildUI.locales.Locales.common.grid.subtype,
                dataIndex: "_type",
                hidden: false,
                align: 'left',
                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                    return CMDBuildUI.util.helper.ModelHelper.getObjectDescription(value);
                }
            });
        }

        Ext.Array.each(fields, function (field, index) {
            var column = me.getColumn(field, config);
            if (column) {
                columns.push(column);
            }
        });
        return columns;
    },

    /**
     * Returns column definition.
     * 
     * @param {Ext.data.field.Field} field
     * @param {Object} config
     * @param {String[]/Boolean} config.allowFilter An array of columns on which enable filters or true to enable filter for each column.
     * @return {Object} An `Ext.grid.column.Column` definition.
     */
    getColumn: function (field, config) {
        var column;
        if (!field.name.startsWith("_")) {
            column = {
                text: field.metadata.description_localized,
                dataIndex: field.name,
                hidden: !field.metadata.showInGrid,
                align: 'left'
            };

            switch (field.cmdbuildtype.toLowerCase()) {
                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase():
                    column.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                        var translation = record.get("_" + field.name + "_description_translation");
                        var description = record.get("_" + field.name + "_description");
                        return translation ? translation : description;
                    };
                    break;
                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase():
                    column.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                        return record.get("_" + field.name + "_description");
                    };
                    break;
                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.text.toLowerCase():
                    column.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                        if (field.metadata.editorType === 'HTML') {
                            value = Ext.util.Format.stripTags(value);
                        }
                        return value;
                    };
                    break;
                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.date.toLowerCase():
                    column.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                        return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.date);
                    };
                    break;
                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.time.toLowerCase():
                    column.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                        return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.time);
                    };
                    break;
                case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.datetime.toLowerCase():
                    column.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                        return Ext.util.Format.date(value, CMDBuildUI.locales.Locales.common.dates.datetime);
                    };
                    break;

            }

            // add column filter
            if (config && config.allowFilter !== undefined && (
                (Ext.isBoolean(config.allowFilter) && config.allowFilter === true) ||
                (Ext.isArray(config.allowFilter) && config.allowFilter.indexOf(field.name) !== -1)
            )) {
                switch (field.cmdbuildtype.toLowerCase()) {
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.boolean.toLowerCase():
                        column.filter = {
                            type: 'boolean'
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.char.toLowerCase():
                        column.filter = {
                            type: 'string',
                            itemDefaults: {
                                maxLength: 1
                            }
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.date.toLowerCase():
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.time.toLowerCase():
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.datetime.toLowerCase():
                        column.filter = {
                            type: 'date'
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.decimal.toLowerCase():
                        column.filter = {
                            type: 'numeric',
                            itemDefaults: {
                                decimalPrecision: field.metadata.scale
                            }
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.double.toLowerCase():
                        column.filter = {
                            type: 'numeric'
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.integer.toLowerCase():
                        column.filter = {
                            type: 'numeric',
                            itemDefaults: {
                                decimalPrecision: 0
                            }
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.ipaddress.toLowerCase():
                        column.filter = {
                            type: 'string'
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup.toLowerCase():
                        var lstore = CMDBuildUI.util.helper.FormHelper.getLookupStore(field.metadata.lookupType);
                        lstore.autoLoad = false;
                        column.filter = {
                            type: 'list',
                            cmdbuildtype: CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.lookup,
                            store: lstore,
                            idField: '_id',
                            labelField: 'description'
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference.toLowerCase():
                        var rstore = CMDBuildUI.util.helper.FormHelper.getReferenceStore(field.metadata.targetType, field.metadata.targetClass);
                        if (rstore) {
                            rstore.autoLoad = false;
                            column.filter = {
                                type: 'list',
                                cmdbuildtype: CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.reference,
                                store: rstore,
                                idField: '_id',
                                labelField: 'Description'
                            };
                        }
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.string.toLowerCase():
                        column.filter = {
                            type: 'string'
                        };
                        break;
                    case CMDBuildUI.util.helper.ModelHelper.cmdbuildtypes.text.toLowerCase():
                        column.filter = {
                            type: 'string'
                        };
                        break;
                }
            }
        }
        return column;
    }
});