Ext.define("CMDBuildUI.util.Stores", {
    singleton: true,

    /**
     * Load classes store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadClassesStore: function () {
        var deferred = new Ext.Deferred();
        var store = Ext.getStore('classes.Classes');
        // load classes store
        store.load({
            params: {
                detailed: true // load full data
            },
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading Classes store.");
                }
            }
        });
        return deferred.promise;
    },

    /**
     * Load processes store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadProcessesStore: function () {
        var deferred = new Ext.Deferred();
        var store = Ext.getStore('processes.Processes');
        // load processes store
        store.load({
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading Processes store.");
                }
            }
        });
        return deferred.promise;
    },

    /**
     * Load reprts store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadReportsStore: function () {
        var deferred = new Ext.Deferred();
        var store = Ext.getStore('Reports');
        // load reports store
        store.load({
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading Reports store.");
                }
            }
        });
        return deferred.promise;
    },

    /**
     * Load dashboards store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadDashboardsStore: function () {
        var deferred = new Ext.Deferred();
        var store = Ext.getStore('Dashboards');
        // load reports store
        store.load({
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading Dashboards store.");
                }
            }
        });
        return deferred.promise;
    },

    /**
     * Load views store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadViewsStore: function () {
        var deferred = new Ext.Deferred();
        var store = Ext.getStore('views.Views');
        // load reports store
        store.load({
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading Views store.");
                }
            }
        });
        return deferred.promise;
    },

    /**
     * Load custompages store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadCustomPagesStore: function () {
        var deferred = new Ext.Deferred();
        var store = Ext.getStore('custompages.CustomPages');
        // load reports store
        store.load({
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading CustomPages store.");
                }
            }
        });
        return deferred.promise;
    },

    /**
     * Load menu store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadMenuStore: function () {
        var deferred = new Ext.Deferred();

        // load classes store
        var store = Ext.getStore('menu.Menu');
        store.setRoot({
            expanded: false
        });
        store.load(function (records, operation, success) {
            if (success) {
                deferred.resolve(records);
            } else {
                deferred.reject("Error loading Menu items store.");
            }
        });

        return deferred.promise;
    },

    /**
     * Load lookup types store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadLookupTypesStore: function () {
        var deferred = new Ext.Deferred();

        // load classes store
        var store = Ext.getStore('lookups.LookupTypes');
        // load reports store
        store.load({
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading Lookup Types store.");
                }
            }
        });
        return deferred.promise;
    },

    /**
     * Load domains store.
     * 
     * @return {Ext.promise.Promise}
     */
    loadDomainsStore: function () {
        var deferred = new Ext.Deferred();

        // load classes store
        var store = Ext.getStore('domains.Domains');
        // load reports store
        store.load({
            callback: function (records, operation, success) {
                if (success) {
                    deferred.resolve(records);
                } else {
                    deferred.reject("Error loading Domains store.");
                }
            }
        });
        return deferred.promise;
    }

});