Ext.define("CMDBuildUI.util.administration.MenuStoreBuilder", {
    singleton: true,
    mixins: ['Ext.mixin.Observable'],

    initialize: function (callback) {
        this.onReloadAdminstrationMenu(callback);
    },

    /**
     * 
     */
    onReloadAdminstrationMenu: function (callback) {
        var me = this;
        var store = Ext.getStore('administration.MenuAdministration');
        var childrens = [];

        Ext.Promise.all([
            me.appendClassesAdminMenu().then(function (classes) {
                childrens[0] = classes;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Classes NOT LOADED!');
            }),
            me.appendProcessesAdminMenu().then(function (processes) {
                childrens[1] = processes;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Processes NOT LOADED!');
            }),
            me.appendDomainsAdminMenu().then(function (domains) {
                childrens[2] = domains;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Domains NOT LOADED!');
            }),
            me.appendLookupTypesAdminMenu().then(function (lookupTypes) {
                childrens[3] = lookupTypes;
            }, function () {
                Ext.Msg.alert('Error', 'Menu LookTypes NOT LOADED!');
            }),
            me.appendViewsAdminMenu().then(function (views) {
                childrens[4] = views;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Views NOT LOADED!');
            }),
            me.appendSearchFiltersAdminMenu().then(function (searchFilter) {
                childrens[5] = searchFilter;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Search filter NOT LOADED!');
            }),
            me.appendDashboardsAdminMenu().then(function (dashboard) {
                childrens[6] = dashboard;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Dashboard NOT LOADED!');
            }),
            me.appendReportsAdminMenu().then(function (reports) {
                childrens[7] = reports;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Report NOT LOADED!');
            }),
            me.appendMenuAdminMenu().then(function (menu) {
                childrens[8] = menu;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Menu NOT LOADED!');
            }),
            me.appendNavigationTreeAdminMenu().then(function (navigationTree) {
                childrens[9] = navigationTree;
            }, function () {
                Ext.Msg.alert('Error', 'Menu Navigation tree NOT LOADED!');
            }),
            me.appendGroupsAndPermissionsAdminMenu().then(function (roleAndPermission) {
                childrens[10] = roleAndPermission;
            }, function () {
                Ext.Msg.alert('Error', 'Menu role and permission NOT LOADED!');
            }),
            me.appendUsersAdminMenu().then(function (users) {
                childrens[11] = users;
            }, function () {
                Ext.Msg.alert('Error', 'Menu role and permission NOT LOADED!');
            }),
            me.appendTasksAdminMenu().then(function (tasks) {
                childrens[12] = tasks;
            }, function () {
                Ext.Msg.alert('Error', 'Menu tasks NOT LOADED!');
            }),
            me.appendEmailsAdminMenu().then(function (emails) {
                childrens[13] = emails;
            }, function () {
                Ext.Msg.alert('Error', 'Menu emails NOT LOADED!');
            }),
            me.appendGisAdminMenu().then(function (gis) {
                childrens[14] = gis;
            }, function () {
                Ext.Msg.alert('Error', 'Menu gis NOT LOADED!');
            }),
            me.appendBimAdminMenu().then(function (bim) {
                childrens[15] = bim;
            }, function () {
                Ext.Msg.alert('Error', 'Menu bim NOT LOADED!');
            }),
            me.appendLanguagesMenu().then(function (languages) {
                childrens[16] = languages;
            }, function () {
                Ext.Msg.alert('Error', 'Menu languages NOT LOADED!');
            }),
            me.appendSettingsMenu().then(function (settings) {
                childrens[17] = settings;
            }, function () {
                Ext.Msg.alert('Error', 'Menu settings NOT LOADED!');
            })
        ]).then(function () {
            store.removeAll();
            store.setRoot({
                expanded: true
            });
            store.getRoot().appendChild(childrens);
            store.sort('index');

            me.onMenuStoreReady(callback);
        });
    },

    /**
     * 0 - Create classes tree
     */
    appendClassesAdminMenu: function () {
        var deferred = new Ext.Deferred();
        var classesStore = Ext.getStore('classes.Classes');
        classesStore.load({
            params: {
                active: false
            },
            callback: function (items, operation, success) {
                var simples = {
                    menutype: 'folder',
                    objecttype: 'Simples',
                    text: CMDBuildUI.locales.Locales.administration.navigation.simples,
                    leaf: false,
                    index: 1,
                    children: this.getRecordsAsSubmenu(items.filter(function (rec, id) {
                        return rec.get('type') === 'simple';
                    }).sort(function (a, b) {

                        var nameA = a.get('description').toUpperCase(); // ignore upper and lowercase
                        var nameB = b.get('description').toUpperCase(); // ignore upper and lowercase
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }

                        // if description are same
                        return 0;
                    }), CMDBuildUI.model.menu.MenuItem.types.klass, '')
                };

                var standard = {
                    menutype: 'folder',
                    objecttype: 'Standard',
                    text: CMDBuildUI.locales.Locales.administration.navigation.standard,
                    leaf: false,
                    index: 0,
                    children: this.getRecordsAsSubmenu(items.filter(function (rec, id) {
                        return rec.get('prototype') === true || rec.get('parent').length > 0;
                    }).sort(this.sortByDescription), CMDBuildUI.model.menu.MenuItem.types.klass, 'Class')
                };

                //TODO: check configuration
                var classesMenu = {
                    menutype: 'folder',
                    objecttype: 'Class',
                    index: 0,
                    text: CMDBuildUI.locales.Locales.administration.navigation.classes,
                    leaf: false,
                    children: [standard, simples]
                };
                deferred.resolve(classesMenu);

            },
            scope: this
        });
        return deferred.promise;
    },
    /**
     * 1 - Create Process tree
     */
    appendProcessesAdminMenu: function () {
        var deferred = new Ext.Deferred();
        var processesStore = Ext.getStore('processes.Processes');
        processesStore.load({
            params: {
                active: false
            },
            callback: function (items, operation, success) {
                var processesMenu = {
                    menutype: 'folder',
                    objecttype: CMDBuildUI.model.administration.MenuItem.types.process,
                    text: CMDBuildUI.locales.Locales.administration.navigation.processes,
                    leaf: false,
                    index: 1,
                    children: this.getRecordsAsSubmenu(items.filter(function (rec, id) {
                        return rec.get('prototype') === true || rec.get('parent').length > 0;
                    }).sort(this.sortByDescription), CMDBuildUI.model.menu.MenuItem.types.process, 'Activity')
                };
                deferred.resolve(processesMenu);
            },
            scope: this
        });
        return deferred.promise;
    },

    /**
     * 2 - Create new folder for domains in menu tree
     * @returns 
     */
    appendDomainsAdminMenu: function () {
        var deferred = new Ext.Deferred();

        var domainsStore = Ext.create('Ext.data.Store', {
            model: 'CMDBuildUI.model.domains.Domain',

            pageSize: 0, // disable pagination

            sorters: [
                'name'
            ]
        });

        domainsStore.load({

            callback: function (items, operation, success) {

                var domainsMenu = {
                    menutype: 'folder',
                    index: 2,
                    objecttype: 'domain',
                    text: CMDBuildUI.locales.Locales.administration.navigation.domains,
                    leaf: false,
                    children: this.getRecordsAsSubmenu(items, CMDBuildUI.model.administration.MenuItem.types.domain, '')
                };

                // append menu item to the store if has children
                deferred.resolve(domainsMenu);
            },
            scope: this
        });
        return deferred.promise;
    },

    /**
     * 3 - Create LookType tree 
     */
    appendLookupTypesAdminMenu: function () {
        var deferred = new Ext.Deferred();

        var lookupTypesStore = Ext.create('Ext.data.Store', {
            model: 'CMDBuildUI.model.lookups.LookupType',

            pageSize: 0, // disable pagination

            sorters: [
                'name'
            ]
        });

        lookupTypesStore.load({

            callback: function (items, operation, success) {
                function setAsPrototype(item) {
                    for (var i in items) {
                        if (items[i].get('name') === item) {
                            items[i].set('prototype', true);
                        }
                    }
                }

                function threeGenerator(items) {

                    for (var item in items) {
                        if (items[item].get('parent').length) {
                            setAsPrototype(items[item].get('parent'));
                        }
                    }
                    return items;
                }
                items = threeGenerator(items);
                var lokupTypesMenu = {
                    menutype: 'folder',
                    index: 3,
                    objecttype: 'lookuptype',
                    text: CMDBuildUI.locales.Locales.administration.navigation.lookuptypes,
                    leaf: false,

                    children: this.getRecordsAsSubmenu(items, CMDBuildUI.model.administration.MenuItem.types.lookuptype, '')
                };

                // append menu item to the store if has children
                deferred.resolve(lokupTypesMenu);
            },
            scope: this
        });
        return deferred.promise;
    },

    /**
     * 4 - Create new folder for domains in menu tree
     * @returns 
     */
    appendViewsAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 4,
                objecttype: 'view',
                text: CMDBuildUI.locales.Locales.administration.navigation.views,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });
        // var viewsStore = Ext.create('Ext.data.Store', {
        //     model: 'CMDBuildUI.model.domains.Domain',

        //     pageSize: 0, // disable pagination

        //     sorters: [
        //         'name'
        //     ]
        // });

        // viewsStore.load({

        //     callback: function (items, operation, success) {

        //         var viewsMenu = {
        //             menutype: 'folder',
        //             index: 1,
        //             objecttype: 'view',
        //             text: 'Views',
        //             leaf: false,

        //             children: this.getRecordsAsSubmenu(items, CMDBuildUI.model.administration.MenuItem.types.views, '')
        //         };

        //         // append menu item to the store if has children
        //         deferred.resolve(viewsMenu);
        //     },
        //     scope: this
        // });
        return deferred.promise;
    },

    /**
     * 5 - Create new folder for search filter in menu tree
     * @returns 
     */
    appendSearchFiltersAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 5,
                objecttype: 'filters',
                text: CMDBuildUI.locales.Locales.administration.navigation.sarchfilters,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },
    /**
     * 6 - Create new folder for search filter in menu tree
     * @returns 
     */
    appendDashboardsAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 6,
                objecttype: 'dashboards',
                text: CMDBuildUI.locales.Locales.administration.navigation.dashboards,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },
    /**
     * 7 - Create new folder for search filter in menu tree
     * @returns 
     */
    appendReportsAdminMenu: function () {
        var deferred = new Ext.Deferred();
        // fa-files-o
        var reportsStore = Ext.getStore('Reports');

        reportsStore.load({

            callback: function (items, operation, success) {

                items.sort(function (a, b) {
                    return (a.get('description').toUpperCase() > b.get('description').toUpperCase()) ? 1 : ((b.get('description').toUpperCase() > a.get('description').toUpperCase()) ? -1 : 0);
                });

                var reportsMenu = {
                    menutype: 'folder',
                    index: 7,
                    objecttype: CMDBuildUI.model.administration.MenuItem.types.report,
                    text: CMDBuildUI.locales.Locales.administration.navigation.reports,
                    leaf: false,
                    href: 'administration/reports_empty',
                    children: this.getRecordsAsSubmenu(items, CMDBuildUI.model.administration.MenuItem.types.report, '')
                };

                // append menu item to the store if has children
                deferred.resolve(reportsMenu);
            },
            scope: this
        });

        return deferred.promise;
    },
    /**
     * 8 - Create Menu tree
     */
    appendMenuAdminMenu: function () {
        var deferred = new Ext.Deferred();

        var menuStore = Ext.create('Ext.data.Store', {
            model: 'CMDBuildUI.model.menu.Menu',

            pageSize: 0, // disable pagination

            sorters: [
                'group'
            ]
        });

        menuStore.load({

            callback: function (items, operation, success) {

                var domainsMenu = {
                    menutype: 'folder',
                    index: 8,
                    objecttype: CMDBuildUI.model.administration.MenuItem.types.menu,
                    text: CMDBuildUI.locales.Locales.administration.navigation.menus,
                    leaf: false,
                    children: this.getRecordsAsSubmenu(items, CMDBuildUI.model.administration.MenuItem.types.menu, '')
                };

                // append menu item to the store if has children
                deferred.resolve(domainsMenu);
            },
            scope: this
        });
        return deferred.promise;
    },
    /**
     * 9 - Create new folder for navigation tree in menu tree
     * @returns 
     */
    appendNavigationTreeAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 9,
                objecttype: 'navigationtree',
                text: CMDBuildUI.locales.Locales.administration.navigation.navigationtrees,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },


    /**
     * 10 - Create new folder for navigation tree in menu tree
     * @returns 
     */
    appendGroupsAndPermissionsAdminMenu: function () {
        /*
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 10,
                objecttype: 'groupsandpermission',
                text:  CMDBuildUI.locales.Locales.administration.navigation.groupsandpermissions,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
        */
        var deferred = new Ext.Deferred();
        var groupsStore = Ext.create('Ext.data.Store', {
            model: 'CMDBuildUI.model.users.Group',

            proxy: {
                type: 'baseproxy',
                url: '/roles',
                pageParam: false, //to remove param "page"
                startParam: false, //to remove param "start"
                limitParam: false //to remove param "limit"
            },
            sorters: ['description']  // TODO: NOT WORK !!!! WHY????
        });

        groupsStore.load({
            callback: function (items, operation, success) {
                items.sort(function(a,b){
                    return (a.get('description') > b.get('description')) ? 1 : ((b.get('description') > a.get('description')) ? -1 : 0);
                });
                var groupsMenu = {
                    menutype: 'folder',
                    index: 10,
                    objecttype: 'groupsandpermission',
                    text: CMDBuildUI.locales.Locales.administration.navigation.groupsandpermissions,
                    leaf: false,
                    children: this.getRecordsAsSubmenu(items, CMDBuildUI.model.administration.MenuItem.types.groupsandpermissions, '')
                };
                deferred.resolve(groupsMenu);
            },
            scope: this
        });
        return deferred.promise;
    },

    /**
     * 11 - Create new folder for users in menu tree
     * @returns 
     */
    appendUsersAdminMenu: function () {
        var me = this;
        var deferred = new Ext.Deferred();

        var userItem = {
            menutype: 'user',
            index: 0,
            objecttype: 'user',
            objectDescription: CMDBuildUI.locales.Locales.administration.navigation.users,
            leaf: true,
            iconCls: 'x-fa fa-user',
            href: 'administration/users'
        };
        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 11,
                objecttype: 'user',
                text: CMDBuildUI.locales.Locales.administration.navigation.users,
                leaf: false,
                children: [userItem]
            };
            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },

    /**
     * 12 - Create new folder for users in menu tree
     * @returns 
     */
    appendTasksAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 12,
                objecttype: 'task',
                text: CMDBuildUI.locales.Locales.administration.navigation.taskmanager,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },
    /**
     * 13 - Create new folder for eamils in menu tree
     * @returns 
     */
    appendEmailsAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 13,
                objecttype: 'email',
                text: CMDBuildUI.locales.Locales.administration.navigation.email,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },
    /**
     * 14 - Create new folder for gis in menu tree
     * @returns 
     */
    appendGisAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 14,
                objecttype: 'gis',
                text: CMDBuildUI.locales.Locales.administration.navigation.gis,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },
    /**
     * 15 - Create new folder for bim in menu tree
     * @returns 
     */
    appendBimAdminMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 15,
                objecttype: 'bim',
                text: CMDBuildUI.locales.Locales.administration.navigation.bim,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },
    /**
     * 16 - Create new folder for language in menu tree
     * @returns 
     */
    appendLanguagesMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 16,
                objecttype: 'language',
                text: CMDBuildUI.locales.Locales.administration.navigation.languages,
                leaf: false,
                children: []
            };

            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },
    /**
     * 17 - Create new folder for configuration in menu tree
     * @returns 
     */
    appendSettingsMenu: function () {
        var deferred = new Ext.Deferred();

        setTimeout(function () {
            var viewsMenu = {
                menutype: 'folder',
                index: 17,
                objecttype: 'configuration',
                text: CMDBuildUI.locales.Locales.administration.navigation.systemconfig,
                leaf: false,
                children: [{
                    menutype: CMDBuildUI.model.administration.MenuItem.types.setup,
                    index: 1,
                    objecttype: 'setup',
                    text: CMDBuildUI.locales.Locales.administration.navigation.generaloptions,
                    leaf: true,
                    iconCls: 'x-fa fa-wrench',
                    href: 'administration/setup/generaloptions'
                }, {
                    menutype: CMDBuildUI.model.administration.MenuItem.types.setup,
                    index: 1,
                    objecttype: 'setup',
                    text: CMDBuildUI.locales.Locales.administration.navigation.multitenant,
                    leaf: true,
                    iconCls: 'x-fa fa-wrench',
                    href: 'administration/setup/multitenant'
                }, {
                    menutype: CMDBuildUI.model.administration.MenuItem.types.setup,
                    index: 1,
                    objecttype: 'setup',
                    text: CMDBuildUI.locales.Locales.administration.navigation.workflow,
                    leaf: true,
                    iconCls: 'x-fa fa-wrench',
                    href: 'administration/setup/workflow'
                }, {
                    menutype: CMDBuildUI.model.administration.MenuItem.types.setup,
                    index: 1,
                    objecttype: 'setup',
                    text: CMDBuildUI.locales.Locales.administration.navigation.dms,
                    leaf: true,
                    iconCls: 'x-fa fa-wrench',
                    href: 'administration/setup/documentmanagementsystem'
                }, {
                    menutype: CMDBuildUI.model.administration.MenuItem.types.setup,
                    index: 1,
                    objecttype: 'setup',
                    text: CMDBuildUI.locales.Locales.administration.navigation.gis,
                    leaf: true,
                    iconCls: 'x-fa fa-wrench',
                    href: 'administration/setup/gis'
                }, {
                    menutype: CMDBuildUI.model.administration.MenuItem.types.setup,
                    index: 1,
                    objecttype: 'setup',
                    text: CMDBuildUI.locales.Locales.administration.navigation.bim,
                    leaf: true,
                    iconCls: 'x-fa fa-wrench',
                    href: 'administration/setup/bim'
                }, {
                    menutype: CMDBuildUI.model.administration.MenuItem.types.setup,
                    index: 1,
                    objecttype: 'setup',
                    text: CMDBuildUI.locales.Locales.administration.navigation.servermanagement,
                    leaf: true,
                    iconCls: 'x-fa fa-wrench',
                    href: 'administration/setup/servermanagement'
                }]
            };

            viewsMenu.children.push();
            deferred.resolve(viewsMenu);
        });

        return deferred.promise;
    },

    onMenuStoreReady: function (callback) {
        if (typeof callback == 'function') {
            callback();
        }
    },

    /**
     * Return the json definition for records as tree.
     * @param {Ext.data.Model[]} records The plain list of items.
     * @param {String} menutype The menu type to use for these records.
     * @param {String} parentmenu The name of the parent item.
     * 
     * @return {Object[]} A list of CMDBuild.model.MenuItem definitions.
     */
    getRecordsAsSubmenu: function (records, menutype, parentname) {
        var output = [];
        var me = this;

        var frecords = Ext.Array.filter(records, function (item) {
            return item.getData().hasOwnProperty('parent') && item.getData().parent === parentname;
        });

        switch (menutype) {
            case CMDBuildUI.model.administration.MenuItem.types.domain:
            case CMDBuildUI.model.administration.MenuItem.types.menu:
            case CMDBuildUI.model.administration.MenuItem.types.report:
            case CMDBuildUI.model.administration.MenuItem.types.groupsandpermissions:
                frecords = records;
                break;
            default:
                break;
        }

        for (var i = 0; i < frecords.length; i++) {

            var record = frecords[i].getData();
            var menuitem = {
                menutype: menutype,
                index: i,
                objecttype: record.name,
                text: record.description,
                leaf: true
            };

            switch (menutype) {
                case CMDBuildUI.model.administration.MenuItem.types.klass:
                    menuitem.href = 'administration/classes/' + menuitem.objecttype;
                    if (record.prototype) {
                        menuitem.leaf = false;
                        menuitem.children = me.getRecordsAsSubmenu(records, menutype, record.name).sort(this.sortByDescription);
                    }
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.lookuptype:
                    menuitem.iconCls = 'x-fa fa-table';
                    if (record.prototype) {
                        menuitem.leaf = false;
                        menuitem.children = me.getRecordsAsSubmenu(records, menutype, record.name).sort(this.sortByDescription);
                    }
                    menuitem.objecttype = CMDBuildUI.util.Utilities.stringToHex(menuitem.objecttype);
                    menuitem.href = 'administration/lookup_types/' + menuitem.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.domain:
                    menuitem.iconCls = 'x-fa fa-table';
                    menuitem.href = 'administration/domains/' + menuitem.objecttype;
                    if (record.prototype) {
                        menuitem.leaf = false;
                        menuitem.children = me.getRecordsAsSubmenu(records, menutype, record.name).sort(this.sortByDescription);
                    }
                    break;

                case CMDBuildUI.model.administration.MenuItem.types.groupsandpermissions:
                    menuitem.objecttype = record._id; 
                    menuitem.iconCls = 'x-fa fa-users';
                    menuitem.href = 'administration/groupsandpermissions/' + menuitem.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.menu:
                    menuitem.objecttype = record._id; // TODO: use getRecordId() not work!!
                    menuitem.iconCls = 'x-fa fa-users';
                    menuitem.href = 'administration/menus/' + menuitem.objecttype;
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.process:
                    menuitem.objecttype = record._id; // TODO: use getRecordId() not work!!
                    menuitem.iconCls = 'x-fa fa-cog';
                    menuitem.href = 'administration/processes/' + menuitem.objecttype;
                    if (record.prototype) {
                        menuitem.leaf = false;
                        menuitem.iconCls = 'x-fa fa-cogs';
                        menuitem.children = me.getRecordsAsSubmenu(records, menutype, record.name).sort(this.sortByDescription);
                    }
                    break;
                case CMDBuildUI.model.administration.MenuItem.types.report:
                    menuitem.objecttype = record._id; // TODO: use getRecordId() not work!!
                    menuitem.iconCls = 'x-fa fa-files-o';
                    menuitem.href = 'administration/reports/' + menuitem.objecttype;
                    break;
            }

            output.push(menuitem);
        }
        return output;
    },

    /**
     * Return the json definition for records as tree.
     * @param {Ext.data.Model[]} records The plain list of items.
     * @param {String} menutype The menu type to use for these records.
     * 
     * @return {Object[]} A list of CMDBuild.model.MenuItem definitions.
     */
    getRecordsAsList: function (records, menutype) {
        var output = [];

        for (var i = 0; i < records.length; i++) {
            var record = records[i].getData();
            var menuitem = {
                menutype: menutype,
                index: i,
                objectid: record._id,
                text: record.description,
                leaf: true
            };
            output.push(menuitem);
        }
        return output;
    },
    privates: {
        sortByDescription: function (a, b) {
            if (a.get && b.get) {
                var nameA = a.get('description').toUpperCase(); // ignore upper and lowercase
                var nameB = b.get('description').toUpperCase(); // ignore upper and lowercase
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // if descriptions are same
                return 0;
            }
        }
    }
});