Ext.define('CMDBuildUI.util.administration.helper.FormHelper', {
    singleton: true,
    /**
     * Get Invalid Fields helper
     * @param {Ext.form.Panel} form 
     */
    getInvalidFields: function (form) {
        var invalidFields = [];
        Ext.suspendLayouts();
        form.getFields().filterBy(function (field) {
            if (field.validate()) return;
            invalidFields.push(field);
        });
        Ext.resumeLayouts(true);
        return invalidFields;
    }
});