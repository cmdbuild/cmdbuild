Ext.define('CMDBuildUI.util.administration.helper.ConfigHelper', {
    singleton: true,

    settings: null,
    getConfig: function (force) {
        var deferred = new Ext.Deferred();
        Ext.Ajax.request({
            url: Ext.String.format('{0}/system/config?detailed=true', CMDBuildUI.util.Config.baseUrl),
            method: 'GET',
            success: function (transport) {
                var jsonResponse = Ext.JSON.decode(transport.responseText);
                var setupKeys = Ext.Object.getAllKeys(jsonResponse.data);
                var result = [];
                setupKeys.forEach(function (key) {
                    /**
                     * Example data
                     * 
                     * default: "DISABLED"
                     * description: "valid values are DISABLED, CMDBUILD_CLASS, DB_FUNCTION"
                     * hasDefinition: true
                     * hasValue: false
                     * _key: "org__DOT__cmdbuild__DOT__multitenant__DOT__mode
                     */
                    jsonResponse.data[key]._key = key.replace(/\./g, '__DOT__');
                    result.push(jsonResponse.data[key]);
                });
                deferred.resolve(result);
            }
        });
        return deferred.promise;
    },

    setConfig: function (theSetup) {
        var deferred = new Ext.Deferred();

        var data = {},
            setupKeys = Ext.Object.getAllKeys(theSetup);

        setupKeys.forEach(function (key) {
            if (!key.startsWith('org__DOT__cmdbuild__DOT__multitenant__DOT__')) {
                data[key.replace(/\__DOT__/g, '.')] = theSetup[key]; //value;
            }
        });

        /**
         * save configuration via custom ajax call
         */
        Ext.Ajax.request({
            url: Ext.String.format('{0}/system/config/_MANY', CMDBuildUI.util.Config.baseUrl),
            method: 'PUT',
            jsonData: data,
            success: function (transport) {
                deferred.resolve(transport);
            }
        });
        return deferred.promise;
    },

    setMultinantData: function (theSetup) {
        var deferred = new Ext.Deferred();

        var data = {},
            setupKeys = Ext.Object.getAllKeys(theSetup);

        setupKeys.forEach(function (key) {
            if (key.startsWith('org__DOT__cmdbuild__DOT__multitenant__DOT__')) {
                // remove not needed data
                switch (data.org__DOT__cmdbuild__DOT__multitenant__DOT__mode) {
                    case 'CMDBUILD_CLASS':
                        data.org__DOT__cmdbuild__DOT__multitenant__DOT__dbFunction = '';
                        break;
                    case 'DB_FUNCTION':
                        data.org__DOT__cmdbuild__DOT__multitenant__DOT__tenantClass = '';
                        data.org__DOT__cmdbuild__DOT__multitenant__DOT__tenantDomain = '';
                        break;
                    default:
                        break;
                }
                data[key.replace(/\__DOT__/g, '.')] = theSetup[key]; //value;
            }
        });

        /**
         * save configuration via custom ajax call
         */
        Ext.Ajax.request({
            url: Ext.String.format('{0}/tenants/configure', CMDBuildUI.util.Config.baseUrl),
            method: 'POST',
            jsonData: data,
            success: function (transport) {
                deferred.resolve(transport);
            }
        });
        return deferred.promise;
    },
});