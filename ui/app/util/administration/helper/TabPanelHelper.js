Ext.define('CMDBuildUI.util.administration.helper.TabPanelHelper', {

    requires: [
        'Ext.util.Format'
    ],

    singleton: true,
    /**
     * Get Invalid Fields helper
     * @param {Ext.tab.Panel} view
     * @param {String} name
     * @param {Array} items
     * @param {Number} index
     * @param {Object} bind
     */
    addTab: function (view, name, localizedName, items, index, bind, tabConfig) {
        try {
            return view.add({
                xtype: "panel",
                items: items,
                reference: (tabConfig && tabConfig.reference) ? tabConfig.reference : name,
                layout: 'fit',
                viewModel: {},
                autoScroll: true,
                config: tabConfig || {},
                padding: 0,
                tabIndex: index,
                tabConfig: {
                    //maxHeight:20,
                    ui: 'administration-tab-item',
                    tabIndex: index,
                    title: localizedName,
                    tooltip: localizedName,
                    autoEl: {
                        'data-testid': Ext.String.format('administration-groupandpermission-tab-{0}', name)
                    },

                },
                bind: bind || {}
            });
        } catch (error) {
            CMDBuildUI.util.Logger.log(Ext.String.format("{0} tab generation exception", name), CMDBuildUI.util.Logger.levels.error);
        }
    }
});