Ext.define("CMDBuildUI.util.Utilities", {
    singleton: true,

    popupOpened: {},
    /**
     * Update the hash. By default, it will not execute the routes if the current token and the
     * token passed are the same.
     * 
     * @param {String/Ext.data.Model} token The token to redirect to.  Can be either a String
     * or a {@link Ext.data.Model Model} instance - if a Model instance is passed it will
     * internally be converted into a String token by calling the Model's
     * {@link Ext.data.Model#toUrl toUrl} function.
     *
     * @param {Boolean} force Force the update of the hash regardless of the current token.
     * 
     * @return {Boolean} Will return `true` if the token was updated.
     */
    redirectTo: function (token, force) {
        if (token.isModel) {
            token = token.toUrl();
        }

        var isCurrent = Ext.util.History.getToken() === token,
            ret = false;

        if (!isCurrent) {
            ret = true;
            Ext.util.History.add(token);
        } else if (force) {
            ret = true;
            Ext.app.route.Router.onStateChange(token);
        }
        return ret;
    },

    /**
     * @param {String} [popupId='popup-panel-1'] The default id for the panel 
     * @param {String} title
     * @param {Object|Ext.Component} content
     * @param {Object} listeners
     * @param {Object} config
     */
    openPopup: function (popupId, title, content, listeners, config) {
        popupId = (!popupId) ? 'popup-panel-' + (Ext.Object.getSize(this.popupOpened) + 1) : popupId;
        listeners = listeners || {};

        listeners = Ext.applyIf(listeners, {
            beforedestroy: function () {
                this.removeAll(true);
            }
        });

        this.closePopup(popupId);
        config = Ext.applyIf(config || {}, {
            id: popupId,
            title: title,

            width: "80%",
            height: "80%",
            ui: "management",
            floating: true,
            closable: true,
            modal: true,
            layout: 'fit',
            alwaysOnTop: true,
            resizable: false,
            draggable: false,

            items: [content],
            listeners: listeners
        });

        var panel = new Ext.panel.Panel(config);

        panel.show();
        this.popupOpened[popupId] = panel;
        return panel;
    },

    /**
     * Close the pop popup from anywhere
     * @param {String|Number} popupId 
     * @return {Boolean} true|false 
     */
    closePopup: function (popupId) {
        if (this.popupOpened[popupId] && typeof this.popupOpened[popupId].destroy == "function") {
            this.popupOpened[popupId].destroy();
        }
        return delete this.popupOpened[popupId];
    },

    /**
     * @return {String} The generated UID
     */
    generateUUID: function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    },

    /**
     * Add loading mask on component.
     * @param {Ext.component.Component} element
     * @return {Ext.LoadMask}
     */
    addLoadMask: function (element) {
        // add load mask
        var loadmask = new Ext.LoadMask({
            target: element
        });
        loadmask.show();
        return loadmask;
    },

    /**
     * Remove loading mask from component.
     * @param {Ext.LoadMask} loadmask
     */
    removeLoadMask: function (loadmask) {
        loadmask.destroy();
    },

    /**
     * Transform utf-8 string to hex string.
     * @param {String} s
     * @return {String}
     */
    stringToHex: function (s) {
        if (s && !s.startsWith('HEX')) {
            // utf8 to latin1
            s = unescape(encodeURIComponent(s));
            var h = '';
            for (var i = 0; i < s.length; i++) {
                h += s.charCodeAt(i).toString(16);
            }
            return "HEX" + h;
        }
        return s;
    },

    /**
     * Transform hex string to utf-8 string.
     * @param {String} s
     * @return {String}
     */
    hexToString: function (h) {
        var s = '';
        if (h && h.startsWith('HEX')) {
            h = h.replace("HEX", "");
            for (var i = 0; i < h.length; i += 2) {
                s += String.fromCharCode(parseInt(h.substr(i, 2), 16));
            }
            return decodeURIComponent(escape(s));
        }
        return s;
    }

});