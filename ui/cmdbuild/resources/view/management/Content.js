
(function () {
    var elementId = 'CMDBuildManagementContent';

    Ext.define('CMDBuildUI.view.management.Content', {
        extend: 'Ext.panel.Panel',

        requires: [
            'CMDBuildUI.view.management.ContentController',
            'CMDBuildUI.view.management.ContentModel',

            'CMDBuildUI.view.management.navigation.Container',
            'CMDBuildUI.view.classes.cards.Grid', 
            'CMDBuildUI.view.classes.cards.detail.CardView',
            'CMDBuildUI.view.classes.cards.detail.CardEdit'
        ],

        statics: {
            elementId: elementId
        },

        xtype: 'management-content',
        controller: 'management-content',
        viewModel: {
            type: 'management-content'
        },
        id: elementId,
        layout: 'card'
    });
})();

