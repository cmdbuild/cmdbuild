package org.cmdbuild.shark;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.cmdbuild.shark.test.utils.EventManagerMatchers.isActivity;

import org.cmdbuild.api.fluent.NewCard;
import org.cmdbuild.workflow.shark.xpdl.XpdlActivity;
import org.cmdbuild.workflow.shark.xpdl.XpdlDocumentHelper.ScriptLanguage;
import org.cmdbuild.workflow.shark.xpdl.XpdlProcess;
import org.junit.Before;
import org.junit.Test;

import org.cmdbuild.shark.test.utils.AbstractLocalSharkServiceTest;
import org.cmdbuild.shark.test.utils.MockSharkWorkflowApiFactory;
import static org.cmdbuild.workflow.XpdlTest.randomName;
import org.junit.Ignore;

public class InjectedApiIT extends AbstractLocalSharkServiceTest {

	private XpdlProcess process;

	@Before
	public void createProcess() throws Exception {
		process = xpdlDocument.createProcess(randomName());
	}

	@Test
	@Ignore
	public void apiSuccessfullyCalled() throws Exception {
		final XpdlActivity scriptActivity = process.createActivity(randomName());
		scriptActivity.setScriptingType(ScriptLanguage.JAVA, //
				"cmdb.newCard(\"Funny\")" //
						+ ".with(\"Code\", \"code\")" //
						+ ".create();");

		final XpdlActivity noImplActivity = process.createActivity(randomName());

		process.createTransition(scriptActivity, noImplActivity);

		uploadXpdlAndStartProcess(process);
		verify(eventManager).activityClosed(argThat(isActivity(scriptActivity)));

		verify(MockSharkWorkflowApiFactory.fluentApiExecutor).create(any(NewCard.class));
		verifyNoMoreInteractions(MockSharkWorkflowApiFactory.fluentApiExecutor);
	}

}
