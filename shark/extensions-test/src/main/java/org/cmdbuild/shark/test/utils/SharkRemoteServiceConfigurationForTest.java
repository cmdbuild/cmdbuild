/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.shark.test.utils;

import com.google.common.eventbus.EventBus;
import java.util.Properties;
import org.cmdbuild.workflow.SharkRemoteServiceConfiguration;

public class SharkRemoteServiceConfigurationForTest implements SharkRemoteServiceConfiguration {

	private final EventBus eventBus = new EventBus();

	@Override
	public String getServerUrl() {
		return "";
	}

	@Override
	public String getUsername() {
		return "admin";
	}

	@Override
	public String getPassword() {
		return "";
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public Properties getClientProperties() {
		Properties properties = new Properties();
//		properties.put("ClientType", "WS");//TODO check this
		properties.put("SharkConfResourceLocation", "Shark.conf");
		return properties;
	}

}
