package org.cmdbuild.scheduler.quartz;

import static java.lang.String.format;
import static org.cmdbuild.scheduler.CMDBuildSerializableJob.EmailQueueConventionalTaskId;
import static org.cmdbuild.scheduler.CMDBuildSerializableJob.EmailQueueParameterLastExecution;

import org.cmdbuild.scheduler.JobWithTask;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.cmdbuild.requestcontext.RequestContextService;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.cmdbuild.logic.taskmanager.TaskService;

/**
 * a quartz job that runs a cmdbuild task (by id)
 *
 * @author davide
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class TaskQuartzJob implements org.quartz.Job {

	private static final String TASK_ID = "taskID";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private TaskService taskManagerLogic;
//	@Autowired
//	private EmailQueueLogic emailQueueLogic;
	@Autowired
	private RequestContextService requestContextService;

	public TaskQuartzJob() {
		logger.debug("rebuilding a QuartzJob...");
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("Executing a QuartzJob...");
		requestContextService.initCurrentRequestContext(format("quartz task %s", randomId()));
		try {
			JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
			if (jobDataMap.containsKey(TASK_ID)) {
				Long taskId = jobDataMap.getLong(TASK_ID);
				logger.info("running task as quartz job, taskid = {}", taskId);
				//check for standard tasks that are not serialized on the persistence layer.. (see CMDBuildSerializableJob conventions)
//				if (EmailQueueConventionalTaskId.equals(taskId)) { // TODO: this is TERRIBLE!!! FIX THIS PLEASE!!
//					logger.info("task is email queue task, run with email queue logic");
//					((DefaultClusteringEmailQueueLogic) emailQueueLogic).execute(context); //this is bad, leaking quartz internals to email service >_<
//				} else {
					taskManagerLogic.execute(taskId);
//				}
			} else {

			}
		} finally {
			requestContextService.destroyCurrentRequestContext();
		}
	}

	public static JobDetail toJobDetail(JobWithTask schedulerJob) {

		LoggerFactory.getLogger(TaskQuartzJob.class).debug("Creating JobDetails for {}", schedulerJob.getName()); //TODO replace logger

		JobDataMap jobDataMap = new JobDataMap();

		jobDataMap.put(TASK_ID, schedulerJob.getTaskId());
		//TODO manage EmailQueue, but delegating to the tasks extra parameter serialization should be evaluated if the case is common
		if (EmailQueueConventionalTaskId.equals(schedulerJob.getTaskId())) {
			jobDataMap.put(EmailQueueParameterLastExecution, null);
		}

		return JobBuilder.newJob(TaskQuartzJob.class).withIdentity(schedulerJob.getName()).setJobData(jobDataMap).build();
	}

}
