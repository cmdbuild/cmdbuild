/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.scheduler.quartz.annotatedmethodjobs;

import org.cmdbuild.scheduler.JobTrigger;
import org.cmdbuild.scheduler.spring.ScheduledJobClusterMode;

/**
 *
 * @author davide
 */
public interface AnnotatedMethodJob {

	String getBeanName();

	String getMethodName();

	JobTrigger getTrigger();

	ScheduledJobClusterMode getClusterMode();
}
