package org.cmdbuild.scheduler.quartz;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;
import static java.lang.String.format;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import static org.cmdbuild.jobs.JobExecutorService.JOBUSER_NOBODY;
import org.cmdbuild.jobs.JobSessionService;
import org.cmdbuild.scheduler.spring.ScheduledJob;
import static org.cmdbuild.utils.lang.CmdbPreconditions.firstNotBlank;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * a quartz job that invokes a method of a spring bean (that was previously
 * annotated with {@link ScheduledJob}
 *
 * @author davide
 */
@DisallowConcurrentExecution
public class SpringBeanQuartzJob implements org.quartz.Job {

	public static final String JOB_NAME_PREFIX = "springBeanQuartzJob";

	private static final String BEAN_NAME = "org.cmdbuild.scheduler.quartz.SpringBeanQuartzJob.beanName",
			METHOD_NAME = "org.cmdbuild.scheduler.quartz.SpringBeanQuartzJob.methodName";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private JobSessionService jobSessionService;

	public SpringBeanQuartzJob() {
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
			String beanName = checkNotNull(jobDataMap.getString(BEAN_NAME)), methodName = checkNotNull(jobDataMap.getString(METHOD_NAME));
			Object bean;
			Method method;
			String user;
			try {
				logger.trace("get job bean for name = {}", beanName);
				bean = applicationContext.getBean(beanName);
				logger.trace("get job method for bean name = {} method name = {}", beanName, methodName);
				method = bean.getClass().getMethod(methodName);
				checkArgument(method.isAnnotationPresent(ScheduledJob.class), "this method is not annotated with ScheduledJob"); // throw error if the bean is not annotated as a scheduled job
				user = firstNotBlank(method.getAnnotation(ScheduledJob.class).user(), JOBUSER_NOBODY);
			} catch (Exception ex) {
				logger.warn("error loading job for bean = {} method = {}", beanName, methodName);
				logger.warn("error loading job", ex);
				return;
			}
			jobSessionService.createJobSessionContext(user, format("quartz service method %s.%s", beanName, methodName));
			logger.debug("invoke bean method = {}.{}", beanName, methodName);
			method.invoke(bean);
			logger.debug("job execution completed for bean name = {} method name = {}", beanName, methodName);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException ex) {
			throw new RuntimeException(ex);
		} finally {
			jobSessionService.destroyJobSessionContext();
		}
	}

	/**
	 * build job detail for a job that will call a method of a spring bean
	 *
	 * @param beanName spring bean name
	 * @param methodName spring bean method name
	 * @param triggerKey unique key from trigger (used to detect changed
	 * triggers)
	 * @return
	 */
	public static JobDetail buildJobDetail(String beanName, String methodName, String triggerKey) {
		checkNotNull(beanName);
		checkNotNull(methodName);
		return JobBuilder.newJob(SpringBeanQuartzJob.class)
				.withIdentity(JOB_NAME_PREFIX + "_" + beanName + "_" + methodName + "_" + triggerKey)
				.usingJobData(BEAN_NAME, beanName)
				.usingJobData(METHOD_NAME, methodName)
				.build();
	}

}
