package org.cmdbuild.scheduler.quartz;

import com.google.common.base.Joiner;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Lists;
import static com.google.common.collect.Maps.newLinkedHashMap;
import org.cmdbuild.scheduler.quartz.annotatedmethodjobs.AnnotatedMethodJobSupplier;
import com.google.common.eventbus.Subscribe;
import static java.lang.String.format;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import static java.util.stream.Collectors.toList;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.clustering.jgroups.ClusterConfiguration;
import org.cmdbuild.config.SchedulerConfiguration;

import org.cmdbuild.scheduler.SchedulerService;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.quartz.simpl.SimpleJobFactory;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.cmdbuild.scheduler.JobWithTask;
import org.cmdbuild.scheduler.JobTrigger;
import org.cmdbuild.scheduler.SimpleScheduledJob;
import org.quartz.Job;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;
import org.cmdbuild.scheduler.ScheduledJobInfo;
import org.cmdbuild.startup.PostStartup;
import org.quartz.CronTrigger;
import org.cmdbuild.config.api.ConfigReloadEvent;
import org.cmdbuild.scheduler.OneTimeTrigger;
import org.cmdbuild.scheduler.RecurringTrigger;
import org.cmdbuild.scheduler.SchedulerException;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_NOTRUNNING;
import static org.cmdbuild.spring.configuration.BeanNamesAndQualifiers.RAW_DATA_SOURCE;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;
import static org.cmdbuild.utils.hash.CmdbuildHashUtils.hash;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.TriggerBuilder;

@Component
public class QuartzSchedulerService implements SchedulerService, SystemService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ClusterConfiguration clusterConfiguration;
	private final SchedulerConfiguration schedulerConfiguration;
	private final ApplicationContext applicationContext;
	private final AnnotatedMethodJobSupplier annotatedMethodJobService;
	private final DataSource datasource;

	private Scheduler scheduler;

	public QuartzSchedulerService(ClusterConfiguration clusterConfiguration, SchedulerConfiguration schedulerConfiguration, ApplicationContext applicationContext, AnnotatedMethodJobSupplier annotatedMethodJobService, @Qualifier(RAW_DATA_SOURCE) DataSource datasource) {
		this.schedulerConfiguration = checkNotNull(schedulerConfiguration);
		this.applicationContext = checkNotNull(applicationContext);
		this.annotatedMethodJobService = checkNotNull(annotatedMethodJobService);
		this.datasource = checkNotNull(datasource);
		this.clusterConfiguration = checkNotNull(clusterConfiguration);
		schedulerConfiguration.getEventBus().register(new Object() {
			@Subscribe
			public void handleConfigUpdateEvent(ConfigReloadEvent event) {
				reloadScheduler();
			}
		});
	}

	@Override
	public String getServiceName() {
		return "Quartz Scheduler";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (isRunning()) {
			return SS_READY;
		} else {
			return SS_NOTRUNNING;
		}
	}

	@Override
	public void startService() {
		checkArgument(!isRunning());
		start();
	}

	@Override
	public void stopService() {
		checkArgument(isRunning());
		stop();
	}

	@PostStartup
	public void postStartup() {
		start();
	}

	@PreDestroy
	public void cleanup() {
		logger.info("stop scheduler service");
		destroySchedulerSafe();
	}

	@Override
	public boolean isRunning() {
		try {
			return scheduler != null && scheduler.isStarted() && !scheduler.isShutdown();
		} catch (Exception ex) {
			logger.warn("error checking scheduler status", ex);
			return false;
		}
	}

	/**
	 *
	 * @return true if scheduler was running, false otherwise
	 */
	private synchronized boolean destroySchedulerSafe() {
		boolean wasRunning = isRunning();
		if (scheduler != null) {
			try {
				if (wasRunning) {
					scheduler.shutdown();
				}
			} catch (Exception ex) {
				logger.warn("error stopping scheduler", ex);
			}
			scheduler = null;
		}
		return wasRunning;
	}

	private synchronized void reloadScheduler() {
		logger.info("reloadScheduler");
		try {
			boolean wasRunning = destroySchedulerSafe();

			prepareScheduler();

			if (wasRunning) {
				start();
			}
		} catch (Exception ex) {
			throw new SchedulerException(ex);
		}
	}

	private synchronized void prepareScheduler() throws Exception {
		logger.info("prepareScheduler");
		QuartzConnectionProvider.setDataSource(datasource);

		StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();

		Properties quartzProperties = map(schedulerConfiguration.getQuartzProperties()).with(
				"org.quartz.jobStore.dataSource", "CMDBuildDatasource",
				"org.quartz.dataSource.CMDBuildDatasource.connectionProvider.class", "org.cmdbuild.scheduler.quartz.QuartzConnectionProvider", //TODO please springify this
				"org.quartz.scheduler.instanceId", clusterConfiguration.getClusterNodeId()
		).toProperties();
		schedulerFactory.initialize(quartzProperties);

		scheduler = schedulerFactory.getScheduler();
		scheduler.setJobFactory(new AutowiringSpringBeanJobFactory());

		Map<String, Pair<JobDetail, Trigger>> jobsToLoad = newLinkedHashMap();
		annotatedMethodJobService.getAnnotatedMethodJobs().forEach((annotatedMethodJob) -> {
			try {
				JobTrigger trigger = annotatedMethodJob.getTrigger();
				JobDetail jobDetail = SpringBeanQuartzJob.buildJobDetail(annotatedMethodJob.getBeanName(), annotatedMethodJob.getMethodName(), idForTrigger(trigger));
				jobsToLoad.put(jobDetail.getKey().getName(), Pair.of(jobDetail, toQuartzTrigger(trigger)));
			} catch (Exception ex) {
				logger.error("error configuring method job = " + annotatedMethodJob, ex);
			}
		});
		logger.info("found {} annotated method jobs", jobsToLoad.size());

		logger.info("check status of annotated method jobs");
		scheduler.getJobKeys(GroupMatcher.anyJobGroup()).stream().filter((jobKey) -> jobKey.getName().startsWith(SpringBeanQuartzJob.JOB_NAME_PREFIX)).forEach((jobKey) -> {
			if (jobsToLoad.containsKey(jobKey.getName())) {
				logger.info("job {} already configured in quartz, no need to reconfigure", jobKey.getName());
				jobsToLoad.remove(jobKey.getName());//job already configured in quartz, skipping
			} else {
				logger.info("delete stale job for key = {}", jobKey.getName());
				try {
					scheduler.deleteJob(jobKey);
				} catch (Exception ex) {
					logger.error("error deleting job " + jobKey, ex);
				}
			}
		});

		logger.info("configure {} annotated method jobs", jobsToLoad.size());
		jobsToLoad.values().forEach((job) -> {
			try {
				add(job.getLeft(), job.getRight());
			} catch (Exception ex) {
				logger.error("error configuring method job = {}", job);
				logger.error("error configuring method job", ex);
			}
		});
	}

	private JobDetail jobToJobDetail(JobWithTask job) {
		return TaskQuartzJob.toJobDetail((JobWithTask) job);
	}

	@Override
	public void add(JobWithTask job, JobTrigger trigger) {
		Trigger quartzTrigger = toQuartzTrigger(trigger);
		logger.info("schedule job = {} with trigger = {}", job, quartzTrigger);
		JobDetail jobDetail = jobToJobDetail(job);
		add(jobDetail, quartzTrigger);
	}

	private void add(JobDetail jobDetail, Trigger trigger) {
		logger.info("schedule job = {} with trigger = {}", jobDetail, trigger);
		try {
			scheduler.deleteJob(jobDetail.getKey());
			scheduler.scheduleJob(jobDetail, trigger);
		} catch (Exception e) {
			throw new SchedulerException(e);
		}
	}

	@Override
	public void remove(JobWithTask job) {
		try {
			scheduler.deleteJob(jobToJobDetail(job).getKey());
		} catch (Exception e) {
			throw new SchedulerException(e);
		}
	}

	@Override
	public boolean isStarted(JobWithTask job) {
		try {
			return scheduler.checkExists(jobToJobDetail(job).getKey());
		} catch (Exception e) {
			throw new SchedulerException(e);
		}
	}

	@Override
	public void start() {
		logger.info("start quartz service");
		try {
			if (scheduler == null) {
				prepareScheduler();
			}
			scheduler.start();
		} catch (Exception e) {
			throw new SchedulerException(e);
		}
	}

	@Override
	public void stop() {
		logger.info("stop quartz service");
		try {
			if (scheduler != null) {
				if (!scheduler.isShutdown()) {
					scheduler.shutdown(true);
				}
				scheduler = null;
			}
		} catch (Exception e) {
			throw new SchedulerException(e);
		}
	}

	@Override
	public List<ScheduledJobInfo> getScheduledJobs() {
		List<ScheduledJobInfo> list = Lists.newArrayList();
		if (scheduler != null) {
			try {
				scheduler.getJobKeys(GroupMatcher.anyJobGroup()).forEach((jobKey) -> {
					try {
						String name = jobKey.getName();
						String triggers = Joiner.on(",").join(scheduler.getTriggersOfJob(jobKey).stream().map((trigger) -> {
							if (trigger instanceof CronTrigger) {
								return ((CronTrigger) trigger).getCronExpression();
							} else {
								return trigger.toString();
							}
						}).collect(toList()));
						list.add(new SimpleScheduledJob(name, triggers));
					} catch (Exception e) {
						throw new SchedulerException(e);
					}

				});
			} catch (Exception e) {
				throw new SchedulerException(e);
			}
		}
		return list;
	}

	private class AutowiringSpringBeanJobFactory extends SimpleJobFactory implements JobFactory {

		private final AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();

		@Override
		public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws org.quartz.SchedulerException {
			Job job = super.newJob(bundle, scheduler);
			beanFactory.autowireBean(job);
			return job;
		}
	}

	private org.quartz.Trigger toQuartzTrigger(JobTrigger trigger) {
		try {
			checkNotNull(trigger);
			if (trigger instanceof OneTimeTrigger) {
				return TriggerBuilder.newTrigger().withIdentity(format("onetimetrigger_%s", hash(trigger.toString(), 10))).withSchedule(simpleSchedule().withRepeatCount(0)).startAt(((OneTimeTrigger) trigger).getDate()).build();
			} else if (trigger instanceof RecurringTrigger) {
				return TriggerBuilder.newTrigger().withIdentity(format("crontrigger_%s", hash(trigger.toString(), 10))).withSchedule(cronSchedule(((RecurringTrigger) trigger).getCronExpression())).build();
			} else {
				throw new IllegalArgumentException(format("unsupported job trigger = %s (%s)", trigger, trigger.getClass()));
			}
		} catch (Exception ex) {
			throw new SchedulerException(ex, "error creating quartz trigger from jobTrigger = %s", trigger);
		}
	}

	private String idForTrigger(JobTrigger trigger) {
		try {
			checkNotNull(trigger);
			if (trigger instanceof OneTimeTrigger) {
				return Hex.encodeHexString(("time: " + ((OneTimeTrigger) trigger).getDate().getTime()).getBytes());
			} else if (trigger instanceof RecurringTrigger) {
				return Hex.encodeHexString(("cron: " + ((RecurringTrigger) trigger).getCronExpression()).getBytes());
			} else {
				throw new IllegalArgumentException(format("unsupported job trigger = %s (%s)", trigger, trigger.getClass()));
			}
		} catch (Exception ex) {
			throw new SchedulerException(ex, "error creating quartz trigger from jobTrigger = %s", trigger);
		}
	}
}
