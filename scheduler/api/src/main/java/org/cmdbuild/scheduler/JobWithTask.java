package org.cmdbuild.scheduler;

public interface JobWithTask {

	/**
	 *
	 * @return the name used to identify the Job
	 */
	public String getName();

	/**
	 * The method called by the scheduler
	 */
	public void execute();

	public void setTaskId(Long taskId);

	public Long getTaskId();
}
