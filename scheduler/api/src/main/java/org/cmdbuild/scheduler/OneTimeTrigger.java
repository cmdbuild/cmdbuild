package org.cmdbuild.scheduler;

import java.util.Date;


public class OneTimeTrigger implements JobTrigger {

	private final Date date;

	private OneTimeTrigger(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public static OneTimeTrigger at(Date date) {
		return new OneTimeTrigger(date);
	}

	@Override
	public String toString() {
		return "OneTimeTrigger{" + "date=" + date + '}';
	}

}
