/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.scheduler;

/**
 * bean interface for scheduled job
 *
 * @author davide
 */
public interface ScheduledJobInfo {

	public String getName();

	public String getTrigger();
}
