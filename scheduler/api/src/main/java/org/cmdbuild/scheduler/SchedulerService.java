package org.cmdbuild.scheduler;

import java.util.List;

public interface SchedulerService {

	void add(JobWithTask job, JobTrigger trigger);

	void remove(JobWithTask job);

	boolean isStarted(JobWithTask job);

	void start();

	void stop();

	List<ScheduledJobInfo> getScheduledJobs();

	boolean isRunning();

}
