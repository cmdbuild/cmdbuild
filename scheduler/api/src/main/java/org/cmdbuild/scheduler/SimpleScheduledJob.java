/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.scheduler;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * bean interface for scheduled job
 *
 * @author davide
 */
public class SimpleScheduledJob implements ScheduledJobInfo {

	private final String name, trigger;

	public SimpleScheduledJob(String name, String trigger) {
		this.name = checkNotNull(name);
		this.trigger = checkNotNull(trigger);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getTrigger() {
		return trigger;
	}

}
