package org.cmdbuild.scheduler;

public class Triggers {

	public static JobTrigger everySecond() {
		return RecurringTrigger.at("0/1 * * * * ?");
	}

	public static JobTrigger everyMinute() {
		return RecurringTrigger.at("0 * * * * ?");
	}

	public static JobTrigger everyHour() {
		return RecurringTrigger.at("0 5 * * * ?");
	}

	private Triggers() {
		// prevents instantiation
	}

}
