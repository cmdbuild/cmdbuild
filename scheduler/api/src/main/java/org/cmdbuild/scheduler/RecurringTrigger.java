package org.cmdbuild.scheduler;

import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public class RecurringTrigger implements JobTrigger {

	private final String cronExpression;

	private RecurringTrigger(String cronExpression) {
		this.cronExpression = checkNotBlank(cronExpression);
	}

	public static RecurringTrigger at(String cronExpression) {
		return new RecurringTrigger(cronExpression);
	}

	public String getCronExpression() {
		return cronExpression;
	}

	@Override
	public String toString() {
		return "RecurringTrigger{" + "cronExpression=" + cronExpression + '}';
	}

}
