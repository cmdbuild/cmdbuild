package org.cmdbuild.scheduler.command;

import com.google.common.collect.ForwardingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ForwardingCommand extends ForwardingObject implements Command {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Usable by subclasses only.
	 */
	protected ForwardingCommand() {
	}

	@Override
	protected abstract Command delegate();

	@Override
	public void execute() {
		delegate().execute();
	}

}
