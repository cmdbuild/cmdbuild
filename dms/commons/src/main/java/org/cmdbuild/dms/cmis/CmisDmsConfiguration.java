package org.cmdbuild.dms.cmis;

import com.google.common.eventbus.EventBus;
import org.cmdbuild.dms.DmsConfiguration;

public interface CmisDmsConfiguration extends DmsConfiguration {

	String getCmisUrl();

	String getCmisUser();

	String getCmisPassword();

	String getCmisPath();

	EventBus getEventBus();

}
