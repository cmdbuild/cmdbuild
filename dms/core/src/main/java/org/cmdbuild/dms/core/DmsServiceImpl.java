package org.cmdbuild.dms.core;

import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.transform;
import com.google.common.collect.Maps;
import static com.google.common.collect.MoreCollectors.toOptional;
import java.util.Optional;
import javax.annotation.Nullable;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.entrytype.Classe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.dms.DmsConfiguration;
import org.cmdbuild.dms.inner.DocumentInfoAndDetail;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.dms.DocumentData;
import org.cmdbuild.dms.DocumentDataImpl;
import org.cmdbuild.dms.inner.DmsProviderService;
import org.cmdbuild.dms.inner.DocumentInfoAndDetailImpl;
import org.cmdbuild.lookup.LookupRepository;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_DISABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_ERROR;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.isBlank;
import static org.cmdbuild.utils.lang.CmdbConvertUtils.toLong;
import static org.cmdbuild.utils.lang.CmdbPreconditions.firstNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringNotBlank;
import static org.cmdbuild.utils.lang.CmdbStringUtils.toStringOrNull;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;

@Component
public class DmsServiceImpl implements DmsService, SystemService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DmsConfiguration configuration;
	private final Map<String, DmsProviderService> services;
	private final LookupRepository lookupStore;
	private final DaoService dao;

	public DmsServiceImpl(List<DmsProviderService> services, DmsConfiguration configuration, LookupRepository lookupStore, DaoService dao) {
		this.configuration = checkNotNull(configuration);
		this.services = Maps.uniqueIndex(checkNotNull(services), DmsProviderService::getDmsProviderServiceName);
		this.lookupStore = checkNotNull(lookupStore);
		this.dao = checkNotNull(dao);
	}

	@Override
	public String getServiceName() {
		return "DMS Service";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (!configuration.isEnabled()) {
			return SS_DISABLED;
		} else if (isDmsServiceOk()) {
			return SS_READY;
		} else {
			return SS_ERROR;
		}
	}

	private boolean isDmsServiceOk() {
		try {
			checkNotNull(getService());
			return true;
		} catch (Exception ex) {
			logger.warn("dms service is NOT OK : {}", ex.toString());
			return false;
		}
	}

	private DmsProviderService getService() {
		return checkNotNull(services.get(configuration.getService()), "dms service not found for name = %s", configuration.getService());
	}

	@Override
	public DocumentInfoAndDetail getCardAttachmentById(String className, long cardId, String documentId) {
		return cmisCategoryToCmdbuildCategory(dao.getClasse(className), getService().getDocument(documentId));//TODO validate card id, class id
	}

	@Override
	public List<DocumentInfoAndDetail> getCardAttachments(String className, long cardId) {
		logger.debug("search all documents for className = {} and cardId = {}", className, cardId);
		return cmisCategoryToCmdbuildCategory(dao.getClasse(className), getService().getDocuments(className, cardId));
	}

	@Override
	@Nullable
	public DocumentInfoAndDetail getCardAttachmentOrNull(String className, long cardId, String fileName) {
		return getCardAttachments(className, cardId).stream().filter((input) -> input.getDocumentId().equals(fileName)).collect(toOptional()).orElse(null);//TODO replace with more efficent query
	}

	@Override
	public List<DocumentInfoAndDetail> getCardAttachmentVersions(String className, long cardId, String filename) {
		DocumentInfoAndDetail document = getCardAttachmentWithFilename(className, cardId, filename);
		return cmisCategoryToCmdbuildCategory(dao.getClasse(className), getService().getDocumentVersions(document.getDocumentId()));
	}

	@Override
	public DocumentInfoAndDetail create(String className, long cardId, DocumentData documentData) {
		Classe classe = dao.getClasse(className);
		documentData = cmdbuildCategoryToCmisCategory(classe, documentData);
		checkArgument(getCardAttachmentOrNull(className, cardId, documentData.getFilename()) == null, "document already present for className = %s cardId = %s fileName = %s", className, cardId, documentData.getFilename());
		DocumentInfoAndDetail document = getService().create(className, cardId, documentData);
		return cmisCategoryToCmdbuildCategory(classe, document);
	}

	@Override
	public DocumentInfoAndDetail updateDocumentWithAttachmentId(String className, long cardId, String attachmentId, DocumentData documentData) {
		Classe classe = dao.getClasse(className);
		documentData = cmdbuildCategoryToCmisCategory(classe, documentData);
		DocumentInfoAndDetail document = getService().update(attachmentId, documentData);//TODO validate card id, class id
		return cmisCategoryToCmdbuildCategory(classe, document);
	}

	@Override
	public DataHandler download(String attachmentId, @Nullable String version) {
		return getService().download(attachmentId, version);
	}

	@Override
	public Optional<DataHandler> preview(String attachmentId) {
		return getService().preview(attachmentId);
	}

	@Override
	public void delete(String documentId) {
		getService().delete(documentId);
	}

	private List<DocumentInfoAndDetail> cmisCategoryToCmdbuildCategory(Classe classe, List<DocumentInfoAndDetail> list) {
		return list(transform(list, d -> cmisCategoryToCmdbuildCategory(classe, d)));
	}

	private DocumentInfoAndDetail cmisCategoryToCmdbuildCategory(Classe classe, DocumentInfoAndDetail document) {
		if (isBlank(document.getCategory())) {
			return document;
		} else {
			return DocumentInfoAndDetailImpl.copyOf(document).withCategory(categoryFromCmis(classe.getAttachmentTypeLookupTypeOrNull(), document.getCategory())).build();
		}
	}

	private DocumentData cmdbuildCategoryToCmisCategory(Classe classe, DocumentData documentData) {
		if (isBlank(documentData.getCategory())) {
			return documentData;
		} else {
			return DocumentDataImpl.copyOf(documentData).withCategory(categoryToCmis(classe.getAttachmentTypeLookupTypeOrNull(), documentData.getCategory())).build();
		}
	}

	private @Nullable
	String categoryToCmis(@Nullable String categoryLookup, @Nullable String value) {
		if (isBlank(value)) {
			return null;
		} else {
			return lookupStore.getOneByTypeAndId(firstNotBlank(categoryLookup, configuration.getDefaultDocumentCategoryLookup()), toLong(value)).getCode();
		}
	}

	private @Nullable
	String categoryFromCmis(@Nullable String categoryLookup, @Nullable Object value) {
		if (isBlank(toStringOrNull(value))) {
			return null;
		} else {
			try {
				return lookupStore.getOneByTypeAndCode(firstNotBlank(categoryLookup, configuration.getDefaultDocumentCategoryLookup()), toStringNotBlank(value)).getId().toString();
			} catch (Exception ex) {
				logger.warn(marker(), "received invalid category code from cmis = %s", value, ex);
				return null;
			}
		}
	}
}
