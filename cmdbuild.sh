#!/bin/bash

cd "$(dirname "$0")"
if ! [ -e cmdbuild/target/*.war ]; then
	mvn -am -pl cmdbuild -Dmaven.test.skip clean install || exit 1
fi

exec java -jar cmdbuild/target/*.war "$@"

