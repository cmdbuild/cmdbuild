package org.cmdbuild.auth.grant;

import com.google.common.eventbus.EventBus;
import java.util.Collection;
import java.util.List;

public interface GrantDataRepository {

	EventBus getEventBus();

	List<GrantData> getGrantsForTypeAndRole(PrivilegedObjectType type, long groupId);

	List<GrantData> getGrantsForRole(long roleId);

	List<GrantData> setGrantsForRole(long roleId, Collection<GrantData> grants);

	enum GrantDataUpdatedEvent {
		INSTANCE
	}
}
