package org.cmdbuild.auth.role;

import org.cmdbuild.auth.userrole.UserRole;
import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.MoreCollectors.toOptional;
import com.google.common.collect.Ordering;

import java.util.Map;

import com.google.common.collect.Streams;
import java.util.Collection;
import static java.util.Collections.emptyMap;
import java.util.List;
import java.util.Optional;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import javax.annotation.Nullable;
import org.cmdbuild.auth.grant.Grant;
import org.cmdbuild.auth.grant.GrantService;
import org.cmdbuild.auth.role.Role;
import org.cmdbuild.auth.role.RoleRepository;
import static org.cmdbuild.auth.role.RoleImpl.copyOf;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.utils.lang.CmdbMapUtils.toMap;

@Component
public class RoleRepositoryImpl implements RoleRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;
	private final GrantService privilegeService;

	private final Holder<List<Role>> groups;
	private final CmdbCache<String, Optional<Role>> groupsByName;
	private final CmdbCache<String, Optional<Role>> groupsById;

	public RoleRepositoryImpl(DaoService dao, GrantService privilegeService, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		this.privilegeService = checkNotNull(privilegeService);
		this.groupsByName = cacheService.newCache("groups_by_name");
		this.groupsById = cacheService.newCache("groups_by_id");
		this.groups = cacheService.newHolder("all_groups");
	}

	private void invalidateCache() {
		groupsByName.invalidateAll();
		groupsById.invalidateAll();
		groups.invalidate();
	}

	@Override
	public List<Role> getAllGroups() {
		return groups.get(this::doGetAllGroups);
	}

	@Override
	@Nullable
	public Role getOrNull(long groupId) {
		return groupsById.get(Long.toString(groupId), () -> doGetGroupWithId(groupId)).orElse(null);
	}

	private Optional<Role> doGetGroupWithId(Long groupId) {
		return getAllGroups().stream().filter((g) -> equal(g.getId(), groupId)).collect(toOptional());
	}

	@Override
	public Map<String, Role> getGroupsByName(Iterable<String> groupNames) {
		if (isEmpty(groupNames)) {
			return emptyMap();
		} else {
			Map<String, Role> map = map();
			Streams.stream(groupNames).forEach((groupName) -> {
				map.put(groupName, getGroupWithName(groupName));//leverage caching
			});
			return map;
		}
	}

	@Override
	@Nullable
	public Role getGroupWithNameOrNull(String groupName) {
		checkNotBlank(groupName);
		return groupsByName.get(groupName, () -> getAllGroups().stream().filter((g) -> equal(g.getName(), groupName)).collect(toOptional())).orElse(null);
	}

	@Override
	public Role create(Role group) {
		group = dao.create(group);
		invalidateCache();
		return group;
	}

	@Override
	public Role update(Role group) {
		group = dao.update(group);
		invalidateCache();
		return group;
	}

	@Override
	public List<UserRole> getUserGroups(long userId) {
		return dao.getJdbcTemplate().query("SELECT g.\"Id\" group_id,COALESCE(m.\"DefaultGroup\",FALSE) is_default FROM \"Role\" g JOIN \"Map_UserRole\" m ON g.\"Id\" = m.\"IdObj2\" WHERE g.\"Status\" = 'A' AND m.\"Status\" = 'A' AND m.\"IdObj1\" = ?", (r, i) -> {
			return new UserRoleImpl(getOrNull(r.getLong("group_id")), r.getBoolean("is_default"));
		}, userId).stream().sorted(Ordering.natural().onResultOf(UserRole::getDescription)).collect(toList());
	}

	@Override
	public void setUserGroups(long userId, Collection<Long> userGroupIds, @Nullable Long defaultGroupId) {
		checkArgument(defaultGroupId == null || userGroupIds.contains(defaultGroupId));

		Map<Long, UserRole> curUserGroups = getUserGroups(userId).stream().collect(toMap(UserRole::getId, identity()));
		Long curDefaultGroup = curUserGroups.values().stream().filter(UserRole::isDefault).collect(toOptional()).map(UserRole::getId).orElse(null);

		Collection<Long> toRemove = curUserGroups.keySet().stream().filter(not(userGroupIds::contains)).collect(toSet());
		Collection<Long> toAdd = userGroupIds.stream().filter(not(curUserGroups::containsKey)).collect(toSet());

		if (curDefaultGroup != null && !equal(curDefaultGroup, defaultGroupId)) {
			dao.getJdbcTemplate().update("UPDATE \"Map_UserRole\" SET \"DefaultGroup\" = FALSE WHERE \"IdObj1\" = ? AND \"IdObj2\" = ? AND \"Status\" = 'A'", userId, curDefaultGroup);
		}
		toAdd.forEach((roleToAdd) -> {
			dao.getJdbcTemplate().update("INSERT INTO \"Map_UserRole\" (\"IdClass1\",\"IdClass2\",\"IdObj1\",\"IdObj2\") VALUES ('\"User\"','\"Role\"',?,?)", userId, roleToAdd);
		});
		toRemove.forEach((roleToRemove) -> {
			dao.getJdbcTemplate().update("UPDATE \"Map_UserRole\" SET \"Status\" = 'N' WHERE \"IdObj1\" = ? AND \"IdObj2\" = ? AND \"Status\" = 'A'", userId, roleToRemove);
		});
		if (defaultGroupId != null && !equal(curDefaultGroup, defaultGroupId)) {
			dao.getJdbcTemplate().update("UPDATE \"Map_UserRole\" SET \"DefaultGroup\" = TRUE WHERE \"IdObj1\" = ? AND \"IdObj2\" = ? AND \"Status\" = 'A'", userId, defaultGroupId);
		}
	}

	private static class UserRoleImpl implements UserRole {

		private final Role group;
		private final boolean isDefault;

		public UserRoleImpl(Role group, boolean isDefault) {
			this.group = checkNotNull(group);
			this.isDefault = isDefault;
		}

		@Override
		public Role getRole() {
			return group;
		}

		@Override
		public boolean isDefault() {
			return isDefault;
		}

	}

	private List<Role> doGetAllGroups() {
		return dao.selectAll().from(Role.class).<Role>asList().stream().map((group) -> {
			Collection<Grant> allPrivileges = privilegeService.getAllPrivilegesByGroupId(group.getId());
			return copyOf(group).withPrivileges(allPrivileges).build();
		}).collect(toList());
	}

}
