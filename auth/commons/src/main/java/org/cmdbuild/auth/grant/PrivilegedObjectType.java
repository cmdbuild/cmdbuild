package org.cmdbuild.auth.grant;

public enum PrivilegedObjectType {

	POT_CLASS, POT_VIEW, POT_FILTER, POT_CUSTOMPAGE, POT_REPORT;

}
