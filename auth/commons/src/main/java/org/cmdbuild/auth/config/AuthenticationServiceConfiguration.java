/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.config;

import java.util.Collection;

/**
 *
 */
public interface AuthenticationServiceConfiguration {

	/**
	 * Returns the names of the authenticators that should be activated, or null
	 * if all authenticators should be activated.
	 *
	 * @return active authenticators or null
	 */
	Collection<String> getActiveAuthenticators();

}
