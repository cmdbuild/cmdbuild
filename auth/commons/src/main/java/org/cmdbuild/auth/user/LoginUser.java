package org.cmdbuild.auth.user;

import static com.google.common.collect.Lists.transform;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.auth.multitenant.api.UserAvailableTenantContext;
import org.cmdbuild.auth.role.RoleInfo;

public interface LoginUser {

	Long getId();

	String getUsername();

	String getDescription();

	List<RoleInfo> getRoleInfos();

	default Collection<String> getGroupNames() {
		return transform(getRoleInfos(), RoleInfo::getName);
	}

	default Collection<String> getGroupDescriptions() {
		return transform(getRoleInfos(), RoleInfo::getDescription);
	}

	/**
	 * Returns the name of the default group for this user, used to try and
	 * select the preferred group in the
	 * {@link org.cmdbuild.auth.AuthenticatedUser}
	 *
	 * @return default group name or null if not set
	 */
	String getDefaultGroupName();

	/**
	 *
	 * @return tenant context available to this user
	 */
	public UserAvailableTenantContext getAvailableTenantContext();

	/**
	 *
	 * @return the email address of the user
	 */
	String getEmail();

	/**
	 *
	 * @return true if the user is active, false otherwise
	 */
	boolean isActive();

	/**
	 *
	 * @return true if the user is a service one, false otherwise
	 */
	boolean isService();

	boolean isPasswordExpired();
	
	boolean hasMultigroupEnabled();

	ZonedDateTime getPasswordExpirationTimestamp();

	ZonedDateTime getLastPasswordChangeTimestamp();

	ZonedDateTime getLastExpiringNotificationTimestamp();

	default boolean hasDefaultGroup() {
		return isNotBlank(getDefaultGroupName());
	}

}
