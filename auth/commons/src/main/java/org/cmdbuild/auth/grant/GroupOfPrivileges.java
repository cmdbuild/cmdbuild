/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.grant;

import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

public interface GroupOfPrivileges {
	
	String getSource();

	Set<GrantPrivilege> getPrivileges();

	@Nullable
	Map<String, Set<GrantAttributePrivilege>> getAttributePrivileges();

	@Nullable
	String getFilterOrNull();

	default boolean hasFilter() {
		return isNotBlank(getFilterOrNull());
	}

	default String getFilter() {
		return checkNotBlank(getFilterOrNull());
	}

}
