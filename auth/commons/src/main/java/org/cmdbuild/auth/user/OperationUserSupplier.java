package org.cmdbuild.auth.user;

import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.auth.grant.UserPrivileges;

public interface OperationUserSupplier {

	/**
	 * Returns the current operation user (or anonymous when missing).
	 *
	 * @return the operation user for this request
	 */
	OperationUser getUser();

	default UserPrivileges getPrivileges() {
		return getUser().getPrivilegeContext();
	}

	default String getUsername() {
		return getUser().getUsername();
	}
}
