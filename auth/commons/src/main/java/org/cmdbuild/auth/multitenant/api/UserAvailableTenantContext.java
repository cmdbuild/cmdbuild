/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.multitenant.api;

import java.util.Set;
import javax.annotation.Nullable;

/**
 *
 * @author davide
 */
public interface UserAvailableTenantContext {

	@Nullable
	Long getDefaultTenantId();

	Set<Long> getAvailableTenantIds();

	boolean ignoreTenantPolicies();
}
