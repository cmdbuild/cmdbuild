/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.userconfig;

import com.google.common.base.Optional;
import java.util.Map;
import javax.annotation.Nullable;

/**
 *
 * @author davide
 */
public interface UserConfigService {

	static final String USER_CONFIG_KEY_MULTIGROUP = "cm_user_multiGroup";

	Map<String, String> getByUsername(String username);

	void setByUsername(String username, Map<String, String> data);

	/**
	 *
	 * @param username
	 * @param key
	 * @return null if key not found, Optional of string otherwise
	 */
	@Nullable
	Optional<String> getByUsername(String username, String key);

	void setByUsername(String username, String key, @Nullable String value);

	void deleteByUsername(String username, String key);

	default void setByUsernameDeleteIfNull(String username, String key, @Nullable String value) {
		if (value == null) {
			deleteByUsername(username, key);
		} else {
			setByUsername(username, key, value);
		}
	}

	default @Nullable
	String getByUsernameOrNull(String username, String key) {
		Optional<String> optional = getByUsername(username, key);
		return optional == null ? null : optional.orNull();
	}

}
