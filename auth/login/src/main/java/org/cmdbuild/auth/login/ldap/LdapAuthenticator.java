package org.cmdbuild.auth.login.ldap;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.cmdbuild.auth.login.LoginUserIdentity;
import org.cmdbuild.auth.login.PasswordAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Component;

@Component
public class LdapAuthenticator implements PasswordAuthenticator {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final LdapAuthenticatorConfiguration configuration;

	public LdapAuthenticator(LdapAuthenticatorConfiguration configuration) {
		this.configuration = configuration;
	}

	@Override
	public String getName() {
		return "LdapAuthenticator";
	}

	@Override
	public boolean isPasswordValid(LoginUserIdentity login, String password) {
		try {
			DirContext ctx = initialize();
			String ldapUser = getUser(ctx, login.getValue());
			if ((ldapUser != null) && bind(ctx, ldapUser, password)) {
				return true;
			}
		} catch (NamingException e) {
			logger.warn("authentication error", e);
		}
		logger.warn("cannot authenticate user '{}' on LDAP", login.getValue());
		return false;
	}

	private DirContext initialize() throws NamingException {
		Hashtable<Object, Object> env = new Hashtable<>(System.getProperties());
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, configuration.getLdapUrl());
		env.put(Context.REFERRAL, "follow");
		try {
			DirContext ctx = new InitialDirContext(env);
			setOriginalAuthentication(ctx);
			return ctx;
		} catch (NamingException e) {
			logger.warn("cannot set LDAP properties", e);
			throw e;
		}
	}

	private String getUser(DirContext ctx, String userToFind) {
		SearchControls sc = new SearchControls();
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String usertobind = null;
		String searchFilter = generateSearchFilter(userToFind);
		try {
			NamingEnumeration<SearchResult> results = ctx.search(configuration.getLdapBaseDN(), searchFilter, sc);
			if (results.hasMore()) {
				SearchResult sr = results.next();
				usertobind = sr.getNameInNamespace();
			}
		} catch (NamingException e) {
			logger.warn("LDAP error", e);
			assert usertobind == null;
		}
		return usertobind;
	}

	private String generateSearchFilter(String userToFind) {
		String searchFilter = configuration.getLdapSearchFilter();
		String searchQuery;
		if (searchFilter != null) {
			searchQuery = String.format("(&%s(%s=%s))", configuration.getLdapSearchFilter(),
					configuration.getLdapBindAttribute(), userToFind);
		} else {
			searchQuery = String.format("(%s=%s)", configuration.getLdapBindAttribute(), userToFind);
		}
		logger.debug("LDAP generated search query: " + searchQuery.toString());
		return searchQuery.toString();
	}

	private boolean bind(DirContext ctx, String username, String password) throws NamingException {
		boolean validate = false;
		try {
			logger.debug("setting simple bind to authenticate");
			ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
			logger.debug("binding with username '{}'", username);
			ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, username);
			logger.trace("binding with password '{}'", password);
			ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, password);
			ctx.getAttributes(EMPTY, null);
			validate = true;
		} catch (NamingException e) {
			logger.warn("error while binding", e);
			logger.info("cannot execute LDAP authentication for user '{}'", username);
			setOriginalAuthentication(ctx);
			validate = false;
		} finally {
			// Terminate context
			ctx.close();
		}
		return validate;
	}

	private void setOriginalAuthentication(DirContext ctx) throws NamingException {
		logger.debug("restoring defaults");
		ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, configuration.getLdapAuthenticationMethod());
		if (isNotEmpty(configuration.getLdapPrincipal())) {
			ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, configuration.getLdapPrincipal());
		}
		if (isNotEmpty(configuration.getLdapPrincipalCredentials())) {
			ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, configuration.getLdapPrincipalCredentials());
		}
	}


}
