/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.multitenant;

import static com.google.common.base.Objects.equal;
import com.google.common.base.Optional;
import org.cmdbuild.auth.multitenant.api.TenantLoginData;
import org.cmdbuild.auth.multitenant.api.UserTenantContext;
import org.cmdbuild.auth.multitenant.api.MultitenantService;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import static com.google.common.collect.Iterables.getOnlyElement;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import static com.google.common.collect.Streams.stream;
import static java.lang.String.format;
import java.sql.ResultSet;
import java.util.Collections;
import static java.util.Collections.emptySet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.cmdbuild.auth.multitenant.UserAvailableTenantContextImpl.fullAccess;
import static org.cmdbuild.auth.multitenant.UserAvailableTenantContextImpl.minimalAccess;
import org.cmdbuild.auth.multitenant.api.TenantInfo;
import org.cmdbuild.auth.multitenant.config.MultitenantConfiguration;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.IGNORE_TENANT_POLICIES_TENANT_ID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.cmdbuild.auth.multitenant.api.UserAvailableTenantContext;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MULTITENANT_CONFIG_PROPERTY_MODE;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MULTITENANT_CONFIG_PROPERTY_TENANT_CLASS;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MULTITENANT_CONFIG_PROPERTY_TENANT_DOMAIN;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MultitenantMode.CMDBUILD_CLASS;
import static org.cmdbuild.auth.multitenant.config.MultitenantConfiguration.MultitenantMode.DB_FUNCTION;
import static org.cmdbuild.auth.user.UserData.USER_CLASS_NAME;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;
import static org.cmdbuild.utils.lang.CmdbExceptionUtils.unsupported;
import static org.cmdbuild.utils.lang.CmdbMapUtils.map;
import static org.cmdbuild.utils.lang.CmdbPreconditions.trimAndCheckNotBlank;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.config.api.GlobalConfigService;
import org.cmdbuild.dao.beans.DomainMetadataImpl;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.entrytype.ClassPermissionMode.CPM_PROTECTED;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.DomainDefinitionImpl;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_DISABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_ERROR;
import static org.cmdbuild.services.SystemServiceStatus.SS_READY;

/**
 *
 */
@Component
public class MultitenantServiceImpl implements MultitenantService, SystemService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final MultitenantConfiguration config;
	private final JdbcTemplate jdbcTemplate;
	private final GlobalConfigService configService;
	private final DaoService dao;

	public MultitenantServiceImpl(MultitenantConfiguration config, JdbcTemplate jdbcTemplate, GlobalConfigService configService, DaoService dao) {
		this.config = checkNotNull(config);
		this.jdbcTemplate = checkNotNull(jdbcTemplate);
		this.configService = checkNotNull(configService);
		this.dao = checkNotNull(dao);
	}

	@Override
	public String getServiceName() {
		return "Multitenant";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (!isEnabled()) {
			return SS_DISABLED;
		} else {
			return checkConfig() == true ? SS_READY : SS_ERROR;
		}
	}

	@Override
	public boolean canControlState() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return config.isMultitenantEnabled();
	}

	@Override
	public UserTenantContext buildUserTenantContext(UserAvailableTenantContext availableTenantContext, @Nullable TenantLoginData tenantLoginData) {
//		logger.debug("loading userTenantContext for user = {}", authUser);
//		UserAvailableTenantContext availableTenantContext = authUser.getAvailableTenantContext();
		Set<Long> tenantIds = availableTenantContext.getAvailableTenantIds(),
				activeTenantIds = (tenantLoginData == null || tenantLoginData.getActiveTenants() == null) ? tenantIds : Sets.intersection(tenantIds, tenantLoginData.getActiveTenants());
		boolean ignoreTenantPolicies = tenantLoginData == null ? false : tenantLoginData.ignoreTenantPolicies();
		checkArgument(ignoreTenantPolicies == false || availableTenantContext.ignoreTenantPolicies() == true, "unable to set ignoreTenantPolicies = true for this user: permission denied");
		Long defaultTenant = Optional.fromNullable(tenantLoginData == null ? null : tenantLoginData.getDefaultTenant())
				.or(Optional.fromNullable(availableTenantContext.getDefaultTenantId()))
				.or(Optional.fromNullable(activeTenantIds.size() == 1 ? Iterables.getOnlyElement(activeTenantIds) : null))
				.orNull();
		checkArgument(defaultTenant == null || tenantIds.contains(defaultTenant));
		UserTenantContext userTenantContext = new UserTenantContextImpl(ignoreTenantPolicies, activeTenantIds, defaultTenant);
//		logger.debug("loaded userTenantContext for user = {}, value = {}", authUser, userTenantContext);
		return userTenantContext;
	}

	@Override
	public UserAvailableTenantContext getAvailableTenantContextForUser(Long userId) {
		logger.debug("fetchTenantIdsForUser = {}", userId);
		checkNotNull(userId);
		logger.debug("multitenant mode = {}", config.getMultitenantMode());
		try {
			Set<Long> tenantIds = fetchTenantIds(checkNotNull(userId));
			logger.debug("tenantIds = {}", tenantIds);
			boolean ignoreTenantPolicies = tenantIds.contains(IGNORE_TENANT_POLICIES_TENANT_ID);
			if (ignoreTenantPolicies) {
				tenantIds = getAllActiveTenantIds();//admin see all active tenants as available
			}
			return new UserAvailableTenantContextImpl(tenantIds.size() == 1 ? getOnlyElement(tenantIds) : null, tenantIds, ignoreTenantPolicies);
		} catch (Exception ex) {
			logger.error(marker(), "unable to retrieve tenant context for user = %s", userId, ex);
			return minimalAccess();
		}
	}

	private boolean checkConfig() {
		try {
			fetchTenantIds(-1l);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	@Override
	public UserAvailableTenantContext getAdminAvailableTenantContext() {
		try {
			Set<Long> tenantIds = getAllActiveTenantIds();
			return new UserAvailableTenantContextImpl(tenantIds.size() == 1 ? getOnlyElement(tenantIds) : null, tenantIds, true);
		} catch (Exception ex) {
			logger.error(marker(), "unable to retrieve tenant context for admin", ex);
			return fullAccess();
		}
	}

	@Override
	public Set<Long> getAllActiveTenantIds() {
		return fetchTenantIds(null);
	}

	@Override
	public void enableMultitenantFunctionMode() {
		if (!equal(config.getMultitenantMode(), DB_FUNCTION)) {
			checkArgument(config.isMultitenantDisabled(), "cannot change multitenant mode: operation not allowed");
			configService.putString(MULTITENANT_CONFIG_PROPERTY_MODE, DB_FUNCTION.name());
		}
	}

	@Override
	public void enableMultitenantClassMode(String tenantClassId) { //TODO make this method transactional
		if (equal(config.getMultitenantMode(), CMDBUILD_CLASS)) {
			checkArgument(equal(tenantClassId, config.getTenantClass()), "cannot change tenant class: operation not allowed");
		} else {
			checkArgument(config.isMultitenantDisabled(), "cannot change multitenant mode: operation not allowed");

			Classe tenantClass = dao.getClasse(tenantClassId),
					userClass = dao.getClasse(USER_CLASS_NAME);
			Domain tenantDomain = dao.createDomain(DomainDefinitionImpl.builder()
					.withSourceClass(userClass)
					.withTargetClass(tenantClass)
					.withName(format("%s%s", userClass.getName(), tenantClass.getName()))
					.withMetadata(DomainMetadataImpl.builder()
							.withMode(CPM_PROTECTED)
							.withCardinality("N:N")//TODO
							.withDirectDescription("belongs to tenant")
							.withInverseDescription("has tenant user")
							.build())
					.build());
			String tenantDomainId = tenantDomain.getName();
			configService.putStrings(map(MULTITENANT_CONFIG_PROPERTY_MODE, CMDBUILD_CLASS.name(),
					MULTITENANT_CONFIG_PROPERTY_TENANT_CLASS, tenantClassId,
					MULTITENANT_CONFIG_PROPERTY_TENANT_DOMAIN, tenantDomainId
			));
		}
	}

	/**
	 *
	 * @param userId if null, return all active ids
	 * @return
	 */
	private Set<Long> fetchTenantIds(@Nullable Long userId) {
		switch (config.getMultitenantMode()) {
			case DISABLED: {
				//disabled, do nothing
				return emptySet();
			}
			case CMDBUILD_CLASS: {
				String tenantClass = trimAndCheckNotBlank(config.getTenantClass());
				String tenantDomainName = trimAndCheckNotBlank(config.getTenantDomain());

				if (userId == null) {
					return ImmutableSet.copyOf(jdbcTemplate.queryForList(format("SELECT \"Id\" FROM \"%s\" WHERE \"Status\" = 'A'", tenantClass), Long.class));
				} else {
					logger.debug("getting tenants for user = {} from tenant class = {} domain = {}", userId, tenantClass, tenantDomainName);
					return ImmutableSet.copyOf(jdbcTemplate.query(format("SELECT m.\"IdObj2\" tenant_id FROM \"Map_%s\" m WHERE m.\"Status\" = 'A' AND m.\"IdObj1\" = ?", tenantDomainName),//TODO handle inverse domains
							(ResultSet rs, int rowNum) -> rs.getLong("tenant_id"), userId));
				}
			}
			case DB_FUNCTION: {
				String functionName = trimAndCheckNotBlank(config.getDbFunction(), "multitenant db function name cannot be null");
				checkArgument(functionName.matches("[a-z0-9_]+"), "unsupported multitenant function name syntax %s (must match /^[a-z0-9_]+$/)", functionName);
				logger.debug("querying tenant function = {} for user id = {}", functionName, userId);
				return ImmutableSet.copyOf(jdbcTemplate.queryForList("SELECT " + functionName + "(?)", new Object[]{firstNonNull(userId, -1l)}, Long.class));
			}
			default:
				throw unsupported("unsupported multitenant mode = %s", config.getMultitenantMode());
		}
	}

	@Override
	public Map<Long, String> getTenantDescriptions(Iterable<Long> tenantIds) {
		logger.debug("get tenant descriptions for tenants = {}", tenantIds);
		if (Iterables.isEmpty(tenantIds)) {
			return Collections.emptyMap();
		} else {
			Map<Long, String> map = map();
			jdbcTemplate.query(format("SELECT \"Id\",\"Description\" FROM \"Class\" WHERE \"Id\" IN (%s)", stream(tenantIds).map((t) -> "?").collect(joining(","))), (ResultSet rs) -> { //TODO translation
				map.put(rs.getLong("Id"), rs.getString("Description"));
			}, list(tenantIds).toArray());
			return map;
		}
	}

	@Override
	public List<TenantInfo> getAllActiveTenants() {
		Set<Long> tenantIds = getAllActiveTenantIds();
		Map<Long, String> tenantDescriptions = getTenantDescriptions(tenantIds);
		return tenantIds.stream().map((id) -> new TenantInfoImpl(id, tenantDescriptions.get(id))).sorted(Ordering.natural().onResultOf(TenantInfo::getDescription)).collect(toList());
	}

	@Override
	public List<TenantInfo> getAvailableUserTenants(UserAvailableTenantContext tenantContext) {
		return getAllActiveTenants().stream().filter((t) -> tenantContext.getAvailableTenantIds().contains(t.getId())).collect(toList());
	}

	private static class TenantInfoImpl implements TenantInfo {

		private final long id;
		private final String description;

		public TenantInfoImpl(Long id, String description) {
			this.id = id;
			this.description = description;
		}

		@Override
		public Long getId() {
			return id;
		}

		@Override
		public String getDescription() {
			return description;
		}

	}

}
