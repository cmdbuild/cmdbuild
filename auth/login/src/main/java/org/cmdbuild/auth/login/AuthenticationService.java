package org.cmdbuild.auth.login;

import org.cmdbuild.auth.user.OperationUserStore;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;
import org.cmdbuild.auth.role.Role;
import org.cmdbuild.auth.role.RoleInfo;
import org.cmdbuild.auth.user.LoginUser;
import org.cmdbuild.auth.login.ClientRequestAuthenticator.RequestInfo;
import org.cmdbuild.auth.user.OperationUser;

public interface AuthenticationService {

	LoginUser checkPasswordAndGetUser(LoginUserIdentity login, String password);

	ClientAuthenticatorResponse authenticate(RequestInfo request);

	LoginUser getUserByIdOrNull(Long userId);

	LoginUser getUser(LoginUserIdentity identity);

	String getPassword(LoginUserIdentity identity);

	@Nullable
	LoginUser getUserByUsernameOrNull(String username);

	default LoginUser getUserByUsername(String username) {
		return checkNotNull(getUserByUsernameOrNull(username), "user not found for username = %s", username);
	}

	Collection<Role> getAllGroups();

	Role fetchGroupWithId(Long groupId);

	@Nullable
	Role getGroupWithNameOrNull(String groupName);

	OperationUser validateCredentialsAndCreateOperationUser(LoginData loginData);

	OperationUser updateOperationUser(LoginData loginData, OperationUser operationUser);

	ClientAuthenticationResponse login(RequestInfo request, OperationUserStore userStore);

	RoleInfo getGroupInfoForGroup(String groupName);

	Collection<String> getGroupNamesForUserWithId(Long userId);

	Collection<String> getGroupNamesForUserWithUsername(String loginString);

	LoginUser getUserWithId(Long userId);

	Role getGroupWithId(Long groupId);

	Role getGroupWithName(String groupName);

	class ClientAuthenticatorResponse {

		private final LoginUser user;
		private final String redirectUrl;

		public ClientAuthenticatorResponse(@Nullable LoginUser user, @Nullable String redirectUrl) {
			this.user = user;
			this.redirectUrl = redirectUrl;
		}

		public @Nullable
		LoginUser getUserOrNull() {
			return user;
		}

		public @Nullable
		String getRedirectUrlOrNull() {
			return redirectUrl;
		}
	}

	interface ClientAuthenticationResponse {

		@Nullable
		String getRedirectUrlOrNull();

	}

}
