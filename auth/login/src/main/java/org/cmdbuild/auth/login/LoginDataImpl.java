package org.cmdbuild.auth.login;

import static com.google.common.collect.ImmutableSet.copyOf;
import java.util.Collection;
import java.util.Set;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.cmdbuild.auth.multitenant.api.TenantLoginData;

public class LoginDataImpl implements LoginData, TenantLoginData {

	private final String loginString;
	private final String unencryptedPassword;
	private final String loginGroupName;
	private final boolean passwordRequired, serviceUsersAllowed, ignoreTenantPolicies;
	private final Long defaultTenant;
	private final Set<Long> activeTenants;

	private LoginDataImpl(SimpleLoginDataBuilder builder) {
		this.loginString = builder.loginString;
		this.unencryptedPassword = builder.unencryptedPassword;
		this.loginGroupName = builder.loginGroupName;
		this.passwordRequired = builder.passwordRequired;
		this.serviceUsersAllowed = builder.serviceUsersAllowed;
		this.defaultTenant = builder.defaultTenant;
		this.activeTenants = builder.activeTenants;
		this.ignoreTenantPolicies = builder.ignoreTenantPolicies;
	}

	@Override
	public String getLoginString() {
		return loginString;
	}

	@Override
	public String getPassword() {
		return unencryptedPassword;
	}

	@Override
	public String getLoginGroupName() {
		return loginGroupName;
	}

	@Override
	public boolean isPasswordRequired() {
		return passwordRequired;
	}

	@Override
	public boolean isServiceUsersAllowed() {
		return serviceUsersAllowed;
	}

	public @Nullable
	@Override
	Long getDefaultTenant() {
		return defaultTenant;
	}

	public @Nullable
	@Override
	Set<Long> getActiveTenants() {
		return activeTenants;
	}

	@Override
	public boolean ignoreTenantPolicies() {
		return ignoreTenantPolicies;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, SHORT_PREFIX_STYLE);
	}

	public static SimpleLoginDataBuilder builder() {
		return new SimpleLoginDataBuilder();
	}

	public static class SimpleLoginDataBuilder implements org.apache.commons.lang3.builder.Builder<LoginDataImpl> {

		private String loginString;
		private String unencryptedPassword;
		private String loginGroupName;
		public boolean passwordRequired = true;
		public boolean serviceUsersAllowed, ignoreTenantPolicies = false;
		private Long defaultTenant;
		private Set<Long> activeTenants;

		@Override
		public LoginDataImpl build() {
			return new LoginDataImpl(this);
		}

		/**
		 *
		 * @param loginString could be the either the username or the email
		 * @return
		 */
		public SimpleLoginDataBuilder withLoginString(final String loginString) {
			this.loginString = loginString;
			return this;
		}

		public SimpleLoginDataBuilder withPassword(final String unencryptedPassword) {
			this.unencryptedPassword = unencryptedPassword;
			this.passwordRequired = true;
			return this;
		}

		public SimpleLoginDataBuilder withGroupName(final String loginGroupName) {
			this.loginGroupName = loginGroupName;
			return this;
		}

		public SimpleLoginDataBuilder withNoPasswordRequired() {
			this.passwordRequired = false;
			return this;
		}

		public SimpleLoginDataBuilder allowServiceUser() {
			return this.withServiceUsersAllowed(true);
		}

		public SimpleLoginDataBuilder withIgnoreTenantPolicies(boolean ignoreTenantPolicies) {
			this.ignoreTenantPolicies = ignoreTenantPolicies;
			return this;
		}

		public SimpleLoginDataBuilder withServiceUsersAllowed(boolean serviceUsersAllowed) {
			this.serviceUsersAllowed = serviceUsersAllowed;
			return this;
		}

		public SimpleLoginDataBuilder withDefaultTenant(@Nullable Long defaultTenant) {
			this.defaultTenant = defaultTenant;
			return this;
		}

		public SimpleLoginDataBuilder withActiveTenants(@Nullable Collection<Long> activeTenants) {
			this.activeTenants = activeTenants == null ? null : copyOf(activeTenants);
			return this;
		}

	}
}
