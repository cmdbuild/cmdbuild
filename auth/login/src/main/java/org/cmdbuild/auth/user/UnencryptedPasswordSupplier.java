package org.cmdbuild.auth.user;

import org.cmdbuild.auth.login.LoginUserIdentity;

public interface UnencryptedPasswordSupplier {

	String getUnencryptedPassword(LoginUserIdentity login);

}
