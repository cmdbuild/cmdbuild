package org.cmdbuild.auth.user;

public interface User {

	int getId();

	String getName();

	String getDescription();

	String getEncryptedPassword();
}
