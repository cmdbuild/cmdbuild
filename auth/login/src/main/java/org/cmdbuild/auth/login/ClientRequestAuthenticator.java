package org.cmdbuild.auth.login;

public interface ClientRequestAuthenticator extends AuthenticatorDelegate {

	RequesthAuthenticatorResponse authenticate(RequestInfo request);

	interface RequestInfo {

		String getRequestUrl();

		String getHeader(String name);

		String getParameter(String name);
	}

	interface RequesthAuthenticatorResponse {

		LoginUserIdentity getLogin();

		String getRedirectUrl();
	}

}
