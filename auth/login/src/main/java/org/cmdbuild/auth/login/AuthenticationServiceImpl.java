package org.cmdbuild.auth.login;

import static com.google.common.base.Preconditions.checkArgument;
import org.cmdbuild.auth.user.UnencryptedPasswordSupplier;
import org.cmdbuild.auth.user.OperationUserStore;
import org.cmdbuild.auth.user.UserPrivilegesImpl;
import org.cmdbuild.auth.user.UserRepository;
import org.cmdbuild.auth.role.RoleRepository;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Collections2.transform;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.cmdbuild.auth.login.AuthenticationService.ClientAuthenticatorResponse;
import org.cmdbuild.auth.user.OperationUser;

import com.google.common.collect.Lists;
import static java.util.Collections.emptyList;
import javax.annotation.Nullable;
import org.cmdbuild.auth.config.AuthenticationServiceConfiguration;
import static org.cmdbuild.auth.login.NullPrivilegeContext.nullPrivilegeContext;
import org.cmdbuild.auth.multitenant.api.UserTenantContext;
import org.cmdbuild.auth.multitenant.api.MultitenantService;
import static org.cmdbuild.auth.user.OperationUserImpl.builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.common.error.ErrorAndWarningCollectorService.marker;
import org.cmdbuild.auth.grant.UserPrivileges;
import org.cmdbuild.auth.role.Role;
import org.cmdbuild.auth.role.RoleInfo;
import org.cmdbuild.auth.user.LoginUser;
import org.cmdbuild.auth.login.ClientRequestAuthenticator.RequestInfo;
import static org.cmdbuild.auth.role.RolePrivilege.RP_DATA_ALL_TENANT;
import org.cmdbuild.auth.user.LoginUserImpl;
import static org.cmdbuild.utils.lang.CmdbCollectionUtils.list;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final RoleRepository groupRepository;
	private final MultitenantService multitenantService;
	private final List<PasswordAuthenticator> passwordAuthenticators;
	private final List<ClientRequestAuthenticator> clientRequestAuthenticators;
	private final UserRepository userRepository;
	private final AuthenticationServiceConfiguration conf;
	private final UnencryptedPasswordSupplier passwordService;

	public AuthenticationServiceImpl(UnencryptedPasswordSupplier passwordService, RoleRepository groupRepository, MultitenantService multitenantService, List<PasswordAuthenticator> passwordAuthenticators, List<ClientRequestAuthenticator> clientRequestAuthenticators, UserRepository userRepository, AuthenticationServiceConfiguration conf) {
		this.groupRepository = checkNotNull(groupRepository);
		this.multitenantService = checkNotNull(multitenantService);
		this.passwordAuthenticators = checkNotNull(passwordAuthenticators);
		this.clientRequestAuthenticators = checkNotNull(clientRequestAuthenticators);
		this.userRepository = checkNotNull(userRepository);
		this.conf = checkNotNull(conf);
		this.passwordService = checkNotNull(passwordService);
	}

	protected Iterable<PasswordAuthenticator> getPasswordAuthenticators() {
		return copyOf(passwordAuthenticators);
	}

	protected Iterable<PasswordAuthenticator> getActivePasswordAuthenticators() {
		return filter(getPasswordAuthenticators(), this::isActive);
	}

	protected Iterable<ClientRequestAuthenticator> getClientRequestAuthenticators() {
		return copyOf(clientRequestAuthenticators);
	}

	protected Iterable<ClientRequestAuthenticator> getActiveClientRequestAuthenticators() {
		return filter(getClientRequestAuthenticators(), this::isActive);
	}

	private boolean isActive(AuthenticatorDelegate authenticator) {
		Collection<String> authenticatorNames = conf.getActiveAuthenticators();
		return authenticatorNames.contains(authenticator.getName());
	}

	@Override
	public LoginUser checkPasswordAndGetUser(LoginUserIdentity identity, String password) {
		try {
			for (PasswordAuthenticator passwordAuthenticator : getActivePasswordAuthenticators()) {
				boolean isUserAuthenticated = passwordAuthenticator.isPasswordValid(identity, password);
				if (isUserAuthenticated) {
					return userRepository.getUser(identity);
				}
			}
		} catch (Exception ex) {
			throw new AuthenticationException("authentication error for user = %s", identity, ex);
		}
		throw new AuthenticationException("invalid login credentials for user = %s", identity);
	}

	@Override
	public ClientAuthenticatorResponse authenticate(RequestInfo request) {
		for (ClientRequestAuthenticator clientRequestAuthenticator : getActiveClientRequestAuthenticators()) {
			ClientRequestAuthenticator.RequesthAuthenticatorResponse response = clientRequestAuthenticator.authenticate(request);
			if (response != null) {
				LoginUser authUser = null;
				if (response.getLogin() != null) {
					authUser = getUserOrNull(response.getLogin());
				}
				return new ClientAuthenticatorResponse(authUser, response.getRedirectUrl());
			}
		}
		throw new AuthenticationException("invalid login credentials for request = %s", request);
	}

	@Override
	public LoginUser getUserByUsernameOrNull(String username) {
		LoginUserIdentity login = LoginUserIdentity.builder() //
				.withValue(username) //
				.build();
		return getUserOrNull(login);
	}

	@Override
	public LoginUser getUser(LoginUserIdentity identity) {
		return userRepository.getUser(identity);
	}

	@Override
	public String getPassword(LoginUserIdentity identity) {
		return passwordService.getUnencryptedPassword(identity);
	}

	private @Nullable
	LoginUser getUserOrNull(LoginUserIdentity login) {
		return userRepository.getUserOrNull(login);
	}

	@Override
	public @Nullable
	LoginUser getUserByIdOrNull(Long userId) {
		return userRepository.getUserByIdOrNull(userId);
	}

	@Override
	public Collection<Role> getAllGroups() {
		return groupRepository.getAllGroups();
	}

	@Override
	public Role fetchGroupWithId(Long groupId) {
		return groupRepository.getOrNull(groupId);
	}

	@Override
	public Role getGroupWithNameOrNull(String groupName) {
		return groupRepository.getGroupWithNameOrNull(groupName);
	}

	@Override
	public OperationUser validateCredentialsAndCreateOperationUser(LoginData loginData) {
		logger.debug("try to login user = {} with group = {} and full info = {}", loginData.getLoginString(), loginData.getLoginGroupName(), loginData);
		LoginUser authUser;
		LoginUserIdentity identity = LoginUserIdentity.build(loginData.getLoginString());
		if (loginData.isPasswordRequired()) {
			authUser = checkPasswordAndGetUser(identity, loginData.getPassword());
		} else {
			authUser = getUser(identity);
		}

		if (!loginData.isServiceUsersAllowed() && authUser.isService()) {
			throw new AuthenticationException("login failed");
		}

		return buildOperationUser(loginData, authUser);
	}

	@Override
	public OperationUser updateOperationUser(LoginData loginData, OperationUser operationUser) {
		return buildOperationUser(loginData, operationUser.getLoginUser());
	}

	private OperationUser buildOperationUser(LoginData loginData, LoginUser authUser) {
		String groupName = loginData.getLoginGroupName();
		UserPrivileges privilegeCtx;
		Role selectedGroup;
		if (isBlank(groupName)) {
			Role guessedGroup = guessPreferredGroup(authUser);
			if (guessedGroup == null) {
				logger.warn(marker(), "The user {} does not have a default group and belongs to multiple groups", authUser.getUsername());
				List<RoleInfo> groupsForLogin = Lists.newArrayList();
				for (String name : authUser.getGroupNames()) {
					groupsForLogin.add(getGroupInfoForGroup(name));
				}
				return builder().withAuthenticatedUser(authUser).withUserTenantContext(multitenantService.buildUserTenantContext(authUser, loginData)).build();
			} else {
				selectedGroup = guessedGroup;
			}
		} else {
			checkArgument(authUser.getGroupNames().contains(groupName), "user has not group = %s", groupName);
			selectedGroup = getGroupWithName(groupName);
		}
		if (authUser.hasMultigroupEnabled() && authUser.getGroupNames().size() > 1) {
			privilegeCtx = buildPrivilegeContext(list(transform(authUser.getGroupNames(), this::getGroupWithName)));
		} else {
			privilegeCtx = buildPrivilegeContext(selectedGroup);
		}
		UserTenantContext userTenantContext;
		if (privilegeCtx.hasPrivileges(RP_DATA_ALL_TENANT)) {
			authUser = LoginUserImpl.copyOf(authUser).withAvailableTenantContext(multitenantService.getAdminAvailableTenantContext()).build();
			userTenantContext = multitenantService.buildAdminTenantContext(loginData);
		} else {
			userTenantContext = multitenantService.buildUserTenantContext(authUser, loginData);
		}
		return builder().withAuthenticatedUser(authUser).withPrivilegeContext(privilegeCtx).withDefaultGroup(selectedGroup).withUserTenantContext(userTenantContext).build();
	}

	@Override
	public ClientAuthenticationResponse login(RequestInfo request, OperationUserStore userStore) {
		logger.info("trying to login with no username or password");
		ClientAuthenticatorResponse response = authenticate(request);
		LoginUser authenticatedUser = response.getUserOrNull();
		if (authenticatedUser != null) {
			boolean hasOneGroupOnly = (authenticatedUser.getGroupNames().size() == 1);
			boolean hasDefaultGroup = (authenticatedUser.getDefaultGroupName() != null);
			logger.debug("user has one group only: {}", hasOneGroupOnly);
			logger.debug("user default group: {}", hasDefaultGroup);
			Role group;
			UserPrivileges privilegeContext;
			if (hasOneGroupOnly) {
				String name = authenticatedUser.getGroupNames().iterator().next();
				group = getGroupWithName(name);
				privilegeContext = buildPrivilegeContext(group);
			} else if (hasDefaultGroup) {
				group = getGroupWithName(authenticatedUser.getDefaultGroupName());
				Role[] groups = authenticatedUser.getGroupNames().stream() //
						.map(input -> getGroupWithName(input)) //
						.toArray(Role[]::new);
				privilegeContext = buildPrivilegeContext(groups);
			} else {
				group = null;
				privilegeContext = nullPrivilegeContext();
			}
			userStore.setUser(builder().withAuthenticatedUser(authenticatedUser).withPrivilegeContext(privilegeContext).withDefaultGroup(group)
					.withUserTenantContext(multitenantService.buildUserTenantContext(authenticatedUser, null)).build());
			return () -> response.getRedirectUrlOrNull();
		} else {
			return () -> null;
		}
	}

	/**
	 * Gets the default group (if any) or the only one. If no default group has
	 * been found and more than one group is present, {@code null} is returned.
	 */
	private Role guessPreferredGroup(LoginUser user) {
		if (user.hasDefaultGroup()) {
			return getGroupWithName(user.getDefaultGroupName());
		} else if (user.getGroupNames().size() == 1) {
			return getGroupWithName(getOnlyElement(user.getGroupNames()));
		} else {
			return null;
		}
	}

	private UserPrivileges buildPrivilegeContext(Role... groups) {
		return UserPrivilegesImpl.builder().withGroups(groups).build();
	}

	private UserPrivileges buildPrivilegeContext(Iterable<Role> groups) {
		return UserPrivilegesImpl.builder().withGroups(groups).build();
	}

	@Override
	public RoleInfo getGroupInfoForGroup(String groupName) {
		return groupRepository.getGroupWithName(groupName);
	}

	@Override
	public Collection<String> getGroupNamesForUserWithId(Long userId) {
		LoginUser user = getUserByIdOrNull(userId);
		return user == null ? emptyList() : user.getGroupNames();
	}

	@Override
	public Collection<String> getGroupNamesForUserWithUsername(String loginString) {
		LoginUser user = getUserByUsernameOrNull(loginString);
		return user == null ? emptyList() : user.getGroupNames();
	}

	@Override
	public LoginUser getUserWithId(Long userId) {
		return getUserByIdOrNull(userId);
	}

	@Override
	public Role getGroupWithId(Long groupId) {
		return fetchGroupWithId(groupId);
	}

	@Override
	public Role getGroupWithName(String groupName) {
		return getGroupWithNameOrNull(groupName);
	}

}
