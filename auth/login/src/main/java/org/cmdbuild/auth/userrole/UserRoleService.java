/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.userrole;

public interface UserRoleService {

	void addRoleToUser(long userId, long roleId);

	void removeRoleFromUser(long userId, long roleId);
}
