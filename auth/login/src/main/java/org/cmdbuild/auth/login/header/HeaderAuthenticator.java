package org.cmdbuild.auth.login.header;

import org.apache.commons.lang3.Validate;
import org.cmdbuild.auth.login.ClientRequestAuthenticator;
import org.cmdbuild.auth.login.LoginUserIdentity;
import static org.cmdbuild.auth.login.RequesthAuthenticatorResponseImpl.newLoginResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Authenticates a user based on the presence of a header parameter. It can be
 * used when a Single Sign-On proxy adds the header.
 */
@Component
public class HeaderAuthenticator implements ClientRequestAuthenticator {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final HeaderAuthenticatorConfiguration conf;

	public HeaderAuthenticator(HeaderAuthenticatorConfiguration conf) {
		Validate.notNull(conf);
		this.conf = conf;
	}

	@Override
	public String getName() {
		return "HeaderAuthenticator";
	}

	@Override
	public RequesthAuthenticatorResponse authenticate(RequestInfo request) {
		String loginString = request.getHeader(conf.getHeaderAttributeName());
		if (loginString != null) {
			LoginUserIdentity login = LoginUserIdentity.builder().withValue(loginString).build();
			logger.info("Authenticated user " + loginString);
			return newLoginResponse(login);
		} else {
			return null;
		}
	}

}
