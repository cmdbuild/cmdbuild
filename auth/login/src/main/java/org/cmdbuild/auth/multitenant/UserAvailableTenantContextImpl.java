/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.multitenant;

import com.google.common.collect.ImmutableSet;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import java.util.Set;
import javax.annotation.Nullable;
import org.cmdbuild.auth.multitenant.api.UserAvailableTenantContext;

/**
 *
 */
public class UserAvailableTenantContextImpl implements UserAvailableTenantContext {

	private final static UserAvailableTenantContext MINIMAL_ACCESS_AVAILABLE_TENANT_CONTEXT = new UserAvailableTenantContextImpl(null, emptyList(), false),
			FULL_ACCESS_AVAILABLE_TENANT_CONTEXT = new UserAvailableTenantContextImpl(null, emptyList(), true);

	private final Long defaultTenantId;
	private final Set<Long> availableTenantIds;
	private final boolean ignoreTenantPolicies;

	public UserAvailableTenantContextImpl(Long defaultTenantId, Iterable<Long> availableTenantIds, boolean ignoreTenantPolicies) {
		this.defaultTenantId = defaultTenantId;
		this.availableTenantIds = ImmutableSet.copyOf(requireNonNull(availableTenantIds));
		this.ignoreTenantPolicies = ignoreTenantPolicies;
	}

	@Override
	public @Nullable
	Long getDefaultTenantId() {
		return defaultTenantId;
	}

	@Override
	public Set<Long> getAvailableTenantIds() {
		return availableTenantIds;
	}

	@Override
	public boolean ignoreTenantPolicies() {
		return ignoreTenantPolicies;
	}

	public static UserAvailableTenantContext minimalAccess() {
		return MINIMAL_ACCESS_AVAILABLE_TENANT_CONTEXT;
	}

	public static UserAvailableTenantContext fullAccess() {
		return FULL_ACCESS_AVAILABLE_TENANT_CONTEXT;
	}

}
