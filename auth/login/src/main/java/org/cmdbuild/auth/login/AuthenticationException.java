package org.cmdbuild.auth.login;

import static java.lang.String.format;

public class AuthenticationException extends RuntimeException {

	public AuthenticationException(String message, Object... args) {
		super(format(message, args));
	}

	public AuthenticationException(Throwable cause, String message, Object... args) {
		super(format(message, args), cause);
	}

}
