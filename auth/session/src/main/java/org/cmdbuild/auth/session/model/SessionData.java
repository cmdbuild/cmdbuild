/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.model;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.Nullable;

/**
 *
 */
public interface SessionData extends Serializable {

	/**
	 *
	 * @return immutable view of session data
	 */
	Map<String, Object> getSessionData();

	default @Nullable
	<E> E get(String key) {
		return (E) getSessionData().get(key);
	}

	/**
	 * add or update value in session data (and mark session as modified for
	 * persistence); null values will not be stored (put(key, null) remove any
	 * existing value for key);
	 *
	 * all values must be string or simple, json-serializable/deserializable
	 * objects (List of strings, map of strings).
	 *
	 * @param key
	 * @param value
	 */
	void put(String key, @Nullable Object value);

	/**
	 * remove session data value (it is actually the same as put(key, null))
	 *
	 * @param key
	 */
	default void remove(String key) {
		put(key, null);
	}
}
