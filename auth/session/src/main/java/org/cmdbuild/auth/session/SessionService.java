package org.cmdbuild.auth.session;

import org.cmdbuild.auth.session.inner.SessionDataService;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.auth.session.model.Session;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.auth.login.LoginData;

public interface SessionService extends SessionDataService {

	default String createAndSet(LoginData login) {
		String session = create(login);
		setCurrent(session);
		return session;
	}

	String create(LoginData login);

	boolean exists(String id);

	void update(String id, LoginData login);

	void deleteSession(String sessionId);

	void deleteAll();

	void impersonate(String id, String username);

	String getCurrentSessionIdOrNull();

	void setCurrent(String id);

	/**
	 * return true if session exists and has default group
	 *
	 * @param id
	 * @return
	 */
	boolean sessionExistsAndHasDefaultGroup(String id);

	OperationUser getUser(String id);

	/**
	 * mostly the same as impersonate
	 *
	 * @param id
	 * @param user
	 */
	void setUser(String id, OperationUser user);

	/**
	 *
	 * @param sessionId
	 * @throws RuntimeException if session is not valid //TODO: throw different
	 * exceptions for not found-expired
	 */
	void validateSessionId(String sessionId);

	/**
	 *
	 * @param sessionId
	 * @return session data
	 * @throws RuntimeException if session not found for id
	 */
	Session getSessionById(String sessionId);

	/**
	 *
	 * @param sessionId
	 * @return session data, or null if not found
	 */
	@Nullable
	Session getSessionByIdOrNull(String sessionId);

	/**
	 * called to perform cleanup/persistence operation after processing a
	 * request that may have changed session data; also mark session as active
	 *
	 * @param session
	 */
	void updateSession(Session session);

	/**
	 * get current session; throw exception if no session available
	 *
	 * @return current session
	 * @throws RuntimeException if no valid session is available
	 */
	Session getCurrentSession();

	/**
	 * return current session, or null if no valid session exists
	 *
	 * @return current session or null
	 */
	@Nullable
	Session getCurrentSessionOrNull();

	int getActiveSessionCount();

	/**
	 *
	 * @return true if session existed and was deleted, false otherwise
	 */
	default boolean deleteCurrentSessionIfExists() {
		String sessionId = getCurrentSessionIdOrNull();
		if (isNotBlank(sessionId)) {
			deleteSession(sessionId);
			return true;
		} else {
			return false;
		}
	}

}
