/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.dao;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.isNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.filterKeys;
import static com.google.common.collect.Sets.newTreeSet;
import java.time.temporal.ChronoUnit;
import static java.util.Collections.singleton;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import static java.util.stream.Collectors.toList;
import javax.annotation.Nullable;
import javax.annotation.PreDestroy;
import org.cmdbuild.auth.user.UserPrivilegesImpl;
import org.cmdbuild.auth.multitenant.api.UserTenantContext;
import org.cmdbuild.auth.multitenant.UserTenantContextImpl;
import static org.cmdbuild.auth.session.model.InvalidSession.invalidSession;
import org.cmdbuild.auth.session.model.SessionImpl;
import org.cmdbuild.auth.session.dao.beans.SessionDataJsonBean;
import org.cmdbuild.auth.session.dao.beans.SessionDataJsonBean.AuthenticatedUserJsonBean;
import org.cmdbuild.auth.session.dao.beans.SessionDataJsonBean.OperationUserJsonBean;
import org.cmdbuild.auth.session.dao.beans.SessionDataJsonBean.UserTenantContextJsonBean;
import org.cmdbuild.auth.user.OperationUserImpl;
import org.cmdbuild.auth.user.OperationUserStack;
import org.cmdbuild.auth.user.OperationUserStackImpl;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.scheduler.spring.ScheduledJob;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import static org.cmdbuild.utils.lang.CmdbExecutorUtils.shutdownQuietly;
import org.cmdbuild.auth.session.model.Session;
import org.cmdbuild.config.CoreConfiguration;
import org.cmdbuild.cache.CmdbCache;
import static org.cmdbuild.utils.date.DateUtils.now;
import static org.cmdbuild.utils.date.DateUtils.toDateTime;
import org.cmdbuild.requestcontext.RequestContextService;
import org.cmdbuild.utils.date.DateUtils;
import org.cmdbuild.auth.role.Role;
import org.cmdbuild.auth.role.RoleRepository;
import org.cmdbuild.auth.login.AuthenticationService;
import org.cmdbuild.auth.user.LoginUser;

/**
 *
 */
@Component
public class SessionRepositoryImpl implements SessionRepository {

	public static final String SESSION_CLASS_NAME = "_Session",
			SESSION_ID_ATTRIBUTE = "SessionId",
			DATA_ATTRIBUTE = "Data",
			LAST_ACTIVE_DATE_ATTRIBUTE = "LastActiveDate";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SessionDataRepository repo;
	private final CoreConfiguration configuration;
	private final RoleRepository groupRepository;
	private final AuthenticationService authenticationService; //TODO is this correct? maybe for 'rest' session management we'll require 'rest' auth service? check this

	private final CmdbCache<String, Session> sessionCacheBySessionId;
	private final ExecutorService executorService;

	public SessionRepositoryImpl(SessionDataRepository repo, CoreConfiguration configuration, AuthenticationService authenticationService, CacheService cacheService, RequestContextService requestContextService, RoleRepository groupRepository) {
		logger.info("init");
		this.repo = checkNotNull(repo);
		this.configuration = checkNotNull(configuration);
		this.authenticationService = checkNotNull(authenticationService);
		this.groupRepository = checkNotNull(groupRepository);
		sessionCacheBySessionId = cacheService.newCache("session_cache");
		executorService = Executors.newSingleThreadExecutor(); //single thread executors: guarantees write order, and limit load on db; WARNING: this is not scalable on a single node (but will be on a cluster with session driven balancing);
		executorService.submit(() -> {
			requestContextService.initCurrentRequestContext("session repository");
		});
	}

	@PreDestroy
	public void cleanup() {
		logger.info("cleanup");
		shutdownQuietly(executorService);
	}

	/**
	 * evict expired sessions from db
	 *
	 * scheduled to run once every hour
	 */
	@ScheduledJob("0 0 * * * ?")
	public void cleanupSessionStore() {
		logger.debug("delete expired sessions from session store");
		deleteExpiredSessions(Period.seconds(configuration.getSessionTimeoutOrDefault() + configuration.getSessionPersistDelay()));
	}

	@Override
	public int getActiveSessionCount() {
		return repo.getActiveSessionCount(Period.seconds(configuration.getSessionActivePeriodForStatistics()));
	}

	public @Nullable
	@Override
	Session getSessionByIdOrNull(String sessionId) {
		logger.trace("findSessionBySessionId, sessionId = {}", sessionId);
		Session session = sessionCacheBySessionId.getIfPresent(sessionId);
		if (session != null) {
			logger.trace("got session from cache, session = {}", session);
			if (!session.isValid()) {
				session = null;
			}
		} else {
			SessionData data = repo.getSessionDataByIdOrNull(sessionId);
			if (data == null) {
				logger.debug("session not found in db for sessionId = {}, return null", sessionId);
				sessionCacheBySessionId.put(sessionId, invalidSession());// cache invalid session token
				return null;
			} else {
				try {
					logger.debug("found session for id = {}, deserializing", sessionId);
//					String sessionDataStr = (String) card.get(DATA_ATTRIBUTE);
//					logger.trace("deserializing json session data = {}", sessionDataStr);
					SessionDataJsonBean sessionData = data.getDataAsBean();
//					checkArgument(sessionData.getVersion() == SessionDataJsonBean.BEAN_VERSION_ID, "unsupported json session data version = %s, current is = %s (json session data from db is not compatible with current java code)", sessionData.getVersion(), SessionDataJsonBean.BEAN_VERSION_ID);
					OperationUserStack operationUser = new OperationUserStackImpl(sessionData.getOperationUserStack().stream().map((operationUserData)
							-> {

						LoginUser authenticatedUser = authenticationService.getUserByUsernameOrNull(operationUserData.getAuthenticatedUser().getUsername());

						Map<String, Role> groupMap = groupRepository.getGroupsByName(newTreeSet(filter(concat(operationUserData.getPrivilegeContext().getGroups(), singleton(operationUserData.getGroup())), not(isNull()))));

						List<Role> privilegeContextGroups = newArrayList(filterKeys(groupMap, in(operationUserData.getPrivilegeContext().getGroups())).values());
						Role defaultGroup = operationUserData.getGroup() == null ? null : groupRepository.getGroupWithName(operationUserData.getGroup());

						return OperationUserImpl.builder()
								.withAuthenticatedUser(authenticatedUser)
								.withPrivilegeContext(UserPrivilegesImpl.builder().withGroups(privilegeContextGroups).build())
								.withDefaultGroup(defaultGroup)
								.withUserTenantContext(new UserTenantContextImpl(operationUserData.getUserTenantContext().getIgnoreTenantPolicies(),
										operationUserData.getUserTenantContext().getActiveTenatIds(),
										operationUserData.getUserTenantContext().getDefaultTenantId()))
								.build();

					}).collect(toList()));
					logger.debug("deserialized session data, returning session for user = {}", operationUser);
					session = SessionImpl.builder()
							.withSessionId(data.getSessionId())
							.withBeginDate(data.getBeginDate())
							.withLastActiveDate(toDateTime(data.getLastActiveDate()))
							.withOperationUser(operationUser)
							.withSessionData(sessionData.getSessionData())
							.build();
					sessionCacheBySessionId.put(sessionId, session); // put session instance in cache
				} catch (Exception ex) {
					logger.warn("error loading session from db for sessionId = " + sessionId, ex);
					return null; // we will not cache an invalid session instance in case of loading error, so that the error can be fixed and later the session loaded successfully
				}
			}
		}

		if (session != null && ChronoUnit.SECONDS.between(session.getLastActiveDate(), DateUtils.now()) > configuration.getSessionTimeoutOrDefault()) {
			logger.debug("session {} is expired, return null", session);
			// we could clean up expired session here, but it's not really necessary... it's already in cache, so performance-wise there is non a real gain
			return null;
		} else {
			return session;
		}
	}

//	private @Nullable
//	CMCard findSessionCardBySessionId(String sessionId) {
//		Classe sessionClass = getSessionClass();
//		QueryResult result = dataView.select(anyAttribute(sessionClass)).from(sessionClass).where(condition(attribute(sessionClass, SESSION_ID_ATTRIBUTE), eq(sessionId))).run();
//		return result.isEmpty() ? null : result.getOnlyRow().getCard(sessionClass);
//	}
	@Override
	public Session updateSession(Session session) {
		logger.trace("update session  = {}", session);
		boolean requireUpdate = session.isNew() || session.isDirty() || (ChronoUnit.SECONDS.between(session.getLastSavedDate(), session.getLastActiveDate()) > configuration.getSessionPersistDelay());
		if (requireUpdate) {
			session = SessionImpl.copyOf(session).withLastSaveDate(now()).build();
			updateSessionOnDbAsync(session);
		} else {
			logger.trace("update on db is not required for session = {}", session);
		}
		sessionCacheBySessionId.put(session.getSessionId(), session);  // update cached session instance
		return session;
	}

	@Override
	public void deleteExpiredSessions(Period expireTime) {
		repo.deleteExpiredSessions(expireTime);
	}

	@Override
	public void deleteSession(String sessionId) {
		repo.deleteSession(sessionId);
		sessionCacheBySessionId.put(sessionId, invalidSession());
	}

	@Override
	public void deleteAll() {
		logger.info("delete all sessions");
		for (String sessionId : copyOf(sessionCacheBySessionId.asMap().keySet())) {
			sessionCacheBySessionId.put(sessionId, invalidSession());//invalidate all session in cache
		}
		repo.deleteAll();
	}

	/**
	 *
	 * This db update will be processed in a separate background thread for
	 * better performance.
	 *
	 * @param session
	 * @param session
	 */
	private void updateSessionOnDbAsync(Session session) {
		logger.debug("will update this session on db, on background thread; session = {}", session);
		Session copyOfUpdatedSession = SessionImpl.copyOf(session).build(); //defensive copy, for store on db in background thread (since the returned instance is mutable)
		executorService.submit(() -> {
			try {
				doUpdateSessionOnDb(copyOfUpdatedSession);
			} catch (Exception ex) {
				logger.error("error persisting session on db, session = {}", copyOfUpdatedSession);
				logger.error("error persisting session on db", ex);
			}
		});
	}

	/**
	 * write session on db (update or create session card)
	 *
	 * @param session
	 */
	private void doUpdateSessionOnDb(Session session) {
		logger.debug("doUpdateSessionOnDb, session = {}", session);
		SessionDataJsonBean sessionData = new SessionDataJsonBean();
		sessionData.setOperationUserStack(session.getOperationUser().getOperationUserStack().stream().map((operationUser) -> {
			UserTenantContext userTenantContext = operationUser.getUserTenantContext();
			UserTenantContextJsonBean tenantBean = new UserTenantContextJsonBean();
			tenantBean.setIgnoreTenantPolicies(userTenantContext.ignoreTenantPolicies());
			tenantBean.setActiveTenatIds(copyOf(userTenantContext.getActiveTenantIds()));
			tenantBean.setDefaultTenantId(userTenantContext.getDefaultTenantId());
			OperationUserJsonBean bean = new OperationUserJsonBean();
			bean.setGroup(operationUser.hasDefaultGroup() ? operationUser.getDefaultGroup().getName() : null);
			bean.setAuthenticatedUser(new AuthenticatedUserJsonBean("regular", operationUser.getLoginUser().getUsername()));
			bean.setUserTenantContext(tenantBean);
			bean.setPrivilegeContext(new SessionDataJsonBean.PrivilegeContextJsonBean(copyOf(operationUser.getPrivilegeContext().getSourceGroups())));
			return bean;
		}).collect(toList()));
		sessionData.setSessionData(new TreeMap<>(session.getSessionData()));
//		String sessionDataString = gson.toJson(sessionData);
//		logger.trace("session json data = {}", sessionDataString);

		repo.createOrUpdateSession(session.getSessionId(), sessionData);

	}

}
