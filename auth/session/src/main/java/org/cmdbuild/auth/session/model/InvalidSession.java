/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.model;

import java.time.ZonedDateTime;
import java.util.Map;
import org.cmdbuild.auth.user.OperationUserStack;

/**
 * represents an invalid session singleton instance; to be used to cache
 * 'session not found' or 'session invalid' results in caches
 */
public class InvalidSession implements Session {

	private static final InvalidSession INSTANCE = new InvalidSession();

	public static InvalidSession invalidSession() {
		return INSTANCE;
	}

	private InvalidSession() {
	}

	@Override
	public boolean isValid() {
		return false;
	}

	@Override
	public OperationUserStack getOperationUser() {
		throw newException();

	}

	@Override
	public String getSessionId() {
		throw newException();
	}

	@Override
	public ZonedDateTime getBeginDate() {
		throw newException();
	}

	@Override
	public void setLastActiveDate(ZonedDateTime now) {
		throw newException();
	}

	@Override
	public ZonedDateTime getLastActiveDate() {
		throw newException();
	}

	@Override
	public ZonedDateTime getLastSavedDate() {
		throw newException();
	}

	@Override
	public boolean isDirty() {
		throw newException();
	}

	@Override
	public Map<String, Object> getSessionData() {
		throw newException();
	}

	@Override
	public void put(String key, Object value) {
		throw newException();
	}

	private RuntimeException newException() {
		return new UnsupportedOperationException("invalid session: operation not supported!");
	}

}
