/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.dao;

import static com.google.common.base.Preconditions.checkArgument;
import java.time.ZonedDateTime;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.gson.Gson;
import javax.annotation.Nullable;
import org.cmdbuild.auth.session.dao.beans.SessionDataJsonBean;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@CardMapping("_Session")
public class SessionDataImpl implements SessionData {

	private final Long id;
	private final String sessionId, data;
	private final ZonedDateTime beginDate, lastActiveDate;
	private final SessionDataJsonBean dataBean;

	private SessionDataImpl(SessionDataImplBuilder builder) {
		this.id = builder.id;
		this.sessionId = checkNotBlank(builder.sessionId);
		this.data = checkNotBlank(builder.data);
		this.beginDate = checkNotNull(builder.beginDate);
		this.lastActiveDate = checkNotNull(builder.lastActiveDate);
		dataBean = new Gson().fromJson(data, SessionDataJsonBean.class);
		checkArgument(dataBean.getVersion() == SessionDataJsonBean.BEAN_VERSION_ID, "unsupported json session data version = %s, current is = %s (json session data from db is not compatible with current java code)", dataBean.getVersion(), SessionDataJsonBean.BEAN_VERSION_ID);
	}

	@Override
	@CardAttr(ATTR_ID)
	public @Nullable
	Long getId() {
		return id;
	}

	@Override
	@CardAttr
	public String getSessionId() {
		return sessionId;
	}

	@Override
	@CardAttr
	public String getData() {
		return data;
	}

	@Override
	@CardAttr
	public ZonedDateTime getBeginDate() {
		return beginDate;
	}

	@Override
	@CardAttr
	public ZonedDateTime getLastActiveDate() {
		return lastActiveDate;
	}

	@Override
	public SessionDataJsonBean getDataAsBean() {
		return dataBean;
	}

	public static SessionDataImplBuilder builder() {
		return new SessionDataImplBuilder();
	}

	public static SessionDataImplBuilder copyOf(SessionData source) {
		return new SessionDataImplBuilder()
				.withId(source.getId())
				.withSessionId(source.getSessionId())
				.withData(source.getData())
				.withBeginDate(source.getBeginDate())
				.withLastActiveDate(source.getLastActiveDate());
	}

	public static class SessionDataImplBuilder implements Builder<SessionDataImpl, SessionDataImplBuilder> {

		private Long id;
		private String sessionId;
		private String data;
		private ZonedDateTime beginDate;
		private ZonedDateTime lastActiveDate;

		public SessionDataImplBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public SessionDataImplBuilder withSessionId(String sessionId) {
			this.sessionId = sessionId;
			return this;
		}

		public SessionDataImplBuilder withData(String data) {
			this.data = data;
			return this;
		}

		public SessionDataImplBuilder withDataBean(SessionDataJsonBean data) {
			return this.withData(new Gson().toJson(checkNotNull(data)));
		}

		public SessionDataImplBuilder withBeginDate(ZonedDateTime beginDate) {
			this.beginDate = beginDate;
			return this;
		}

		public SessionDataImplBuilder withLastActiveDate(ZonedDateTime lastActiveDate) {
			this.lastActiveDate = lastActiveDate;
			return this;
		}

		@Override
		public SessionDataImpl build() {
			return new SessionDataImpl(this);
		}

	}
}
