/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session;

import org.cmdbuild.auth.session.inner.CurrentSessionHolder;
import org.cmdbuild.auth.session.inner.SessionDataService;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.eventbus.Subscribe;
import javax.annotation.Nullable;
import org.cmdbuild.audit.RequestEventService;
import org.cmdbuild.audit.RequestEventService.RequestCompleteEvent;
import org.cmdbuild.auth.user.OperationUserStore;
import org.cmdbuild.auth.user.OperationUser;
import static org.cmdbuild.auth.user.OperationUserImpl.anonymousOperationUser;
import org.cmdbuild.auth.user.OperationUserStack;
import org.cmdbuild.auth.login.LoginDataImpl;
import org.springframework.stereotype.Component;
import static org.cmdbuild.auth.session.model.SessionImpl.builder;
import static org.cmdbuild.auth.session.model.SessionImpl.copyOf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmdbuild.auth.session.model.Session;
import org.cmdbuild.auth.session.model.SessionData;
import org.cmdbuild.utils.date.DateUtils;
import org.cmdbuild.auth.login.LoginData;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.DEFAULT_RANDOM_ID_SIZE;
import static org.cmdbuild.utils.random.CmdbuildRandomUtils.randomId;
import org.cmdbuild.auth.session.dao.SessionRepository;
import org.cmdbuild.auth.login.AuthenticationService;
import static org.cmdbuild.auth.role.RolePrivilege.RP_IMPERSONATE_ALL;

/**
 * handle sessions. All storage and caching is delegated to session store.
 */
@Component
public class SessionServiceImpl implements SessionService {

	public static final int SESSION_TOKEN_SIZE = DEFAULT_RANDOM_ID_SIZE;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final CurrentSessionHolder currentSessionIdHolder;
	private final SessionRepository sessionStore;
	private final OperationUserStore userStore;//TODO remove this, use static/threadlocal binding
	private final AuthenticationService authenticationService;
	private final SessionDataService sessionDataService;

	public SessionServiceImpl(CurrentSessionHolder currentSessionHolder, CurrentSessionHolder currentSessionIdHolder, SessionRepository sessionStore, OperationUserStore userStore, AuthenticationService authenticationLogic, RequestEventService requestEventService, SessionDataService sessionDataService) {
		this.sessionStore = checkNotNull(sessionStore);
		this.userStore = checkNotNull(userStore);
		this.authenticationService = checkNotNull(authenticationLogic);
		this.sessionDataService = checkNotNull(sessionDataService);
		this.currentSessionIdHolder = checkNotNull(currentSessionIdHolder);
		requestEventService.getEventBus().register(new Object() {
			@Subscribe
			public void handleRequestCompleteEvent(RequestCompleteEvent event) {
				Session session = getCurrentSessionOrNull();
				if (session != null) {
					updateSession(session);
				}
			}
		});

	}

	@Override
	public void validateSessionId(String sessionId) {
		getSessionById(sessionId);//TODO replace with more efficent query
	}

	@Override
	public Session getSessionById(String sessionId) {
		return checkNotNull(getSessionByIdOrNull(sessionId), "cannot find session for id = %s", sessionId);
	}

	@Override
	public @Nullable
	Session getSessionByIdOrNull(String sessionId) {
		return sessionStore.getSessionByIdOrNull(sessionId);
	}

	@Override
	public int getActiveSessionCount() {
		return sessionStore.getActiveSessionCount();
	}

	//old session logic compatibility stuff BEGIN 
	@Override
	public String create(LoginData login) {
		OperationUser user = authenticationService.validateCredentialsAndCreateOperationUser(login);
		String sessionToken = createSessionToken();
		sessionStore.updateSession(builder().withSessionId(sessionToken).withOperationUser(user).build());
		return sessionToken;
	}

	private String createSessionToken() {
		return randomId(SESSION_TOKEN_SIZE);
	}

	@Override
	public boolean exists(String id) {
		try {
			getSessionById(id); //TODO more efficient query; do not rely on exception for control flow
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void update(String sessionId, LoginData loginBean) {
		Session session = getSessionById(sessionId);
		OperationUser user = authenticationService.updateOperationUser(loginBean, session.getOperationUser());
		sessionStore.updateSession(builder().withSessionId(sessionId).withOperationUser(user).build());
	}

	@Override
	public void deleteSession(String sessionId) {
		sessionStore.deleteSession(sessionId);
		//TODO release all session lock (use event trigger)
	}

	@Override
	public void deleteAll() {
		sessionStore.deleteAll();
	}

	@Override
	public void impersonate(String sessionId, @Nullable String username) {
		Session session = getSessionById(sessionId);
		OperationUserStack operationUserStack = session.getOperationUser();
		if (username != null) {
			checkArgument(!canImpersonateOtherUsers(operationUserStack.getRootOperationUser()), "cannot impersonate from current user = %s", operationUserStack);
			OperationUser inner = authenticationService.validateCredentialsAndCreateOperationUser(LoginDataImpl.builder().withLoginString(username).withNoPasswordRequired().build());
			sessionStore.updateSession(copyOf(session).impersonate(inner).build());
		} else {
			sessionStore.updateSession(copyOf(session).deImpersonateAll().build());
		}
	}

	private boolean canImpersonateOtherUsers(OperationUser user) {
		return user.hasPrivileges(RP_IMPERSONATE_ALL);//TODO check this
	}

	@Override
	public @Nullable String getCurrentSessionIdOrNull() {
		return currentSessionIdHolder.getOrNull();
	}

	@Override
	public void setCurrent(String id) {
		currentSessionIdHolder.set(id);
		userStore.setUser((id == null) ? null : getUserOrAnonymousWhenMissing(id));
	}

	@Override
	public Session getCurrentSession() {
		return getSessionById(checkNotNull(getCurrentSessionIdOrNull(), "current session not set"));
	}

	@Override
	public @Nullable
	Session getCurrentSessionOrNull() {
		String sessionId = getCurrentSessionIdOrNull();
		return sessionId == null ? null : getSessionByIdOrNull(sessionId);
	}

	private OperationUser getUserOrAnonymousWhenMissing(String sessionId) {
		Session session = getSessionByIdOrNull(sessionId);
		return session == null ? anonymousOperationUser() : session.getOperationUser();
	}

	@Override
	public boolean sessionExistsAndHasDefaultGroup(String sessionId) {
		return (sessionId == null) ? false : getUserOrAnonymousWhenMissing(sessionId).hasDefaultGroup();
	}

	@Override
	public OperationUser getUser(String sessionId) {
		return getSessionById(sessionId).getOperationUser();
	}

	@Override
	public void setUser(String sessionId, OperationUser user) {
		sessionStore.updateSession(copyOf(getSessionById(sessionId)).impersonate(user).build());
	}

	@Override
	public void updateSession(Session session) {
		logger.debug("update session {}", session);
		session.setLastActiveDate(DateUtils.now());
		sessionStore.updateSession(session);
	}

	@Override
	public SessionData getCurrentSessionDataSafe() {
		return sessionDataService.getCurrentSessionDataSafe();
	}

}
