/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.dao.beans;

import java.util.List;
import java.util.Map;

/**
 *
 */
public class SessionDataJsonBean {

	public final static int BEAN_VERSION_ID = 3;

	private int version = BEAN_VERSION_ID;
	private List<OperationUserJsonBean> operationUserStack;
	private Map<String, Object> sessionData;

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<OperationUserJsonBean> getOperationUserStack() {
		return operationUserStack;
	}

	public void setOperationUserStack(List<OperationUserJsonBean> operationUserStack) {
		this.operationUserStack = operationUserStack;
	}

	public Map<String, Object> getSessionData() {
		return sessionData;
	}

	public void setSessionData(Map<String, Object> sessionData) {
		this.sessionData = sessionData;
	}

	public static class OperationUserJsonBean {

		private PrivilegeContextJsonBean privilegeContext;
		private String group;
		private AuthenticatedUserJsonBean authenticatedUser;
		private UserTenantContextJsonBean userTenantContext;

		public PrivilegeContextJsonBean getPrivilegeContext() {
			return privilegeContext;
		}

		public void setPrivilegeContext(PrivilegeContextJsonBean privilegeContext) {
			this.privilegeContext = privilegeContext;
		}

		public String getGroup() {
			return group;
		}

		public void setGroup(String group) {
			this.group = group;
		}

		public AuthenticatedUserJsonBean getAuthenticatedUser() {
			return authenticatedUser;
		}

		public void setAuthenticatedUser(AuthenticatedUserJsonBean authenticatedUser) {
			this.authenticatedUser = authenticatedUser;
		}

		public UserTenantContextJsonBean getUserTenantContext() {
			return userTenantContext;
		}

		public void setUserTenantContext(UserTenantContextJsonBean userTenantContext) {
			this.userTenantContext = userTenantContext;
		}

	}

	/**
	 * privileges are build from active groups
	 */
	public static class PrivilegeContextJsonBean {

		private List<String> groups;

		public PrivilegeContextJsonBean() {
		}

		public PrivilegeContextJsonBean(List<String> groups) {
			this.groups = groups;
		}

		public List<String> getGroups() {
			return groups;
		}

		public void setGroups(List<String> groups) {
			this.groups = groups;
		}

	}

	public static class AuthenticatedUserJsonBean {

		private String userType, username;

		public AuthenticatedUserJsonBean() {
		}

		public AuthenticatedUserJsonBean(String userType, String username) {
			this.userType = userType;
			this.username = username;
		}

		public String getUserType() {
			return userType;
		}

		public void setUserType(String userType) {
			this.userType = userType;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

	}

	public static class UserTenantContextJsonBean {

		private List<Long> activeTenatIds;
		private Long defaultTenantId;
		private boolean ignoreTenantPolicies;

		public List<Long> getActiveTenatIds() {
			return activeTenatIds;
		}

		public void setActiveTenatIds(List<Long> activeTenatIds) {
			this.activeTenatIds = activeTenatIds;
		}

		public Long getDefaultTenantId() {
			return defaultTenantId;
		}

		public void setDefaultTenantId(Long defaultTenantId) {
			this.defaultTenantId = defaultTenantId;
		}

		public boolean getIgnoreTenantPolicies() {
			return ignoreTenantPolicies;
		}

		public void setIgnoreTenantPolicies(boolean ignoreTenantPolicies) {
			this.ignoreTenantPolicies = ignoreTenantPolicies;
		}

	}
}
