/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.model;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Map;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.apache.commons.lang3.builder.Builder;
import org.cmdbuild.auth.user.OperationUser;
import org.cmdbuild.auth.user.OperationUserStack;
import org.cmdbuild.auth.user.OperationUserStackImpl;
import static org.cmdbuild.auth.user.OperationUserStackImpl.toSimpleOperationUser;
import static org.cmdbuild.utils.date.DateUtils.now;
import org.joda.time.DateTime;

/**
 * a session object. Most of this object data is immutable.
 *
 * Mutable fields are:<ul>
 * <li>lastActiveDate: initially equal to lastSavedDate, may be incremented if
 * the session is activated-modified and not yet saved</li>
 * <li>sessionData: session data is a mutable map of session data</li>
 * <li>dirty: a dirty flag is set to true if session data is modified</li>
 * </ul>
 *
 * all mutable fields are re-set in case of session save/persist (session data
 * is saved, other fields are re-set to their default values).
 *
 * all the other fields (operation user, etc) require a copy of the whole
 * session object to be modified.
 *
 * This session object is supposed to be created (or restored from persistence),
 * then accumulate session data and updates for a while, and then be persisted
 * again on storage.
 */
public class SessionImpl implements Session {

	private final OperationUserStack operationUserStack;
	private final String sessionId;
	private final ZonedDateTime beginDate, lastSavedDate;
	private ZonedDateTime lastActiveDate;
	private final Map<String, Object> sessionData = Maps.newHashMap(), immutableSessionData = Collections.unmodifiableMap(sessionData);
	private boolean dirty = false;

	private SessionImpl(OperationUserStack operationUserStack,
			String sessionId,
			ZonedDateTime beginDate,
			ZonedDateTime lastActiveDate,
			@Nullable ZonedDateTime lastSavedDate,
			Map<String, Object> sessionData) {
		this.operationUserStack = checkNotNull(operationUserStack);
		this.sessionId = checkNotNull(sessionId);
		this.beginDate = checkNotNull(beginDate);
		this.lastActiveDate = checkNotNull(lastActiveDate);
		this.lastSavedDate = lastSavedDate;
		this.sessionData.putAll(sessionData);
	}

	@Override
	public OperationUserStack getOperationUser() {
		return operationUserStack;
	}

	@Override
	public String getSessionId() {
		return sessionId;
	}

	@Override
	public ZonedDateTime getBeginDate() {
		return beginDate;
	}

	@Override
	public void setLastActiveDate(ZonedDateTime lastActiveDate) {
		this.lastActiveDate = lastActiveDate;
	}

	@Override
	public ZonedDateTime getLastActiveDate() {
		return lastActiveDate;
	}

	public static NewSessionBuilder builder() {
		return new NewSessionBuilder();
	}

	public static NewSessionBuilder copyOf(Session session) {
		return new NewSessionBuilder()
				.withBeginDate(session.getBeginDate())
				.withLastActiveDate(session.getLastActiveDate())
				.withLastSaveDate(session.getLastSavedDate())
				.withOperationUser(session.getOperationUser())
				.withSessionId(session.getSessionId())
				.withSessionData(session.getSessionData());
	}

	@Override
	public Map<String, Object> getSessionData() {
		return immutableSessionData;
	}

	@Override
	public void put(String key, @Nullable Object value) {
		if (!equal(get(key), value)) {
			lastActiveDate = now();
			dirty = true;
			if (value == null) {
				sessionData.remove(key);
			} else {
				sessionData.put(key, value);
			}
		}
	}

	@Override
	public @Nullable
	ZonedDateTime getLastSavedDate() {
		return lastSavedDate;
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	public static class NewSessionBuilder implements Builder<Session> {

		private OperationUserStack operationUserStack;
		private String sessionId;
		private ZonedDateTime beginDate, lastActiveDate, lastSaveDate;
		private final Map<String, Object> sessionData = Maps.newHashMap();

		private NewSessionBuilder() {
			beginDate = lastActiveDate = now();
		}

		public NewSessionBuilder withOperationUser(OperationUserStack operationUserStack) {
			this.operationUserStack = checkNotNull(operationUserStack);
			return this;
		}

		public NewSessionBuilder withOperationUser(OperationUser operationUser) {
			return this.withOperationUser(OperationUserStackImpl.wrapOrCast(operationUser));
		}

		public NewSessionBuilder withSessionId(String sessionId) {
			this.sessionId = checkNotNull(trimToNull(sessionId));
			return this;
		}

		public NewSessionBuilder withSessionData(Map<String, Object> sessionData) {
			this.sessionData.putAll(checkNotNull(sessionData));
			return this;
		}

		public NewSessionBuilder withBeginDate(ZonedDateTime beginDate) {
			this.beginDate = checkNotNull(beginDate);
			return this;
		}

		public NewSessionBuilder withLastActiveDate(ZonedDateTime lastActiveDate) {
			this.lastActiveDate = checkNotNull(lastActiveDate);
			return this;
		}

		public NewSessionBuilder withLastSaveDate(@Nullable ZonedDateTime lastSaveDate) {
			this.lastSaveDate = lastSaveDate;
			return this;
		}

		public NewSessionBuilder impersonate(OperationUser operationUser) {
			return this.withOperationUser(new OperationUserStackImpl(ImmutableList.<OperationUser>builder().addAll(this.operationUserStack.getOperationUserStack()).add(toSimpleOperationUser(operationUser)).build()));
		}

		public NewSessionBuilder deImpersonate() {
			return this.withOperationUser(new OperationUserStackImpl(ImmutableList.<OperationUser>builder().addAll(Iterables.limit(this.operationUserStack.getOperationUserStack(), this.operationUserStack.getOperationUserStackSize() - 1)).build()));
		}

		public NewSessionBuilder deImpersonateAll() {
			return this.withOperationUser(new OperationUserStackImpl(this.operationUserStack.getRootOperationUser()));
		}

		@Override
		public Session build() {
			return new SessionImpl(operationUserStack, sessionId, beginDate, lastActiveDate, lastSaveDate, sessionData);
		}

	}

	@Override
	public String toString() {
		return "Session{" + "user=" + operationUserStack.getLoginUser().getUsername() + ", sessionId=" + sessionId + ", beginDate=" + beginDate + ", lastActiveDate=" + lastActiveDate + '}';
	}

}
