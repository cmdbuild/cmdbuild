/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.dao;

import java.time.ZonedDateTime;
import javax.annotation.Nullable;
import org.cmdbuild.auth.session.dao.beans.SessionDataJsonBean;

public interface SessionData {

	@Nullable
	Long getId();

	String getSessionId();

	String getData();

	ZonedDateTime getBeginDate();

	ZonedDateTime getLastActiveDate();

	SessionDataJsonBean getDataAsBean();

}
