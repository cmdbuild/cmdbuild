/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.dao;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.cmdbuild.auth.session.dao.SessionRepositoryImpl.SESSION_CLASS_NAME;
import static org.cmdbuild.auth.session.dao.SessionRepositoryImpl.SESSION_ID_ATTRIBUTE;
import org.cmdbuild.auth.session.dao.beans.SessionDataJsonBean;
import static org.cmdbuild.utils.date.DateUtils.now;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.WhereOperator.EQ;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@Component
public class SessionDataRepositoryImpl implements SessionDataRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DaoService dao;

	public SessionDataRepositoryImpl(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@Override
	public @Nullable
	SessionData getSessionDataByIdOrNull(String sessionId) {
		return dao.selectAll().from(SessionData.class).where(SESSION_ID_ATTRIBUTE, EQ, checkNotBlank(sessionId)).getOneOrNull();
	}

	@Override
	public void deleteSession(String sessionId) {
		dao.getJdbcTemplate().update("DELETE FROM \"" + SESSION_CLASS_NAME + "\" WHERE \"" + SESSION_ID_ATTRIBUTE + "\" = ?", checkNotNull(trimToNull(sessionId)));
	}

	@Override
	public void deleteAll() {
		dao.getJdbcTemplate().update("DELETE FROM \"" + SESSION_CLASS_NAME + "\" ");
	}

	@Override
	public int getActiveSessionCount(Period activePeriod) {
		return dao.getJdbcTemplate().queryForObject(format("SELECT COUNT(*) _count FROM \"_Session\" WHERE \"LastActiveDate\" > NOW() - '%s seconds'::interval", activePeriod.getSeconds()), Integer.class);
	}

	@Override
	public SessionData createOrUpdateSession(String sessionId, SessionDataJsonBean sessionData) {
		SessionData current = getSessionDataByIdOrNull(sessionId);
		if (current == null) {
			logger.debug("create session = {}", sessionId);
			return dao.create(SessionDataImpl.builder()
					.withBeginDate(now())
					.withSessionId(sessionId)
					.withDataBean(sessionData)
					.withLastActiveDate(now())
					.build());
		} else {
			logger.debug("update session = {}", current);
			return dao.update(SessionDataImpl.copyOf(current)
					.withDataBean(sessionData)
					.withLastActiveDate(now())
					.build());
		}
	}

	@Override
	public void deleteExpiredSessions(Period expireTime) {
		logger.debug("deleting expired sessions (older than {})", expireTime);
		// cleanup in cache is handled by cache expiration and lazy invalidation check on read
		int res = dao.getJdbcTemplate().update(format("DELETE FROM \"_Session\" WHERE \"LastActiveDate\" < NOW() - '%s seconds'::interval", expireTime.getSeconds()));
		if (res > 0) {
			logger.info("deleteted {} expired sessions (older than {})", res, expireTime);
		}
	}

}
