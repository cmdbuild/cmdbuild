/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.auth.session.dao;

import javax.annotation.Nullable;
import org.joda.time.Period;
import org.cmdbuild.auth.session.model.Session;

/**
 *
 */
public interface SessionRepository {

	/**
	 * find session by session id (return null if not found)
	 *
	 * @param sessionId
	 * @return session object
	 */
	@Nullable
	Session getSessionByIdOrNull(String sessionId);

	/**
	 * update session data on cache/db (actual db update may be deferred or
	 * skipped if session is not dirty and has been saved on db recently, see
	 * {@link CmdbuildConfiguration#getSessionPersistDelay()})
	 *
	 * @param session
	 * @return new (updated) session object (to be stored in cache etc)
	 */
	Session updateSession(Session session);

	/**
	 * delete expired sessions (ie all sessions with last active date older than
	 * expireTime)
	 * <br><br>
	 * warning: if session cache does not flush immediately on database, this
	 * session data may be slightly outdated, so expireTime should not be equal
	 * to actual session expire time, but more like sessionExpireTime +
	 * maxDelayFromCacheUpdate, or something like that. <br>
	 * Actually expired sessions may also be removed after a longer time, even
	 * days. Old session removal is required only for performance reasons.
	 *
	 * @param expireTime max inactive session time before eviction
	 */
	void deleteExpiredSessions(Period expireTime);

	/**
	 * delete session by session id
	 *
	 * @param sessionId
	 */
	void deleteSession(String sessionId);

	/**
	 * delete all sessions
	 */
	void deleteAll();
	
	int getActiveSessionCount();
}
