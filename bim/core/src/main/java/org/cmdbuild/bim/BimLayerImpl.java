/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@CardMapping("_BimLayer")
public class BimLayerImpl implements BimLayer {

	private final Long id;
	private final String ownerClassId;
	private final boolean isRoot, isActive;
	private final String rootReference;

	private BimLayerImpl(BimLayerImplBuilder builder) {
		this.id = builder.id;
		this.ownerClassId = checkNotBlank(builder.ownerClassId);
		this.isRoot = builder.isRoot;
		this.isActive = builder.isActive;
		this.rootReference = isRoot ? null : trimToNull(builder.rootReference);
	}

	@CardAttr(ATTR_ID)
	public @Nullable
	@Override
	Long getId() {
		return id;
	}

	@Override
	@CardAttr
	public String getOwnerClassId() {
		return ownerClassId;
	}

	@Override
	@CardAttr
	public boolean isRoot() {
		return isRoot;
	}

	@Override
	@CardAttr
	public boolean isActive() {
		return isActive;
	}

	@CardAttr
	@Override
	public @Nullable
	String getRootReference() {
		return rootReference;
	}

	public static BimLayerImplBuilder builder() {
		return new BimLayerImplBuilder();
	}

	public static BimLayerImplBuilder copyOf(BimLayerImpl source) {
		return new BimLayerImplBuilder()
				.withId(source.getId())
				.withOwnerClassId(source.getOwnerClassId())
				.withRoot(source.isRoot())
				.withActive(source.isActive())
				.withRootReference(source.getRootReference());
	}

	public static class BimLayerImplBuilder implements Builder<BimLayerImpl, BimLayerImplBuilder> {

		private Long id;
		private String ownerClassId;
		private Boolean isRoot;
		private Boolean isActive;
		private String rootReference;

		public BimLayerImplBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public BimLayerImplBuilder withOwnerClassId(String ownerClassId) {
			this.ownerClassId = ownerClassId;
			return this;
		}

		public BimLayerImplBuilder withRoot(Boolean isRoot) {
			this.isRoot = isRoot;
			return this;
		}

		public BimLayerImplBuilder withActive(Boolean isActive) {
			this.isActive = isActive;
			return this;
		}

		public BimLayerImplBuilder withRootReference(String rootReference) {
			this.rootReference = rootReference;
			return this;
		}

		@Override
		public BimLayerImpl build() {
			return new BimLayerImpl(this);
		}

	}
}
