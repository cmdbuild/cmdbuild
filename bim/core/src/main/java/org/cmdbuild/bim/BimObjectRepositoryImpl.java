/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nullable;
import static org.cmdbuild.bim.BimObjectImpl.BIM_OBJECT_ATTR_OWNER_CARD_ID;
import static org.cmdbuild.bim.BimObjectImpl.BIM_OBJECT_ATTR_OWNER_CLASS_ID;
import org.cmdbuild.dao.beans.CardIdAndClassName;
import org.cmdbuild.dao.core.q3.DaoService;
import static org.cmdbuild.dao.core.q3.QueryBuilder.EQ;
import static org.cmdbuild.dao.core.q3.WhereOperator.ISNULL;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class BimObjectRepositoryImpl implements BimObjectRepository {

	private final DaoService dao;

	public BimObjectRepositoryImpl(DaoService dao) {
		this.dao = checkNotNull(dao);
	}

	@Override
	public @Nullable
	BimObject getBimObjectForCardOrNull(CardIdAndClassName card) {
		return dao.selectAll().from(BimObject.class)
				.where(BIM_OBJECT_ATTR_OWNER_CLASS_ID, EQ, card.getClassName())
				.where(BIM_OBJECT_ATTR_OWNER_CARD_ID, EQ, card.getId())
				.getOneOrNull();
	}

	@Override
	public BimObject getBimObjectForProject(BimProject bimProject) {
		return dao.selectAll().from(BimObject.class)
				.where("ProjectId", EQ, bimProject.getPoid())
				.where("GlobalId", ISNULL)
				.getOne();
	}

}
