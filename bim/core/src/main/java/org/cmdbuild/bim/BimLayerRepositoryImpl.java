/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.MoreCollectors.toOptional;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.Nullable;
import org.cmdbuild.cache.CacheService;
import org.cmdbuild.cache.CmdbCache;
import org.cmdbuild.cache.Holder;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.entrytype.Classe;
import org.springframework.stereotype.Component;

@Component
public class BimLayerRepositoryImpl implements BimLayerRepository {

	private final DaoService dao;
	private final Holder<Collection<BimLayer>> bimLayers;
	private final CmdbCache<String, Optional<BimLayer>> bimLayersByOwnerOid;

	public BimLayerRepositoryImpl(DaoService dao, CacheService cacheService) {
		this.dao = checkNotNull(dao);
		bimLayers = cacheService.newHolder("bim_layers");
		bimLayersByOwnerOid = cacheService.newCache("bim_layers_by_owner_oid");
	}

	@Override
	public Collection<BimLayer> getAll() {
		return bimLayers.get(this::doGetAll);
	}

	private Collection<BimLayer> doGetAll() {
		return dao.selectAll().from(BimLayer.class).asList();
	}

	@Override
	public @Nullable
	BimLayer getByOwnerOrNull(Classe classe) {
		return bimLayersByOwnerOid.get(classe.getOid().toString(), () -> Optional.ofNullable(doGetByOwnerOrNull(classe))).orElse(null);
	}

	private @Nullable
	BimLayer doGetByOwnerOrNull(Classe classe) {
		return getAll().stream().filter((l) -> equal(l.getOwnerClassId(), classe.getName())).collect(toOptional()).orElse(null);
	}

}
