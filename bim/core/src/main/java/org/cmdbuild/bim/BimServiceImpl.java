/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Collection;
import javax.activation.DataHandler;
import javax.annotation.Nullable;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.bim.bimserverclient.BimserverClientService;
import static org.cmdbuild.bim.utils.BimConstants.IFC_FORMAT_DEFAULT;
import org.cmdbuild.config.BimserverConfiguration;
import org.cmdbuild.dao.beans.CardIdAndClassName;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.services.SystemService;
import org.cmdbuild.services.SystemServiceStatus;
import static org.cmdbuild.services.SystemServiceStatus.SS_DISABLED;
import static org.cmdbuild.services.SystemServiceStatus.SS_ENABLED;
import org.springframework.stereotype.Component;

@Component
public class BimServiceImpl implements BimService, SystemService {

	private final BimserverConfiguration configuration;
	private final BimLayerRepository layerRepository;
	private final BimObjectRepository objectRepository;
	private final BimProjectRepository projectRepository;
	private final BimserverClientService bimserverClient;
	private final DaoService dao;

	public BimServiceImpl(BimserverConfiguration configuration, BimLayerRepository layerRepository, BimObjectRepository objectRepository, BimProjectRepository projectRepository, BimserverClientService bimserverClient, DaoService dao) {
		this.configuration = checkNotNull(configuration);
		this.layerRepository = checkNotNull(layerRepository);
		this.objectRepository = checkNotNull(objectRepository);
		this.projectRepository = checkNotNull(projectRepository);
		this.bimserverClient = checkNotNull(bimserverClient);
		this.dao = checkNotNull(dao);
	}

	@Override
	public String getServiceName() {
		return "BIM Service";
	}

	@Override
	public SystemServiceStatus getServiceStatus() {
		if (!isEnabled()) {
			return SS_DISABLED;
		} else {
			return SS_ENABLED;
		}
	}

	@Override
	public boolean isEnabled() {
		return configuration.isEnabled();
	}

	@Override
	public Collection<BimProject> getAllProjects() {
		return projectRepository.getAllProjects();
	}

	@Override
	public BimProject getProjectById(long id) {
		return projectRepository.getProjectById(id);
	}

	@Override
	public boolean hasBim(Classe classe) {
		return layerRepository.getByOwnerOrNull(classe) != null;
	}

	@Override
	public @Nullable
	BimObject getBimObjectForCardOrNull(CardIdAndClassName card) {
		return objectRepository.getBimObjectForCardOrNull(card);
	}

	@Override
	public BimObject getBimObjectForProject(BimProject bimProject) {
		return objectRepository.getBimObjectForProject(bimProject);
	}

	@Override
	public DataHandler downloadIfcFile(long projectId, @Nullable String ifcFormat) {
		BimProject project = projectRepository.getProjectById(projectId);
		String revisionId = bimserverClient.getLastRevisionOfProject(project.getPoid());
		return bimserverClient.downloadIfc(revisionId, defaultIfBlank(ifcFormat, IFC_FORMAT_DEFAULT));
	}

	@Override
	public void uploadIfcFile(long projectId, DataHandler dataHandler, @Nullable String ifcFormat) {
		BimProject project = projectRepository.getProjectById(projectId);
		bimserverClient.uploadIfc(project.getPoid(), dataHandler, trimToNull(ifcFormat));
	}

	@Override
	public Pair<BimProject, BimObject> getProjectAndObject(long projectId) {
		BimProject project = getProjectById(projectId);
		BimObject bimObject = getBimObjectForProject(project);
		return Pair.of(project, bimObject);
	}

}
