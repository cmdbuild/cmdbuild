/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nullable;
import org.cmdbuild.dao.beans.CardIdAndClassName;

public interface BimObjectRepository {

	@Nullable
	BimObject getBimObjectForCardOrNull(CardIdAndClassName card);

	default BimObject getBimObjectForCard(CardIdAndClassName card) {
		return checkNotNull(getBimObjectForCardOrNull(card), "bim object not found for card = %s", card);
	}
	
	BimObject getBimObjectForProject(BimProject bimProject);
}
