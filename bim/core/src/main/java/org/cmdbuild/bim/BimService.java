/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import javax.activation.DataHandler;
import javax.annotation.Nullable;
import org.apache.commons.lang3.tuple.Pair;
import org.cmdbuild.dao.entrytype.Classe;

public interface BimService extends BimObjectRepository, BimProjectRepository {

	boolean isEnabled();

	boolean hasBim(Classe classe);

	DataHandler downloadIfcFile(long projectId, @Nullable String ifcFormat);

	void uploadIfcFile(long projectId, DataHandler dataHandler, @Nullable String ifcFormat);

	Pair<BimProject, BimObject> getProjectAndObject(long projectId);

}
