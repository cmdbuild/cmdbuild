/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import javax.annotation.Nullable;

public interface BimLayer {

	@Nullable
	Long getId();

	String getOwnerClassId();

	boolean isRoot();

	boolean isActive();

	@Nullable
	String getRootReference();

}
