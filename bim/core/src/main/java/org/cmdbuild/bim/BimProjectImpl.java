/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import java.time.ZonedDateTime;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nullable;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_CODE;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_DESCRIPTION;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import org.cmdbuild.dao.orm.annotations.CardAttr;
import org.cmdbuild.dao.orm.annotations.CardMapping;
import org.cmdbuild.utils.lang.Builder;
import static org.cmdbuild.utils.lang.CmdbPreconditions.checkNotBlank;

@CardMapping("_BimProject")
public class BimProjectImpl implements BimProject {

	private final Long id;
	private final String poid, name, description, importMapping;
	private final boolean isActive;
	private final ZonedDateTime lastCheckin;

	private BimProjectImpl(BimProjectImplBuilder builder) {
		this.id = builder.id;
		this.poid = checkNotBlank(builder.poid);
		this.name = checkNotBlank(builder.name);
		this.description = builder.description;
		this.importMapping = builder.importMapping;
		this.isActive = builder.isActive;
		this.lastCheckin = builder.lastCheckin;
	}

	@Override
	@CardAttr(ATTR_ID)
	public @Nullable
	Long getId() {
		return id;
	}

	@Override
	@CardAttr("ProjectId")
	public String getPoid() {
		return poid;
	}

	@Override
	@CardAttr(ATTR_CODE)
	public String getName() {
		return name;
	}

	@Override
	@CardAttr(ATTR_DESCRIPTION)
	public @Nullable
	String getDescription() {
		return description;
	}

	@Override
	@CardAttr
	@Nullable
	public String getImportMapping() {
		return importMapping;
	}

	@Override
	@CardAttr
	public boolean isActive() {
		return isActive;
	}

	@Override
	@CardAttr
	@Nullable
	public ZonedDateTime getLastCheckin() {
		return lastCheckin;
	}

	public static BimProjectImplBuilder builder() {
		return new BimProjectImplBuilder();
	}

	public static BimProjectImplBuilder copyOf(BimProject source) {
		return new BimProjectImplBuilder()
				.withPoid(source.getPoid())
				.withName(source.getName())
				.withDescription(source.getDescription())
				.withImportMapping(source.getImportMapping())
				.withId(source.getId())
				.withActive(source.isActive())
				.withLastCheckin(source.getLastCheckin());
	}

	public static class BimProjectImplBuilder implements Builder<BimProjectImpl, BimProjectImplBuilder> {

		private String poid;
		private String name;
		private String description;
		private String importMapping;
		private Boolean isActive;
		private ZonedDateTime lastCheckin;
		private Long id;

		public BimProjectImplBuilder withPoid(String projectId) {
			this.poid = projectId;
			return this;
		}

		public BimProjectImplBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public BimProjectImplBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public BimProjectImplBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public BimProjectImplBuilder withImportMapping(String importMapping) {
			this.importMapping = importMapping;
			return this;
		}

		public BimProjectImplBuilder withActive(Boolean isActive) {
			this.isActive = isActive;
			return this;
		}

		public BimProjectImplBuilder withLastCheckin(ZonedDateTime lastCheckin) {
			this.lastCheckin = lastCheckin;
			return this;
		}

		@Override
		public BimProjectImpl build() {
			return new BimProjectImpl(this);
		}

	}
}
