package org.cmdbuild.bim;

import com.google.common.eventbus.EventBus;

import org.cmdbuild.config.BimserverConfiguration;
import org.cmdbuild.config.api.ConfigComponent;
import org.cmdbuild.config.api.ConfigService;
import org.cmdbuild.config.api.ConfigValue;
import static org.cmdbuild.config.api.ConfigValue.FALSE;
import org.springframework.stereotype.Component;
import org.cmdbuild.config.api.NamespacedConfigService;

@Component
@ConfigComponent("org.cmdbuild.bim")
public final class BimserverConfigurationImpl implements BimserverConfiguration {

	@ConfigValue(key = "enabled", description = "bim server enabled", defaultValue = FALSE)
	private boolean isEnabled;

	@ConfigValue(key = "username", description = "bim server username", defaultValue = "admin@bimserver.com")
	private String username;

	@ConfigValue(key = "password", description = "bim server password", defaultValue = "admin")
	private String password;

	@ConfigValue(key = "url", description = "bim server url", defaultValue = "http://localhost:8080/bimserver")
	private String url;

	@ConfigService
	private NamespacedConfigService config;

	@Override
	public boolean isEnabled() {
		return isEnabled;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public EventBus getEventBus() {
		return config.getEventBus();
	}

}
