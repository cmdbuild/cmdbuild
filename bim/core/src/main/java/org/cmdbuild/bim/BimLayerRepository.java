/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.bim;

import java.util.Collection;
import javax.annotation.Nullable;
import org.cmdbuild.dao.entrytype.Classe;

public interface BimLayerRepository {

	Collection<BimLayer> getAll();

	@Nullable
	BimLayer getByOwnerOrNull(Classe classe);
}
